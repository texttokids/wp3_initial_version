euh donc en fait voilà une maison avec des personnages des animaux et d'autres objets ...
je peux l'ouvrir ?
xxx .
et donc j'aimerais que vous jouiez ensemble en imaginant une histoire ...
vous avez environ vingt minutes et vous inquiétez pas du temps je vous dirai un peu avant la fin euh quand il faudra s'arrêter ...
d'accord .
donc vous pouvez imaginer euh tout ce que vous voulez ...
xxx une histoire avant la fin .
vas y !
c'est parti !
mhm mhm !
ouh !
ouhla y en a des choses !
alors tu veux qu'on fasse quoi en premier ?
on range un peu ?
ouais ?
euh ...
c'est quoi tu fais quoi ?
c'est la chambre ?
tu parles plus du coup .
dis moi .
xxx .
ça c'est une autre chambre ...
c'est une autre chambre ?
oui .
d'accord .
ça c'est la cuisine .
d'accord .
où on met le biberon ?
ben je sais pas y a //.
c'est la cuisine là ?
oui .
regarde parce qu'il y a un bébé regarde .
oui il est là .
tiens regarde ça c'est pour la cuisine .
je te laisse mettre .
ah ben regarde tiens xxx la chaise haute .
mhm mhm .
tu veux mettre ça où ?
euh là .
nan .
ah mais y a deux bébés !
oui .
regarde .
ça c'est les couvertures xxx .
t'as vu ça ?
et le bébé là ?
ah !
tu crois que c'est ça ?
okay .
et je sais pas où mettre .
ça fait beaucoup de choses hein !
on dirait presque notre maison où y a plein de bazar .
ben voilà !
donc magnifique .
ah regarde il ressemble presque au nôtre t'as vu ça !
xxx .
où on met les poussettes .
comme tu veux .
on peut dire qu'y a un espace extérieur nan ?
je sais pas moi .
ben tu me dis .
tiens regarde .
xxx bouquet de fleurs .
yyy .
où il est xxx ?
okay .
bon ben ...
là .
là je pense qu'elle est à peu près bien rangée notre //.
oh ben dis donc eh tu sais quoi alors euh ça ressemble euh //.
on a quoi un bébé un bébé une petite fille ...
une maman ...
yyy .
et puis plein de chats waw .
oups .
et y a un deuxième !
waw des jumeaux !
et un tas de chats .
un tas de chats .
ah .
et c'est important le doudou .
les deux bébés ils vont où dormir ?
ben ils se partagent le berceau .
y en a qu'un .
okay alors tu veux commencer quoi l'histoire on dit que c'est quoi ?
c'est une maman qui lit un livre .
elle lit un livre d'accord .
elle lit un livre toute seule ?
oui .
d'accord .
et pendant ce temps qu'est ce que il se passe ?
lui il prépare le déjeuner .
oups .
parfait .
xxx .
soyons organisées .
et ch chats ...
et les chats alors ils font quoi ?
ils se baladent dans la maison .
oups .
quoi .
okay .
mhm moi je suis ton histoire hin .
très bien .
et la grande soeur elle va à l'école .
elle va à l'école d'accord okay .
elle est allée .
donc là elle est partie à l'école ?
xxx .
comme une grande ?
oui .
comme Léonie le matin ?
attends .
un bébé dort là .
ah c'est l'heure de la sieste et l'autre alors il fait quoi ?
l'autre il va dormir là .
dans le lit de la grande soeur il en profite ?
elle est pas là .
super .
on peut mettre le lit comme ça si c'est plus simple .
zoup !
voilà .
qu'est ce qu'on fait ?
ça c'est le matin en fait ?
oui .
donc la maman lit ...
le papa fait à manger ...
la grande soeur est partie et les bébés dorment .
magnifique .
et alors qu'est ce qui se passe dans ton histoire après ?
et après ils sont réveillés ...
et pourquoi ils se réveillent ?
parce que après c'est ...
c'est fini la sieste .
oui .
ils vont euh comment il lui il va manger ?
y en a qui xxx xxx .
bah soit à tour de rôle soit euh la maman elle peut en prendre un sur ses genoux par exemple .
soulève les bras je pense ça peut aider .
voilà .
xxx .
ah ben pose le il a du mal .
bah c'est pas grave .
hum okay .
xxx on va le mettre là .
d'accord donc lui il prend son biberon .
xxx .
et l'autre alors ?
l'autre il va se réveiller .
xxx .
touk@i touk@i touk@i .
il va se réveiller .
il va là tac@i .
tac@i .
on peut dire que la cuisine elle sort un peu de la maison hein@i si t'as pas de place là bas .
okay ?
j'ai du mal à mettre le bébé .
voilà .
mhm mhm .
ouais attends je vais t'aider .
on va pousser un peu la table pour faire de la place .
ah@i purée !
vas y .
tu tiens ?
bon on bouge plus .
c'est bon .
okay .
ils ont qu'à s'asseoir par terre .
xxx .
okay .
donc là c'est le repas de midi ?
oui .
déjà fiou@i rapide !
et c'est quoi au menu ?
qu'est ce que t'aimes bien ?
qu'est ce que tu voudrais qu'ils mangent ?
riz au thon .
du riz au thon magnifique .
quel plat incroyable !
après ils vont faire une ...
je précise sait faire ça .
d'accord .
ils vont ils vont faire une balade .
ah@i après c'est la balade allez zou@i .
regarde moi je vais te faire un petit chat bazar hop@i le chat il part avec .
zoup@i t'as vu la petite place qu'il a trouvée .
non il doit xxx .
hin@i il va là okay .
avec un autre .
d'accord .
après y en a encore un qui va là .
ah@i dis donc eh@i c'est toute la famille qui part .
et lui il veut aller avec eux .
c'est là xxx .
c'est la poussette .
xxx .
pendant qu'on se balade le papa fait la vaisselle ?
oui .
c'est vrai ?
la classe .
et maman elle doit faire ça .
tu es trop petite .
beuh .
attends xxx .
il pleut pas .
à une main tih@i tih@i .
alors on part où ?
il part en balade .
en balade d'accord .
bin@i écoute je te suis .
hop@i .
ils ont yyy .
xxx .
c'est l'heure de dormir .
encore ?
oui .
ah@i bah@i c'est la sieste de l'après midi alors ?
nan c'est l'heure de dormir c'est la nuit .
le soir ?
nan mais t'as oublié un moment très important de la journée .
xxx .
c'est quand quand la grande rentre de l'école et que les bébés sont un peu énervés .
les xxx .
ça te rappelle quelque chose ?
oui .
et alors qu'est ce qu'on fait ?
on va aller au dodo !
ils sont d'accord ?
oui .
attends .
ah@i c'est des bébés sympas .
le chat il reste là .
euh la petite fille elle dort là .
elle dort pas encore ?
si elle dort la petite fille c'est /.
elle fait comme Léonie ?
elle se cache pour lire dans le lit ?
nan .
nan elle fait pas ça la petite fille ?
ils vont dormir .
ils vont dormir ?
du premier coup comme ça sans rien dire ?
et le papa aussi hein@i .
ah@i ben@i dis donc tout le monde au lit pouf@i .
et la maman aussi .
la maman aussi .
elle dort là .
et le l'autre bébé il va dormir avec la maman .
hop@i zoup@i .
hop@i .
xxx .
allez on va on va dire qu'ils dorment sans .
xxx .
voilà !
tout le monde est au lit .
c'est bon la maison est bien rangée ?
euh les chats .
et alors les chats ils font quoi ?
ils vont /.
ils mettent le bazar pendant la nuit ?
oui ils veulent xxx ils dort .
ils dorment aussi ?
c'est une famille sage ça .
ouais .
chacun sa cachette .
y en a un qui est derrière .
il doit y en avoir un qui va aller là .
d'accord .
on dit xxx dodo .
eh@i les couvertures !
les couvertures .
quoi ?
rien .
il avait failli y avoir chute .
très bien .
et donc là qu'est ce qu'il se passe maintenant ?
euh hum@i .
un événement extraordinaire ?
la maman elle allait en courses !
elle va faire les courses ?
oui !
ça c'est extraordinaire ouais .
avec ses bébés .
ah@i ouhla@i oui ça c'est super !
d'accord elle va aller aux courses .
tac@i .
les deux ?
oui !
allez hop@i .
tout le monde descend .
et les autres ils vont aller ...
ils se fait une grasse matinée .
ils se font une grasse matinée ?
ben@i ça c'est Léonie nan ?
dans le lit ?
qui voudrait dormir tous les matins .
zoup@i hop@i ils emmènent le doudou .
tiens tu as oublié un élément très très essentiel là si elle veut payer ses courses .
allez c'est parti .
ils ont même un chat .
alors elle va acheter quoi ?
on dit que le magasin c'est là derrière hein@i .
qu'est ce tu en penses ?
xxx .
et elle laisse les bébés comme ça ?
oui mais ils sont ...
là y a un truc qui cache hein@i .
ah@i ils sont cachés c'est bon on peut les laisser dehors comme ça .
xxx .
alors elle a acheté quoi ?
elle a acheté du pain ...
okay .
elle a acheté du lait ...
elle a acheté du beurre ...
elle a elle a acheté du riz et du thon !
ah@i oui très important pour refaire du riz au thon ?
oui .
ah@i bah@i oui .
parce que la famille adorait le riz au thon .
ah@i ça c'est une famille parfaite pour toi .
tac@i .
et ils mangent pas de légumes ?
un peu .
un petit peu .
donc touc@i touc@i touc@i touc@i touc@i touc@i touc@i .
très bien .
j'ai faim .
elle elle veut rentrer hein@i .
regarde regarde c'est magique joup@i on peut rentrer avec la poussette dans la maison .
comme ça ils sont déjà installés .
elle vide elle vide elle vide elle vide elle met les elle met les ingrédients là ta@i ta@i ta@i .
alors comment on fait du riz au thon tu sais toi ?
ben@i attends .
elle va chercher un bol ta@i .
hop@i .
tu connais pas la recette ?
nan .
comment on cuit les pâtes alors ?
on met de l'eau bouillante .
attends y a un truc .
ah@i nan c'est rien .
mhm mhm .
attends je vais faire le repas xxx .
d'accord donc là c'est nouveau riz au thon comme hier ?
oui .
elle prend le le thon elle le coupe .
elle prend une assiette pour mettre le thon tac@i tac@i .
elle coupe .
après elle prend le riz tic@i .
elle met de l'eau bouillante .
d'accord .
et voilà elle laisse fondre et elle elle va lire des livres .
eh@i bah@i elle lit beaucoup cette maman .
oui je sais elle adore lire .
d'accord .
elle va dormir en dormant .
elle va /.
et pendant ce temps tout cuit ?
oui .
tranquille .
oh@i regarde .
et par contre euh Léonie elle dort toujours là haut hein@i .
ouais je sais .
après elle elle va aller à l'école .
je croyais que c'était grasse matinée ?
mais elle a déjà fait sa grasse matinée .
elle a fait sa grasse matinée .
elle va aller elle va aller en voiture .
de toute seule ?
mais nan avec avec son papa .
ah@i !
le papa se réveille quand même .
d'accord .
allez on va aller en voiture .
vas y fais .
allez hop@i on fait semblant .
hop@i hop@i hop@i hop@i .
faut que tu restes dans le champ de la caméra quand même poulette .
et hop@i .
xxx voyage xxx .
donc ils ont bien voyagé ?
donc c'est bon .
il revient .
Léonie est à l'école .
regarde on la voit plus elle est à l'école fut@i cachée .
pendant ce temps ...
pendant ce temps qu'est ce qui se passe ?
bah@i le papa ...
emmène un bébé .
d'accord .
il est en train de dormir .
ils dorment beaucoup ces bébés ils sont sympa hein@i .
après ...
il prend le doudou .
il va emmener le doudou avec .
d'accord .
regarde .
il l'emmène se balader ?
oui .
ouh@i .
tu y arrives viens ?
tac@i .
tac@i .
vas y .
tac@i .
voilà .
elle //.
ah@i il tombe .
après elle va récupérer la grande soeur .
ah@i d'accord ils vont à l'école .
salut !
je vais aller à la maison toute seule .
ah@i oui .
ça tu aimerais bien hein@i .
oui .
et là c'est quoi c'est l'heure du goûter alors ?
nan .
pas encore ?
si !
si .
ils vont manger !
ils vont manger quoi ?
du riz au thon .
du riz au thon au goûter aussi ?
mais oui ils ont fait du riz au thon .
ah@i oui c'est midi c'est vrai .
aïe@i aïe@i ils vont manger là .
d'accord .
les bébés aussi mangent le riz au thon ?
oui .
allez hop@i .
tout le monde mange du riz au thon .
allez je vais aller mettre mon sac là .
d'accord c'est bien rangé .
et là ils sont à table alors tous zou@i ?
ils vont tous manger .
et les bébés ils vont rester dans la poussette c'est pas mal .
et la maman elle est où ?
elle est en haut .
allez viens on va la chercher .
elle est en train de xxx .
maman il faut dormir //.
il faut aller manger !
il faut dormir ?
zoup@i zoup@i zoup@i .
et là attention question piège de la maman ...
tu es prête ?
alors ma chérie qu'est ce que tu as fait ce matin à l'école ?
euh ...
alors ?
j'ai fait des maths .
t'as fait des maths ?
oui .
mhm mhm .
je fais yyy .
et alors c'est quoi en maths ce que tu fais ?
enfin ce qu'elle fait la petite fille .
les euros .
les euros .
les euros ?
oui .
elle apprend à compter les pièces ?
oui .
hin@i hin@i .
attends .
très intéressant .
attends je vais avec les chats là .
attends eux ils vont manger son gr son pâté de chat .
son pâté de chat .
mais oui ça existe .
lui il mange là .
lui il mange là .
et lui il mange là .
eh@i ils mangent des trucs bizarres dans cette famille entre le riz au thon et le pâté de chat euh .
quoi !
tu as quoi de chose quoi d'autre encore sur la table de bizarre ?
euh ...
des fleurs .
à manger ?
euh ...
du chocolat .
du chocolat .
nan .
hum@i euh quand ce sera fini le film là ?
pourquoi ?
parce que si c'est ça faut xxx finir .
tu veux savoir quand ça se finit ?
oui .
hin@i .
tu peux encore euh jouer euh quelques minutes .
ah@i .
c'est comme tu veux .
tu peux encore .
moi je propose de savoir qu'est ce qu'ils vont manger en dessert .
on va manger ...
moi je pense que la maman elle va prendre juste un yaourt nature sans sucre .
moi je vais manger du flan .
du flan ?
du flan aux oeufs avec du caramel qui coule ?
ouais .
huuuum@i .
moi je mange je vais aller dormir tou@i tou@i .
bo@i bo@i bo@i c'est quoi cette famille où ils dorment tout le temps ?
mais quoi ?
nan mais c'est bien .
quand ils dorment là quand ils dorment il fait il lit un livre hein@i .
ah@i il lit aussi .
dis donc dans cette famille ils lisent ils lisent hein@i .
zou@i tiens regarde fut@i .
bah@i moi j'ai fini de manger .
et alors là la maman la maman et la petite fille ils en profitent pour faire quoi ?
caca .
pour faire tomber .
euh .
on peut aller jouer dehors !
attends .
tu veux //.
ah@i elle aussi elle est au lit ?
et elle fait quoi au lit ?
décidément .
xxx .
elle dort .
elle dort aussi ?
elle joue avec son chat !
d'accord !
zoup@i .
là y a la cruche .
vous pouvez finir quand vous voulez .
c'est ...
d'accord .
ben@i Léonie tu me dis .
euh ...
tu veux qu'on finisse comment ?
en faisant la révérence comme au théâtre ?
nan !
nan ?
tu veux finir comment dis moi ?
quand il dort .
quand il dort allez quand il dort .
d'accord .
bon bah@i les bébés ils dorment dans la poussette .
et il manque qui encore ?
qui dort toujours pas ?
là ...
parce que je pense qu'elle est en train de ranger des trucs là nan ?
maman .
tiens je te laisse la mettre .
moi je te range le sac .
comment comment tu l'avais mis ?
ça tient ?
comme ça ?
oui .
euh .
elle elle va tomber du lit après .
à cause d'un cauchemar ?
nan .
elle tombait du lit et elle dormait par terre .
pff@i ben@i sympa .
d'accord .
le papa il levait ses bras .
alors je vais le mettre comme il faut .
tac@i .
voilà .
parfait .
et voilà .
oh@i il dort avec ?
hum@i .
sympa .
il veut pas dormir seul .
xxx il va dormir là .
d'accord .
et c'est fini .
et c'est fini .
c'est fini ?
clap de fin .
okay .
super alors je coupe le film .
