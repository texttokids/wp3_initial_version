ouais !
il faut que tu expliques ce que c'est aussi à la caméra Léonard .
alors qu'est ce que c'est ?
il faut que tu expliques ce que c'est ce truc rouge .
c'est un clown ?
explique à quoi ça sert peut être .
c'est pas très précis si tu dis un truc rouge .
alors fais voir les travaux .
oh là là c'est dur les travaux .
il faut que tu montres comment tu fais les travaux .
essaie de le mettre là dedans voir si ça rentre .
ben ça rentre très bien là dedans j'arrive pas à sortir le vert .
on va y arriver .
faut pas pleurer c'est ça le secret des trucs c'est qu'il faut jamais pleurer .
c'est ta chambre ici Léonard ?
et tu dors ici .
wah !
voilà ça va mieux comme ça .
attends il faut pas s'énerver .
on va trouver une solution .
déjà j'avais fait un petit progrès avec ça .
dis moi Marie Eve t'envoie tous ses baisers ...
ouais .
on y est arrivé !
et quand je lui ai dit que t'étais enceinte elle m'a demandée si c'était un garçon ou une fille et je sais même pas si tu sais .
non tu veux pas savoir ?
non .
il me semblait que tu voulais pas savoir .
ouais alors .
est ce que t'es sûr que c'est toi qui l'a trouvée ?
ah que j'ai un peu de mérite quand même .
on avait oublié plein de trucs .
regarde tout ce qui traîne !
y a plein de choses là qui doivent être dans les travaux .
ah ouais .
oui .
ah très bien .
et raconte moi Léonard .
comment tu fais pour aller à l'école en ce moment .
t'habites loin .
c'est loin hein .
t'y va comment .
en voiture en métro ?
wouh ah quel luxe !
c'est qui qui t'amènes ?
à l'école ?
ah non si tu fais ça on verra rien du tout sur la caméra .
il faut pas mettre des choses près Léonard .
alors tu pars à quelle heure le matin tu sais ?
après le petit déjeuner .
t'as le temps de prendre le petit déjeuner .
t'es pas trop pressé ?
oh !
mais t'es un petit roi !
qui c'est qui te l'amènes ?
ah oui !
le roi chevalier Léonard .
et c'est qui qui t'amènes ton petit déjeuner dans ta chambre ?
ah dis donc elle est sympa Manette hein ?
et à la maison à Ménilmontant tu prenais ton petit déjeuner où .
tu te souviens ou pas ?
ouais c'est différent à Ménilmontant .
ça fait mal aux oreilles Léonard pas trop .
tu sais ça va faire mal aux oreilles du bébé .
Léonard quand tu fais beaucoup de bruit ...
écoute ce qu'elle dit Aliyah .
ça fait du mal aux oreilles .
ça fait mal oreilles du bébé dans le ventre de maman alors il faut être tout doux .
voilà .
alors s'il est un peu cassé xxx ben là il faut un peu taper .
et toi tu trouves pas que c'est mieux comme ça .
mais montre moi où il est cassé ce truc .
il est fendu .
le quoi le décor ?
ouais .
ah bon .
et Léonard t'as pas raconté à Aliyah qu'on est allé voir le film de papa .
déjà .
non on est allé voir le travail .
comment il travaille .
c'était comment ?
avec le silence ?
ouais .
comment c'était avec le silence ?
tu expliques à Aliyah comment on fait avec le silence ?
huh !
alors toi t'étais dans le film ?
et c'était où ?
et y avait d'autres enfants ?
et toi t'étais le plus petit ?
t'étais le plus petit toi ?
et maman aussi elle était dans le film ?
t'as entendu ?
maman aussi elle était dans le film ?
juste toi .
et c'était papa qui filmait .
ah maman aussi .
et papa t'as vu comment il filmait avec la caméra ?
tu te souviens ?
et il est presque fini ce film ?
dans la cour où il fait le film y a un camion ?
huh .
ah oui la lumière qui faisait comme le jour .
comme si c'était le jour .
ouais .
en fait il a fini le tournage et là il est en train de monter les vraies informations .
oh mais il est toujours cassé ce truc !
et ce film il va passer dans les grands circuits ?
ouais ouais il est distribué par ...
Karmitz .
je sais même pas qui c'est Karmitz .
tu sais qu'a des salles quatorze juillet .
ah ouais !
et comment ça se passe .
si ça marche bien ça peut être distribué par d'autres .
ou ça reste dans tous les quatorze juillet .
non de toutes façons il va pas être que dans les quatorze juillet .
aussi dans des autres salles parce que c'est plus c'est plus heuh ...
c'est plus compliqué que ça y a pas d'exclusivité .
ouais je sais pas exactement comment ça marche .
je sais juste qu'il a un distributeur ...
eh moi j'ai mal aux oreilles .
oh que c'est trop fort .
moi j'ai mal aux oreilles .
ouais .
ah bon ?
les petits pots oui .
huh huh .
et dis moi qui est ce que ça fait de ...
la décision de ne pas savoir si c'est un garçon ou une fille c'est pour quoi ?
t'as pas envie de fixer déjà l'image du bébé ou ...
ben j'sais pas ...
et Dante i ...
Dante il aime il aurait aimé savoir lui .
ah ouais .
ouais .
et Léonard aussi il aurait aimé savoir .
mais j'sais pas .
je me suis imaginé pendant qu'on me le disait ...
tu vois je me suis imaginée la scène moi à l'échographie .
et quelqu'un me disant c'est une fille ou quelqu'un me disant c'est un garçon .
et c'était un sentiment très désagréable intuitivement .
ah ouais .
ouais .
même si tu vois sur l'écho .
donc tu ne regardes pas tes écho quand il te les font .
ça c'est un o@l .
qu'est ce que c'est comme lettre ça ?
ça pourrait être un h@l très bizarre .
très bizarre hein .
parce que normalement la jambe du h@l à droite elle est aussi grande que la jambe du h@l à gauche .
si c'est comme ça regarde si c'est comme ça c'est un t@l .
comme dans tonton Laurent .
et tu peux même faire un l@l .
est ce que tu sais faire un a@l ?
ah ben oui .
oui aussi oui ça peut être plein de choses .
et est ce que tu sais faire un a@l .
avec ce petit truc là en plus .
je filme ton a@l quand même tu es génial Léonard .
le a@l de Léonard .
et est ce que tu sais faire un i@l ?
t'as jamais appris ?
regarde .
oh c'est facile .
hop voilà un i@l .
faut un petit point dessus quand même .
un petit point .
ah oui .
ah ça fait moins de bruit comme ça c'est mieux .
moi je préfère .
et en fait je crois que tant que l'enfant n'est pas là pour moi c'est pas un enfant .
c'est une idée d'enfant ou c'est ...
donc tu préfères garder l'idée .
en plus si tu veux c'est pas euh ...
j'ai pas une relation de personne à personne avec cette personne parce qu'elle est pas encore dans le monde des personnes .
et le fait de savoir si c'est une fille ou un garçon ça me paraît être un élément tellement précis .
par rapport à tout ce qui est relationnel .
que ça me parait être une relation déplacée .
par rapport à tout ce qui est organique animal mais pas du toutcomme une relation humaine quoi .
du coup ça m'était désagréable comme vraiment un truc vraiment déplacé quoi .
j'sais pas .
j'ai pas du tout eu envie .
je me suis posé la question puisque Dante aurait bien voulu savoir .
et puis il me faisait le chantage genre et si je sais pas si c'est une fille ou un garçon je vais pas réussir à trouver un prénom .
et vous avez trouvé deux prénoms ?
tu rigoles on n'a même pas commencé à chercher .
c'est te dire à quel point c'est pas encore une personne .
alors il faut un petit bâton supplémentaire .
un grand a@l comme ça a@l !
et Léonard il pose des questions sur l'accouchement tout ça ?
ouais .
c'est un grand a@l .
ah ouais il est très large .
très large .
et si tu fais comme ça regarde .
si tu fais comme ça c'est un h@l .
et tu sais faire un l@l ?
comme dans Léonard .
c'est deux i@l .
oui des i@l voilà .
et un ...
ouais .
alors ça c'est ...
j@l .
j@l un peu .
un peu .
et ça c'est un v@l c'est un w@l même .
et un l@l regarde comment on fait un l@l .
on prend un grand et un petit .
comme dans Léonard .
Léonard ça commence par l@l Léonard .
l@l .
ah c'est plus un l@l .
c'est un i@l avec son bébé i@l collé .
oh il est petit le papa .
ça c'est le papa .
et le papa il est où ?
ah !
et ce que t'as dans la main c'est quoi ?
y a qu'un seul bébé ?
aussi .
ah !
mais alors celui dans le ventre de ta maman il va sortir ?
ah !
et comment i va faire pour sortir ?
non entre les deux .
entre les deux .
pas par les deux entre les deux mais un petit trou spécial spécial exprès pour les enfants .
parce qu'il m'a dit est ce que ça va déchirer le ventre ?
mais non .
il va faire toc toc d'abord avant de sortir .
mais non !
ah bon ?
oh ben pose ton marteau alors .
et là tu peux entrer ?
oh elle est détruite la porte comme ça .
mais qu'est ce que tu fais avec tous ces trucs jaunes rouges et verts .
moi je voudrais comprendre .
tous les petits trucs par terre .
tout ça .
t'entends ?
qu'est ce que tu fais avec tout ça Léonard ?
bing !
est ce que tu entends quand Aliyah te parle Léonard ?
ou est ce que t'es devenu sourd ?
ouh ouh !
alors est ce que tu sais ce qu'elle t'a demandé Aliyah ?
t'as pas écouté ?
c'est quoi ?
un quoi ?
c'est un quoi ?
qui va où ?
ah !
très très bien .
ah bon .
et la mer elle est où ?
alors le bateau il est sur la plage .
qu'est ce qu'il fait de magique ?
ah !
ah bon !
oh !
et qu'est ce qu'il lui a dit ?
c'est tout ?
c'est quoi ?
tu joues à la marelle ?
ah !
et tu l'as vu le film ?
et ben moi aussi .
où est ce que tu l'as vu Léonard ?
oh dis donc .
oui je filme le livre .
où est ce que tu l'as vu le film de Aladin ?
tu te souviens plus où on était quand tu l'as vu ?
menteur .
on était en vacances à ..?
où est ce qu'elle habite tante Elsa ?
mais non !
tu dis n'importe quoi .
à Lon ..?
mais tu dis n'importe quoi .
à Londres .
tu te souviens qu'on était à Londres ?
huh !
tu l'as vu en anglais le film ?
dis donc et t'as compris ?
et montre à Aliyah comment tu sais parler anglais Léonard .
youre@seng welcome@seng .
t'es resté combien de temps à Londres ?
non ça fait trop de bruit Léonard !
tu vas tuer le film .
tu as aimé Londres ?
t'as tout ce dont un n'importe quel petit garçon pourrait rêver .
kangourou .
Bernard au pays des kangourous .
ben moi je connais même pas .
tu me racontes l'histoire ?
non ils sont pas là les trucs du zoo Léonard .
ils sont dans le sac vert parce qu'on les avait amené en Angleterre .
ils sont dans le sac vert si tu veux les montrer à Aliyah .
c'est le sac vert ça ?
non mais sort un peu .
non pas sur ses pieds pas sur ses pieds .
voilà là oui .
ben oui pourquoi tu les mets sur ses pieds c'est pas malin .
attention à la plante .
comment je fais pour filmer moi ?
ben oui je vois .
elles sont mexicaines ?
tu te souviens où tu l'as eu la cochonne ?
ouais .
à Londres ?
à Londres le Mac Donald ou à Paris ?
il a des cousins ?
où ?
à Londres .
non ma soeur elle a pas d'enfants .
c'est ma petite soeur .
mais enfin c'est comme si c'était sa cousine parce que ils ont le même âge mental .
non je veux dire ils s'entendent très bien .
ah d'accord .
ben il faudra lui faire plein de cadeaux au bébé quand il va sortir .
mais qu'est ce que tu vas lui donner d'autre ?
faut que tu prépares le cadeau hein .
parce que tu vas être le grand frère .
ben vous allez habiter dans la même maison avec le bébé tu crois pas ?
tu crois qu'il va habiter où le bébé Léonard ?
ben tu pourrais peut être lui en donner un au bébé .
alors tu crois qu'il va habiter où le bébé quand il sera sorti ?
tu sais pas ?
comment tu sais pas .
mais non il va habiter Ménilmontant avec nous patate .
hein tu le sais .
plus fort plus fort .
tu veux que je souffle ?
alors tu vas pas lui trouver des cadeaux pour le bébé ?
tu peux aller dans un magasin puis lui trouver un cadeau pour bébés .
parce que tes jouets à toi c'est des jouets de grand garçon .
le bébé il a besoin de jouets pour bébé ...
c'est pas les mêmes .
ouais .
tu vois ?
qu'est ce que tu vois ?
tiens on voit qui c'est qui arrive on va filmer .
oh elle a pas fini son petit déjeuner .
elle a même pas pris son goûter .
je viens de commencer .
en fait j'ai pris un café en rentrant .
d'ailleurs je fais de la tachycardie depuis je suis comme ça ...
ah !
ça fait de l'effet !
ça me fait beaucoup d'effet ce café .
je sais pas comment faire pour ...
pour aérer parce que ...
j'ai aéré partout .
j'ai ouvert celle là .
j'ai ouvert celle là xx .
j'ai tout ouvert .
on peut pas .
à part ça moi j'adore cette odeur .
c'est un peu comme de l'éther .
ouais j'aime bien .
c'est pour ça plus le café .
j'suis complètement .
oh qu'est ce que c'est ?
attends .
tiens d'abord tu me rends un service ...
chère demoiselle ou chère dame je ne sais pas .
voulez vous une tasse de thé ?
mais je veux bien c'est très gentil .
et toi chère dame .
un petit whisky pour moi s'il te plaît .
un petit whisky tu peux toujours courir .
c'est marrant parce que tu sais que papi il a fait venir une autre bonne femme ce matin pour remplacer xx .
ah oui il a trouvé quelqu'un ?
et cette autre dame ce matin elle nous a vues à la laverie .
et elle s'est bien marré .
ah bon !
en train de xx .
parce qu'on a fait des conneries à la laverie .
qu'est ce que vous avez fait ?
on a mis dans le nettoyage à sec des coussins alors qu'on a pas le droit de mettre de coussins .
mais y avait marqué pas d'oreiller .
et pour moi un oreiller c'est pas pareil qu'un coussin .
pas d'oreiller pas de couette .
je pensais que c'était une histoire de plumes .
et qu'est ce qui s'est passé ?
eh ben y a un des coussins qui s'est mis à se déniapper en direct .
il s'est ouvert et tout est sorti .
et comme c'est des coussins qu'a fait la grand mère de Dante avec des tas de chutes ...
ah oui !
des bouts de fourrures des bouts de chiffon des bouts de ce que tu veux .
et moi je voyais ça et c'est de la science fiction xx .
et après y a le mec du lavomatic qu'est arrivé .
il a pas vu qu'il s'était déniappé .
il a dit vous avez mis des coussins dans la machine faut pas !
nous après on était comme ça monsieur on savait pas et tout .
mais c'est marqué .
on croyait que c'était les plumes .
c'est des coussins .
y a marqué des oreillers .
alors on a fait des conneries .
et depuis i puent parce que le produit n'arrive pas à s'échapper .
enfin on a fait que des conneries .
va jeter ça à la poubelle mon chichou d'accord ?
mais c'est ceux de la maison de coussins .
de ma maison .
tout est de chez elle .
tu vas jeter ça à la poubelle mon chéri .
oui .
la petite sorcière .
qu'est ce qu'elle fait la petite sorcière ?
tu sais ce qu'elle lui demande à l'oiseau Léonard ?
elle lui demande ...
attends excuse moi je vois pas le texte .
elle s'appelle comment cette petite fille ?
est ce qu'elle s'appelle Pondarinette ?
est ce que tu trouves que c'est un joli nom pour une petite fille ?
bon alors Pondarinette dit à l'oiseau .
qui es tu ?
que fais tu ..?
mais tu le connais par cœur ce livre alors .
tu as raison .
c'est absolument vrai .
c'est l'oiseau qui dit .
qui es tu ?
que fais tu ?
qui es tu que fais tu ?
alors elle qu'est ce qu'elle dit ?
ouais je suis huh ...
qu'est ce que t'as fait ?
qu'est ce qu'il a fait ?
bon !
ne recommence pas .
d'accord .
elle dit je suis Pandarinette .
et après qu'est ce qu'elle dit ?
je suis très ...
pauvre .
qu'est ce ça veut dire pauvre ?
oh mais c'est vachement moderne !
ah mais c'est marqué c'est pour ça .
et je vais de par le vaste monde pour chercher du travail .
parce que je n'ai pas de sous pour m'acheter un nouveau parapluie .
alors l'oiseau qu'est ce qui dit ?
encore il répète la même chose .
alors qu'est ce qui dit ?
vas y fais le toi .
je te l'ai déjà dit .
je suis la petite Pondarinette .
et je marche au milieu de cette forêt sombre et profonde pour chercher fortune .
et l'oiseau i recommence .
cet oiseau sur la branche doit être un perroquet .
il répète tout le temps la même chose .
et l'oiseau i dit .
pas du tout pas du tout pas du tout .
oh !
et qu'est ce qui se passe ?
oh regarde !
qu'est ce qui dit le loup ?
ah ah ah !
ah elle lui tape dessus avec son parapluie .
parce que lui il veut la ..?
qu'est ce qui veut lui faire à Pandarinette ?
ouais .
et qui c'est qui va la sauver .
ouais comme ça paf !
je vais l'assommer avec mon parapluie .
oh qu'est ce que c'est ?
bah qui c'est ça ?
oh dis donc elle a le bras long .
qu'est ce qu'elle lui dit la sorcière à Pandarinette ?
tu crois qu'elle est contente de voir Pandarinette ?
ah !
elle elle veut que ça devienne sa cuisinière !
alors qu'est ce qui se passe ?
ouh là là là là !
la voilà dans la cuisine de l'horrible sorcière et elle va regarder dans les placards .
et qu'est ce qu'elle va trouver ?
c'est quoi ça ?
moi non plus .
un trible mm ça fait peur ?
c'est vrai il a un joli sourire oui oui c'est vrai .
voilà ben puisque tu fais des cachotteries puisque tu es caché je te referme dans le placard .
alors ensuite .
oh minuit a sonné .
c'est l'heure où les sorcières prennent leur balais .
alors le trible tu sais ce qu'il dit c'est l'heure où tu devraisapprendre à devenir une sorcière !
il est gentil .
alors Pandarinette puisque la sorcière s'est envolée ...
t'as vu la sorcière par la fenêtre ?
xxx .
qu'est ce qu'elle fait Pandarinette ?
ah il est cassé ?
qu'est ce qu'elle faisait avant qu'il soit cassé ?
ça se transformait ici ?
ah elle savait transformer une souris en loup ?
ouais je vois rien que t'es sincère et tout tu regrettes .
xxx .
non t'as pas envie là .
j'sais pas .
non non moi j'prendrai plutôt un orangina .
si j'prends un petit thé avec du citron .
un petit thé avec du citron .
mais on va venir dans la cuisine .
oui .
alors qu'est ce qui se passe ici ?
ça c'est un livre avec marqué gnafgnafgnaf@wp .
tu sais ce que ça veut dire ?
oh !
c'est une formule magique .
comment c'est la formule magique de pépé Aldo ?
tu sais .
oh tu montres à Aliyah le tour de magie .
comment on fait le tour de magie .
tu sais faire ?
vas y fais lui .
attends d'abord ...
montre lui d'abord ton doigt .
montre lui bien .
un doigt .
maintenant .
wouh ah !
deux doigts .
ouais c'est fort hein !
grâce à la formule magique outchilegoutchilebenkatou@wp .
alors qu'est ce que c'est que ça ?
oh !
qu'est ce qu'il a fait .
radibididon^radibididaine@wp .
oh c'est lui qui s'est transformé en œuf .
le trible !
euh !
oh !
ah mais c'est Pandarinette qui l'a transformé en œuf .
elle est pas très douée encore .
qu'est ce ...
que on dirait qu'il boit de la soupe .
ah bon .
et c'est bon ?
d'accord .
et là qu'est ce qui se passe ?
oh elle a fait une grosse bêtise !
bien sûr mais on va pas le faire .
qu'est ce que c'est que ça ?
c'est le prince Pandarino !
oh ben .
toi t'es le prince quoi Léonard ?
ah d'accord .
c'est mieux qu'un prince .
alors attends .
et maintenant ...
mais c'est à l'envers que tu lis qu'on le lis .
mais va t'en petite souris .
et la petite souris qui fait au secours je tombe du livre ah !
on lui a volé son balai à la sorcière .
et qui est ce qui lui a volé son balai ?
moi je crois que c'est Pondarinette et Pondarino ...
oui !
regarde !
ils ont volé le balai pour s'envoler dans un lointain pays .
alors bonjour les amis !
oh !
maintenant c'est toi qui fait l'histoire .
moi j'écoute .
toi tu fais le spectacle .
comment on fait pour faire le spectacle ?
tu nous fait le spectacle ?
ils sont ingonfables ces ballons Léonard .
ingonfables .
t'as raison .
hum !
mm !
ah t'as retrouvé le mot .
un squelette dans le placard .
en plus t'as retrouvé toute l'expression .
ouh !
mais tu es intelligent toi !
Léonard y a ...
Manette vient de nous appeler dans la cuisine .
pour boire le thé .
pour boire le thé .
parce que comme tu as pu le remarquer Aliyah n'a pas bu de thé encore .
la pauvre elle est tout déshydratée .
regarde comme elle est desséchée .
faut qu'elle aille boire un thé .
comme le petit garçon qu'avait très soif dans le désert .
ah oui mais c'est cassé quand même .
Léonard !
regarde ma belle moustache .
et je te permets pas !
je vais te faire une fessée si tu m'enlèves ma moustache !
xxx .
bon on va prendre le thé .
oh il est long ce livre .
il met trois heures à finir .
on en était là .
ça ne marchait pas .
alors là qu'est ce qu'y a ?
un squelette dans le placard !
et là qu'est ce qu'y a .
tatatam@wp !
un squelette dans le placard .
pas original .
et là .
une sorcière derrière sa porte .
et là tututi@wp .
qui es tu ?
que fais tu ?
je n'ai pas d'argent .
je cherche du travail .
et je fais aussi des coups de parapluie au méchant loup .
au revoir !
vas y .
le thé il va être tout froid après .
bon moi je m'endors Léonard .
tu viens boire le thé sinon je vais m'endormir Léonard .
regarde regarde Léonard .
y a Manette qu'a crié quelque chose .
y a Manette qu'a crié .
elle a crié vite !
le thé va refroidir !
allez viens .
aide moi à me lever .
allez tire là .
fort .
allez fort fort fort de tous tes muscles .
bravo t'es très fort .
non pas comme ça comme ça .
non c'est comme ça qu'i fait .
ah wah !
bouh !
c'est bien Léonard .
ah non non non maman c'est le bordel le thé chez Léonard .
allez viens Léonard .
on va prendre le thé .
allez !
tu me montre où est la cuisine .
oh tu m'as acheté du pain complet c'est vachement sympa .
oui parce que y avait une queue énorme à la boulangerie .
et comme j'étais dans le truc xx .
mais t'as bien fait c'est bon ça .
il est bon jusqu'au vingt deux janvier en plus .
j'espère que je serai chez moi !
ma poche ?
elle est là .
qu'est ce que c'est que ça ?
qu'est ce que c'est que ça ?
ah ah !
qu'est ce que c'est ah !
mais qu'est ce que ça fait dans ma poche .
mais qui est ce qui l'a mis dans ma poche ?
mais qui c'est Léonard ?
ah bon mais t'es qui toi .
ah bon !
mais d'où tu sors .
mais d'où ?
c'est qui ton papa et ta maman ?
il est où ?
ah d'accord !
oh !
c'est qui ?
ben c'est Darwin .
ben j'regarde .
ouais .
il a plus de chapeau .
et lui c'est qui ?
ben qui c'est celui là .
c'est un autre .
c'est pas le même .
oh ça c'est chic !
et un crayon faudrait aussi .
on peut lui faire des trucs !
mais je regarde comme ça .
ouais .
ah elle est féroce .
il a une narine .
ils sont bizarres ses yeux .
il respire de travers comme ça .
il a une narine plus haute que l'autre alors i respire de travers .
c'est des petites bêtes ?
oh là là .
oh là là !
il a besoin de chirurgie esthétique .
c'est cubiste .
ouais .
ah ouais ?
un cul sur la tête .
mais elle regarde elle regarde .
mais je joue pas je regarde .
elle regarde elle regarde .
elle regarde à travers sa caméra .
je filme .
et ça va aller où ?
moi je crois qu'on a besoin d'une bombe là .
il commence à devenir très plein ce dessin .
non c'est rien à côté de ce qui fait .
il peut aller beaucoup plus ...
mais c'est bizarre ce truc .
et ça c'est quoi ça fait quoi sur les dessins ?
ah c'est une petite loupe .
au fait Agnès la bourse hachette c'est qui qui l'a eue ?
c'est Florence !
c'est pas vrai !
oh j'savais pas !
mais si .
oh dis donc .
donc elle écrit son bouquin .
ah ben elle l'aurait écrit de toutes façons .
donc elle prête de l'argent à ses parents .
donc elle veut m'acheter une voiture .
donc elle aide son frère à ...
c'est ça en fait .
oh j'aime bien ça le code barre .
ou !
wau !
ah ben dis donc .
alors maintenant qu'est ce que tu vas faire là ?
ouais parce qu'il va falloir recommencer .
ah !
maintenant tu peux faire un petit dessin dans l'carré .
ouais .
ah ben dis donc .
c'est un dessin surréaliste .
mais tu m'expliques ce que t'es en train de faire .
hein tu m'expliques ?
mais je vois mais je comprends pas .
ben y a quoi sur ton dessin ?
on comprend plus rien .
ah !
c'est la guerre alors .
bom !
t'effaces ?
mais si on voit le blanc .
mais moi je le vois le blanc .
il faut que tu fasses ...
tu dessines une histoire .
ben si .
alors raconte l'histoire que t'es en train de dessiner .
non .
alors .
ça va être bientôt .
ça va être tout effacé .
y aura plus rien .
faut laisser un petit bout non ?
hein .
boum !
je fais une diversion .
j'éteins ?
ah je suis fatigué moi .
je te filme plus alors .
parce que tu veux pas que je regarde dans ma caméra .
et ce soir qu'est ce que tu vas faire ce soir ?
hein ?
tu regardes quoi ?
il va rentrer bientôt papa .
et vous allez dîner ?
c'est tout fini .
tout effacé .
ouais .
ça fait une forêt .
qu'est ce qu'on met à la place ?
dans les arbres ?
c'est le facteur qui les a perdues .
elles sont envolées partout partout dans la forêt .
il a plein de mains .
c'est un monstre ce facteur .
y a eu beaucoup de pluie alors y a beaucoup de boue .
oh mais c'est quelqu'un qui a marché là dans la forêt .
c'est le facteur ?
oh là là on les voit plus .
il sont noyés dans la boue .
à l'aide à l'aide !
on est plein de boue !
ah ouais .
un deux et ...
et tu l'as vu où le crabe ?
très loin ou y a très longtemps ?
et tu l'as pris dans tes bras ?
tu l'as pris par la main le crabe ?
par la pince ?
il a fait pak@o .
t'as saigné ?
ah !
alors t'avais pas peur .
ah !
c'est qui la jeune fille qui là sur la photo .
Léonard ?
là sur le mur la photo c'est qui ?
regarde là la photo c'est qui ?
sur le mur derrière la lampe .
elle est belle !
elle ressemble à maman ?
un œuf au plat .
tu vas le manger ?
tu vas le manger l'œuf au plat ?
ouais mais c'est pour protéger l'écran pour pas te faire mal aux yeux ça .
qu'est ce que tu fais maintenant ?
hum j'aime bien moi .
oh ça a tout effacé .
mais ouais mais y a plus rien dessous .
on peut plus l'enlever maintenant ?
ah oui mais maintenant on voit plus ce qu'y a dessous alors .
oh !
ah c'est ça les œufs au plat .
mais on peut pas les manger ceux là ils sont bizarres .
ben ça y est tout est caché maintenant dans la prison .
ah !
qu'est ce tu vas mettre dans le trou noir ?
ah !
