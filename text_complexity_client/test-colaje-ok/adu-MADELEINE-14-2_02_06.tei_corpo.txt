tu proposes à Martine un café ?
oh oui volontiers !
merci .
xxx café .
oh oui s'il te plaît .
qu'est ce que tu portes à la main ?
des fleurs ?
oui .
elles tournent avec le vent .
souffle !
elles sont un peu vieilles hein .
elles tournent plus .
ça date de Marie donc euh elles sont plus très jeunes !
tu es en train de dire que Marie est vieille ?
han !
si tu l'avais vue hier ...
oui je veux bien .
pour le dernier jour d'école elle doit se déguiser en barbie .
ouh là !
et elle avait mis un mini short rose ...
des chaussures à talon qu'elle m'avait piquées !
ouah@o !
c'était impressionnant ?
c'était très impressionnant .
oui Madeleine je veux bien .
finalement tu vois la jeune fille apparaître .
qu'est ce que tu fais Madeleine ?
et ouais .
tu voles sur ton balai ?
c'est un cheval ?
c'est quoi ?
c'est du vent ?
c'est des petits moulins ?
bah oui tu peux souffler .
elles sont un peu vieilles oui .
oui .
ah c'est gentil .
comment ?
oui .
oui .
une soucoupe .
oui elles sont un peu abîmées tordues .
ah celle là elle marche bien .
merci beaucoup .
tu devrais souffler dessus .
oui .
et les autres elles tournent ?
pas mal .
et si tu souffles dessus ?
bien !
j'ai pas mis de sucre pas grave ?
c'est pas grave .
ah xx .
qu'est ce que c'est ce bruit ?
oui elle est au téléphone .
tu veux jouer ?
à quoi tu veux jouer Madeleine ?
ce que je t'ai donné ?
un jeu un jeu que je t'ai donné ?
elle t'a appris ?
hein quelque chose que /.
viens on va aller le chercher !
et il est où ce livre ?
dans ta chambre .
ah tu vas /.
d'accord attends .
ouais .
on y va ?
je te suis .
oui je viens je te suis .
là haut ?
on y va ?
oui tu fais attention de pas tomber hein .
comme Marie Ange ?
Marie Ange elle monte comme ça à quatre pattes ?
où où tu vois une grosse mouche ?
où là ?
ah oui y a une mouche .
elle est pas si grosse que ça hein .
c'est juste une mouche .
alors où est ce livre ?
comment il s'appelle ?
c'est quoi le titre ?
c'est quoi le titre comment s'appelle ce livre ?
c'est lequel il est dans cette rangée là ?
sur quelle étagère il est ?
tu sais pas !
bah comment je vais le trouver moi alors ?
oh tu as de mosaïquedes champignons ?
c'est bien ce jeu .
il est là haut .
tu veux jouer avec ?
tu sais faire des mosaïques ?
attends .
en bas ?
on va on redescend ?
ou si tu veux on on joue dans le salon .
ou ou ici comme tu veux .
dans le salon .
alors attention en descendant .
houp là !
qu'est ce que tu veux faire ?
tu veux jouer au au mosaïque ?
on va le faire ici c'est bien non .
xx .
avec Martine .
ouh@i tout est mélangé .
mais n'entre pas en transe pour autant .
elle est là tout est mélangé .
attends on va remettre dans les petits plots là dansles petits bacs .
hein .
attends je vais me mettre de l'autre côté .
ça va mieux .
houp@o !
ils roulent ils roulent .
attends reste debout là .
reste debout là Madeleine .
tu veux faire quel dessin ?
je sais pas .
tu crois non je crois pas .
moi j'aime beaucoup la cigogne .
ah faire ça !
c'est dur hein !
ouh@i ça c'est difficile .
tu vois nous on en était plutôt à à mettre les bonnescouleurs sur ceux là .
tu veux faire la cigogne ?
ou le ou le dinosaure ?
tu veux faire le dinosaure le chien ..?
ça c'est quoi ?
ça c'est quoi ?
Madeleine ?
il a une drôle de couleur il est de quelle couleur ?
tu crois que c'est jaune un cochon ?
non c'est rose normalement .
ça c'est un toucan .
dinosaure .
oui .
ça existe plus ça n'existe plus .
il est de quelle couleur ?
non !
là le champignon là !
ça c'est de quelle couleur ?
non !
bah oui !
sur du rouge .
c'est du rouge ça ?
bah oui !
excusez moi .
c'est de quelle couleur ça ?
ah .
alors ça se met dans le ..?
bleu très bien vas y .
tu mets tous les bleus ?
oui .
ah faut viser hein .
tu veux que je t'aide ?
non ?
bravo .
ah regarde où est ce qu'y a du blanc ?
très bien !
le ciel est blanc en fait .
le ciel est blanc .
ah non !
ça c'est quelle /.
bah oui .
alors que ton petit champignon il est ..?
tout rouge !
et le dinosaure est de quelle couleur ?
ah bah alors tout va bien !
oui !
c'est de quelle couleur ?
oui .
oui !
bah oui .
mm .
oui .
oui !
non .
non !
tout bariolé !
ça c'est un livre .
où il est ton livre sur les poissons ?
où est passé ton livre sur les poissons Madeleine ?
mm .
il doit être à côté .
tout bariolé bouge pas bouge pas .
elle va revenir .
elle l'a trouvé ?
ouais !
ouh@i il a l'air beau ce livre !
qui est ce qui t'a donné ce livre ?
oui !
alors tu montres le poisson tout bariolé à Martine ?
oh oui !
y a les poissons ...
il est beau !
il est effectivement tout bariolé .
les poissons ...
non ceux là ?
tous ...
et des tous ..?
bariolés !
ils sont de toutes les couleurs !
et puis il y a des poissons qui adorent les ...
les câlins .
et d'autres qui aiment la ...
la bagarre !
la bagarre !
et y a des poissons vraiment ...
élégants .
et des tous ...
ébouriffés !
oh regarde !
eh oui les poissons sont tous différents .
et heureux d'être ensemble ils font la fête dans l'océan .
voilà .
l'origine de bariolé .
oui c'est de quelle couleur ça ?
c'est une feuille ...
non .
non .
non ça c'est quelle couleur ?
je sais pas bah si tu sais .
c'est une verte ...
mais oui c'est une feuille verte .
ah là tu te trompes .
oui .
tu te trompes ?
oh bah non là tu te trompes pas c'est très bien .
mm .
oui .
je les ai rangés là .
ah et tu voulais montrer Martine à Martine le livre deMadeleine ?
hein ?
le livre de Madeleine ?
est ce que tu connais le livre de Madeleine ?
non .
ah ha !
tu veux qu'on aille le chercher ?
c'est l'histoire de qui ?
c'est ton histoire ?
est ce que c'est vraiment ton histoire ?
tiens on va aller là bas en fait on est bien sur ce lit .
ce livre xxx fait encore .
après on finira le jeu .
c'est l'histoire de ...
de Madeleine !
alors !
bah non !
donc c'est on est pas sûr que ce soit exactement tonhistoire à toi .
c'est peut être une autre Madeleine .
hein ?
quand même hein .
parce qu'elle te ressemble .
à Paris dans une vieille maison aux murs recouverts de vigne ...
qui ressemble à notre maison .
vivaient ...
douze petites filles .
regarde un deux trois quatre cinq six sept huit neuf ...
dix onze ...
douze !
sur deux rangs elles déjeunaient .
oh c'est une grande table hein .
voilà .
elles se brossaient les dents .
dans les douze lavabos et puis se couchaient .
et les douze lits douze petites filles dans douze lits .
elles souriaient quand il convenait ...
et se renfrognaient s'il le fallait .
elles font toutes la grimace oui !
parce qu'elles voient passer un voleur .
qui a volé qui a volé un sac .
et à l'occasion elles s'attristaient .
à neuf heures et demi elles partaient se promener parfaitement enrang .
tu vois ?
non on sait pas où est Madeleine là .
sous la pluie ou par beau temps .
qu'est ce qu'elles tiennent là ?
quoi ?
par beau temps tu vois y a le ciel bleu ...
le soleil les oiseaux .
il fait beau .
c'était ...
c'était Madeleine la plus petite .
attends pas encore .
c'était Madeleine la plus petite .
elle n'avait pas peur des souris .
alors que toutes les autres avaient peur .
elle aimait la glace la neige l'hiver gris .
oh on voit Montmartre là .
au zoo le tigre rugissait !
mais Madeleine s'en moquait .
et nul ne savait comme elle faire peur à Miss Clavel .
han elle fait un peu des bêtises .
au beau milieu d'une /.
bah elle marche sur le parapet du pont ...
c'est carrément une grosse bêtise .
au beau milieu d'une nuit /.
ouais .
Miss Clavel /.
faut descendre tout de suite enfin !
au beau milieu d'une nuit Miss Clavel allume et dit mais j'entends quelqu'un qui crie .
Madeleine dans son //.
attends oui .
Madeleine dans son lit les yeux rouges pleure et gémit .
oui appelé le docteur Don se rue sur le téléphone .
il fait le numéro ...
c'est pour une appendicite .
han !
une maladie embêtante .
et tout le monde de s'inquiéter !
et tout le monde de sangloter !
tu as vu tout le monde pleure !
même Miss Clavel .
le médecin dans ses bras porte Madeleine en bas .
qui ?
bah oui elle a mal au ventre .
et l'ambulance l'emporte pour aller où ?
avec le avec le docteur .
dans la nuit son feu clignote .
en s'éveillant vers sept heures Madeleine voit des fleurs .
bientôt elle a le droit de boire et de manger dans son lit àmanivelle qu'on peut redresser .
c'est une barrette .
une fissure au plafond fait un dessin qui parfois ressemble bienà ...
à un lapin .
avec les arbres les oiseaux et le ciel clair deux semainespassèrent comme un éclair .
un beau matin Miss Clavel dit quelle belle journéeaujourd'hui !
allons rendre visite à Madeleine .
bien vite .
visites de deux à quatre précise la pancarte .
oh qu'est ce qu'elles ont dans la main ?
pourquoi elles ont des fleurs .
elles vont à l'hôpital et elles vont les donner à qui ces fleurs ?
pas à Marie .
à Madeleine !
toi aussi tu mélanges Marie et Madeleine .
une fleur à la main et fort intimidées elles entrent dans lachambre sur la pointe des pieds .
han elles s'esclaffent toutes ravies de voir cadeaux et gâteriesque son père lui a remis .
qu'est ce qu'il a fait le papa de Madeleine ?
il lui a donné plein de cadeaux .
de cadeaux et de gâteries .
des choses pour lui faire plaisir .
pour elle .
parce qu'elle avait mal au ventre .
fallait la consoler .
mais leur plus grande surprise c'est de voir sur son ventre ...
de voir quoi sur son ventre Madeleine ?
qu'est ce qu'elle leur montre ?
une cicatrice .
hein ?
toi tu as une cicatrice ?
c'est pas faux que le nombril soit une cicatrice .
hein ?
oui on peut dire ça .
c'est vrai .
au revoir disent elles nous ...
nous reviendrons !
et sous la pluie elles rentrent à la maison .
après avoir dîné s'être brossé les dents elles vont se ...
coucher !
oh y a un lit ...
et le lit de Madeleine il est encore vide parce qu'elle est restée à l'hôpital .
au beau milieu de la nuit Miss Clavel allume et lit .
c'est Miss Clavel .
craignant un nouveau désastre Miss Clavel vite .
et dit voyons mes enfants qu'est ce qui vous trouble tant ?
toutes les petites filles crient on veut être opérées aussi !
ben oui !
elles voudraient aller à l'hôpital .
alors elles pleurent toutes .
de Madeleine .
pourquoi elles veulent aller toutes à l'hôpital ?
pourquoi ?
oui .
qui ?
ah elles veulent aller avec Madeleine alors c'est pour ça ?
ou c'est pour avoir des cadeaux aussi ?
et qu'est ce qu'elle .
bonne nuit petites filles maintenant ...
soyez gentilles Miss Clavel éteint la lumière elle refermela porte retourne dans son .
qu'est ce que tu fais ?
tu te mets comme moi ?
ah oui .
on va faire une chorégraphie toutes les deux une danse .
oh tu nous fais un spectacle ?
oh ça c'est formidable !
oh !
ça c'est du spectacle hein !
tu pourrais chanter à Martine poule en haut poule en bas .
tu le connais ?
vas y .
tu veux que je chante avec toi ?
poule en haut poule en bas .
oeuf à la coque tu le fais tu sais tufais comme ça et puis tu tournes .
non non tu le fais bien toi regarde .
oeuf à la coque oeuf sur le plat .
oeuf en omelette .
oeuf mimosa .
oeuf mollet oeuf dur .
ah oeuf poché pas pas mollet pardon oeuf poché oeufpoché ...
ça ça vient des Jeannettes hein !
ça me rappelle aussi des choses !
oui alors avec Marie c'est la rage !
ah .
ah tu y arrives mieux ici ?
xxx .
à côté du fameux canapé vert .
bien connu .
poule en haut .
poule en haut poule en bas oeuf à la coque oeuf sur le plat .
oeuf plat il faut mettre une jambe comme ça puis une autre comme ça .
ah oui ?
ah !
ah oui ?
poule en haut haut poule en bas ah là c'estoeuf à la coque .
oeuf sur le plat .
oeuf en omelette .
oeuf mimosa ...
oeuf poché !
oeuf dur !
oui bravo !
ah tu en apprends des choses hein !
hein tu en apprends des choses !
non tiens tu sais ce que tu devrais faire c'est prendre leslego peut être non ?
dans le grand sac dans ta chambre .
ah non pascelui là .
ça ce sont les habits de poupée .
tu sais dans celui là .
regarde là .
non c'est pas ça mais ...
tu sais quand on a fait quand on a fait la maison .
tu te souviens ?
regarde qu'est ce que c'est ça ?
ah oui y en a des choses là dedans .
qu'est ce que c'est ce que tu as dans la main là c'est quoiça ?
c'est c'est quoi ?
ah oui .
c'est la porte ?
là j'ai le jardin .
tu arrives à le mettre ?
bien .
ah tiens la barrette que je cherchais .
tu veux l'escalier ?
bah ce que tu veux .
un coussin ?
mm .
est ce que tu veux l'escalier ?
tu arrives pas à mettre le coussin ?
tiens .
mh ou pour descendre .
oui .
attends .
on le met là ?
tu es assise sur un cheval .
ah .
alors xxx .
ouh là là .
tiens on peut mettre ça .
le cheval tu crois qu'il a monté l'escalier .
je crois que c'est un peu difficile pour les chevaux .
oui en fait notre escalier il a un problème .
il aboutit nulle part .
c'est comme un piège xx .
tiens là tu as un petit monsieur houp@o !
regarde tu as un petit monsieur .
il habite où ce petit monsieur ?
tu crois ?
mais regarde je crois c'est plutôt celui là .
tu vois il se met là .
c'est plutôt l'autre maison je crois .
tiens y a aussi c'est quoi ça ?
oui .
si tu veux .
ah faut mettre droit parce que sinon ça tient pas .
hum .
très bien bravo .
tiens là y a ...
non !
pourquoi normalement y a des portes sur les placards peut être .
houp là pardon .
je crois pas que ce soit cassé .
ah si ça c'est le fond tiens donne moi ton morceau là .
peut être qu'on peut mettre la porte du fond .
ah oui .
ça va être mieux comme ça .
comme ça ?
ouh là il est sur le toit !
han il va se casser la figure ton cheval !
oh c'est un cheval acrobate ça !
et ça qu'est ce que c'est ?
un aspirateur ?
le bus qui passe ?
ouh@i !
oui !
et oui .
oh c'est comme un toboggan pour chevaux !
et est ce que les vaches savent faire la même chose ?
ah oui .
elle rentre pas dans le bus .
tiens tu veux la petite vache regarde ça c'est un veau je crois .
hop@o !
oh !
attention !
ah oui .
le bus fait peur au cheval ?
où est ce que tu dis ça Madeleine ?
oui !
et ça qu'est ce que c'est ?
un quoi ?
petit veau non c'est pas un veau .
c'est un mouton ...
un mouton .
il est tombé !
et ça c'est quoi ?
hein tu le mets où ça ?
Madeleine ?
je te le mets là .
ouh@i regarde .
qu'est ce que c'est ça ?
ah lui aussi ?
qu'est ce que c'est ce que j'ai dans la main Madeleine ?
et il habite où le coq ?
je le mets là ?
ouais .
je sais pas ce que ça veut dire .
c'est Antoinette qui dit ça ?
oui ?
quand vous allez vous promenez .
ah oui ça doit être sur le trajet du toboggan .
avec qui tu vas faire du toboggan ?
avec Antoinette et Marie Ange .
tu aimes bien faire du toboggan ?
tu veux un autre animal ?
ou un personnage ?
oh qu'est ce que c'est que ça ?
il est de quelle couleur ?
ouah@o !
tu crois qu'il est jaune vraiment ?
oui plutôt hein .
ça se met dans le salon .
bah où tu veux .
mm .
xxx ça tourne ça .
tiens .
ah bah oui il garde le mouton c'est normal .
oh sur le mouton !
je suis pas sûre que ça plaise tellement au mouton .
par exemple oui .
oh et ça c'est quoi ?
une cafetière ?
et ça ?
une casserole .
oh là j'ai une table .
tu veux une table ?
houp là .
pardon .
comme tu veux !
il peut y avoir une table en haut ...
il peut y avoir une table en bas ...
il peut y avoir une table dans le jardin .
et est ce que tu veux un fauteuil ?
voilà très bien .
veux tu un fauteuil ?
vraiment ?
hop là .
attends attends .
la table est tombée .
le fauteuil là .
voilà .
la cafetière ?
sur la table très bien .
est ce que tu veux une fleur pour ton jardin ?
sur la maman quoi ?
tu lui mets une fleur sur le sur le dos ?
je sais pas si ça tient en plus .
non .
peut être oui .
ouh@i qui c'est ça ?
c'est ?
Nina ?
je savais pas qu'elle s'appelait Nina .
est ce que tu veux un lit pour Nina ?
hein ?
oui elle a une couette .
pourquoi elle a envie d'aller dans son lit ?
ah !
bah je sais pas ce qu'elle a comme doudou .
ça ça peut lui faire un doudou ?
c'est un peu grand .
ah non regarde j'ai trouvé un doudou !
c'est un doudou pour elle ça !
bah !
c'est le doudou qui fait pipi ?
ton doudou il fait pipi ?
ah non !
oh quelle drôle d'idée !
qui est ce qui fait pipi ?
oh mais pourquoi il va dans les toilettes ce doudou ?
tu veux que je le mette dans la couette ?
c'est difficile hein .
voilà .
et la petite fille ?
elle a sommeil ?
non ?
un coup d'aspirateur ?
oh bah merci tu me fais mon ménage !
c'est une bonne idée .
oh là là dans ma chambre !
c'est terrible en ce moment on a eu une invasion de fourmis etde petites mouches hier soir .
effectivement ça a besoin d'un coup d'aspirateur .
des fourmis volantes ?
oui .
oui oui oui .
c'est affreux vous en avez aussi ?
mais en fait c'est pas si affreux que ça .
parce que ce sont des elles sortent toutes tout d'un coupelles éclosent et elles s'envolent .
et elles partent .
elles partent ?
oui .
elles restent pas là .
c'est impressionnant c'est impressionnant quand ellessortent toutes ensemble .
xxx ah je déteste ça moi .
y a des saletés bah faut faire le ménage .
non le défais pas ça va lecasser .
ah très bien tu fais le ménage dans la maison .
oui .
c'est ton /.
ton quoi ?
ça sent mauvais ?
pourquoi ?
oh !
mais c'est l' c'est l'ours là qui est allé aux toilettes .
il avait pas tiré la chasse d'eau !
et là bah oui .
tant qu'on y est hein .
elle est propre cette petite fille ?
faut tirer la chasse d'eau Madeleine .
faut appuyer sur le bouton .
là .
pshit@o !
voilà !
et toi tu es propre ?
m tu veux faire pipi ?
et pourquoi est ce qu'il faut être propre Madeleine ?
hein ?
Madeleine ?
Madeleine ?
est ce qu'il faut être propre ?
pour faire quoi ?
est ce que tu as dit à Martine que tu étais allée à l'école ?
et qu'est ce que tu as fait à l'école ?
hein tu as fait quoi à l'école ?
tu as vu qui ?
ouh là là tout se casse la figure .
bah oui tu as raison ça se met pas sur le toit .
oh ça fait très joli ce que tu fais là .
sur la sur la barrière !
une très bonne idée .
attends tu veux que je t'aide ?
ah bah je sais pas .
c'est vrai que c'est joli ta maison là .
mais oui regarde là .
moi aussi j'aimerais avoir une vache dans mon jardin .
on irait chercher le lait !
tu as pas de cheval sur ton toit !
oui .
ah non .
tu sais la réparer ?
ouh !
elle est fragile hein .
oui .
oh !
comme on veut .
faut appuyer plus .
bien !
là c'est bien .
ah c'était presque hein .
oh il transporte sa maison sur son dos ce cheval ?
c'est un cheval tortue .
oh dans le canapé tu vas avoir du mal à faire tenir la maison .
moi je ferais pas //.
attends regarde .
ce que tu peux faire c'est mettre les toilettes dedans .
comme ça .
hop@o .
tu veux faire pipi en vrai ?
hein ?
mais où ?
non !
non tu peux pas .
tu es trop grande .
quel bébé ?
il est où ?
il est où ton bébé ?
c'est pas du repassage !
c'est du ménage .
tu nettoies ton tabouret ?
oh là là ça va être propre hein .
est ce que tu veux montrer ton gros livre à Martine ?
tu sais le gros livre que papa t'a acheté ?
il est où ?
hein Madeleine elle le connaît pas Martine .
ouf@o il est gros hein .
oh là là là là !
tu le montres ?
c'est un dictionnaire ?
on va le montrer sur le canapé peut être Madeleine ?
regarde si tu le poses là .
comme ça tu peux le montrer .
tu peux dire quelles sont les histoires .
ouh@i !
qu'est ce qu'il y a comme histoires dedans ?
ça c'est Blanche Neige tu connais //.
ah ça tu connais .
qu'est ce qu'il lui arrive à Aladin ?
magnifique .
il pleurait ?
comment s'appelle la femme d'Aladin ?
tu te rappelles ?
c'est qui ça ?
non c'est le méchant magicien .
ça c'est Aladin .
je t'avais lu aussi le //.
qu'est ce que c'était c'était quoi comme histoire ça ?
attends .
ça c'est Blanche Neige on l'a pas encore lue .
ah ça !
c'est qui ça ?
non !
là ?
c'est le Petit Chaperon Rouge ...
oui !
ah ouais on le lira ce soir si tu veux .
et l'histoire de quoi ?
non non !
tu sais elle qu'est ce qu'il lui arrive elle joue avec sapetite balle en ...
non !
elle est en or sa balle Madeleine !
comment on actualise les contes .
han c'est qui ça ?
qui est ce ?
c'est le ...
han qu'est ce qu'il veut faire le loup ?
la grand mère .
oh et là ?
qu'est ce qu'elle veut la grenouille ?
elle va chercher la balle de la grenouille princesse ?
mais qu'est ce qu'elle veut aussi ?
regarde elle est là .
elle veut manger dans la même assiette que la princesse !
et boire dans le même gobelet .
le gobelet ..?
est ce qu'on voit le gobelet ?
ça doit être ça le gobelet .
attends là regarde .
et après la grenouille elle va où ?
dans le lit de la princesse ?
oh oui on va fermer les rideaux hein pour pas voir cette grenouilledans le lit de la princesse !
et elle est contente elle ?
pourquoi ?
tu as quelque chose à faire ?
et ça je l'ai pas encore lue cette histoire là .
ça a l'air d'être un petit monsieur .
dans la forêt .
ah oui ?
ah oui ?
ah oui ?
y a pas tellement de forêt là .
hein ?
tu entends du bruit ?
quel bruit ?
c'est les oiseaux ?
ah oui ?
c'est quel monsieur ?
yy ?
ça a l'air d'être un génie encore ça .
qu'est ce que c'est que ça ?
le cheval de Troie !
ouh@i !
peut être plus tard .
hein parce que c'est une très longue histoire j'ai l'impression .
le cheval de Troie .
tu es pas en fin de cassette ?
si .
si hein .
le /.
