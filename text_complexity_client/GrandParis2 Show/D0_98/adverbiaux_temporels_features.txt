====== Sentence Level Features ======

1. Sentence: Un exposé plus détaillé de l ' évolution passée , de 1850 à 1962 , figure dans l ' avant-projet de programme duodécennal pour la région de Paris publié en 1963 (pages 46 à 62) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1850 à 1962', 'en 1963']
adverbiaux_localisation_temporelle::1::['en 1963']
adverbiaux_duratifs_iteratifs::1::['de 1850 à 1962']
adverbiaux_pointage_absolu::2::['de 1850 à 1962', 'en 1963']
adverbiaux_pointage_non_absolu::1::['en 1963']
adverbiaux_loc_temp_focalisation_id::2::['de 1850 à 1962', 'en 1963']
adverbiaux_loc_temp_regionalisation_id::2::['de 1850 à 1962', 'en 1963']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1963']
adverbiaux_dur_iter_absolu::2::['de 1850 à 1962', 'en 1963']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1850 à 1962', 'en 1963']
adverbiaux_localisation_temporelle::1::['en 1963']
adverbiaux_duratifs_iteratifs::1::['de 1850 à 1962']
adverbiaux_pointage_absolu::2::['de 1850 à 1962', 'en 1963']
adverbiaux_pointage_non_absolu::1::['en 1963']
adverbiaux_loc_temp_focalisation_id::2::['de 1850 à 1962', 'en 1963']
adverbiaux_loc_temp_regionalisation_id::2::['de 1850 à 1962', 'en 1963']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1963']
adverbiaux_dur_iter_absolu::2::['de 1850 à 1962', 'en 1963']
Total feature counts: 14

