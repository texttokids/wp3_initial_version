====== Sentence Level Features ======

1. Sentence: Malgré l ' accélération du progrès , la mise au point d ' un véhicule dont la sécurité doit être rigoureusement garantie , et dont la fabrication exigerait de lourds investissements , serait nécessairement longue .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ malgré, sécurité, devoir, exiger, lourd, nécessairement, long ]
marqueurs_niveau3 :: 4 :: [ devoir, exiger, lourd, nécessairement ]
alethique :: 3 :: [ devoir, exiger, nécessairement ]
epistemique :: 3 :: [ devoir, lourd, long ]
alethique_niveau3 :: 3 :: [ devoir, exiger, nécessairement ]
deontique :: 2 :: [ devoir, exiger ]
pas_d_indication :: 2 :: [ malgré, sécurité ]
marqueurs_niveau0 :: 2 :: [ malgré, sécurité ]
deontique_niveau3 :: 2 :: [ devoir, exiger ]
epistemique_niveau3 :: 2 :: [ devoir, lourd ]
pas_d_indication_niveau0 :: 2 :: [ malgré, sécurité ]
boulique :: 1 :: [ exiger ]
marqueurs_niveau2 :: 1 :: [ long ]
boulique_niveau3 :: 1 :: [ exiger ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 36


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ malgré, sécurité, devoir, exiger, lourd, nécessairement, long ]
marqueurs_niveau3 :: 4 :: [ devoir, exiger, lourd, nécessairement ]
alethique :: 3 :: [ devoir, exiger, nécessairement ]
epistemique :: 3 :: [ devoir, lourd, long ]
alethique_niveau3 :: 3 :: [ devoir, exiger, nécessairement ]
deontique :: 2 :: [ devoir, exiger ]
pas_d_indication :: 2 :: [ malgré, sécurité ]
marqueurs_niveau0 :: 2 :: [ malgré, sécurité ]
deontique_niveau3 :: 2 :: [ devoir, exiger ]
epistemique_niveau3 :: 2 :: [ devoir, lourd ]
pas_d_indication_niveau0 :: 2 :: [ malgré, sécurité ]
boulique :: 1 :: [ exiger ]
marqueurs_niveau2 :: 1 :: [ long ]
boulique_niveau3 :: 1 :: [ exiger ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 36

