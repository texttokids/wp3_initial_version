
********** Entity Names Features **********
*******************************************

1. Sentence: La réalisation sur cet axe d ' une grande structure d ' aménagement rassemblant activités et habitat en un ensemble urbain organisé à une grande échelle , permettra de prendre en compte et d ' orienter de manière plus efficace le développement actuel extrêmement dynamique mais également désordonné du Sud-Ouest de l ' agglomération parisienne .
Entity Name (Type) :: Sud-Ouest (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La réalisation sur cet axe d ' une grande structure d ' aménagement rassemblant activités et habitat en un ensemble urbain organisé à une grande échelle , permettra de prendre en compte et d ' orienter de manière plus efficace le développement actuel extrêmement dynamique mais également désordonné du Sud-Ouest de l ' agglomération parisienne .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La réalisation sur cet axe d ' une grande structure d ' aménagement rassemblant activités et habitat en un ensemble urbain organisé à une grande échelle , permettra de prendre en compte et d ' orienter de manière plus efficace le développement actuel extrêmement dynamique mais également désordonné du Sud-Ouest de l ' agglomération parisienne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ grand, grand, permettre ]
marqueurs_niveau3 :: 3 :: [ grand, grand, permettre ]
epistemique :: 2 :: [ grand, grand ]
epistemique_niveau3 :: 2 :: [ grand, grand ]
deontique :: 1 :: [ permettre ]
deontique_niveau3 :: 1 :: [ permettre ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La réalisation sur cet axe d ' une grande structure d ' aménagement rassemblant activités et habitat en un ensemble urbain organisé à une grande échelle , permettra de prendre en compte et d ' orienter de manière plus efficace le développement actuel extrêmement dynamique mais également désordonné du Sud-Ouest de l ' agglomération parisienne .
No features found.

