
********** Entity Names Features **********
*******************************************

1. Sentence: La limitation de la surface agglomérée de chacune des communes et la limitation du volume des constructions permettra de remédier à l ’ éparpillement des éléments formant la région parisienne et , en même temps , d ' en assurer le développement méthodique , car bien que la dénatalité de la capitale fasse prévoir une limite à la population , il faut tenir compte de l ’ attrait sur la Province qui , jusqu ’ ici , a largement contrebalancé cette dénatalité .
Entity Name (Type) :: Province (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La limitation de la surface agglomérée de chacune des communes et la limitation du volume des constructions permettra de remédier à l ’ éparpillement des éléments formant la région parisienne et , en même temps , d ' en assurer le développement méthodique , car bien que la dénatalité de la capitale fasse prévoir une limite à la population , il faut tenir compte de l ’ attrait sur la Province qui , jusqu ’ ici , a largement contrebalancé cette dénatalité .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La limitation de la surface agglomérée de chacune des communes et la limitation du volume des constructions permettra de remédier à l ’ éparpillement des éléments formant la région parisienne et , en même temps , d ' en assurer le développement méthodique , car bien que la dénatalité de la capitale fasse prévoir une limite à la population , il faut tenir compte de l ’ attrait sur la Province qui , jusqu ’ ici , a largement contrebalancé cette dénatalité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ permettre, bien, falloir ]
marqueurs_niveau3 :: 3 :: [ permettre, bien, falloir ]
deontique :: 2 :: [ permettre, falloir ]
deontique_niveau3 :: 2 :: [ permettre, falloir ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La limitation de la surface agglomérée de chacune des communes et la limitation du volume des constructions permettra de remédier à l ’ éparpillement des éléments formant la région parisienne et , en même temps , d ' en assurer le développement méthodique , car bien que la dénatalité de la capitale fasse prévoir une limite à la population , il faut tenir compte de l ’ attrait sur la Province qui , jusqu ’ ici , a largement contrebalancé cette dénatalité .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['jusqu ’ ici ,', 'jusqu ’ ici']
adverbiaux_localisation_temporelle::1::['jusqu ’ ici ,']
adverbiaux_duratifs_iteratifs::1::['jusqu ’ ici']
adverbiaux_pointage_non_absolu::2::['jusqu ’ ici ,', 'jusqu ’ ici']
adverbiaux_loc_temp_focalisation_id::1::['jusqu ’ ici ,']
adverbiaux_loc_temp_regionalisation_non_id::1::['jusqu ’ ici ,']
adverbiaux_loc_temp_pointage_non_absolu::1::['jusqu ’ ici ,']
adverbiaux_dur_iter_deictique::1::['jusqu ’ ici']

