
********** Entity Names Features **********
*******************************************

1. Sentence: Certains aménagements au fil de l ' eau sont possibles , en utilisant en particulier d ' anciennes gravières , mais ne permettent trop souvent de satisfaire qu ' aux besoins des sports nautiques , sans que puisse s ' y ajouter sur la rive une gamme aussi étendue d ' autres activités de loisirs qu ' il serait souhaitable .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Certains aménagements au fil de l ' eau sont possibles , en utilisant en particulier d ' anciennes gravières , mais ne permettent trop souvent de satisfaire qu ' aux besoins des sports nautiques , sans que puisse s ' y ajouter sur la rive une gamme aussi étendue d ' autres activités de loisirs qu ' il serait souhaitable .
Tokens having emotion: [ satisfaire ]
Lemmas having emotion: [ satisfaire ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Certains aménagements au fil de l ' eau sont possibles , en utilisant en particulier d ' anciennes gravières , mais ne permettent trop souvent de satisfaire qu ' aux besoins des sports nautiques , sans que puisse s ' y ajouter sur la rive une gamme aussi étendue d ' autres activités de loisirs qu ' il serait souhaitable .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ certain, possible, permettre, souvent, satisfaire, besoin, pouvoir, loisir, souhaitable ]
marqueurs_niveau3 :: 7 :: [ certain, possible, permettre, souvent, besoin, pouvoir, souhaitable ]
alethique :: 4 :: [ possible, souvent, besoin, pouvoir ]
alethique_niveau3 :: 4 :: [ possible, souvent, besoin, pouvoir ]
appreciatif :: 2 :: [ satisfaire, loisir ]
boulique :: 2 :: [ besoin, souhaitable ]
deontique :: 2 :: [ permettre, pouvoir ]
epistemique :: 2 :: [ certain, pouvoir ]
marqueurs_niveau2 :: 2 :: [ satisfaire, loisir ]
appreciatif_niveau2 :: 2 :: [ satisfaire, loisir ]
boulique_niveau3 :: 2 :: [ besoin, souhaitable ]
deontique_niveau3 :: 2 :: [ permettre, pouvoir ]
epistemique_niveau3 :: 2 :: [ certain, pouvoir ]
Total feature counts: 42


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Certains aménagements au fil de l ' eau sont possibles , en utilisant en particulier d ' anciennes gravières , mais ne permettent trop souvent de satisfaire qu ' aux besoins des sports nautiques , sans que puisse s ' y ajouter sur la rive une gamme aussi étendue d ' autres activités de loisirs qu ' il serait souhaitable .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['trop souvent']
adverbiaux_purement_iteratifs::1::['trop souvent']
adverbiaux_iterateur_frequentiel::1::['trop souvent']

