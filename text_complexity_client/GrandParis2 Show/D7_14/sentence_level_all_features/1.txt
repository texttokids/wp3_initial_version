
********** Entity Names Features **********
*******************************************

1. Sentence: L ' organisation de cette consultation internationale de R&D sur « Le grand pari de l ' agglomération parisienne » s ' inscrit dans le cadre d ' une politique scientifique existante , au sein du Programme interdisciplinaire de recherche sur “ L ' architecture de la grande échelle " (AGE) conçu et mis en œuvre depuis 2006 par le ministère de la Culture et de la Communication (MCC) en partenariat avec le ministère de l ' Ecologie , de l ' Energie , du Développement durable et de l ' Aménagement du territoire (MEEDDAT) .
Entity Name (Type) :: MEEDDAT (ORG)


********** Emotions Features **********
***************************************

1. Sentence: L ' organisation de cette consultation internationale de R&D sur « Le grand pari de l ' agglomération parisienne » s ' inscrit dans le cadre d ' une politique scientifique existante , au sein du Programme interdisciplinaire de recherche sur “ L ' architecture de la grande échelle " (AGE) conçu et mis en œuvre depuis 2006 par le ministère de la Culture et de la Communication (MCC) en partenariat avec le ministère de l ' Ecologie , de l ' Energie , du Développement durable et de l ' Aménagement du territoire (MEEDDAT) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' organisation de cette consultation internationale de R&D sur « Le grand pari de l ' agglomération parisienne » s ' inscrit dans le cadre d ' une politique scientifique existante , au sein du Programme interdisciplinaire de recherche sur “ L ' architecture de la grande échelle " (AGE) conçu et mis en œuvre depuis 2006 par le ministère de la Culture et de la Communication (MCC) en partenariat avec le ministère de l ' Ecologie , de l ' Energie , du Développement durable et de l ' Aménagement du territoire (MEEDDAT) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ grand, grand ]
epistemique :: 2 :: [ grand, grand ]
marqueurs_niveau3 :: 2 :: [ grand, grand ]
epistemique_niveau3 :: 2 :: [ grand, grand ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' organisation de cette consultation internationale de R&D sur « Le grand pari de l ' agglomération parisienne » s ' inscrit dans le cadre d ' une politique scientifique existante , au sein du Programme interdisciplinaire de recherche sur “ L ' architecture de la grande échelle " (AGE) conçu et mis en œuvre depuis 2006 par le ministère de la Culture et de la Communication (MCC) en partenariat avec le ministère de l ' Ecologie , de l ' Energie , du Développement durable et de l ' Aménagement du territoire (MEEDDAT) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['depuis 2006']
adverbiaux_duratifs_iteratifs::1::['depuis 2006']
adverbiaux_pointage_absolu::1::['depuis 2006']
adverbiaux_loc_temp_focalisation_id::1::['depuis 2006']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis 2006']
adverbiaux_dur_iter_absolu::1::['depuis 2006']

