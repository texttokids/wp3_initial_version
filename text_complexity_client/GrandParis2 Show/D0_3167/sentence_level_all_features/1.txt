
********** Entity Names Features **********
*******************************************

1. Sentence: Entre un centre unique régional où sont aujourd ' hui rassemblées la plupart des activités spécialisées , et une multitude d ' équipements locaux sans grand rayonnement , l ' organisation envisagée pour la région parisienne fait place à des zones d ' activités d ' importance et de complexité croissantes .
Entity Name (Type) :: Entre (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Entre un centre unique régional où sont aujourd ' hui rassemblées la plupart des activités spécialisées , et une multitude d ' équipements locaux sans grand rayonnement , l ' organisation envisagée pour la région parisienne fait place à des zones d ' activités d ' importance et de complexité croissantes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Entre un centre unique régional où sont aujourd ' hui rassemblées la plupart des activités spécialisées , et une multitude d ' équipements locaux sans grand rayonnement , l ' organisation envisagée pour la région parisienne fait place à des zones d ' activités d ' importance et de complexité croissantes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ unique, grand, croissant ]
appreciatif :: 2 :: [ unique, croissant ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ croissant ]
marqueurs_niveau1 :: 1 :: [ unique ]
appreciatif_niveau2 :: 1 :: [ croissant ]
appreciatif_niveau1 :: 1 :: [ unique ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Entre un centre unique régional où sont aujourd ' hui rassemblées la plupart des activités spécialisées , et une multitude d ' équipements locaux sans grand rayonnement , l ' organisation envisagée pour la région parisienne fait place à des zones d ' activités d ' importance et de complexité croissantes .
No features found.

