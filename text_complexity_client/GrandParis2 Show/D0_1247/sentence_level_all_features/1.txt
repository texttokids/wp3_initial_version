
********** Entity Names Features **********
*******************************************

1. Sentence: - Besoins d ' aujourd ' hui : il est de fait que l ' agglomération est actuellement radioconcentrique et les 5 millions d ' habitants de la banlieue souffrent de l ' insuffisance des transports .
Entity Name (Type) :: Besoins (LOC)


********** Emotions Features **********
***************************************

1. Sentence: - Besoins d ' aujourd ' hui : il est de fait que l ' agglomération est actuellement radioconcentrique et les 5 millions d ' habitants de la banlieue souffrent de l ' insuffisance des transports .
No features found.


********** Modality Features **********
***************************************

1. Sentence: - Besoins d ' aujourd ' hui : il est de fait que l ' agglomération est actuellement radioconcentrique et les 5 millions d ' habitants de la banlieue souffrent de l ' insuffisance des transports .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ besoin ]
alethique :: 1 :: [ besoin ]
boulique :: 1 :: [ besoin ]
marqueurs_niveau3 :: 1 :: [ besoin ]
alethique_niveau3 :: 1 :: [ besoin ]
boulique_niveau3 :: 1 :: [ besoin ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - Besoins d ' aujourd ' hui : il est de fait que l ' agglomération est actuellement radioconcentrique et les 5 millions d ' habitants de la banlieue souffrent de l ' insuffisance des transports .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['actuellement']
adverbiaux_localisation_temporelle::1::['actuellement']
adverbiaux_pointage_non_absolu::2::['actuellement', 'actuellement']
adverbiaux_loc_temp_focalisation_id::1::['actuellement']
adverbiaux_loc_temp_regionalisation_id::1::['actuellement']
adverbiaux_loc_temp_pointage_non_absolu::1::['actuellement']
adverbiaux_dur_iter_deictique::1::['actuellement']

