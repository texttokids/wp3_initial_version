====== Sentence Level Features ======

1. Sentence: Pour que demain s ' accomplisse une nouvelle transformation radicale , il faudrait qu ' on inventât un moyen de locomotion individuel , accessible à la plupart des citadins , plus rapide que l ' automobile et , surtout , pratiquement affranchi de toute infrastructure .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['demain']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['demain']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']
Total feature counts: 7

