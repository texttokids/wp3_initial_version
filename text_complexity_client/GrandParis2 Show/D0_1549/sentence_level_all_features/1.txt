
********** Entity Names Features **********
*******************************************

1. Sentence: Contrastant avec le caractère pavillonnaire qui a prévalu jusqu ' à la guerre de 1940 , la croissance des 15 dernières années a pris très souvent la forme d ' ensembles d ' immeubles collectifs , parfois très importants , sans pour autant donner naissance à de véritables villes nouvelles .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Contrastant avec le caractère pavillonnaire qui a prévalu jusqu ' à la guerre de 1940 , la croissance des 15 dernières années a pris très souvent la forme d ' ensembles d ' immeubles collectifs , parfois très importants , sans pour autant donner naissance à de véritables villes nouvelles .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Contrastant avec le caractère pavillonnaire qui a prévalu jusqu ' à la guerre de 1940 , la croissance des 15 dernières années a pris très souvent la forme d ' ensembles d ' immeubles collectifs , parfois très importants , sans pour autant donner naissance à de véritables villes nouvelles .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ guerre, souvent, parfois, important, véritable ]
marqueurs_niveau3 :: 3 :: [ guerre, souvent, parfois ]
alethique :: 2 :: [ souvent, parfois ]
axiologique :: 2 :: [ guerre, important ]
alethique_niveau3 :: 2 :: [ souvent, parfois ]
appreciatif :: 1 :: [ important ]
epistemique :: 1 :: [ important ]
pas_d_indication :: 1 :: [ véritable ]
marqueurs_niveau1 :: 1 :: [ important ]
marqueurs_niveau0 :: 1 :: [ véritable ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau3 :: 1 :: [ guerre ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ véritable ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Contrastant avec le caractère pavillonnaire qui a prévalu jusqu ' à la guerre de 1940 , la croissance des 15 dernières années a pris très souvent la forme d ' ensembles d ' immeubles collectifs , parfois très importants , sans pour autant donner naissance à de véritables villes nouvelles .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::4::['de 1940', 'des 15 dernières années', 'très souvent', 'parfois']
adverbiaux_localisation_temporelle::2::['de 1940', 'des 15 dernières années']
adverbiaux_purement_iteratifs::2::['très souvent', 'parfois']
adverbiaux_pointage_absolu::1::['de 1940']
adverbiaux_pointage_non_absolu::1::['des 15 dernières années']
adverbiaux_loc_temp_focalisation_id::2::['de 1940', 'des 15 dernières années']
adverbiaux_loc_temp_regionalisation_id::2::['de 1940', 'des 15 dernières années']
adverbiaux_loc_temp_pointage_absolu::1::['de 1940']
adverbiaux_loc_temp_pointage_non_absolu::1::['des 15 dernières années']
adverbiaux_iterateur_frequentiel::2::['très souvent', 'parfois']

