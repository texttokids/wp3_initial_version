
********** Entity Names Features **********
*******************************************

1. Sentence: Or l ' enfer des villes a parfois , dans le passé , été pavé des meilleures intentions architecturales .
Entity Name (Type) :: Or (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Or l ' enfer des villes a parfois , dans le passé , été pavé des meilleures intentions architecturales .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Or l ' enfer des villes a parfois , dans le passé , été pavé des meilleures intentions architecturales .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ parfois, meilleur, intention ]
marqueurs_niveau2 :: 2 :: [ meilleur, intention ]
alethique :: 1 :: [ parfois ]
appreciatif :: 1 :: [ meilleur ]
axiologique :: 1 :: [ meilleur ]
boulique :: 1 :: [ intention ]
marqueurs_niveau3 :: 1 :: [ parfois ]
alethique_niveau3 :: 1 :: [ parfois ]
appreciatif_niveau2 :: 1 :: [ meilleur ]
axiologique_niveau2 :: 1 :: [ meilleur ]
boulique_niveau2 :: 1 :: [ intention ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Or l ' enfer des villes a parfois , dans le passé , été pavé des meilleures intentions architecturales .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

