
********** Entity Names Features **********
*******************************************

1. Sentence: Les plans d ’ urbanisme intercommunaux en région parisienne faits par des groupes de 3 ou 4 ou 5 communes , nous commençons à les mettre en révision pour les insérer dans l ’ échelle des infrastructures régionales , cependant que nous étudions sur des terrains entièrement nouveaux qui jusqu ' à présent étaient des terrains arables , le schéma de structure de la ville nouvelle qui fera lui aussi l ' objet de débats , d ' explications qui ne deviendra opérationnel que le jour où il aura été reconnu et publié suivant les règles actuelles du Code de la Construction .
Entity Name (Type) :: Code de la Construction (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Les plans d ’ urbanisme intercommunaux en région parisienne faits par des groupes de 3 ou 4 ou 5 communes , nous commençons à les mettre en révision pour les insérer dans l ’ échelle des infrastructures régionales , cependant que nous étudions sur des terrains entièrement nouveaux qui jusqu ' à présent étaient des terrains arables , le schéma de structure de la ville nouvelle qui fera lui aussi l ' objet de débats , d ' explications qui ne deviendra opérationnel que le jour où il aura été reconnu et publié suivant les règles actuelles du Code de la Construction .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les plans d ’ urbanisme intercommunaux en région parisienne faits par des groupes de 3 ou 4 ou 5 communes , nous commençons à les mettre en révision pour les insérer dans l ’ échelle des infrastructures régionales , cependant que nous étudions sur des terrains entièrement nouveaux qui jusqu ' à présent étaient des terrains arables , le schéma de structure de la ville nouvelle qui fera lui aussi l ' objet de débats , d ' explications qui ne deviendra opérationnel que le jour où il aura été reconnu et publié suivant les règles actuelles du Code de la Construction .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ lui ]
alethique :: 1 :: [ lui ]
epistemique :: 1 :: [ lui ]
marqueurs_niveau3 :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les plans d ’ urbanisme intercommunaux en région parisienne faits par des groupes de 3 ou 4 ou 5 communes , nous commençons à les mettre en révision pour les insérer dans l ’ échelle des infrastructures régionales , cependant que nous étudions sur des terrains entièrement nouveaux qui jusqu ' à présent étaient des terrains arables , le schéma de structure de la ville nouvelle qui fera lui aussi l ' objet de débats , d ' explications qui ne deviendra opérationnel que le jour où il aura été reconnu et publié suivant les règles actuelles du Code de la Construction .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à présent']
adverbiaux_localisation_temporelle::1::['à présent']
adverbiaux_pointage_non_absolu::2::['à présent', 'à présent']
adverbiaux_loc_temp_focalisation_id::1::['à présent']
adverbiaux_loc_temp_regionalisation_id::1::['à présent']
adverbiaux_loc_temp_pointage_non_absolu::1::['à présent']
adverbiaux_dur_iter_deictique::1::['à présent']

