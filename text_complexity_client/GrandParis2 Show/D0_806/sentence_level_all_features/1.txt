
********** Entity Names Features **********
*******************************************

1. Sentence: Les pouvoirs publics , devant l ' insuffisance des terrains disponibles à l ' intérieur du périmètre , ont déjà accordé , en dérogation au P . A . D . O . G . , des permis de construire ou des accords préalables concernant une vingtaine de milliers de logements , sur des terrains extérieurs au périmètre d ' agglomération fixé à la fin de 1960 , il y a moins de cinq ans .
Entity Name (Type) :: G (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Les pouvoirs publics , devant l ' insuffisance des terrains disponibles à l ' intérieur du périmètre , ont déjà accordé , en dérogation au P . A . D . O . G . , des permis de construire ou des accords préalables concernant une vingtaine de milliers de logements , sur des terrains extérieurs au périmètre d ' agglomération fixé à la fin de 1960 , il y a moins de cinq ans .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les pouvoirs publics , devant l ' insuffisance des terrains disponibles à l ' intérieur du périmètre , ont déjà accordé , en dérogation au P . A . D . O . G . , des permis de construire ou des accords préalables concernant une vingtaine de milliers de logements , sur des terrains extérieurs au périmètre d ' agglomération fixé à la fin de 1960 , il y a moins de cinq ans .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ pouvoir ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les pouvoirs publics , devant l ' insuffisance des terrains disponibles à l ' intérieur du périmètre , ont déjà accordé , en dérogation au P . A . D . O . G . , des permis de construire ou des accords préalables concernant une vingtaine de milliers de logements , sur des terrains extérieurs au périmètre d ' agglomération fixé à la fin de 1960 , il y a moins de cinq ans .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à la fin de 1960']
adverbiaux_localisation_temporelle::1::['à la fin de 1960']
adverbiaux_pointage_absolu::1::['à la fin de 1960']
adverbiaux_pointage_non_absolu::1::['à la fin de 1960']
adverbiaux_loc_temp_focalisation_non_id::1::['à la fin de 1960']
adverbiaux_loc_temp_regionalisation_id::1::['à la fin de 1960']
adverbiaux_loc_temp_pointage_non_absolu::1::['à la fin de 1960']
adverbiaux_dur_iter_absolu::1::['à la fin de 1960']

