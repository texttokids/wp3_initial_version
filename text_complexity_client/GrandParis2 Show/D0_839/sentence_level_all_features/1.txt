
********** Entity Names Features **********
*******************************************

1. Sentence: Un décret du 5 janvier 1955 , complété notamment par un décret du 31 décembre 1958 , soumet à autorisation gouvernementale , après avis d ' une commission interministérielle , toute création supérieure à 500 m2 et toute extension supérieure à 10 % de la surface utilisée par des activités d ' industrie ou de bureau à la date du décret de 1955 .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Un décret du 5 janvier 1955 , complété notamment par un décret du 31 décembre 1958 , soumet à autorisation gouvernementale , après avis d ' une commission interministérielle , toute création supérieure à 500 m2 et toute extension supérieure à 10 % de la surface utilisée par des activités d ' industrie ou de bureau à la date du décret de 1955 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Un décret du 5 janvier 1955 , complété notamment par un décret du 31 décembre 1958 , soumet à autorisation gouvernementale , après avis d ' une commission interministérielle , toute création supérieure à 500 m2 et toute extension supérieure à 10 % de la surface utilisée par des activités d ' industrie ou de bureau à la date du décret de 1955 .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un décret du 5 janvier 1955 , complété notamment par un décret du 31 décembre 1958 , soumet à autorisation gouvernementale , après avis d ' une commission interministérielle , toute création supérieure à 500 m2 et toute extension supérieure à 10 % de la surface utilisée par des activités d ' industrie ou de bureau à la date du décret de 1955 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::4::['du 5 janvier 1955', 'du 31 décembre 1958', 'à la date', 'de 1955']
adverbiaux_localisation_temporelle::4::['du 5 janvier 1955', 'du 31 décembre 1958', 'à la date', 'de 1955']
adverbiaux_pointage_absolu::3::['du 5 janvier 1955', 'du 31 décembre 1958', 'de 1955']
adverbiaux_pointage_non_absolu::1::['à la date']
adverbiaux_loc_temp_focalisation_id::4::['du 5 janvier 1955', 'du 31 décembre 1958', 'à la date', 'de 1955']
adverbiaux_loc_temp_regionalisation_id::4::['du 5 janvier 1955', 'du 31 décembre 1958', 'à la date', 'de 1955']
adverbiaux_loc_temp_pointage_absolu::3::['du 5 janvier 1955', 'du 31 décembre 1958', 'de 1955']
adverbiaux_loc_temp_pointage_non_absolu::1::['à la date']

