
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est à ce moment là que je ne suis retourné vers un organisme que H .
Entity Name (Type) :: H (PER)


********** Emotions Features **********
***************************************

1. Sentence: C ' est à ce moment là que je ne suis retourné vers un organisme que H .
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' est à ce moment là que je ne suis retourné vers un organisme que H .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est à ce moment là que je ne suis retourné vers un organisme que H .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['à ce moment là', 'à ce moment']
adverbiaux_localisation_temporelle::2::['à ce moment là', 'à ce moment']
adverbiaux_pointage_non_absolu::3::['à ce moment là', 'à ce moment', 'à ce moment']
adverbiaux_loc_temp_focalisation_id::1::['à ce moment là']
adverbiaux_loc_temp_regionalisation_id::1::['à ce moment là']
adverbiaux_loc_temp_pointage_non_absolu::2::['à ce moment là', 'à ce moment']
adverbiaux_dur_iter_anaphorique::1::['à ce moment']

