
********** Entity Names Features **********
*******************************************

1. Sentence: J ’ espère aussi que si jamais les circonstances faisaient que le droit de préemption soit à exercer extrêmement souvent , le Ministre des Finances accepterait parce que là ou est entré dans le mécanisme , si on n ’ exerce pas le droit de préemption cela se retourne contre vous , car dès lorsque vous ne l ’ avez pas exercé , vous officialisez le prix auquel le vendeur a vendu le terrain à l ' acheteur ,
Entity Name (Type) :: Ministre des Finances (ORG)


********** Emotions Features **********
***************************************

1. Sentence: J ’ espère aussi que si jamais les circonstances faisaient que le droit de préemption soit à exercer extrêmement souvent , le Ministre des Finances accepterait parce que là ou est entré dans le mécanisme , si on n ’ exerce pas le droit de préemption cela se retourne contre vous , car dès lorsque vous ne l ’ avez pas exercé , vous officialisez le prix auquel le vendeur a vendu le terrain à l ' acheteur ,
Tokens having emotion: [ espère ]
Lemmas having emotion: [ espérer ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: J ’ espère aussi que si jamais les circonstances faisaient que le droit de préemption soit à exercer extrêmement souvent , le Ministre des Finances accepterait parce que là ou est entré dans le mécanisme , si on n ’ exerce pas le droit de préemption cela se retourne contre vous , car dès lorsque vous ne l ’ avez pas exercé , vous officialisez le prix auquel le vendeur a vendu le terrain à l ' acheteur ,
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ espérer, jamais, droit, souvent, droit, prix ]
marqueurs_niveau3 :: 5 :: [ espérer, jamais, droit, souvent, droit ]
alethique :: 2 :: [ jamais, souvent ]
deontique :: 2 :: [ droit, droit ]
alethique_niveau3 :: 2 :: [ jamais, souvent ]
deontique_niveau3 :: 2 :: [ droit, droit ]
appreciatif :: 1 :: [ prix ]
axiologique :: 1 :: [ prix ]
boulique :: 1 :: [ espérer ]
epistemique :: 1 :: [ espérer ]
marqueurs_niveau2 :: 1 :: [ prix ]
appreciatif_niveau2 :: 1 :: [ prix ]
axiologique_niveau2 :: 1 :: [ prix ]
boulique_niveau3 :: 1 :: [ espérer ]
epistemique_niveau3 :: 1 :: [ espérer ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ’ espère aussi que si jamais les circonstances faisaient que le droit de préemption soit à exercer extrêmement souvent , le Ministre des Finances accepterait parce que là ou est entré dans le mécanisme , si on n ’ exerce pas le droit de préemption cela se retourne contre vous , car dès lorsque vous ne l ’ avez pas exercé , vous officialisez le prix auquel le vendeur a vendu le terrain à l ' acheteur ,
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['si jamais', 'extrêmement souvent']
adverbiaux_purement_iteratifs::2::['si jamais', 'extrêmement souvent']
adverbiaux_iterateur_frequentiel::2::['si jamais', 'extrêmement souvent']

