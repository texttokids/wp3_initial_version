
********** Entity Names Features **********
*******************************************

1. Sentence: Dans d ' autres cas , des sites et des équipements affectés à une fonction déterminée peuvent présenter , pour peu qu ' elle soit bien discernée , une valeur non négligeable de culture ou de détente .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Dans d ' autres cas , des sites et des équipements affectés à une fonction déterminée peuvent présenter , pour peu qu ' elle soit bien discernée , une valeur non négligeable de culture ou de détente .
Tokens having emotion: [ affectés, déterminée, détente ]
Lemmas having emotion: [ affecter, déterminer, détente ]
Categories of the emotion lemmas: [ non_specifiee, audace, apaisement ]


********** Modality Features **********
***************************************

1. Sentence: Dans d ' autres cas , des sites et des équipements affectés à une fonction déterminée peuvent présenter , pour peu qu ' elle soit bien discernée , une valeur non négligeable de culture ou de détente .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pouvoir, bien, détente ]
marqueurs_niveau3 :: 2 :: [ pouvoir, bien ]
alethique :: 1 :: [ pouvoir ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ détente ]
marqueurs_niveau0 :: 1 :: [ détente ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ détente ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans d ' autres cas , des sites et des équipements affectés à une fonction déterminée peuvent présenter , pour peu qu ' elle soit bien discernée , une valeur non négligeable de culture ou de détente .
No features found.

