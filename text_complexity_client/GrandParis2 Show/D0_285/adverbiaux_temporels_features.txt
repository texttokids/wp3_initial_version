====== Sentence Level Features ======

1. Sentence: Cette prévision est d ' autant plus importante qu ' elle conduit à l ' action : préparer une région de Paris pour 14 millions de citadins qui y habiteraient aux environs de 2000 seulement , et non à une date plus rapprochée , c ' est tenir pour vraisemblable que les responsables des autres villes de France auront d ' ici là préparé leur ville , ou plutôt leur agglomération et dans certains cas leur région urbaine toute entière , à accueillir une population nouvelle à un rythme supérieur à celui de Paris , et du même ordre que celui que les plus dynamiques d ' entre elles ont vécu depuis quelques années .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 2000', 'depuis quelques années']
adverbiaux_localisation_temporelle::1::['de 2000']
adverbiaux_duratifs_iteratifs::1::['depuis quelques années']
adverbiaux_pointage_absolu::1::['de 2000']
adverbiaux_pointage_non_absolu::1::['depuis quelques années']
adverbiaux_loc_temp_focalisation_id::2::['de 2000', 'depuis quelques années']
adverbiaux_loc_temp_regionalisation_id::1::['de 2000']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis quelques années']
adverbiaux_loc_temp_pointage_absolu::1::['de 2000']
adverbiaux_dur_iter_deictique::1::['depuis quelques années']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 2000', 'depuis quelques années']
adverbiaux_localisation_temporelle::1::['de 2000']
adverbiaux_duratifs_iteratifs::1::['depuis quelques années']
adverbiaux_pointage_absolu::1::['de 2000']
adverbiaux_pointage_non_absolu::1::['depuis quelques années']
adverbiaux_loc_temp_focalisation_id::2::['de 2000', 'depuis quelques années']
adverbiaux_loc_temp_regionalisation_id::1::['de 2000']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis quelques années']
adverbiaux_loc_temp_pointage_absolu::1::['de 2000']
adverbiaux_dur_iter_deictique::1::['depuis quelques années']
Total feature counts: 12

