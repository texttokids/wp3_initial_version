
********** Entity Names Features **********
*******************************************

1. Sentence: La vie urbaine est faite de rencontres et d ' échanges et , mieux que des équipements dispersés , un à un , au milieu d ' usines ou d ' habitat , les centres urbains nouveaux peuvent contribuer à donner aux habitants des banlieues ce que la vie urbaine peut leur apporter de facultés d ' épanouissement humain .
Entity Name (Type) :: La vie urbaine (MISC)


********** Emotions Features **********
***************************************

1. Sentence: La vie urbaine est faite de rencontres et d ' échanges et , mieux que des équipements dispersés , un à un , au milieu d ' usines ou d ' habitat , les centres urbains nouveaux peuvent contribuer à donner aux habitants des banlieues ce que la vie urbaine peut leur apporter de facultés d ' épanouissement humain .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La vie urbaine est faite de rencontres et d ' échanges et , mieux que des équipements dispersés , un à un , au milieu d ' usines ou d ' habitat , les centres urbains nouveaux peuvent contribuer à donner aux habitants des banlieues ce que la vie urbaine peut leur apporter de facultés d ' épanouissement humain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ mieux, pouvoir, pouvoir, lui ]
alethique :: 3 :: [ pouvoir, pouvoir, lui ]
epistemique :: 3 :: [ pouvoir, pouvoir, lui ]
marqueurs_niveau3 :: 3 :: [ pouvoir, pouvoir, lui ]
alethique_niveau3 :: 3 :: [ pouvoir, pouvoir, lui ]
epistemique_niveau3 :: 3 :: [ pouvoir, pouvoir, lui ]
deontique :: 2 :: [ pouvoir, pouvoir ]
deontique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
appreciatif :: 1 :: [ mieux ]
axiologique :: 1 :: [ mieux ]
marqueurs_niveau2 :: 1 :: [ mieux ]
appreciatif_niveau2 :: 1 :: [ mieux ]
axiologique_niveau2 :: 1 :: [ mieux ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La vie urbaine est faite de rencontres et d ' échanges et , mieux que des équipements dispersés , un à un , au milieu d ' usines ou d ' habitat , les centres urbains nouveaux peuvent contribuer à donner aux habitants des banlieues ce que la vie urbaine peut leur apporter de facultés d ' épanouissement humain .
No features found.

