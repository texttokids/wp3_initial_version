
********** Entity Names Features **********
*******************************************

1. Sentence: La dispersion des lotissements au hasard des spéculations , depuis la guerre , est une lourde charge qui grèvera pendant très longtemps les finances des municipalités de la région parisienne .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La dispersion des lotissements au hasard des spéculations , depuis la guerre , est une lourde charge qui grèvera pendant très longtemps les finances des municipalités de la région parisienne .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La dispersion des lotissements au hasard des spéculations , depuis la guerre , est une lourde charge qui grèvera pendant très longtemps les finances des municipalités de la région parisienne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ guerre, lourd, longtemps ]
epistemique :: 2 :: [ lourd, longtemps ]
marqueurs_niveau3 :: 2 :: [ guerre, lourd ]
axiologique :: 1 :: [ guerre ]
marqueurs_niveau1 :: 1 :: [ longtemps ]
axiologique_niveau3 :: 1 :: [ guerre ]
epistemique_niveau3 :: 1 :: [ lourd ]
epistemique_niveau1 :: 1 :: [ longtemps ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La dispersion des lotissements au hasard des spéculations , depuis la guerre , est une lourde charge qui grèvera pendant très longtemps les finances des municipalités de la région parisienne .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['pendant très longtemps']
adverbiaux_duratifs_iteratifs::1::['pendant très longtemps']
adverbiaux_pointage_non_absolu::1::['pendant très longtemps']
adverbiaux_dur_iter_anaphorique::1::['pendant très longtemps']

