
********** Entity Names Features **********
*******************************************

1. Sentence: La mobilisation du potentiel forestier existant en région de Paris - qui constitue maintenant un véritable service public - pose donc des problèmes d ' acquisition par les collectivités publiques , d ' aménagement et , il faut le dire , de police (l) .
Entity Name (Type) :: région de Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La mobilisation du potentiel forestier existant en région de Paris - qui constitue maintenant un véritable service public - pose donc des problèmes d ' acquisition par les collectivités publiques , d ' aménagement et , il faut le dire , de police (l) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La mobilisation du potentiel forestier existant en région de Paris - qui constitue maintenant un véritable service public - pose donc des problèmes d ' acquisition par les collectivités publiques , d ' aménagement et , il faut le dire , de police (l) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ véritable, problème, falloir, dire ]
marqueurs_niveau3 :: 2 :: [ falloir, dire ]
appreciatif :: 1 :: [ problème ]
boulique :: 1 :: [ dire ]
deontique :: 1 :: [ falloir ]
pas_d_indication :: 1 :: [ véritable ]
marqueurs_niveau2 :: 1 :: [ problème ]
marqueurs_niveau0 :: 1 :: [ véritable ]
appreciatif_niveau2 :: 1 :: [ problème ]
boulique_niveau3 :: 1 :: [ dire ]
deontique_niveau3 :: 1 :: [ falloir ]
pas_d_indication_niveau0 :: 1 :: [ véritable ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La mobilisation du potentiel forestier existant en région de Paris - qui constitue maintenant un véritable service public - pose donc des problèmes d ' acquisition par les collectivités publiques , d ' aménagement et , il faut le dire , de police (l) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['maintenant']
adverbiaux_localisation_temporelle::1::['maintenant']
adverbiaux_pointage_non_absolu::1::['maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::1::['maintenant']
adverbiaux_loc_temp_pointage_non_absolu::1::['maintenant']

