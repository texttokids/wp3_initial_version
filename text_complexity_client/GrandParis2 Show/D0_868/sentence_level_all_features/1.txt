
********** Entity Names Features **********
*******************************************

1. Sentence: Après une stagnation de plusieurs décennies , la pression des besoins a été ressentie avec force , et dès 1960 un programme triennal a fait démarrer de grands travaux d ' équipement public dans la région de Paris : les crédits de paiement , pour l ' ensemble des équipements publics en cette région , d ' un milliard de francs en 1960 , avaient doublé en 1963;
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Après une stagnation de plusieurs décennies , la pression des besoins a été ressentie avec force , et dès 1960 un programme triennal a fait démarrer de grands travaux d ' équipement public dans la région de Paris : les crédits de paiement , pour l ' ensemble des équipements publics en cette région , d ' un milliard de francs en 1960 , avaient doublé en 1963;
Tokens having emotion: [ ressentie ]
Lemmas having emotion: [ ressentir ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Après une stagnation de plusieurs décennies , la pression des besoins a été ressentie avec force , et dès 1960 un programme triennal a fait démarrer de grands travaux d ' équipement public dans la région de Paris : les crédits de paiement , pour l ' ensemble des équipements publics en cette région , d ' un milliard de francs en 1960 , avaient doublé en 1963;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ pression, besoin, force, grand, travail ]
marqueurs_niveau3 :: 3 :: [ besoin, grand, travail ]
pas_d_indication :: 2 :: [ pression, force ]
marqueurs_niveau0 :: 2 :: [ pression, force ]
pas_d_indication_niveau0 :: 2 :: [ pression, force ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ travail ]
boulique :: 1 :: [ besoin ]
epistemique :: 1 :: [ grand ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau3 :: 1 :: [ travail ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Après une stagnation de plusieurs décennies , la pression des besoins a été ressentie avec force , et dès 1960 un programme triennal a fait démarrer de grands travaux d ' équipement public dans la région de Paris : les crédits de paiement , pour l ' ensemble des équipements publics en cette région , d ' un milliard de francs en 1960 , avaient doublé en 1963;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_localisation_temporelle::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_pointage_absolu::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_pointage_non_absolu::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_loc_temp_focalisation_id::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_loc_temp_regionalisation_id::2::['en 1960', 'en 1963']
adverbiaux_loc_temp_regionalisation_non_id::1::['dès 1960']
adverbiaux_loc_temp_pointage_non_absolu::3::['dès 1960', 'en 1960', 'en 1963']
adverbiaux_dur_iter_absolu::3::['dès 1960', 'en 1960', 'en 1963']

