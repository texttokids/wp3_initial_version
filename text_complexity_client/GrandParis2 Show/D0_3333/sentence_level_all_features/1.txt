
********** Entity Names Features **********
*******************************************

1. Sentence: Le District a établi un programme d ' ensemble et a commencé , par l ' ouverture de crédits en son budget , à rassembler les moyens d ' acheter celles de ces forêts qui sont le plus menacées , et de les aménager afin que leur ouverture au public ne leur soit en rien dommageable .
Entity Name (Type) :: District (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le District a établi un programme d ' ensemble et a commencé , par l ' ouverture de crédits en son budget , à rassembler les moyens d ' acheter celles de ces forêts qui sont le plus menacées , et de les aménager afin que leur ouverture au public ne leur soit en rien dommageable .
Tokens having emotion: [ menacées ]
Lemmas having emotion: [ menacer ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Le District a établi un programme d ' ensemble et a commencé , par l ' ouverture de crédits en son budget , à rassembler les moyens d ' acheter celles de ces forêts qui sont le plus menacées , et de les aménager afin que leur ouverture au public ne leur soit en rien dommageable .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ menacer, lui, rien ]
alethique :: 2 :: [ lui, rien ]
marqueurs_niveau2 :: 2 :: [ menacer, rien ]
appreciatif :: 1 :: [ menacer ]
axiologique :: 1 :: [ menacer ]
epistemique :: 1 :: [ lui ]
marqueurs_niveau3 :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
alethique_niveau2 :: 1 :: [ rien ]
appreciatif_niveau2 :: 1 :: [ menacer ]
axiologique_niveau2 :: 1 :: [ menacer ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le District a établi un programme d ' ensemble et a commencé , par l ' ouverture de crédits en son budget , à rassembler les moyens d ' acheter celles de ces forêts qui sont le plus menacées , et de les aménager afin que leur ouverture au public ne leur soit en rien dommageable .
No features found.

