
********** Entity Names Features **********
*******************************************

1. Sentence: Paris , capitale du pays et métropole mondiale , s ' identifie souvent dans l ' opinion au siège du gouvernement , au quartier général de la grande industrie et du grand commerce , aux universités , aux théâtres , aux boutiques luxueuses qui bordent quelques rues célèbres .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Paris , capitale du pays et métropole mondiale , s ' identifie souvent dans l ' opinion au siège du gouvernement , au quartier général de la grande industrie et du grand commerce , aux universités , aux théâtres , aux boutiques luxueuses qui bordent quelques rues célèbres .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Paris , capitale du pays et métropole mondiale , s ' identifie souvent dans l ' opinion au siège du gouvernement , au quartier général de la grande industrie et du grand commerce , aux universités , aux théâtres , aux boutiques luxueuses qui bordent quelques rues célèbres .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ souvent, grand, grand ]
marqueurs_niveau3 :: 3 :: [ souvent, grand, grand ]
epistemique :: 2 :: [ grand, grand ]
epistemique_niveau3 :: 2 :: [ grand, grand ]
alethique :: 1 :: [ souvent ]
alethique_niveau3 :: 1 :: [ souvent ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Paris , capitale du pays et métropole mondiale , s ' identifie souvent dans l ' opinion au siège du gouvernement , au quartier général de la grande industrie et du grand commerce , aux universités , aux théâtres , aux boutiques luxueuses qui bordent quelques rues célèbres .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

