
********** Entity Names Features **********
*******************************************

1. Sentence: Cette proportion donne déjà une mesure des besoins d ' espace à satisfaire , mais une mesure très insuffisante .
Entity Name (Type) :: Cette (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette proportion donne déjà une mesure des besoins d ' espace à satisfaire , mais une mesure très insuffisante .
Tokens having emotion: [ satisfaire ]
Lemmas having emotion: [ satisfaire ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Cette proportion donne déjà une mesure des besoins d ' espace à satisfaire , mais une mesure très insuffisante .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ besoin, satisfaire ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ satisfaire ]
boulique :: 1 :: [ besoin ]
marqueurs_niveau3 :: 1 :: [ besoin ]
marqueurs_niveau2 :: 1 :: [ satisfaire ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau2 :: 1 :: [ satisfaire ]
boulique_niveau3 :: 1 :: [ besoin ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette proportion donne déjà une mesure des besoins d ' espace à satisfaire , mais une mesure très insuffisante .
No features found.

