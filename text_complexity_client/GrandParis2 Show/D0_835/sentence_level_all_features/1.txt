
********** Entity Names Features **********
*******************************************

1. Sentence: Au total , l ' exiguïté du périmètre d ' agglomération a facilité , en la justifiant , la généralisation de l '  " urbanisme dérogatoire " et la généralisation de l ' urbanisme dérogatoire , outre les inégalités , les injustices et les tentations qu ' elle provoque , risque de tuer tout effort et bientôt tout espoir d ' aménagement rationnel et de composition d ' urbanisme .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Au total , l ' exiguïté du périmètre d ' agglomération a facilité , en la justifiant , la généralisation de l '  " urbanisme dérogatoire " et la généralisation de l ' urbanisme dérogatoire , outre les inégalités , les injustices et les tentations qu ' elle provoque , risque de tuer tout effort et bientôt tout espoir d ' aménagement rationnel et de composition d ' urbanisme .
Tokens having emotion: [ tentations, provoque, espoir ]
Lemmas having emotion: [ tentation, provoquer, espoir ]
Categories of the emotion lemmas: [ desir, non_specifiee, desir ]


********** Modality Features **********
***************************************

1. Sentence: Au total , l ' exiguïté du périmètre d ' agglomération a facilité , en la justifiant , la généralisation de l '  " urbanisme dérogatoire " et la généralisation de l ' urbanisme dérogatoire , outre les inégalités , les injustices et les tentations qu ' elle provoque , risque de tuer tout effort et bientôt tout espoir d ' aménagement rationnel et de composition d ' urbanisme .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ inégalité, injustice, risquer, tuer, espoir ]
marqueurs_niveau2 :: 3 :: [ injustice, tuer, espoir ]
axiologique :: 2 :: [ injustice, tuer ]
axiologique_niveau2 :: 2 :: [ injustice, tuer ]
alethique :: 1 :: [ tuer ]
appreciatif :: 1 :: [ risquer ]
boulique :: 1 :: [ espoir ]
epistemique :: 1 :: [ espoir ]
pas_d_indication :: 1 :: [ inégalité ]
marqueurs_niveau3 :: 1 :: [ risquer ]
marqueurs_niveau0 :: 1 :: [ inégalité ]
alethique_niveau2 :: 1 :: [ tuer ]
appreciatif_niveau3 :: 1 :: [ risquer ]
boulique_niveau2 :: 1 :: [ espoir ]
epistemique_niveau2 :: 1 :: [ espoir ]
pas_d_indication_niveau0 :: 1 :: [ inégalité ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Au total , l ' exiguïté du périmètre d ' agglomération a facilité , en la justifiant , la généralisation de l '  " urbanisme dérogatoire " et la généralisation de l ' urbanisme dérogatoire , outre les inégalités , les injustices et les tentations qu ' elle provoque , risque de tuer tout effort et bientôt tout espoir d ' aménagement rationnel et de composition d ' urbanisme .
No features found.

