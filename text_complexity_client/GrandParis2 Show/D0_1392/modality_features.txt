====== Sentence Level Features ======

1. Sentence: au fond de la vallée , des espaces verts , des installations de loisir et un habitat très dispersé peuvent mettre en valeur des sites qui sont généralement agréables , et , dans certaines des boucles de la Seine , d ' une rare qualité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ loisir, pouvoir, généralement, agréable, certain, rare ]
marqueurs_niveau3 :: 4 :: [ pouvoir, généralement, agréable, certain ]
epistemique :: 3 :: [ pouvoir, certain, rare ]
alethique :: 2 :: [ pouvoir, généralement ]
appreciatif :: 2 :: [ loisir, agréable ]
alethique_niveau3 :: 2 :: [ pouvoir, généralement ]
epistemique_niveau3 :: 2 :: [ pouvoir, certain ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ loisir ]
marqueurs_niveau1 :: 1 :: [ rare ]
appreciatif_niveau3 :: 1 :: [ agréable ]
appreciatif_niveau2 :: 1 :: [ loisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ rare ]
Total feature counts: 28


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ loisir, pouvoir, généralement, agréable, certain, rare ]
marqueurs_niveau3 :: 4 :: [ pouvoir, généralement, agréable, certain ]
epistemique :: 3 :: [ pouvoir, certain, rare ]
alethique :: 2 :: [ pouvoir, généralement ]
appreciatif :: 2 :: [ loisir, agréable ]
alethique_niveau3 :: 2 :: [ pouvoir, généralement ]
epistemique_niveau3 :: 2 :: [ pouvoir, certain ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ loisir ]
marqueurs_niveau1 :: 1 :: [ rare ]
appreciatif_niveau3 :: 1 :: [ agréable ]
appreciatif_niveau2 :: 1 :: [ loisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ rare ]
Total feature counts: 28

