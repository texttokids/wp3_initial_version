====== Sentence Level Features ======

1. Sentence: Ce paradoxe apparent s ' explique par le fait que les activités les plus dynamiques sont mieux représentées en région parisienne , même si leur taux d ’ expansion y est moindre qu ' ailleurs , tandis que les secteurs économiques en recul , comme les industries textiles , ou en faible progression , comme la sidérurgie , n ' y occupent généralement qu ' une place modeste : la structure des activités favorisera donc la croissance de l ' économie parisienne , tant que les métropoles d ' équilibre , dont le développement est encouragé , n ' auront pas atteint le seuil qui leur permettra d ' accueillir efficacement des activités de pointe .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['généralement']
adverbiaux_purement_iteratifs::1::['généralement']
adverbiaux_iterateur_frequentiel::1::['généralement']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['généralement']
adverbiaux_purement_iteratifs::1::['généralement']
adverbiaux_iterateur_frequentiel::1::['généralement']
Total feature counts: 3

