
********** Entity Names Features **********
*******************************************

1. Sentence: - A l ' est du plateau de Montreuil , s ' étendant jusqu ' aux hauteurs de Clichy-sous-Bois et de la forêt de Bondy , d ' autres lotissements , souvent de bonne qualité , se sont localisés , au Raincy et à Villemomble sur la ligne de Meaux .
Entity Name (Type) :: ligne de Meaux (LOC)


********** Emotions Features **********
***************************************

1. Sentence: - A l ' est du plateau de Montreuil , s ' étendant jusqu ' aux hauteurs de Clichy-sous-Bois et de la forêt de Bondy , d ' autres lotissements , souvent de bonne qualité , se sont localisés , au Raincy et à Villemomble sur la ligne de Meaux .
No features found.


********** Modality Features **********
***************************************

1. Sentence: - A l ' est du plateau de Montreuil , s ' étendant jusqu ' aux hauteurs de Clichy-sous-Bois et de la forêt de Bondy , d ' autres lotissements , souvent de bonne qualité , se sont localisés , au Raincy et à Villemomble sur la ligne de Meaux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ souvent, bon ]
marqueurs_niveau3 :: 2 :: [ souvent, bon ]
alethique :: 1 :: [ souvent ]
appreciatif :: 1 :: [ bon ]
axiologique :: 1 :: [ bon ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau3 :: 1 :: [ bon ]
axiologique_niveau3 :: 1 :: [ bon ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - A l ' est du plateau de Montreuil , s ' étendant jusqu ' aux hauteurs de Clichy-sous-Bois et de la forêt de Bondy , d ' autres lotissements , souvent de bonne qualité , se sont localisés , au Raincy et à Villemomble sur la ligne de Meaux .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

