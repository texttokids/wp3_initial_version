
********** Entity Names Features **********
*******************************************

1. Sentence: Malheureusement n ' ayant pas eu les moyens financiers que j ’ ai pu obtenir du district ils sont dans une affaire plus difficile parce qu ’ elle est d ' essence économique avant d ’ être d ' essence urbaine .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Malheureusement n ' ayant pas eu les moyens financiers que j ’ ai pu obtenir du district ils sont dans une affaire plus difficile parce qu ’ elle est d ' essence économique avant d ’ être d ' essence urbaine .
Tokens having emotion: [ Malheureusement ]
Lemmas having emotion: [ malheureusement ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Malheureusement n ' ayant pas eu les moyens financiers que j ’ ai pu obtenir du district ils sont dans une affaire plus difficile parce qu ’ elle est d ' essence économique avant d ’ être d ' essence urbaine .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ malheureusement, pouvoir, difficile ]
appreciatif :: 2 :: [ malheureusement, difficile ]
epistemique :: 2 :: [ pouvoir, difficile ]
marqueurs_niveau2 :: 2 :: [ malheureusement, difficile ]
appreciatif_niveau2 :: 2 :: [ malheureusement, difficile ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ difficile ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Malheureusement n ' ayant pas eu les moyens financiers que j ’ ai pu obtenir du district ils sont dans une affaire plus difficile parce qu ’ elle est d ' essence économique avant d ’ être d ' essence urbaine .
No features found.

