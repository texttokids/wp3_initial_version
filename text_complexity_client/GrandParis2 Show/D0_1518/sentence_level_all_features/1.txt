
********** Entity Names Features **********
*******************************************

1. Sentence: Ainsi se trouve confirmée sur le sol une des options majeures du parti d ' aménagement , qui est de limiter vers le nord l ' extension de l ' agglomération parisienne en maintenant en zone agricole la Plaine de France , située au sud des ensembles forestiers de Chantilly .
Entity Name (Type) :: Chantilly (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ainsi se trouve confirmée sur le sol une des options majeures du parti d ' aménagement , qui est de limiter vers le nord l ' extension de l ' agglomération parisienne en maintenant en zone agricole la Plaine de France , située au sud des ensembles forestiers de Chantilly .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ainsi se trouve confirmée sur le sol une des options majeures du parti d ' aménagement , qui est de limiter vers le nord l ' extension de l ' agglomération parisienne en maintenant en zone agricole la Plaine de France , située au sud des ensembles forestiers de Chantilly .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ainsi se trouve confirmée sur le sol une des options majeures du parti d ' aménagement , qui est de limiter vers le nord l ' extension de l ' agglomération parisienne en maintenant en zone agricole la Plaine de France , située au sud des ensembles forestiers de Chantilly .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['maintenant']
adverbiaux_localisation_temporelle::1::['maintenant']
adverbiaux_pointage_non_absolu::1::['maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::1::['maintenant']
adverbiaux_loc_temp_pointage_non_absolu::1::['maintenant']

