
********** Entity Names Features **********
*******************************************

1. Sentence: Ce million et demi d ' habitants , équivalant à la population future d ' une métropole d ' équilibre , est un solde : il n ' est pas possible de prédire dans quelle proportion il s ' agira de provinciaux restant en province – mais pas forcément dans leur localité ou dans leur région d ’ origine – de parisiens quittant leur ville natale , d ' étrangers trouvant ailleurs les emplois et le cadre de vie qu ' ils seraient venus chercher à Paris .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ce million et demi d ' habitants , équivalant à la population future d ' une métropole d ' équilibre , est un solde : il n ' est pas possible de prédire dans quelle proportion il s ' agira de provinciaux restant en province – mais pas forcément dans leur localité ou dans leur région d ’ origine – de parisiens quittant leur ville natale , d ' étrangers trouvant ailleurs les emplois et le cadre de vie qu ' ils seraient venus chercher à Paris .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ce million et demi d ' habitants , équivalant à la population future d ' une métropole d ' équilibre , est un solde : il n ' est pas possible de prédire dans quelle proportion il s ' agira de provinciaux restant en province – mais pas forcément dans leur localité ou dans leur région d ’ origine – de parisiens quittant leur ville natale , d ' étrangers trouvant ailleurs les emplois et le cadre de vie qu ' ils seraient venus chercher à Paris .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ possible, prédire, forcément, quitter, emploi, venir, chercher ]
alethique :: 4 :: [ possible, forcément, quitter, venir ]
marqueurs_niveau3 :: 3 :: [ possible, venir, chercher ]
marqueurs_niveau2 :: 3 :: [ forcément, quitter, emploi ]
alethique_niveau3 :: 2 :: [ possible, venir ]
alethique_niveau2 :: 2 :: [ forcément, quitter ]
appreciatif :: 1 :: [ emploi ]
boulique :: 1 :: [ chercher ]
pas_d_indication :: 1 :: [ prédire ]
marqueurs_niveau0 :: 1 :: [ prédire ]
appreciatif_niveau2 :: 1 :: [ emploi ]
boulique_niveau3 :: 1 :: [ chercher ]
pas_d_indication_niveau0 :: 1 :: [ prédire ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce million et demi d ' habitants , équivalant à la population future d ' une métropole d ' équilibre , est un solde : il n ' est pas possible de prédire dans quelle proportion il s ' agira de provinciaux restant en province – mais pas forcément dans leur localité ou dans leur région d ’ origine – de parisiens quittant leur ville natale , d ' étrangers trouvant ailleurs les emplois et le cadre de vie qu ' ils seraient venus chercher à Paris .
No features found.

