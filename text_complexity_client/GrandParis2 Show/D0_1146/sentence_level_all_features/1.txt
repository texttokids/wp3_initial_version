
********** Entity Names Features **********
*******************************************

1. Sentence: Cette heure qui aujourd ' hui sépare de Paris ces villes périphériques peut être dans quelques années réduite à cinquante minutes , voire plus tard à une demi-heure* .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette heure qui aujourd ' hui sépare de Paris ces villes périphériques peut être dans quelques années réduite à cinquante minutes , voire plus tard à une demi-heure* .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Cette heure qui aujourd ' hui sépare de Paris ces villes périphériques peut être dans quelques années réduite à cinquante minutes , voire plus tard à une demi-heure* .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ pouvoir ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette heure qui aujourd ' hui sépare de Paris ces villes périphériques peut être dans quelques années réduite à cinquante minutes , voire plus tard à une demi-heure* .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['Cette heure', 'plus tard', 'à une demi-heure']
adverbiaux_localisation_temporelle::3::['Cette heure', 'plus tard', 'à une demi-heure']
adverbiaux_pointage_non_absolu::4::['Cette heure', 'plus tard', 'à une demi-heure', 'à une demi-heure']
adverbiaux_loc_temp_focalisation_id::1::['Cette heure']
adverbiaux_loc_temp_regionalisation_id::2::['Cette heure', 'plus tard']
adverbiaux_loc_temp_pointage_non_absolu::3::['Cette heure', 'plus tard', 'à une demi-heure']
adverbiaux_dur_iter_relatif::1::['à une demi-heure']

