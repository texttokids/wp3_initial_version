
********** Entity Names Features **********
*******************************************

1. Sentence: Le préconisent tantôt quelques politiques parisiens soucieux d ' avoir un maire de Paris , tantôt quelques régionalistes fervents croyant qu ' en décentrant la capitale on activerait la décentralisation , tantôt quelques amateurs de capitale neuve au milieu des champs .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le préconisent tantôt quelques politiques parisiens soucieux d ' avoir un maire de Paris , tantôt quelques régionalistes fervents croyant qu ' en décentrant la capitale on activerait la décentralisation , tantôt quelques amateurs de capitale neuve au milieu des champs .
Tokens having emotion: [ soucieux ]
Lemmas having emotion: [ soucieux ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Le préconisent tantôt quelques politiques parisiens soucieux d ' avoir un maire de Paris , tantôt quelques régionalistes fervents croyant qu ' en décentrant la capitale on activerait la décentralisation , tantôt quelques amateurs de capitale neuve au milieu des champs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ croire ]
epistemique :: 1 :: [ croire ]
marqueurs_niveau3 :: 1 :: [ croire ]
epistemique_niveau3 :: 1 :: [ croire ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le préconisent tantôt quelques politiques parisiens soucieux d ' avoir un maire de Paris , tantôt quelques régionalistes fervents croyant qu ' en décentrant la capitale on activerait la décentralisation , tantôt quelques amateurs de capitale neuve au milieu des champs .
No features found.

