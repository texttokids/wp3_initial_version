====== Sentence Level Features ======

1. Sentence: Ce Comité fut à l ' origine de la loi du 14 mai 1932 qui définissait la région parisienne comme l ' intérieur d ' un cercle de 35 kilomètres de rayon tracé autour de Notre-Dame (1) , prescrivait l ' établissement d ' un plan d ' aménagement de la région et créait , au sein du comité supérieur , un groupe de 40 membres , juristes , urbanistes , ingénieurs , nommés pour 3 ans par le Ministre de l ' Intérieur .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['du 14 mai 1932', 'pour 3 ans']
adverbiaux_localisation_temporelle::1::['du 14 mai 1932']
adverbiaux_duratifs_iteratifs::1::['pour 3 ans']
adverbiaux_pointage_absolu::1::['du 14 mai 1932']
adverbiaux_pointage_non_absolu::1::['pour 3 ans']
adverbiaux_loc_temp_focalisation_id::1::['du 14 mai 1932']
adverbiaux_loc_temp_regionalisation_id::1::['du 14 mai 1932']
adverbiaux_loc_temp_pointage_absolu::1::['du 14 mai 1932']
adverbiaux_dur_iter_relatif::1::['pour 3 ans']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['du 14 mai 1932', 'pour 3 ans']
adverbiaux_localisation_temporelle::1::['du 14 mai 1932']
adverbiaux_duratifs_iteratifs::1::['pour 3 ans']
adverbiaux_pointage_absolu::1::['du 14 mai 1932']
adverbiaux_pointage_non_absolu::1::['pour 3 ans']
adverbiaux_loc_temp_focalisation_id::1::['du 14 mai 1932']
adverbiaux_loc_temp_regionalisation_id::1::['du 14 mai 1932']
adverbiaux_loc_temp_pointage_absolu::1::['du 14 mai 1932']
adverbiaux_dur_iter_relatif::1::['pour 3 ans']
Total feature counts: 10

