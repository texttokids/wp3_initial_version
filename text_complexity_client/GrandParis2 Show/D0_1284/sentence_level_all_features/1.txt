
********** Entity Names Features **********
*******************************************

1. Sentence: de tels désagréments existent d ' ailleurs dans beaucoup d ' appartements anciens de Paris intra-muros , où ils sont moins " ressentis " parce que ces appartements n ' offrent pas l ' inconvénient fondamental de l ' éloignement du centre urbain .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: de tels désagréments existent d ' ailleurs dans beaucoup d ' appartements anciens de Paris intra-muros , où ils sont moins " ressentis " parce que ces appartements n ' offrent pas l ' inconvénient fondamental de l ' éloignement du centre urbain .
Tokens having emotion: [ désagréments, ressentis ]
Lemmas having emotion: [ désagrément, ressentir ]
Categories of the emotion lemmas: [ deplaisir, non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: de tels désagréments existent d ' ailleurs dans beaucoup d ' appartements anciens de Paris intra-muros , où ils sont moins " ressentis " parce que ces appartements n ' offrent pas l ' inconvénient fondamental de l ' éloignement du centre urbain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ beaucoup, offrir ]
appreciatif :: 1 :: [ offrir ]
epistemique :: 1 :: [ beaucoup ]
marqueurs_niveau2 :: 1 :: [ offrir ]
marqueurs_niveau1 :: 1 :: [ beaucoup ]
appreciatif_niveau2 :: 1 :: [ offrir ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: de tels désagréments existent d ' ailleurs dans beaucoup d ' appartements anciens de Paris intra-muros , où ils sont moins " ressentis " parce que ces appartements n ' offrent pas l ' inconvénient fondamental de l ' éloignement du centre urbain .
No features found.

