====== Sentence Level Features ======

1. Sentence: Le Gouvernement britannique , en même temps qu ' il décidait une extension de la ceinture verte sur les terres rurales de l ' extérieur , a admis que les autorités locales d ' urbanisme doivent " prendre en considération les possibilités offertes pour la construction par certains terrains situés à l ' intérieur de cette ceinture " .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ décider, autorité, devoir, possibilité, offrir, certain ]
marqueurs_niveau3 :: 4 :: [ décider, devoir, possibilité, certain ]
epistemique :: 3 :: [ décider, devoir, certain ]
epistemique_niveau3 :: 3 :: [ décider, devoir, certain ]
alethique :: 2 :: [ devoir, possibilité ]
alethique_niveau3 :: 2 :: [ devoir, possibilité ]
appreciatif :: 1 :: [ offrir ]
boulique :: 1 :: [ décider ]
deontique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ autorité ]
marqueurs_niveau2 :: 1 :: [ offrir ]
marqueurs_niveau0 :: 1 :: [ autorité ]
appreciatif_niveau2 :: 1 :: [ offrir ]
boulique_niveau3 :: 1 :: [ décider ]
deontique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ autorité ]
Total feature counts: 30


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ décider, autorité, devoir, possibilité, offrir, certain ]
marqueurs_niveau3 :: 4 :: [ décider, devoir, possibilité, certain ]
epistemique :: 3 :: [ décider, devoir, certain ]
epistemique_niveau3 :: 3 :: [ décider, devoir, certain ]
alethique :: 2 :: [ devoir, possibilité ]
alethique_niveau3 :: 2 :: [ devoir, possibilité ]
appreciatif :: 1 :: [ offrir ]
boulique :: 1 :: [ décider ]
deontique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ autorité ]
marqueurs_niveau2 :: 1 :: [ offrir ]
marqueurs_niveau0 :: 1 :: [ autorité ]
appreciatif_niveau2 :: 1 :: [ offrir ]
boulique_niveau3 :: 1 :: [ décider ]
deontique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ autorité ]
Total feature counts: 30

