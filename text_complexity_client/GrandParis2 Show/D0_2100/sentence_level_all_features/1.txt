
********** Entity Names Features **********
*******************************************

1. Sentence: Mais la concentration de ces derniers pendant de courtes périodes de l ' année rend cet exode spectaculaire : les trois quarts des Parisiens partent en vacances , et presque la moitié des départs se situe au cours de la première quinzaine d ' août .
Entity Name (Type) :: Parisiens (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Mais la concentration de ces derniers pendant de courtes périodes de l ' année rend cet exode spectaculaire : les trois quarts des Parisiens partent en vacances , et presque la moitié des départs se situe au cours de la première quinzaine d ' août .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mais la concentration de ces derniers pendant de courtes périodes de l ' année rend cet exode spectaculaire : les trois quarts des Parisiens partent en vacances , et presque la moitié des départs se situe au cours de la première quinzaine d ' août .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ spectaculaire, partir, vacance ]
appreciatif :: 2 :: [ spectaculaire, vacance ]
alethique :: 1 :: [ partir ]
marqueurs_niveau3 :: 1 :: [ vacance ]
marqueurs_niveau2 :: 1 :: [ spectaculaire ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau1 :: 1 :: [ partir ]
appreciatif_niveau3 :: 1 :: [ vacance ]
appreciatif_niveau2 :: 1 :: [ spectaculaire ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais la concentration de ces derniers pendant de courtes périodes de l ' année rend cet exode spectaculaire : les trois quarts des Parisiens partent en vacances , et presque la moitié des départs se situe au cours de la première quinzaine d ' août .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['d ` août']
adverbiaux_localisation_temporelle::1::['d ` août']
adverbiaux_pointage_non_absolu::1::['d ` août']
adverbiaux_loc_temp_focalisation_id::1::['d ` août']
adverbiaux_loc_temp_regionalisation_id::1::['d ` août']
adverbiaux_loc_temp_pointage_non_absolu::1::['d ` août']

