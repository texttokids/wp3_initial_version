====== Sentence Level Features ======

1. Sentence: Il permet que l ' agglomération actuelle puisse enfin disposer de liaisons comparables à celles dont Haussmann a doté Paris intra-muros il y a un siècle , sans que , pour cela , l ' agglomération de demain soit condamnée à n ' être que l ' extrapolation de celle d ' aujourd ' hui .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de demain', 'demain']
adverbiaux_localisation_temporelle::2::['de demain', 'demain']
adverbiaux_pointage_non_absolu::3::['de demain', 'demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['de demain']
adverbiaux_loc_temp_pointage_non_absolu::2::['de demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de demain', 'demain']
adverbiaux_localisation_temporelle::2::['de demain', 'demain']
adverbiaux_pointage_non_absolu::3::['de demain', 'demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['de demain']
adverbiaux_loc_temp_pointage_non_absolu::2::['de demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']
Total feature counts: 11

