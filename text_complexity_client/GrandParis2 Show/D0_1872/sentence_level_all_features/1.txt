
********** Entity Names Features **********
*******************************************

1. Sentence: Paris doit conserver ce qui , comme ville , constitue depuis des siècles le trait le plus spécifique de son être vivant : la variété des fonctions qui s ' y exercent et la richesse des échanges humains qu ' elle permet .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Paris doit conserver ce qui , comme ville , constitue depuis des siècles le trait le plus spécifique de son être vivant : la variété des fonctions qui s ' y exercent et la richesse des échanges humains qu ' elle permet .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Paris doit conserver ce qui , comme ville , constitue depuis des siècles le trait le plus spécifique de son être vivant : la variété des fonctions qui s ' y exercent et la richesse des échanges humains qu ' elle permet .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ devoir, permettre ]
deontique :: 2 :: [ devoir, permettre ]
marqueurs_niveau3 :: 2 :: [ devoir, permettre ]
deontique_niveau3 :: 2 :: [ devoir, permettre ]
alethique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Paris doit conserver ce qui , comme ville , constitue depuis des siècles le trait le plus spécifique de son être vivant : la variété des fonctions qui s ' y exercent et la richesse des échanges humains qu ' elle permet .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['depuis des siècles']
adverbiaux_duratifs_iteratifs::1::['depuis des siècles']
adverbiaux_pointage_non_absolu::1::['depuis des siècles']
adverbiaux_loc_temp_focalisation_id::1::['depuis des siècles']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis des siècles']
adverbiaux_dur_iter_deictique::1::['depuis des siècles']

