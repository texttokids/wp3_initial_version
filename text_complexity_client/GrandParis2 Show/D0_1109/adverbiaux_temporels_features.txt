====== Sentence Level Features ======

1. Sentence: Il en est de même , à plus forte raison , à l ' échelle de l ' agglomération parisienne toute entière : sans axes de transport nouveaux , il n ' y aurait pas de centres urbains nouveaux , car les liaisons avec le reste de l ' agglomération seraient insuffisantes; et , dans les zones qui se construiront dans les prochaines années , sans axes de transport nouveaux il n ' y aurait pas de villes nouvelles - de vraies villes , par opposition à la succession uniforme de banlieues-dortoirs indifférenciées .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['dans les prochaines années']
adverbiaux_localisation_temporelle::1::['dans les prochaines années']
adverbiaux_pointage_non_absolu::1::['dans les prochaines années']
adverbiaux_loc_temp_focalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_regionalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_pointage_non_absolu::1::['dans les prochaines années']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['dans les prochaines années']
adverbiaux_localisation_temporelle::1::['dans les prochaines années']
adverbiaux_pointage_non_absolu::1::['dans les prochaines années']
adverbiaux_loc_temp_focalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_regionalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_pointage_non_absolu::1::['dans les prochaines années']
Total feature counts: 6

