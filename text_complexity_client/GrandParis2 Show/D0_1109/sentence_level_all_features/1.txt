
********** Entity Names Features **********
*******************************************

1. Sentence: Il en est de même , à plus forte raison , à l ' échelle de l ' agglomération parisienne toute entière : sans axes de transport nouveaux , il n ' y aurait pas de centres urbains nouveaux , car les liaisons avec le reste de l ' agglomération seraient insuffisantes; et , dans les zones qui se construiront dans les prochaines années , sans axes de transport nouveaux il n ' y aurait pas de villes nouvelles - de vraies villes , par opposition à la succession uniforme de banlieues-dortoirs indifférenciées .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il en est de même , à plus forte raison , à l ' échelle de l ' agglomération parisienne toute entière : sans axes de transport nouveaux , il n ' y aurait pas de centres urbains nouveaux , car les liaisons avec le reste de l ' agglomération seraient insuffisantes; et , dans les zones qui se construiront dans les prochaines années , sans axes de transport nouveaux il n ' y aurait pas de villes nouvelles - de vraies villes , par opposition à la succession uniforme de banlieues-dortoirs indifférenciées .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il en est de même , à plus forte raison , à l ' échelle de l ' agglomération parisienne toute entière : sans axes de transport nouveaux , il n ' y aurait pas de centres urbains nouveaux , car les liaisons avec le reste de l ' agglomération seraient insuffisantes; et , dans les zones qui se construiront dans les prochaines années , sans axes de transport nouveaux il n ' y aurait pas de villes nouvelles - de vraies villes , par opposition à la succession uniforme de banlieues-dortoirs indifférenciées .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ fort, vrai ]
alethique :: 1 :: [ vrai ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau1 :: 1 :: [ vrai ]
marqueurs_niveau0 :: 1 :: [ fort ]
alethique_niveau1 :: 1 :: [ vrai ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il en est de même , à plus forte raison , à l ' échelle de l ' agglomération parisienne toute entière : sans axes de transport nouveaux , il n ' y aurait pas de centres urbains nouveaux , car les liaisons avec le reste de l ' agglomération seraient insuffisantes; et , dans les zones qui se construiront dans les prochaines années , sans axes de transport nouveaux il n ' y aurait pas de villes nouvelles - de vraies villes , par opposition à la succession uniforme de banlieues-dortoirs indifférenciées .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['dans les prochaines années']
adverbiaux_localisation_temporelle::1::['dans les prochaines années']
adverbiaux_pointage_non_absolu::1::['dans les prochaines années']
adverbiaux_loc_temp_focalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_regionalisation_id::1::['dans les prochaines années']
adverbiaux_loc_temp_pointage_non_absolu::1::['dans les prochaines années']

