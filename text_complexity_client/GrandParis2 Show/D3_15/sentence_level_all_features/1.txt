
********** Entity Names Features **********
*******************************************

1. Sentence: Cette Cité , c ' est en vérité notre pays tout entier , le territoire de nos valeurs , de nos références , de nos espérances - en un mot , cette Cité c ' est le lieu de notre identité .
Entity Name (Type) :: Cité c (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette Cité , c ' est en vérité notre pays tout entier , le territoire de nos valeurs , de nos références , de nos espérances - en un mot , cette Cité c ' est le lieu de notre identité .
Tokens having emotion: [ espérances ]
Lemmas having emotion: [ espérance ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Cette Cité , c ' est en vérité notre pays tout entier , le territoire de nos valeurs , de nos références , de nos espérances - en un mot , cette Cité c ' est le lieu de notre identité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ vérité ]
alethique :: 1 :: [ vérité ]
marqueurs_niveau1 :: 1 :: [ vérité ]
alethique_niveau1 :: 1 :: [ vérité ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette Cité , c ' est en vérité notre pays tout entier , le territoire de nos valeurs , de nos références , de nos espérances - en un mot , cette Cité c ' est le lieu de notre identité .
No features found.

