
********** Entity Names Features **********
*******************************************

1. Sentence: L ' illustration ancestrale de cette distinction - trop souvent oubliée par suite de l ' imprécision du vocabulaire de cet art encore neuf qu ' est l ' urbanisme - se trouve dans le chef-lieu de canton ou le gros bourg-marché qui n ' a que peu d ' habitants , mais dont chacun sait qu ' il dessert , en outre , un nombre de personnes vivant alentour parfois supérieur à celui de ses propres habitants;
No features found.


********** Emotions Features **********
***************************************

1. Sentence: L ' illustration ancestrale de cette distinction - trop souvent oubliée par suite de l ' imprécision du vocabulaire de cet art encore neuf qu ' est l ' urbanisme - se trouve dans le chef-lieu de canton ou le gros bourg-marché qui n ' a que peu d ' habitants , mais dont chacun sait qu ' il dessert , en outre , un nombre de personnes vivant alentour parfois supérieur à celui de ses propres habitants;
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' illustration ancestrale de cette distinction - trop souvent oubliée par suite de l ' imprécision du vocabulaire de cet art encore neuf qu ' est l ' urbanisme - se trouve dans le chef-lieu de canton ou le gros bourg-marché qui n ' a que peu d ' habitants , mais dont chacun sait qu ' il dessert , en outre , un nombre de personnes vivant alentour parfois supérieur à celui de ses propres habitants;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ souvent, oublier, gros, savoir, personne, parfois ]
alethique :: 3 :: [ souvent, personne, parfois ]
marqueurs_niveau3 :: 3 :: [ souvent, savoir, parfois ]
epistemique :: 2 :: [ gros, savoir ]
marqueurs_niveau2 :: 2 :: [ gros, personne ]
alethique_niveau3 :: 2 :: [ souvent, parfois ]
pas_d_indication :: 1 :: [ oublier ]
marqueurs_niveau0 :: 1 :: [ oublier ]
alethique_niveau2 :: 1 :: [ personne ]
epistemique_niveau3 :: 1 :: [ savoir ]
epistemique_niveau2 :: 1 :: [ gros ]
pas_d_indication_niveau0 :: 1 :: [ oublier ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' illustration ancestrale de cette distinction - trop souvent oubliée par suite de l ' imprécision du vocabulaire de cet art encore neuf qu ' est l ' urbanisme - se trouve dans le chef-lieu de canton ou le gros bourg-marché qui n ' a que peu d ' habitants , mais dont chacun sait qu ' il dessert , en outre , un nombre de personnes vivant alentour parfois supérieur à celui de ses propres habitants;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['trop souvent', 'encore', 'parfois']
adverbiaux_purement_iteratifs::2::['trop souvent', 'parfois']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_iterateur_frequentiel::2::['trop souvent', 'parfois']
adverbiaux_dur_iter_relatif::1::['encore']

