
********** Entity Names Features **********
*******************************************

1. Sentence: Elle deviendrait trop forte et offrirait à ce moment les défauts de la Préfecture de la Seine d ’ être atteinte de gigantisme .
Entity Name (Type) :: Préfecture de la Seine (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Elle deviendrait trop forte et offrirait à ce moment les défauts de la Préfecture de la Seine d ’ être atteinte de gigantisme .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Elle deviendrait trop forte et offrirait à ce moment les défauts de la Préfecture de la Seine d ’ être atteinte de gigantisme .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ fort, offrir ]
appreciatif :: 1 :: [ offrir ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau2 :: 1 :: [ offrir ]
marqueurs_niveau0 :: 1 :: [ fort ]
appreciatif_niveau2 :: 1 :: [ offrir ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Elle deviendrait trop forte et offrirait à ce moment les défauts de la Préfecture de la Seine d ’ être atteinte de gigantisme .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à ce moment']
adverbiaux_localisation_temporelle::1::['à ce moment']
adverbiaux_pointage_non_absolu::2::['à ce moment', 'à ce moment']
adverbiaux_loc_temp_focalisation_id::1::['à ce moment']
adverbiaux_loc_temp_regionalisation_id::1::['à ce moment']
adverbiaux_loc_temp_pointage_non_absolu::1::['à ce moment']
adverbiaux_dur_iter_anaphorique::1::['à ce moment']

