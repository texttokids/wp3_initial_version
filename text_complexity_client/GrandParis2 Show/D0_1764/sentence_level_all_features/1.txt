
********** Entity Names Features **********
*******************************************

1. Sentence: Les activités y sont peu nombreuses , un réseau dense d ' autobus et la ligne de la Bastille transportent chaque jour dans Paris où elle y trouve son emploi la majeure partie de la population .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Les activités y sont peu nombreuses , un réseau dense d ' autobus et la ligne de la Bastille transportent chaque jour dans Paris où elle y trouve son emploi la majeure partie de la population .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les activités y sont peu nombreuses , un réseau dense d ' autobus et la ligne de la Bastille transportent chaque jour dans Paris où elle y trouve son emploi la majeure partie de la population .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ emploi ]
appreciatif :: 1 :: [ emploi ]
marqueurs_niveau2 :: 1 :: [ emploi ]
appreciatif_niveau2 :: 1 :: [ emploi ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les activités y sont peu nombreuses , un réseau dense d ' autobus et la ligne de la Bastille transportent chaque jour dans Paris où elle y trouve son emploi la majeure partie de la population .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['chaque jour']
adverbiaux_purement_iteratifs::1::['chaque jour']
adverbiaux_loc_temp_focalisation_id::1::['chaque jour']
adverbiaux_loc_temp_regionalisation_id::1::['chaque jour']
adverbiaux_iterateur_calendaire::1::['chaque jour']

