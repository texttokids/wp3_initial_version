====== Sentence Level Features ======

1. Sentence: d ' une enquête réalisée par l ' Agence Foncière et Technique de la région parisienne , il ressort que le prix moyen au mètre carré de terrain à bâtir a triplé de 1958 à 1963 , ce qui correspond à une augmentation moyenne de 25 % chaque année ;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1958 à 1963', 'chaque année']
adverbiaux_purement_iteratifs::1::['chaque année']
adverbiaux_duratifs_iteratifs::1::['de 1958 à 1963']
adverbiaux_pointage_absolu::1::['de 1958 à 1963']
adverbiaux_loc_temp_focalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_loc_temp_regionalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_iterateur_calendaire::1::['chaque année']
adverbiaux_dur_iter_absolu::1::['de 1958 à 1963']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1958 à 1963', 'chaque année']
adverbiaux_purement_iteratifs::1::['chaque année']
adverbiaux_duratifs_iteratifs::1::['de 1958 à 1963']
adverbiaux_pointage_absolu::1::['de 1958 à 1963']
adverbiaux_loc_temp_focalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_loc_temp_regionalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_iterateur_calendaire::1::['chaque année']
adverbiaux_dur_iter_absolu::1::['de 1958 à 1963']
Total feature counts: 11

