
********** Entity Names Features **********
*******************************************

1. Sentence: d ' une enquête réalisée par l ' Agence Foncière et Technique de la région parisienne , il ressort que le prix moyen au mètre carré de terrain à bâtir a triplé de 1958 à 1963 , ce qui correspond à une augmentation moyenne de 25 % chaque année ;
Entity Name (Type) :: Agence Foncière et Technique de la région parisienne (ORG)


********** Emotions Features **********
***************************************

1. Sentence: d ' une enquête réalisée par l ' Agence Foncière et Technique de la région parisienne , il ressort que le prix moyen au mètre carré de terrain à bâtir a triplé de 1958 à 1963 , ce qui correspond à une augmentation moyenne de 25 % chaque année ;
No features found.


********** Modality Features **********
***************************************

1. Sentence: d ' une enquête réalisée par l ' Agence Foncière et Technique de la région parisienne , il ressort que le prix moyen au mètre carré de terrain à bâtir a triplé de 1958 à 1963 , ce qui correspond à une augmentation moyenne de 25 % chaque année ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ réaliser, prix ]
alethique :: 1 :: [ réaliser ]
appreciatif :: 1 :: [ prix ]
axiologique :: 1 :: [ prix ]
marqueurs_niveau2 :: 1 :: [ prix ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
alethique_niveau1 :: 1 :: [ réaliser ]
appreciatif_niveau2 :: 1 :: [ prix ]
axiologique_niveau2 :: 1 :: [ prix ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: d ' une enquête réalisée par l ' Agence Foncière et Technique de la région parisienne , il ressort que le prix moyen au mètre carré de terrain à bâtir a triplé de 1958 à 1963 , ce qui correspond à une augmentation moyenne de 25 % chaque année ;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1958 à 1963', 'chaque année']
adverbiaux_purement_iteratifs::1::['chaque année']
adverbiaux_duratifs_iteratifs::1::['de 1958 à 1963']
adverbiaux_pointage_absolu::1::['de 1958 à 1963']
adverbiaux_loc_temp_focalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_loc_temp_regionalisation_id::2::['de 1958 à 1963', 'chaque année']
adverbiaux_iterateur_calendaire::1::['chaque année']
adverbiaux_dur_iter_absolu::1::['de 1958 à 1963']

