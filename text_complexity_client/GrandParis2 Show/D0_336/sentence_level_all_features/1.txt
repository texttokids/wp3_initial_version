
********** Entity Names Features **********
*******************************************

1. Sentence: Tokyo : + 6 , 8 millions en 20 ans (de 19 , 8 à 26 , 6 millions , de 1955 à 1975) .
Entity Name (Type) :: Tokyo (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Tokyo : + 6 , 8 millions en 20 ans (de 19 , 8 à 26 , 6 millions , de 1955 à 1975) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Tokyo : + 6 , 8 millions en 20 ans (de 19 , 8 à 26 , 6 millions , de 1955 à 1975) .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Tokyo : + 6 , 8 millions en 20 ans (de 19 , 8 à 26 , 6 millions , de 1955 à 1975) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1955 à 1975', 'en 20 ans']
adverbiaux_localisation_temporelle::1::['de 1955 à 1975']
adverbiaux_purement_duratifs::1::['en 20 ans']
adverbiaux_pointage_absolu::1::['de 1955 à 1975']
adverbiaux_pointage_non_absolu::1::['en 20 ans']
adverbiaux_loc_temp_focalisation_id::1::['de 1955 à 1975']
adverbiaux_loc_temp_regionalisation_id::1::['de 1955 à 1975']
adverbiaux_loc_temp_pointage_absolu::1::['de 1955 à 1975']
adverbiaux_dur_iter_relatif::1::['en 20 ans']

