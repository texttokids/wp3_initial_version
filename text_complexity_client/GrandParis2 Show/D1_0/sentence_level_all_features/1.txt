
********** Entity Names Features **********
*******************************************

1. Sentence: Le projet présenté à Monsieur le Ministre de l ’ Intérieur , en application de la Loi du 14 Mai 1932 , est destiné à ordonner et à vertébrer l ’ Agglomération Parisienne :  
Circulation - Hygiène - Esthétique sont les objectifs de l ’ Aménagement proposé .
Entity Name (Type) :: Aménagement (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le projet présenté à Monsieur le Ministre de l ’ Intérieur , en application de la Loi du 14 Mai 1932 , est destiné à ordonner et à vertébrer l ’ Agglomération Parisienne :  
Circulation - Hygiène - Esthétique sont les objectifs de l ’ Aménagement proposé .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Le projet présenté à Monsieur le Ministre de l ’ Intérieur , en application de la Loi du 14 Mai 1932 , est destiné à ordonner et à vertébrer l ’ Agglomération Parisienne :  
Circulation - Hygiène - Esthétique sont les objectifs de l ’ Aménagement proposé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ loi, ordonner ]
deontique :: 2 :: [ loi, ordonner ]
marqueurs_niveau3 :: 1 :: [ ordonner ]
marqueurs_niveau2 :: 1 :: [ loi ]
deontique_niveau3 :: 1 :: [ ordonner ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le projet présenté à Monsieur le Ministre de l ’ Intérieur , en application de la Loi du 14 Mai 1932 , est destiné à ordonner et à vertébrer l ’ Agglomération Parisienne :  
Circulation - Hygiène - Esthétique sont les objectifs de l ’ Aménagement proposé .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['du 14 Mai 1932']
adverbiaux_localisation_temporelle::1::['du 14 Mai 1932']
adverbiaux_pointage_absolu::1::['du 14 Mai 1932']
adverbiaux_loc_temp_focalisation_id::1::['du 14 Mai 1932']
adverbiaux_loc_temp_regionalisation_id::1::['du 14 Mai 1932']
adverbiaux_loc_temp_pointage_absolu::1::['du 14 Mai 1932']

