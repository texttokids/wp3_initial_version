
********** Entity Names Features **********
*******************************************

1. Sentence: la boucle de Carrières , en face de Poissy est actuellement peu urbanisée : des champs d ' épandages occupent près de la moitié des terrains de la plaine située au bas des coteaux de Chanteloup dominés par la forêt de l ' Hautil dont l ' altitude - 180 mètres - est l ' une des plus élevées de la région parisienne .
Entity Name (Type) :: forêt de l ' Hautil (LOC)


********** Emotions Features **********
***************************************

1. Sentence: la boucle de Carrières , en face de Poissy est actuellement peu urbanisée : des champs d ' épandages occupent près de la moitié des terrains de la plaine située au bas des coteaux de Chanteloup dominés par la forêt de l ' Hautil dont l ' altitude - 180 mètres - est l ' une des plus élevées de la région parisienne .
No features found.


********** Modality Features **********
***************************************

1. Sentence: la boucle de Carrières , en face de Poissy est actuellement peu urbanisée : des champs d ' épandages occupent près de la moitié des terrains de la plaine située au bas des coteaux de Chanteloup dominés par la forêt de l ' Hautil dont l ' altitude - 180 mètres - est l ' une des plus élevées de la région parisienne .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: la boucle de Carrières , en face de Poissy est actuellement peu urbanisée : des champs d ' épandages occupent près de la moitié des terrains de la plaine située au bas des coteaux de Chanteloup dominés par la forêt de l ' Hautil dont l ' altitude - 180 mètres - est l ' une des plus élevées de la région parisienne .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['actuellement']
adverbiaux_localisation_temporelle::1::['actuellement']
adverbiaux_pointage_non_absolu::2::['actuellement', 'actuellement']
adverbiaux_loc_temp_focalisation_id::1::['actuellement']
adverbiaux_loc_temp_regionalisation_id::1::['actuellement']
adverbiaux_loc_temp_pointage_non_absolu::1::['actuellement']
adverbiaux_dur_iter_deictique::1::['actuellement']

