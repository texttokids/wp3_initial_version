
********** Entity Names Features **********
*******************************************

1. Sentence: Les bois de Boulogne et de Vincennes couvrent chacun 10 km2 (4) , soit ensemble 20 km2 : l ' ensemble de la région comporte donc une superficie boisée cent fois plus grande que ces deux bois donnés il y a un siècle par Napoléon III aux Parisiens , et aménagés par Haussmann aux portes d ' un Paris dont il avait lui-même élargi les limites .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Les bois de Boulogne et de Vincennes couvrent chacun 10 km2 (4) , soit ensemble 20 km2 : l ' ensemble de la région comporte donc une superficie boisée cent fois plus grande que ces deux bois donnés il y a un siècle par Napoléon III aux Parisiens , et aménagés par Haussmann aux portes d ' un Paris dont il avait lui-même élargi les limites .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les bois de Boulogne et de Vincennes couvrent chacun 10 km2 (4) , soit ensemble 20 km2 : l ' ensemble de la région comporte donc une superficie boisée cent fois plus grande que ces deux bois donnés il y a un siècle par Napoléon III aux Parisiens , et aménagés par Haussmann aux portes d ' un Paris dont il avait lui-même élargi les limites .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ grand ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les bois de Boulogne et de Vincennes couvrent chacun 10 km2 (4) , soit ensemble 20 km2 : l ' ensemble de la région comporte donc une superficie boisée cent fois plus grande que ces deux bois donnés il y a un siècle par Napoléon III aux Parisiens , et aménagés par Haussmann aux portes d ' un Paris dont il avait lui-même élargi les limites .
No features found.

