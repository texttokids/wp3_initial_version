
********** Entity Names Features **********
*******************************************

1. Sentence: Et l ’ important était d ’ assister à ce Comité interministériel pour essayer que le compte-rendu fait par le Secrétaire Général du Gouvernement comporte des précisions suffisantes pour que l ’ on puisse espérer un commencement d ’ exécution .
Entity Name (Type) :: Secrétaire Général du Gouvernement (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Et l ’ important était d ’ assister à ce Comité interministériel pour essayer que le compte-rendu fait par le Secrétaire Général du Gouvernement comporte des précisions suffisantes pour que l ’ on puisse espérer un commencement d ’ exécution .
Tokens having emotion: [ suffisantes, espérer ]
Lemmas having emotion: [ suffisant, espérer ]
Categories of the emotion lemmas: [ orgueil, desir ]


********** Modality Features **********
***************************************

1. Sentence: Et l ’ important était d ’ assister à ce Comité interministériel pour essayer que le compte-rendu fait par le Secrétaire Général du Gouvernement comporte des précisions suffisantes pour que l ’ on puisse espérer un commencement d ’ exécution .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ important, essayer, suffisant, pouvoir, espérer ]
epistemique :: 4 :: [ important, essayer, pouvoir, espérer ]
marqueurs_niveau3 :: 3 :: [ essayer, pouvoir, espérer ]
epistemique_niveau3 :: 3 :: [ essayer, pouvoir, espérer ]
boulique :: 2 :: [ essayer, espérer ]
boulique_niveau3 :: 2 :: [ essayer, espérer ]
alethique :: 1 :: [ pouvoir ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ suffisant ]
marqueurs_niveau1 :: 1 :: [ important ]
marqueurs_niveau0 :: 1 :: [ suffisant ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ suffisant ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Et l ’ important était d ’ assister à ce Comité interministériel pour essayer que le compte-rendu fait par le Secrétaire Général du Gouvernement comporte des précisions suffisantes pour que l ’ on puisse espérer un commencement d ’ exécution .
No features found.

