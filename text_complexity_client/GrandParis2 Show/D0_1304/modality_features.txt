====== Sentence Level Features ======

1. Sentence: Il est souhaitable que sans y être poussés par d ' autres motifs que leur désir de promotion professionnelle , d ' enrichissement humain , ou de simple curiosité , beaucoup des habitants de cette région aient envie de travailler , d ' étudier ou de se distraire ailleurs que là où ils habitent , de sortir d ' un cadre de vie étriqué et de tirer , par là même , pleinement profit de ce que peut leur offrir l ' une des plus grandes métropoles du monde .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 10 :: [ souhaitable, désir, beaucoup, envie, travailler, tirer, pouvoir, lui, offrir, grand ]
marqueurs_niveau3 :: 6 :: [ souhaitable, désir, envie, pouvoir, lui, grand ]
epistemique :: 4 :: [ beaucoup, pouvoir, lui, grand ]
boulique :: 3 :: [ souhaitable, désir, envie ]
boulique_niveau3 :: 3 :: [ souhaitable, désir, envie ]
epistemique_niveau3 :: 3 :: [ pouvoir, lui, grand ]
alethique :: 2 :: [ pouvoir, lui ]
appreciatif :: 2 :: [ travailler, offrir ]
marqueurs_niveau2 :: 2 :: [ travailler, offrir ]
alethique_niveau3 :: 2 :: [ pouvoir, lui ]
appreciatif_niveau2 :: 2 :: [ travailler, offrir ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ tirer ]
marqueurs_niveau1 :: 1 :: [ beaucoup ]
marqueurs_niveau0 :: 1 :: [ tirer ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
pas_d_indication_niveau0 :: 1 :: [ tirer ]
Total feature counts: 46


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 10 :: [ souhaitable, désir, beaucoup, envie, travailler, tirer, pouvoir, lui, offrir, grand ]
marqueurs_niveau3 :: 6 :: [ souhaitable, désir, envie, pouvoir, lui, grand ]
epistemique :: 4 :: [ beaucoup, pouvoir, lui, grand ]
boulique :: 3 :: [ souhaitable, désir, envie ]
boulique_niveau3 :: 3 :: [ souhaitable, désir, envie ]
epistemique_niveau3 :: 3 :: [ pouvoir, lui, grand ]
alethique :: 2 :: [ pouvoir, lui ]
appreciatif :: 2 :: [ travailler, offrir ]
marqueurs_niveau2 :: 2 :: [ travailler, offrir ]
alethique_niveau3 :: 2 :: [ pouvoir, lui ]
appreciatif_niveau2 :: 2 :: [ travailler, offrir ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ tirer ]
marqueurs_niveau1 :: 1 :: [ beaucoup ]
marqueurs_niveau0 :: 1 :: [ tirer ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
pas_d_indication_niveau0 :: 1 :: [ tirer ]
Total feature counts: 46

