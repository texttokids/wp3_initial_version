
********** Entity Names Features **********
*******************************************

1. Sentence: Toute l ' histoire de l ' art vrai , de l ' art le plus moderne à l ' art le plus ancien , témoigne que ce qui est beau peut être fonctionnel (2); et ce serait une vue d ' esthète , ignorant l ' état de l ' habitat parisien , la tension des horaires , la fatigue de chaque soir , que de négliger toutes les possibilités d ' alléger la peine des hommes et des femmes qui vivent en région de Paris .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Toute l ' histoire de l ' art vrai , de l ' art le plus moderne à l ' art le plus ancien , témoigne que ce qui est beau peut être fonctionnel (2); et ce serait une vue d ' esthète , ignorant l ' état de l ' habitat parisien , la tension des horaires , la fatigue de chaque soir , que de négliger toutes les possibilités d ' alléger la peine des hommes et des femmes qui vivent en région de Paris .
Tokens having emotion: [ état, tension, peine ]
Lemmas having emotion: [ état, tension, peine ]
Categories of the emotion lemmas: [ non_specifiee, deplaisir, tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Toute l ' histoire de l ' art vrai , de l ' art le plus moderne à l ' art le plus ancien , témoigne que ce qui est beau peut être fonctionnel (2); et ce serait une vue d ' esthète , ignorant l ' état de l ' habitat parisien , la tension des horaires , la fatigue de chaque soir , que de négliger toutes les possibilités d ' alléger la peine des hommes et des femmes qui vivent en région de Paris .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ vrai, moderne, beau, pouvoir, possibilité, peine ]
alethique :: 3 :: [ vrai, pouvoir, possibilité ]
marqueurs_niveau3 :: 3 :: [ beau, pouvoir, possibilité ]
appreciatif :: 2 :: [ beau, peine ]
alethique_niveau3 :: 2 :: [ pouvoir, possibilité ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ moderne ]
marqueurs_niveau2 :: 1 :: [ peine ]
marqueurs_niveau1 :: 1 :: [ vrai ]
marqueurs_niveau0 :: 1 :: [ moderne ]
alethique_niveau1 :: 1 :: [ vrai ]
appreciatif_niveau3 :: 1 :: [ beau ]
appreciatif_niveau2 :: 1 :: [ peine ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ moderne ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Toute l ' histoire de l ' art vrai , de l ' art le plus moderne à l ' art le plus ancien , témoigne que ce qui est beau peut être fonctionnel (2); et ce serait une vue d ' esthète , ignorant l ' état de l ' habitat parisien , la tension des horaires , la fatigue de chaque soir , que de négliger toutes les possibilités d ' alléger la peine des hommes et des femmes qui vivent en région de Paris .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['chaque soir']
adverbiaux_purement_iteratifs::1::['chaque soir']
adverbiaux_iterateur_calendaire::1::['chaque soir']

