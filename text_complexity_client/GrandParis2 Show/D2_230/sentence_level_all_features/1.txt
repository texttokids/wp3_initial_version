
********** Entity Names Features **********
*******************************************

1. Sentence: Vous savez que par exemple l ’ ingénieur en chef de Seine et Oise a été l ’ auteur de l ’ autoroute du Sud , conçue beaucoup plus petitement que l ’ autoroute de l ’ Ouest , et il s ’ est trouvé aux prises avec des difficultés d ’ exiguïté de crédits et forcé de faire l ’ autoroute du Sud au moment où les grands immeubles d ' habitation allaient barrer la route; et cet homme était très sensibilisé à tout ce qui était un peu grand parce qu ’ il avait reçu trop de coups sur les doigts quand il avait fait l ’ autoroute du Sud ;
Entity Name (Type) :: Sud (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Vous savez que par exemple l ’ ingénieur en chef de Seine et Oise a été l ’ auteur de l ’ autoroute du Sud , conçue beaucoup plus petitement que l ’ autoroute de l ’ Ouest , et il s ’ est trouvé aux prises avec des difficultés d ’ exiguïté de crédits et forcé de faire l ’ autoroute du Sud au moment où les grands immeubles d ' habitation allaient barrer la route; et cet homme était très sensibilisé à tout ce qui était un peu grand parce qu ’ il avait reçu trop de coups sur les doigts quand il avait fait l ’ autoroute du Sud ;
No features found.


********** Modality Features **********
***************************************

1. Sentence: Vous savez que par exemple l ’ ingénieur en chef de Seine et Oise a été l ’ auteur de l ’ autoroute du Sud , conçue beaucoup plus petitement que l ’ autoroute de l ’ Ouest , et il s ’ est trouvé aux prises avec des difficultés d ’ exiguïté de crédits et forcé de faire l ’ autoroute du Sud au moment où les grands immeubles d ' habitation allaient barrer la route; et cet homme était très sensibilisé à tout ce qui était un peu grand parce qu ’ il avait reçu trop de coups sur les doigts quand il avait fait l ’ autoroute du Sud ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ savoir, beaucoup, difficulté, forcer, grand, aller, grand ]
epistemique :: 4 :: [ savoir, beaucoup, grand, grand ]
marqueurs_niveau3 :: 3 :: [ savoir, grand, grand ]
epistemique_niveau3 :: 3 :: [ savoir, grand, grand ]
pas_d_indication :: 2 :: [ difficulté, forcer ]
marqueurs_niveau1 :: 2 :: [ beaucoup, aller ]
marqueurs_niveau0 :: 2 :: [ difficulté, forcer ]
pas_d_indication_niveau0 :: 2 :: [ difficulté, forcer ]
alethique :: 1 :: [ aller ]
alethique_niveau1 :: 1 :: [ aller ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Vous savez que par exemple l ’ ingénieur en chef de Seine et Oise a été l ’ auteur de l ’ autoroute du Sud , conçue beaucoup plus petitement que l ’ autoroute de l ’ Ouest , et il s ’ est trouvé aux prises avec des difficultés d ’ exiguïté de crédits et forcé de faire l ’ autoroute du Sud au moment où les grands immeubles d ' habitation allaient barrer la route; et cet homme était très sensibilisé à tout ce qui était un peu grand parce qu ’ il avait reçu trop de coups sur les doigts quand il avait fait l ’ autoroute du Sud ;
No features found.

