
********** Entity Names Features **********
*******************************************

1. Sentence: Dans ces zones mixtes , ne seraient tolérés , au milieu des habitations , que les industries susceptibles de ne causer aucune gêne , c ' est à dire de ne répandre aucun bruit , aucune fumée , aucune odeur .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Dans ces zones mixtes , ne seraient tolérés , au milieu des habitations , que les industries susceptibles de ne causer aucune gêne , c ' est à dire de ne répandre aucun bruit , aucune fumée , aucune odeur .
Tokens having emotion: [ susceptibles ]
Lemmas having emotion: [ susceptible ]
Categories of the emotion lemmas: [ orgueil ]


********** Modality Features **********
***************************************

1. Sentence: Dans ces zones mixtes , ne seraient tolérés , au milieu des habitations , que les industries susceptibles de ne causer aucune gêne , c ' est à dire de ne répandre aucun bruit , aucune fumée , aucune odeur .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ tolérer, aucun, dire, aucun, aucun, aucun ]
marqueurs_niveau3 :: 6 :: [ tolérer, aucun, dire, aucun, aucun, aucun ]
alethique :: 5 :: [ tolérer, aucun, aucun, aucun, aucun ]
alethique_niveau3 :: 5 :: [ tolérer, aucun, aucun, aucun, aucun ]
boulique :: 2 :: [ tolérer, dire ]
boulique_niveau3 :: 2 :: [ tolérer, dire ]
deontique :: 1 :: [ tolérer ]
deontique_niveau3 :: 1 :: [ tolérer ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans ces zones mixtes , ne seraient tolérés , au milieu des habitations , que les industries susceptibles de ne causer aucune gêne , c ' est à dire de ne répandre aucun bruit , aucune fumée , aucune odeur .
No features found.

