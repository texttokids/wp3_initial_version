
********** Entity Names Features **********
*******************************************

1. Sentence: Le sort de Paris intéresse le monde .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le sort de Paris intéresse le monde .
Tokens having emotion: [ intéresse ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Le sort de Paris intéresse le monde .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ intéresser ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le sort de Paris intéresse le monde .
No features found.

