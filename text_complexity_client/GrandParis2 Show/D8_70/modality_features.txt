====== Sentence Level Features ======

1. Sentence: Comme on doit prendre soin d ’ adapter la géographie politique à la géographie démographique et économique toujours mouvante , il conviendrait peut-être de décider que des lois subséquentes ou des décrets en Conseil d ’ État pourraient venir , par une lente progression , agréger , dans l ’ avenir , d ’ autres communes de Seine-et-Oise , peut-être même certaines communes de Seine-et-Marne au Département du Grand Paris .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 11 :: [ devoir, toujours, peut-être, décider, loi, pouvoir, venir, lent, peut-être, certain, grand ]
marqueurs_niveau3 :: 9 :: [ devoir, toujours, peut-être, décider, pouvoir, venir, peut-être, certain, grand ]
epistemique :: 8 :: [ devoir, peut-être, décider, pouvoir, lent, peut-être, certain, grand ]
epistemique_niveau3 :: 7 :: [ devoir, peut-être, décider, pouvoir, peut-être, certain, grand ]
alethique :: 4 :: [ devoir, toujours, pouvoir, venir ]
alethique_niveau3 :: 4 :: [ devoir, toujours, pouvoir, venir ]
deontique :: 3 :: [ devoir, loi, pouvoir ]
marqueurs_niveau2 :: 2 :: [ loi, lent ]
deontique_niveau3 :: 2 :: [ devoir, pouvoir ]
boulique :: 1 :: [ décider ]
boulique_niveau3 :: 1 :: [ décider ]
deontique_niveau2 :: 1 :: [ loi ]
epistemique_niveau2 :: 1 :: [ lent ]
Total feature counts: 54


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 11 :: [ devoir, toujours, peut-être, décider, loi, pouvoir, venir, lent, peut-être, certain, grand ]
marqueurs_niveau3 :: 9 :: [ devoir, toujours, peut-être, décider, pouvoir, venir, peut-être, certain, grand ]
epistemique :: 8 :: [ devoir, peut-être, décider, pouvoir, lent, peut-être, certain, grand ]
epistemique_niveau3 :: 7 :: [ devoir, peut-être, décider, pouvoir, peut-être, certain, grand ]
alethique :: 4 :: [ devoir, toujours, pouvoir, venir ]
alethique_niveau3 :: 4 :: [ devoir, toujours, pouvoir, venir ]
deontique :: 3 :: [ devoir, loi, pouvoir ]
marqueurs_niveau2 :: 2 :: [ loi, lent ]
deontique_niveau3 :: 2 :: [ devoir, pouvoir ]
boulique :: 1 :: [ décider ]
boulique_niveau3 :: 1 :: [ décider ]
deontique_niveau2 :: 1 :: [ loi ]
epistemique_niveau2 :: 1 :: [ lent ]
Total feature counts: 54

