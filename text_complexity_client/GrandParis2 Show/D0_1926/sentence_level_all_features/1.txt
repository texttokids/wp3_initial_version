
********** Entity Names Features **********
*******************************************

1. Sentence: Des exemples montrent en Europe occidentale la prévoyance et le courage manifesté par les édiles pour conserver au cœur de leur ville le cœur des affaires qui en fait une métropole .
Entity Name (Type) :: Europe (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Des exemples montrent en Europe occidentale la prévoyance et le courage manifesté par les édiles pour conserver au cœur de leur ville le cœur des affaires qui en fait une métropole .
Tokens having emotion: [ courage ]
Lemmas having emotion: [ courage ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Des exemples montrent en Europe occidentale la prévoyance et le courage manifesté par les édiles pour conserver au cœur de leur ville le cœur des affaires qui en fait une métropole .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ courage, manifester ]
appreciatif :: 1 :: [ courage ]
axiologique :: 1 :: [ courage ]
pas_d_indication :: 1 :: [ manifester ]
marqueurs_niveau3 :: 1 :: [ courage ]
marqueurs_niveau0 :: 1 :: [ manifester ]
appreciatif_niveau3 :: 1 :: [ courage ]
axiologique_niveau3 :: 1 :: [ courage ]
pas_d_indication_niveau0 :: 1 :: [ manifester ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Des exemples montrent en Europe occidentale la prévoyance et le courage manifesté par les édiles pour conserver au cœur de leur ville le cœur des affaires qui en fait une métropole .
No features found.

