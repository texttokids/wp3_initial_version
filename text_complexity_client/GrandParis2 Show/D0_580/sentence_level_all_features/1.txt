
********** Entity Names Features **********
*******************************************

1. Sentence: La densité résidentielle - la densité à 3 heures du matin , a-t-on pu dire - ne peut pas être l ' unique mesure de l ' occupation du sol , ni le seul critère d ' une bonne organisation urbaine .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La densité résidentielle - la densité à 3 heures du matin , a-t-on pu dire - ne peut pas être l ' unique mesure de l ' occupation du sol , ni le seul critère d ' une bonne organisation urbaine .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La densité résidentielle - la densité à 3 heures du matin , a-t-on pu dire - ne peut pas être l ' unique mesure de l ' occupation du sol , ni le seul critère d ' une bonne organisation urbaine .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ pouvoir, dire, pouvoir, unique, occupation, bon ]
marqueurs_niveau3 :: 4 :: [ pouvoir, dire, pouvoir, bon ]
alethique :: 2 :: [ pouvoir, pouvoir ]
appreciatif :: 2 :: [ unique, bon ]
axiologique :: 2 :: [ occupation, bon ]
deontique :: 2 :: [ pouvoir, pouvoir ]
epistemique :: 2 :: [ pouvoir, pouvoir ]
alethique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
deontique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
boulique :: 1 :: [ dire ]
marqueurs_niveau2 :: 1 :: [ occupation ]
marqueurs_niveau1 :: 1 :: [ unique ]
appreciatif_niveau3 :: 1 :: [ bon ]
appreciatif_niveau1 :: 1 :: [ unique ]
axiologique_niveau3 :: 1 :: [ bon ]
axiologique_niveau2 :: 1 :: [ occupation ]
boulique_niveau3 :: 1 :: [ dire ]
Total feature counts: 34


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La densité résidentielle - la densité à 3 heures du matin , a-t-on pu dire - ne peut pas être l ' unique mesure de l ' occupation du sol , ni le seul critère d ' une bonne organisation urbaine .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à 3 heures du matin']
adverbiaux_localisation_temporelle::1::['à 3 heures du matin']
adverbiaux_pointage_non_absolu::1::['à 3 heures du matin']
adverbiaux_loc_temp_focalisation_id::1::['à 3 heures du matin']
adverbiaux_loc_temp_regionalisation_id::1::['à 3 heures du matin']
adverbiaux_loc_temp_pointage_non_absolu::1::['à 3 heures du matin']

