
********** Entity Names Features **********
*******************************************

1. Sentence: Je souhaite que la nouvelle Cité de l ' Architecture et du Patrimoine nous apporte les harmonies des temps anciens , qu ' elle nous initie à la lecture des monuments contemporains , qu ' elle nourrisse l ' âme des architectes de demain , qu ' elle refasse de l ' architecture un bien commun , et qu ' elle la replace au cœur des choix de société .
Entity Name (Type) :: Cité de l ' Architecture et du Patrimoine (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Je souhaite que la nouvelle Cité de l ' Architecture et du Patrimoine nous apporte les harmonies des temps anciens , qu ' elle nous initie à la lecture des monuments contemporains , qu ' elle nourrisse l ' âme des architectes de demain , qu ' elle refasse de l ' architecture un bien commun , et qu ' elle la replace au cœur des choix de société .
Tokens having emotion: [ souhaite, harmonies ]
Lemmas having emotion: [ souhaiter, harmonie ]
Categories of the emotion lemmas: [ desir, apaisement ]


********** Modality Features **********
***************************************

1. Sentence: Je souhaite que la nouvelle Cité de l ' Architecture et du Patrimoine nous apporte les harmonies des temps anciens , qu ' elle nous initie à la lecture des monuments contemporains , qu ' elle nourrisse l ' âme des architectes de demain , qu ' elle refasse de l ' architecture un bien commun , et qu ' elle la replace au cœur des choix de société .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ bien, choix ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
boulique :: 1 :: [ choix ]
marqueurs_niveau3 :: 1 :: [ bien ]
marqueurs_niveau2 :: 1 :: [ choix ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
boulique_niveau2 :: 1 :: [ choix ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je souhaite que la nouvelle Cité de l ' Architecture et du Patrimoine nous apporte les harmonies des temps anciens , qu ' elle nous initie à la lecture des monuments contemporains , qu ' elle nourrisse l ' âme des architectes de demain , qu ' elle refasse de l ' architecture un bien commun , et qu ' elle la replace au cœur des choix de société .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['des temps', 'de demain', 'demain']
adverbiaux_localisation_temporelle::3::['des temps', 'de demain', 'demain']
adverbiaux_pointage_non_absolu::4::['des temps', 'de demain', 'demain', 'demain']
adverbiaux_loc_temp_focalisation_id::1::['des temps']
adverbiaux_loc_temp_regionalisation_id::2::['des temps', 'de demain']
adverbiaux_loc_temp_pointage_non_absolu::3::['des temps', 'de demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']

