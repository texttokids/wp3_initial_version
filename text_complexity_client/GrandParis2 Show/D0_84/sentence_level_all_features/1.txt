
********** Entity Names Features **********
*******************************************

1. Sentence: Aussi , avant de préciser les objectifs de l ' aménagement et de l ' urbanisme en région de Paris , puis la portée de la carte , qui inscrit , comme schéma directeur , les grandes orientations du futur , est-il nécessaire de chiffrer l ' évolution prévisible de l ' économie et de la démographie de cette région au cours des prochaines décennies .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Aussi , avant de préciser les objectifs de l ' aménagement et de l ' urbanisme en région de Paris , puis la portée de la carte , qui inscrit , comme schéma directeur , les grandes orientations du futur , est-il nécessaire de chiffrer l ' évolution prévisible de l ' économie et de la démographie de cette région au cours des prochaines décennies .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Aussi , avant de préciser les objectifs de l ' aménagement et de l ' urbanisme en région de Paris , puis la portée de la carte , qui inscrit , comme schéma directeur , les grandes orientations du futur , est-il nécessaire de chiffrer l ' évolution prévisible de l ' économie et de la démographie de cette région au cours des prochaines décennies .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ grand, nécessaire ]
marqueurs_niveau3 :: 2 :: [ grand, nécessaire ]
alethique :: 1 :: [ nécessaire ]
epistemique :: 1 :: [ grand ]
alethique_niveau3 :: 1 :: [ nécessaire ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Aussi , avant de préciser les objectifs de l ' aménagement et de l ' urbanisme en région de Paris , puis la portée de la carte , qui inscrit , comme schéma directeur , les grandes orientations du futur , est-il nécessaire de chiffrer l ' évolution prévisible de l ' économie et de la démographie de cette région au cours des prochaines décennies .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['au cours des prochaines décennies']
adverbiaux_localisation_temporelle::1::['au cours des prochaines décennies']
adverbiaux_pointage_non_absolu::1::['au cours des prochaines décennies']
adverbiaux_loc_temp_focalisation_id::1::['au cours des prochaines décennies']
adverbiaux_loc_temp_regionalisation_id::1::['au cours des prochaines décennies']
adverbiaux_loc_temp_pointage_non_absolu::1::['au cours des prochaines décennies']

