====== Sentence Level Features ======

1. Sentence: Ce double cheminement (2) , du présent schéma directeur aux schémas des structures et aux plans d ' urbanisme commun aux ou intercommunaux d ' une part , aux objectifs 1975 d ' équipement et aux tranches régionales des plans de cinq ans d ' autre part , correspond à la prise de conscience que les deux éléments qui jusqu ' à présent existaient " en bout de chaîne " - plans d ' urbanisme communaux ou intercommunaux et plans d ' équipement de 5 ans - devaient être éclairés par une prévision commune , qui ne pouvait se situer que loin en avant , et pour l ' ensemble de la région : cette prévision , c ' est le présent schéma directeur d ' aménagement et d ' urbanisme (1) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à présent']
adverbiaux_localisation_temporelle::1::['à présent']
adverbiaux_pointage_non_absolu::2::['à présent', 'à présent']
adverbiaux_loc_temp_focalisation_id::1::['à présent']
adverbiaux_loc_temp_regionalisation_id::1::['à présent']
adverbiaux_loc_temp_pointage_non_absolu::1::['à présent']
adverbiaux_dur_iter_deictique::1::['à présent']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à présent']
adverbiaux_localisation_temporelle::1::['à présent']
adverbiaux_pointage_non_absolu::2::['à présent', 'à présent']
adverbiaux_loc_temp_focalisation_id::1::['à présent']
adverbiaux_loc_temp_regionalisation_id::1::['à présent']
adverbiaux_loc_temp_pointage_non_absolu::1::['à présent']
adverbiaux_dur_iter_deictique::1::['à présent']
Total feature counts: 8

