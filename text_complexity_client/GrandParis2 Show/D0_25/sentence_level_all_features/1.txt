
********** Entity Names Features **********
*******************************************

1. Sentence: elle en définissait ainsi la mission (art . 54) :  " Pour la coordination et l ' établissement des projets mentionnés à l ' article précédent (coordination du projet d ' aménagement de la ville de Paris et du projet régional approuvé , et établissement du projet régional étendu à toute la région parisienne) , il est institué un service technique qui relève directement du Délégué général à l ' équipement national , et auquel les divers services de l ' État des départements et des communes intéressés doivent prêter leur concours " .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: elle en définissait ainsi la mission (art . 54) :  " Pour la coordination et l ' établissement des projets mentionnés à l ' article précédent (coordination du projet d ' aménagement de la ville de Paris et du projet régional approuvé , et établissement du projet régional étendu à toute la région parisienne) , il est institué un service technique qui relève directement du Délégué général à l ' équipement national , et auquel les divers services de l ' État des départements et des communes intéressés doivent prêter leur concours " .
Tokens having emotion: [ État, intéressés ]
Lemmas having emotion: [ état, intéressé ]
Categories of the emotion lemmas: [ non_specifiee, desir ]


********** Modality Features **********
***************************************

1. Sentence: elle en définissait ainsi la mission (art . 54) :  " Pour la coordination et l ' établissement des projets mentionnés à l ' article précédent (coordination du projet d ' aménagement de la ville de Paris et du projet régional approuvé , et établissement du projet régional étendu à toute la région parisienne) , il est institué un service technique qui relève directement du Délégué général à l ' équipement national , et auquel les divers services de l ' État des départements et des communes intéressés doivent prêter leur concours " .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ approuver, devoir ]
deontique :: 2 :: [ approuver, devoir ]
marqueurs_niveau3 :: 2 :: [ approuver, devoir ]
deontique_niveau3 :: 2 :: [ approuver, devoir ]
alethique :: 1 :: [ devoir ]
axiologique :: 1 :: [ approuver ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
axiologique_niveau3 :: 1 :: [ approuver ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: elle en définissait ainsi la mission (art . 54) :  " Pour la coordination et l ' établissement des projets mentionnés à l ' article précédent (coordination du projet d ' aménagement de la ville de Paris et du projet régional approuvé , et établissement du projet régional étendu à toute la région parisienne) , il est institué un service technique qui relève directement du Délégué général à l ' équipement national , et auquel les divers services de l ' État des départements et des communes intéressés doivent prêter leur concours " .
No features found.

