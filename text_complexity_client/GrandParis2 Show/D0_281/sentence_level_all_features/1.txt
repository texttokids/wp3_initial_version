
********** Entity Names Features **********
*******************************************

1. Sentence: - Paris , 14 millions d ' habitants aux environs de l ' an 2000 : ce n ' est certes pas le souhait des responsables de l ' aménagement de la région parisienne;
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: - Paris , 14 millions d ' habitants aux environs de l ' an 2000 : ce n ' est certes pas le souhait des responsables de l ' aménagement de la région parisienne;
Tokens having emotion: [ souhait ]
Lemmas having emotion: [ souhait ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: - Paris , 14 millions d ' habitants aux environs de l ' an 2000 : ce n ' est certes pas le souhait des responsables de l ' aménagement de la région parisienne;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ souhait, responsable ]
boulique :: 1 :: [ souhait ]
deontique :: 1 :: [ responsable ]
marqueurs_niveau3 :: 1 :: [ souhait ]
marqueurs_niveau2 :: 1 :: [ responsable ]
boulique_niveau3 :: 1 :: [ souhait ]
deontique_niveau2 :: 1 :: [ responsable ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - Paris , 14 millions d ' habitants aux environs de l ' an 2000 : ce n ' est certes pas le souhait des responsables de l ' aménagement de la région parisienne;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de l ` an 2000']
adverbiaux_localisation_temporelle::1::['de l ` an 2000']
adverbiaux_pointage_absolu::1::['de l ` an 2000']
adverbiaux_loc_temp_focalisation_id::1::['de l ` an 2000']
adverbiaux_loc_temp_regionalisation_id::1::['de l ` an 2000']
adverbiaux_loc_temp_pointage_absolu::1::['de l ` an 2000']

