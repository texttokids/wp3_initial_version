
********** Entity Names Features **********
*******************************************

1. Sentence: bien sûr sur l ’ axe principal l ’ effort suffisant devait être fait mais là alors nous commencions à manger la plaine de Beauce ce qui était aussi fort contestable .
Entity Name (Type) :: Beauce (LOC)


********** Emotions Features **********
***************************************

1. Sentence: bien sûr sur l ’ axe principal l ’ effort suffisant devait être fait mais là alors nous commencions à manger la plaine de Beauce ce qui était aussi fort contestable .
Tokens having emotion: [ suffisant ]
Lemmas having emotion: [ suffisant ]
Categories of the emotion lemmas: [ orgueil ]


********** Modality Features **********
***************************************

1. Sentence: bien sûr sur l ’ axe principal l ’ effort suffisant devait être fait mais là alors nous commencions à manger la plaine de Beauce ce qui était aussi fort contestable .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ bien, sûr, suffisant, devoir, fort ]
epistemique :: 2 :: [ sûr, devoir ]
pas_d_indication :: 2 :: [ suffisant, fort ]
marqueurs_niveau3 :: 2 :: [ bien, devoir ]
marqueurs_niveau0 :: 2 :: [ suffisant, fort ]
pas_d_indication_niveau0 :: 2 :: [ suffisant, fort ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
deontique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ sûr ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau2 :: 1 :: [ sûr ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: bien sûr sur l ’ axe principal l ’ effort suffisant devait être fait mais là alors nous commencions à manger la plaine de Beauce ce qui était aussi fort contestable .
No features found.

