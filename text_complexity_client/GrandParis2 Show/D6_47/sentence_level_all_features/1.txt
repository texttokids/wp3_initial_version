
********** Entity Names Features **********
*******************************************

1. Sentence: 3°) les prescriptions du décret du 26 Mars 1852 qui permet aux villes , dans certains cas , d ’ obliger les propriétaires à céder des portions d ’ immeubles , même si elles ne sont pas nécessaires à l ’ assiette de la voie .
Entity Name (Type) :: décret du 26 Mars 1852 (MISC)


********** Emotions Features **********
***************************************

1. Sentence: 3°) les prescriptions du décret du 26 Mars 1852 qui permet aux villes , dans certains cas , d ’ obliger les propriétaires à céder des portions d ’ immeubles , même si elles ne sont pas nécessaires à l ’ assiette de la voie .
No features found.


********** Modality Features **********
***************************************

1. Sentence: 3°) les prescriptions du décret du 26 Mars 1852 qui permet aux villes , dans certains cas , d ’ obliger les propriétaires à céder des portions d ’ immeubles , même si elles ne sont pas nécessaires à l ’ assiette de la voie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ permettre, certain, obliger, céder, nécessaire ]
marqueurs_niveau3 :: 4 :: [ permettre, certain, obliger, nécessaire ]
deontique :: 2 :: [ permettre, obliger ]
epistemique :: 2 :: [ certain, céder ]
deontique_niveau3 :: 2 :: [ permettre, obliger ]
alethique :: 1 :: [ nécessaire ]
boulique :: 1 :: [ céder ]
marqueurs_niveau2 :: 1 :: [ céder ]
alethique_niveau3 :: 1 :: [ nécessaire ]
boulique_niveau2 :: 1 :: [ céder ]
epistemique_niveau3 :: 1 :: [ certain ]
epistemique_niveau2 :: 1 :: [ céder ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: 3°) les prescriptions du décret du 26 Mars 1852 qui permet aux villes , dans certains cas , d ’ obliger les propriétaires à céder des portions d ’ immeubles , même si elles ne sont pas nécessaires à l ’ assiette de la voie .
No features found.

