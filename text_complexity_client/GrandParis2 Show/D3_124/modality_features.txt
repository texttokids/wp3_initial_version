====== Sentence Level Features ======

1. Sentence: A ceux qui s ' interrogeaient pour savoir si c ' était bien la place d ' un Président de la République de parler de l ' architecture , eh bien je veux répondre que si le chef de l ’ État considère l ' architecture comme un sujet secondaire , il ne faudra pas se plaindre que dans cinquante ans il n ' y ait pas les projets d ' aujourd ' hui à montrer .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ savoir, bien, bien, vouloir, considérer, falloir, plaindre ]
marqueurs_niveau3 :: 6 :: [ savoir, bien, bien, vouloir, considérer, falloir ]
appreciatif :: 3 :: [ bien, bien, plaindre ]
axiologique :: 2 :: [ bien, bien ]
epistemique :: 2 :: [ savoir, considérer ]
appreciatif_niveau3 :: 2 :: [ bien, bien ]
axiologique_niveau3 :: 2 :: [ bien, bien ]
epistemique_niveau3 :: 2 :: [ savoir, considérer ]
boulique :: 1 :: [ vouloir ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ plaindre ]
appreciatif_niveau2 :: 1 :: [ plaindre ]
boulique_niveau3 :: 1 :: [ vouloir ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 32


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ savoir, bien, bien, vouloir, considérer, falloir, plaindre ]
marqueurs_niveau3 :: 6 :: [ savoir, bien, bien, vouloir, considérer, falloir ]
appreciatif :: 3 :: [ bien, bien, plaindre ]
axiologique :: 2 :: [ bien, bien ]
epistemique :: 2 :: [ savoir, considérer ]
appreciatif_niveau3 :: 2 :: [ bien, bien ]
axiologique_niveau3 :: 2 :: [ bien, bien ]
epistemique_niveau3 :: 2 :: [ savoir, considérer ]
boulique :: 1 :: [ vouloir ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ plaindre ]
appreciatif_niveau2 :: 1 :: [ plaindre ]
boulique_niveau3 :: 1 :: [ vouloir ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 32

