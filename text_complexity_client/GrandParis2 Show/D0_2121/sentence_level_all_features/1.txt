
********** Entity Names Features **********
*******************************************

1. Sentence: Gêné , gênant , mais indispensable dans l ' agglomération , le transport des marchandises devient , entre Paris et la province , l ' expression la plus tangible des besoins d ' échange entre la région et le reste du territoire national : l ' un des traits dominants des cartes illustrant la circulation des marchandises est l ' importance , quel que soit le mode de transport , du réseau rayonnant depuis Paris , irrigant la France autant qu ' il est alimenté par elle .
Entity Name (Type) :: la France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Gêné , gênant , mais indispensable dans l ' agglomération , le transport des marchandises devient , entre Paris et la province , l ' expression la plus tangible des besoins d ' échange entre la région et le reste du territoire national : l ' un des traits dominants des cartes illustrant la circulation des marchandises est l ' importance , quel que soit le mode de transport , du réseau rayonnant depuis Paris , irrigant la France autant qu ' il est alimenté par elle .
Tokens having emotion: [ expression ]
Lemmas having emotion: [ expression ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Gêné , gênant , mais indispensable dans l ' agglomération , le transport des marchandises devient , entre Paris et la province , l ' expression la plus tangible des besoins d ' échange entre la région et le reste du territoire national : l ' un des traits dominants des cartes illustrant la circulation des marchandises est l ' importance , quel que soit le mode de transport , du réseau rayonnant depuis Paris , irrigant la France autant qu ' il est alimenté par elle .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ gêner, gênant, indispensable, besoin ]
pas_d_indication :: 2 :: [ gêner, gênant ]
marqueurs_niveau3 :: 2 :: [ indispensable, besoin ]
marqueurs_niveau0 :: 2 :: [ gêner, gênant ]
pas_d_indication_niveau0 :: 2 :: [ gêner, gênant ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ indispensable ]
boulique :: 1 :: [ besoin ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau3 :: 1 :: [ indispensable ]
boulique_niveau3 :: 1 :: [ besoin ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Gêné , gênant , mais indispensable dans l ' agglomération , le transport des marchandises devient , entre Paris et la province , l ' expression la plus tangible des besoins d ' échange entre la région et le reste du territoire national : l ' un des traits dominants des cartes illustrant la circulation des marchandises est l ' importance , quel que soit le mode de transport , du réseau rayonnant depuis Paris , irrigant la France autant qu ' il est alimenté par elle .
No features found.

