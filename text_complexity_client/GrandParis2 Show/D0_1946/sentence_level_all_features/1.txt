
********** Entity Names Features **********
*******************************************

1. Sentence: les administrations publiques ou privées manquent de place et certains de leurs employés travaillent dans des conditions étonnantes d ' inconfort mais leurs services mécanographiques occupent en plein cœur de Paris des mètres carrés d ' un prix également étonnant , alors que d ' autres éléments de services centraux , qui , eux , devraient se trouver , près du responsable , ministre ou directeur général , sont contraints de chercher à la périphérie . . .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: les administrations publiques ou privées manquent de place et certains de leurs employés travaillent dans des conditions étonnantes d ' inconfort mais leurs services mécanographiques occupent en plein cœur de Paris des mètres carrés d ' un prix également étonnant , alors que d ' autres éléments de services centraux , qui , eux , devraient se trouver , près du responsable , ministre ou directeur général , sont contraints de chercher à la périphérie . . .
Tokens having emotion: [ étonnantes, étonnant ]
Lemmas having emotion: [ étonnant, étonner ]
Categories of the emotion lemmas: [ surprise, surprise ]


********** Modality Features **********
***************************************

1. Sentence: les administrations publiques ou privées manquent de place et certains de leurs employés travaillent dans des conditions étonnantes d ' inconfort mais leurs services mécanographiques occupent en plein cœur de Paris des mètres carrés d ' un prix également étonnant , alors que d ' autres éléments de services centraux , qui , eux , devraient se trouver , près du responsable , ministre ou directeur général , sont contraints de chercher à la périphérie . . .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ certain, travailler, étonnant, prix, étonner, lui, devoir, responsable, chercher ]
epistemique :: 4 :: [ certain, étonnant, lui, devoir ]
marqueurs_niveau3 :: 4 :: [ certain, lui, devoir, chercher ]
marqueurs_niveau2 :: 3 :: [ travailler, prix, responsable ]
epistemique_niveau3 :: 3 :: [ certain, lui, devoir ]
alethique :: 2 :: [ lui, devoir ]
appreciatif :: 2 :: [ travailler, prix ]
deontique :: 2 :: [ devoir, responsable ]
alethique_niveau3 :: 2 :: [ lui, devoir ]
appreciatif_niveau2 :: 2 :: [ travailler, prix ]
axiologique :: 1 :: [ prix ]
boulique :: 1 :: [ chercher ]
pas_d_indication :: 1 :: [ étonner ]
marqueurs_niveau1 :: 1 :: [ étonnant ]
marqueurs_niveau0 :: 1 :: [ étonner ]
axiologique_niveau2 :: 1 :: [ prix ]
boulique_niveau3 :: 1 :: [ chercher ]
deontique_niveau3 :: 1 :: [ devoir ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau1 :: 1 :: [ étonnant ]
pas_d_indication_niveau0 :: 1 :: [ étonner ]
Total feature counts: 44


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: les administrations publiques ou privées manquent de place et certains de leurs employés travaillent dans des conditions étonnantes d ' inconfort mais leurs services mécanographiques occupent en plein cœur de Paris des mètres carrés d ' un prix également étonnant , alors que d ' autres éléments de services centraux , qui , eux , devraient se trouver , près du responsable , ministre ou directeur général , sont contraints de chercher à la périphérie . . .
No features found.

