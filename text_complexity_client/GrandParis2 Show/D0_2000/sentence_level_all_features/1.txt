
********** Entity Names Features **********
*******************************************

1. Sentence: Ainsi , Paris , allégé des tâches les moins importantes comme les plus encombrantes dans l ' exercice de ses fonctions primordiales , restaurant la beauté de ses quartiers historiques , rejetant le choix romantique et iconoclaste de la table rase , se modernisant dans les quartiers à rénover mais conservant dans l ' ensemble le volume modéré de ses bâtiments , ponctué ici et là , pour accuser la silhouette de la ville , de quelques édifices plus élevés , acceptant à la fois des voies modernes de pénétration et de rocade avec des parcs de stationnement et une plus stricte discipline de ses transports en faveur des transports en commun multipliés en souterrain et en surface ,
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ainsi , Paris , allégé des tâches les moins importantes comme les plus encombrantes dans l ' exercice de ses fonctions primordiales , restaurant la beauté de ses quartiers historiques , rejetant le choix romantique et iconoclaste de la table rase , se modernisant dans les quartiers à rénover mais conservant dans l ' ensemble le volume modéré de ses bâtiments , ponctué ici et là , pour accuser la silhouette de la ville , de quelques édifices plus élevés , acceptant à la fois des voies modernes de pénétration et de rocade avec des parcs de stationnement et une plus stricte discipline de ses transports en faveur des transports en commun multipliés en souterrain et en surface ,
Tokens having emotion: [ romantique ]
Lemmas having emotion: [ romantique ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: Ainsi , Paris , allégé des tâches les moins importantes comme les plus encombrantes dans l ' exercice de ses fonctions primordiales , restaurant la beauté de ses quartiers historiques , rejetant le choix romantique et iconoclaste de la table rase , se modernisant dans les quartiers à rénover mais conservant dans l ' ensemble le volume modéré de ses bâtiments , ponctué ici et là , pour accuser la silhouette de la ville , de quelques édifices plus élevés , acceptant à la fois des voies modernes de pénétration et de rocade avec des parcs de stationnement et une plus stricte discipline de ses transports en faveur des transports en commun multipliés en souterrain et en surface ,
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ important, rejeter, choix, romantique, table, accuser, moderne ]
marqueurs_niveau2 :: 4 :: [ rejeter, choix, romantique, accuser ]
appreciatif :: 2 :: [ important, romantique ]
axiologique :: 2 :: [ important, accuser ]
boulique :: 2 :: [ rejeter, choix ]
boulique_niveau2 :: 2 :: [ rejeter, choix ]
alethique :: 1 :: [ table ]
epistemique :: 1 :: [ important ]
pas_d_indication :: 1 :: [ moderne ]
marqueurs_niveau3 :: 1 :: [ table ]
marqueurs_niveau1 :: 1 :: [ important ]
marqueurs_niveau0 :: 1 :: [ moderne ]
alethique_niveau3 :: 1 :: [ table ]
appreciatif_niveau2 :: 1 :: [ romantique ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau2 :: 1 :: [ accuser ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ moderne ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ainsi , Paris , allégé des tâches les moins importantes comme les plus encombrantes dans l ' exercice de ses fonctions primordiales , restaurant la beauté de ses quartiers historiques , rejetant le choix romantique et iconoclaste de la table rase , se modernisant dans les quartiers à rénover mais conservant dans l ' ensemble le volume modéré de ses bâtiments , ponctué ici et là , pour accuser la silhouette de la ville , de quelques édifices plus élevés , acceptant à la fois des voies modernes de pénétration et de rocade avec des parcs de stationnement et une plus stricte discipline de ses transports en faveur des transports en commun multipliés en souterrain et en surface ,
No features found.

