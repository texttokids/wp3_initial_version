
********** Entity Names Features **********
*******************************************

1. Sentence: Il est intéressant de noter que , dans le monde entier , les perspectives d ' urbanisme établies récemment pour les villes les plus importantes s ' attachent à faire face à des besoins semblables , à partir d ' une approche comparable :
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il est intéressant de noter que , dans le monde entier , les perspectives d ' urbanisme établies récemment pour les villes les plus importantes s ' attachent à faire face à des besoins semblables , à partir d ' une approche comparable :
Tokens having emotion: [ intéressant ]
Lemmas having emotion: [ intéressant ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Il est intéressant de noter que , dans le monde entier , les perspectives d ' urbanisme établies récemment pour les villes les plus importantes s ' attachent à faire face à des besoins semblables , à partir d ' une approche comparable :
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ important, besoin, partir ]
alethique :: 2 :: [ besoin, partir ]
marqueurs_niveau1 :: 2 :: [ important, partir ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
boulique :: 1 :: [ besoin ]
epistemique :: 1 :: [ important ]
marqueurs_niveau3 :: 1 :: [ besoin ]
alethique_niveau3 :: 1 :: [ besoin ]
alethique_niveau1 :: 1 :: [ partir ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il est intéressant de noter que , dans le monde entier , les perspectives d ' urbanisme établies récemment pour les villes les plus importantes s ' attachent à faire face à des besoins semblables , à partir d ' une approche comparable :
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['récemment']
adverbiaux_duratifs_iteratifs::1::['récemment']
adverbiaux_loc_temp_regionalisation_id::1::['récemment']

