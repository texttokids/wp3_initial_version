
********** Entity Names Features **********
*******************************************

1. Sentence: Il convient maintenant d ' esquisser comment , dans le cadre général qui vient d ' être rappelé , ces aménagements auront ainsi une possibilité d ' être réalisés : forêts et plans d ' eau , qui constituent les sites les plus précieux des loisirs de plein air , puis les petites vallées , dont l ' une des vocations est l ' accueil des résidences secondaires;
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il convient maintenant d ' esquisser comment , dans le cadre général qui vient d ' être rappelé , ces aménagements auront ainsi une possibilité d ' être réalisés : forêts et plans d ' eau , qui constituent les sites les plus précieux des loisirs de plein air , puis les petites vallées , dont l ' une des vocations est l ' accueil des résidences secondaires;
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il convient maintenant d ' esquisser comment , dans le cadre général qui vient d ' être rappelé , ces aménagements auront ainsi une possibilité d ' être réalisés : forêts et plans d ' eau , qui constituent les sites les plus précieux des loisirs de plein air , puis les petites vallées , dont l ' une des vocations est l ' accueil des résidences secondaires;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ venir, possibilité, réaliser, précieux, loisir, petit ]
alethique :: 3 :: [ venir, possibilité, réaliser ]
marqueurs_niveau3 :: 3 :: [ venir, possibilité, petit ]
appreciatif :: 2 :: [ précieux, loisir ]
marqueurs_niveau2 :: 2 :: [ précieux, loisir ]
alethique_niveau3 :: 2 :: [ venir, possibilité ]
appreciatif_niveau2 :: 2 :: [ précieux, loisir ]
epistemique :: 1 :: [ petit ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
alethique_niveau1 :: 1 :: [ réaliser ]
epistemique_niveau3 :: 1 :: [ petit ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il convient maintenant d ' esquisser comment , dans le cadre général qui vient d ' être rappelé , ces aménagements auront ainsi une possibilité d ' être réalisés : forêts et plans d ' eau , qui constituent les sites les plus précieux des loisirs de plein air , puis les petites vallées , dont l ' une des vocations est l ' accueil des résidences secondaires;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['maintenant']
adverbiaux_localisation_temporelle::1::['maintenant']
adverbiaux_pointage_non_absolu::1::['maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::1::['maintenant']
adverbiaux_loc_temp_pointage_non_absolu::1::['maintenant']

