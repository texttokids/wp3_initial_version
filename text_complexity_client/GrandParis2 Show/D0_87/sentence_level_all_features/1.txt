
********** Entity Names Features **********
*******************************************

1. Sentence: - Le pouvoir d ' achat des années 1960-1965 sera , en moyenne par personne , multiplié par 2 , 5 vers 1985 , et par 3 , 5 environ , vers 2000 .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: - Le pouvoir d ' achat des années 1960-1965 sera , en moyenne par personne , multiplié par 2 , 5 vers 1985 , et par 3 , 5 environ , vers 2000 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: - Le pouvoir d ' achat des années 1960-1965 sera , en moyenne par personne , multiplié par 2 , 5 vers 1985 , et par 3 , 5 environ , vers 2000 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ pouvoir, personne ]
alethique :: 2 :: [ pouvoir, personne ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ personne ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau2 :: 1 :: [ personne ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - Le pouvoir d ' achat des années 1960-1965 sera , en moyenne par personne , multiplié par 2 , 5 vers 1985 , et par 3 , 5 environ , vers 2000 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['des années 1960_1965', 'vers 1985', 'vers 2000']
adverbiaux_localisation_temporelle::2::['vers 1985', 'vers 2000']
adverbiaux_pointage_absolu::2::['vers 1985', 'vers 2000']
adverbiaux_loc_temp_focalisation_id::3::['des années 1960_1965', 'vers 1985', 'vers 2000']
adverbiaux_loc_temp_regionalisation_id::1::['des années 1960_1965']
adverbiaux_loc_temp_regionalisation_non_id::2::['vers 1985', 'vers 2000']
adverbiaux_loc_temp_pointage_absolu::2::['vers 1985', 'vers 2000']

