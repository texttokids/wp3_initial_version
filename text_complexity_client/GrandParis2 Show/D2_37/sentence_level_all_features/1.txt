
********** Entity Names Features **********
*******************************************

1. Sentence: les gens que l ’ on convoque à des réunions sont toujours très heureux .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: les gens que l ’ on convoque à des réunions sont toujours très heureux .
Tokens having emotion: [ heureux ]
Lemmas having emotion: [ heureux ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: les gens que l ’ on convoque à des réunions sont toujours très heureux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ convoquer, toujours, heureux ]
marqueurs_niveau3 :: 2 :: [ toujours, heureux ]
alethique :: 1 :: [ toujours ]
appreciatif :: 1 :: [ heureux ]
deontique :: 1 :: [ convoquer ]
marqueurs_niveau2 :: 1 :: [ convoquer ]
alethique_niveau3 :: 1 :: [ toujours ]
appreciatif_niveau3 :: 1 :: [ heureux ]
deontique_niveau2 :: 1 :: [ convoquer ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: les gens que l ’ on convoque à des réunions sont toujours très heureux .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

