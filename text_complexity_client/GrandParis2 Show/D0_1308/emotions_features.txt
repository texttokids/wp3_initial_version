====== Sentence Level Features ======

1. Sentence: Parce que cette région est tout entière ordonnée à la vie de l ' agglomération de Paris - même dans ses parties rurales - parce que s ' estompe continûment la différence des modes de vie entre ville et campagne , et que , si peu d ' agriculteurs logent aujourd ' hui en ville , de plus en plus de citadins habitent les villages , un terme nouveau s ' impose pour désigner cette unité nouvelle : à ceux , parfois utilisés à l ' étranger , de " mégalopole " , de " conurbation " , ou de " nébuleuse " , est apparu préférable celui de " région urbaine " .
No features found.


====== Text Level Features ======

No features found.

