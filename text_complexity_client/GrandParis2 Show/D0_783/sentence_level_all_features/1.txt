
********** Entity Names Features **********
*******************************************

1. Sentence: Les grands ensembles construits depuis la guerre en banlieue , et les critiques qui se sont concentrées sur leurs déficiences (3) , ont fait oublier les conditions lamentables dans lesquelles , au cours des années qui ont suivi la première guerre mondiale , les lotissements ont submergé les environs , boisés ou cultivés , de l ' agglomération d ' alors .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les grands ensembles construits depuis la guerre en banlieue , et les critiques qui se sont concentrées sur leurs déficiences (3) , ont fait oublier les conditions lamentables dans lesquelles , au cours des années qui ont suivi la première guerre mondiale , les lotissements ont submergé les environs , boisés ou cultivés , de l ' agglomération d ' alors .
Tokens having emotion: [ critiques, lamentables ]
Lemmas having emotion: [ critique, lamentable ]
Categories of the emotion lemmas: [ mepris, mepris ]


********** Modality Features **********
***************************************

1. Sentence: Les grands ensembles construits depuis la guerre en banlieue , et les critiques qui se sont concentrées sur leurs déficiences (3) , ont fait oublier les conditions lamentables dans lesquelles , au cours des années qui ont suivi la première guerre mondiale , les lotissements ont submergé les environs , boisés ou cultivés , de l ' agglomération d ' alors .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ grand, guerre, oublier, guerre, submerger ]
marqueurs_niveau3 :: 3 :: [ grand, guerre, guerre ]
axiologique :: 2 :: [ guerre, guerre ]
pas_d_indication :: 2 :: [ oublier, submerger ]
marqueurs_niveau0 :: 2 :: [ oublier, submerger ]
axiologique_niveau3 :: 2 :: [ guerre, guerre ]
pas_d_indication_niveau0 :: 2 :: [ oublier, submerger ]
epistemique :: 1 :: [ grand ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les grands ensembles construits depuis la guerre en banlieue , et les critiques qui se sont concentrées sur leurs déficiences (3) , ont fait oublier les conditions lamentables dans lesquelles , au cours des années qui ont suivi la première guerre mondiale , les lotissements ont submergé les environs , boisés ou cultivés , de l ' agglomération d ' alors .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['au cours des années qui ont suivi']
adverbiaux_loc_temp_focalisation_id::1::['au cours des années qui ont suivi']
adverbiaux_loc_temp_regionalisation_id::1::['au cours des années qui ont suivi']

