
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est cette liberté qui a commencé de transformer les villes , ouvrant à l ' urbanisation les baies que les banlieues filiformes du passé ont laissé subsister entre elles , conquérant , au-delà , les zones rurales pour les peupler de résidences secondaires .
Entity Name (Type) :: C (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est cette liberté qui a commencé de transformer les villes , ouvrant à l ' urbanisation les baies que les banlieues filiformes du passé ont laissé subsister entre elles , conquérant , au-delà , les zones rurales pour les peupler de résidences secondaires .
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' est cette liberté qui a commencé de transformer les villes , ouvrant à l ' urbanisation les baies que les banlieues filiformes du passé ont laissé subsister entre elles , conquérant , au-delà , les zones rurales pour les peupler de résidences secondaires .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ liberté, ouvrir, laisser ]
appreciatif :: 1 :: [ liberté ]
boulique :: 1 :: [ ouvrir ]
deontique :: 1 :: [ laisser ]
marqueurs_niveau3 :: 1 :: [ ouvrir ]
marqueurs_niveau2 :: 1 :: [ laisser ]
marqueurs_niveau1 :: 1 :: [ liberté ]
appreciatif_niveau1 :: 1 :: [ liberté ]
boulique_niveau3 :: 1 :: [ ouvrir ]
deontique_niveau2 :: 1 :: [ laisser ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est cette liberté qui a commencé de transformer les villes , ouvrant à l ' urbanisation les baies que les banlieues filiformes du passé ont laissé subsister entre elles , conquérant , au-delà , les zones rurales pour les peupler de résidences secondaires .
No features found.

