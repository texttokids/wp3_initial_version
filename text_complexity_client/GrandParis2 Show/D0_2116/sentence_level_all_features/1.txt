
********** Entity Names Features **********
*******************************************

1. Sentence: Ces livraisons engendrent une importante circulation de camions qui soulève de difficiles problèmes d ' exploitation , illustrés par les résultats d ' une enquête récente : dans le centre de Paris , 45 % des voitures de livraison sont contraintes de s ' arrêter en stationnement illégal .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ces livraisons engendrent une importante circulation de camions qui soulève de difficiles problèmes d ' exploitation , illustrés par les résultats d ' une enquête récente : dans le centre de Paris , 45 % des voitures de livraison sont contraintes de s ' arrêter en stationnement illégal .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ces livraisons engendrent une importante circulation de camions qui soulève de difficiles problèmes d ' exploitation , illustrés par les résultats d ' une enquête récente : dans le centre de Paris , 45 % des voitures de livraison sont contraintes de s ' arrêter en stationnement illégal .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ important, soulever, difficile, problème, illégal ]
appreciatif :: 3 :: [ important, difficile, problème ]
axiologique :: 2 :: [ important, illégal ]
epistemique :: 2 :: [ important, difficile ]
marqueurs_niveau2 :: 2 :: [ difficile, problème ]
appreciatif_niveau2 :: 2 :: [ difficile, problème ]
pas_d_indication :: 1 :: [ soulever ]
marqueurs_niveau3 :: 1 :: [ illégal ]
marqueurs_niveau1 :: 1 :: [ important ]
marqueurs_niveau0 :: 1 :: [ soulever ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau3 :: 1 :: [ illégal ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau2 :: 1 :: [ difficile ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ soulever ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ces livraisons engendrent une importante circulation de camions qui soulève de difficiles problèmes d ' exploitation , illustrés par les résultats d ' une enquête récente : dans le centre de Paris , 45 % des voitures de livraison sont contraintes de s ' arrêter en stationnement illégal .
No features found.

