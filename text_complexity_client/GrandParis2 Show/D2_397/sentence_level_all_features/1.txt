
********** Entity Names Features **********
*******************************************

1. Sentence: Alors là c ’ est vraiment le combat entre l ’ intérêt général ou régional et l ’ intérêt particulier et local et c ’ est sur Trappes que nous avons les difficultés politiques les plus importantes , parce que nous sommes coincés entre l ’ aérodrome de Toussus-le Noble et celui de Guyancourt dont les tenants sont fort bien placés à Paris , et le Conseiller Général de Saint-Denis qui est porteur de l ’ idée des complexes urbano-ruraux qui voudrait voir atomiser la France en une centaine de villes comme Romorantin .
Entity Name (Type) :: Romorantin (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Alors là c ’ est vraiment le combat entre l ’ intérêt général ou régional et l ’ intérêt particulier et local et c ’ est sur Trappes que nous avons les difficultés politiques les plus importantes , parce que nous sommes coincés entre l ’ aérodrome de Toussus-le Noble et celui de Guyancourt dont les tenants sont fort bien placés à Paris , et le Conseiller Général de Saint-Denis qui est porteur de l ’ idée des complexes urbano-ruraux qui voudrait voir atomiser la France en une centaine de villes comme Romorantin .
Tokens having emotion: [ intérêt, intérêt ]
Lemmas having emotion: [ intérêt, intérêt ]
Categories of the emotion lemmas: [ desir, desir ]


********** Modality Features **********
***************************************

1. Sentence: Alors là c ’ est vraiment le combat entre l ’ intérêt général ou régional et l ’ intérêt particulier et local et c ’ est sur Trappes que nous avons les difficultés politiques les plus importantes , parce que nous sommes coincés entre l ’ aérodrome de Toussus-le Noble et celui de Guyancourt dont les tenants sont fort bien placés à Paris , et le Conseiller Général de Saint-Denis qui est porteur de l ’ idée des complexes urbano-ruraux qui voudrait voir atomiser la France en une centaine de villes comme Romorantin .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ vraiment, combat, intérêt, intérêt, difficulté, important, fort, bien, vouloir ]
axiologique :: 3 :: [ combat, important, bien ]
boulique :: 3 :: [ intérêt, intérêt, vouloir ]
marqueurs_niveau2 :: 3 :: [ combat, intérêt, intérêt ]
appreciatif :: 2 :: [ important, bien ]
pas_d_indication :: 2 :: [ difficulté, fort ]
marqueurs_niveau3 :: 2 :: [ bien, vouloir ]
marqueurs_niveau1 :: 2 :: [ vraiment, important ]
marqueurs_niveau0 :: 2 :: [ difficulté, fort ]
boulique_niveau2 :: 2 :: [ intérêt, intérêt ]
pas_d_indication_niveau0 :: 2 :: [ difficulté, fort ]
alethique :: 1 :: [ vraiment ]
epistemique :: 1 :: [ important ]
alethique_niveau1 :: 1 :: [ vraiment ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau3 :: 1 :: [ bien ]
axiologique_niveau2 :: 1 :: [ combat ]
axiologique_niveau1 :: 1 :: [ important ]
boulique_niveau3 :: 1 :: [ vouloir ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 42


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Alors là c ’ est vraiment le combat entre l ’ intérêt général ou régional et l ’ intérêt particulier et local et c ’ est sur Trappes que nous avons les difficultés politiques les plus importantes , parce que nous sommes coincés entre l ’ aérodrome de Toussus-le Noble et celui de Guyancourt dont les tenants sont fort bien placés à Paris , et le Conseiller Général de Saint-Denis qui est porteur de l ’ idée des complexes urbano-ruraux qui voudrait voir atomiser la France en une centaine de villes comme Romorantin .
No features found.

