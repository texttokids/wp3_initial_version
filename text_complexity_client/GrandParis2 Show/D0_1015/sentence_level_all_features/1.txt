
********** Entity Names Features **********
*******************************************

1. Sentence: Un musée des techniques , un parc de la culture , un musée de l ' art urbain et des villes , constituent de premiers exemples de ce qui pourrait être fait en ce sens , et il est sûr que les initiatives ne manqueront pas , qui meurent aujourd ' hui faute de pouvoir se localiser dans le cœur de Paris , dès qu ' apparaîtra réellement possible , et non plus comme du domaine du rêve , leur implantation dans des centres largement dessinés et accessibles de partout .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Un musée des techniques , un parc de la culture , un musée de l ' art urbain et des villes , constituent de premiers exemples de ce qui pourrait être fait en ce sens , et il est sûr que les initiatives ne manqueront pas , qui meurent aujourd ' hui faute de pouvoir se localiser dans le cœur de Paris , dès qu ' apparaîtra réellement possible , et non plus comme du domaine du rêve , leur implantation dans des centres largement dessinés et accessibles de partout .
Tokens having emotion: [ faute ]
Lemmas having emotion: [ faute ]
Categories of the emotion lemmas: [ culpabilite ]


********** Modality Features **********
***************************************

1. Sentence: Un musée des techniques , un parc de la culture , un musée de l ' art urbain et des villes , constituent de premiers exemples de ce qui pourrait être fait en ce sens , et il est sûr que les initiatives ne manqueront pas , qui meurent aujourd ' hui faute de pouvoir se localiser dans le cœur de Paris , dès qu ' apparaîtra réellement possible , et non plus comme du domaine du rêve , leur implantation dans des centres largement dessinés et accessibles de partout .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 8 :: [ pouvoir, sûr, mourir, faute, pouvoir, possible, rêve, partout ]
alethique :: 5 :: [ pouvoir, pouvoir, possible, rêve, partout ]
marqueurs_niveau2 :: 4 :: [ sûr, mourir, faute, partout ]
appreciatif :: 3 :: [ mourir, faute, rêve ]
epistemique :: 3 :: [ pouvoir, sûr, pouvoir ]
marqueurs_niveau3 :: 3 :: [ pouvoir, pouvoir, possible ]
alethique_niveau3 :: 3 :: [ pouvoir, pouvoir, possible ]
deontique :: 2 :: [ pouvoir, pouvoir ]
appreciatif_niveau2 :: 2 :: [ mourir, faute ]
deontique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
axiologique :: 1 :: [ faute ]
marqueurs_niveau1 :: 1 :: [ rêve ]
alethique_niveau2 :: 1 :: [ partout ]
alethique_niveau1 :: 1 :: [ rêve ]
appreciatif_niveau1 :: 1 :: [ rêve ]
axiologique_niveau2 :: 1 :: [ faute ]
epistemique_niveau2 :: 1 :: [ sûr ]
Total feature counts: 44


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un musée des techniques , un parc de la culture , un musée de l ' art urbain et des villes , constituent de premiers exemples de ce qui pourrait être fait en ce sens , et il est sûr que les initiatives ne manqueront pas , qui meurent aujourd ' hui faute de pouvoir se localiser dans le cœur de Paris , dès qu ' apparaîtra réellement possible , et non plus comme du domaine du rêve , leur implantation dans des centres largement dessinés et accessibles de partout .
No features found.

