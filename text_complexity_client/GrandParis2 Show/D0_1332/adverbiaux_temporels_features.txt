====== Sentence Level Features ======

1. Sentence: La recherche de la beauté , impératif naguère trop oublié de ceux qui modèlent un cadre de vie , impose à " l ' urbanisme conscient " de restaurer la " valeur de site " , c ' est-à-dire de se préoccuper à nouveau des sites , non seulement pour préserver ceux qui doivent rester vierges de constructions mais aussi , puisqu ' il faut livrer des terrains à l ' urbanisation , pour choisir les sites où implanter les villes nouvelles et leurs zones d ' accompagnement .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à nouveau']
adverbiaux_purement_iteratifs::1::['à nouveau']
adverbiaux_iterateur_quantificationnel::1::['à nouveau']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à nouveau']
adverbiaux_purement_iteratifs::1::['à nouveau']
adverbiaux_iterateur_quantificationnel::1::['à nouveau']
Total feature counts: 3

