====== Sentence Level Features ======

1. Sentence: 22 du même décret-loi à toutes autres régions que la région parisienne - ont introduit dans la législation de l ’ urbanisme deux modes de contrainte qui existaient déjà en d ’ autres matières : en effet , d ’ une part , l ’ art . 26 de la loi du 21 Juin 1865 a armé le Gouvernement du droit de constituer d ’ office des associations syndicales , dites forcées , afin d ’ assurer l ’ exécution de travaux , d ' intérêt collectif primordial , visés aux nos 1 , 2 et 3 de l ’ art . 1er de cette loi; d ’ autre part , une loi du 27 nov . 1918 (remplacée par un décret-loi du 30 Oct . 1935 , D . P . 1935 , 4 . 533) a organisé toute une procédure destinée à permettre le remembrement , par association syndicale , des propriétés rurales et une loi du 14 Mars 1919 avait même institué le remembrement d ’ office (mais seulement dans les régions dévastées par la guerre ) d ' immeubles , ou portions d ' immeubles situés tant dans l ’ intérieur des agglomérations qu ' en rase campagne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 10 :: [ loi, droit, dire, travail, intérêt, loi, permettre, loi, dévaster, guerre ]
deontique :: 5 :: [ loi, droit, loi, permettre, loi ]
marqueurs_niveau3 :: 5 :: [ droit, dire, travail, permettre, guerre ]
marqueurs_niveau2 :: 4 :: [ loi, intérêt, loi, loi ]
deontique_niveau2 :: 3 :: [ loi, loi, loi ]
appreciatif :: 2 :: [ travail, dévaster ]
axiologique :: 2 :: [ dévaster, guerre ]
boulique :: 2 :: [ dire, intérêt ]
deontique_niveau3 :: 2 :: [ droit, permettre ]
marqueurs_niveau1 :: 1 :: [ dévaster ]
appreciatif_niveau3 :: 1 :: [ travail ]
appreciatif_niveau1 :: 1 :: [ dévaster ]
axiologique_niveau3 :: 1 :: [ guerre ]
axiologique_niveau1 :: 1 :: [ dévaster ]
boulique_niveau3 :: 1 :: [ dire ]
boulique_niveau2 :: 1 :: [ intérêt ]
Total feature counts: 42


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 10 :: [ loi, droit, dire, travail, intérêt, loi, permettre, loi, dévaster, guerre ]
deontique :: 5 :: [ loi, droit, loi, permettre, loi ]
marqueurs_niveau3 :: 5 :: [ droit, dire, travail, permettre, guerre ]
marqueurs_niveau2 :: 4 :: [ loi, intérêt, loi, loi ]
deontique_niveau2 :: 3 :: [ loi, loi, loi ]
appreciatif :: 2 :: [ travail, dévaster ]
axiologique :: 2 :: [ dévaster, guerre ]
boulique :: 2 :: [ dire, intérêt ]
deontique_niveau3 :: 2 :: [ droit, permettre ]
marqueurs_niveau1 :: 1 :: [ dévaster ]
appreciatif_niveau3 :: 1 :: [ travail ]
appreciatif_niveau1 :: 1 :: [ dévaster ]
axiologique_niveau3 :: 1 :: [ guerre ]
axiologique_niveau1 :: 1 :: [ dévaster ]
boulique_niveau3 :: 1 :: [ dire ]
boulique_niveau2 :: 1 :: [ intérêt ]
Total feature counts: 42

