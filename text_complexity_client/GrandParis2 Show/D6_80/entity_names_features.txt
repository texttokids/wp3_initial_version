====== Sentence Level Features ======

1. Sentence: 22 du même décret-loi à toutes autres régions que la région parisienne - ont introduit dans la législation de l ’ urbanisme deux modes de contrainte qui existaient déjà en d ’ autres matières : en effet , d ’ une part , l ’ art . 26 de la loi du 21 Juin 1865 a armé le Gouvernement du droit de constituer d ’ office des associations syndicales , dites forcées , afin d ’ assurer l ’ exécution de travaux , d ' intérêt collectif primordial , visés aux nos 1 , 2 et 3 de l ’ art . 1er de cette loi; d ’ autre part , une loi du 27 nov . 1918 (remplacée par un décret-loi du 30 Oct . 1935 , D . P . 1935 , 4 . 533) a organisé toute une procédure destinée à permettre le remembrement , par association syndicale , des propriétés rurales et une loi du 14 Mars 1919 avait même institué le remembrement d ’ office (mais seulement dans les régions dévastées par la guerre ) d ' immeubles , ou portions d ' immeubles situés tant dans l ’ intérieur des agglomérations qu ' en rase campagne .
Entity Name (Type) :: Mars 1919 (MISC)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
loi du 21 Juin 1865 (MISC)
Gouvernement du droit (ORG)
Oct (LOC)
Mars 1919 (MISC)
Total entitiy counts: 4

