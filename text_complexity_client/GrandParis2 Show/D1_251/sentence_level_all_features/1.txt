
********** Entity Names Features **********
*******************************************

1. Sentence: La perspective que l ’ on découvre de l‘emplacement de l ' ancien château de Marly , conformément au vœu émis par la Commission départementale des Sites et Monuments Naturels , (Seine et Oise) prescrit des servitudes très strictes concernant les terrains constituant les abords du parc de Marly .
Entity Name (Type) :: parc de Marly (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La perspective que l ’ on découvre de l‘emplacement de l ' ancien château de Marly , conformément au vœu émis par la Commission départementale des Sites et Monuments Naturels , (Seine et Oise) prescrit des servitudes très strictes concernant les terrains constituant les abords du parc de Marly .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La perspective que l ’ on découvre de l‘emplacement de l ' ancien château de Marly , conformément au vœu émis par la Commission départementale des Sites et Monuments Naturels , (Seine et Oise) prescrit des servitudes très strictes concernant les terrains constituant les abords du parc de Marly .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ découvrir, château ]
appreciatif :: 1 :: [ château ]
epistemique :: 1 :: [ château ]
pas_d_indication :: 1 :: [ découvrir ]
marqueurs_niveau3 :: 1 :: [ château ]
marqueurs_niveau0 :: 1 :: [ découvrir ]
appreciatif_niveau3 :: 1 :: [ château ]
epistemique_niveau3 :: 1 :: [ château ]
pas_d_indication_niveau0 :: 1 :: [ découvrir ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La perspective que l ’ on découvre de l‘emplacement de l ' ancien château de Marly , conformément au vœu émis par la Commission départementale des Sites et Monuments Naturels , (Seine et Oise) prescrit des servitudes très strictes concernant les terrains constituant les abords du parc de Marly .
No features found.

