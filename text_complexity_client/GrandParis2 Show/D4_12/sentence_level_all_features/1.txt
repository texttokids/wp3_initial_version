
********** Entity Names Features **********
*******************************************

1. Sentence: A ce moment , la Commune par exemple en France , ayant secoué un peu les esprits d ' autant plus que l ' on a essayé de la biffer , c ’ est le refoulement qui s ’ exerce vis-à-vis de la Commune de 1870 , on a vu apparaître pour la classe bourgeoise une espèce de prise de conscience qu ’ il fallait faire un petit quelque chose , et alors on a vu apparaître les cités ouvrières , des notions telles que la lutte contre le taudis .
Entity Name (Type) :: Commune (MISC)


********** Emotions Features **********
***************************************

1. Sentence: A ce moment , la Commune par exemple en France , ayant secoué un peu les esprits d ' autant plus que l ' on a essayé de la biffer , c ’ est le refoulement qui s ’ exerce vis-à-vis de la Commune de 1870 , on a vu apparaître pour la classe bourgeoise une espèce de prise de conscience qu ’ il fallait faire un petit quelque chose , et alors on a vu apparaître les cités ouvrières , des notions telles que la lutte contre le taudis .
No features found.


********** Modality Features **********
***************************************

1. Sentence: A ce moment , la Commune par exemple en France , ayant secoué un peu les esprits d ' autant plus que l ' on a essayé de la biffer , c ’ est le refoulement qui s ’ exerce vis-à-vis de la Commune de 1870 , on a vu apparaître pour la classe bourgeoise une espèce de prise de conscience qu ’ il fallait faire un petit quelque chose , et alors on a vu apparaître les cités ouvrières , des notions telles que la lutte contre le taudis .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ esprit, essayer, falloir, petit, lutte ]
marqueurs_niveau3 :: 4 :: [ esprit, essayer, falloir, petit ]
epistemique :: 3 :: [ esprit, essayer, petit ]
epistemique_niveau3 :: 3 :: [ esprit, essayer, petit ]
axiologique :: 1 :: [ lutte ]
boulique :: 1 :: [ essayer ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ lutte ]
axiologique_niveau2 :: 1 :: [ lutte ]
boulique_niveau3 :: 1 :: [ essayer ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: A ce moment , la Commune par exemple en France , ayant secoué un peu les esprits d ' autant plus que l ' on a essayé de la biffer , c ’ est le refoulement qui s ’ exerce vis-à-vis de la Commune de 1870 , on a vu apparaître pour la classe bourgeoise une espèce de prise de conscience qu ’ il fallait faire un petit quelque chose , et alors on a vu apparaître les cités ouvrières , des notions telles que la lutte contre le taudis .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['A ce moment', 'de 1870']
adverbiaux_localisation_temporelle::2::['A ce moment', 'de 1870']
adverbiaux_pointage_absolu::1::['de 1870']
adverbiaux_pointage_non_absolu::2::['A ce moment', 'A ce moment']
adverbiaux_loc_temp_focalisation_id::2::['A ce moment', 'de 1870']
adverbiaux_loc_temp_regionalisation_id::2::['A ce moment', 'de 1870']
adverbiaux_loc_temp_pointage_absolu::1::['de 1870']
adverbiaux_loc_temp_pointage_non_absolu::1::['A ce moment']
adverbiaux_dur_iter_anaphorique::1::['A ce moment']

