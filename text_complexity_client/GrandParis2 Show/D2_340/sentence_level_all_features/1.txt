
********** Entity Names Features **********
*******************************************

1. Sentence: et à ce moment le juge foncier est fondé à dire quand vous expropriez : vous n ' avez pas exercé le droit de préemption donc vous trouviez que le terrain valait bien tant .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: et à ce moment le juge foncier est fondé à dire quand vous expropriez : vous n ' avez pas exercé le droit de préemption donc vous trouviez que le terrain valait bien tant .
No features found.


********** Modality Features **********
***************************************

1. Sentence: et à ce moment le juge foncier est fondé à dire quand vous expropriez : vous n ' avez pas exercé le droit de préemption donc vous trouviez que le terrain valait bien tant .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ dire, droit, bien ]
marqueurs_niveau3 :: 3 :: [ dire, droit, bien ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
boulique :: 1 :: [ dire ]
deontique :: 1 :: [ droit ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
boulique_niveau3 :: 1 :: [ dire ]
deontique_niveau3 :: 1 :: [ droit ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: et à ce moment le juge foncier est fondé à dire quand vous expropriez : vous n ' avez pas exercé le droit de préemption donc vous trouviez que le terrain valait bien tant .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à ce moment']
adverbiaux_localisation_temporelle::1::['à ce moment']
adverbiaux_pointage_non_absolu::2::['à ce moment', 'à ce moment']
adverbiaux_loc_temp_focalisation_id::1::['à ce moment']
adverbiaux_loc_temp_regionalisation_id::1::['à ce moment']
adverbiaux_loc_temp_pointage_non_absolu::1::['à ce moment']
adverbiaux_dur_iter_anaphorique::1::['à ce moment']

