
********** Entity Names Features **********
*******************************************

1. Sentence: (1) " Ce n ' est pas seulement à la grandeur de sa nature et de son esprit , mais aussi à l ' aisance de leur approche , à la commodité de leur jouissance , que se reconnaît une grande Patrie .
Entity Name (Type) :: Patrie (MISC)


********** Emotions Features **********
***************************************

1. Sentence: (1) " Ce n ' est pas seulement à la grandeur de sa nature et de son esprit , mais aussi à l ' aisance de leur approche , à la commodité de leur jouissance , que se reconnaît une grande Patrie .
Tokens having emotion: [ aisance, jouissance ]
Lemmas having emotion: [ aisance, jouissance ]
Categories of the emotion lemmas: [ audace, joie ]


********** Modality Features **********
***************************************

1. Sentence: (1) " Ce n ' est pas seulement à la grandeur de sa nature et de son esprit , mais aussi à l ' aisance de leur approche , à la commodité de leur jouissance , que se reconnaît une grande Patrie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ esprit, grand ]
epistemique :: 2 :: [ esprit, grand ]
marqueurs_niveau3 :: 2 :: [ esprit, grand ]
epistemique_niveau3 :: 2 :: [ esprit, grand ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: (1) " Ce n ' est pas seulement à la grandeur de sa nature et de son esprit , mais aussi à l ' aisance de leur approche , à la commodité de leur jouissance , que se reconnaît une grande Patrie .
No features found.

