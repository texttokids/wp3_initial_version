
********** Entity Names Features **********
*******************************************

1. Sentence: Çà et là émergent quelques noyaux plus développés , centres de bourgs autrefois séparés de la capitale et maintenant submergés par l ' afflux des Parisiens .
Entity Name (Type) :: Parisiens (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Çà et là émergent quelques noyaux plus développés , centres de bourgs autrefois séparés de la capitale et maintenant submergés par l ' afflux des Parisiens .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Çà et là émergent quelques noyaux plus développés , centres de bourgs autrefois séparés de la capitale et maintenant submergés par l ' afflux des Parisiens .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ submerger ]
pas_d_indication :: 1 :: [ submerger ]
marqueurs_niveau0 :: 1 :: [ submerger ]
pas_d_indication_niveau0 :: 1 :: [ submerger ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Çà et là émergent quelques noyaux plus développés , centres de bourgs autrefois séparés de la capitale et maintenant submergés par l ' afflux des Parisiens .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['autrefois', 'maintenant']
adverbiaux_localisation_temporelle::2::['autrefois', 'maintenant']
adverbiaux_pointage_non_absolu::2::['autrefois', 'maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::2::['autrefois', 'maintenant']
adverbiaux_loc_temp_pointage_non_absolu::2::['autrefois', 'maintenant']

