====== Sentence Level Features ======

1. Sentence: Puisque l ' agglomération parisienne doit subirune véritable mutation , dans son fonctionnement - par la création de centres urbains nouveaux - comme dans l ' orientation de son développement - selon des axes préférentiels - , le système de transport va lui aussi - et lui d ' abord , faut-il l ' espérer - subir une mutation égale .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ devoir, véritable, selon, aller, lui, lui, falloir, espérer, subir ]
marqueurs_niveau3 :: 6 :: [ devoir, selon, lui, lui, falloir, espérer ]
epistemique :: 5 :: [ devoir, selon, lui, lui, espérer ]
epistemique_niveau3 :: 5 :: [ devoir, selon, lui, lui, espérer ]
alethique :: 4 :: [ devoir, aller, lui, lui ]
alethique_niveau3 :: 3 :: [ devoir, lui, lui ]
boulique :: 2 :: [ espérer, subir ]
deontique :: 2 :: [ devoir, falloir ]
deontique_niveau3 :: 2 :: [ devoir, falloir ]
pas_d_indication :: 1 :: [ véritable ]
marqueurs_niveau2 :: 1 :: [ subir ]
marqueurs_niveau1 :: 1 :: [ aller ]
marqueurs_niveau0 :: 1 :: [ véritable ]
alethique_niveau1 :: 1 :: [ aller ]
boulique_niveau3 :: 1 :: [ espérer ]
boulique_niveau2 :: 1 :: [ subir ]
pas_d_indication_niveau0 :: 1 :: [ véritable ]
Total feature counts: 46


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ devoir, véritable, selon, aller, lui, lui, falloir, espérer, subir ]
marqueurs_niveau3 :: 6 :: [ devoir, selon, lui, lui, falloir, espérer ]
epistemique :: 5 :: [ devoir, selon, lui, lui, espérer ]
epistemique_niveau3 :: 5 :: [ devoir, selon, lui, lui, espérer ]
alethique :: 4 :: [ devoir, aller, lui, lui ]
alethique_niveau3 :: 3 :: [ devoir, lui, lui ]
boulique :: 2 :: [ espérer, subir ]
deontique :: 2 :: [ devoir, falloir ]
deontique_niveau3 :: 2 :: [ devoir, falloir ]
pas_d_indication :: 1 :: [ véritable ]
marqueurs_niveau2 :: 1 :: [ subir ]
marqueurs_niveau1 :: 1 :: [ aller ]
marqueurs_niveau0 :: 1 :: [ véritable ]
alethique_niveau1 :: 1 :: [ aller ]
boulique_niveau3 :: 1 :: [ espérer ]
boulique_niveau2 :: 1 :: [ subir ]
pas_d_indication_niveau0 :: 1 :: [ véritable ]
Total feature counts: 46

