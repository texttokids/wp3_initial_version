
********** Entity Names Features **********
*******************************************

1. Sentence: d ' autre part , de soumettre les constructions projetées à des conditions qui seraient jugées nécessaires dans l ' intérêt de la conservation des sites et des perspectives monumentales .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: d ' autre part , de soumettre les constructions projetées à des conditions qui seraient jugées nécessaires dans l ' intérêt de la conservation des sites et des perspectives monumentales .
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: d ' autre part , de soumettre les constructions projetées à des conditions qui seraient jugées nécessaires dans l ' intérêt de la conservation des sites et des perspectives monumentales .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ juger, nécessaire, intérêt ]
alethique :: 1 :: [ nécessaire ]
boulique :: 1 :: [ intérêt ]
pas_d_indication :: 1 :: [ juger ]
marqueurs_niveau3 :: 1 :: [ nécessaire ]
marqueurs_niveau2 :: 1 :: [ intérêt ]
marqueurs_niveau0 :: 1 :: [ juger ]
alethique_niveau3 :: 1 :: [ nécessaire ]
boulique_niveau2 :: 1 :: [ intérêt ]
pas_d_indication_niveau0 :: 1 :: [ juger ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: d ' autre part , de soumettre les constructions projetées à des conditions qui seraient jugées nécessaires dans l ' intérêt de la conservation des sites et des perspectives monumentales .
No features found.

