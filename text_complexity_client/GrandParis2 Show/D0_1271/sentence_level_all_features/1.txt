
********** Entity Names Features **********
*******************************************

1. Sentence: Mais ces contraintes progressivement desserrées -- et c ' est là l ' un des objectifs majeurs de ce schéma directeur comme des programmes d ' équipement qui le mettront en œuvre - il serait erroné de croire et néfaste d ' espérer que le nombre total des déplacements sera réduit (1) .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Mais ces contraintes progressivement desserrées -- et c ' est là l ' un des objectifs majeurs de ce schéma directeur comme des programmes d ' équipement qui le mettront en œuvre - il serait erroné de croire et néfaste d ' espérer que le nombre total des déplacements sera réduit (1) .
Tokens having emotion: [ espérer ]
Lemmas having emotion: [ espérer ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Mais ces contraintes progressivement desserrées -- et c ' est là l ' un des objectifs majeurs de ce schéma directeur comme des programmes d ' équipement qui le mettront en œuvre - il serait erroné de croire et néfaste d ' espérer que le nombre total des déplacements sera réduit (1) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ croire, néfaste, espérer ]
marqueurs_niveau3 :: 3 :: [ croire, néfaste, espérer ]
epistemique :: 2 :: [ croire, espérer ]
epistemique_niveau3 :: 2 :: [ croire, espérer ]
appreciatif :: 1 :: [ néfaste ]
boulique :: 1 :: [ espérer ]
appreciatif_niveau3 :: 1 :: [ néfaste ]
boulique_niveau3 :: 1 :: [ espérer ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais ces contraintes progressivement desserrées -- et c ' est là l ' un des objectifs majeurs de ce schéma directeur comme des programmes d ' équipement qui le mettront en œuvre - il serait erroné de croire et néfaste d ' espérer que le nombre total des déplacements sera réduit (1) .
No features found.

