====== Sentence Level Features ======

1. Sentence: La progression récente des emplois dans la région de Paris présente cette particularité d ' avoir été globalement plus rapide que celle des emplois provinciaux (pour les emplois non agricoles , de 1954 à 1962 : 11 , 8 % dans la région , 9 , 2 % ailleurs) alors que dans chaque branche d ' activité , prise isolément , la croissance des établissements y a été le plus souvent inférieure à ce qu ' elle était en province .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ emploi, rapide, emploi, emploi, souvent ]
appreciatif :: 3 :: [ emploi, emploi, emploi ]
marqueurs_niveau2 :: 3 :: [ emploi, emploi, emploi ]
appreciatif_niveau2 :: 3 :: [ emploi, emploi, emploi ]
marqueurs_niveau3 :: 2 :: [ rapide, souvent ]
alethique :: 1 :: [ souvent ]
epistemique :: 1 :: [ rapide ]
alethique_niveau3 :: 1 :: [ souvent ]
epistemique_niveau3 :: 1 :: [ rapide ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ emploi, rapide, emploi, emploi, souvent ]
appreciatif :: 3 :: [ emploi, emploi, emploi ]
marqueurs_niveau2 :: 3 :: [ emploi, emploi, emploi ]
appreciatif_niveau2 :: 3 :: [ emploi, emploi, emploi ]
marqueurs_niveau3 :: 2 :: [ rapide, souvent ]
alethique :: 1 :: [ souvent ]
epistemique :: 1 :: [ rapide ]
alethique_niveau3 :: 1 :: [ souvent ]
epistemique_niveau3 :: 1 :: [ rapide ]
Total feature counts: 20

