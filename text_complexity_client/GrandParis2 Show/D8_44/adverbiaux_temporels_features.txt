====== Sentence Level Features ======

1. Sentence: En Mai 1956 , et en vue d ’ alléger le Département de Seine-et-Oise et les communes de Seine-et-Oise des charges financières , jugées excessives , qu ’ occasionne l ’ arrivée en Seine-et-Oise des travailleurs de Paris et de sa banlieue - charges sans contre-partie , faute des mesures de péréquation suffisantes - , il était proposé de désannexer en totalité ou en partie onze cantons de Seine-et-Oise , et de rattacher ces territoires au Département de la Seine (voir Combat du 5 Mai 56) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_localisation_temporelle::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_pointage_absolu::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_pointage_non_absolu::1::['En Mai 1956']
adverbiaux_loc_temp_focalisation_id::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_loc_temp_regionalisation_id::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_loc_temp_pointage_absolu::1::['du 5 Mai 56']
adverbiaux_loc_temp_pointage_non_absolu::1::['En Mai 1956']
adverbiaux_dur_iter_absolu::1::['En Mai 1956']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_localisation_temporelle::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_pointage_absolu::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_pointage_non_absolu::1::['En Mai 1956']
adverbiaux_loc_temp_focalisation_id::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_loc_temp_regionalisation_id::2::['En Mai 1956', 'du 5 Mai 56']
adverbiaux_loc_temp_pointage_absolu::1::['du 5 Mai 56']
adverbiaux_loc_temp_pointage_non_absolu::1::['En Mai 1956']
adverbiaux_dur_iter_absolu::1::['En Mai 1956']
Total feature counts: 14

