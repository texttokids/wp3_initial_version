====== Sentence Level Features ======

1. Sentence: Le schéma directeur marque , sur ces deux points , une rupture avec le passé (2) : les axes d ' urbanisation choisis respectent les forêts existantes; et s ' ils s ' inscrivent le long des vallées , c ' est à une échelle suffisante pour que le bord de l ' eau puisse être réservé dans une large mesure aux loisirs (3) , l ' habitat et les activités économiques étant reportés pour l ' essentiel plus haut , sur les rebords des plateaux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 8 :: [ rupture, choisir, respecter, long, suffisant, pouvoir, loisir, haut ]
marqueurs_niveau2 :: 4 :: [ choisir, respecter, long, loisir ]
pas_d_indication :: 3 :: [ rupture, suffisant, haut ]
marqueurs_niveau0 :: 3 :: [ rupture, suffisant, haut ]
pas_d_indication_niveau0 :: 3 :: [ rupture, suffisant, haut ]
appreciatif :: 2 :: [ respecter, loisir ]
epistemique :: 2 :: [ long, pouvoir ]
appreciatif_niveau2 :: 2 :: [ respecter, loisir ]
alethique :: 1 :: [ pouvoir ]
axiologique :: 1 :: [ respecter ]
boulique :: 1 :: [ choisir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
axiologique_niveau2 :: 1 :: [ respecter ]
boulique_niveau2 :: 1 :: [ choisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 38


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 8 :: [ rupture, choisir, respecter, long, suffisant, pouvoir, loisir, haut ]
marqueurs_niveau2 :: 4 :: [ choisir, respecter, long, loisir ]
pas_d_indication :: 3 :: [ rupture, suffisant, haut ]
marqueurs_niveau0 :: 3 :: [ rupture, suffisant, haut ]
pas_d_indication_niveau0 :: 3 :: [ rupture, suffisant, haut ]
appreciatif :: 2 :: [ respecter, loisir ]
epistemique :: 2 :: [ long, pouvoir ]
appreciatif_niveau2 :: 2 :: [ respecter, loisir ]
alethique :: 1 :: [ pouvoir ]
axiologique :: 1 :: [ respecter ]
boulique :: 1 :: [ choisir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
axiologique_niveau2 :: 1 :: [ respecter ]
boulique_niveau2 :: 1 :: [ choisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 38

