
********** Entity Names Features **********
*******************************************

1. Sentence: La loi du 14 Mars 1919-19 Juil . 1924 , relative aux projets communaux d ’ aménagement , dans son art .
Entity Name (Type) :: Mars 1919-19 Juil (MISC)


********** Emotions Features **********
***************************************

1. Sentence: La loi du 14 Mars 1919-19 Juil . 1924 , relative aux projets communaux d ’ aménagement , dans son art .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La loi du 14 Mars 1919-19 Juil . 1924 , relative aux projets communaux d ’ aménagement , dans son art .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ loi ]
deontique :: 1 :: [ loi ]
marqueurs_niveau2 :: 1 :: [ loi ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La loi du 14 Mars 1919-19 Juil . 1924 , relative aux projets communaux d ’ aménagement , dans son art .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['19_19 Juil .', '1924 ,']
adverbiaux_localisation_temporelle::1::['1924 ,']
adverbiaux_pointage_absolu::1::['1924 ,']
adverbiaux_loc_temp_focalisation_id::1::['19_19 Juil .']
adverbiaux_loc_temp_regionalisation_id::1::['19_19 Juil .']
adverbiaux_loc_temp_pointage_absolu::1::['1924 ,']

