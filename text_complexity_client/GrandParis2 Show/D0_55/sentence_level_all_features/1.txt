
********** Entity Names Features **********
*******************************************

1. Sentence: Quant à l ' ensemble du schéma directeur , il fut progressivement établi au cours des années 1963 et 1964 par l ' Institut d ' aménagement et d ' urbanisme de la région parisienne , suivant les directives du Délégué général au District et en fonction des orientations générales concernant la population et les emplois en région de Paris qui étaient précisées , au même moment , par la commission nationale d ' aménagement du territoire , au sein du commissariat général du Plan .
Entity Name (Type) :: commissariat général du Plan (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Quant à l ' ensemble du schéma directeur , il fut progressivement établi au cours des années 1963 et 1964 par l ' Institut d ' aménagement et d ' urbanisme de la région parisienne , suivant les directives du Délégué général au District et en fonction des orientations générales concernant la population et les emplois en région de Paris qui étaient précisées , au même moment , par la commission nationale d ' aménagement du territoire , au sein du commissariat général du Plan .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Quant à l ' ensemble du schéma directeur , il fut progressivement établi au cours des années 1963 et 1964 par l ' Institut d ' aménagement et d ' urbanisme de la région parisienne , suivant les directives du Délégué général au District et en fonction des orientations générales concernant la population et les emplois en région de Paris qui étaient précisées , au même moment , par la commission nationale d ' aménagement du territoire , au sein du commissariat général du Plan .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ emploi ]
appreciatif :: 1 :: [ emploi ]
marqueurs_niveau2 :: 1 :: [ emploi ]
appreciatif_niveau2 :: 1 :: [ emploi ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Quant à l ' ensemble du schéma directeur , il fut progressivement établi au cours des années 1963 et 1964 par l ' Institut d ' aménagement et d ' urbanisme de la région parisienne , suivant les directives du Délégué général au District et en fonction des orientations générales concernant la population et les emplois en région de Paris qui étaient précisées , au même moment , par la commission nationale d ' aménagement du territoire , au sein du commissariat général du Plan .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['au cours des années 1963 et 1964', 'au cours des années 1963']
adverbiaux_localisation_temporelle::1::['au cours des années 1963']
adverbiaux_pointage_absolu::1::['au cours des années 1963']
adverbiaux_pointage_non_absolu::1::['au cours des années 1963']
adverbiaux_loc_temp_focalisation_id::1::['au cours des années 1963 et 1964']
adverbiaux_loc_temp_regionalisation_id::1::['au cours des années 1963 et 1964']
adverbiaux_loc_temp_pointage_non_absolu::1::['au cours des années 1963']
adverbiaux_dur_iter_absolu::1::['au cours des années 1963']

