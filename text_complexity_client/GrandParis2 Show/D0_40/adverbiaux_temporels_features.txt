====== Sentence Level Features ======

1. Sentence: S ' attachant à poser les problèmes dans leur véritable ampleur sans pouvoir encore leur apporter de projets de solutions à leur dimension , un premier document intitulé " avant-projet de programme duodécennal pour la région de Paris " (2) fut publié le 23 février 1963 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['le 23 février 1963', 'encore']
adverbiaux_localisation_temporelle::1::['le 23 février 1963']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_absolu::1::['le 23 février 1963']
adverbiaux_pointage_non_absolu::2::['le 23 février 1963', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['le 23 février 1963']
adverbiaux_loc_temp_regionalisation_id::1::['le 23 février 1963']
adverbiaux_loc_temp_pointage_non_absolu::1::['le 23 février 1963']
adverbiaux_dur_iter_absolu::1::['le 23 février 1963']
adverbiaux_dur_iter_relatif::1::['encore']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['le 23 février 1963', 'encore']
adverbiaux_localisation_temporelle::1::['le 23 février 1963']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_absolu::1::['le 23 février 1963']
adverbiaux_pointage_non_absolu::2::['le 23 février 1963', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['le 23 février 1963']
adverbiaux_loc_temp_regionalisation_id::1::['le 23 février 1963']
adverbiaux_loc_temp_pointage_non_absolu::1::['le 23 février 1963']
adverbiaux_dur_iter_absolu::1::['le 23 février 1963']
adverbiaux_dur_iter_relatif::1::['encore']
Total feature counts: 12

