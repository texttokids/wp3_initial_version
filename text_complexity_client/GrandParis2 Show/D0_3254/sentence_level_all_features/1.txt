
********** Entity Names Features **********
*******************************************

1. Sentence: Globalement les dépenses de loisirs des Français entre 1950 et 1960 - sans que la durée des loisirs ait sensiblement varié entre ces deux dates - ont augmenté de 180 % (en francs constants) pour atteindre 15 milliards de francs en 1960 .
Entity Name (Type) :: Français (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Globalement les dépenses de loisirs des Français entre 1950 et 1960 - sans que la durée des loisirs ait sensiblement varié entre ces deux dates - ont augmenté de 180 % (en francs constants) pour atteindre 15 milliards de francs en 1960 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Globalement les dépenses de loisirs des Français entre 1950 et 1960 - sans que la durée des loisirs ait sensiblement varié entre ces deux dates - ont augmenté de 180 % (en francs constants) pour atteindre 15 milliards de francs en 1960 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ loisir, loisir ]
appreciatif :: 2 :: [ loisir, loisir ]
marqueurs_niveau2 :: 2 :: [ loisir, loisir ]
appreciatif_niveau2 :: 2 :: [ loisir, loisir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Globalement les dépenses de loisirs des Français entre 1950 et 1960 - sans que la durée des loisirs ait sensiblement varié entre ces deux dates - ont augmenté de 180 % (en francs constants) pour atteindre 15 milliards de francs en 1960 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['entre 1950 et 1960', 'en 1960']
adverbiaux_localisation_temporelle::2::['entre 1950 et 1960', 'en 1960']
adverbiaux_pointage_absolu::2::['entre 1950 et 1960', 'en 1960']
adverbiaux_pointage_non_absolu::1::['en 1960']
adverbiaux_loc_temp_focalisation_id::1::['en 1960']
adverbiaux_loc_temp_regionalisation_id::1::['en 1960']
adverbiaux_loc_temp_pointage_absolu::1::['entre 1950 et 1960']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1960']
adverbiaux_dur_iter_absolu::1::['en 1960']

