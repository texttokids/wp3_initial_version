
********** Entity Names Features **********
*******************************************

1. Sentence: Ainsi , dans deux villes d ' importance assez comparable , dont la population jouit d ' un revenu de niveau semblable , le taux d ' utilisation de la voiture individuelle varie , pour une même catégorie de déplacements , dans le rapport de 1 à 2 , 6 , alors que le taux de motorisation ne varie que de 1 à 1 , 9 .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Ainsi , dans deux villes d ' importance assez comparable , dont la population jouit d ' un revenu de niveau semblable , le taux d ' utilisation de la voiture individuelle varie , pour une même catégorie de déplacements , dans le rapport de 1 à 2 , 6 , alors que le taux de motorisation ne varie que de 1 à 1 , 9 .
Tokens having emotion: [ jouit ]
Lemmas having emotion: [ jouir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Ainsi , dans deux villes d ' importance assez comparable , dont la population jouit d ' un revenu de niveau semblable , le taux d ' utilisation de la voiture individuelle varie , pour une même catégorie de déplacements , dans le rapport de 1 à 2 , 6 , alors que le taux de motorisation ne varie que de 1 à 1 , 9 .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ainsi , dans deux villes d ' importance assez comparable , dont la population jouit d ' un revenu de niveau semblable , le taux d ' utilisation de la voiture individuelle varie , pour une même catégorie de déplacements , dans le rapport de 1 à 2 , 6 , alors que le taux de motorisation ne varie que de 1 à 1 , 9 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de 1 à 2', 'de 1 à 1']
adverbiaux_duratifs_iteratifs::2::['de 1 à 2', 'de 1 à 1']
adverbiaux_pointage_absolu::2::['de 1 à 2', 'de 1 à 1']
adverbiaux_dur_iter_absolu::2::['de 1 à 2', 'de 1 à 1']

