
********** Entity Names Features **********
*******************************************

1. Sentence: si nous pouvons raccourcir le délai par le progrès des moyens de transport , en même temps qu ’ en élargissant la distance , et c ’ est ce qui se produit dès aujourd ’ hui parce qu ' au fur et à mesure que des autoroutes se créent des gens qui avaient des résidences secondaires les transforment en résidences principales , et la vieille pensée d ’ Allais se trouve encore plus réalisée  : ils habitent la campagne , et ils viennent travailler en ville .
Entity Name (Type) :: Allais (PER)


********** Emotions Features **********
***************************************

1. Sentence: si nous pouvons raccourcir le délai par le progrès des moyens de transport , en même temps qu ’ en élargissant la distance , et c ’ est ce qui se produit dès aujourd ’ hui parce qu ' au fur et à mesure que des autoroutes se créent des gens qui avaient des résidences secondaires les transforment en résidences principales , et la vieille pensée d ’ Allais se trouve encore plus réalisée  : ils habitent la campagne , et ils viennent travailler en ville .
No features found.


********** Modality Features **********
***************************************

1. Sentence: si nous pouvons raccourcir le délai par le progrès des moyens de transport , en même temps qu ’ en élargissant la distance , et c ’ est ce qui se produit dès aujourd ’ hui parce qu ' au fur et à mesure que des autoroutes se créent des gens qui avaient des résidences secondaires les transforment en résidences principales , et la vieille pensée d ’ Allais se trouve encore plus réalisée  : ils habitent la campagne , et ils viennent travailler en ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ pouvoir, vieux, réaliser, venir, travailler ]
alethique :: 3 :: [ pouvoir, réaliser, venir ]
marqueurs_niveau3 :: 2 :: [ pouvoir, venir ]
alethique_niveau3 :: 2 :: [ pouvoir, venir ]
appreciatif :: 1 :: [ travailler ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ vieux ]
marqueurs_niveau2 :: 1 :: [ travailler ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
marqueurs_niveau0 :: 1 :: [ vieux ]
alethique_niveau1 :: 1 :: [ réaliser ]
appreciatif_niveau2 :: 1 :: [ travailler ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ vieux ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: si nous pouvons raccourcir le délai par le progrès des moyens de transport , en même temps qu ’ en élargissant la distance , et c ’ est ce qui se produit dès aujourd ’ hui parce qu ' au fur et à mesure que des autoroutes se créent des gens qui avaient des résidences secondaires les transforment en résidences principales , et la vieille pensée d ’ Allais se trouve encore plus réalisée  : ils habitent la campagne , et ils viennent travailler en ville .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['aujourd ’ hui', 'encore']
adverbiaux_localisation_temporelle::1::['aujourd ’ hui']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::3::['aujourd ’ hui', 'aujourd ’ hui', 'encore']
adverbiaux_loc_temp_pointage_non_absolu::1::['aujourd ’ hui']
adverbiaux_dur_iter_deictique::1::['aujourd ’ hui']
adverbiaux_dur_iter_relatif::1::['encore']

