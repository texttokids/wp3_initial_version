====== Sentence Level Features ======

1. Sentence: si nous pouvons raccourcir le délai par le progrès des moyens de transport , en même temps qu ’ en élargissant la distance , et c ’ est ce qui se produit dès aujourd ’ hui parce qu ' au fur et à mesure que des autoroutes se créent des gens qui avaient des résidences secondaires les transforment en résidences principales , et la vieille pensée d ’ Allais se trouve encore plus réalisée  : ils habitent la campagne , et ils viennent travailler en ville .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['aujourd ’ hui', 'encore']
adverbiaux_localisation_temporelle::1::['aujourd ’ hui']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::3::['aujourd ’ hui', 'aujourd ’ hui', 'encore']
adverbiaux_loc_temp_pointage_non_absolu::1::['aujourd ’ hui']
adverbiaux_dur_iter_deictique::1::['aujourd ’ hui']
adverbiaux_dur_iter_relatif::1::['encore']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['aujourd ’ hui', 'encore']
adverbiaux_localisation_temporelle::1::['aujourd ’ hui']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::3::['aujourd ’ hui', 'aujourd ’ hui', 'encore']
adverbiaux_loc_temp_pointage_non_absolu::1::['aujourd ’ hui']
adverbiaux_dur_iter_deictique::1::['aujourd ’ hui']
adverbiaux_dur_iter_relatif::1::['encore']
Total feature counts: 10

