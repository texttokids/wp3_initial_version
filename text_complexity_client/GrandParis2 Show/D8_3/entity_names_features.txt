====== Sentence Level Features ======

1. Sentence: Rappelons , pour ne citer qu ’ un seul exemple , que le bon fonctionnement d ’ un grand nombre de Services Publics de l ’ État - services civils et militaires - se trouve entravé , sur l ’ ensemble du territoire , parfois même paralysé , du fait que toute mutation de fonctionnaire de la province vers Paris pose pour les intéressés et leurs familles , des questions de logement fréquemment insolubles , et que , d ' autre part , l ' évacuation de logements de fonctions occupés par des fonctionnaires mis à la retraite ou licenciés et qui ne savent où se loger provoque également des difficultés presque insurmontables .
Entity Name (Type) :: Paris (LOC)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
Services Publics de l ’ État (ORG)
Paris (LOC)
Total entitiy counts: 2

