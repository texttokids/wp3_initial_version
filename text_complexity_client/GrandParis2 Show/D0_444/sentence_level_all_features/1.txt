
********** Entity Names Features **********
*******************************************

1. Sentence: En matière d ' équipement , le présent schéma directeur définit le cadre dans lequel seront assumées les principales fonctions urbaines - transports , activités , loisirs - en région de Paris , au fur et à mesure de l ' accroissement des besoins dûs tant à l ' accroissement du niveau de vie qu ' à celui du nombre des citadins , jusqu ' à 14 millions d ' habitants .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En matière d ' équipement , le présent schéma directeur définit le cadre dans lequel seront assumées les principales fonctions urbaines - transports , activités , loisirs - en région de Paris , au fur et à mesure de l ' accroissement des besoins dûs tant à l ' accroissement du niveau de vie qu ' à celui du nombre des citadins , jusqu ' à 14 millions d ' habitants .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En matière d ' équipement , le présent schéma directeur définit le cadre dans lequel seront assumées les principales fonctions urbaines - transports , activités , loisirs - en région de Paris , au fur et à mesure de l ' accroissement des besoins dûs tant à l ' accroissement du niveau de vie qu ' à celui du nombre des citadins , jusqu ' à 14 millions d ' habitants .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ loisir, besoin ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ loisir ]
boulique :: 1 :: [ besoin ]
marqueurs_niveau3 :: 1 :: [ besoin ]
marqueurs_niveau2 :: 1 :: [ loisir ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau2 :: 1 :: [ loisir ]
boulique_niveau3 :: 1 :: [ besoin ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En matière d ' équipement , le présent schéma directeur définit le cadre dans lequel seront assumées les principales fonctions urbaines - transports , activités , loisirs - en région de Paris , au fur et à mesure de l ' accroissement des besoins dûs tant à l ' accroissement du niveau de vie qu ' à celui du nombre des citadins , jusqu ' à 14 millions d ' habitants .
No features found.

