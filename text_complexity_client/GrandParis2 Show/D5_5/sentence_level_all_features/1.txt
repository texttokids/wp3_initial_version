
********** Entity Names Features **********
*******************************************

1. Sentence: Le tracé de la future route interurbaine de Seine-et-Oise entre l ’ agglomération parisienne et la " plaine de France " résulte de la volonté de bien marquer une séparation entre une zone d ' économie urbaine indiscutable et une région dont le caractère encore rural ou peu urbanisé doit être maintenu .
Entity Name (Type) :: plaine de France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le tracé de la future route interurbaine de Seine-et-Oise entre l ’ agglomération parisienne et la " plaine de France " résulte de la volonté de bien marquer une séparation entre une zone d ' économie urbaine indiscutable et une région dont le caractère encore rural ou peu urbanisé doit être maintenu .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Le tracé de la future route interurbaine de Seine-et-Oise entre l ’ agglomération parisienne et la " plaine de France " résulte de la volonté de bien marquer une séparation entre une zone d ' économie urbaine indiscutable et une région dont le caractère encore rural ou peu urbanisé doit être maintenu .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ volonté, bien, devoir ]
marqueurs_niveau3 :: 3 :: [ volonté, bien, devoir ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
boulique :: 1 :: [ volonté ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
boulique_niveau3 :: 1 :: [ volonté ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le tracé de la future route interurbaine de Seine-et-Oise entre l ’ agglomération parisienne et la " plaine de France " résulte de la volonté de bien marquer une séparation entre une zone d ' économie urbaine indiscutable et une région dont le caractère encore rural ou peu urbanisé doit être maintenu .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

