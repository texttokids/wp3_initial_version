
********** Entity Names Features **********
*******************************************

1. Sentence: La nécessité de " restructurer " la banlieue autour de centres urbains puissants devient maintenant assez évidente pour que l ' on puisse espérer cependant que la réalisation d ' un plan d ' ensemble soit comprise de tous .
Entity Name (Type) :: La (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La nécessité de " restructurer " la banlieue autour de centres urbains puissants devient maintenant assez évidente pour que l ' on puisse espérer cependant que la réalisation d ' un plan d ' ensemble soit comprise de tous .
Tokens having emotion: [ espérer ]
Lemmas having emotion: [ espérer ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: La nécessité de " restructurer " la banlieue autour de centres urbains puissants devient maintenant assez évidente pour que l ' on puisse espérer cependant que la réalisation d ' un plan d ' ensemble soit comprise de tous .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ nécessité, puissant, pouvoir, espérer ]
marqueurs_niveau3 :: 3 :: [ nécessité, pouvoir, espérer ]
alethique :: 2 :: [ nécessité, pouvoir ]
epistemique :: 2 :: [ pouvoir, espérer ]
alethique_niveau3 :: 2 :: [ nécessité, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, espérer ]
boulique :: 1 :: [ espérer ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ puissant ]
marqueurs_niveau0 :: 1 :: [ puissant ]
boulique_niveau3 :: 1 :: [ espérer ]
deontique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ puissant ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La nécessité de " restructurer " la banlieue autour de centres urbains puissants devient maintenant assez évidente pour que l ' on puisse espérer cependant que la réalisation d ' un plan d ' ensemble soit comprise de tous .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['maintenant']
adverbiaux_localisation_temporelle::1::['maintenant']
adverbiaux_pointage_non_absolu::1::['maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::1::['maintenant']
adverbiaux_loc_temp_pointage_non_absolu::1::['maintenant']

