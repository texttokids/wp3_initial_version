
********** Entity Names Features **********
*******************************************

1. Sentence: le relogement des habitants et des artisans du quartier ancien dans des immeubles réhabilités paraît , en raison du prix de revient de l ' opération (1) , encore plus difficile à assurer que dans le cas d ' une rénovation , qui comporte généralement une forte proportion de logements sociaux .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: le relogement des habitants et des artisans du quartier ancien dans des immeubles réhabilités paraît , en raison du prix de revient de l ' opération (1) , encore plus difficile à assurer que dans le cas d ' une rénovation , qui comporte généralement une forte proportion de logements sociaux .
No features found.


********** Modality Features **********
***************************************

1. Sentence: le relogement des habitants et des artisans du quartier ancien dans des immeubles réhabilités paraît , en raison du prix de revient de l ' opération (1) , encore plus difficile à assurer que dans le cas d ' une rénovation , qui comporte généralement une forte proportion de logements sociaux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ paraître, prix, difficile, généralement, fort ]
appreciatif :: 2 :: [ prix, difficile ]
epistemique :: 2 :: [ paraître, difficile ]
marqueurs_niveau3 :: 2 :: [ paraître, généralement ]
marqueurs_niveau2 :: 2 :: [ prix, difficile ]
appreciatif_niveau2 :: 2 :: [ prix, difficile ]
alethique :: 1 :: [ généralement ]
axiologique :: 1 :: [ prix ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau0 :: 1 :: [ fort ]
alethique_niveau3 :: 1 :: [ généralement ]
axiologique_niveau2 :: 1 :: [ prix ]
epistemique_niveau3 :: 1 :: [ paraître ]
epistemique_niveau2 :: 1 :: [ difficile ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: le relogement des habitants et des artisans du quartier ancien dans des immeubles réhabilités paraît , en raison du prix de revient de l ' opération (1) , encore plus difficile à assurer que dans le cas d ' une rénovation , qui comporte généralement une forte proportion de logements sociaux .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['encore', 'généralement']
adverbiaux_purement_iteratifs::1::['généralement']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_iterateur_frequentiel::1::['généralement']
adverbiaux_dur_iter_relatif::1::['encore']

