====== Sentence Level Features ======

1. Sentence: Pour que de telles erreurs ne se reproduisent pas , il est utile de marquer des directions interdites , et il conviendra d ' être plus strict que naguère sur le respect de ces interdictions .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ utile, interdire, respect, interdiction ]
appreciatif :: 2 :: [ utile, respect ]
deontique :: 2 :: [ interdire, interdiction ]
marqueurs_niveau3 :: 2 :: [ utile, interdire ]
marqueurs_niveau2 :: 2 :: [ respect, interdiction ]
axiologique :: 1 :: [ respect ]
appreciatif_niveau3 :: 1 :: [ utile ]
appreciatif_niveau2 :: 1 :: [ respect ]
axiologique_niveau2 :: 1 :: [ respect ]
deontique_niveau3 :: 1 :: [ interdire ]
deontique_niveau2 :: 1 :: [ interdiction ]
Total feature counts: 18


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ utile, interdire, respect, interdiction ]
appreciatif :: 2 :: [ utile, respect ]
deontique :: 2 :: [ interdire, interdiction ]
marqueurs_niveau3 :: 2 :: [ utile, interdire ]
marqueurs_niveau2 :: 2 :: [ respect, interdiction ]
axiologique :: 1 :: [ respect ]
appreciatif_niveau3 :: 1 :: [ utile ]
appreciatif_niveau2 :: 1 :: [ respect ]
axiologique_niveau2 :: 1 :: [ respect ]
deontique_niveau3 :: 1 :: [ interdire ]
deontique_niveau2 :: 1 :: [ interdiction ]
Total feature counts: 18

