
********** Entity Names Features **********
*******************************************

1. Sentence: Les perspectives de croissance qui viennent d ' être esquissées pour la région parisienne laissent prévoir que la part du centre de l ' agglomération diminuera tandis qu ' à la périphérie doivent se créer autant d ' emplois que la ville de Paris en abrite aujourd ' hui .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Les perspectives de croissance qui viennent d ' être esquissées pour la région parisienne laissent prévoir que la part du centre de l ' agglomération diminuera tandis qu ' à la périphérie doivent se créer autant d ' emplois que la ville de Paris en abrite aujourd ' hui .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les perspectives de croissance qui viennent d ' être esquissées pour la région parisienne laissent prévoir que la part du centre de l ' agglomération diminuera tandis qu ' à la périphérie doivent se créer autant d ' emplois que la ville de Paris en abrite aujourd ' hui .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ venir, laisser, devoir, emploi ]
alethique :: 2 :: [ venir, devoir ]
deontique :: 2 :: [ laisser, devoir ]
marqueurs_niveau3 :: 2 :: [ venir, devoir ]
marqueurs_niveau2 :: 2 :: [ laisser, emploi ]
alethique_niveau3 :: 2 :: [ venir, devoir ]
appreciatif :: 1 :: [ emploi ]
epistemique :: 1 :: [ devoir ]
appreciatif_niveau2 :: 1 :: [ emploi ]
deontique_niveau3 :: 1 :: [ devoir ]
deontique_niveau2 :: 1 :: [ laisser ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les perspectives de croissance qui viennent d ' être esquissées pour la région parisienne laissent prévoir que la part du centre de l ' agglomération diminuera tandis qu ' à la périphérie doivent se créer autant d ' emplois que la ville de Paris en abrite aujourd ' hui .
No features found.

