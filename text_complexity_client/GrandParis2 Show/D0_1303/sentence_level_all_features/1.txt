
********** Entity Names Features **********
*******************************************

1. Sentence: Ainsi dotés des moyens de transports nécessaires , ce sont le travail et le loisir - et demain plus encore le loisir que le travail - qui sont les puissants facteurs d ' unité de la région , et l ' unité , une fois assurée dans cette vaste région composée d '  " établissements humains " différenciés , accroît à son tour les libertés concrètes des hommes qui y habitent , non pas par la vie sans contraintes mais en restaurant le choix entre des possibilités dont les contraintes sont différentes .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Ainsi dotés des moyens de transports nécessaires , ce sont le travail et le loisir - et demain plus encore le loisir que le travail - qui sont les puissants facteurs d ' unité de la région , et l ' unité , une fois assurée dans cette vaste région composée d '  " établissements humains " différenciés , accroît à son tour les libertés concrètes des hommes qui y habitent , non pas par la vie sans contraintes mais en restaurant le choix entre des possibilités dont les contraintes sont différentes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ainsi dotés des moyens de transports nécessaires , ce sont le travail et le loisir - et demain plus encore le loisir que le travail - qui sont les puissants facteurs d ' unité de la région , et l ' unité , une fois assurée dans cette vaste région composée d '  " établissements humains " différenciés , accroît à son tour les libertés concrètes des hommes qui y habitent , non pas par la vie sans contraintes mais en restaurant le choix entre des possibilités dont les contraintes sont différentes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ nécessaire, travail, loisir, loisir, travail, puissant, liberté, choix, possibilité ]
appreciatif :: 5 :: [ travail, loisir, loisir, travail, liberté ]
marqueurs_niveau3 :: 4 :: [ nécessaire, travail, travail, possibilité ]
marqueurs_niveau2 :: 3 :: [ loisir, loisir, choix ]
alethique :: 2 :: [ nécessaire, possibilité ]
alethique_niveau3 :: 2 :: [ nécessaire, possibilité ]
appreciatif_niveau3 :: 2 :: [ travail, travail ]
appreciatif_niveau2 :: 2 :: [ loisir, loisir ]
boulique :: 1 :: [ choix ]
pas_d_indication :: 1 :: [ puissant ]
marqueurs_niveau1 :: 1 :: [ liberté ]
marqueurs_niveau0 :: 1 :: [ puissant ]
appreciatif_niveau1 :: 1 :: [ liberté ]
boulique_niveau2 :: 1 :: [ choix ]
pas_d_indication_niveau0 :: 1 :: [ puissant ]
Total feature counts: 36


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ainsi dotés des moyens de transports nécessaires , ce sont le travail et le loisir - et demain plus encore le loisir que le travail - qui sont les puissants facteurs d ' unité de la région , et l ' unité , une fois assurée dans cette vaste région composée d '  " établissements humains " différenciés , accroît à son tour les libertés concrètes des hommes qui y habitent , non pas par la vie sans contraintes mais en restaurant le choix entre des possibilités dont les contraintes sont différentes .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['demain', 'encore']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::3::['demain', 'demain', 'encore']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']
adverbiaux_dur_iter_relatif::1::['encore']

