====== Sentence Level Features ======

1. Sentence: Elle ne signifie nullement que le lycée , la maison de jeunes , la bibliothèque et le cinéma de quartier , le bassin d ' entraînement à la natation , le magasin populaire , le petit restaurant ou le commissariat de police ne doivent pas être multipliés en banlieue : ce sont au contraire ces équipements qui donnent pour beaucoup sa substance et sa forme à la vie quotidienne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ jeune, cinéma, populaire, petit, devoir, beaucoup ]
epistemique :: 3 :: [ petit, devoir, beaucoup ]
alethique :: 2 :: [ cinéma, devoir ]
pas_d_indication :: 2 :: [ jeune, populaire ]
marqueurs_niveau3 :: 2 :: [ petit, devoir ]
marqueurs_niveau1 :: 2 :: [ cinéma, beaucoup ]
marqueurs_niveau0 :: 2 :: [ jeune, populaire ]
epistemique_niveau3 :: 2 :: [ petit, devoir ]
pas_d_indication_niveau0 :: 2 :: [ jeune, populaire ]
appreciatif :: 1 :: [ cinéma ]
deontique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau1 :: 1 :: [ cinéma ]
appreciatif_niveau1 :: 1 :: [ cinéma ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 30


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ jeune, cinéma, populaire, petit, devoir, beaucoup ]
epistemique :: 3 :: [ petit, devoir, beaucoup ]
alethique :: 2 :: [ cinéma, devoir ]
pas_d_indication :: 2 :: [ jeune, populaire ]
marqueurs_niveau3 :: 2 :: [ petit, devoir ]
marqueurs_niveau1 :: 2 :: [ cinéma, beaucoup ]
marqueurs_niveau0 :: 2 :: [ jeune, populaire ]
epistemique_niveau3 :: 2 :: [ petit, devoir ]
pas_d_indication_niveau0 :: 2 :: [ jeune, populaire ]
appreciatif :: 1 :: [ cinéma ]
deontique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau1 :: 1 :: [ cinéma ]
appreciatif_niveau1 :: 1 :: [ cinéma ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 30

