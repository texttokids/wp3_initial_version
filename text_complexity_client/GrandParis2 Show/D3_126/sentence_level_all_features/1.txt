
********** Entity Names Features **********
*******************************************

1. Sentence: Il est tout à fait normal qu ' en tant que chef de l ’ État je m ' engage pleinement dans cette mission : redonner à l ' architecture la possibilité de l ' audace .
Entity Name (Type) :: État je m (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Il est tout à fait normal qu ' en tant que chef de l ’ État je m ' engage pleinement dans cette mission : redonner à l ' architecture la possibilité de l ' audace .
Tokens having emotion: [ État, audace ]
Lemmas having emotion: [ état, audace ]
Categories of the emotion lemmas: [ non_specifiee, audace ]


********** Modality Features **********
***************************************

1. Sentence: Il est tout à fait normal qu ' en tant que chef de l ’ État je m ' engage pleinement dans cette mission : redonner à l ' architecture la possibilité de l ' audace .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ normal, possibilité ]
alethique :: 1 :: [ possibilité ]
epistemique :: 1 :: [ normal ]
marqueurs_niveau3 :: 1 :: [ possibilité ]
marqueurs_niveau1 :: 1 :: [ normal ]
alethique_niveau3 :: 1 :: [ possibilité ]
epistemique_niveau1 :: 1 :: [ normal ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il est tout à fait normal qu ' en tant que chef de l ’ État je m ' engage pleinement dans cette mission : redonner à l ' architecture la possibilité de l ' audace .
No features found.

