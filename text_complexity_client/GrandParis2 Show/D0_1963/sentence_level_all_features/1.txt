
********** Entity Names Features **********
*******************************************

1. Sentence: La faculté d ' Orsay , ni " campus " ni " quartier latin " n ' est pas , jusqu ' à présent , une réussite d ' urbanisme ( 1) .
Entity Name (Type) :: Orsay (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La faculté d ' Orsay , ni " campus " ni " quartier latin " n ' est pas , jusqu ' à présent , une réussite d ' urbanisme ( 1) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La faculté d ' Orsay , ni " campus " ni " quartier latin " n ' est pas , jusqu ' à présent , une réussite d ' urbanisme ( 1) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ réussite ]
appreciatif :: 1 :: [ réussite ]
marqueurs_niveau2 :: 1 :: [ réussite ]
appreciatif_niveau2 :: 1 :: [ réussite ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La faculté d ' Orsay , ni " campus " ni " quartier latin " n ' est pas , jusqu ' à présent , une réussite d ' urbanisme ( 1) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à présent']
adverbiaux_localisation_temporelle::1::['à présent']
adverbiaux_pointage_non_absolu::2::['à présent', 'à présent']
adverbiaux_loc_temp_focalisation_id::1::['à présent']
adverbiaux_loc_temp_regionalisation_id::1::['à présent']
adverbiaux_loc_temp_pointage_non_absolu::1::['à présent']
adverbiaux_dur_iter_deictique::1::['à présent']

