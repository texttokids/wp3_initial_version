
********** Entity Names Features **********
*******************************************

1. Sentence: l ’ année dernière , l ’ attention de Monsieur le Recteur de l ’ Université de Paris et de Monsieur le Doyen de la Faculté des Sciences .
Entity Name (Type) :: Faculté des Sciences (ORG)


********** Emotions Features **********
***************************************

1. Sentence: l ’ année dernière , l ’ attention de Monsieur le Recteur de l ’ Université de Paris et de Monsieur le Doyen de la Faculté des Sciences .
No features found.


********** Modality Features **********
***************************************

1. Sentence: l ’ année dernière , l ’ attention de Monsieur le Recteur de l ’ Université de Paris et de Monsieur le Doyen de la Faculté des Sciences .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: l ’ année dernière , l ’ attention de Monsieur le Recteur de l ’ Université de Paris et de Monsieur le Doyen de la Faculté des Sciences .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['l ’ année dernière']
adverbiaux_localisation_temporelle::1::['l ’ année dernière']
adverbiaux_pointage_non_absolu::2::['l ’ année dernière', 'l ’ année dernière']
adverbiaux_loc_temp_focalisation_id::1::['l ’ année dernière']
adverbiaux_loc_temp_regionalisation_id::1::['l ’ année dernière']
adverbiaux_loc_temp_pointage_non_absolu::1::['l ’ année dernière']
adverbiaux_dur_iter_deictique::1::['l ’ année dernière']

