
********** Entity Names Features **********
*******************************************

1. Sentence: Le schéma directeur fut ensuite présenté , en séances de travail , aux préfets des départements de la région , au Délégué à l ' aménagement du territoire , au Commissaire général au Plan , au Conseil supérieur d ' architecture , au Groupe central de planification urbaine et à chacun des Ministres intéressés .
Entity Name (Type) :: Ministres intéressés (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Le schéma directeur fut ensuite présenté , en séances de travail , aux préfets des départements de la région , au Délégué à l ' aménagement du territoire , au Commissaire général au Plan , au Conseil supérieur d ' architecture , au Groupe central de planification urbaine et à chacun des Ministres intéressés .
Tokens having emotion: [ intéressés ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Le schéma directeur fut ensuite présenté , en séances de travail , aux préfets des départements de la région , au Délégué à l ' aménagement du territoire , au Commissaire général au Plan , au Conseil supérieur d ' architecture , au Groupe central de planification urbaine et à chacun des Ministres intéressés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ travail, intéresser ]
appreciatif :: 1 :: [ travail ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau3 :: 1 :: [ travail ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
appreciatif_niveau3 :: 1 :: [ travail ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le schéma directeur fut ensuite présenté , en séances de travail , aux préfets des départements de la région , au Délégué à l ' aménagement du territoire , au Commissaire général au Plan , au Conseil supérieur d ' architecture , au Groupe central de planification urbaine et à chacun des Ministres intéressés .
No features found.

