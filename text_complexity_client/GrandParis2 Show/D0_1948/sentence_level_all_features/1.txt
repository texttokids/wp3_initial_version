
********** Entity Names Features **********
*******************************************

1. Sentence: Le premier geste dans ce sens , courageux et sans doute décisif , est le départ des Halles centrales , à la Villette pour le marché de la viande , et pour les autres marchés alimentaires de gros , dans le centre urbain à la fois nouveau et rénové , qui , linéairement , s ' étendra du carrefour de la Belle-Épine , sur la commune de Rungis , jusqu ' à Choisy-le-Roi , et qui comprendra la première gare routière de la région parisienne .
Entity Name (Type) :: Choisy-le-Roi (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le premier geste dans ce sens , courageux et sans doute décisif , est le départ des Halles centrales , à la Villette pour le marché de la viande , et pour les autres marchés alimentaires de gros , dans le centre urbain à la fois nouveau et rénové , qui , linéairement , s ' étendra du carrefour de la Belle-Épine , sur la commune de Rungis , jusqu ' à Choisy-le-Roi , et qui comprendra la première gare routière de la région parisienne .
Tokens having emotion: [ courageux ]
Lemmas having emotion: [ courageux ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Le premier geste dans ce sens , courageux et sans doute décisif , est le départ des Halles centrales , à la Villette pour le marché de la viande , et pour les autres marchés alimentaires de gros , dans le centre urbain à la fois nouveau et rénové , qui , linéairement , s ' étendra du carrefour de la Belle-Épine , sur la commune de Rungis , jusqu ' à Choisy-le-Roi , et qui comprendra la première gare routière de la région parisienne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ courageux, doute, gros ]
epistemique :: 2 :: [ doute, gros ]
marqueurs_niveau3 :: 2 :: [ courageux, doute ]
appreciatif :: 1 :: [ courageux ]
axiologique :: 1 :: [ courageux ]
marqueurs_niveau2 :: 1 :: [ gros ]
appreciatif_niveau3 :: 1 :: [ courageux ]
axiologique_niveau3 :: 1 :: [ courageux ]
epistemique_niveau3 :: 1 :: [ doute ]
epistemique_niveau2 :: 1 :: [ gros ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le premier geste dans ce sens , courageux et sans doute décisif , est le départ des Halles centrales , à la Villette pour le marché de la viande , et pour les autres marchés alimentaires de gros , dans le centre urbain à la fois nouveau et rénové , qui , linéairement , s ' étendra du carrefour de la Belle-Épine , sur la commune de Rungis , jusqu ' à Choisy-le-Roi , et qui comprendra la première gare routière de la région parisienne .
No features found.

