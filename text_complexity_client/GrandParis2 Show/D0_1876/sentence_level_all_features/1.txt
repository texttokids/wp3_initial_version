
********** Entity Names Features **********
*******************************************

1. Sentence: Il est d ' abord à mes yeux la ville la plus complète qui soit au monde , car je n ' en vois point où la diversité des occupations , des industries , des fonctions , des produits et des idées soit plus riche et aussi mêlée qu ' ici " (5) , - et encore "  . . . C ' est pourquoi Paris est bien autre chose qu ' une capitale politique et un centre industriel , qu ' un port de première importance et un marché de toutes valeurs , qu ' un paradis artificiel et un sanctuaire de la culture .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Il est d ' abord à mes yeux la ville la plus complète qui soit au monde , car je n ' en vois point où la diversité des occupations , des industries , des fonctions , des produits et des idées soit plus riche et aussi mêlée qu ' ici " (5) , - et encore "  . . . C ' est pourquoi Paris est bien autre chose qu ' une capitale politique et un centre industriel , qu ' un port de première importance et un marché de toutes valeurs , qu ' un paradis artificiel et un sanctuaire de la culture .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il est d ' abord à mes yeux la ville la plus complète qui soit au monde , car je n ' en vois point où la diversité des occupations , des industries , des fonctions , des produits et des idées soit plus riche et aussi mêlée qu ' ici " (5) , - et encore "  . . . C ' est pourquoi Paris est bien autre chose qu ' une capitale politique et un centre industriel , qu ' un port de première importance et un marché de toutes valeurs , qu ' un paradis artificiel et un sanctuaire de la culture .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ occupation, riche, bien ]
appreciatif :: 2 :: [ riche, bien ]
axiologique :: 2 :: [ occupation, bien ]
marqueurs_niveau2 :: 2 :: [ occupation, riche ]
marqueurs_niveau3 :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau2 :: 1 :: [ riche ]
axiologique_niveau3 :: 1 :: [ bien ]
axiologique_niveau2 :: 1 :: [ occupation ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il est d ' abord à mes yeux la ville la plus complète qui soit au monde , car je n ' en vois point où la diversité des occupations , des industries , des fonctions , des produits et des idées soit plus riche et aussi mêlée qu ' ici " (5) , - et encore "  . . . C ' est pourquoi Paris est bien autre chose qu ' une capitale politique et un centre industriel , qu ' un port de première importance et un marché de toutes valeurs , qu ' un paradis artificiel et un sanctuaire de la culture .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

