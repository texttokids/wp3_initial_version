
********** Entity Names Features **********
*******************************************

1. Sentence: - la position arrêtée ou changeante des municipalités (2) les plus proches de Paris sont plus sensibles au devoir de loger les nouveaux citadins , quitte à mordre sur les espaces réservés;
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: - la position arrêtée ou changeante des municipalités (2) les plus proches de Paris sont plus sensibles au devoir de loger les nouveaux citadins , quitte à mordre sur les espaces réservés;
Tokens having emotion: [ sensibles ]
Lemmas having emotion: [ sensible ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: - la position arrêtée ou changeante des municipalités (2) les plus proches de Paris sont plus sensibles au devoir de loger les nouveaux citadins , quitte à mordre sur les espaces réservés;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ sensible, devoir, quitter, mordre ]
alethique :: 2 :: [ devoir, quitter ]
appreciatif :: 1 :: [ mordre ]
axiologique :: 1 :: [ mordre ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ sensible ]
marqueurs_niveau3 :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ quitter ]
marqueurs_niveau1 :: 1 :: [ mordre ]
marqueurs_niveau0 :: 1 :: [ sensible ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau2 :: 1 :: [ quitter ]
appreciatif_niveau1 :: 1 :: [ mordre ]
axiologique_niveau1 :: 1 :: [ mordre ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ sensible ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - la position arrêtée ou changeante des municipalités (2) les plus proches de Paris sont plus sensibles au devoir de loger les nouveaux citadins , quitte à mordre sur les espaces réservés;
No features found.

