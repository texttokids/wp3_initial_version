
********** Entity Names Features **********
*******************************************

1. Sentence: - L ' an 2000 , c ' est la date symbolique , et qui ne doit être prise que comme telle , où cette région sera constituée de 14 millions de personnes , en moyenne trois fois plus riches qu ' à l ' heure actuelle , et par conséquent plus exigeantes pour leur ville qu ' elles ne peuvent l ' être maintenant .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: - L ' an 2000 , c ' est la date symbolique , et qui ne doit être prise que comme telle , où cette région sera constituée de 14 millions de personnes , en moyenne trois fois plus riches qu ' à l ' heure actuelle , et par conséquent plus exigeantes pour leur ville qu ' elles ne peuvent l ' être maintenant .
No features found.


********** Modality Features **********
***************************************

1. Sentence: - L ' an 2000 , c ' est la date symbolique , et qui ne doit être prise que comme telle , où cette région sera constituée de 14 millions de personnes , en moyenne trois fois plus riches qu ' à l ' heure actuelle , et par conséquent plus exigeantes pour leur ville qu ' elles ne peuvent l ' être maintenant .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ devoir, personne, riche, pouvoir ]
alethique :: 3 :: [ devoir, personne, pouvoir ]
deontique :: 2 :: [ devoir, pouvoir ]
epistemique :: 2 :: [ devoir, pouvoir ]
marqueurs_niveau3 :: 2 :: [ devoir, pouvoir ]
marqueurs_niveau2 :: 2 :: [ personne, riche ]
alethique_niveau3 :: 2 :: [ devoir, pouvoir ]
deontique_niveau3 :: 2 :: [ devoir, pouvoir ]
epistemique_niveau3 :: 2 :: [ devoir, pouvoir ]
appreciatif :: 1 :: [ riche ]
alethique_niveau2 :: 1 :: [ personne ]
appreciatif_niveau2 :: 1 :: [ riche ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - L ' an 2000 , c ' est la date symbolique , et qui ne doit être prise que comme telle , où cette région sera constituée de 14 millions de personnes , en moyenne trois fois plus riches qu ' à l ' heure actuelle , et par conséquent plus exigeantes pour leur ville qu ' elles ne peuvent l ' être maintenant .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['L ` an 2000', 'à l ` heure actuelle', 'maintenant']
adverbiaux_localisation_temporelle::3::['L ` an 2000', 'à l ` heure actuelle', 'maintenant']
adverbiaux_pointage_absolu::1::['L ` an 2000']
adverbiaux_pointage_non_absolu::2::['à l ` heure actuelle', 'maintenant']
adverbiaux_loc_temp_focalisation_id::3::['L ` an 2000', 'à l ` heure actuelle', 'maintenant']
adverbiaux_loc_temp_regionalisation_id::3::['L ` an 2000', 'à l ` heure actuelle', 'maintenant']
adverbiaux_loc_temp_pointage_absolu::1::['L ` an 2000']
adverbiaux_loc_temp_pointage_non_absolu::2::['à l ` heure actuelle', 'maintenant']

