
********** Entity Names Features **********
*******************************************

1. Sentence: Même si le rythme des départs devait se ralentir - et il est actuellement plus faible que celui de nombre de villes comparables à notre capitale (3) - Paris perdrait encore probablement le dixième de sa population .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Même si le rythme des départs devait se ralentir - et il est actuellement plus faible que celui de nombre de villes comparables à notre capitale (3) - Paris perdrait encore probablement le dixième de sa population .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Même si le rythme des départs devait se ralentir - et il est actuellement plus faible que celui de nombre de villes comparables à notre capitale (3) - Paris perdrait encore probablement le dixième de sa population .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ devoir, perdre, probablement ]
epistemique :: 2 :: [ devoir, probablement ]
marqueurs_niveau3 :: 2 :: [ devoir, probablement ]
epistemique_niveau3 :: 2 :: [ devoir, probablement ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ perdre ]
deontique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ perdre ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau2 :: 1 :: [ perdre ]
deontique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Même si le rythme des départs devait se ralentir - et il est actuellement plus faible que celui de nombre de villes comparables à notre capitale (3) - Paris perdrait encore probablement le dixième de sa population .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['actuellement', 'encore']
adverbiaux_localisation_temporelle::1::['actuellement']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::3::['actuellement', 'actuellement', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['actuellement']
adverbiaux_loc_temp_regionalisation_id::1::['actuellement']
adverbiaux_loc_temp_pointage_non_absolu::1::['actuellement']
adverbiaux_dur_iter_deictique::1::['actuellement']
adverbiaux_dur_iter_relatif::1::['encore']

