
********** Entity Names Features **********
*******************************************

1. Sentence: La culture , qui signe notre appartenance à une histoire et à des valeurs communes , doit être un élément fédérateur et déterminant de la dynamique novatrice que le Grand Paris est en train de créer et je souhaite que soit développée la dimension culturelle de cet ambitieux projet .
Entity Name (Type) :: Grand Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La culture , qui signe notre appartenance à une histoire et à des valeurs communes , doit être un élément fédérateur et déterminant de la dynamique novatrice que le Grand Paris est en train de créer et je souhaite que soit développée la dimension culturelle de cet ambitieux projet .
Tokens having emotion: [ souhaite ]
Lemmas having emotion: [ souhaiter ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: La culture , qui signe notre appartenance à une histoire et à des valeurs communes , doit être un élément fédérateur et déterminant de la dynamique novatrice que le Grand Paris est en train de créer et je souhaite que soit développée la dimension culturelle de cet ambitieux projet .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ devoir, grand ]
epistemique :: 2 :: [ devoir, grand ]
marqueurs_niveau3 :: 2 :: [ devoir, grand ]
epistemique_niveau3 :: 2 :: [ devoir, grand ]
alethique :: 1 :: [ devoir ]
deontique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
deontique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La culture , qui signe notre appartenance à une histoire et à des valeurs communes , doit être un élément fédérateur et déterminant de la dynamique novatrice que le Grand Paris est en train de créer et je souhaite que soit développée la dimension culturelle de cet ambitieux projet .
No features found.

