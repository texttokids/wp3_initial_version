
********** Entity Names Features **********
*******************************************

1. Sentence: Confrontées aux réalisations françaises les plus spectaculaires (Massy-Antony , Sarcelles , Mourenx) , aux projets français les plus importants (Toulouse-Mirail , Caen-Herouville) ou encore à l ' exemple souvent cité des " new towns " britanniques des années 1950 (1) , ces villes nouvelles se caractérisent d ' abord par un changement d ' échelle , de 1 à 5 au minimum , et plus souvent de 1 à 10 : d ' un ensemble de 30 .
Entity Name (Type) :: Caen-Herouville (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Confrontées aux réalisations françaises les plus spectaculaires (Massy-Antony , Sarcelles , Mourenx) , aux projets français les plus importants (Toulouse-Mirail , Caen-Herouville) ou encore à l ' exemple souvent cité des " new towns " britanniques des années 1950 (1) , ces villes nouvelles se caractérisent d ' abord par un changement d ' échelle , de 1 à 5 au minimum , et plus souvent de 1 à 10 : d ' un ensemble de 30 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Confrontées aux réalisations françaises les plus spectaculaires (Massy-Antony , Sarcelles , Mourenx) , aux projets français les plus importants (Toulouse-Mirail , Caen-Herouville) ou encore à l ' exemple souvent cité des " new towns " britanniques des années 1950 (1) , ces villes nouvelles se caractérisent d ' abord par un changement d ' échelle , de 1 à 5 au minimum , et plus souvent de 1 à 10 : d ' un ensemble de 30 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ spectaculaire, important, souvent, souvent ]
alethique :: 2 :: [ souvent, souvent ]
appreciatif :: 2 :: [ spectaculaire, important ]
marqueurs_niveau3 :: 2 :: [ souvent, souvent ]
alethique_niveau3 :: 2 :: [ souvent, souvent ]
axiologique :: 1 :: [ important ]
epistemique :: 1 :: [ important ]
marqueurs_niveau2 :: 1 :: [ spectaculaire ]
marqueurs_niveau1 :: 1 :: [ important ]
appreciatif_niveau2 :: 1 :: [ spectaculaire ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Confrontées aux réalisations françaises les plus spectaculaires (Massy-Antony , Sarcelles , Mourenx) , aux projets français les plus importants (Toulouse-Mirail , Caen-Herouville) ou encore à l ' exemple souvent cité des " new towns " britanniques des années 1950 (1) , ces villes nouvelles se caractérisent d ' abord par un changement d ' échelle , de 1 à 5 au minimum , et plus souvent de 1 à 10 : d ' un ensemble de 30 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::4::['des années 1950', 'encore', 'souvent', 'plus souvent']
adverbiaux_localisation_temporelle::1::['des années 1950']
adverbiaux_purement_iteratifs::2::['souvent', 'plus souvent']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_absolu::1::['des années 1950']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_loc_temp_focalisation_id::1::['des années 1950']
adverbiaux_loc_temp_regionalisation_id::1::['des années 1950']
adverbiaux_loc_temp_pointage_absolu::1::['des années 1950']
adverbiaux_iterateur_frequentiel::2::['souvent', 'plus souvent']
adverbiaux_dur_iter_relatif::1::['encore']

