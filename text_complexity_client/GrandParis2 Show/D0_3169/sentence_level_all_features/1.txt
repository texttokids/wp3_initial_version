
********** Entity Names Features **********
*******************************************

1. Sentence: - A un niveau intermédiaire , des centres urbains pouvant occuper quelques milliers d ' emplois regrouperont des commerces de fréquentation moins courante , tels que des magasins à prix uniques , des supermarchés , complétés par quelques services et les bureaux que peuvent occuper les entreprises les moins dépendantes du milieu d ' affaires .
Entity Name (Type) :: A (LOC)


********** Emotions Features **********
***************************************

1. Sentence: - A un niveau intermédiaire , des centres urbains pouvant occuper quelques milliers d ' emplois regrouperont des commerces de fréquentation moins courante , tels que des magasins à prix uniques , des supermarchés , complétés par quelques services et les bureaux que peuvent occuper les entreprises les moins dépendantes du milieu d ' affaires .
No features found.


********** Modality Features **********
***************************************

1. Sentence: - A un niveau intermédiaire , des centres urbains pouvant occuper quelques milliers d ' emplois regrouperont des commerces de fréquentation moins courante , tels que des magasins à prix uniques , des supermarchés , complétés par quelques services et les bureaux que peuvent occuper les entreprises les moins dépendantes du milieu d ' affaires .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ pouvoir, emploi, prix, unique, pouvoir, dépendant ]
appreciatif :: 4 :: [ emploi, prix, unique, dépendant ]
alethique :: 2 :: [ pouvoir, pouvoir ]
deontique :: 2 :: [ pouvoir, pouvoir ]
epistemique :: 2 :: [ pouvoir, pouvoir ]
marqueurs_niveau3 :: 2 :: [ pouvoir, pouvoir ]
marqueurs_niveau2 :: 2 :: [ emploi, prix ]
marqueurs_niveau1 :: 2 :: [ unique, dépendant ]
alethique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
appreciatif_niveau2 :: 2 :: [ emploi, prix ]
appreciatif_niveau1 :: 2 :: [ unique, dépendant ]
deontique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, pouvoir ]
axiologique :: 1 :: [ prix ]
axiologique_niveau2 :: 1 :: [ prix ]
Total feature counts: 34


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - A un niveau intermédiaire , des centres urbains pouvant occuper quelques milliers d ' emplois regrouperont des commerces de fréquentation moins courante , tels que des magasins à prix uniques , des supermarchés , complétés par quelques services et les bureaux que peuvent occuper les entreprises les moins dépendantes du milieu d ' affaires .
No features found.

