
********** Entity Names Features **********
*******************************************

1. Sentence: Ce choix d ' une vallée comme axe d ' urbanisation pouvait soulever une objection grave : les vallées constituent des sites naturels en nombre limité , d ' une grande valeur pour la beauté de leur site et l ' organisation de loisirs au bord de l ' eau .
Entity Name (Type) :: Ce (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ce choix d ' une vallée comme axe d ' urbanisation pouvait soulever une objection grave : les vallées constituent des sites naturels en nombre limité , d ' une grande valeur pour la beauté de leur site et l ' organisation de loisirs au bord de l ' eau .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ce choix d ' une vallée comme axe d ' urbanisation pouvait soulever une objection grave : les vallées constituent des sites naturels en nombre limité , d ' une grande valeur pour la beauté de leur site et l ' organisation de loisirs au bord de l ' eau .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ choix, pouvoir, soulever, grave, grand, loisir ]
marqueurs_niveau2 :: 3 :: [ choix, grave, loisir ]
appreciatif :: 2 :: [ grave, loisir ]
epistemique :: 2 :: [ pouvoir, grand ]
marqueurs_niveau3 :: 2 :: [ pouvoir, grand ]
appreciatif_niveau2 :: 2 :: [ grave, loisir ]
epistemique_niveau3 :: 2 :: [ pouvoir, grand ]
alethique :: 1 :: [ pouvoir ]
axiologique :: 1 :: [ grave ]
boulique :: 1 :: [ choix ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ soulever ]
marqueurs_niveau0 :: 1 :: [ soulever ]
alethique_niveau3 :: 1 :: [ pouvoir ]
axiologique_niveau2 :: 1 :: [ grave ]
boulique_niveau2 :: 1 :: [ choix ]
deontique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ soulever ]
Total feature counts: 30


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce choix d ' une vallée comme axe d ' urbanisation pouvait soulever une objection grave : les vallées constituent des sites naturels en nombre limité , d ' une grande valeur pour la beauté de leur site et l ' organisation de loisirs au bord de l ' eau .
No features found.

