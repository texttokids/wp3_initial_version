
********** Entity Names Features **********
*******************************************

1. Sentence: - Le " temps libre " - ainsi appelé faute d ' un meilleur terme (2) – va progressivement gagner sur le temps de travail professionnel , et surtout la demande , pour toutes les activités de loisirs , sera dans moins de quarante ans plus de quatre fois supérieure en valeur .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: - Le " temps libre " - ainsi appelé faute d ' un meilleur terme (2) – va progressivement gagner sur le temps de travail professionnel , et surtout la demande , pour toutes les activités de loisirs , sera dans moins de quarante ans plus de quatre fois supérieure en valeur .
Tokens having emotion: [ faute ]
Lemmas having emotion: [ faute ]
Categories of the emotion lemmas: [ culpabilite ]


********** Modality Features **********
***************************************

1. Sentence: - Le " temps libre " - ainsi appelé faute d ' un meilleur terme (2) – va progressivement gagner sur le temps de travail professionnel , et surtout la demande , pour toutes les activités de loisirs , sera dans moins de quarante ans plus de quatre fois supérieure en valeur .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ libre, faute, meilleur, aller, gagner, travail, loisir ]
appreciatif :: 6 :: [ libre, faute, meilleur, gagner, travail, loisir ]
marqueurs_niveau2 :: 4 :: [ faute, meilleur, gagner, loisir ]
appreciatif_niveau2 :: 4 :: [ faute, meilleur, gagner, loisir ]
axiologique :: 2 :: [ faute, meilleur ]
marqueurs_niveau1 :: 2 :: [ libre, aller ]
axiologique_niveau2 :: 2 :: [ faute, meilleur ]
alethique :: 1 :: [ aller ]
marqueurs_niveau3 :: 1 :: [ travail ]
alethique_niveau1 :: 1 :: [ aller ]
appreciatif_niveau3 :: 1 :: [ travail ]
appreciatif_niveau1 :: 1 :: [ libre ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: - Le " temps libre " - ainsi appelé faute d ' un meilleur terme (2) – va progressivement gagner sur le temps de travail professionnel , et surtout la demande , pour toutes les activités de loisirs , sera dans moins de quarante ans plus de quatre fois supérieure en valeur .
No features found.

