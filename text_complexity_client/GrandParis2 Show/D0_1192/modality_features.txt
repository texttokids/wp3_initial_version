====== Sentence Level Features ======

1. Sentence: Un nombre d ' axes limité à trois ou quatre doit permettre d ' éviter ce risque : c ' est en pratique - comme on le décrira plus loin (5) - sur un axe principal , double à la vérité , et sur deux axes secondaires , que sera " prise en compte " l ' expansion de l ' agglomération , et , pour le faire plus efficacement , ces axes seront , en certaines parties , tangents à cette agglomération .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ devoir, permettre, vérité, certain ]
marqueurs_niveau3 :: 3 :: [ devoir, permettre, certain ]
alethique :: 2 :: [ devoir, vérité ]
deontique :: 2 :: [ devoir, permettre ]
epistemique :: 2 :: [ devoir, certain ]
deontique_niveau3 :: 2 :: [ devoir, permettre ]
epistemique_niveau3 :: 2 :: [ devoir, certain ]
marqueurs_niveau1 :: 1 :: [ vérité ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau1 :: 1 :: [ vérité ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ devoir, permettre, vérité, certain ]
marqueurs_niveau3 :: 3 :: [ devoir, permettre, certain ]
alethique :: 2 :: [ devoir, vérité ]
deontique :: 2 :: [ devoir, permettre ]
epistemique :: 2 :: [ devoir, certain ]
deontique_niveau3 :: 2 :: [ devoir, permettre ]
epistemique_niveau3 :: 2 :: [ devoir, certain ]
marqueurs_niveau1 :: 1 :: [ vérité ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau1 :: 1 :: [ vérité ]
Total feature counts: 20

