
********** Entity Names Features **********
*******************************************

1. Sentence: Les axes selon lesquels il faut canaliser l ' expansion ne doivent pas être trop nombreux , sous peine de perdre les avantages de coût et de proximité de la nature qu ' offrent un tel principe , et de retrouver le processus décrit plus haut de la formation de doigts de gant suivie du comblement progressif des espaces intercalaires .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les axes selon lesquels il faut canaliser l ' expansion ne doivent pas être trop nombreux , sous peine de perdre les avantages de coût et de proximité de la nature qu ' offrent un tel principe , et de retrouver le processus décrit plus haut de la formation de doigts de gant suivie du comblement progressif des espaces intercalaires .
Tokens having emotion: [ peine ]
Lemmas having emotion: [ peine ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Les axes selon lesquels il faut canaliser l ' expansion ne doivent pas être trop nombreux , sous peine de perdre les avantages de coût et de proximité de la nature qu ' offrent un tel principe , et de retrouver le processus décrit plus haut de la formation de doigts de gant suivie du comblement progressif des espaces intercalaires .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ selon, falloir, devoir, peine, perdre, offrir, haut ]
appreciatif :: 3 :: [ peine, perdre, offrir ]
marqueurs_niveau3 :: 3 :: [ selon, falloir, devoir ]
marqueurs_niveau2 :: 3 :: [ peine, perdre, offrir ]
appreciatif_niveau2 :: 3 :: [ peine, perdre, offrir ]
deontique :: 2 :: [ falloir, devoir ]
epistemique :: 2 :: [ selon, devoir ]
deontique_niveau3 :: 2 :: [ falloir, devoir ]
epistemique_niveau3 :: 2 :: [ selon, devoir ]
alethique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ haut ]
marqueurs_niveau0 :: 1 :: [ haut ]
alethique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ haut ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les axes selon lesquels il faut canaliser l ' expansion ne doivent pas être trop nombreux , sous peine de perdre les avantages de coût et de proximité de la nature qu ' offrent un tel principe , et de retrouver le processus décrit plus haut de la formation de doigts de gant suivie du comblement progressif des espaces intercalaires .
No features found.

