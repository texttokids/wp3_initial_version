
********** Entity Names Features **********
*******************************************

1. Sentence: Leur caractère industriel est très accusé dans des quartiers vétustes où se juxtaposent souvent usines incommodes et habitations .
Entity Name (Type) :: Leur (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Leur caractère industriel est très accusé dans des quartiers vétustes où se juxtaposent souvent usines incommodes et habitations .
Tokens having emotion: [ incommodes ]
Lemmas having emotion: [ incommode ]
Categories of the emotion lemmas: [ deplaisir ]


********** Modality Features **********
***************************************

1. Sentence: Leur caractère industriel est très accusé dans des quartiers vétustes où se juxtaposent souvent usines incommodes et habitations .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ accuser, souvent ]
alethique :: 1 :: [ souvent ]
axiologique :: 1 :: [ accuser ]
marqueurs_niveau3 :: 1 :: [ souvent ]
marqueurs_niveau2 :: 1 :: [ accuser ]
alethique_niveau3 :: 1 :: [ souvent ]
axiologique_niveau2 :: 1 :: [ accuser ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Leur caractère industriel est très accusé dans des quartiers vétustes où se juxtaposent souvent usines incommodes et habitations .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

