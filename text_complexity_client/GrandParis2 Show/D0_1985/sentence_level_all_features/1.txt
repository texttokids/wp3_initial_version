
********** Entity Names Features **********
*******************************************

1. Sentence: Il est évident que les problèmes de circulation , de transport et de stationnement pour Paris intra-muros devront faire l ' objet d ' une étude d ' ensemble - aussi scientifique que possible – portant sur le trafic , dans son ampleur et dans ses conditions , prévisible pour les années 1975-1985 .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Il est évident que les problèmes de circulation , de transport et de stationnement pour Paris intra-muros devront faire l ' objet d ' une étude d ' ensemble - aussi scientifique que possible – portant sur le trafic , dans son ampleur et dans ses conditions , prévisible pour les années 1975-1985 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il est évident que les problèmes de circulation , de transport et de stationnement pour Paris intra-muros devront faire l ' objet d ' une étude d ' ensemble - aussi scientifique que possible – portant sur le trafic , dans son ampleur et dans ses conditions , prévisible pour les années 1975-1985 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ problème, devoir, possible ]
alethique :: 2 :: [ devoir, possible ]
marqueurs_niveau3 :: 2 :: [ devoir, possible ]
alethique_niveau3 :: 2 :: [ devoir, possible ]
appreciatif :: 1 :: [ problème ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ problème ]
appreciatif_niveau2 :: 1 :: [ problème ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il est évident que les problèmes de circulation , de transport et de stationnement pour Paris intra-muros devront faire l ' objet d ' une étude d ' ensemble - aussi scientifique que possible – portant sur le trafic , dans son ampleur et dans ses conditions , prévisible pour les années 1975-1985 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['les années 1975_1985']
adverbiaux_loc_temp_focalisation_id::1::['les années 1975_1985']
adverbiaux_loc_temp_regionalisation_id::1::['les années 1975_1985']

