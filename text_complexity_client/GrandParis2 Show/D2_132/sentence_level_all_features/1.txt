
********** Entity Names Features **********
*******************************************

1. Sentence: la présence d ' un aéroport détermine des zones de bruit , et celui-là qui sera pour les supersoniques a des zones de bruit qui s ’ étendront jusqu ’ à 25 kilomètres et ceci barrait l ’ urbanisation vers le Nord;
Entity Name (Type) :: Nord; (LOC)


********** Emotions Features **********
***************************************

1. Sentence: la présence d ' un aéroport détermine des zones de bruit , et celui-là qui sera pour les supersoniques a des zones de bruit qui s ’ étendront jusqu ’ à 25 kilomètres et ceci barrait l ’ urbanisation vers le Nord;
Tokens having emotion: [ détermine ]
Lemmas having emotion: [ déterminer ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: la présence d ' un aéroport détermine des zones de bruit , et celui-là qui sera pour les supersoniques a des zones de bruit qui s ’ étendront jusqu ’ à 25 kilomètres et ceci barrait l ’ urbanisation vers le Nord;
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: la présence d ' un aéroport détermine des zones de bruit , et celui-là qui sera pour les supersoniques a des zones de bruit qui s ’ étendront jusqu ’ à 25 kilomètres et ceci barrait l ’ urbanisation vers le Nord;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jusqu ’ à 25']
adverbiaux_duratifs_iteratifs::1::['jusqu ’ à 25']
adverbiaux_pointage_absolu::1::['jusqu ’ à 25']
adverbiaux_loc_temp_focalisation_id::1::['jusqu ’ à 25']
adverbiaux_loc_temp_regionalisation_non_id::1::['jusqu ’ à 25']
adverbiaux_dur_iter_absolu::1::['jusqu ’ à 25']

