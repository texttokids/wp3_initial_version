
********** Entity Names Features **********
*******************************************

1. Sentence: Le deuxième combat s ’ est placé sur le plateau de Trappes et ce qui est amusant d ' ailleurs c ’ est que c ’ est aussi le combat politique principal que nous aurons ;
Entity Name (Type) :: Trappes (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le deuxième combat s ’ est placé sur le plateau de Trappes et ce qui est amusant d ' ailleurs c ’ est que c ’ est aussi le combat politique principal que nous aurons ;
Tokens having emotion: [ amusant ]
Lemmas having emotion: [ amusant ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Le deuxième combat s ’ est placé sur le plateau de Trappes et ce qui est amusant d ' ailleurs c ’ est que c ’ est aussi le combat politique principal que nous aurons ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ combat, amusant, combat ]
marqueurs_niveau2 :: 3 :: [ combat, amusant, combat ]
axiologique :: 2 :: [ combat, combat ]
axiologique_niveau2 :: 2 :: [ combat, combat ]
appreciatif :: 1 :: [ amusant ]
appreciatif_niveau2 :: 1 :: [ amusant ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le deuxième combat s ’ est placé sur le plateau de Trappes et ce qui est amusant d ' ailleurs c ’ est que c ’ est aussi le combat politique principal que nous aurons ;
No features found.

