====== Sentence Level Features ======

1. Sentence: Dans la plupart de ces communes , des immeubles d ' habitations ont été édifiés depuis quelques années , ce qui pourrait laisser supposer que l ' urbanisation de ce secteur sera encore développée dans l ' avenir .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['depuis quelques années', 'encore']
adverbiaux_duratifs_iteratifs::2::['depuis quelques années', 'encore']
adverbiaux_pointage_non_absolu::2::['depuis quelques années', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['depuis quelques années']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis quelques années']
adverbiaux_dur_iter_deictique::1::['depuis quelques années']
adverbiaux_dur_iter_relatif::1::['encore']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['depuis quelques années', 'encore']
adverbiaux_duratifs_iteratifs::2::['depuis quelques années', 'encore']
adverbiaux_pointage_non_absolu::2::['depuis quelques années', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['depuis quelques années']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis quelques années']
adverbiaux_dur_iter_deictique::1::['depuis quelques années']
adverbiaux_dur_iter_relatif::1::['encore']
Total feature counts: 10

