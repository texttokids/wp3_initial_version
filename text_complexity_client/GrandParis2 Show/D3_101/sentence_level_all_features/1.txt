
********** Entity Names Features **********
*******************************************

1. Sentence: Il en résulte une tendance naturelle à l ' appauvrissement de la diversité et au mitage des paysages naturels , dégradation accrue par la multiplication des " zones d ' activité "  aux abords des villes , qui ne sont rien d ' autre qu ' une forme de scandale , comme si la ville laide en périphérie c ' était normal pour garder la ville superbe en son cœur : ce n ' est pas un raisonnement républicain .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il en résulte une tendance naturelle à l ' appauvrissement de la diversité et au mitage des paysages naturels , dégradation accrue par la multiplication des " zones d ' activité "  aux abords des villes , qui ne sont rien d ' autre qu ' une forme de scandale , comme si la ville laide en périphérie c ' était normal pour garder la ville superbe en son cœur : ce n ' est pas un raisonnement républicain .
Tokens having emotion: [ scandale ]
Lemmas having emotion: [ scandale ]
Categories of the emotion lemmas: [ colere ]


********** Modality Features **********
***************************************

1. Sentence: Il en résulte une tendance naturelle à l ' appauvrissement de la diversité et au mitage des paysages naturels , dégradation accrue par la multiplication des " zones d ' activité "  aux abords des villes , qui ne sont rien d ' autre qu ' une forme de scandale , comme si la ville laide en périphérie c ' était normal pour garder la ville superbe en son cœur : ce n ' est pas un raisonnement républicain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ rien, scandale, normal, superbe ]
alethique :: 1 :: [ rien ]
appreciatif :: 1 :: [ superbe ]
epistemique :: 1 :: [ normal ]
pas_d_indication :: 1 :: [ scandale ]
marqueurs_niveau3 :: 1 :: [ superbe ]
marqueurs_niveau2 :: 1 :: [ rien ]
marqueurs_niveau1 :: 1 :: [ normal ]
marqueurs_niveau0 :: 1 :: [ scandale ]
alethique_niveau2 :: 1 :: [ rien ]
appreciatif_niveau3 :: 1 :: [ superbe ]
epistemique_niveau1 :: 1 :: [ normal ]
pas_d_indication_niveau0 :: 1 :: [ scandale ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il en résulte une tendance naturelle à l ' appauvrissement de la diversité et au mitage des paysages naturels , dégradation accrue par la multiplication des " zones d ' activité "  aux abords des villes , qui ne sont rien d ' autre qu ' une forme de scandale , comme si la ville laide en périphérie c ' était normal pour garder la ville superbe en son cœur : ce n ' est pas un raisonnement républicain .
No features found.

