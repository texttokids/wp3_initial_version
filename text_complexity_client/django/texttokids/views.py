from django.views.generic import TemplateView

class HomePageView(TemplateView):
    template_name = 'home.html'

from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import requires_csrf_token

from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from django.contrib import messages

def handle_uploaded_file(f):
    with open('datatoprocess.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

import requests
import json
import sys

sys.path.insert(1, "../../text_complexity_server/alternative/rashedur/")
from text_processor import alternative

def request_session_get_secure(request, key):
    if key not in request.session:
        return ''
    else:
        return request.session.get(key)

from .forms import ChainForm
@csrf_exempt
@csrf_protect
@requires_csrf_token
def chain(request):
#    form = ChainForm()
#    return render(request,'chain.html',{'form':form})

    if request.method == 'POST':
        # POST, generate form with data from the request
        form = ChainForm(request.POST)
        #form = ChainForm(request.POST, request.FILES)
        # check if it's valid:
        if form.is_valid():
            # handle_uploaded_file(request.FILES['file'])
            # process data, insert into DB, generate email,etc
            choice_levels = form.cleaned_data['levels']
            choice_processors = form.cleaned_data['processors']
            choice_text = form.cleaned_data['text']

            # fetch processing chain
            data = { 'text': choice_text, 'processors': choice_processors, 'levels': choice_levels }

            server_address = 'http://localhost:8081' # Warning - put localhost for distribution
            #server_address = 'http://vheborto-corliapi.inist.fr:8081' # Warning - put vheborto for testing outside vheborto

            response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)
            data_json = response.json()
            str_response = repr(data_json)
            request.session['origin'] = 'chain'
            request.session['data_json_chain'] = data_json
            request.session['data_levels_chain'] = choice_levels
            request.session['data_processors_chain'] = choice_processors
            request.session['data_text_chain'] = choice_text
            request.session['str_output_chain'] = json.dumps(data_json, indent=4, ensure_ascii=False)
            request.session['str_levels_chain'] = ','.join(choice_levels)
            request.session['str_processors_chain'] = ','.join(choice_processors)
            # redirect to a new URL:
            return HttpResponseRedirect('/results')
#            return render(request,'results.html', { 'form':formresult, 'str_levels': repr(choice_levels),
#                'str_processors': repr(choice_processors), 'str_results': str_response })
    else:
        # GET, generate blank form
        if ('data_text_chain' in request.session):
            form = ChainForm(initial={'levels': 'sentence', 'text': request.session['data_text_chain']})
        else:
            form = ChainForm(initial={'levels': 'sentence'})
    return render(request,'chain.html',{'form':form})

from .forms import SemanticsForm
@csrf_exempt
@csrf_protect
@requires_csrf_token
def semantics(request):
#    form = ChainForm()
#    return render(request,'chain.html',{'form':form})

    if request.method == 'POST':
        # POST, generate form with data from the request
        form = SemanticsForm(request.POST)
        #form = ChainForm(request.POST, request.FILES)
        # check if it's valid:
        if form.is_valid():
            # handle_uploaded_file(request.FILES['file'])
            # process data, insert into DB, generate email,etc
            choice_levels = form.cleaned_data['levels']
            choice_processors = form.cleaned_data['processors']
            choice_text = form.cleaned_data['text']
            # write a file with the content of the input
            with open("../data_alternative/text_input.txt", 'w') as fh:
                fh.write(form.cleaned_data['text'])
                fh.close()
            alternative("../../text_complexity_server/alternative/rashedur/",
                "../../../text_complexity_client/data_alternative/",
                "../../../text_complexity_client/data_alternative/",
                "text_input.txt",
                "output_alt.zip", choice_levels, choice_processors)
            request.session['origin'] = 'semantics'
            request.session['data_json_sem'] = "see the content of the zip file"
            request.session['data_levels_sem'] = choice_levels
            request.session['data_processors_sem'] = choice_processors
            request.session['data_text_sem'] = choice_text
            request.session['str_output_sem'] = "see the content of the zip file"
            request.session['str_levels_sem'] = ','.join(choice_levels)
            request.session['str_processors_sem'] = ','.join(choice_processors)
                # redirect to a new URL:
            return HttpResponseRedirect('/results-semantics')

    else:
        # GET, generate blank form
        if ('data_text_sem' in request.session):
            form = SemanticsForm(initial={'levels': 'sentence', 'processors': 'emotions', 'text': request.session['data_text_sem']})
        else:
            form = SemanticsForm(initial={'levels': 'sentence', 'processors': 'emotions'})
    return render(request,'semantics.html',{'form':form})

import os
from django.conf import settings
from django.http import HttpResponse, Http404

def download(file_path):
    #file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/text")
            response['Content-Disposition'] = 'inline; filename=' + file_path
            return response
    raise Http404

from django.http import FileResponse

def send_file(name):
    img = open(name, 'rb')
    response = FileResponse(img)
    return response

def create_name(levs, procs):
    name = '_'.join(levs)
    for l in procs:
        name = name + '_' + l
    return name

from . import writecsv
from .forms import ResultsForm
def results(request):
    if request.method == 'POST':
        # POST, generate form with data from the request
        form = ResultsForm(request.POST)
        # check if it's valid:
        if form.is_valid():
            # save data
            #messages.add_message(request, messages.INFO, 'Sauvegarde sous format [' + form.cleaned_data['action'] + ']')
            if form.cleaned_data['action'] == 'JSON':
                #messages.add_message(request, messages.INFO, 'Sauvegarde JSON ' + request_session_get_secure(request, 'str_levels') + request_session_get_secure(request, 'str_processors'))
                response = HttpResponse( request_session_get_secure(request, 'str_output_chain'), content_type="application/txt")
                response['Content-Disposition'] = 'inline; filename=' + create_name(request_session_get_secure(request, 'data_levels_chain'), request_session_get_secure(request, 'data_processors_chain')) + '.json'
                return response
            else:
                #messages.add_message(request, messages.INFO, 'Sauvegarde CSV+XLSX ' + request_session_get_secure(request, 'str_levels') + request_session_get_secure(request, 'str_processors'))
                list_files = writecsv.write_analysis_result('./data', 0, 'textfield', request_session_get_secure(request, 'data_json_chain'))
                writecsv.write_xlsx_file("temporary.xlsx", list_files)
                # remove files ?
                for tfn in list_files:
                    os.remove(tfn)
                    print("remove: " + tfn)
                with open("temporary.xlsx", 'rb') as fh:
                    response = HttpResponse( fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + create_name(request_session_get_secure(request, 'data_levels_chain'), request_session_get_secure(request, 'data_processors_chain')) + '.xlsx'
                    return response

    else:
        # GET, generate blank form
        form = ResultsForm()

    if ('str_output_chain' in request.session):
        json_html = request_session_get_secure(request, 'str_output_chain').replace('\n', '<br/>').replace('    ','&nbsp;&nbsp;&nbsp;&nbsp;')
    else:
        json_html = ''
    return render(request,'results.html',{'form':form,
        'str_levels': request_session_get_secure(request, 'str_levels_chain'),
        'str_processors': request_session_get_secure(request, 'str_processors_chain'),
        'str_output': json_html })


from .forms import ResultsSemanticsForm
def results_semantics(request):
    if request.method == 'POST':
        # POST, generate form with data from the request
        form = ResultsSemanticsForm(request.POST)
        # check if it's valid:
        if form.is_valid():
            # save data
            # get the ZIP file
            #messages.add_message(request, messages.INFO, 'Sauvegarde ZIP des extractions sémantiques')
            with open("../data_alternative/output_alt.zip", 'rb') as fh:
                response = HttpResponse( fh.read(), content_type="application/zip")
                response['Content-Disposition'] = 'inline; filename=alternative.zip'
                return response

    else:
        # GET, generate blank form
        form = ResultsSemanticsForm()

    str_text = request_session_get_secure(request, 'str_output_sem').replace('\n', '<br/>').replace('    ','&nbsp;&nbsp;&nbsp;&nbsp;')
    return render(request,'results-semantics.html',{'form':form,
        'str_levels': request_session_get_secure(request, 'str_levels_sem'),
        'str_processors': request_session_get_secure(request, 'str_processors_sem'),
        'str_output': str_text })
