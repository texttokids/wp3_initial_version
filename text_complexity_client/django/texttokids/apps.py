from django.apps import AppConfig

class TexttokidsConfig(AppConfig):
    name = 'texttokids'
