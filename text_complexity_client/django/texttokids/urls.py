from django.urls import path
from .views import HomePageView
from . import views

urlpatterns = [
    path('chain/', views.chain, name='chain'),
    path('semantics/', views.semantics, name='semantics'),
    path('results/', views.results, name='results'),
    path('results-semantics/', views.results_semantics, name='results-semantics'),
    path('', HomePageView.as_view(), name='home'),
]
