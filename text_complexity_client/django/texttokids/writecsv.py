import os
from os.path import exists
import csv

def write_text_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                if descriptor_field != 'text_id':
                    titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        raw_values = [text_id, input_file_path.replace('\\', '/')]
        for analyzed_element in descriptor_block_content:
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
        csv_file_writer.writerow(raw_values)


def write_sentence_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
       
        for analyzed_element in descriptor_block_content:
            raw_values = [text_id, input_file_path.replace('\\', '/')]
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
            csv_file_writer.writerow(raw_values)


def get_analysis_block_result_type(descriptor_block_content):

    analysis_block_type = 'unknown'
    if len(descriptor_block_content) > 0:
        for descriptor_field in descriptor_block_content[0].keys():
            if descriptor_field == 'sent_id':
                analysis_block_type = 'sentence'
                break
            elif descriptor_field == 'text_id':
                analysis_block_type = 'text'
                break
    return analysis_block_type


def write_analysis_result(output_folder: str, text_id: int, input_file_path: str, analysis):
    if not hasattr(analysis, "keys"):
        print('no results for ', input_file_path)
        return []
    
    list_result_files = []

    for descriptor_block_key in analysis.keys():
        output_file_path = output_folder + '/' + descriptor_block_key + '.csv'
        list_result_files.append(output_file_path)

        analysis_block_type = get_analysis_block_result_type(analysis[descriptor_block_key])

        if analysis_block_type == 'text':
            write_text_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])
        elif analysis_block_type == 'sentence':
            write_sentence_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])
    return list_result_files

import pandas as pd
import sys
import os

def write_xlsx_file(xlsxfile: str, list_files):
    writer = pd.ExcelWriter(xlsxfile)
    for csvfilename in list_files:
        df = pd.read_csv(csvfilename, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        df.to_excel(writer,sheet_name=csvfilename[7:].replace('/', ''))
    writer.close()
