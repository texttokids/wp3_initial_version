# texttokids/tests.py
from django.test import SimpleTestCase
class SimpleTests(SimpleTestCase):
    def test_home_page_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_chain_page_status_code(self):
        response = self.client.get('/chain/')
        self.assertEqual(response.status_code, 200)

    def test_results_page_status_code(self):
        response = self.client.get('/results/')
        self.assertEqual(response.status_code, 200)