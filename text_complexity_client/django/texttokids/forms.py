# forms.py in app named 'contact'
from django import forms

class ChainForm(forms.Form):
    LEVELS = (
        ('text', 'Texte'),
        ('sentence', 'Phrases'),
    )

    PROCESSORS = (
        ("graphie", "Graphie"),
        ("phonetique", "Phonétique"),
        ("niveau_lexical", "Niveau lexical"),
        ("richesse_lexicale", "Richesse lexicale"),
        ("adjectifs_ordinaux", "Adjectifs ordinaux"),
        ("flexions_verbales", "Flexions verbales"),
        ("parties_du_discours", "Parties du discours"),
        ("pronoms", "Pronoms"),
        ("pluriels", "Pluriels"),
        ("dependances_syntaxiques", "Dépendances syntaxiques"),
        ("structures_passives", "Structures passives"),
        ("structures_syntaxiques", "Structures syntaxiques"),
        ("superlatifs_inferiorite", "Superlatifs inferiorite"),
        ("adverbiaux_temporels", "Adverbiaux temporels"),
        ("modalite", "Modalité"),
        ("emotions", "Emotions"),
        ("emotyc", "Emotyc"),
        ("connecteurs_organisateurs", "Connecteurs organisateurs"),
        ("entites_nommees", "Entités nommées"),
        ("propositions_subordonnees", "Propositions subordonnées"),
        ("metaphores", "Métaphores"),
        ("age", "Age"),
        ("formules_lisibilite_standard", "Formules lisibilité standard"),
    )

    levels = forms.MultipleChoiceField(label='Texte ou/et Phrase', choices=LEVELS)
    processors = forms.MultipleChoiceField(label='Les processeurs...', choices=PROCESSORS) # widget=forms.CheckboxSelectMultiple
    text = forms.CharField(label='Texte à analyser', widget=forms.Textarea)
    #file = forms.FileField(label='Fichier à analyser')

class SemanticsForm(forms.Form):
    LEVELS = (
        ('text', 'Texte'),
        ('sentence', 'Phrases'),
    )

    SEMANTIC_PROCESSORS = (
        ("adverbiaux_temporels", "Adverbiaux temporels"),
        ("emotions", "Emotions"),
        ("emotyc", "Emotyc"),
        ("entity_names", "Entités nommées"),
        ("modality", "Modalité"),
        ("metaphores", "Métaphores"),
        ("connecteurs_organisateurs", "Connecteurs organisateurs"),
        ("propositions_subordonnees", "Propositions subordonnées"),
    )

    levels = forms.MultipleChoiceField(label='Texte ou/et Phrase', choices=LEVELS)
    processors = forms.MultipleChoiceField(label='Les processeurs...', choices=SEMANTIC_PROCESSORS) # widget=forms.CheckboxSelectMultiple
    text = forms.CharField(label='Texte à analyser', widget=forms.Textarea)
    #file = forms.FileField(label='Fichier à analyser')

class ResultsForm(forms.Form):
    SAVEAS = (
        ('XLSX', 'XLSX'),
        ('JSON', 'JSON'),
    )
    data_json = ''
    data_levels = []
    data_processors = []
    action = forms.ChoiceField(label='Enregistrer les résultats sous le format:', choices=SAVEAS)

class ResultsSemanticsForm(forms.Form):
    data_json = ''
    data_levels = []
    data_processors = []
