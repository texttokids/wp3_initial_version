
********** Entity Names Features **********
*******************************************

1. Sentence: J ' aime bien À bâtons rompus Entretien avec François Ascher par Ariella Masboungi les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009 , avec la collaboration d ' Olivia Barbet-Massin . défricher .
Entity Name (Type) :: Olivia Barbet-Massin (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' aime bien À bâtons rompus Entretien avec François Ascher par Ariella Masboungi les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009 , avec la collaboration d ' Olivia Barbet-Massin . défricher .
Tokens having emotion: [ aime ]
Lemmas having emotion: [ aimer ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: J ' aime bien À bâtons rompus Entretien avec François Ascher par Ariella Masboungi les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009 , avec la collaboration d ' Olivia Barbet-Massin . défricher .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ aimer, bien, collaboration ]
marqueurs_niveau3 :: 3 :: [ aimer, bien, collaboration ]
appreciatif :: 2 :: [ aimer, bien ]
axiologique :: 2 :: [ bien, collaboration ]
appreciatif_niveau3 :: 2 :: [ aimer, bien ]
axiologique_niveau3 :: 2 :: [ bien, collaboration ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' aime bien À bâtons rompus Entretien avec François Ascher par Ariella Masboungi les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009 , avec la collaboration d ' Olivia Barbet-Massin . défricher .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']
adverbiaux_localisation_temporelle::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']
adverbiaux_pointage_absolu::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']
adverbiaux_loc_temp_focalisation_id::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']
adverbiaux_loc_temp_regionalisation_id::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']
adverbiaux_loc_temp_pointage_absolu::1::['les 30 avril , 7 mai , 14 mai , 19 mai , 27 mai et 3 juin 2009']

