====== Sentence Level Features ======

1. Sentence: *** Partant de la complexité du système hydraulique et du besoin d ' un territoire plus sûr , nous nous rapprochons des questions plus ardues d ' «éthique de la terre» qui remettent en cause notre rôle traditionnel d ' «agents géologiques» , capables de changer la composition du sol , les régimes hydrauliques et le climat lui-même .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ partir, besoin, sûr ]
alethique :: 2 :: [ partir, besoin ]
boulique :: 1 :: [ besoin ]
epistemique :: 1 :: [ sûr ]
marqueurs_niveau3 :: 1 :: [ besoin ]
marqueurs_niveau2 :: 1 :: [ sûr ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau3 :: 1 :: [ besoin ]
alethique_niveau1 :: 1 :: [ partir ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau2 :: 1 :: [ sûr ]
Total feature counts: 14


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ partir, besoin, sûr ]
alethique :: 2 :: [ partir, besoin ]
boulique :: 1 :: [ besoin ]
epistemique :: 1 :: [ sûr ]
marqueurs_niveau3 :: 1 :: [ besoin ]
marqueurs_niveau2 :: 1 :: [ sûr ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau3 :: 1 :: [ besoin ]
alethique_niveau1 :: 1 :: [ partir ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau2 :: 1 :: [ sûr ]
Total feature counts: 14

