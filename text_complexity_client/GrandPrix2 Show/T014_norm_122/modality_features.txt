====== Sentence Level Features ======

1. Sentence: À force de s ' enfermer dans des périmètres de procédures , on finit par avoir des périmètres mentaux et par perdre de vue l ' essentiel : concentrer la matière grise , les efforts , les ressources et les talents sur la seule qualité des espaces de projets .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ force, enfermer, perdre, talent ]
pas_d_indication :: 3 :: [ force, enfermer, talent ]
marqueurs_niveau0 :: 3 :: [ force, enfermer, talent ]
pas_d_indication_niveau0 :: 3 :: [ force, enfermer, talent ]
appreciatif :: 1 :: [ perdre ]
marqueurs_niveau2 :: 1 :: [ perdre ]
appreciatif_niveau2 :: 1 :: [ perdre ]
Total feature counts: 16


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ force, enfermer, perdre, talent ]
pas_d_indication :: 3 :: [ force, enfermer, talent ]
marqueurs_niveau0 :: 3 :: [ force, enfermer, talent ]
pas_d_indication_niveau0 :: 3 :: [ force, enfermer, talent ]
appreciatif :: 1 :: [ perdre ]
marqueurs_niveau2 :: 1 :: [ perdre ]
appreciatif_niveau2 :: 1 :: [ perdre ]
Total feature counts: 16

