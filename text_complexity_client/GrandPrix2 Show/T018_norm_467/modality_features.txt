====== Sentence Level Features ======

1. Sentence: En tant que maître d ' œuvre , nous questionnons sans relâche notre sujet , nous sommes “inquiets” , au sens où nous ne savons pas d ' emblée ce qu ' il faut faire ni comment le faire avec exactitude .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ inquiet, savoir, falloir ]
epistemique :: 2 :: [ inquiet, savoir ]
marqueurs_niveau3 :: 2 :: [ savoir, falloir ]
appreciatif :: 1 :: [ inquiet ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ inquiet ]
appreciatif_niveau2 :: 1 :: [ inquiet ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ savoir ]
epistemique_niveau2 :: 1 :: [ inquiet ]
Total feature counts: 14


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ inquiet, savoir, falloir ]
epistemique :: 2 :: [ inquiet, savoir ]
marqueurs_niveau3 :: 2 :: [ savoir, falloir ]
appreciatif :: 1 :: [ inquiet ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ inquiet ]
appreciatif_niveau2 :: 1 :: [ inquiet ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ savoir ]
epistemique_niveau2 :: 1 :: [ inquiet ]
Total feature counts: 14

