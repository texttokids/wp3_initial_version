
********** Entity Names Features **********
*******************************************

1. Sentence: Georges Descombes Georges Descombes , en charge du paysage à l ' Institut d ' architecture , paysagiste au travail rigoureux et déterminé , se moque amicalement de nous : «Vous , les Français , avez théorisé l ' impuissance .
Entity Name (Type) :: Français (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Georges Descombes Georges Descombes , en charge du paysage à l ' Institut d ' architecture , paysagiste au travail rigoureux et déterminé , se moque amicalement de nous : «Vous , les Français , avez théorisé l ' impuissance .
Tokens having emotion: [ déterminé, amicalement ]
Lemmas having emotion: [ déterminé, amicalement ]
Categories of the emotion lemmas: [ audace, amour ]


********** Modality Features **********
***************************************

1. Sentence: Georges Descombes Georges Descombes , en charge du paysage à l ' Institut d ' architecture , paysagiste au travail rigoureux et déterminé , se moque amicalement de nous : «Vous , les Français , avez théorisé l ' impuissance .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ travail, moquer ]
appreciatif :: 1 :: [ travail ]
pas_d_indication :: 1 :: [ moquer ]
marqueurs_niveau3 :: 1 :: [ travail ]
marqueurs_niveau0 :: 1 :: [ moquer ]
appreciatif_niveau3 :: 1 :: [ travail ]
pas_d_indication_niveau0 :: 1 :: [ moquer ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Georges Descombes Georges Descombes , en charge du paysage à l ' Institut d ' architecture , paysagiste au travail rigoureux et déterminé , se moque amicalement de nous : «Vous , les Français , avez théorisé l ' impuissance .
No features found.

