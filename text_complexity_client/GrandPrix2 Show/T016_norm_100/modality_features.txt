====== Sentence Level Features ======

1. Sentence: Comment un territoire qui traverse un moment critique du point de vue économique , social et environnemental — tel celui de Bruxelles ou de la ville diffuse vénitienne — peut-il renverser en opportunités , c ' est-à-dire en possibilités , certaines de ses supposées limites , inhérentes aux caractères de la dispersion de longue durée , à ses diverses morphologies , à la structure de son support infrastructurel ?
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, possibilité, certain, long ]
epistemique :: 3 :: [ pouvoir, certain, long ]
marqueurs_niveau3 :: 3 :: [ pouvoir, possibilité, certain ]
alethique :: 2 :: [ pouvoir, possibilité ]
alethique_niveau3 :: 2 :: [ pouvoir, possibilité ]
epistemique_niveau3 :: 2 :: [ pouvoir, certain ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ long ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, possibilité, certain, long ]
epistemique :: 3 :: [ pouvoir, certain, long ]
marqueurs_niveau3 :: 3 :: [ pouvoir, possibilité, certain ]
alethique :: 2 :: [ pouvoir, possibilité ]
alethique_niveau3 :: 2 :: [ pouvoir, possibilité ]
epistemique_niveau3 :: 2 :: [ pouvoir, certain ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ long ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 20

