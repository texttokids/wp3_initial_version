
********** Entity Names Features **********
*******************************************

1. Sentence: Cette grande échelle de perception , que je trouve importante , manque presque toujours dans les périphéries .
Entity Name (Type) :: Cette (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette grande échelle de perception , que je trouve importante , manque presque toujours dans les périphéries .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Cette grande échelle de perception , que je trouve importante , manque presque toujours dans les périphéries .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ grand, important, toujours ]
epistemique :: 2 :: [ grand, important ]
marqueurs_niveau3 :: 2 :: [ grand, toujours ]
alethique :: 1 :: [ toujours ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
marqueurs_niveau1 :: 1 :: [ important ]
alethique_niveau3 :: 1 :: [ toujours ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau3 :: 1 :: [ grand ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette grande échelle de perception , que je trouve importante , manque presque toujours dans les périphéries .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

