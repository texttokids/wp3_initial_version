====== Sentence Level Features ======

1. Sentence: J ’ en garde le souvenir de longues matinées de recherches , le sentiment d ’ avoir trouvé quelques pépites (ces plans bien droits , peignés par le mouvement , lissés par l ’ architecture des Lumières que le siècle suivant allait leur emprunter) , mes après-midi se passant à faire exactement l ’ inverse et à décortiquer Paris pour tenter de comprendre comment cette ville sans plan avait pu devenir à ce point extraordinaire , au point qu ’ une simple cour , un bout de mur , permettent de l ’ identifier .
Entity Name (Type) :: Paris (LOC)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
Lumières (PER)
Paris (LOC)
Total entitiy counts: 2

