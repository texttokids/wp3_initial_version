
********** Entity Names Features **********
*******************************************

1. Sentence: La fixation de normes , d ' obligations de résultats , est impérative mais doit s ' inscrire dans une démarche répondant aux conditions de la satisfaction d ' usage , d ' un nouveau plaisir de vie en ville , comme de la maîtrise de coûts de production .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La fixation de normes , d ' obligations de résultats , est impérative mais doit s ' inscrire dans une démarche répondant aux conditions de la satisfaction d ' usage , d ' un nouveau plaisir de vie en ville , comme de la maîtrise de coûts de production .
Tokens having emotion: [ satisfaction, plaisir ]
Lemmas having emotion: [ satisfaction, plaisir ]
Categories of the emotion lemmas: [ joie, joie ]


********** Modality Features **********
***************************************

1. Sentence: La fixation de normes , d ' obligations de résultats , est impérative mais doit s ' inscrire dans une démarche répondant aux conditions de la satisfaction d ' usage , d ' un nouveau plaisir de vie en ville , comme de la maîtrise de coûts de production .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ obligation, devoir, satisfaction, plaisir ]
marqueurs_niveau3 :: 3 :: [ obligation, devoir, plaisir ]
appreciatif :: 2 :: [ satisfaction, plaisir ]
deontique :: 2 :: [ obligation, devoir ]
deontique_niveau3 :: 2 :: [ obligation, devoir ]
alethique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ satisfaction ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ plaisir ]
appreciatif_niveau2 :: 1 :: [ satisfaction ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La fixation de normes , d ' obligations de résultats , est impérative mais doit s ' inscrire dans une démarche répondant aux conditions de la satisfaction d ' usage , d ' un nouveau plaisir de vie en ville , comme de la maîtrise de coûts de production .
No features found.

