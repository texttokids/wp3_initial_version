
********** Entity Names Features **********
*******************************************

1. Sentence: À ma grande honte , je dois dire n ' avoir jamais compris ce choix et , très sincèrement , je ne pense pas que mon projet était incontournable , mais que l ' on déqualifie de la sorte ce site , en ne le considérant pas pour ce qu ' il était , un magnifique lieu d ' accueil , cela m ' a , au fond , bouleversé .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: À ma grande honte , je dois dire n ' avoir jamais compris ce choix et , très sincèrement , je ne pense pas que mon projet était incontournable , mais que l ' on déqualifie de la sorte ce site , en ne le considérant pas pour ce qu ' il était , un magnifique lieu d ' accueil , cela m ' a , au fond , bouleversé .
Tokens having emotion: [ honte, considérant ]
Lemmas having emotion: [ honte, considérer ]
Categories of the emotion lemmas: [ embarras, admiration ]


********** Modality Features **********
***************************************

1. Sentence: À ma grande honte , je dois dire n ' avoir jamais compris ce choix et , très sincèrement , je ne pense pas que mon projet était incontournable , mais que l ' on déqualifie de la sorte ce site , en ne le considérant pas pour ce qu ' il était , un magnifique lieu d ' accueil , cela m ' a , au fond , bouleversé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ grand, honte, devoir, dire, jamais, choix, penser, considérer, magnifique ]
marqueurs_niveau3 :: 7 :: [ grand, devoir, dire, jamais, penser, considérer, magnifique ]
epistemique :: 4 :: [ grand, devoir, penser, considérer ]
epistemique_niveau3 :: 4 :: [ grand, devoir, penser, considérer ]
alethique :: 2 :: [ devoir, jamais ]
boulique :: 2 :: [ dire, choix ]
marqueurs_niveau2 :: 2 :: [ honte, choix ]
alethique_niveau3 :: 2 :: [ devoir, jamais ]
appreciatif :: 1 :: [ magnifique ]
axiologique :: 1 :: [ honte ]
deontique :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ magnifique ]
axiologique_niveau2 :: 1 :: [ honte ]
boulique_niveau3 :: 1 :: [ dire ]
boulique_niveau2 :: 1 :: [ choix ]
deontique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 40


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À ma grande honte , je dois dire n ' avoir jamais compris ce choix et , très sincèrement , je ne pense pas que mon projet était incontournable , mais que l ' on déqualifie de la sorte ce site , en ne le considérant pas pour ce qu ' il était , un magnifique lieu d ' accueil , cela m ' a , au fond , bouleversé .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jamais']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_iterateur_frequentiel::1::['jamais']

