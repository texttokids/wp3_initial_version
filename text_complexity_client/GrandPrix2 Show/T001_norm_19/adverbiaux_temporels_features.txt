====== Sentence Level Features ======

1. Sentence: On n ' enlèvera donc rien aux mérites de Roland Castro — coups de cœur et tapages , réussites et loupés — , à ceux de Bohigas disant que la ville commence avec l ' art de poser les pavés : la Courneuve n ' aurait sans eux ni les habitations de Serge et Lipa qu ' il faut endurer (Billancourt) , et celle de la navigation de plus en plus acrobatique entre une théorie conciliante qui cherche à retrouver la ville , à modifier l ' état des choses ou à le conforter , et puis une autre , anti-utopique et branchée , pour laquelle la métropole n ' est rien d ' autre que ce qu ' elle est : bigarrée , mal foutue et — la photographie aidant — faite d ' esquisses attachantes auxquelles il faut tenter de s ' accro cher .
No features found.


====== Text Level Features ======

No features found.

