
********** Entity Names Features **********
*******************************************

1. Sentence: Cela se ressent sur des indicateurs précis  : chômage , niveau de revenus , santé .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Cela se ressent sur des indicateurs précis  : chômage , niveau de revenus , santé .
Tokens having emotion: [ ressent ]
Lemmas having emotion: [ ressentir ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Cela se ressent sur des indicateurs précis  : chômage , niveau de revenus , santé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ chômage, santé ]
appreciatif :: 1 :: [ chômage ]
pas_d_indication :: 1 :: [ santé ]
marqueurs_niveau2 :: 1 :: [ chômage ]
marqueurs_niveau0 :: 1 :: [ santé ]
appreciatif_niveau2 :: 1 :: [ chômage ]
pas_d_indication_niveau0 :: 1 :: [ santé ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cela se ressent sur des indicateurs précis  : chômage , niveau de revenus , santé .
No features found.

