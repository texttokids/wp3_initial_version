
********** Entity Names Features **********
*******************************************

1. Sentence: Deuxième prix au concours : succès d ' estime , début des publications nationales et internationales , premières conférences , sentiment d ' être vraiment au cœur du sujet… désillusions .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Deuxième prix au concours : succès d ' estime , début des publications nationales et internationales , premières conférences , sentiment d ' être vraiment au cœur du sujet… désillusions .
Tokens having emotion: [ estime, sentiment, désillusions ]
Lemmas having emotion: [ estime, sentiment, désillusion ]
Categories of the emotion lemmas: [ admiration, non_specifiee, deplaisir ]


********** Modality Features **********
***************************************

1. Sentence: Deuxième prix au concours : succès d ' estime , début des publications nationales et internationales , premières conférences , sentiment d ' être vraiment au cœur du sujet… désillusions .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ prix, succès, vraiment ]
appreciatif :: 2 :: [ prix, succès ]
marqueurs_niveau2 :: 2 :: [ prix, succès ]
appreciatif_niveau2 :: 2 :: [ prix, succès ]
alethique :: 1 :: [ vraiment ]
axiologique :: 1 :: [ prix ]
marqueurs_niveau1 :: 1 :: [ vraiment ]
alethique_niveau1 :: 1 :: [ vraiment ]
axiologique_niveau2 :: 1 :: [ prix ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Deuxième prix au concours : succès d ' estime , début des publications nationales et internationales , premières conférences , sentiment d ' être vraiment au cœur du sujet… désillusions .
No features found.

