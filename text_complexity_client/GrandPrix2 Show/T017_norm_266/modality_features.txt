====== Sentence Level Features ======

1. Sentence: Je m ' inspire en cela de Pierre Legendre 1 qui évoque dans sa conférence intitulée  : « À la jeunesse désireuse de Loi » ,  l ' inscription de la société dans une tradition judéo-romano-chrétienne plongeant ses racines dans un droit inné , nécessaire à la vie , transmis oralement puis recensé dans le Talmud et la Thora , transcrit dans le droit romain et enfin repris par la chrétienté .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ loi, droit, nécessaire, droit ]
deontique :: 3 :: [ loi, droit, droit ]
marqueurs_niveau3 :: 3 :: [ droit, nécessaire, droit ]
deontique_niveau3 :: 2 :: [ droit, droit ]
alethique :: 1 :: [ nécessaire ]
marqueurs_niveau2 :: 1 :: [ loi ]
alethique_niveau3 :: 1 :: [ nécessaire ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 16


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ loi, droit, nécessaire, droit ]
deontique :: 3 :: [ loi, droit, droit ]
marqueurs_niveau3 :: 3 :: [ droit, nécessaire, droit ]
deontique_niveau3 :: 2 :: [ droit, droit ]
alethique :: 1 :: [ nécessaire ]
marqueurs_niveau2 :: 1 :: [ loi ]
alethique_niveau3 :: 1 :: [ nécessaire ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 16

