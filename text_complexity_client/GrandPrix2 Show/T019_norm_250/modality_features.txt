====== Sentence Level Features ======

1. Sentence: Son anticonformisme nous manque , lui qui avait osé monter l ' Institut de la ville en mouvement (IMV) avec PSA à un moment où l ' automobile était (déjà) accusée de tous les maux , lui qui ne détestait pas un zeste de provocation pour faire avancer les causes auxquelles il tenait .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ lui, oser, accuser, mal, lui, détester, provocation ]
marqueurs_niveau3 :: 4 :: [ lui, mal, lui, détester ]
appreciatif :: 3 :: [ oser, mal, détester ]
axiologique :: 3 :: [ oser, accuser, mal ]
alethique :: 2 :: [ lui, lui ]
epistemique :: 2 :: [ lui, lui ]
marqueurs_niveau2 :: 2 :: [ oser, accuser ]
alethique_niveau3 :: 2 :: [ lui, lui ]
appreciatif_niveau3 :: 2 :: [ mal, détester ]
axiologique_niveau2 :: 2 :: [ oser, accuser ]
epistemique_niveau3 :: 2 :: [ lui, lui ]
pas_d_indication :: 1 :: [ provocation ]
marqueurs_niveau0 :: 1 :: [ provocation ]
appreciatif_niveau2 :: 1 :: [ oser ]
axiologique_niveau3 :: 1 :: [ mal ]
pas_d_indication_niveau0 :: 1 :: [ provocation ]
Total feature counts: 36


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ lui, oser, accuser, mal, lui, détester, provocation ]
marqueurs_niveau3 :: 4 :: [ lui, mal, lui, détester ]
appreciatif :: 3 :: [ oser, mal, détester ]
axiologique :: 3 :: [ oser, accuser, mal ]
alethique :: 2 :: [ lui, lui ]
epistemique :: 2 :: [ lui, lui ]
marqueurs_niveau2 :: 2 :: [ oser, accuser ]
alethique_niveau3 :: 2 :: [ lui, lui ]
appreciatif_niveau3 :: 2 :: [ mal, détester ]
axiologique_niveau2 :: 2 :: [ oser, accuser ]
epistemique_niveau3 :: 2 :: [ lui, lui ]
pas_d_indication :: 1 :: [ provocation ]
marqueurs_niveau0 :: 1 :: [ provocation ]
appreciatif_niveau2 :: 1 :: [ oser ]
axiologique_niveau3 :: 1 :: [ mal ]
pas_d_indication_niveau0 :: 1 :: [ provocation ]
Total feature counts: 36

