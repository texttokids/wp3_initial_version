
********** Entity Names Features **********
*******************************************

1. Sentence: En Angleterre , à la fin du xviiie siècle , le parc agricole était un moyen pour les nobles de retenir une population sinon immanquablement embarquée dans le grand exode urbain que la machine industrielle mettait en branle .
Entity Name (Type) :: Angleterre (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En Angleterre , à la fin du xviiie siècle , le parc agricole était un moyen pour les nobles de retenir une population sinon immanquablement embarquée dans le grand exode urbain que la machine industrielle mettait en branle .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En Angleterre , à la fin du xviiie siècle , le parc agricole était un moyen pour les nobles de retenir une population sinon immanquablement embarquée dans le grand exode urbain que la machine industrielle mettait en branle .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ grand ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En Angleterre , à la fin du xviiie siècle , le parc agricole était un moyen pour les nobles de retenir une population sinon immanquablement embarquée dans le grand exode urbain que la machine industrielle mettait en branle .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à la fin du xviiie siècle']
adverbiaux_localisation_temporelle::1::['à la fin du xviiie siècle']
adverbiaux_pointage_absolu::1::['à la fin du xviiie siècle']
adverbiaux_loc_temp_focalisation_non_id::1::['à la fin du xviiie siècle']
adverbiaux_loc_temp_regionalisation_id::1::['à la fin du xviiie siècle']
adverbiaux_loc_temp_pointage_absolu::1::['à la fin du xviiie siècle']

