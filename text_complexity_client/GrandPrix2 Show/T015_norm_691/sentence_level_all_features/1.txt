
********** Entity Names Features **********
*******************************************

1. Sentence: J ' aime le dessin à la main , essentiel pour clarifier ce qui doit être dit et montré , deux démarches ne relevant pas du même processus .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: J ' aime le dessin à la main , essentiel pour clarifier ce qui doit être dit et montré , deux démarches ne relevant pas du même processus .
Tokens having emotion: [ aime ]
Lemmas having emotion: [ aimer ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: J ' aime le dessin à la main , essentiel pour clarifier ce qui doit être dit et montré , deux démarches ne relevant pas du même processus .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ aimer, devoir, dire ]
marqueurs_niveau3 :: 3 :: [ aimer, devoir, dire ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ aimer ]
boulique :: 1 :: [ dire ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ aimer ]
boulique_niveau3 :: 1 :: [ dire ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' aime le dessin à la main , essentiel pour clarifier ce qui doit être dit et montré , deux démarches ne relevant pas du même processus .
No features found.

