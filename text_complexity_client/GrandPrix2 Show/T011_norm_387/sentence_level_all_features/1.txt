
********** Entity Names Features **********
*******************************************

1. Sentence: Peut-on étendre le centre directionnel vers le Sud (sur les terrains de la Foire de Lille) pour lui donner une présence suffisante ?
Entity Name (Type) :: Foire de Lille (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Peut-on étendre le centre directionnel vers le Sud (sur les terrains de la Foire de Lille) pour lui donner une présence suffisante ?
Tokens having emotion: [ suffisante ]
Lemmas having emotion: [ suffisant ]
Categories of the emotion lemmas: [ orgueil ]


********** Modality Features **********
***************************************

1. Sentence: Peut-on étendre le centre directionnel vers le Sud (sur les terrains de la Foire de Lille) pour lui donner une présence suffisante ?
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pouvoir, lui, suffisant ]
alethique :: 2 :: [ pouvoir, lui ]
epistemique :: 2 :: [ pouvoir, lui ]
marqueurs_niveau3 :: 2 :: [ pouvoir, lui ]
alethique_niveau3 :: 2 :: [ pouvoir, lui ]
epistemique_niveau3 :: 2 :: [ pouvoir, lui ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ suffisant ]
marqueurs_niveau0 :: 1 :: [ suffisant ]
deontique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ suffisant ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Peut-on étendre le centre directionnel vers le Sud (sur les terrains de la Foire de Lille) pour lui donner une présence suffisante ?
No features found.

