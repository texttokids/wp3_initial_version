
********** Entity Names Features **********
*******************************************

1. Sentence: Toutefois , en prenant en considération les tendances de développement et les formes de l ' habitat à basse densité , les conséquences sur l ' utilisation des terres fertiles de la plaine et à risque hydraulique seraient désastreuses , sans parler de l ' interruption des continuités écologiques transversales à la Durance .
Entity Name (Type) :: Durance (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Toutefois , en prenant en considération les tendances de développement et les formes de l ' habitat à basse densité , les conséquences sur l ' utilisation des terres fertiles de la plaine et à risque hydraulique seraient désastreuses , sans parler de l ' interruption des continuités écologiques transversales à la Durance .
Tokens having emotion: [ considération ]
Lemmas having emotion: [ considération ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Toutefois , en prenant en considération les tendances de développement et les formes de l ' habitat à basse densité , les conséquences sur l ' utilisation des terres fertiles de la plaine et à risque hydraulique seraient désastreuses , sans parler de l ' interruption des continuités écologiques transversales à la Durance .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ désastreux ]
appreciatif :: 1 :: [ désastreux ]
marqueurs_niveau2 :: 1 :: [ désastreux ]
appreciatif_niveau2 :: 1 :: [ désastreux ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Toutefois , en prenant en considération les tendances de développement et les formes de l ' habitat à basse densité , les conséquences sur l ' utilisation des terres fertiles de la plaine et à risque hydraulique seraient désastreuses , sans parler de l ' interruption des continuités écologiques transversales à la Durance .
No features found.

