
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est là que j ' ai découvert l ' absurdité des règlements qui , par exemple , empêchaient qu ' on attribue des rez-de-chaussée vides à des Togolais ou Vietnamiens du quartier , prêts à y développer des activités commerciales , sous prétexte qu ' ils n ' étaient pas inscrits au registre du commerce , ou encore de cultiver des légumes sur d ' anciens massifs de fleurs…
Entity Name (Type) :: Vietnamiens (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est là que j ' ai découvert l ' absurdité des règlements qui , par exemple , empêchaient qu ' on attribue des rez-de-chaussée vides à des Togolais ou Vietnamiens du quartier , prêts à y développer des activités commerciales , sous prétexte qu ' ils n ' étaient pas inscrits au registre du commerce , ou encore de cultiver des légumes sur d ' anciens massifs de fleurs…
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' est là que j ' ai découvert l ' absurdité des règlements qui , par exemple , empêchaient qu ' on attribue des rez-de-chaussée vides à des Togolais ou Vietnamiens du quartier , prêts à y développer des activités commerciales , sous prétexte qu ' ils n ' étaient pas inscrits au registre du commerce , ou encore de cultiver des légumes sur d ' anciens massifs de fleurs…
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ découvrir, empêcher, fleur ]
pas_d_indication :: 3 :: [ découvrir, empêcher, fleur ]
marqueurs_niveau0 :: 3 :: [ découvrir, empêcher, fleur ]
pas_d_indication_niveau0 :: 3 :: [ découvrir, empêcher, fleur ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est là que j ' ai découvert l ' absurdité des règlements qui , par exemple , empêchaient qu ' on attribue des rez-de-chaussée vides à des Togolais ou Vietnamiens du quartier , prêts à y développer des activités commerciales , sous prétexte qu ' ils n ' étaient pas inscrits au registre du commerce , ou encore de cultiver des légumes sur d ' anciens massifs de fleurs…
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

