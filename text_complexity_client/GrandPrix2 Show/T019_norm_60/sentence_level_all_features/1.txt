
********** Entity Names Features **********
*******************************************

1. Sentence: Ce laboratoire , constitué d ' abord à l ' École des ponts autour d ' un tout petit noyau de personnes , souvent à double profil disciplinaire , est devenu assez vite un des plus importants centres de sciences sociales en France , en se liant avec l ' université et le cnrs .
Entity Name (Type) :: France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ce laboratoire , constitué d ' abord à l ' École des ponts autour d ' un tout petit noyau de personnes , souvent à double profil disciplinaire , est devenu assez vite un des plus importants centres de sciences sociales en France , en se liant avec l ' université et le cnrs .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ce laboratoire , constitué d ' abord à l ' École des ponts autour d ' un tout petit noyau de personnes , souvent à double profil disciplinaire , est devenu assez vite un des plus importants centres de sciences sociales en France , en se liant avec l ' université et le cnrs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ école, petit, personne, souvent, vite, important ]
alethique :: 3 :: [ école, personne, souvent ]
epistemique :: 3 :: [ petit, vite, important ]
marqueurs_niveau3 :: 3 :: [ petit, souvent, vite ]
marqueurs_niveau1 :: 2 :: [ école, important ]
epistemique_niveau3 :: 2 :: [ petit, vite ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
marqueurs_niveau2 :: 1 :: [ personne ]
alethique_niveau3 :: 1 :: [ souvent ]
alethique_niveau2 :: 1 :: [ personne ]
alethique_niveau1 :: 1 :: [ école ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce laboratoire , constitué d ' abord à l ' École des ponts autour d ' un tout petit noyau de personnes , souvent à double profil disciplinaire , est devenu assez vite un des plus importants centres de sciences sociales en France , en se liant avec l ' université et le cnrs .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

