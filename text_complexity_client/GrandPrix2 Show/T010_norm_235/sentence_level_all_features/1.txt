
********** Entity Names Features **********
*******************************************

1. Sentence: Mais c ' est surtout avec le projet Olivetti le long de l ' autoroute de Milan , où le déploiement de l ' échangeur routier génère le paysage des parkings , les nappes des ateliers et les deux bâtiments curvilignes des bureaux , qu ' il réussit la plus parfaite «synthèse des arts» .
Entity Name (Type) :: autoroute de Milan (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Mais c ' est surtout avec le projet Olivetti le long de l ' autoroute de Milan , où le déploiement de l ' échangeur routier génère le paysage des parkings , les nappes des ateliers et les deux bâtiments curvilignes des bureaux , qu ' il réussit la plus parfaite «synthèse des arts» .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mais c ' est surtout avec le projet Olivetti le long de l ' autoroute de Milan , où le déploiement de l ' échangeur routier génère le paysage des parkings , les nappes des ateliers et les deux bâtiments curvilignes des bureaux , qu ' il réussit la plus parfaite «synthèse des arts» .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ long, réussir, parfait ]
appreciatif :: 2 :: [ réussir, parfait ]
marqueurs_niveau2 :: 2 :: [ long, parfait ]
axiologique :: 1 :: [ parfait ]
epistemique :: 1 :: [ long ]
marqueurs_niveau3 :: 1 :: [ réussir ]
appreciatif_niveau3 :: 1 :: [ réussir ]
appreciatif_niveau2 :: 1 :: [ parfait ]
axiologique_niveau2 :: 1 :: [ parfait ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais c ' est surtout avec le projet Olivetti le long de l ' autoroute de Milan , où le déploiement de l ' échangeur routier génère le paysage des parkings , les nappes des ateliers et les deux bâtiments curvilignes des bureaux , qu ' il réussit la plus parfaite «synthèse des arts» .
No features found.

