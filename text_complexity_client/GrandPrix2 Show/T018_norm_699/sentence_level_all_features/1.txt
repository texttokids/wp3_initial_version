
********** Entity Names Features **********
*******************************************

1. Sentence: Saint-Nazaire ressentait la nécessité d ' affirmer sa présence sur la mer , de s ' échapper quelque peu de l ' ancrage industriel .
Entity Name (Type) :: Saint-Nazaire (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Saint-Nazaire ressentait la nécessité d ' affirmer sa présence sur la mer , de s ' échapper quelque peu de l ' ancrage industriel .
Tokens having emotion: [ ressentait ]
Lemmas having emotion: [ ressentir ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Saint-Nazaire ressentait la nécessité d ' affirmer sa présence sur la mer , de s ' échapper quelque peu de l ' ancrage industriel .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ nécessité, affirmer, échapper ]
pas_d_indication :: 2 :: [ affirmer, échapper ]
marqueurs_niveau0 :: 2 :: [ affirmer, échapper ]
pas_d_indication_niveau0 :: 2 :: [ affirmer, échapper ]
alethique :: 1 :: [ nécessité ]
marqueurs_niveau3 :: 1 :: [ nécessité ]
alethique_niveau3 :: 1 :: [ nécessité ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Saint-Nazaire ressentait la nécessité d ' affirmer sa présence sur la mer , de s ' échapper quelque peu de l ' ancrage industriel .
No features found.

