
********** Entity Names Features **********
*******************************************

1. Sentence: Cette partie de l ' île , sur laquelle il y a encore eu peu de réflexion , concerne de grandes emprises ferroviaires (dont le déménagement est en négociations avec la SNCF et RFF depuis plusieurs années) , le Marché d ' intérêt national (Min) et divers sites industriels ou portuaires plus ou moins occupés .
Entity Name (Type) :: Min (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette partie de l ' île , sur laquelle il y a encore eu peu de réflexion , concerne de grandes emprises ferroviaires (dont le déménagement est en négociations avec la SNCF et RFF depuis plusieurs années) , le Marché d ' intérêt national (Min) et divers sites industriels ou portuaires plus ou moins occupés .
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Cette partie de l ' île , sur laquelle il y a encore eu peu de réflexion , concerne de grandes emprises ferroviaires (dont le déménagement est en négociations avec la SNCF et RFF depuis plusieurs années) , le Marché d ' intérêt national (Min) et divers sites industriels ou portuaires plus ou moins occupés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ grand, intérêt ]
boulique :: 1 :: [ intérêt ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ intérêt ]
boulique_niveau2 :: 1 :: [ intérêt ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette partie de l ' île , sur laquelle il y a encore eu peu de réflexion , concerne de grandes emprises ferroviaires (dont le déménagement est en négociations avec la SNCF et RFF depuis plusieurs années) , le Marché d ' intérêt national (Min) et divers sites industriels ou portuaires plus ou moins occupés .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['depuis plusieurs années', 'encore']
adverbiaux_duratifs_iteratifs::2::['depuis plusieurs années', 'encore']
adverbiaux_pointage_non_absolu::2::['depuis plusieurs années', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['depuis plusieurs années']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis plusieurs années']
adverbiaux_dur_iter_deictique::1::['depuis plusieurs années']
adverbiaux_dur_iter_relatif::1::['encore']

