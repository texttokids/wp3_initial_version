
********** Entity Names Features **********
*******************************************

1. Sentence: Urbanisme et architecture sont à mon sens distincts , mais bien entendu loin d ' être indifférents l ' un à l ' autre .
Entity Name (Type) :: Urbanisme (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Urbanisme et architecture sont à mon sens distincts , mais bien entendu loin d ' être indifférents l ' un à l ' autre .
Tokens having emotion: [ indifférents ]
Lemmas having emotion: [ indifférent ]
Categories of the emotion lemmas: [ impassibilite ]


********** Modality Features **********
***************************************

1. Sentence: Urbanisme et architecture sont à mon sens distincts , mais bien entendu loin d ' être indifférents l ' un à l ' autre .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ bien ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
marqueurs_niveau3 :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Urbanisme et architecture sont à mon sens distincts , mais bien entendu loin d ' être indifférents l ' un à l ' autre .
No features found.

