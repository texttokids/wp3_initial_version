
********** Entity Names Features **********
*******************************************

1. Sentence: Même si l ' on ne rattrape pas le manque de mètres carrés d ' un logement avec un banc sur un trottoir , on est maintenant conscient comme jamais du besoin de trouver au plus près de chez soi toute la diversité de ce qu ' on attend d ' une vie de quartier , toute une aménité variée de l ' espace partagé — plus d ' hospitalité de la ville à l ' échelle des corps , horizon d ' un micro-urbanisme nécessaire après les méfaits trop fréquents de l ' aménagement à grande échelle .
Entity Name (Type) :: Même (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Même si l ' on ne rattrape pas le manque de mètres carrés d ' un logement avec un banc sur un trottoir , on est maintenant conscient comme jamais du besoin de trouver au plus près de chez soi toute la diversité de ce qu ' on attend d ' une vie de quartier , toute une aménité variée de l ' espace partagé — plus d ' hospitalité de la ville à l ' échelle des corps , horizon d ' un micro-urbanisme nécessaire après les méfaits trop fréquents de l ' aménagement à grande échelle .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Même si l ' on ne rattrape pas le manque de mètres carrés d ' un logement avec un banc sur un trottoir , on est maintenant conscient comme jamais du besoin de trouver au plus près de chez soi toute la diversité de ce qu ' on attend d ' une vie de quartier , toute une aménité variée de l ' espace partagé — plus d ' hospitalité de la ville à l ' échelle des corps , horizon d ' un micro-urbanisme nécessaire après les méfaits trop fréquents de l ' aménagement à grande échelle .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ jamais, besoin, nécessaire, grand ]
marqueurs_niveau3 :: 4 :: [ jamais, besoin, nécessaire, grand ]
alethique :: 3 :: [ jamais, besoin, nécessaire ]
alethique_niveau3 :: 3 :: [ jamais, besoin, nécessaire ]
boulique :: 1 :: [ besoin ]
epistemique :: 1 :: [ grand ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Même si l ' on ne rattrape pas le manque de mètres carrés d ' un logement avec un banc sur un trottoir , on est maintenant conscient comme jamais du besoin de trouver au plus près de chez soi toute la diversité de ce qu ' on attend d ' une vie de quartier , toute une aménité variée de l ' espace partagé — plus d ' hospitalité de la ville à l ' échelle des corps , horizon d ' un micro-urbanisme nécessaire après les méfaits trop fréquents de l ' aménagement à grande échelle .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['maintenant', 'jamais']
adverbiaux_localisation_temporelle::1::['maintenant']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_pointage_non_absolu::1::['maintenant']
adverbiaux_loc_temp_focalisation_id::1::['maintenant']
adverbiaux_loc_temp_regionalisation_id::1::['maintenant']
adverbiaux_loc_temp_pointage_non_absolu::1::['maintenant']
adverbiaux_iterateur_frequentiel::1::['jamais']

