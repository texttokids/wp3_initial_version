====== Sentence Level Features ======

1. Sentence: Rasée par les bombardements alliés de 1945 , reconstruite selon le plan fonctionnaliste de Le Maresquier , qui l ' isolait de ses zones productives et la réorientait en ignorant son port ,  l ' estuaire et la mer , elle était coupée de l ' eau , à l ' est , par la base sous-marine construite par l ' armée allemande , Partie III Projets d ' après un entretien avec Laurent Théry par Ariella Masboungi et Olivia Barbet-Massin Saint-Nazaire devait réinventer sa centralité et se retourner vers son port et vers la mer , ses raisons d ' être — Saint-Nazaire a été fondé au Second Empire pour donner un port à Nantes .
Entity Name (Type) :: Nantes (LOC)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
Le Maresquier (LOC)
Partie III Projets (MISC)
Laurent Théry (PER)
Ariella Masboungi (PER)
Olivia Barbet-Massin Saint-Nazaire (PER)
Saint-Nazaire (LOC)
Second Empire (MISC)
Nantes (LOC)
Total entitiy counts: 8

