
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai tenté de transformer des écoles existantes  :  l ' École des beaux-arts de Bourges , en y créant un département « environnement » qui reliait l ' art à l ' espace public , au design urbain et à l ' archi tecture éphémère ;
Entity Name (Type) :: École des beaux-arts de Bourges (LOC)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai tenté de transformer des écoles existantes  :  l ' École des beaux-arts de Bourges , en y créant un département « environnement » qui reliait l ' art à l ' espace public , au design urbain et à l ' archi tecture éphémère ;
Tokens having emotion: [ tenté ]
Lemmas having emotion: [ tenter ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai tenté de transformer des écoles existantes  :  l ' École des beaux-arts de Bourges , en y créant un département « environnement » qui reliait l ' art à l ' espace public , au design urbain et à l ' archi tecture éphémère ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ tenter, école, école ]
alethique :: 2 :: [ école, école ]
marqueurs_niveau1 :: 2 :: [ école, école ]
alethique_niveau1 :: 2 :: [ école, école ]
boulique :: 1 :: [ tenter ]
marqueurs_niveau2 :: 1 :: [ tenter ]
boulique_niveau2 :: 1 :: [ tenter ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai tenté de transformer des écoles existantes  :  l ' École des beaux-arts de Bourges , en y créant un département « environnement » qui reliait l ' art à l ' espace public , au design urbain et à l ' archi tecture éphémère ;
No features found.

