
********** Entity Names Features **********
*******************************************

1. Sentence: D ' autres projets d ' ampleur sont sous la responsabilité première de l ' État (Euroméditerranée à Marseille ,  l ' Epase à Saint-Étienne , Plaine de France , Orsa et autres en région parisienne) .
Entity Name (Type) :: Orsa (LOC)


********** Emotions Features **********
***************************************

1. Sentence: D ' autres projets d ' ampleur sont sous la responsabilité première de l ' État (Euroméditerranée à Marseille ,  l ' Epase à Saint-Étienne , Plaine de France , Orsa et autres en région parisienne) .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: D ' autres projets d ' ampleur sont sous la responsabilité première de l ' État (Euroméditerranée à Marseille ,  l ' Epase à Saint-Étienne , Plaine de France , Orsa et autres en région parisienne) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ responsabilité ]
deontique :: 1 :: [ responsabilité ]
marqueurs_niveau2 :: 1 :: [ responsabilité ]
deontique_niveau2 :: 1 :: [ responsabilité ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: D ' autres projets d ' ampleur sont sous la responsabilité première de l ' État (Euroméditerranée à Marseille ,  l ' Epase à Saint-Étienne , Plaine de France , Orsa et autres en région parisienne) .
No features found.

