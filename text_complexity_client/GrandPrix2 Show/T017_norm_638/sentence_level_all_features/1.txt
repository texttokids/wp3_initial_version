
********** Entity Names Features **********
*******************************************

1. Sentence: Un tel morceau de ville génèrerait un espace de solidarité « incarnée » où collaboreraient celui qui donne et celui qui reçoit , celui qui achète en centre ville et celui qui a recours à une grande distribution capable de renouer avec des espaces délaissés comme une galerie commerciale de centre ville 5 .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Un tel morceau de ville génèrerait un espace de solidarité « incarnée » où collaboreraient celui qui donne et celui qui reçoit , celui qui achète en centre ville et celui qui a recours à une grande distribution capable de renouer avec des espaces délaissés comme une galerie commerciale de centre ville 5 .
Tokens having emotion: [ délaissés ]
Lemmas having emotion: [ délaisser ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Un tel morceau de ville génèrerait un espace de solidarité « incarnée » où collaboreraient celui qui donne et celui qui reçoit , celui qui achète en centre ville et celui qui a recours à une grande distribution capable de renouer avec des espaces délaissés comme une galerie commerciale de centre ville 5 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ grand ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un tel morceau de ville génèrerait un espace de solidarité « incarnée » où collaboreraient celui qui donne et celui qui reçoit , celui qui achète en centre ville et celui qui a recours à une grande distribution capable de renouer avec des espaces délaissés comme une galerie commerciale de centre ville 5 .
No features found.

