
********** Entity Names Features **********
*******************************************

1. Sentence: La commune de banlieue dans la dynamique du Grand Paris qui risque de pousser plus loin les populations à revenus modestes , dont certaines sont encore en lien avec la ruralité .
Entity Name (Type) :: Grand Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La commune de banlieue dans la dynamique du Grand Paris qui risque de pousser plus loin les populations à revenus modestes , dont certaines sont encore en lien avec la ruralité .
No features found.


********** Modality Features **********
***************************************

1. Sentence: La commune de banlieue dans la dynamique du Grand Paris qui risque de pousser plus loin les populations à revenus modestes , dont certaines sont encore en lien avec la ruralité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ grand, risquer, certain ]
marqueurs_niveau3 :: 3 :: [ grand, risquer, certain ]
epistemique :: 2 :: [ grand, certain ]
epistemique_niveau3 :: 2 :: [ grand, certain ]
appreciatif :: 1 :: [ risquer ]
appreciatif_niveau3 :: 1 :: [ risquer ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La commune de banlieue dans la dynamique du Grand Paris qui risque de pousser plus loin les populations à revenus modestes , dont certaines sont encore en lien avec la ruralité .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

