
********** Entity Names Features **********
*******************************************

1. Sentence: Si cette réalité n ' était ni transposable ni souhaitable ici — une simple inversion des rôles ne serait pas aussi pertinente qu ' une collaboration complice — , il était devenu évident à mes yeux que l ' espace public était une préoccupation centrale pour un paysagiste , et singulièrement pour moi qui voyais soudain qu ' il pouvait être l ' objet d ' une sorte de vocation personnelle que je devais assurément affirmer .
Entity Name (Type) :: Si (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Si cette réalité n ' était ni transposable ni souhaitable ici — une simple inversion des rôles ne serait pas aussi pertinente qu ' une collaboration complice — , il était devenu évident à mes yeux que l ' espace public était une préoccupation centrale pour un paysagiste , et singulièrement pour moi qui voyais soudain qu ' il pouvait être l ' objet d ' une sorte de vocation personnelle que je devais assurément affirmer .
Tokens having emotion: [ préoccupation ]
Lemmas having emotion: [ préoccupation ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Si cette réalité n ' était ni transposable ni souhaitable ici — une simple inversion des rôles ne serait pas aussi pertinente qu ' une collaboration complice — , il était devenu évident à mes yeux que l ' espace public était une préoccupation centrale pour un paysagiste , et singulièrement pour moi qui voyais soudain qu ' il pouvait être l ' objet d ' une sorte de vocation personnelle que je devais assurément affirmer .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ réalité, souhaitable, collaboration, préoccupation, lui, soudain, pouvoir, devoir, affirmer ]
marqueurs_niveau3 :: 5 :: [ souhaitable, collaboration, lui, pouvoir, devoir ]
alethique :: 4 :: [ réalité, lui, pouvoir, devoir ]
epistemique :: 3 :: [ lui, pouvoir, devoir ]
alethique_niveau3 :: 3 :: [ lui, pouvoir, devoir ]
epistemique_niveau3 :: 3 :: [ lui, pouvoir, devoir ]
deontique :: 2 :: [ pouvoir, devoir ]
pas_d_indication :: 2 :: [ soudain, affirmer ]
marqueurs_niveau1 :: 2 :: [ réalité, préoccupation ]
marqueurs_niveau0 :: 2 :: [ soudain, affirmer ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
pas_d_indication_niveau0 :: 2 :: [ soudain, affirmer ]
appreciatif :: 1 :: [ préoccupation ]
axiologique :: 1 :: [ collaboration ]
boulique :: 1 :: [ souhaitable ]
alethique_niveau1 :: 1 :: [ réalité ]
appreciatif_niveau1 :: 1 :: [ préoccupation ]
axiologique_niveau3 :: 1 :: [ collaboration ]
boulique_niveau3 :: 1 :: [ souhaitable ]
Total feature counts: 46


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Si cette réalité n ' était ni transposable ni souhaitable ici — une simple inversion des rôles ne serait pas aussi pertinente qu ' une collaboration complice — , il était devenu évident à mes yeux que l ' espace public était une préoccupation centrale pour un paysagiste , et singulièrement pour moi qui voyais soudain qu ' il pouvait être l ' objet d ' une sorte de vocation personnelle que je devais assurément affirmer .
No features found.

