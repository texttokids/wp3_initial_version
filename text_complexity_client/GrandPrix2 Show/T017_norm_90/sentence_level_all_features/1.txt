
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai créé la SCIC , ou société coopérative d ' intérêt collectif , qui offrait l ' avantage de pouvoir être maître d ' ouvrage de l ' opération .
Entity Name (Type) :: SCIC (ORG)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai créé la SCIC , ou société coopérative d ' intérêt collectif , qui offrait l ' avantage de pouvoir être maître d ' ouvrage de l ' opération .
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai créé la SCIC , ou société coopérative d ' intérêt collectif , qui offrait l ' avantage de pouvoir être maître d ' ouvrage de l ' opération .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ intérêt, offrir, pouvoir ]
marqueurs_niveau2 :: 2 :: [ intérêt, offrir ]
alethique :: 1 :: [ pouvoir ]
appreciatif :: 1 :: [ offrir ]
boulique :: 1 :: [ intérêt ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau2 :: 1 :: [ offrir ]
boulique_niveau2 :: 1 :: [ intérêt ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai créé la SCIC , ou société coopérative d ' intérêt collectif , qui offrait l ' avantage de pouvoir être maître d ' ouvrage de l ' opération .
No features found.

