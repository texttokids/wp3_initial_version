
********** Entity Names Features **********
*******************************************

1. Sentence: Les Japonais avaient honte de ce qui fait une partie de la gloire de la plaine du Var à Nice :  l ' hectare de fleurs de courgettes .
Entity Name (Type) :: Nice (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Les Japonais avaient honte de ce qui fait une partie de la gloire de la plaine du Var à Nice :  l ' hectare de fleurs de courgettes .
Tokens having emotion: [ honte, gloire ]
Lemmas having emotion: [ honte, gloire ]
Categories of the emotion lemmas: [ embarras, admiration ]


********** Modality Features **********
***************************************

1. Sentence: Les Japonais avaient honte de ce qui fait une partie de la gloire de la plaine du Var à Nice :  l ' hectare de fleurs de courgettes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ honte, gloire, fleur ]
marqueurs_niveau2 :: 2 :: [ honte, gloire ]
appreciatif :: 1 :: [ gloire ]
axiologique :: 1 :: [ honte ]
pas_d_indication :: 1 :: [ fleur ]
marqueurs_niveau0 :: 1 :: [ fleur ]
appreciatif_niveau2 :: 1 :: [ gloire ]
axiologique_niveau2 :: 1 :: [ honte ]
pas_d_indication_niveau0 :: 1 :: [ fleur ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les Japonais avaient honte de ce qui fait une partie de la gloire de la plaine du Var à Nice :  l ' hectare de fleurs de courgettes .
No features found.

