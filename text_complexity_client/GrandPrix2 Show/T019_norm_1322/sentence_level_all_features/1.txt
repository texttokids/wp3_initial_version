
********** Entity Names Features **********
*******************************************

1. Sentence: Mais le grand risque est de démocratiser ainsi pour les apprentis chercheurs du monde entier le formidable privi‑ lège que les bibliothèques des universités américaines ont longtemps offert à nos collègues d ' Outre‑Atlantique celui d ' une science sociale conçue comme un immense chantier borgésien de recyclage de livres et d ' articles écrits par les confrères , alimenté de manière secondaire par les échos assourdis du monde réel parvenant à filtrer vers les campus .
Entity Name (Type) :: Outre‑Atlantique (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Mais le grand risque est de démocratiser ainsi pour les apprentis chercheurs du monde entier le formidable privi‑ lège que les bibliothèques des universités américaines ont longtemps offert à nos collègues d ' Outre‑Atlantique celui d ' une science sociale conçue comme un immense chantier borgésien de recyclage de livres et d ' articles écrits par les confrères , alimenté de manière secondaire par les échos assourdis du monde réel parvenant à filtrer vers les campus .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mais le grand risque est de démocratiser ainsi pour les apprentis chercheurs du monde entier le formidable privi‑ lège que les bibliothèques des universités américaines ont longtemps offert à nos collègues d ' Outre‑Atlantique celui d ' une science sociale conçue comme un immense chantier borgésien de recyclage de livres et d ' articles écrits par les confrères , alimenté de manière secondaire par les échos assourdis du monde réel parvenant à filtrer vers les campus .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ grand, formidable, longtemps, offrir, immense ]
epistemique :: 3 :: [ grand, longtemps, immense ]
marqueurs_niveau2 :: 3 :: [ formidable, offrir, immense ]
appreciatif :: 2 :: [ formidable, offrir ]
appreciatif_niveau2 :: 2 :: [ formidable, offrir ]
marqueurs_niveau3 :: 1 :: [ grand ]
marqueurs_niveau1 :: 1 :: [ longtemps ]
epistemique_niveau3 :: 1 :: [ grand ]
epistemique_niveau2 :: 1 :: [ immense ]
epistemique_niveau1 :: 1 :: [ longtemps ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais le grand risque est de démocratiser ainsi pour les apprentis chercheurs du monde entier le formidable privi‑ lège que les bibliothèques des universités américaines ont longtemps offert à nos collègues d ' Outre‑Atlantique celui d ' une science sociale conçue comme un immense chantier borgésien de recyclage de livres et d ' articles écrits par les confrères , alimenté de manière secondaire par les échos assourdis du monde réel parvenant à filtrer vers les campus .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['longtemps']
adverbiaux_duratifs_iteratifs::1::['longtemps']

