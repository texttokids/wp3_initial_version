====== Sentence Level Features ======

1. Sentence: Le besoin de projet
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ besoin ]
alethique :: 1 :: [ besoin ]
boulique :: 1 :: [ besoin ]
marqueurs_niveau3 :: 1 :: [ besoin ]
alethique_niveau3 :: 1 :: [ besoin ]
boulique_niveau3 :: 1 :: [ besoin ]
Total feature counts: 6

2. Sentence: Une autre caractéristique de cette expérience est aussi que les défis sont nécessaires , et que certaines missions jugées impossibles ont eu des résultats grâce au fait que le projet a pu être porté par le politique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ nécessaire, certain, juger, impossible, pouvoir ]
marqueurs_niveau3 :: 4 :: [ nécessaire, certain, impossible, pouvoir ]
alethique :: 3 :: [ nécessaire, impossible, pouvoir ]
alethique_niveau3 :: 3 :: [ nécessaire, impossible, pouvoir ]
epistemique :: 2 :: [ certain, pouvoir ]
epistemique_niveau3 :: 2 :: [ certain, pouvoir ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ juger ]
marqueurs_niveau0 :: 1 :: [ juger ]
deontique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ juger ]
Total feature counts: 24


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ besoin, nécessaire, certain, juger, impossible, pouvoir ]
marqueurs_niveau3 :: 5 :: [ besoin, nécessaire, certain, impossible, pouvoir ]
alethique :: 4 :: [ besoin, nécessaire, impossible, pouvoir ]
alethique_niveau3 :: 4 :: [ besoin, nécessaire, impossible, pouvoir ]
epistemique :: 2 :: [ certain, pouvoir ]
epistemique_niveau3 :: 2 :: [ certain, pouvoir ]
boulique :: 1 :: [ besoin ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ juger ]
marqueurs_niveau0 :: 1 :: [ juger ]
boulique_niveau3 :: 1 :: [ besoin ]
deontique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ juger ]
Total feature counts: 30

