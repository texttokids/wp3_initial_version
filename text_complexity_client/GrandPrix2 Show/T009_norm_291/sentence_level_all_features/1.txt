
********** Entity Names Features **********
*******************************************

1. Sentence: Ce quartier repose sur des capitaux modestes , rapatriés par des travailleurs d ' Europe qui construisent sur des parcelles petites , très vaguement viabilisées , des immeubles qui collent à la moindre différence de niveau et qui trouvent leur équilibre économique entre le volume et la hauteur admissible par le système structurel le plus économique .
Entity Name (Type) :: Europe (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ce quartier repose sur des capitaux modestes , rapatriés par des travailleurs d ' Europe qui construisent sur des parcelles petites , très vaguement viabilisées , des immeubles qui collent à la moindre différence de niveau et qui trouvent leur équilibre économique entre le volume et la hauteur admissible par le système structurel le plus économique .
Tokens having emotion: [ repose ]
Lemmas having emotion: [ reposer ]
Categories of the emotion lemmas: [ apaisement ]


********** Modality Features **********
***************************************

1. Sentence: Ce quartier repose sur des capitaux modestes , rapatriés par des travailleurs d ' Europe qui construisent sur des parcelles petites , très vaguement viabilisées , des immeubles qui collent à la moindre différence de niveau et qui trouvent leur équilibre économique entre le volume et la hauteur admissible par le système structurel le plus économique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ petit, moindre ]
epistemique :: 2 :: [ petit, moindre ]
marqueurs_niveau3 :: 1 :: [ petit ]
marqueurs_niveau2 :: 1 :: [ moindre ]
epistemique_niveau3 :: 1 :: [ petit ]
epistemique_niveau2 :: 1 :: [ moindre ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce quartier repose sur des capitaux modestes , rapatriés par des travailleurs d ' Europe qui construisent sur des parcelles petites , très vaguement viabilisées , des immeubles qui collent à la moindre différence de niveau et qui trouvent leur équilibre économique entre le volume et la hauteur admissible par le système structurel le plus économique .
No features found.

