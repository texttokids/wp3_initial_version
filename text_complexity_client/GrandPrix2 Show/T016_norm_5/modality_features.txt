====== Sentence Level Features ======

1. Sentence: Plus tard , à l ' école d ' architecture de Florence , j ' ai eu la chance de suivre pendant quelques années les recherches de Gianfranco Caniggia , le meilleur élève de Saverio Muratori , un professeur important , entouré d ' assistants , qui donnait des cours fascinants non seulement sur l ' évolution typologique du tissu urbain de Florence , mais aussi sur la construction territoriale dans le temps long .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ école, chance, meilleur, important, long ]
appreciatif :: 3 :: [ chance, meilleur, important ]
axiologique :: 2 :: [ meilleur, important ]
epistemique :: 2 :: [ important, long ]
marqueurs_niveau2 :: 2 :: [ meilleur, long ]
marqueurs_niveau1 :: 2 :: [ école, important ]
alethique :: 1 :: [ école ]
marqueurs_niveau3 :: 1 :: [ chance ]
alethique_niveau1 :: 1 :: [ école ]
appreciatif_niveau3 :: 1 :: [ chance ]
appreciatif_niveau2 :: 1 :: [ meilleur ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau2 :: 1 :: [ meilleur ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau2 :: 1 :: [ long ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 26


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ école, chance, meilleur, important, long ]
appreciatif :: 3 :: [ chance, meilleur, important ]
axiologique :: 2 :: [ meilleur, important ]
epistemique :: 2 :: [ important, long ]
marqueurs_niveau2 :: 2 :: [ meilleur, long ]
marqueurs_niveau1 :: 2 :: [ école, important ]
alethique :: 1 :: [ école ]
marqueurs_niveau3 :: 1 :: [ chance ]
alethique_niveau1 :: 1 :: [ école ]
appreciatif_niveau3 :: 1 :: [ chance ]
appreciatif_niveau2 :: 1 :: [ meilleur ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau2 :: 1 :: [ meilleur ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau2 :: 1 :: [ long ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 26

