
********** Entity Names Features **********
*******************************************

1. Sentence: Il faut faire des choix , dessiner la ville de demain , mettre en forme et représenter un futur implicite ou explicite .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il faut faire des choix , dessiner la ville de demain , mettre en forme et représenter un futur implicite ou explicite .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il faut faire des choix , dessiner la ville de demain , mettre en forme et représenter un futur implicite ou explicite .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ falloir, choix ]
boulique :: 1 :: [ choix ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau3 :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ choix ]
boulique_niveau2 :: 1 :: [ choix ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il faut faire des choix , dessiner la ville de demain , mettre en forme et représenter un futur implicite ou explicite .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['de demain', 'demain']
adverbiaux_localisation_temporelle::2::['de demain', 'demain']
adverbiaux_pointage_non_absolu::3::['de demain', 'demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['de demain']
adverbiaux_loc_temp_pointage_non_absolu::2::['de demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']

