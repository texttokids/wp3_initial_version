
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai été ensuite peu marquée par ma formation d ' urbaniste à l ' Institut d ' urbanisme de Paris (Créteil) , balbutiante et erratique , mais j ' ai un souvenir ému des conférences de Gérald Hanning sur le rôle du parcellaire à toutes les échelles , de Jean Lojkine , de Henri Coing qui nous sensibilisait à la dimension sociale et économique de l ' urbanisme…
Entity Name (Type) :: Henri Coing (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai été ensuite peu marquée par ma formation d ' urbaniste à l ' Institut d ' urbanisme de Paris (Créteil) , balbutiante et erratique , mais j ' ai un souvenir ému des conférences de Gérald Hanning sur le rôle du parcellaire à toutes les échelles , de Jean Lojkine , de Henri Coing qui nous sensibilisait à la dimension sociale et économique de l ' urbanisme…
Tokens having emotion: [ ému ]
Lemmas having emotion: [ émouvoir ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai été ensuite peu marquée par ma formation d ' urbaniste à l ' Institut d ' urbanisme de Paris (Créteil) , balbutiante et erratique , mais j ' ai un souvenir ému des conférences de Gérald Hanning sur le rôle du parcellaire à toutes les échelles , de Jean Lojkine , de Henri Coing qui nous sensibilisait à la dimension sociale et économique de l ' urbanisme…
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ souvenir ]
pas_d_indication :: 1 :: [ souvenir ]
marqueurs_niveau0 :: 1 :: [ souvenir ]
pas_d_indication_niveau0 :: 1 :: [ souvenir ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai été ensuite peu marquée par ma formation d ' urbaniste à l ' Institut d ' urbanisme de Paris (Créteil) , balbutiante et erratique , mais j ' ai un souvenir ému des conférences de Gérald Hanning sur le rôle du parcellaire à toutes les échelles , de Jean Lojkine , de Henri Coing qui nous sensibilisait à la dimension sociale et économique de l ' urbanisme…
No features found.

