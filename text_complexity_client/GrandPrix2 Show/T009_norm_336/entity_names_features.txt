====== Sentence Level Features ======

1. Sentence: Cette position , « l ' espace public comme représentation monumentale» , n ' est pas comprise par les architectes lyonnais qui regrettent l ' absence d ' effets de balance et de péristyle , mais elle est très bien comprise par les magistrats avec lesquels j ' ai travaillé au quotidien , par l ' artiste Gérard Garouste qui aide à donner du sens à cette salle des pas perdus au nom des droits de l ' homme , par le quartier enfin dont je tiens compte absolument comme s ' il était d ' une qualité exceptionnelle , ce qui n ' est pas le cas .
Entity Name (Type) :: Gérard Garouste (PER)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
Gérard Garouste (PER)
Total entitiy counts: 1

