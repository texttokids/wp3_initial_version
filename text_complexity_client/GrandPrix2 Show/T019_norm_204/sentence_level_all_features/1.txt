
********** Entity Names Features **********
*******************************************

1. Sentence: Une place essentielle doit rester ouverte pour les choix formels , la projection de la qualité poétique et sensible des espaces .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Une place essentielle doit rester ouverte pour les choix formels , la projection de la qualité poétique et sensible des espaces .
Tokens having emotion: [ sensible ]
Lemmas having emotion: [ sensible ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Une place essentielle doit rester ouverte pour les choix formels , la projection de la qualité poétique et sensible des espaces .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ devoir, ouvrir, choix, sensible ]
boulique :: 2 :: [ ouvrir, choix ]
marqueurs_niveau3 :: 2 :: [ devoir, ouvrir ]
alethique :: 1 :: [ devoir ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ sensible ]
marqueurs_niveau2 :: 1 :: [ choix ]
marqueurs_niveau0 :: 1 :: [ sensible ]
alethique_niveau3 :: 1 :: [ devoir ]
boulique_niveau3 :: 1 :: [ ouvrir ]
boulique_niveau2 :: 1 :: [ choix ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ sensible ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Une place essentielle doit rester ouverte pour les choix formels , la projection de la qualité poétique et sensible des espaces .
No features found.

