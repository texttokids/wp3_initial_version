
********** Entity Names Features **********
*******************************************

1. Sentence: Maîtrise d ' œuvre urbaine , maîtrise d ' œuvre des espaces publics : Atelier Ruelle et Océanis BET . Nantes Malakoff-Pré Gauchet , 2002-… Le nouveau Malakoff-sur-Loire Ce territoire , né de la Loire , longtemps inondable et remblayé dans les années 1960 par du sable , est délimité par le fleuve et le canal Saint-Félix .
Entity Name (Type) :: canal Saint-Félix (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Maîtrise d ' œuvre urbaine , maîtrise d ' œuvre des espaces publics : Atelier Ruelle et Océanis BET . Nantes Malakoff-Pré Gauchet , 2002-… Le nouveau Malakoff-sur-Loire Ce territoire , né de la Loire , longtemps inondable et remblayé dans les années 1960 par du sable , est délimité par le fleuve et le canal Saint-Félix .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Maîtrise d ' œuvre urbaine , maîtrise d ' œuvre des espaces publics : Atelier Ruelle et Océanis BET . Nantes Malakoff-Pré Gauchet , 2002-… Le nouveau Malakoff-sur-Loire Ce territoire , né de la Loire , longtemps inondable et remblayé dans les années 1960 par du sable , est délimité par le fleuve et le canal Saint-Félix .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ longtemps ]
epistemique :: 1 :: [ longtemps ]
marqueurs_niveau1 :: 1 :: [ longtemps ]
epistemique_niveau1 :: 1 :: [ longtemps ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Maîtrise d ' œuvre urbaine , maîtrise d ' œuvre des espaces publics : Atelier Ruelle et Océanis BET . Nantes Malakoff-Pré Gauchet , 2002-… Le nouveau Malakoff-sur-Loire Ce territoire , né de la Loire , longtemps inondable et remblayé dans les années 1960 par du sable , est délimité par le fleuve et le canal Saint-Félix .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['dans les années 1960', 'longtemps']
adverbiaux_localisation_temporelle::1::['dans les années 1960']
adverbiaux_duratifs_iteratifs::1::['longtemps']
adverbiaux_pointage_absolu::1::['dans les années 1960']
adverbiaux_loc_temp_focalisation_id::1::['dans les années 1960']
adverbiaux_loc_temp_regionalisation_id::1::['dans les années 1960']
adverbiaux_loc_temp_pointage_absolu::1::['dans les années 1960']

