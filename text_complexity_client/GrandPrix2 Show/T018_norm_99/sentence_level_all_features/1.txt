
********** Entity Names Features **********
*******************************************

1. Sentence: En réalité , à cette époque , les prototypes de mobilier urbain vieillissaient mal , car ils se démodaient souvent vite et ne pouvaient être remplacés en cas de détérioration , les séries étant trop faibles .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: En réalité , à cette époque , les prototypes de mobilier urbain vieillissaient mal , car ils se démodaient souvent vite et ne pouvaient être remplacés en cas de détérioration , les séries étant trop faibles .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En réalité , à cette époque , les prototypes de mobilier urbain vieillissaient mal , car ils se démodaient souvent vite et ne pouvaient être remplacés en cas de détérioration , les séries étant trop faibles .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ réalité, mal, souvent, vite, pouvoir ]
marqueurs_niveau3 :: 4 :: [ mal, souvent, vite, pouvoir ]
alethique :: 3 :: [ réalité, souvent, pouvoir ]
epistemique :: 2 :: [ vite, pouvoir ]
alethique_niveau3 :: 2 :: [ souvent, pouvoir ]
epistemique_niveau3 :: 2 :: [ vite, pouvoir ]
appreciatif :: 1 :: [ mal ]
axiologique :: 1 :: [ mal ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau1 :: 1 :: [ réalité ]
alethique_niveau1 :: 1 :: [ réalité ]
appreciatif_niveau3 :: 1 :: [ mal ]
axiologique_niveau3 :: 1 :: [ mal ]
deontique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En réalité , à cette époque , les prototypes de mobilier urbain vieillissaient mal , car ils se démodaient souvent vite et ne pouvaient être remplacés en cas de détérioration , les séries étant trop faibles .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['à cette époque', 'souvent']
adverbiaux_localisation_temporelle::1::['à cette époque']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_pointage_non_absolu::2::['à cette époque', 'à cette époque']
adverbiaux_loc_temp_focalisation_id::1::['à cette époque']
adverbiaux_loc_temp_regionalisation_id::1::['à cette époque']
adverbiaux_loc_temp_pointage_non_absolu::1::['à cette époque']
adverbiaux_iterateur_frequentiel::1::['souvent']
adverbiaux_dur_iter_anaphorique::1::['à cette époque']

