
********** Entity Names Features **********
*******************************************

1. Sentence: L ' État providence ne pourra pas répondre à tous les défis et conduit , avec le même argent , à faire autant mais différemment , et à regarder avec plus de liberté les règlements .
Entity Name (Type) :: État providence (MISC)


********** Emotions Features **********
***************************************

1. Sentence: L ' État providence ne pourra pas répondre à tous les défis et conduit , avec le même argent , à faire autant mais différemment , et à regarder avec plus de liberté les règlements .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: L ' État providence ne pourra pas répondre à tous les défis et conduit , avec le même argent , à faire autant mais différemment , et à regarder avec plus de liberté les règlements .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ pouvoir, liberté ]
alethique :: 1 :: [ pouvoir ]
appreciatif :: 1 :: [ liberté ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau1 :: 1 :: [ liberté ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau1 :: 1 :: [ liberté ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' État providence ne pourra pas répondre à tous les défis et conduit , avec le même argent , à faire autant mais différemment , et à regarder avec plus de liberté les règlements .
No features found.

