
********** Entity Names Features **********
*******************************************

1. Sentence: moi qui , servant exemplaire de la bonne conscience , me rangeais du côté du bonheur , du profit .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: moi qui , servant exemplaire de la bonne conscience , me rangeais du côté du bonheur , du profit .
Tokens having emotion: [ bonheur ]
Lemmas having emotion: [ bonheur ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: moi qui , servant exemplaire de la bonne conscience , me rangeais du côté du bonheur , du profit .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ lui, bon, bonheur ]
marqueurs_niveau3 :: 3 :: [ lui, bon, bonheur ]
appreciatif :: 2 :: [ bon, bonheur ]
appreciatif_niveau3 :: 2 :: [ bon, bonheur ]
alethique :: 1 :: [ lui ]
axiologique :: 1 :: [ bon ]
epistemique :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
axiologique_niveau3 :: 1 :: [ bon ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: moi qui , servant exemplaire de la bonne conscience , me rangeais du côté du bonheur , du profit .
No features found.

