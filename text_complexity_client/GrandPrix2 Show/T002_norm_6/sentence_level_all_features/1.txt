
********** Entity Names Features **********
*******************************************

1. Sentence: Avant mon séjour aux États-Unis en 1972 , la relative indigence des livres d ' urbanisme en France , à laquelle je me suis heurté , m ' a poussé sans cesse à essayer de capitaliser des expériences et de permettre de diffuser , de faire connaître les travaux émergents de professionnels , entre autres des étrangers , dans le souci de briser un ghetto intellectuel dans lequel nous avons trop souvent vécu .
Entity Name (Type) :: France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Avant mon séjour aux États-Unis en 1972 , la relative indigence des livres d ' urbanisme en France , à laquelle je me suis heurté , m ' a poussé sans cesse à essayer de capitaliser des expériences et de permettre de diffuser , de faire connaître les travaux émergents de professionnels , entre autres des étrangers , dans le souci de briser un ghetto intellectuel dans lequel nous avons trop souvent vécu .
Tokens having emotion: [ souci ]
Lemmas having emotion: [ souci ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Avant mon séjour aux États-Unis en 1972 , la relative indigence des livres d ' urbanisme en France , à laquelle je me suis heurté , m ' a poussé sans cesse à essayer de capitaliser des expériences et de permettre de diffuser , de faire connaître les travaux émergents de professionnels , entre autres des étrangers , dans le souci de briser un ghetto intellectuel dans lequel nous avons trop souvent vécu .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ essayer, permettre, connaître, travail, souci, souvent ]
marqueurs_niveau3 :: 4 :: [ essayer, permettre, travail, souvent ]
appreciatif :: 2 :: [ travail, souci ]
epistemique :: 2 :: [ essayer, connaître ]
marqueurs_niveau2 :: 2 :: [ connaître, souci ]
alethique :: 1 :: [ souvent ]
boulique :: 1 :: [ essayer ]
deontique :: 1 :: [ permettre ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau3 :: 1 :: [ travail ]
appreciatif_niveau2 :: 1 :: [ souci ]
boulique_niveau3 :: 1 :: [ essayer ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau3 :: 1 :: [ essayer ]
epistemique_niveau2 :: 1 :: [ connaître ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Avant mon séjour aux États-Unis en 1972 , la relative indigence des livres d ' urbanisme en France , à laquelle je me suis heurté , m ' a poussé sans cesse à essayer de capitaliser des expériences et de permettre de diffuser , de faire connaître les travaux émergents de professionnels , entre autres des étrangers , dans le souci de briser un ghetto intellectuel dans lequel nous avons trop souvent vécu .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['en 1972', 'sans cesse', 'trop souvent']
adverbiaux_localisation_temporelle::1::['en 1972']
adverbiaux_purement_iteratifs::1::['trop souvent']
adverbiaux_duratifs_iteratifs::1::['sans cesse']
adverbiaux_pointage_absolu::1::['en 1972']
adverbiaux_pointage_non_absolu::1::['en 1972']
adverbiaux_loc_temp_focalisation_id::1::['en 1972']
adverbiaux_loc_temp_regionalisation_id::1::['en 1972']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1972']
adverbiaux_iterateur_frequentiel::1::['trop souvent']
adverbiaux_dur_iter_absolu::1::['en 1972']

