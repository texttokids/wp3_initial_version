====== Sentence Level Features ======

1. Sentence: Mais ce parcours est aussi un parcours dans la commande qui a évolué en Europe comme en France , et qui montre clairement aujourd ' hui la volonté d ' intervenir par l ' action urbaine à des échelles qui semblaient , il y a peu de temps encore , inaccessibles .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['peu de temps', 'encore']
adverbiaux_localisation_temporelle::1::['peu de temps']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::2::['peu de temps', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['peu de temps']
adverbiaux_loc_temp_regionalisation_id::1::['peu de temps']
adverbiaux_loc_temp_pointage_non_absolu::1::['peu de temps']
adverbiaux_dur_iter_relatif::1::['encore']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['peu de temps', 'encore']
adverbiaux_localisation_temporelle::1::['peu de temps']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::2::['peu de temps', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['peu de temps']
adverbiaux_loc_temp_regionalisation_id::1::['peu de temps']
adverbiaux_loc_temp_pointage_non_absolu::1::['peu de temps']
adverbiaux_dur_iter_relatif::1::['encore']
Total feature counts: 10

