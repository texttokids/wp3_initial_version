
********** Entity Names Features **********
*******************************************

1. Sentence: Saclay n ' aurait pas pu démarrer sans la structure d ' État que j ' ai eu le privilège de diriger .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Saclay n ' aurait pas pu démarrer sans la structure d ' État que j ' ai eu le privilège de diriger .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Saclay n ' aurait pas pu démarrer sans la structure d ' État que j ' ai eu le privilège de diriger .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ pouvoir, privilège ]
deontique :: 2 :: [ pouvoir, privilège ]
alethique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ privilège ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau2 :: 1 :: [ privilège ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Saclay n ' aurait pas pu démarrer sans la structure d ' État que j ' ai eu le privilège de diriger .
No features found.

