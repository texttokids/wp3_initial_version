
********** Entity Names Features **********
*******************************************

1. Sentence: Se souvenant des enseignements qu ' il avait lui-même reçus de Bernard Rousseau et de Jacques Simon , ceux-ci sont largement fondés sur l ' observation et la perception du monde sensible .
Entity Name (Type) :: Jacques Simon (PER)


********** Emotions Features **********
***************************************

1. Sentence: Se souvenant des enseignements qu ' il avait lui-même reçus de Bernard Rousseau et de Jacques Simon , ceux-ci sont largement fondés sur l ' observation et la perception du monde sensible .
Tokens having emotion: [ sensible ]
Lemmas having emotion: [ sensible ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Se souvenant des enseignements qu ' il avait lui-même reçus de Bernard Rousseau et de Jacques Simon , ceux-ci sont largement fondés sur l ' observation et la perception du monde sensible .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ souvenir, sensible ]
pas_d_indication :: 2 :: [ souvenir, sensible ]
marqueurs_niveau0 :: 2 :: [ souvenir, sensible ]
pas_d_indication_niveau0 :: 2 :: [ souvenir, sensible ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Se souvenant des enseignements qu ' il avait lui-même reçus de Bernard Rousseau et de Jacques Simon , ceux-ci sont largement fondés sur l ' observation et la perception du monde sensible .
No features found.

