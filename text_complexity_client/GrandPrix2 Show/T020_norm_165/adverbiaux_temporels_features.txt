====== Sentence Level Features ======

1. Sentence: Il rejoint Versailles en 2004 , où il a dirigé le conseil d ' administration et le Conseil pédagogique et de la Recherche pendant plusieurs années .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en 2004', 'pendant plusieurs années']
adverbiaux_localisation_temporelle::1::['en 2004']
adverbiaux_duratifs_iteratifs::1::['pendant plusieurs années']
adverbiaux_pointage_absolu::1::['en 2004']
adverbiaux_pointage_non_absolu::2::['en 2004', 'pendant plusieurs années']
adverbiaux_loc_temp_focalisation_id::1::['en 2004']
adverbiaux_loc_temp_regionalisation_id::1::['en 2004']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 2004']
adverbiaux_dur_iter_absolu::1::['en 2004']
adverbiaux_dur_iter_deictique::1::['pendant plusieurs années']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en 2004', 'pendant plusieurs années']
adverbiaux_localisation_temporelle::1::['en 2004']
adverbiaux_duratifs_iteratifs::1::['pendant plusieurs années']
adverbiaux_pointage_absolu::1::['en 2004']
adverbiaux_pointage_non_absolu::2::['en 2004', 'pendant plusieurs années']
adverbiaux_loc_temp_focalisation_id::1::['en 2004']
adverbiaux_loc_temp_regionalisation_id::1::['en 2004']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 2004']
adverbiaux_dur_iter_absolu::1::['en 2004']
adverbiaux_dur_iter_deictique::1::['pendant plusieurs années']
Total feature counts: 12

