
********** Entity Names Features **********
*******************************************

1. Sentence: Par exemple , j ' ai construit à cette époque un foyer de personnes âgées sur une parcelle curieuse , entre deux pignons modernes non alignés , qui donne à la rue du Château des Rentiers (1982 , Paris XIIIe) une nouvelle lisibilité .
Entity Name (Type) :: Paris XIIIe (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Par exemple , j ' ai construit à cette époque un foyer de personnes âgées sur une parcelle curieuse , entre deux pignons modernes non alignés , qui donne à la rue du Château des Rentiers (1982 , Paris XIIIe) une nouvelle lisibilité .
Tokens having emotion: [ curieuse ]
Lemmas having emotion: [ curieux ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Par exemple , j ' ai construit à cette époque un foyer de personnes âgées sur une parcelle curieuse , entre deux pignons modernes non alignés , qui donne à la rue du Château des Rentiers (1982 , Paris XIIIe) une nouvelle lisibilité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ personne, curieux, moderne, château ]
epistemique :: 2 :: [ curieux, château ]
marqueurs_niveau2 :: 2 :: [ personne, curieux ]
alethique :: 1 :: [ personne ]
appreciatif :: 1 :: [ château ]
boulique :: 1 :: [ curieux ]
pas_d_indication :: 1 :: [ moderne ]
marqueurs_niveau3 :: 1 :: [ château ]
marqueurs_niveau0 :: 1 :: [ moderne ]
alethique_niveau2 :: 1 :: [ personne ]
appreciatif_niveau3 :: 1 :: [ château ]
boulique_niveau2 :: 1 :: [ curieux ]
epistemique_niveau3 :: 1 :: [ château ]
epistemique_niveau2 :: 1 :: [ curieux ]
pas_d_indication_niveau0 :: 1 :: [ moderne ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Par exemple , j ' ai construit à cette époque un foyer de personnes âgées sur une parcelle curieuse , entre deux pignons modernes non alignés , qui donne à la rue du Château des Rentiers (1982 , Paris XIIIe) une nouvelle lisibilité .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à cette époque']
adverbiaux_localisation_temporelle::1::['à cette époque']
adverbiaux_pointage_non_absolu::2::['à cette époque', 'à cette époque']
adverbiaux_loc_temp_focalisation_id::1::['à cette époque']
adverbiaux_loc_temp_regionalisation_id::1::['à cette époque']
adverbiaux_loc_temp_pointage_non_absolu::1::['à cette époque']
adverbiaux_dur_iter_anaphorique::1::['à cette époque']

