====== Sentence Level Features ======

1. Sentence: L ' ouverture du jeu des acteurs doit favoriser l ' émergence de formes de gouvernance nouvelles , différenciées selon les thèmes et les projets énoncés , sans essayer de deviner aujourd ' hui des réponses institutionnelles à des questions qui ne se poseront , peut-être , qu ' à long terme .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ devoir, favoriser, selon, essayer, peut-être, long ]
epistemique :: 5 :: [ devoir, selon, essayer, peut-être, long ]
marqueurs_niveau3 :: 4 :: [ devoir, selon, essayer, peut-être ]
epistemique_niveau3 :: 4 :: [ devoir, selon, essayer, peut-être ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ favoriser ]
boulique :: 1 :: [ essayer ]
deontique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ long ]
marqueurs_niveau1 :: 1 :: [ favoriser ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau1 :: 1 :: [ favoriser ]
boulique_niveau3 :: 1 :: [ essayer ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 30


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ devoir, favoriser, selon, essayer, peut-être, long ]
epistemique :: 5 :: [ devoir, selon, essayer, peut-être, long ]
marqueurs_niveau3 :: 4 :: [ devoir, selon, essayer, peut-être ]
epistemique_niveau3 :: 4 :: [ devoir, selon, essayer, peut-être ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ favoriser ]
boulique :: 1 :: [ essayer ]
deontique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ long ]
marqueurs_niveau1 :: 1 :: [ favoriser ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau1 :: 1 :: [ favoriser ]
boulique_niveau3 :: 1 :: [ essayer ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 30

