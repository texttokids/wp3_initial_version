
********** Entity Names Features **********
*******************************************

1. Sentence: Ce que l ' art apporte aux champs de l ' urbanisme L ' art comme guide de la pensée , de la créativité et comme fondateur d ' une nouvelle approche de l ' archi tecture et de l ' urbanisme , c ' est ce que revendique résolument Patrick Bouchain , nourri dans son exercice professionnel par des « maîtres » qui deviennent souvent des amis , tels Daniel Buren et Bartabas .
Entity Name (Type) :: Bartabas (PER)


********** Emotions Features **********
***************************************

1. Sentence: Ce que l ' art apporte aux champs de l ' urbanisme L ' art comme guide de la pensée , de la créativité et comme fondateur d ' une nouvelle approche de l ' archi tecture et de l ' urbanisme , c ' est ce que revendique résolument Patrick Bouchain , nourri dans son exercice professionnel par des « maîtres » qui deviennent souvent des amis , tels Daniel Buren et Bartabas .
Tokens having emotion: [ amis ]
Lemmas having emotion: [ ami ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: Ce que l ' art apporte aux champs de l ' urbanisme L ' art comme guide de la pensée , de la créativité et comme fondateur d ' une nouvelle approche de l ' archi tecture et de l ' urbanisme , c ' est ce que revendique résolument Patrick Bouchain , nourri dans son exercice professionnel par des « maîtres » qui deviennent souvent des amis , tels Daniel Buren et Bartabas .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ revendiquer, souvent, ami ]
alethique :: 1 :: [ souvent ]
appreciatif :: 1 :: [ ami ]
boulique :: 1 :: [ revendiquer ]
deontique :: 1 :: [ revendiquer ]
marqueurs_niveau3 :: 1 :: [ souvent ]
marqueurs_niveau2 :: 1 :: [ ami ]
marqueurs_niveau1 :: 1 :: [ revendiquer ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau2 :: 1 :: [ ami ]
boulique_niveau1 :: 1 :: [ revendiquer ]
deontique_niveau1 :: 1 :: [ revendiquer ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce que l ' art apporte aux champs de l ' urbanisme L ' art comme guide de la pensée , de la créativité et comme fondateur d ' une nouvelle approche de l ' archi tecture et de l ' urbanisme , c ' est ce que revendique résolument Patrick Bouchain , nourri dans son exercice professionnel par des « maîtres » qui deviennent souvent des amis , tels Daniel Buren et Bartabas .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

