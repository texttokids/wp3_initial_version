
********** Entity Names Features **********
*******************************************

1. Sentence: L ' aménagement et les transports avaient évidemment une place importante à tenir dans la partition d ' ensemble , car on ne pouvait pas imaginer un complexe vivant et attractif sans sortir de l ' anomie urbaine existante , sans susciter une nouvelle urbanité , sans recréer quelque chose qui ressemble à un vrai campus .
Entity Name (Type) :: L (LOC)


********** Emotions Features **********
***************************************

1. Sentence: L ' aménagement et les transports avaient évidemment une place importante à tenir dans la partition d ' ensemble , car on ne pouvait pas imaginer un complexe vivant et attractif sans sortir de l ' anomie urbaine existante , sans susciter une nouvelle urbanité , sans recréer quelque chose qui ressemble à un vrai campus .
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' aménagement et les transports avaient évidemment une place importante à tenir dans la partition d ' ensemble , car on ne pouvait pas imaginer un complexe vivant et attractif sans sortir de l ' anomie urbaine existante , sans susciter une nouvelle urbanité , sans recréer quelque chose qui ressemble à un vrai campus .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ évidemment, important, pouvoir, imaginer, vrai ]
epistemique :: 3 :: [ important, pouvoir, imaginer ]
alethique :: 2 :: [ pouvoir, vrai ]
marqueurs_niveau3 :: 2 :: [ pouvoir, imaginer ]
marqueurs_niveau1 :: 2 :: [ important, vrai ]
epistemique_niveau3 :: 2 :: [ pouvoir, imaginer ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
deontique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ évidemment ]
marqueurs_niveau0 :: 1 :: [ évidemment ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau1 :: 1 :: [ vrai ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ évidemment ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' aménagement et les transports avaient évidemment une place importante à tenir dans la partition d ' ensemble , car on ne pouvait pas imaginer un complexe vivant et attractif sans sortir de l ' anomie urbaine existante , sans susciter une nouvelle urbanité , sans recréer quelque chose qui ressemble à un vrai campus .
No features found.

