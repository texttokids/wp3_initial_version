
********** Entity Names Features **********
*******************************************

1. Sentence: Dès lors ,  l ' objectif fut de proposer plus d ' aisance , de créer le désir de les emprunter .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Dès lors ,  l ' objectif fut de proposer plus d ' aisance , de créer le désir de les emprunter .
Tokens having emotion: [ aisance, désir ]
Lemmas having emotion: [ aisance, désir ]
Categories of the emotion lemmas: [ audace, desir ]


********** Modality Features **********
***************************************

1. Sentence: Dès lors ,  l ' objectif fut de proposer plus d ' aisance , de créer le désir de les emprunter .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ désir ]
boulique :: 1 :: [ désir ]
marqueurs_niveau3 :: 1 :: [ désir ]
boulique_niveau3 :: 1 :: [ désir ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dès lors ,  l ' objectif fut de proposer plus d ' aisance , de créer le désir de les emprunter .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['Dès lors']
adverbiaux_localisation_temporelle::1::['Dès lors']
adverbiaux_pointage_non_absolu::1::['Dès lors']
adverbiaux_loc_temp_focalisation_id::1::['Dès lors']
adverbiaux_loc_temp_regionalisation_non_id::1::['Dès lors']
adverbiaux_loc_temp_pointage_non_absolu::1::['Dès lors']

