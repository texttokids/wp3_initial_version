
********** Entity Names Features **********
*******************************************

1. Sentence: Ce qui vaut pour les lieux hautement technologiques est vrai aussi , du reste , pour l ' autre extrémité du spectre social , c ' est‑à‑dire les mondes des individus et des groupes exclus ou marginaux par rapport à la société « ordinaire » , celle du smartphone pour tous et des journées passées devant les écrans .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Ce qui vaut pour les lieux hautement technologiques est vrai aussi , du reste , pour l ' autre extrémité du spectre social , c ' est‑à‑dire les mondes des individus et des groupes exclus ou marginaux par rapport à la société « ordinaire » , celle du smartphone pour tous et des journées passées devant les écrans .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ce qui vaut pour les lieux hautement technologiques est vrai aussi , du reste , pour l ' autre extrémité du spectre social , c ' est‑à‑dire les mondes des individus et des groupes exclus ou marginaux par rapport à la société « ordinaire » , celle du smartphone pour tous et des journées passées devant les écrans .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ vrai ]
alethique :: 1 :: [ vrai ]
marqueurs_niveau1 :: 1 :: [ vrai ]
alethique_niveau1 :: 1 :: [ vrai ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce qui vaut pour les lieux hautement technologiques est vrai aussi , du reste , pour l ' autre extrémité du spectre social , c ' est‑à‑dire les mondes des individus et des groupes exclus ou marginaux par rapport à la société « ordinaire » , celle du smartphone pour tous et des journées passées devant les écrans .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['des journées passées']
adverbiaux_localisation_temporelle::1::['des journées passées']
adverbiaux_pointage_non_absolu::1::['des journées passées']
adverbiaux_loc_temp_focalisation_id::1::['des journées passées']
adverbiaux_loc_temp_regionalisation_id::1::['des journées passées']
adverbiaux_loc_temp_pointage_non_absolu::1::['des journées passées']

