
********** Entity Names Features **********
*******************************************

1. Sentence: En 1995 , cet accompagnement s ' est concrétisé par la présence de Dominique Bailly , artiste qui avait choisi d ' œuvrer dans la nature , proche ainsi du Land Art et de tous ceux qui manifestaient depuis les années soixante un autre regard sur la Terre .
Entity Name (Type) :: Terre (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En 1995 , cet accompagnement s ' est concrétisé par la présence de Dominique Bailly , artiste qui avait choisi d ' œuvrer dans la nature , proche ainsi du Land Art et de tous ceux qui manifestaient depuis les années soixante un autre regard sur la Terre .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 1995 , cet accompagnement s ' est concrétisé par la présence de Dominique Bailly , artiste qui avait choisi d ' œuvrer dans la nature , proche ainsi du Land Art et de tous ceux qui manifestaient depuis les années soixante un autre regard sur la Terre .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ choisir, manifester ]
boulique :: 1 :: [ choisir ]
pas_d_indication :: 1 :: [ manifester ]
marqueurs_niveau2 :: 1 :: [ choisir ]
marqueurs_niveau0 :: 1 :: [ manifester ]
boulique_niveau2 :: 1 :: [ choisir ]
pas_d_indication_niveau0 :: 1 :: [ manifester ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 1995 , cet accompagnement s ' est concrétisé par la présence de Dominique Bailly , artiste qui avait choisi d ' œuvrer dans la nature , proche ainsi du Land Art et de tous ceux qui manifestaient depuis les années soixante un autre regard sur la Terre .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['En 1995', 'depuis les années soixante']
adverbiaux_localisation_temporelle::2::['En 1995', 'depuis les années soixante']
adverbiaux_pointage_absolu::2::['En 1995', 'depuis les années soixante']
adverbiaux_pointage_non_absolu::1::['En 1995']
adverbiaux_loc_temp_focalisation_id::2::['En 1995', 'depuis les années soixante']
adverbiaux_loc_temp_regionalisation_id::1::['En 1995']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis les années soixante']
adverbiaux_loc_temp_pointage_absolu::1::['depuis les années soixante']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 1995']
adverbiaux_dur_iter_absolu::1::['En 1995']

