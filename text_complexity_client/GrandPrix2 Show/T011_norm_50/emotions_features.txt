====== Sentence Level Features ======

1. Sentence: On ne rendra jamais assez hommage à Pierre-Yves Ligen , alors jeune conseiller d ' État intrépide , mort en cette année 2001 , d ' avoir su réunir et mener à la victoire sur des services parisiens rénovateurs endiablés , une équipe impertinente composée d ' individualités bien affirmées .
Tokens having emotion: [ État, intrépide ]
Lemmas having emotion: [ état, intrépide ]
Categories of the emotion lemmas: [ non_specifiee, audace ]


====== Text Level Features ======

Tokens having emotion: [ État, intrépide ]
Lemmas having emotion: [ état, intrépide ]
Category of the emotion lemmas: [ non_specifiee, audace ]
Total emotion lemma counts: 2

