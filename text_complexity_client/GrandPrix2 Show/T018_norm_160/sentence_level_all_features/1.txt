
********** Entity Names Features **********
*******************************************

1. Sentence: J ' y ai pénétré tel un chevalier plein d ' espoir : ronces entremêlées , arbres couchés , communs effondrés , fenêtres défoncées , château pillé , défoncé , en partie brûlé .
Entity Name (Type) :: J (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' y ai pénétré tel un chevalier plein d ' espoir : ronces entremêlées , arbres couchés , communs effondrés , fenêtres défoncées , château pillé , défoncé , en partie brûlé .
Tokens having emotion: [ espoir ]
Lemmas having emotion: [ espoir ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: J ' y ai pénétré tel un chevalier plein d ' espoir : ronces entremêlées , arbres couchés , communs effondrés , fenêtres défoncées , château pillé , défoncé , en partie brûlé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ espoir, château, piller, brûler ]
appreciatif :: 2 :: [ château, brûler ]
axiologique :: 2 :: [ piller, brûler ]
epistemique :: 2 :: [ espoir, château ]
marqueurs_niveau2 :: 2 :: [ espoir, piller ]
boulique :: 1 :: [ espoir ]
marqueurs_niveau3 :: 1 :: [ château ]
marqueurs_niveau1 :: 1 :: [ brûler ]
appreciatif_niveau3 :: 1 :: [ château ]
appreciatif_niveau1 :: 1 :: [ brûler ]
axiologique_niveau2 :: 1 :: [ piller ]
axiologique_niveau1 :: 1 :: [ brûler ]
boulique_niveau2 :: 1 :: [ espoir ]
epistemique_niveau3 :: 1 :: [ château ]
epistemique_niveau2 :: 1 :: [ espoir ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' y ai pénétré tel un chevalier plein d ' espoir : ronces entremêlées , arbres couchés , communs effondrés , fenêtres défoncées , château pillé , défoncé , en partie brûlé .
No features found.

