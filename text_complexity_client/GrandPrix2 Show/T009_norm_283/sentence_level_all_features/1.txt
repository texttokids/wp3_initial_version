
********** Entity Names Features **********
*******************************************

1. Sentence: Cette démarche en forme de manifeste réclamait pour la banlieue plus d ' attention et voulait rompre avec l ' insolent isolement de celle-ci .
Entity Name (Type) :: Cette (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette démarche en forme de manifeste réclamait pour la banlieue plus d ' attention et voulait rompre avec l ' insolent isolement de celle-ci .
Tokens having emotion: [ isolement ]
Lemmas having emotion: [ isolement ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Cette démarche en forme de manifeste réclamait pour la banlieue plus d ' attention et voulait rompre avec l ' insolent isolement de celle-ci .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ réclamer, vouloir ]
boulique :: 2 :: [ réclamer, vouloir ]
marqueurs_niveau3 :: 1 :: [ vouloir ]
marqueurs_niveau1 :: 1 :: [ réclamer ]
boulique_niveau3 :: 1 :: [ vouloir ]
boulique_niveau1 :: 1 :: [ réclamer ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette démarche en forme de manifeste réclamait pour la banlieue plus d ' attention et voulait rompre avec l ' insolent isolement de celle-ci .
No features found.

