
********** Entity Names Features **********
*******************************************

1. Sentence: Un passé de militant politique et syndical m ' a formé à la nécessité de garder toujours présent à l ' esprit le besoin de stratégie et de programme dans la conception d ' un projet à porter et à mettre en œuvre à moyen et long terme .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Un passé de militant politique et syndical m ' a formé à la nécessité de garder toujours présent à l ' esprit le besoin de stratégie et de programme dans la conception d ' un projet à porter et à mettre en œuvre à moyen et long terme .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Un passé de militant politique et syndical m ' a formé à la nécessité de garder toujours présent à l ' esprit le besoin de stratégie et de programme dans la conception d ' un projet à porter et à mettre en œuvre à moyen et long terme .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ nécessité, toujours, esprit, besoin, long ]
marqueurs_niveau3 :: 4 :: [ nécessité, toujours, esprit, besoin ]
alethique :: 3 :: [ nécessité, toujours, besoin ]
alethique_niveau3 :: 3 :: [ nécessité, toujours, besoin ]
epistemique :: 2 :: [ esprit, long ]
boulique :: 1 :: [ besoin ]
marqueurs_niveau2 :: 1 :: [ long ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ esprit ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un passé de militant politique et syndical m ' a formé à la nécessité de garder toujours présent à l ' esprit le besoin de stratégie et de programme dans la conception d ' un projet à porter et à mettre en œuvre à moyen et long terme .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

