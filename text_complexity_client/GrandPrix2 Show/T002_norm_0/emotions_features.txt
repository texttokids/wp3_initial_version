====== Sentence Level Features ======

1. Sentence: Grand Prix de l ' urbanisme 2006 L ' exercice périlleux qui consiste à relater son parcours professionnel , conceptuel et méthodologique m ' offre l ' occasion stimulante de remettre en perspective un long cheminement et d ' en chercher une cohérence : trente années qui m ' ont conduit de Nancy à Bordeaux en opérant un véritable tour de France , tel un compagnon du devoir , me faisant passer du métier peu palpitant d ' assistant ingénieur à celui passionnant d ' urbaniste .
Tokens having emotion: [ périlleux, palpitant ]
Lemmas having emotion: [ périlleux, palpitant ]
Categories of the emotion lemmas: [ peur, comportement ]


====== Text Level Features ======

Tokens having emotion: [ périlleux, palpitant ]
Lemmas having emotion: [ périlleux, palpitant ]
Category of the emotion lemmas: [ peur, comportement ]
Total emotion lemma counts: 2

