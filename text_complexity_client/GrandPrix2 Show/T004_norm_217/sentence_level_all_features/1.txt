
********** Entity Names Features **********
*******************************************

1. Sentence: Cette générosité et cette énergie ont eu pour effet immédiat de « sauver » l ' École : définitivement détachée de l ' École d ' horticulture , la section paysage devient , en 1977 ,  l ' École nationale supérieure du paysage .
Entity Name (Type) :: École nationale supérieure du paysage (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Cette générosité et cette énergie ont eu pour effet immédiat de « sauver » l ' École : définitivement détachée de l ' École d ' horticulture , la section paysage devient , en 1977 ,  l ' École nationale supérieure du paysage .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Cette générosité et cette énergie ont eu pour effet immédiat de « sauver » l ' École : définitivement détachée de l ' École d ' horticulture , la section paysage devient , en 1977 ,  l ' École nationale supérieure du paysage .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ sauver, école, école, école ]
alethique :: 3 :: [ école, école, école ]
marqueurs_niveau1 :: 3 :: [ école, école, école ]
alethique_niveau1 :: 3 :: [ école, école, école ]
appreciatif :: 1 :: [ sauver ]
marqueurs_niveau2 :: 1 :: [ sauver ]
appreciatif_niveau2 :: 1 :: [ sauver ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette générosité et cette énergie ont eu pour effet immédiat de « sauver » l ' École : définitivement détachée de l ' École d ' horticulture , la section paysage devient , en 1977 ,  l ' École nationale supérieure du paysage .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en 1977']
adverbiaux_localisation_temporelle::1::['en 1977']
adverbiaux_pointage_absolu::1::['en 1977']
adverbiaux_pointage_non_absolu::1::['en 1977']
adverbiaux_loc_temp_focalisation_id::1::['en 1977']
adverbiaux_loc_temp_regionalisation_id::1::['en 1977']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1977']
adverbiaux_dur_iter_absolu::1::['en 1977']

