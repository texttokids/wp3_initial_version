
********** Entity Names Features **********
*******************************************

1. Sentence: On a beau regretter les bords francs que l ' utopie moderne a si mal expliqués — aimer le pavillon de Barcelone , l ' effort d ' un architecte comme Louis Kahn pour capturer l ' espace , celui de Sejima pour ne pas le blesser — , il n ' empêche que le flou , les écarts qui se sont installés nous portent bien plus efficacement que nous ne sommes à même de les discipliner;
Entity Name (Type) :: Sejima (PER)


********** Emotions Features **********
***************************************

1. Sentence: On a beau regretter les bords francs que l ' utopie moderne a si mal expliqués — aimer le pavillon de Barcelone , l ' effort d ' un architecte comme Louis Kahn pour capturer l ' espace , celui de Sejima pour ne pas le blesser — , il n ' empêche que le flou , les écarts qui se sont installés nous portent bien plus efficacement que nous ne sommes à même de les discipliner;
Tokens having emotion: [ regretter, aimer ]
Lemmas having emotion: [ regretter, aimer ]
Categories of the emotion lemmas: [ culpabilite, amour ]


********** Modality Features **********
***************************************

1. Sentence: On a beau regretter les bords francs que l ' utopie moderne a si mal expliqués — aimer le pavillon de Barcelone , l ' effort d ' un architecte comme Louis Kahn pour capturer l ' espace , celui de Sejima pour ne pas le blesser — , il n ' empêche que le flou , les écarts qui se sont installés nous portent bien plus efficacement que nous ne sommes à même de les discipliner;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 8 :: [ beau, regretter, moderne, mal, aimer, blesser, empêcher, bien ]
appreciatif :: 6 :: [ beau, regretter, mal, aimer, blesser, bien ]
marqueurs_niveau3 :: 5 :: [ beau, regretter, mal, aimer, bien ]
appreciatif_niveau3 :: 5 :: [ beau, regretter, mal, aimer, bien ]
axiologique :: 2 :: [ mal, bien ]
pas_d_indication :: 2 :: [ moderne, empêcher ]
marqueurs_niveau0 :: 2 :: [ moderne, empêcher ]
axiologique_niveau3 :: 2 :: [ mal, bien ]
pas_d_indication_niveau0 :: 2 :: [ moderne, empêcher ]
epistemique :: 1 :: [ regretter ]
marqueurs_niveau2 :: 1 :: [ blesser ]
appreciatif_niveau2 :: 1 :: [ blesser ]
epistemique_niveau3 :: 1 :: [ regretter ]
Total feature counts: 38


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: On a beau regretter les bords francs que l ' utopie moderne a si mal expliqués — aimer le pavillon de Barcelone , l ' effort d ' un architecte comme Louis Kahn pour capturer l ' espace , celui de Sejima pour ne pas le blesser — , il n ' empêche que le flou , les écarts qui se sont installés nous portent bien plus efficacement que nous ne sommes à même de les discipliner;
No features found.

