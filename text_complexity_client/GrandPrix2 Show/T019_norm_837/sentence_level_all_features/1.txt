
********** Entity Names Features **********
*******************************************

1. Sentence: Desvigne a dessiné des allées généreuses , très plantées , que j ' aurais parfois imaginées plus modestes .
Entity Name (Type) :: Desvigne (PER)


********** Emotions Features **********
***************************************

1. Sentence: Desvigne a dessiné des allées généreuses , très plantées , que j ' aurais parfois imaginées plus modestes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Desvigne a dessiné des allées généreuses , très plantées , que j ' aurais parfois imaginées plus modestes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ généreux, parfois, imaginer ]
marqueurs_niveau3 :: 3 :: [ généreux, parfois, imaginer ]
alethique :: 1 :: [ parfois ]
appreciatif :: 1 :: [ généreux ]
axiologique :: 1 :: [ généreux ]
epistemique :: 1 :: [ imaginer ]
alethique_niveau3 :: 1 :: [ parfois ]
appreciatif_niveau3 :: 1 :: [ généreux ]
axiologique_niveau3 :: 1 :: [ généreux ]
epistemique_niveau3 :: 1 :: [ imaginer ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Desvigne a dessiné des allées généreuses , très plantées , que j ' aurais parfois imaginées plus modestes .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

