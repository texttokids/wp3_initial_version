
********** Entity Names Features **********
*******************************************

1. Sentence: Plus encore , la construction est essentiellement réalisée par des gens de condition modeste , très souvent immigrés , comme si de la pauvreté venait sans cesse des savoir-faire qu ' ils ont amenés avec eux qu ' il faudrait exploiter pour construire en France .
Entity Name (Type) :: France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Plus encore , la construction est essentiellement réalisée par des gens de condition modeste , très souvent immigrés , comme si de la pauvreté venait sans cesse des savoir-faire qu ' ils ont amenés avec eux qu ' il faudrait exploiter pour construire en France .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Plus encore , la construction est essentiellement réalisée par des gens de condition modeste , très souvent immigrés , comme si de la pauvreté venait sans cesse des savoir-faire qu ' ils ont amenés avec eux qu ' il faudrait exploiter pour construire en France .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ réaliser, souvent, pauvreté, venir, lui, falloir, exploiter ]
alethique :: 4 :: [ réaliser, souvent, venir, lui ]
marqueurs_niveau3 :: 4 :: [ souvent, venir, lui, falloir ]
alethique_niveau3 :: 3 :: [ souvent, venir, lui ]
marqueurs_niveau1 :: 2 :: [ réaliser, exploiter ]
appreciatif :: 1 :: [ exploiter ]
axiologique :: 1 :: [ exploiter ]
deontique :: 1 :: [ falloir ]
epistemique :: 1 :: [ lui ]
pas_d_indication :: 1 :: [ pauvreté ]
marqueurs_niveau0 :: 1 :: [ pauvreté ]
alethique_niveau1 :: 1 :: [ réaliser ]
appreciatif_niveau1 :: 1 :: [ exploiter ]
axiologique_niveau1 :: 1 :: [ exploiter ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ lui ]
pas_d_indication_niveau0 :: 1 :: [ pauvreté ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Plus encore , la construction est essentiellement réalisée par des gens de condition modeste , très souvent immigrés , comme si de la pauvreté venait sans cesse des savoir-faire qu ' ils ont amenés avec eux qu ' il faudrait exploiter pour construire en France .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['encore', 'très souvent', 'sans cesse']
adverbiaux_purement_iteratifs::1::['très souvent']
adverbiaux_duratifs_iteratifs::2::['encore', 'sans cesse']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_iterateur_frequentiel::1::['très souvent']
adverbiaux_dur_iter_relatif::1::['encore']

