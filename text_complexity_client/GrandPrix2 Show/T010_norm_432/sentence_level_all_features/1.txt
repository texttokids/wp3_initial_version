
********** Entity Names Features **********
*******************************************

1. Sentence: L ' exigence de circuits courts Face à cette accélération des estimations les plus pessimistes il faut assurer des politiques de recyclage et de moindres déplacements dans trois domaines .
Entity Name (Type) :: Face (MISC)


********** Emotions Features **********
***************************************

1. Sentence: L ' exigence de circuits courts Face à cette accélération des estimations les plus pessimistes il faut assurer des politiques de recyclage et de moindres déplacements dans trois domaines .
Tokens having emotion: [ pessimistes ]
Lemmas having emotion: [ pessimiste ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: L ' exigence de circuits courts Face à cette accélération des estimations les plus pessimistes il faut assurer des politiques de recyclage et de moindres déplacements dans trois domaines .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ falloir, moindre ]
deontique :: 1 :: [ falloir ]
epistemique :: 1 :: [ moindre ]
marqueurs_niveau3 :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ moindre ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau2 :: 1 :: [ moindre ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' exigence de circuits courts Face à cette accélération des estimations les plus pessimistes il faut assurer des politiques de recyclage et de moindres déplacements dans trois domaines .
No features found.

