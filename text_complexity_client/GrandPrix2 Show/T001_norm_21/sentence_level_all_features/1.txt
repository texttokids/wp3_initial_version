
********** Entity Names Features **********
*******************************************

1. Sentence: Ils ont laissé sur place les rêves à la Poussin du Mouvement moderne , Chandigarh , Brasilia , les lacs encore intacts de Futur System et l ' Atlanpole que Nantes avait un moment adoptée .
Entity Name (Type) :: Nantes (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ils ont laissé sur place les rêves à la Poussin du Mouvement moderne , Chandigarh , Brasilia , les lacs encore intacts de Futur System et l ' Atlanpole que Nantes avait un moment adoptée .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ils ont laissé sur place les rêves à la Poussin du Mouvement moderne , Chandigarh , Brasilia , les lacs encore intacts de Futur System et l ' Atlanpole que Nantes avait un moment adoptée .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ laisser, rêve, moderne ]
alethique :: 1 :: [ rêve ]
appreciatif :: 1 :: [ rêve ]
deontique :: 1 :: [ laisser ]
pas_d_indication :: 1 :: [ moderne ]
marqueurs_niveau2 :: 1 :: [ laisser ]
marqueurs_niveau1 :: 1 :: [ rêve ]
marqueurs_niveau0 :: 1 :: [ moderne ]
alethique_niveau1 :: 1 :: [ rêve ]
appreciatif_niveau1 :: 1 :: [ rêve ]
deontique_niveau2 :: 1 :: [ laisser ]
pas_d_indication_niveau0 :: 1 :: [ moderne ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ils ont laissé sur place les rêves à la Poussin du Mouvement moderne , Chandigarh , Brasilia , les lacs encore intacts de Futur System et l ' Atlanpole que Nantes avait un moment adoptée .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

