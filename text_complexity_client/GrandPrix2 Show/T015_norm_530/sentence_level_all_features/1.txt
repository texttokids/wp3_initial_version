
********** Entity Names Features **********
*******************************************

1. Sentence: Il prend place aussi hors des petites villes , dans une campagne remembrée , réservoir de précarité et de peurs .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il prend place aussi hors des petites villes , dans une campagne remembrée , réservoir de précarité et de peurs .
Tokens having emotion: [ peurs ]
Lemmas having emotion: [ peur ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Il prend place aussi hors des petites villes , dans une campagne remembrée , réservoir de précarité et de peurs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ petit, peur ]
epistemique :: 2 :: [ petit, peur ]
appreciatif :: 1 :: [ peur ]
marqueurs_niveau3 :: 1 :: [ petit ]
marqueurs_niveau2 :: 1 :: [ peur ]
appreciatif_niveau2 :: 1 :: [ peur ]
epistemique_niveau3 :: 1 :: [ petit ]
epistemique_niveau2 :: 1 :: [ peur ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il prend place aussi hors des petites villes , dans une campagne remembrée , réservoir de précarité et de peurs .
No features found.

