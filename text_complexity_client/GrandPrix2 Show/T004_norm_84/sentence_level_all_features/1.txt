
********** Entity Names Features **********
*******************************************

1. Sentence: Géométrie et tracés Autre difficulté du projet sur le paysage : tout ce qu ' il considère est fuyant , gauche , indiscipliné , circonstanciel .
Entity Name (Type) :: Autre (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Géométrie et tracés Autre difficulté du projet sur le paysage : tout ce qu ' il considère est fuyant , gauche , indiscipliné , circonstanciel .
Tokens having emotion: [ considère ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Géométrie et tracés Autre difficulté du projet sur le paysage : tout ce qu ' il considère est fuyant , gauche , indiscipliné , circonstanciel .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ difficulté, considérer, fuir ]
appreciatif :: 1 :: [ fuir ]
axiologique :: 1 :: [ fuir ]
boulique :: 1 :: [ fuir ]
epistemique :: 1 :: [ considérer ]
pas_d_indication :: 1 :: [ difficulté ]
marqueurs_niveau3 :: 1 :: [ considérer ]
marqueurs_niveau1 :: 1 :: [ fuir ]
marqueurs_niveau0 :: 1 :: [ difficulté ]
appreciatif_niveau1 :: 1 :: [ fuir ]
axiologique_niveau1 :: 1 :: [ fuir ]
boulique_niveau1 :: 1 :: [ fuir ]
epistemique_niveau3 :: 1 :: [ considérer ]
pas_d_indication_niveau0 :: 1 :: [ difficulté ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Géométrie et tracés Autre difficulté du projet sur le paysage : tout ce qu ' il considère est fuyant , gauche , indiscipliné , circonstanciel .
No features found.

