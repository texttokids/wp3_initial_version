
********** Entity Names Features **********
*******************************************

1. Sentence: L ' entre prise de chaque projet urbain est une nouvelle aventure , qui est à définir dans un contexte particulier , mais aussi en fonction des ambiances , de l ' état de l ' opinion publique et de la culture locales , si différentes d ' une ville à l ' autre .
Entity Name (Type) :: L (MISC)


********** Emotions Features **********
***************************************

1. Sentence: L ' entre prise de chaque projet urbain est une nouvelle aventure , qui est à définir dans un contexte particulier , mais aussi en fonction des ambiances , de l ' état de l ' opinion publique et de la culture locales , si différentes d ' une ville à l ' autre .
Tokens having emotion: [ état ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: L ' entre prise de chaque projet urbain est une nouvelle aventure , qui est à définir dans un contexte particulier , mais aussi en fonction des ambiances , de l ' état de l ' opinion publique et de la culture locales , si différentes d ' une ville à l ' autre .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ aventure ]
pas_d_indication :: 1 :: [ aventure ]
marqueurs_niveau0 :: 1 :: [ aventure ]
pas_d_indication_niveau0 :: 1 :: [ aventure ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' entre prise de chaque projet urbain est une nouvelle aventure , qui est à définir dans un contexte particulier , mais aussi en fonction des ambiances , de l ' état de l ' opinion publique et de la culture locales , si différentes d ' une ville à l ' autre .
No features found.

