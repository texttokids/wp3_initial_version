
********** Entity Names Features **********
*******************************************

1. Sentence: celles produites dans des villes ayant subi de plein fouet les différentes restructurations industrielles , comme Firminy ou Briey-en-Forêt , traversent ou ont traversé de graves crises .
Entity Name (Type) :: Briey-en-Forêt (LOC)


********** Emotions Features **********
***************************************

1. Sentence: celles produites dans des villes ayant subi de plein fouet les différentes restructurations industrielles , comme Firminy ou Briey-en-Forêt , traversent ou ont traversé de graves crises .
No features found.


********** Modality Features **********
***************************************

1. Sentence: celles produites dans des villes ayant subi de plein fouet les différentes restructurations industrielles , comme Firminy ou Briey-en-Forêt , traversent ou ont traversé de graves crises .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ subir, grave, crise ]
marqueurs_niveau2 :: 3 :: [ subir, grave, crise ]
appreciatif :: 2 :: [ grave, crise ]
appreciatif_niveau2 :: 2 :: [ grave, crise ]
axiologique :: 1 :: [ grave ]
boulique :: 1 :: [ subir ]
epistemique :: 1 :: [ crise ]
axiologique_niveau2 :: 1 :: [ grave ]
boulique_niveau2 :: 1 :: [ subir ]
epistemique_niveau2 :: 1 :: [ crise ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: celles produites dans des villes ayant subi de plein fouet les différentes restructurations industrielles , comme Firminy ou Briey-en-Forêt , traversent ou ont traversé de graves crises .
No features found.

