
********** Entity Names Features **********
*******************************************

1. Sentence: L ' intense besoin d ' apprendre les bases théoriques de ce métier , grâce à mes pérégrinations universitaires à Bruxelles , Paris et Philadelphie , m ' a permis de me doter d ' un bagage assez volumineux et de diplômes suffisamment reconnus pour oublier la blessure d ' études interrompues , puis reprises dans des conditions parfois difficiles .
Entity Name (Type) :: Philadelphie (LOC)


********** Emotions Features **********
***************************************

1. Sentence: L ' intense besoin d ' apprendre les bases théoriques de ce métier , grâce à mes pérégrinations universitaires à Bruxelles , Paris et Philadelphie , m ' a permis de me doter d ' un bagage assez volumineux et de diplômes suffisamment reconnus pour oublier la blessure d ' études interrompues , puis reprises dans des conditions parfois difficiles .
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' intense besoin d ' apprendre les bases théoriques de ce métier , grâce à mes pérégrinations universitaires à Bruxelles , Paris et Philadelphie , m ' a permis de me doter d ' un bagage assez volumineux et de diplômes suffisamment reconnus pour oublier la blessure d ' études interrompues , puis reprises dans des conditions parfois difficiles .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ besoin, permettre, suffisamment, oublier, parfois, difficile ]
marqueurs_niveau3 :: 3 :: [ besoin, permettre, parfois ]
alethique :: 2 :: [ besoin, parfois ]
pas_d_indication :: 2 :: [ suffisamment, oublier ]
marqueurs_niveau0 :: 2 :: [ suffisamment, oublier ]
alethique_niveau3 :: 2 :: [ besoin, parfois ]
pas_d_indication_niveau0 :: 2 :: [ suffisamment, oublier ]
appreciatif :: 1 :: [ difficile ]
boulique :: 1 :: [ besoin ]
deontique :: 1 :: [ permettre ]
epistemique :: 1 :: [ difficile ]
marqueurs_niveau2 :: 1 :: [ difficile ]
appreciatif_niveau2 :: 1 :: [ difficile ]
boulique_niveau3 :: 1 :: [ besoin ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau2 :: 1 :: [ difficile ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' intense besoin d ' apprendre les bases théoriques de ce métier , grâce à mes pérégrinations universitaires à Bruxelles , Paris et Philadelphie , m ' a permis de me doter d ' un bagage assez volumineux et de diplômes suffisamment reconnus pour oublier la blessure d ' études interrompues , puis reprises dans des conditions parfois difficiles .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

