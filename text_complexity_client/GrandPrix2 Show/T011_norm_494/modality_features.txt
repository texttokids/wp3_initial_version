====== Sentence Level Features ======

1. Sentence: Il peut y perdre sa route , sinon son âme , s ' il ne travaille pas en confiance et en dialogue constant avec les responsables politiques et avec une équipe de maîtrise d ' œuvre urbaine forcément complexe qu ' il doit réunir , animer et coordonner (architectes , ingénieurs de circulation , paysagistes , économistes…) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ pouvoir, perdre, travailler, confiance, responsable, forcément, devoir ]
marqueurs_niveau2 :: 5 :: [ perdre, travailler, confiance, responsable, forcément ]
alethique :: 3 :: [ pouvoir, forcément, devoir ]
deontique :: 3 :: [ pouvoir, responsable, devoir ]
epistemique :: 3 :: [ pouvoir, confiance, devoir ]
appreciatif :: 2 :: [ perdre, travailler ]
marqueurs_niveau3 :: 2 :: [ pouvoir, devoir ]
alethique_niveau3 :: 2 :: [ pouvoir, devoir ]
appreciatif_niveau2 :: 2 :: [ perdre, travailler ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, devoir ]
alethique_niveau2 :: 1 :: [ forcément ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau2 :: 1 :: [ confiance ]
Total feature counts: 36


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ pouvoir, perdre, travailler, confiance, responsable, forcément, devoir ]
marqueurs_niveau2 :: 5 :: [ perdre, travailler, confiance, responsable, forcément ]
alethique :: 3 :: [ pouvoir, forcément, devoir ]
deontique :: 3 :: [ pouvoir, responsable, devoir ]
epistemique :: 3 :: [ pouvoir, confiance, devoir ]
appreciatif :: 2 :: [ perdre, travailler ]
marqueurs_niveau3 :: 2 :: [ pouvoir, devoir ]
alethique_niveau3 :: 2 :: [ pouvoir, devoir ]
appreciatif_niveau2 :: 2 :: [ perdre, travailler ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, devoir ]
alethique_niveau2 :: 1 :: [ forcément ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau2 :: 1 :: [ confiance ]
Total feature counts: 36

