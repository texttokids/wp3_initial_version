
********** Entity Names Features **********
*******************************************

1. Sentence: Transformer son logement devrait être un droit Jouir d ' un logement c ' est le transformer , ce qui suppose de concevoir des lieux offrant un inachèvement 7 , laissant possible un parachèvement par un « acte de l ' habitant » .
Entity Name (Type) :: Jouir (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Transformer son logement devrait être un droit Jouir d ' un logement c ' est le transformer , ce qui suppose de concevoir des lieux offrant un inachèvement 7 , laissant possible un parachèvement par un « acte de l ' habitant » .
Tokens having emotion: [ Jouir ]
Lemmas having emotion: [ jouir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Transformer son logement devrait être un droit Jouir d ' un logement c ' est le transformer , ce qui suppose de concevoir des lieux offrant un inachèvement 7 , laissant possible un parachèvement par un « acte de l ' habitant » .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ devoir, droit, offrir, laisser, possible ]
deontique :: 3 :: [ devoir, droit, laisser ]
marqueurs_niveau3 :: 3 :: [ devoir, droit, possible ]
alethique :: 2 :: [ devoir, possible ]
marqueurs_niveau2 :: 2 :: [ offrir, laisser ]
alethique_niveau3 :: 2 :: [ devoir, possible ]
deontique_niveau3 :: 2 :: [ devoir, droit ]
appreciatif :: 1 :: [ offrir ]
epistemique :: 1 :: [ devoir ]
appreciatif_niveau2 :: 1 :: [ offrir ]
deontique_niveau2 :: 1 :: [ laisser ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Transformer son logement devrait être un droit Jouir d ' un logement c ' est le transformer , ce qui suppose de concevoir des lieux offrant un inachèvement 7 , laissant possible un parachèvement par un « acte de l ' habitant » .
No features found.

