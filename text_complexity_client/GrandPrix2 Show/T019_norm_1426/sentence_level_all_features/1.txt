
********** Entity Names Features **********
*******************************************

1. Sentence: Dans le texte le plus cité des manuels scolaires de la IIIe République ,  l ' extraordinaire panoramique du Tableau de la France de Michelet (1833) ,  l ' image est fixée d ' un territoire , « vaste et puissant organisme » qui , à travers sa diversité , exprime une sorte d ' unité essentielle se confondant avec l ' unité de la République .
Entity Name (Type) :: République (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Dans le texte le plus cité des manuels scolaires de la IIIe République ,  l ' extraordinaire panoramique du Tableau de la France de Michelet (1833) ,  l ' image est fixée d ' un territoire , « vaste et puissant organisme » qui , à travers sa diversité , exprime une sorte d ' unité essentielle se confondant avec l ' unité de la République .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Dans le texte le plus cité des manuels scolaires de la IIIe République ,  l ' extraordinaire panoramique du Tableau de la France de Michelet (1833) ,  l ' image est fixée d ' un territoire , « vaste et puissant organisme » qui , à travers sa diversité , exprime une sorte d ' unité essentielle se confondant avec l ' unité de la République .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ extraordinaire, puissant ]
appreciatif :: 1 :: [ extraordinaire ]
epistemique :: 1 :: [ extraordinaire ]
pas_d_indication :: 1 :: [ puissant ]
marqueurs_niveau1 :: 1 :: [ extraordinaire ]
marqueurs_niveau0 :: 1 :: [ puissant ]
appreciatif_niveau1 :: 1 :: [ extraordinaire ]
epistemique_niveau1 :: 1 :: [ extraordinaire ]
pas_d_indication_niveau0 :: 1 :: [ puissant ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans le texte le plus cité des manuels scolaires de la IIIe République ,  l ' extraordinaire panoramique du Tableau de la France de Michelet (1833) ,  l ' image est fixée d ' un territoire , « vaste et puissant organisme » qui , à travers sa diversité , exprime une sorte d ' unité essentielle se confondant avec l ' unité de la République .
No features found.

