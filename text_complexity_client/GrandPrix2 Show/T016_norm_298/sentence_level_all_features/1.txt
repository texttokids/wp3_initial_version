
********** Entity Names Features **********
*******************************************

1. Sentence: Mais aussi celui qu ' ils pratiquent euxmêmes , composant avec les éléments déjà là (que ce soit un relief , des matériaux , des formes bâties ou une «énergie» de projet déjà lancé) , en écho à Levi-Strauss : «Toujours s ' arranger avec les “moyens du bord” , c ' est-à-dire un ensemble à chaque instant fini d ' outils et de matériaux , hétéroclites au surplus , parce que [cet ensemble] est le résultat contingent de toutes les occasions qui se sont présentées de renouveler ou d ' enrichir le stock , en vertu du principe que “ça peut toujours servir”5 .
Entity Name (Type) :: Levi-Strauss (PER)


********** Emotions Features **********
***************************************

1. Sentence: Mais aussi celui qu ' ils pratiquent euxmêmes , composant avec les éléments déjà là (que ce soit un relief , des matériaux , des formes bâties ou une «énergie» de projet déjà lancé) , en écho à Levi-Strauss : «Toujours s ' arranger avec les “moyens du bord” , c ' est-à-dire un ensemble à chaque instant fini d ' outils et de matériaux , hétéroclites au surplus , parce que [cet ensemble] est le résultat contingent de toutes les occasions qui se sont présentées de renouveler ou d ' enrichir le stock , en vertu du principe que “ça peut toujours servir”5 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mais aussi celui qu ' ils pratiquent euxmêmes , composant avec les éléments déjà là (que ce soit un relief , des matériaux , des formes bâties ou une «énergie» de projet déjà lancé) , en écho à Levi-Strauss : «Toujours s ' arranger avec les “moyens du bord” , c ' est-à-dire un ensemble à chaque instant fini d ' outils et de matériaux , hétéroclites au surplus , parce que [cet ensemble] est le résultat contingent de toutes les occasions qui se sont présentées de renouveler ou d ' enrichir le stock , en vertu du principe que “ça peut toujours servir”5 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ toujours, enrichir, pouvoir, toujours ]
alethique :: 3 :: [ toujours, pouvoir, toujours ]
marqueurs_niveau3 :: 3 :: [ toujours, pouvoir, toujours ]
alethique_niveau3 :: 3 :: [ toujours, pouvoir, toujours ]
appreciatif :: 1 :: [ enrichir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ enrichir ]
appreciatif_niveau2 :: 1 :: [ enrichir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais aussi celui qu ' ils pratiquent euxmêmes , composant avec les éléments déjà là (que ce soit un relief , des matériaux , des formes bâties ou une «énergie» de projet déjà lancé) , en écho à Levi-Strauss : «Toujours s ' arranger avec les “moyens du bord” , c ' est-à-dire un ensemble à chaque instant fini d ' outils et de matériaux , hétéroclites au surplus , parce que [cet ensemble] est le résultat contingent de toutes les occasions qui se sont présentées de renouveler ou d ' enrichir le stock , en vertu du principe que “ça peut toujours servir”5 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['Toujours', 'chaque instant', 'toujours']
adverbiaux_purement_iteratifs::1::['chaque instant']
adverbiaux_duratifs_iteratifs::2::['Toujours', 'toujours']
adverbiaux_iterateur_calendaire::1::['chaque instant']

