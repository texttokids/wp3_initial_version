
********** Entity Names Features **********
*******************************************

1. Sentence: Dès 2000 , il a été décidé de substituer au parc unitaire , initialement projeté pour Lyon Confluence , un «système de parcs» composé d ' un cordon longeant la Saône et de ramifications innervant l ' intérieur du site .
Entity Name (Type) :: Saône (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Dès 2000 , il a été décidé de substituer au parc unitaire , initialement projeté pour Lyon Confluence , un «système de parcs» composé d ' un cordon longeant la Saône et de ramifications innervant l ' intérieur du site .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Dès 2000 , il a été décidé de substituer au parc unitaire , initialement projeté pour Lyon Confluence , un «système de parcs» composé d ' un cordon longeant la Saône et de ramifications innervant l ' intérieur du site .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ décider ]
boulique :: 1 :: [ décider ]
epistemique :: 1 :: [ décider ]
marqueurs_niveau3 :: 1 :: [ décider ]
boulique_niveau3 :: 1 :: [ décider ]
epistemique_niveau3 :: 1 :: [ décider ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dès 2000 , il a été décidé de substituer au parc unitaire , initialement projeté pour Lyon Confluence , un «système de parcs» composé d ' un cordon longeant la Saône et de ramifications innervant l ' intérieur du site .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['Dès 2000']
adverbiaux_localisation_temporelle::1::['Dès 2000']
adverbiaux_pointage_absolu::1::['Dès 2000']
adverbiaux_pointage_non_absolu::1::['Dès 2000']
adverbiaux_loc_temp_focalisation_id::1::['Dès 2000']
adverbiaux_loc_temp_regionalisation_non_id::1::['Dès 2000']
adverbiaux_loc_temp_pointage_non_absolu::1::['Dès 2000']
adverbiaux_dur_iter_absolu::1::['Dès 2000']

