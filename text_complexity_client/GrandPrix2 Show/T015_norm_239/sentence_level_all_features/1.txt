
********** Entity Names Features **********
*******************************************

1. Sentence: J ' aime l ' idée de ces transformations tout comme je redoute de contribuer à un nouvel académisme de la complexité .
Entity Name (Type) :: J (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' aime l ' idée de ces transformations tout comme je redoute de contribuer à un nouvel académisme de la complexité .
Tokens having emotion: [ aime, redoute ]
Lemmas having emotion: [ aimer, redouter ]
Categories of the emotion lemmas: [ amour, peur ]


********** Modality Features **********
***************************************

1. Sentence: J ' aime l ' idée de ces transformations tout comme je redoute de contribuer à un nouvel académisme de la complexité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ aimer, redouter ]
appreciatif :: 2 :: [ aimer, redouter ]
marqueurs_niveau3 :: 2 :: [ aimer, redouter ]
appreciatif_niveau3 :: 2 :: [ aimer, redouter ]
boulique :: 1 :: [ redouter ]
epistemique :: 1 :: [ redouter ]
boulique_niveau3 :: 1 :: [ redouter ]
epistemique_niveau3 :: 1 :: [ redouter ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' aime l ' idée de ces transformations tout comme je redoute de contribuer à un nouvel académisme de la complexité .
No features found.

