
********** Entity Names Features **********
*******************************************

1. Sentence: Élisabeth Georges a apprécié le souci de « gérer la boîte rationnellement , contrairement à toutes ces agences d ' architecture où il était bien vu de ne pas compter ses heures » .
Entity Name (Type) :: Élisabeth Georges (PER)


********** Emotions Features **********
***************************************

1. Sentence: Élisabeth Georges a apprécié le souci de « gérer la boîte rationnellement , contrairement à toutes ces agences d ' architecture où il était bien vu de ne pas compter ses heures » .
Tokens having emotion: [ souci ]
Lemmas having emotion: [ souci ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Élisabeth Georges a apprécié le souci de « gérer la boîte rationnellement , contrairement à toutes ces agences d ' architecture où il était bien vu de ne pas compter ses heures » .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ apprécier, souci, bien ]
appreciatif :: 3 :: [ apprécier, souci, bien ]
marqueurs_niveau3 :: 2 :: [ apprécier, bien ]
appreciatif_niveau3 :: 2 :: [ apprécier, bien ]
axiologique :: 1 :: [ bien ]
marqueurs_niveau2 :: 1 :: [ souci ]
appreciatif_niveau2 :: 1 :: [ souci ]
axiologique_niveau3 :: 1 :: [ bien ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Élisabeth Georges a apprécié le souci de « gérer la boîte rationnellement , contrairement à toutes ces agences d ' architecture où il était bien vu de ne pas compter ses heures » .
No features found.

