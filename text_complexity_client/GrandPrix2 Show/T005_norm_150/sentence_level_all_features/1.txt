
********** Entity Names Features **********
*******************************************

1. Sentence: Considérée comme une fonctionnaire atypique , voire une non-fonctionnaire , je pense que , pour bien des raisons , il est crucial , dans la France contemporaine , de revendiquer cette appartenance .
Entity Name (Type) :: la France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Considérée comme une fonctionnaire atypique , voire une non-fonctionnaire , je pense que , pour bien des raisons , il est crucial , dans la France contemporaine , de revendiquer cette appartenance .
Tokens having emotion: [ Considérée ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Considérée comme une fonctionnaire atypique , voire une non-fonctionnaire , je pense que , pour bien des raisons , il est crucial , dans la France contemporaine , de revendiquer cette appartenance .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ considérer, penser, bien, revendiquer ]
marqueurs_niveau3 :: 3 :: [ considérer, penser, bien ]
epistemique :: 2 :: [ considérer, penser ]
epistemique_niveau3 :: 2 :: [ considérer, penser ]
appreciatif :: 1 :: [ bien ]
axiologique :: 1 :: [ bien ]
boulique :: 1 :: [ revendiquer ]
deontique :: 1 :: [ revendiquer ]
marqueurs_niveau1 :: 1 :: [ revendiquer ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
boulique_niveau1 :: 1 :: [ revendiquer ]
deontique_niveau1 :: 1 :: [ revendiquer ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Considérée comme une fonctionnaire atypique , voire une non-fonctionnaire , je pense que , pour bien des raisons , il est crucial , dans la France contemporaine , de revendiquer cette appartenance .
No features found.

