
********** Entity Names Features **********
*******************************************

1. Sentence: On l ' imagine dormante , attentive à se dessiner , et voici que ses parcs , ses jardins , ses verrières , tout ce vocabulaire diffus et récurrent qui a fait d ' elle un paysage n ' est jamais que l ' effet du travail qu ' elle s ' était imposé (à travers les voyages et les cartes , en s ' inventant toute une géographie d ' à-pics et de vallées) pour se penser comme un réseau : tissu ouvert , instable , ville sans fin pour laquelle les Lumières ont inventé la mer (Corbin) , sont allées chercher Palladio et le premier discontinu d ' objets .
Entity Name (Type) :: Palladio (PER)


********** Emotions Features **********
***************************************

1. Sentence: On l ' imagine dormante , attentive à se dessiner , et voici que ses parcs , ses jardins , ses verrières , tout ce vocabulaire diffus et récurrent qui a fait d ' elle un paysage n ' est jamais que l ' effet du travail qu ' elle s ' était imposé (à travers les voyages et les cartes , en s ' inventant toute une géographie d ' à-pics et de vallées) pour se penser comme un réseau : tissu ouvert , instable , ville sans fin pour laquelle les Lumières ont inventé la mer (Corbin) , sont allées chercher Palladio et le premier discontinu d ' objets .
No features found.


********** Modality Features **********
***************************************

1. Sentence: On l ' imagine dormante , attentive à se dessiner , et voici que ses parcs , ses jardins , ses verrières , tout ce vocabulaire diffus et récurrent qui a fait d ' elle un paysage n ' est jamais que l ' effet du travail qu ' elle s ' était imposé (à travers les voyages et les cartes , en s ' inventant toute une géographie d ' à-pics et de vallées) pour se penser comme un réseau : tissu ouvert , instable , ville sans fin pour laquelle les Lumières ont inventé la mer (Corbin) , sont allées chercher Palladio et le premier discontinu d ' objets .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ imaginer, jamais, travail, imposer, inventer, penser, inventer, aller, chercher ]
marqueurs_niveau3 :: 5 :: [ imaginer, jamais, travail, penser, chercher ]
epistemique :: 4 :: [ imaginer, inventer, penser, inventer ]
marqueurs_niveau2 :: 3 :: [ imposer, inventer, inventer ]
alethique :: 2 :: [ jamais, aller ]
epistemique_niveau3 :: 2 :: [ imaginer, penser ]
epistemique_niveau2 :: 2 :: [ inventer, inventer ]
appreciatif :: 1 :: [ travail ]
boulique :: 1 :: [ chercher ]
deontique :: 1 :: [ imposer ]
marqueurs_niveau1 :: 1 :: [ aller ]
alethique_niveau3 :: 1 :: [ jamais ]
alethique_niveau1 :: 1 :: [ aller ]
appreciatif_niveau3 :: 1 :: [ travail ]
boulique_niveau3 :: 1 :: [ chercher ]
deontique_niveau2 :: 1 :: [ imposer ]
Total feature counts: 36


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: On l ' imagine dormante , attentive à se dessiner , et voici que ses parcs , ses jardins , ses verrières , tout ce vocabulaire diffus et récurrent qui a fait d ' elle un paysage n ' est jamais que l ' effet du travail qu ' elle s ' était imposé (à travers les voyages et les cartes , en s ' inventant toute une géographie d ' à-pics et de vallées) pour se penser comme un réseau : tissu ouvert , instable , ville sans fin pour laquelle les Lumières ont inventé la mer (Corbin) , sont allées chercher Palladio et le premier discontinu d ' objets .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jamais']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_iterateur_frequentiel::1::['jamais']

