
********** Entity Names Features **********
*******************************************

1. Sentence: Vous avez compris que je suis avec d ' autres un partisan inconditionnel du retour à la géographie : cette position , je l ' ai affirmée aux côtés de Marc Mimram et d ' Alexandre Chemetoff , à Anvers , où la question de la modification de la ville après le déplacement du port était posée lors d ' un concours international en 1990 .
Entity Name (Type) :: Anvers (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Vous avez compris que je suis avec d ' autres un partisan inconditionnel du retour à la géographie : cette position , je l ' ai affirmée aux côtés de Marc Mimram et d ' Alexandre Chemetoff , à Anvers , où la question de la modification de la ville après le déplacement du port était posée lors d ' un concours international en 1990 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Vous avez compris que je suis avec d ' autres un partisan inconditionnel du retour à la géographie : cette position , je l ' ai affirmée aux côtés de Marc Mimram et d ' Alexandre Chemetoff , à Anvers , où la question de la modification de la ville après le déplacement du port était posée lors d ' un concours international en 1990 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ affirmer ]
pas_d_indication :: 1 :: [ affirmer ]
marqueurs_niveau0 :: 1 :: [ affirmer ]
pas_d_indication_niveau0 :: 1 :: [ affirmer ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Vous avez compris que je suis avec d ' autres un partisan inconditionnel du retour à la géographie : cette position , je l ' ai affirmée aux côtés de Marc Mimram et d ' Alexandre Chemetoff , à Anvers , où la question de la modification de la ville après le déplacement du port était posée lors d ' un concours international en 1990 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en 1990']
adverbiaux_localisation_temporelle::1::['en 1990']
adverbiaux_pointage_absolu::1::['en 1990']
adverbiaux_pointage_non_absolu::1::['en 1990']
adverbiaux_loc_temp_focalisation_id::1::['en 1990']
adverbiaux_loc_temp_regionalisation_id::1::['en 1990']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1990']
adverbiaux_dur_iter_absolu::1::['en 1990']

