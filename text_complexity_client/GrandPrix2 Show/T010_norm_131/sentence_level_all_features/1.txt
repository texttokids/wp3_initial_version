
********** Entity Names Features **********
*******************************************

1. Sentence: De nouveaux processus bouleversent radicalement la façon de faire la ville : le désengagement de l ' État , la déconnexion apparente entre projets de réseaux en réseaux , en ressources et en déplacements pose la question d ' une plus grande densité des formes urbaines de l ' habitat .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: De nouveaux processus bouleversent radicalement la façon de faire la ville : le désengagement de l ' État , la déconnexion apparente entre projets de réseaux en réseaux , en ressources et en déplacements pose la question d ' une plus grande densité des formes urbaines de l ' habitat .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: De nouveaux processus bouleversent radicalement la façon de faire la ville : le désengagement de l ' État , la déconnexion apparente entre projets de réseaux en réseaux , en ressources et en déplacements pose la question d ' une plus grande densité des formes urbaines de l ' habitat .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ grand ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: De nouveaux processus bouleversent radicalement la façon de faire la ville : le désengagement de l ' État , la déconnexion apparente entre projets de réseaux en réseaux , en ressources et en déplacements pose la question d ' une plus grande densité des formes urbaines de l ' habitat .
No features found.

