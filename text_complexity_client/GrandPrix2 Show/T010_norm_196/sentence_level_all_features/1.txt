
********** Entity Names Features **********
*******************************************

1. Sentence: La ville au péril de la montée des eaux David MANGIN Sihanoukville Projet pour le développement de Sihanoukville , 2006 Carte des itinéraires touristiques aériens au Cambodge aujourd ' hui .
Entity Name (Type) :: Carte des itinéraires touristiques aériens au Cambodge (MISC)


********** Emotions Features **********
***************************************

1. Sentence: La ville au péril de la montée des eaux David MANGIN Sihanoukville Projet pour le développement de Sihanoukville , 2006 Carte des itinéraires touristiques aériens au Cambodge aujourd ' hui .
Tokens having emotion: [ péril ]
Lemmas having emotion: [ péril ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: La ville au péril de la montée des eaux David MANGIN Sihanoukville Projet pour le développement de Sihanoukville , 2006 Carte des itinéraires touristiques aériens au Cambodge aujourd ' hui .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ péril ]
appreciatif :: 1 :: [ péril ]
marqueurs_niveau2 :: 1 :: [ péril ]
appreciatif_niveau2 :: 1 :: [ péril ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La ville au péril de la montée des eaux David MANGIN Sihanoukville Projet pour le développement de Sihanoukville , 2006 Carte des itinéraires touristiques aériens au Cambodge aujourd ' hui .
No features found.

