
********** Entity Names Features **********
*******************************************

1. Sentence: Le projet repose sur une étroite intrication des vides et des pleins , au moyen de dispositifs qui rendent les surfaces bâties plus poreuses et ainsi plus hospitalières , en multipliant le plus possible les interfaces , les seuils doit-on dire pour ne pas oublier que c ' est à ce niveau , depuis les Grecs , que se pratique , se ressent et se pense l ' hospitalité .
Entity Name (Type) :: Grecs (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Le projet repose sur une étroite intrication des vides et des pleins , au moyen de dispositifs qui rendent les surfaces bâties plus poreuses et ainsi plus hospitalières , en multipliant le plus possible les interfaces , les seuils doit-on dire pour ne pas oublier que c ' est à ce niveau , depuis les Grecs , que se pratique , se ressent et se pense l ' hospitalité .
Tokens having emotion: [ repose, ressent ]
Lemmas having emotion: [ reposer, ressentir ]
Categories of the emotion lemmas: [ apaisement, non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Le projet repose sur une étroite intrication des vides et des pleins , au moyen de dispositifs qui rendent les surfaces bâties plus poreuses et ainsi plus hospitalières , en multipliant le plus possible les interfaces , les seuils doit-on dire pour ne pas oublier que c ' est à ce niveau , depuis les Grecs , que se pratique , se ressent et se pense l ' hospitalité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ possible, devoir, dire, oublier, penser ]
marqueurs_niveau3 :: 4 :: [ possible, devoir, dire, penser ]
alethique :: 2 :: [ possible, devoir ]
epistemique :: 2 :: [ devoir, penser ]
alethique_niveau3 :: 2 :: [ possible, devoir ]
epistemique_niveau3 :: 2 :: [ devoir, penser ]
boulique :: 1 :: [ dire ]
deontique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ oublier ]
marqueurs_niveau0 :: 1 :: [ oublier ]
boulique_niveau3 :: 1 :: [ dire ]
deontique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ oublier ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le projet repose sur une étroite intrication des vides et des pleins , au moyen de dispositifs qui rendent les surfaces bâties plus poreuses et ainsi plus hospitalières , en multipliant le plus possible les interfaces , les seuils doit-on dire pour ne pas oublier que c ' est à ce niveau , depuis les Grecs , que se pratique , se ressent et se pense l ' hospitalité .
No features found.

