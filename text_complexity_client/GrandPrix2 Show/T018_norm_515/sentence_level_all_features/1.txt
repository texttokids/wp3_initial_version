
********** Entity Names Features **********
*******************************************

1. Sentence: — le vent dont on se protège ou que l ' on utilise pour faire trembler ;
No features found.


********** Emotions Features **********
***************************************

1. Sentence: — le vent dont on se protège ou que l ' on utilise pour faire trembler ;
Tokens having emotion: [ trembler ]
Lemmas having emotion: [ trembler ]
Categories of the emotion lemmas: [ comportement ]


********** Modality Features **********
***************************************

1. Sentence: — le vent dont on se protège ou que l ' on utilise pour faire trembler ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ vent, protéger ]
alethique :: 1 :: [ vent ]
appreciatif :: 1 :: [ vent ]
pas_d_indication :: 1 :: [ protéger ]
marqueurs_niveau2 :: 1 :: [ vent ]
marqueurs_niveau0 :: 1 :: [ protéger ]
alethique_niveau2 :: 1 :: [ vent ]
appreciatif_niveau2 :: 1 :: [ vent ]
pas_d_indication_niveau0 :: 1 :: [ protéger ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: — le vent dont on se protège ou que l ' on utilise pour faire trembler ;
No features found.

