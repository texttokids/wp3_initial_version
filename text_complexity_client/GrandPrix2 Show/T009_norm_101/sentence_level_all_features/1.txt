
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est ce qu ' il faut dire dans les écoles d ' architecture et aux responsables (usagers , profession , État) , dire aussi que c ' est autour du foncier (délaissés des infrastructures , vacuité des grands ensembles , surconsommation du territoire voué à l ' activité économique) que résident les recettes qui nous permettront de faire face à ces dépenses .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est ce qu ' il faut dire dans les écoles d ' architecture et aux responsables (usagers , profession , État) , dire aussi que c ' est autour du foncier (délaissés des infrastructures , vacuité des grands ensembles , surconsommation du territoire voué à l ' activité économique) que résident les recettes qui nous permettront de faire face à ces dépenses .
Tokens having emotion: [ État, délaissés ]
Lemmas having emotion: [ état, délaisser ]
Categories of the emotion lemmas: [ non_specifiee, tristesse ]


********** Modality Features **********
***************************************

1. Sentence: C ' est ce qu ' il faut dire dans les écoles d ' architecture et aux responsables (usagers , profession , État) , dire aussi que c ' est autour du foncier (délaissés des infrastructures , vacuité des grands ensembles , surconsommation du territoire voué à l ' activité économique) que résident les recettes qui nous permettront de faire face à ces dépenses .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 8 :: [ falloir, dire, école, responsable, dire, grand, surconsommation, permettre ]
marqueurs_niveau3 :: 5 :: [ falloir, dire, dire, grand, permettre ]
deontique :: 3 :: [ falloir, responsable, permettre ]
boulique :: 2 :: [ dire, dire ]
boulique_niveau3 :: 2 :: [ dire, dire ]
deontique_niveau3 :: 2 :: [ falloir, permettre ]
alethique :: 1 :: [ école ]
epistemique :: 1 :: [ grand ]
pas_d_indication :: 1 :: [ surconsommation ]
marqueurs_niveau2 :: 1 :: [ responsable ]
marqueurs_niveau1 :: 1 :: [ école ]
marqueurs_niveau0 :: 1 :: [ surconsommation ]
alethique_niveau1 :: 1 :: [ école ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau3 :: 1 :: [ grand ]
pas_d_indication_niveau0 :: 1 :: [ surconsommation ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est ce qu ' il faut dire dans les écoles d ' architecture et aux responsables (usagers , profession , État) , dire aussi que c ' est autour du foncier (délaissés des infrastructures , vacuité des grands ensembles , surconsommation du territoire voué à l ' activité économique) que résident les recettes qui nous permettront de faire face à ces dépenses .
No features found.

