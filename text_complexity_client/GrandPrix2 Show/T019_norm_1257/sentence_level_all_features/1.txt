
********** Entity Names Features **********
*******************************************

1. Sentence: Les robots sont en train d ' étendre leur empire parce que certains gestes d ' assemblage qui étaient encore l ' apanage des humains — tout simplement parce que le trio cerveau/œil/main reste plus performant chez l ' homme que chez les robots — sont eux aussi en train d ' être automatisés .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les robots sont en train d ' étendre leur empire parce que certains gestes d ' assemblage qui étaient encore l ' apanage des humains — tout simplement parce que le trio cerveau/œil/main reste plus performant chez l ' homme que chez les robots — sont eux aussi en train d ' être automatisés .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les robots sont en train d ' étendre leur empire parce que certains gestes d ' assemblage qui étaient encore l ' apanage des humains — tout simplement parce que le trio cerveau/œil/main reste plus performant chez l ' homme que chez les robots — sont eux aussi en train d ' être automatisés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ certain, lui ]
epistemique :: 2 :: [ certain, lui ]
marqueurs_niveau3 :: 2 :: [ certain, lui ]
epistemique_niveau3 :: 2 :: [ certain, lui ]
alethique :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les robots sont en train d ' étendre leur empire parce que certains gestes d ' assemblage qui étaient encore l ' apanage des humains — tout simplement parce que le trio cerveau/œil/main reste plus performant chez l ' homme que chez les robots — sont eux aussi en train d ' être automatisés .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

