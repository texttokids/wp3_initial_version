
********** Entity Names Features **********
*******************************************

1. Sentence: Si , aujourd ' hui , une bonne part se fait de manière erratique , demain , le lotissement écologique va «prendre» et sera une règle de l ' urbanisme pavillonnaire .
Entity Name (Type) :: Si (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Si , aujourd ' hui , une bonne part se fait de manière erratique , demain , le lotissement écologique va «prendre» et sera une règle de l ' urbanisme pavillonnaire .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Si , aujourd ' hui , une bonne part se fait de manière erratique , demain , le lotissement écologique va «prendre» et sera une règle de l ' urbanisme pavillonnaire .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ bon, aller ]
alethique :: 1 :: [ aller ]
appreciatif :: 1 :: [ bon ]
axiologique :: 1 :: [ bon ]
marqueurs_niveau3 :: 1 :: [ bon ]
marqueurs_niveau1 :: 1 :: [ aller ]
alethique_niveau1 :: 1 :: [ aller ]
appreciatif_niveau3 :: 1 :: [ bon ]
axiologique_niveau3 :: 1 :: [ bon ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Si , aujourd ' hui , une bonne part se fait de manière erratique , demain , le lotissement écologique va «prendre» et sera une règle de l ' urbanisme pavillonnaire .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['demain']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']

