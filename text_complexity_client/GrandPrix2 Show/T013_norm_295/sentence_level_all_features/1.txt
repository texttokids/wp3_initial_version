
********** Entity Names Features **********
*******************************************

1. Sentence: Celle-ci est synonyme de diversité et de partage , efficace quand les uns et les autres entretiennent leur goût de la liberté , développent une capacité critique et évoluent dans le respect du travail de chacun .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Celle-ci est synonyme de diversité et de partage , efficace quand les uns et les autres entretiennent leur goût de la liberté , développent une capacité critique et évoluent dans le respect du travail de chacun .
Tokens having emotion: [ critique, respect ]
Lemmas having emotion: [ critique, respect ]
Categories of the emotion lemmas: [ mepris, admiration ]


********** Modality Features **********
***************************************

1. Sentence: Celle-ci est synonyme de diversité et de partage , efficace quand les uns et les autres entretiennent leur goût de la liberté , développent une capacité critique et évoluent dans le respect du travail de chacun .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ liberté, respect, travail ]
appreciatif :: 3 :: [ liberté, respect, travail ]
axiologique :: 1 :: [ respect ]
marqueurs_niveau3 :: 1 :: [ travail ]
marqueurs_niveau2 :: 1 :: [ respect ]
marqueurs_niveau1 :: 1 :: [ liberté ]
appreciatif_niveau3 :: 1 :: [ travail ]
appreciatif_niveau2 :: 1 :: [ respect ]
appreciatif_niveau1 :: 1 :: [ liberté ]
axiologique_niveau2 :: 1 :: [ respect ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Celle-ci est synonyme de diversité et de partage , efficace quand les uns et les autres entretiennent leur goût de la liberté , développent une capacité critique et évoluent dans le respect du travail de chacun .
No features found.

