
********** Entity Names Features **********
*******************************************

1. Sentence: Mon diplôme de fin d ' études a porté sur le processus d ' urbanisation de Belleville et de Ménilmontant à partir du xviiie siècle .
Entity Name (Type) :: Ménilmontant (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Mon diplôme de fin d ' études a porté sur le processus d ' urbanisation de Belleville et de Ménilmontant à partir du xviiie siècle .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mon diplôme de fin d ' études a porté sur le processus d ' urbanisation de Belleville et de Ménilmontant à partir du xviiie siècle .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ partir ]
alethique :: 1 :: [ partir ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau1 :: 1 :: [ partir ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mon diplôme de fin d ' études a porté sur le processus d ' urbanisation de Belleville et de Ménilmontant à partir du xviiie siècle .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à partir du xviiie siècle']
adverbiaux_localisation_temporelle::1::['à partir du xviiie siècle']
adverbiaux_pointage_absolu::1::['à partir du xviiie siècle']
adverbiaux_loc_temp_focalisation_id::1::['à partir du xviiie siècle']
adverbiaux_loc_temp_regionalisation_non_id::1::['à partir du xviiie siècle']
adverbiaux_loc_temp_pointage_absolu::1::['à partir du xviiie siècle']

