
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai toujours cru qu ' elles devaient se regrouper , et sortir de l ' opposition stérile avec l ' université .
Entity Name (Type) :: J (LOC)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai toujours cru qu ' elles devaient se regrouper , et sortir de l ' opposition stérile avec l ' université .
No features found.


********** Modality Features **********
***************************************

1. Sentence: J ' ai toujours cru qu ' elles devaient se regrouper , et sortir de l ' opposition stérile avec l ' université .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ toujours, devoir, stérile ]
alethique :: 3 :: [ toujours, devoir, stérile ]
marqueurs_niveau3 :: 2 :: [ toujours, devoir ]
alethique_niveau3 :: 2 :: [ toujours, devoir ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ stérile ]
alethique_niveau2 :: 1 :: [ stérile ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai toujours cru qu ' elles devaient se regrouper , et sortir de l ' opposition stérile avec l ' université .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

