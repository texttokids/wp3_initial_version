
********** Entity Names Features **********
*******************************************

1. Sentence: Entre les rivières d ' amont et la Loire , la Maine a déterminé la fondation d ' Angers , de son château et du centre historique de l ' agglomération .
Entity Name (Type) :: Angers (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Entre les rivières d ' amont et la Loire , la Maine a déterminé la fondation d ' Angers , de son château et du centre historique de l ' agglomération .
Tokens having emotion: [ déterminé ]
Lemmas having emotion: [ déterminer ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Entre les rivières d ' amont et la Loire , la Maine a déterminé la fondation d ' Angers , de son château et du centre historique de l ' agglomération .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ château ]
appreciatif :: 1 :: [ château ]
epistemique :: 1 :: [ château ]
marqueurs_niveau3 :: 1 :: [ château ]
appreciatif_niveau3 :: 1 :: [ château ]
epistemique_niveau3 :: 1 :: [ château ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Entre les rivières d ' amont et la Loire , la Maine a déterminé la fondation d ' Angers , de son château et du centre historique de l ' agglomération .
No features found.

