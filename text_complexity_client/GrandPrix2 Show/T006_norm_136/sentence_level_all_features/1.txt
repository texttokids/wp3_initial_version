
********** Entity Names Features **********
*******************************************

1. Sentence: À Dunkerque , au Havre , à Saint-Nazaire dans des projets liés à la reconquête des zones portuaires mais aussi à Aix-en-Provence , Strasbourg ou encore Mulhouse , ils sont conçus comme des assemblages où il faut croiser les bonnes hypothèses aux bons endroits dans des équilibres souvent précaires .
Entity Name (Type) :: Mulhouse (LOC)


********** Emotions Features **********
***************************************

1. Sentence: À Dunkerque , au Havre , à Saint-Nazaire dans des projets liés à la reconquête des zones portuaires mais aussi à Aix-en-Provence , Strasbourg ou encore Mulhouse , ils sont conçus comme des assemblages où il faut croiser les bonnes hypothèses aux bons endroits dans des équilibres souvent précaires .
No features found.


********** Modality Features **********
***************************************

1. Sentence: À Dunkerque , au Havre , à Saint-Nazaire dans des projets liés à la reconquête des zones portuaires mais aussi à Aix-en-Provence , Strasbourg ou encore Mulhouse , ils sont conçus comme des assemblages où il faut croiser les bonnes hypothèses aux bons endroits dans des équilibres souvent précaires .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ falloir, bon, bon, souvent ]
marqueurs_niveau3 :: 4 :: [ falloir, bon, bon, souvent ]
appreciatif :: 2 :: [ bon, bon ]
axiologique :: 2 :: [ bon, bon ]
appreciatif_niveau3 :: 2 :: [ bon, bon ]
axiologique_niveau3 :: 2 :: [ bon, bon ]
alethique :: 1 :: [ souvent ]
deontique :: 1 :: [ falloir ]
alethique_niveau3 :: 1 :: [ souvent ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À Dunkerque , au Havre , à Saint-Nazaire dans des projets liés à la reconquête des zones portuaires mais aussi à Aix-en-Provence , Strasbourg ou encore Mulhouse , ils sont conçus comme des assemblages où il faut croiser les bonnes hypothèses aux bons endroits dans des équilibres souvent précaires .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['encore', 'souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_iterateur_frequentiel::1::['souvent']
adverbiaux_dur_iter_relatif::1::['encore']

