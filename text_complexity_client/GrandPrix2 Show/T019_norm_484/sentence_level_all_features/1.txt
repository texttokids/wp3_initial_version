
********** Entity Names Features **********
*******************************************

1. Sentence: Jean‑Louis Subileau  : Les contrats de développement territorial (CDT) , mis en place en 2011 , offrent de bons exemples de définition de stratégies territoriales déclinées en actions .
Entity Name (Type) :: CDT (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Jean‑Louis Subileau  : Les contrats de développement territorial (CDT) , mis en place en 2011 , offrent de bons exemples de définition de stratégies territoriales déclinées en actions .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Jean‑Louis Subileau  : Les contrats de développement territorial (CDT) , mis en place en 2011 , offrent de bons exemples de définition de stratégies territoriales déclinées en actions .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ offrir, bon ]
appreciatif :: 2 :: [ offrir, bon ]
axiologique :: 1 :: [ bon ]
marqueurs_niveau3 :: 1 :: [ bon ]
marqueurs_niveau2 :: 1 :: [ offrir ]
appreciatif_niveau3 :: 1 :: [ bon ]
appreciatif_niveau2 :: 1 :: [ offrir ]
axiologique_niveau3 :: 1 :: [ bon ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Jean‑Louis Subileau  : Les contrats de développement territorial (CDT) , mis en place en 2011 , offrent de bons exemples de définition de stratégies territoriales déclinées en actions .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en 2011']
adverbiaux_localisation_temporelle::1::['en 2011']
adverbiaux_pointage_absolu::1::['en 2011']
adverbiaux_pointage_non_absolu::1::['en 2011']
adverbiaux_loc_temp_focalisation_id::1::['en 2011']
adverbiaux_loc_temp_regionalisation_id::1::['en 2011']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 2011']
adverbiaux_dur_iter_absolu::1::['en 2011']

