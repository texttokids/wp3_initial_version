====== Sentence Level Features ======

1. Sentence: Je ne parle pas des effets formels qui relèvent de la citation ou du pastiche , mais des échanges et complémentarités capables d ' établir une forme de cohésion , bien préférable à la normalisation . Bref , dans une démarche qui joue avec le temps de développements progressifs à partir d ' orientations clairement définies , sans volonté de maîtrise absolue et de coordination excessive aboutissant à des ensembles urbains uniformes et figés , je défends le principe que les dispositions d ' urbanisme doivent ménager le champ d ' intervention de chaque maître d ' œuvre .
No features found.


====== Text Level Features ======

No features found.

