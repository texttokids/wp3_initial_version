
********** Entity Names Features **********
*******************************************

1. Sentence: Élargissant son champ d ' intervention , Patrick Bouchain s ' aventure jusqu ' à oser des théories audacieuses sur l ' aménagement du territoire en croisant le droit de l ' urbanisme avec le Code rural et forestier et parfois le Code de la propriété…
Entity Name (Type) :: Code de la propriété (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Élargissant son champ d ' intervention , Patrick Bouchain s ' aventure jusqu ' à oser des théories audacieuses sur l ' aménagement du territoire en croisant le droit de l ' urbanisme avec le Code rural et forestier et parfois le Code de la propriété…
Tokens having emotion: [ audacieuses ]
Lemmas having emotion: [ audacieux ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Élargissant son champ d ' intervention , Patrick Bouchain s ' aventure jusqu ' à oser des théories audacieuses sur l ' aménagement du territoire en croisant le droit de l ' urbanisme avec le Code rural et forestier et parfois le Code de la propriété…
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ aventure, oser, croire, droit, parfois ]
marqueurs_niveau3 :: 3 :: [ croire, droit, parfois ]
alethique :: 1 :: [ parfois ]
appreciatif :: 1 :: [ oser ]
axiologique :: 1 :: [ oser ]
deontique :: 1 :: [ droit ]
epistemique :: 1 :: [ croire ]
pas_d_indication :: 1 :: [ aventure ]
marqueurs_niveau2 :: 1 :: [ oser ]
marqueurs_niveau0 :: 1 :: [ aventure ]
alethique_niveau3 :: 1 :: [ parfois ]
appreciatif_niveau2 :: 1 :: [ oser ]
axiologique_niveau2 :: 1 :: [ oser ]
deontique_niveau3 :: 1 :: [ droit ]
epistemique_niveau3 :: 1 :: [ croire ]
pas_d_indication_niveau0 :: 1 :: [ aventure ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Élargissant son champ d ' intervention , Patrick Bouchain s ' aventure jusqu ' à oser des théories audacieuses sur l ' aménagement du territoire en croisant le droit de l ' urbanisme avec le Code rural et forestier et parfois le Code de la propriété…
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

