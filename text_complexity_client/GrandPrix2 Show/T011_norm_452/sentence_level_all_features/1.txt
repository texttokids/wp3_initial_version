
********** Entity Names Features **********
*******************************************

1. Sentence: Je n ' ai pas d ' expérience étrangère , si ce n ' est l ' année 1977 , largement consacrée , avec l ' équipe de l ' Apur , à la réflexion sur l ' aménagement du centre de Beyrouth (notre plan qui proscrivait toute modification de la ligne côtière ne devait pas hélas être suivi d ' effets) ,  l ' organisation d ' un colloque «Paris-New York survival strategies» en 1978 , et la participation à un jury pour l ' aménagement du quartier Pu-dong à Shanghaï .
Entity Name (Type) :: Shanghaï (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Je n ' ai pas d ' expérience étrangère , si ce n ' est l ' année 1977 , largement consacrée , avec l ' équipe de l ' Apur , à la réflexion sur l ' aménagement du centre de Beyrouth (notre plan qui proscrivait toute modification de la ligne côtière ne devait pas hélas être suivi d ' effets) ,  l ' organisation d ' un colloque «Paris-New York survival strategies» en 1978 , et la participation à un jury pour l ' aménagement du quartier Pu-dong à Shanghaï .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Je n ' ai pas d ' expérience étrangère , si ce n ' est l ' année 1977 , largement consacrée , avec l ' équipe de l ' Apur , à la réflexion sur l ' aménagement du centre de Beyrouth (notre plan qui proscrivait toute modification de la ligne côtière ne devait pas hélas être suivi d ' effets) ,  l ' organisation d ' un colloque «Paris-New York survival strategies» en 1978 , et la participation à un jury pour l ' aménagement du quartier Pu-dong à Shanghaï .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ devoir, hélas ]
marqueurs_niveau3 :: 2 :: [ devoir, hélas ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ hélas ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ hélas ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je n ' ai pas d ' expérience étrangère , si ce n ' est l ' année 1977 , largement consacrée , avec l ' équipe de l ' Apur , à la réflexion sur l ' aménagement du centre de Beyrouth (notre plan qui proscrivait toute modification de la ligne côtière ne devait pas hélas être suivi d ' effets) ,  l ' organisation d ' un colloque «Paris-New York survival strategies» en 1978 , et la participation à un jury pour l ' aménagement du quartier Pu-dong à Shanghaï .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['l ` année 1977', 'en 1978']
adverbiaux_localisation_temporelle::2::['l ` année 1977', 'en 1978']
adverbiaux_pointage_absolu::2::['l ` année 1977', 'en 1978']
adverbiaux_pointage_non_absolu::1::['en 1978']
adverbiaux_loc_temp_focalisation_id::2::['l ` année 1977', 'en 1978']
adverbiaux_loc_temp_regionalisation_id::2::['l ` année 1977', 'en 1978']
adverbiaux_loc_temp_pointage_absolu::1::['l ` année 1977']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1978']
adverbiaux_dur_iter_absolu::1::['en 1978']

