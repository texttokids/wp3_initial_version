====== Sentence Level Features ======

1. Sentence: la Poétique de la ville du regretté Pierre Sansot , Une Traversée de Paris d ' Éric Hazan , que je suis en train de lire , les textes de Jean-Luc Nancy sur Los Angeles (La Ville au loin) , ou l ' extraordinaire évocation de Brooklyn par James Agee (« Brooklyn existe ») , pour n ' en citer que quelques-uns .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ regretter, lire, extraordinaire, quelqu'un ]
appreciatif :: 2 :: [ regretter, extraordinaire ]
epistemique :: 2 :: [ regretter, extraordinaire ]
marqueurs_niveau3 :: 2 :: [ regretter, lire ]
alethique :: 1 :: [ quelqu'un ]
boulique :: 1 :: [ lire ]
marqueurs_niveau2 :: 1 :: [ quelqu'un ]
marqueurs_niveau1 :: 1 :: [ extraordinaire ]
alethique_niveau2 :: 1 :: [ quelqu'un ]
appreciatif_niveau3 :: 1 :: [ regretter ]
appreciatif_niveau1 :: 1 :: [ extraordinaire ]
boulique_niveau3 :: 1 :: [ lire ]
epistemique_niveau3 :: 1 :: [ regretter ]
epistemique_niveau1 :: 1 :: [ extraordinaire ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ regretter, lire, extraordinaire, quelqu'un ]
appreciatif :: 2 :: [ regretter, extraordinaire ]
epistemique :: 2 :: [ regretter, extraordinaire ]
marqueurs_niveau3 :: 2 :: [ regretter, lire ]
alethique :: 1 :: [ quelqu'un ]
boulique :: 1 :: [ lire ]
marqueurs_niveau2 :: 1 :: [ quelqu'un ]
marqueurs_niveau1 :: 1 :: [ extraordinaire ]
alethique_niveau2 :: 1 :: [ quelqu'un ]
appreciatif_niveau3 :: 1 :: [ regretter ]
appreciatif_niveau1 :: 1 :: [ extraordinaire ]
boulique_niveau3 :: 1 :: [ lire ]
epistemique_niveau3 :: 1 :: [ regretter ]
epistemique_niveau1 :: 1 :: [ extraordinaire ]
Total feature counts: 20

