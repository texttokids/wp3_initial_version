
********** Entity Names Features **********
*******************************************

1. Sentence: Pierre Veltz  : On a reproché à Blanc de ne pas dialoguer .
Entity Name (Type) :: Blanc (PER)


********** Emotions Features **********
***************************************

1. Sentence: Pierre Veltz  : On a reproché à Blanc de ne pas dialoguer .
Tokens having emotion: [ reproché ]
Lemmas having emotion: [ reprocher ]
Categories of the emotion lemmas: [ culpabilite ]


********** Modality Features **********
***************************************

1. Sentence: Pierre Veltz  : On a reproché à Blanc de ne pas dialoguer .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ reprocher ]
axiologique :: 1 :: [ reprocher ]
marqueurs_niveau2 :: 1 :: [ reprocher ]
axiologique_niveau2 :: 1 :: [ reprocher ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pierre Veltz  : On a reproché à Blanc de ne pas dialoguer .
No features found.

