
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai appris , dans mon pays d ' origine , à me méfier d ' un certain usage du folklore .
Entity Name (Type) :: J (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai appris , dans mon pays d ' origine , à me méfier d ' un certain usage du folklore .
Tokens having emotion: [ méfier ]
Lemmas having emotion: [ méfier ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai appris , dans mon pays d ' origine , à me méfier d ' un certain usage du folklore .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ certain ]
epistemique :: 1 :: [ certain ]
marqueurs_niveau3 :: 1 :: [ certain ]
epistemique_niveau3 :: 1 :: [ certain ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai appris , dans mon pays d ' origine , à me méfier d ' un certain usage du folklore .
No features found.

