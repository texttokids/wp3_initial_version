====== Sentence Level Features ======

1. Sentence: Servir l ' État , c ' est ainsi apporter de l ' intelligence , de l ' agilité , voire de la perturbation pour faire bouger les lignes quand c ' est nécessaire , pour initier des processus et les mener à bien dans un temps le plus court possible , en évitant les temps longs et inutiles de la bureaucratie , inhérents à toutes les grandes entreprises .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ nécessaire, bien, possible, long, inutile, grand ]
marqueurs_niveau3 :: 4 :: [ nécessaire, bien, possible, grand ]
alethique :: 2 :: [ nécessaire, possible ]
appreciatif :: 2 :: [ bien, inutile ]
epistemique :: 2 :: [ long, grand ]
marqueurs_niveau2 :: 2 :: [ long, inutile ]
alethique_niveau3 :: 2 :: [ nécessaire, possible ]
axiologique :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau2 :: 1 :: [ inutile ]
axiologique_niveau3 :: 1 :: [ bien ]
epistemique_niveau3 :: 1 :: [ grand ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 26


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ nécessaire, bien, possible, long, inutile, grand ]
marqueurs_niveau3 :: 4 :: [ nécessaire, bien, possible, grand ]
alethique :: 2 :: [ nécessaire, possible ]
appreciatif :: 2 :: [ bien, inutile ]
epistemique :: 2 :: [ long, grand ]
marqueurs_niveau2 :: 2 :: [ long, inutile ]
alethique_niveau3 :: 2 :: [ nécessaire, possible ]
axiologique :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau2 :: 1 :: [ inutile ]
axiologique_niveau3 :: 1 :: [ bien ]
epistemique_niveau3 :: 1 :: [ grand ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 26

