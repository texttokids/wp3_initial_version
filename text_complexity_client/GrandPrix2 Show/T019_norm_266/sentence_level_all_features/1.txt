
********** Entity Names Features **********
*******************************************

1. Sentence: De la culture de la sélection à la culture de l ' innovation Issu de mon passage à la direction de l ' École des ponts , le livre plaide pour une réforme profonde des grandes écoles , une nouvelle articulation avec les univer‑ sités , des regroupements permettant d ' accéder à une taille critique , une ouver‑ ture sociale accrue , et surtout une prise de recul par rapport à une obsession quasi pathologique de la sélection qui met en tension tout le système scolaire français , comme si celui‑ci n ' avait pour ultime objectif que de dégager une très mince élite .
Entity Name (Type) :: École des ponts (ORG)


********** Emotions Features **********
***************************************

1. Sentence: De la culture de la sélection à la culture de l ' innovation Issu de mon passage à la direction de l ' École des ponts , le livre plaide pour une réforme profonde des grandes écoles , une nouvelle articulation avec les univer‑ sités , des regroupements permettant d ' accéder à une taille critique , une ouver‑ ture sociale accrue , et surtout une prise de recul par rapport à une obsession quasi pathologique de la sélection qui met en tension tout le système scolaire français , comme si celui‑ci n ' avait pour ultime objectif que de dégager une très mince élite .
Tokens having emotion: [ critique, tension ]
Lemmas having emotion: [ critique, tension ]
Categories of the emotion lemmas: [ mepris, deplaisir ]


********** Modality Features **********
***************************************

1. Sentence: De la culture de la sélection à la culture de l ' innovation Issu de mon passage à la direction de l ' École des ponts , le livre plaide pour une réforme profonde des grandes écoles , une nouvelle articulation avec les univer‑ sités , des regroupements permettant d ' accéder à une taille critique , une ouver‑ ture sociale accrue , et surtout une prise de recul par rapport à une obsession quasi pathologique de la sélection qui met en tension tout le système scolaire français , comme si celui‑ci n ' avait pour ultime objectif que de dégager une très mince élite .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ école, grand, école, permettre, mince ]
marqueurs_niveau3 :: 3 :: [ grand, permettre, mince ]
alethique :: 2 :: [ école, école ]
epistemique :: 2 :: [ grand, mince ]
marqueurs_niveau1 :: 2 :: [ école, école ]
alethique_niveau1 :: 2 :: [ école, école ]
epistemique_niveau3 :: 2 :: [ grand, mince ]
deontique :: 1 :: [ permettre ]
deontique_niveau3 :: 1 :: [ permettre ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: De la culture de la sélection à la culture de l ' innovation Issu de mon passage à la direction de l ' École des ponts , le livre plaide pour une réforme profonde des grandes écoles , une nouvelle articulation avec les univer‑ sités , des regroupements permettant d ' accéder à une taille critique , une ouver‑ ture sociale accrue , et surtout une prise de recul par rapport à une obsession quasi pathologique de la sélection qui met en tension tout le système scolaire français , comme si celui‑ci n ' avait pour ultime objectif que de dégager une très mince élite .
No features found.

