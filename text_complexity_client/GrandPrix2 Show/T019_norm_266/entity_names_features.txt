====== Sentence Level Features ======

1. Sentence: De la culture de la sélection à la culture de l ' innovation Issu de mon passage à la direction de l ' École des ponts , le livre plaide pour une réforme profonde des grandes écoles , une nouvelle articulation avec les univer‑ sités , des regroupements permettant d ' accéder à une taille critique , une ouver‑ ture sociale accrue , et surtout une prise de recul par rapport à une obsession quasi pathologique de la sélection qui met en tension tout le système scolaire français , comme si celui‑ci n ' avait pour ultime objectif que de dégager une très mince élite .
Entity Name (Type) :: École des ponts (ORG)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
École des ponts (ORG)
Total entitiy counts: 1

