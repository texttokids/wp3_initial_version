====== Sentence Level Features ======

1. Sentence: De métropole , il fallait qu ' elle devienne ce que Melvin Weber avait commencé à décrire dans les années soixante : un semis , un tableau méticuleusement séquencé que ses moteurs (débrouille et flux tendus , téléports et ubiquité) finissent toujours par faire marcher . «Métapole» chez François Ascher , «ville diffuse» contournant les petites cités idéales que les successeurs d ' Alberti avaient élaborées , chez Bernardo Secchi , périphéries tout simplement qu ' on ne sait plus par quel bout prendre tant les positions sont acquises , les vides plus ou moins affectés .
Entity Name (Type) :: Bernardo Secchi (PER)


====== Text Level Features ======

Entity Name (Entity Type)
-------------------------
Melvin Weber (PER)
François Ascher (PER)
Alberti (PER)
Bernardo Secchi (PER)
Total entitiy counts: 4

