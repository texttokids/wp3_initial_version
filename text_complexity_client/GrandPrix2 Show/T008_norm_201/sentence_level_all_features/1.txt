
********** Entity Names Features **********
*******************************************

1. Sentence: En 1990 , je travaille sur un îlot du XIIIe arrondissement que Michel Lombardini , le président de la Régie immobilière de la Ville de Paris , me demande de transformer , tout près des Hautes Formes et de la rue du Château des Rentiers , où j ' avais déjà affronté deux situations urbaines très différentes .
Entity Name (Type) :: Château des Rentiers (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En 1990 , je travaille sur un îlot du XIIIe arrondissement que Michel Lombardini , le président de la Régie immobilière de la Ville de Paris , me demande de transformer , tout près des Hautes Formes et de la rue du Château des Rentiers , où j ' avais déjà affronté deux situations urbaines très différentes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 1990 , je travaille sur un îlot du XIIIe arrondissement que Michel Lombardini , le président de la Régie immobilière de la Ville de Paris , me demande de transformer , tout près des Hautes Formes et de la rue du Château des Rentiers , où j ' avais déjà affronté deux situations urbaines très différentes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ travailler, demander, haut, château, affronter ]
appreciatif :: 2 :: [ travailler, château ]
marqueurs_niveau3 :: 2 :: [ demander, château ]
axiologique :: 1 :: [ affronter ]
boulique :: 1 :: [ demander ]
epistemique :: 1 :: [ château ]
pas_d_indication :: 1 :: [ haut ]
marqueurs_niveau2 :: 1 :: [ travailler ]
marqueurs_niveau1 :: 1 :: [ affronter ]
marqueurs_niveau0 :: 1 :: [ haut ]
appreciatif_niveau3 :: 1 :: [ château ]
appreciatif_niveau2 :: 1 :: [ travailler ]
axiologique_niveau1 :: 1 :: [ affronter ]
boulique_niveau3 :: 1 :: [ demander ]
epistemique_niveau3 :: 1 :: [ château ]
pas_d_indication_niveau0 :: 1 :: [ haut ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 1990 , je travaille sur un îlot du XIIIe arrondissement que Michel Lombardini , le président de la Régie immobilière de la Ville de Paris , me demande de transformer , tout près des Hautes Formes et de la rue du Château des Rentiers , où j ' avais déjà affronté deux situations urbaines très différentes .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['En 1990']
adverbiaux_localisation_temporelle::1::['En 1990']
adverbiaux_pointage_absolu::1::['En 1990']
adverbiaux_pointage_non_absolu::1::['En 1990']
adverbiaux_loc_temp_focalisation_id::1::['En 1990']
adverbiaux_loc_temp_regionalisation_id::1::['En 1990']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 1990']
adverbiaux_dur_iter_absolu::1::['En 1990']

