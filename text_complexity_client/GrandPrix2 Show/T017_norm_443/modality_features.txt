====== Sentence Level Features ======

1. Sentence: Stabiliser cette approche demande de puiser dans les dispositifs existants , tels les baux (préférés à la propriété pleine) , les structures de gestion inédites comme la SCIC 1 pour la Friche de la Belle de Mai , ou les statuts permettant à des programmes inattendus , générés par les sites (et pas l ' inverse) , d ' être éligibles aux financements publics 2 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ demander, inédit, beau, permettre, inattendu ]
marqueurs_niveau3 :: 3 :: [ demander, beau, permettre ]
appreciatif :: 2 :: [ beau, inattendu ]
alethique :: 1 :: [ inattendu ]
boulique :: 1 :: [ demander ]
deontique :: 1 :: [ permettre ]
pas_d_indication :: 1 :: [ inédit ]
marqueurs_niveau1 :: 1 :: [ inattendu ]
marqueurs_niveau0 :: 1 :: [ inédit ]
alethique_niveau1 :: 1 :: [ inattendu ]
appreciatif_niveau3 :: 1 :: [ beau ]
appreciatif_niveau1 :: 1 :: [ inattendu ]
boulique_niveau3 :: 1 :: [ demander ]
deontique_niveau3 :: 1 :: [ permettre ]
pas_d_indication_niveau0 :: 1 :: [ inédit ]
Total feature counts: 22


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ demander, inédit, beau, permettre, inattendu ]
marqueurs_niveau3 :: 3 :: [ demander, beau, permettre ]
appreciatif :: 2 :: [ beau, inattendu ]
alethique :: 1 :: [ inattendu ]
boulique :: 1 :: [ demander ]
deontique :: 1 :: [ permettre ]
pas_d_indication :: 1 :: [ inédit ]
marqueurs_niveau1 :: 1 :: [ inattendu ]
marqueurs_niveau0 :: 1 :: [ inédit ]
alethique_niveau1 :: 1 :: [ inattendu ]
appreciatif_niveau3 :: 1 :: [ beau ]
appreciatif_niveau1 :: 1 :: [ inattendu ]
boulique_niveau3 :: 1 :: [ demander ]
deontique_niveau3 :: 1 :: [ permettre ]
pas_d_indication_niveau0 :: 1 :: [ inédit ]
Total feature counts: 22

