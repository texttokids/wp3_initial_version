
********** Entity Names Features **********
*******************************************

1. Sentence: À son sens l ' amour et le projet urbain et architectural ont en commun d ' être essentiellement imprévisibles  : jouer avec l ' imprévu , fort de la distinction que le Code napoléonien établit entre mobile et immobile , c ' est établir un rapport utile et confiant à l ' indéfini .
Entity Name (Type) :: Code napoléonien (MISC)


********** Emotions Features **********
***************************************

1. Sentence: À son sens l ' amour et le projet urbain et architectural ont en commun d ' être essentiellement imprévisibles  : jouer avec l ' imprévu , fort de la distinction que le Code napoléonien établit entre mobile et immobile , c ' est établir un rapport utile et confiant à l ' indéfini .
Tokens having emotion: [ amour, confiant ]
Lemmas having emotion: [ amour, confiant ]
Categories of the emotion lemmas: [ amour, audace ]


********** Modality Features **********
***************************************

1. Sentence: À son sens l ' amour et le projet urbain et architectural ont en commun d ' être essentiellement imprévisibles  : jouer avec l ' imprévu , fort de la distinction que le Code napoléonien établit entre mobile et immobile , c ' est établir un rapport utile et confiant à l ' indéfini .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ amour, imprévisible, jouer, fort, immobile, utile ]
appreciatif :: 4 :: [ amour, imprévisible, jouer, utile ]
pas_d_indication :: 2 :: [ fort, immobile ]
marqueurs_niveau2 :: 2 :: [ amour, jouer ]
marqueurs_niveau0 :: 2 :: [ fort, immobile ]
appreciatif_niveau2 :: 2 :: [ amour, jouer ]
pas_d_indication_niveau0 :: 2 :: [ fort, immobile ]
alethique :: 1 :: [ imprévisible ]
marqueurs_niveau3 :: 1 :: [ utile ]
marqueurs_niveau1 :: 1 :: [ imprévisible ]
alethique_niveau1 :: 1 :: [ imprévisible ]
appreciatif_niveau3 :: 1 :: [ utile ]
appreciatif_niveau1 :: 1 :: [ imprévisible ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À son sens l ' amour et le projet urbain et architectural ont en commun d ' être essentiellement imprévisibles  : jouer avec l ' imprévu , fort de la distinction que le Code napoléonien établit entre mobile et immobile , c ' est établir un rapport utile et confiant à l ' indéfini .
No features found.

