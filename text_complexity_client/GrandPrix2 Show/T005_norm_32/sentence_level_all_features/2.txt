
********** Entity Names Features **********
*******************************************

2. Sentence: Tous les hommes sont mortels de Beauvoir , que j ' ai relu récemment , fait saisir que le sens de la vie est donné par sa ﬁnitude , par la mort .
Entity Name (Type) :: Beauvoir (PER)


********** Emotions Features **********
***************************************

2. Sentence: Tous les hommes sont mortels de Beauvoir , que j ' ai relu récemment , fait saisir que le sens de la vie est donné par sa ﬁnitude , par la mort .
No features found.


********** Modality Features **********
***************************************

2. Sentence: Tous les hommes sont mortels de Beauvoir , que j ' ai relu récemment , fait saisir que le sens de la vie est donné par sa ﬁnitude , par la mort .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ mort ]
appreciatif :: 1 :: [ mort ]
marqueurs_niveau2 :: 1 :: [ mort ]
appreciatif_niveau2 :: 1 :: [ mort ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

2. Sentence: Tous les hommes sont mortels de Beauvoir , que j ' ai relu récemment , fait saisir que le sens de la vie est donné par sa ﬁnitude , par la mort .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['récemment']
adverbiaux_duratifs_iteratifs::1::['récemment']
adverbiaux_loc_temp_regionalisation_id::1::['récemment']

