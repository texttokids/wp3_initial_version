====== Sentence Level Features ======

1. Sentence: Mais quand la Loi est insuffisante ou inadaptée , il ne faut pas hésiter à l ' interpréter à partir d ' une expérimentation et constituer des jurisprudences , notamment dans le cas du droit à la création comme cela a été fait dans le cas des « Deux Plateaux » avec Daniel Buren , dans la cour d ' honneur du Palais-Royal .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ loi, falloir, partir, droit, honneur ]
deontique :: 3 :: [ loi, falloir, droit ]
marqueurs_niveau3 :: 2 :: [ falloir, droit ]
marqueurs_niveau2 :: 2 :: [ loi, honneur ]
deontique_niveau3 :: 2 :: [ falloir, droit ]
alethique :: 1 :: [ partir ]
axiologique :: 1 :: [ honneur ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau1 :: 1 :: [ partir ]
axiologique_niveau2 :: 1 :: [ honneur ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ loi, falloir, partir, droit, honneur ]
deontique :: 3 :: [ loi, falloir, droit ]
marqueurs_niveau3 :: 2 :: [ falloir, droit ]
marqueurs_niveau2 :: 2 :: [ loi, honneur ]
deontique_niveau3 :: 2 :: [ falloir, droit ]
alethique :: 1 :: [ partir ]
axiologique :: 1 :: [ honneur ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau1 :: 1 :: [ partir ]
axiologique_niveau2 :: 1 :: [ honneur ]
deontique_niveau2 :: 1 :: [ loi ]
Total feature counts: 20

