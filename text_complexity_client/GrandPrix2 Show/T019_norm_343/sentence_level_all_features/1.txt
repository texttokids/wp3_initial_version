
********** Entity Names Features **********
*******************************************

1. Sentence: Les inégalités , d ' abord  : les écarts géographiques entre les zones riches et les zones pauvres s ' accroissent dangereusement .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les inégalités , d ' abord  : les écarts géographiques entre les zones riches et les zones pauvres s ' accroissent dangereusement .
Tokens having emotion: [ dangereusement ]
Lemmas having emotion: [ dangereusement ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Les inégalités , d ' abord  : les écarts géographiques entre les zones riches et les zones pauvres s ' accroissent dangereusement .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ inégalité, riche, pauvre ]
pas_d_indication :: 2 :: [ inégalité, pauvre ]
marqueurs_niveau0 :: 2 :: [ inégalité, pauvre ]
pas_d_indication_niveau0 :: 2 :: [ inégalité, pauvre ]
appreciatif :: 1 :: [ riche ]
marqueurs_niveau2 :: 1 :: [ riche ]
appreciatif_niveau2 :: 1 :: [ riche ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les inégalités , d ' abord  : les écarts géographiques entre les zones riches et les zones pauvres s ' accroissent dangereusement .
No features found.

