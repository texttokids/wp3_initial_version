
********** Entity Names Features **********
*******************************************

1. Sentence: Le projet opte pour une intervention minimaliste , particulièrement attentive à utiliser la topographie comme élément révélateur du Parc Dräi Eechelen , Luxembourg , 1999-2010 [maîtrise d ' œuvre des espaces publics] par de petits jardins qui , paradoxalement , sont les lieux où sont rassemblées toutes les fonctions .
Entity Name (Type) :: Luxembourg (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le projet opte pour une intervention minimaliste , particulièrement attentive à utiliser la topographie comme élément révélateur du Parc Dräi Eechelen , Luxembourg , 1999-2010 [maîtrise d ' œuvre des espaces publics] par de petits jardins qui , paradoxalement , sont les lieux où sont rassemblées toutes les fonctions .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Le projet opte pour une intervention minimaliste , particulièrement attentive à utiliser la topographie comme élément révélateur du Parc Dräi Eechelen , Luxembourg , 1999-2010 [maîtrise d ' œuvre des espaces publics] par de petits jardins qui , paradoxalement , sont les lieux où sont rassemblées toutes les fonctions .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ petit ]
epistemique :: 1 :: [ petit ]
marqueurs_niveau3 :: 1 :: [ petit ]
epistemique_niveau3 :: 1 :: [ petit ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le projet opte pour une intervention minimaliste , particulièrement attentive à utiliser la topographie comme élément révélateur du Parc Dräi Eechelen , Luxembourg , 1999-2010 [maîtrise d ' œuvre des espaces publics] par de petits jardins qui , paradoxalement , sont les lieux où sont rassemblées toutes les fonctions .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['1999_2010']
adverbiaux_localisation_temporelle::1::['1999_2010']
adverbiaux_pointage_absolu::1::['1999_2010']
adverbiaux_loc_temp_focalisation_id::1::['1999_2010']
adverbiaux_loc_temp_regionalisation_id::1::['1999_2010']
adverbiaux_loc_temp_pointage_absolu::1::['1999_2010']

