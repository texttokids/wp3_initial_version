
********** Entity Names Features **********
*******************************************

1. Sentence: Nous le voyons avec la Preuve par 7  :  l ' alliance entre Pérignat-surAllier et Billom parle d ' un rapprochement historique toujours nécessaire du point de vue du fonctionnement de ce territoire .
Entity Name (Type) :: Billom (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Nous le voyons avec la Preuve par 7  :  l ' alliance entre Pérignat-surAllier et Billom parle d ' un rapprochement historique toujours nécessaire du point de vue du fonctionnement de ce territoire .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Nous le voyons avec la Preuve par 7  :  l ' alliance entre Pérignat-surAllier et Billom parle d ' un rapprochement historique toujours nécessaire du point de vue du fonctionnement de ce territoire .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ preuve, toujours, nécessaire ]
alethique :: 2 :: [ toujours, nécessaire ]
marqueurs_niveau3 :: 2 :: [ toujours, nécessaire ]
alethique_niveau3 :: 2 :: [ toujours, nécessaire ]
pas_d_indication :: 1 :: [ preuve ]
marqueurs_niveau0 :: 1 :: [ preuve ]
pas_d_indication_niveau0 :: 1 :: [ preuve ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Nous le voyons avec la Preuve par 7  :  l ' alliance entre Pérignat-surAllier et Billom parle d ' un rapprochement historique toujours nécessaire du point de vue du fonctionnement de ce territoire .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

