
********** Entity Names Features **********
*******************************************

1. Sentence: Je n ' ai jamais été en compétition , ni jaloux .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Je n ' ai jamais été en compétition , ni jaloux .
Tokens having emotion: [ jaloux ]
Lemmas having emotion: [ jaloux ]
Categories of the emotion lemmas: [ jalousie ]


********** Modality Features **********
***************************************

1. Sentence: Je n ' ai jamais été en compétition , ni jaloux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ jamais, jaloux ]
alethique :: 1 :: [ jamais ]
appreciatif :: 1 :: [ jaloux ]
boulique :: 1 :: [ jaloux ]
epistemique :: 1 :: [ jaloux ]
marqueurs_niveau3 :: 1 :: [ jamais ]
marqueurs_niveau2 :: 1 :: [ jaloux ]
alethique_niveau3 :: 1 :: [ jamais ]
appreciatif_niveau2 :: 1 :: [ jaloux ]
boulique_niveau2 :: 1 :: [ jaloux ]
epistemique_niveau2 :: 1 :: [ jaloux ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je n ' ai jamais été en compétition , ni jaloux .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jamais']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_iterateur_frequentiel::1::['jamais']

