====== Sentence Level Features ======

1. Sentence: Contextualistes si l ' on veut , auxquels il faudrait ajouter tous les apparents puritains — abstraits plutôt lyriques , en vérité — , pour lesquels sa liberté était en rade , prisonnière d ' un fonctionnalisme qui , certes , l ' a changée , raidie et simplifiée , mais sur un mode rugueux que l ' un des grands acquis de la période récente est d ' avoir dépassé : chose légère chez Piano , logique et entêtée chez Chemetov ou simplement limpide dans les dessins d ' Hauvette , de Falloci et de Berger .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ vouloir, falloir, vérité, liberté, grand ]
marqueurs_niveau3 :: 3 :: [ vouloir, falloir, grand ]
marqueurs_niveau1 :: 2 :: [ vérité, liberté ]
alethique :: 1 :: [ vérité ]
appreciatif :: 1 :: [ liberté ]
boulique :: 1 :: [ vouloir ]
deontique :: 1 :: [ falloir ]
epistemique :: 1 :: [ grand ]
alethique_niveau1 :: 1 :: [ vérité ]
appreciatif_niveau1 :: 1 :: [ liberté ]
boulique_niveau3 :: 1 :: [ vouloir ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ vouloir, falloir, vérité, liberté, grand ]
marqueurs_niveau3 :: 3 :: [ vouloir, falloir, grand ]
marqueurs_niveau1 :: 2 :: [ vérité, liberté ]
alethique :: 1 :: [ vérité ]
appreciatif :: 1 :: [ liberté ]
boulique :: 1 :: [ vouloir ]
deontique :: 1 :: [ falloir ]
epistemique :: 1 :: [ grand ]
alethique_niveau1 :: 1 :: [ vérité ]
appreciatif_niveau1 :: 1 :: [ liberté ]
boulique_niveau3 :: 1 :: [ vouloir ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 20

