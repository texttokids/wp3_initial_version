
********** Entity Names Features **********
*******************************************

1. Sentence: Donc , quotidiennement , dans l ' ombre ou la lumière , face aux «injonctions paradoxales» , aux aléas de la «démocratie participative » et aux intérêts parfois contradictoires entre ville , transporteurs et bailleurs , «je défends l ' espace public 6» .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Donc , quotidiennement , dans l ' ombre ou la lumière , face aux «injonctions paradoxales» , aux aléas de la «démocratie participative » et aux intérêts parfois contradictoires entre ville , transporteurs et bailleurs , «je défends l ' espace public 6» .
Tokens having emotion: [ intérêts ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Donc , quotidiennement , dans l ' ombre ou la lumière , face aux «injonctions paradoxales» , aux aléas de la «démocratie participative » et aux intérêts parfois contradictoires entre ville , transporteurs et bailleurs , «je défends l ' espace public 6» .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ lumière, intérêt, parfois, défendre ]
alethique :: 2 :: [ lumière, parfois ]
marqueurs_niveau1 :: 2 :: [ lumière, défendre ]
appreciatif :: 1 :: [ lumière ]
axiologique :: 1 :: [ défendre ]
boulique :: 1 :: [ intérêt ]
marqueurs_niveau3 :: 1 :: [ parfois ]
marqueurs_niveau2 :: 1 :: [ intérêt ]
alethique_niveau3 :: 1 :: [ parfois ]
alethique_niveau1 :: 1 :: [ lumière ]
appreciatif_niveau1 :: 1 :: [ lumière ]
axiologique_niveau1 :: 1 :: [ défendre ]
boulique_niveau2 :: 1 :: [ intérêt ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Donc , quotidiennement , dans l ' ombre ou la lumière , face aux «injonctions paradoxales» , aux aléas de la «démocratie participative » et aux intérêts parfois contradictoires entre ville , transporteurs et bailleurs , «je défends l ' espace public 6» .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['quotidiennement', 'parfois']
adverbiaux_purement_iteratifs::2::['quotidiennement', 'parfois']
adverbiaux_iterateur_calendaire::1::['quotidiennement']
adverbiaux_iterateur_frequentiel::1::['parfois']

