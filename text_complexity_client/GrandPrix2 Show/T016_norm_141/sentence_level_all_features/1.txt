
********** Entity Names Features **********
*******************************************

1. Sentence: Dans la «Métropole horizontale» de Bruxelles et sa région et dans le cas de Lille notamment , nous avons suggéré d ' envisager de façon réaliste la possibilité de réduire considérablement l ' usage du véhicule individuel en faveur des transports en commun , dans une aire où la densité de lignes ferroviaires et de tramway est déjà impressionnante et où les projets prévus conduiront à une plus grande intégration entre les territoires de la dispersion et les villes .
Entity Name (Type) :: Lille (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Dans la «Métropole horizontale» de Bruxelles et sa région et dans le cas de Lille notamment , nous avons suggéré d ' envisager de façon réaliste la possibilité de réduire considérablement l ' usage du véhicule individuel en faveur des transports en commun , dans une aire où la densité de lignes ferroviaires et de tramway est déjà impressionnante et où les projets prévus conduiront à une plus grande intégration entre les territoires de la dispersion et les villes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Dans la «Métropole horizontale» de Bruxelles et sa région et dans le cas de Lille notamment , nous avons suggéré d ' envisager de façon réaliste la possibilité de réduire considérablement l ' usage du véhicule individuel en faveur des transports en commun , dans une aire où la densité de lignes ferroviaires et de tramway est déjà impressionnante et où les projets prévus conduiront à une plus grande intégration entre les territoires de la dispersion et les villes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possibilité, impressionnant, grand ]
marqueurs_niveau3 :: 2 :: [ possibilité, grand ]
alethique :: 1 :: [ possibilité ]
appreciatif :: 1 :: [ impressionnant ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ impressionnant ]
alethique_niveau3 :: 1 :: [ possibilité ]
appreciatif_niveau2 :: 1 :: [ impressionnant ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans la «Métropole horizontale» de Bruxelles et sa région et dans le cas de Lille notamment , nous avons suggéré d ' envisager de façon réaliste la possibilité de réduire considérablement l ' usage du véhicule individuel en faveur des transports en commun , dans une aire où la densité de lignes ferroviaires et de tramway est déjà impressionnante et où les projets prévus conduiront à une plus grande intégration entre les territoires de la dispersion et les villes .
No features found.

