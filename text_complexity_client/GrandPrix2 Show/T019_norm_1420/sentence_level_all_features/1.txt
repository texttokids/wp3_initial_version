
********** Entity Names Features **********
*******************************************

1. Sentence: Les États restent des acteurs majeurs et ne sont pas près d ' être marginalisés (du moins les grands États structurés , qui ne représentent qu ' une petite minorité des États du monde) .
Entity Name (Type) :: États du monde (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Les États restent des acteurs majeurs et ne sont pas près d ' être marginalisés (du moins les grands États structurés , qui ne représentent qu ' une petite minorité des États du monde) .
Tokens having emotion: [ États, États, États ]
Lemmas having emotion: [ état, état, état ]
Categories of the emotion lemmas: [ non_specifiee, non_specifiee, non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Les États restent des acteurs majeurs et ne sont pas près d ' être marginalisés (du moins les grands États structurés , qui ne représentent qu ' une petite minorité des États du monde) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ grand, petit ]
epistemique :: 2 :: [ grand, petit ]
marqueurs_niveau3 :: 2 :: [ grand, petit ]
epistemique_niveau3 :: 2 :: [ grand, petit ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les États restent des acteurs majeurs et ne sont pas près d ' être marginalisés (du moins les grands États structurés , qui ne représentent qu ' une petite minorité des États du monde) .
No features found.

