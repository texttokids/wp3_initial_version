
********** Entity Names Features **********
*******************************************

1. Sentence: Le PGD , régulièrement revu depuis , s ' intéresse à l ' ensemble des questions qui font la ville — la culture , les sports , la vie sociale ,  l ' urbanité , le développement économique , etc .
Entity Name (Type) :: PGD (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Le PGD , régulièrement revu depuis , s ' intéresse à l ' ensemble des questions qui font la ville — la culture , les sports , la vie sociale ,  l ' urbanité , le développement économique , etc .
Tokens having emotion: [ intéresse ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Le PGD , régulièrement revu depuis , s ' intéresse à l ' ensemble des questions qui font la ville — la culture , les sports , la vie sociale ,  l ' urbanité , le développement économique , etc .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ intéresser ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le PGD , régulièrement revu depuis , s ' intéresse à l ' ensemble des questions qui font la ville — la culture , les sports , la vie sociale ,  l ' urbanité , le développement économique , etc .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['régulièrement', 'depuis']
adverbiaux_purement_iteratifs::1::['régulièrement']
adverbiaux_duratifs_iteratifs::1::['depuis']
adverbiaux_pointage_non_absolu::1::['depuis']
adverbiaux_iterateur_frequentiel::1::['régulièrement']
adverbiaux_dur_iter_anaphorique::1::['depuis']

