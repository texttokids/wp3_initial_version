
********** Entity Names Features **********
*******************************************

1. Sentence: Ces entreprises ont fini par tisser entre elles des réseaux d ' échange , se renforçant mutuellement , et elles constituent à présent un ensemble dont la suite du projet de l ' île de Nantes va devoir tenir compte .
Entity Name (Type) :: île de Nantes (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ces entreprises ont fini par tisser entre elles des réseaux d ' échange , se renforçant mutuellement , et elles constituent à présent un ensemble dont la suite du projet de l ' île de Nantes va devoir tenir compte .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ces entreprises ont fini par tisser entre elles des réseaux d ' échange , se renforçant mutuellement , et elles constituent à présent un ensemble dont la suite du projet de l ' île de Nantes va devoir tenir compte .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ aller, devoir ]
alethique :: 2 :: [ aller, devoir ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
marqueurs_niveau3 :: 1 :: [ devoir ]
marqueurs_niveau1 :: 1 :: [ aller ]
alethique_niveau3 :: 1 :: [ devoir ]
alethique_niveau1 :: 1 :: [ aller ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ces entreprises ont fini par tisser entre elles des réseaux d ' échange , se renforçant mutuellement , et elles constituent à présent un ensemble dont la suite du projet de l ' île de Nantes va devoir tenir compte .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['à présent']
adverbiaux_localisation_temporelle::1::['à présent']
adverbiaux_pointage_non_absolu::2::['à présent', 'à présent']
adverbiaux_loc_temp_focalisation_id::1::['à présent']
adverbiaux_loc_temp_regionalisation_id::1::['à présent']
adverbiaux_loc_temp_pointage_non_absolu::1::['à présent']
adverbiaux_dur_iter_deictique::1::['à présent']

