
********** Entity Names Features **********
*******************************************

1. Sentence: Le rôle de l ' État est de diffuser ce savoir-faire , et ce encore davantage à l ' heure de la décentralisation .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le rôle de l ' État est de diffuser ce savoir-faire , et ce encore davantage à l ' heure de la décentralisation .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Le rôle de l ' État est de diffuser ce savoir-faire , et ce encore davantage à l ' heure de la décentralisation .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le rôle de l ' État est de diffuser ce savoir-faire , et ce encore davantage à l ' heure de la décentralisation .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

