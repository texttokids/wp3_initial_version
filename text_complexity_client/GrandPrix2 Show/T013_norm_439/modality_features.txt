====== Sentence Level Features ======

1. Sentence: Les documents de Solà-Morales — notamment un dessin aux tonalités verdâtres qui figurait la fameuse rampe et un espace informe que se disputaient des arbres maigrelets , des voitures , des fauteuils et des flaques d ' eau — avaient de quoi choquer les esprits : davantage qu ' un produit fini , il soutenait une démarche pour atteindre un objectif , avec une qualité et une force d ' expression exceptionnelles .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ informe, disputer, choquer, esprit, force, exceptionnel ]
pas_d_indication :: 3 :: [ informe, choquer, force ]
marqueurs_niveau0 :: 3 :: [ informe, choquer, force ]
pas_d_indication_niveau0 :: 3 :: [ informe, choquer, force ]
appreciatif :: 2 :: [ disputer, exceptionnel ]
axiologique :: 2 :: [ disputer, exceptionnel ]
epistemique :: 2 :: [ esprit, exceptionnel ]
marqueurs_niveau3 :: 1 :: [ esprit ]
marqueurs_niveau2 :: 1 :: [ disputer ]
marqueurs_niveau1 :: 1 :: [ exceptionnel ]
appreciatif_niveau2 :: 1 :: [ disputer ]
appreciatif_niveau1 :: 1 :: [ exceptionnel ]
axiologique_niveau2 :: 1 :: [ disputer ]
axiologique_niveau1 :: 1 :: [ exceptionnel ]
epistemique_niveau3 :: 1 :: [ esprit ]
epistemique_niveau1 :: 1 :: [ exceptionnel ]
Total feature counts: 30


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ informe, disputer, choquer, esprit, force, exceptionnel ]
pas_d_indication :: 3 :: [ informe, choquer, force ]
marqueurs_niveau0 :: 3 :: [ informe, choquer, force ]
pas_d_indication_niveau0 :: 3 :: [ informe, choquer, force ]
appreciatif :: 2 :: [ disputer, exceptionnel ]
axiologique :: 2 :: [ disputer, exceptionnel ]
epistemique :: 2 :: [ esprit, exceptionnel ]
marqueurs_niveau3 :: 1 :: [ esprit ]
marqueurs_niveau2 :: 1 :: [ disputer ]
marqueurs_niveau1 :: 1 :: [ exceptionnel ]
appreciatif_niveau2 :: 1 :: [ disputer ]
appreciatif_niveau1 :: 1 :: [ exceptionnel ]
axiologique_niveau2 :: 1 :: [ disputer ]
axiologique_niveau1 :: 1 :: [ exceptionnel ]
epistemique_niveau3 :: 1 :: [ esprit ]
epistemique_niveau1 :: 1 :: [ exceptionnel ]
Total feature counts: 30

