
********** Entity Names Features **********
*******************************************

1. Sentence: Aurelio Galfetti , Luigi Snozzi , Alfredo Pini (Atelier 5) sont des personnages précieux : on connaît la manière avec laquelle ils ont su superposer une certaine modernité germanique avec une attention très précise aux sites .
Entity Name (Type) :: Atelier 5 (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Aurelio Galfetti , Luigi Snozzi , Alfredo Pini (Atelier 5) sont des personnages précieux : on connaît la manière avec laquelle ils ont su superposer une certaine modernité germanique avec une attention très précise aux sites .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Aurelio Galfetti , Luigi Snozzi , Alfredo Pini (Atelier 5) sont des personnages précieux : on connaît la manière avec laquelle ils ont su superposer une certaine modernité germanique avec une attention très précise aux sites .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ précieux, connaître, savoir, certain ]
epistemique :: 3 :: [ connaître, savoir, certain ]
marqueurs_niveau3 :: 2 :: [ savoir, certain ]
marqueurs_niveau2 :: 2 :: [ précieux, connaître ]
epistemique_niveau3 :: 2 :: [ savoir, certain ]
appreciatif :: 1 :: [ précieux ]
appreciatif_niveau2 :: 1 :: [ précieux ]
epistemique_niveau2 :: 1 :: [ connaître ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Aurelio Galfetti , Luigi Snozzi , Alfredo Pini (Atelier 5) sont des personnages précieux : on connaît la manière avec laquelle ils ont su superposer une certaine modernité germanique avec une attention très précise aux sites .
No features found.

