
********** Entity Names Features **********
*******************************************

1. Sentence: Parfois les architectes sont amenés par les promoteurs mais souvent ils sont choisis lors de consultations publiques ou privées auxquelles je participe comme membre du jury .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Parfois les architectes sont amenés par les promoteurs mais souvent ils sont choisis lors de consultations publiques ou privées auxquelles je participe comme membre du jury .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Parfois les architectes sont amenés par les promoteurs mais souvent ils sont choisis lors de consultations publiques ou privées auxquelles je participe comme membre du jury .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ parfois, souvent, choisir ]
alethique :: 2 :: [ parfois, souvent ]
marqueurs_niveau3 :: 2 :: [ parfois, souvent ]
alethique_niveau3 :: 2 :: [ parfois, souvent ]
boulique :: 1 :: [ choisir ]
marqueurs_niveau2 :: 1 :: [ choisir ]
boulique_niveau2 :: 1 :: [ choisir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Parfois les architectes sont amenés par les promoteurs mais souvent ils sont choisis lors de consultations publiques ou privées auxquelles je participe comme membre du jury .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['Parfois', 'souvent']
adverbiaux_purement_iteratifs::2::['Parfois', 'souvent']
adverbiaux_iterateur_frequentiel::2::['Parfois', 'souvent']

