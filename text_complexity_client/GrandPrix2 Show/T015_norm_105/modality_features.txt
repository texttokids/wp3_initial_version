====== Sentence Level Features ======

1. Sentence: Avant que des projets substantiels ne prennent forme , il est au moins possible de profiter de cette vacance pour qu ' une sorte de nature s ' installe dans la ville , une nature d ' une peupleraie vue d ' avion dans laquelle la rivière sculpte une clairière , un lieu qui peut être habitable ,  l ' objectif est de reconstituer une forêt alluviale .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possible, vacance, pouvoir ]
marqueurs_niveau3 :: 3 :: [ possible, vacance, pouvoir ]
alethique :: 2 :: [ possible, pouvoir ]
alethique_niveau3 :: 2 :: [ possible, pouvoir ]
appreciatif :: 1 :: [ vacance ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
appreciatif_niveau3 :: 1 :: [ vacance ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 16


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possible, vacance, pouvoir ]
marqueurs_niveau3 :: 3 :: [ possible, vacance, pouvoir ]
alethique :: 2 :: [ possible, pouvoir ]
alethique_niveau3 :: 2 :: [ possible, pouvoir ]
appreciatif :: 1 :: [ vacance ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
appreciatif_niveau3 :: 1 :: [ vacance ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 16

