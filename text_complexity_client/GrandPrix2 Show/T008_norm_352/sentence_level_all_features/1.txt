
********** Entity Names Features **********
*******************************************

1. Sentence: Beijing logistic port , Pékin , Chine (2003) À Pékin , en ces années d ' une croissance urbaine qu ' aucune ville n ' a jamais connue à ce rythme effréné et à cette échelle , on compte par centaines les « poches » , les camps résidentiels fermés et les condominiums en chantier .
Entity Name (Type) :: Pékin (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Beijing logistic port , Pékin , Chine (2003) À Pékin , en ces années d ' une croissance urbaine qu ' aucune ville n ' a jamais connue à ce rythme effréné et à cette échelle , on compte par centaines les « poches » , les camps résidentiels fermés et les condominiums en chantier .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Beijing logistic port , Pékin , Chine (2003) À Pékin , en ces années d ' une croissance urbaine qu ' aucune ville n ' a jamais connue à ce rythme effréné et à cette échelle , on compte par centaines les « poches » , les camps résidentiels fermés et les condominiums en chantier .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ aucun, jamais, connaître ]
alethique :: 2 :: [ aucun, jamais ]
marqueurs_niveau3 :: 2 :: [ aucun, jamais ]
alethique_niveau3 :: 2 :: [ aucun, jamais ]
epistemique :: 1 :: [ connaître ]
marqueurs_niveau2 :: 1 :: [ connaître ]
epistemique_niveau2 :: 1 :: [ connaître ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Beijing logistic port , Pékin , Chine (2003) À Pékin , en ces années d ' une croissance urbaine qu ' aucune ville n ' a jamais connue à ce rythme effréné et à cette échelle , on compte par centaines les « poches » , les camps résidentiels fermés et les condominiums en chantier .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jamais']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_iterateur_frequentiel::1::['jamais']

