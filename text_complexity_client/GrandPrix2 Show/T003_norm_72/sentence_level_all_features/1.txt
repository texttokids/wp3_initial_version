
********** Entity Names Features **********
*******************************************

1. Sentence: inscription agricole quand le travail paysan a laissé des traces singulières qui donnent parfois une forte identité aux lieux ;
No features found.


********** Emotions Features **********
***************************************

1. Sentence: inscription agricole quand le travail paysan a laissé des traces singulières qui donnent parfois une forte identité aux lieux ;
No features found.


********** Modality Features **********
***************************************

1. Sentence: inscription agricole quand le travail paysan a laissé des traces singulières qui donnent parfois une forte identité aux lieux ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ travail, laisser, parfois, fort ]
marqueurs_niveau3 :: 2 :: [ travail, parfois ]
alethique :: 1 :: [ parfois ]
appreciatif :: 1 :: [ travail ]
deontique :: 1 :: [ laisser ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau2 :: 1 :: [ laisser ]
marqueurs_niveau0 :: 1 :: [ fort ]
alethique_niveau3 :: 1 :: [ parfois ]
appreciatif_niveau3 :: 1 :: [ travail ]
deontique_niveau2 :: 1 :: [ laisser ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: inscription agricole quand le travail paysan a laissé des traces singulières qui donnent parfois une forte identité aux lieux ;
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

