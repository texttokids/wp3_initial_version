
********** Entity Names Features **********
*******************************************

1. Sentence: Les innovations peuvent certes contribuer à modifier les choix , mais elles doivent s ' inscrire dans le respect de cette diversité et de la démocratie .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les innovations peuvent certes contribuer à modifier les choix , mais elles doivent s ' inscrire dans le respect de cette diversité et de la démocratie .
Tokens having emotion: [ respect ]
Lemmas having emotion: [ respect ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Les innovations peuvent certes contribuer à modifier les choix , mais elles doivent s ' inscrire dans le respect de cette diversité et de la démocratie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, choix, devoir, respect ]
alethique :: 2 :: [ pouvoir, devoir ]
deontique :: 2 :: [ pouvoir, devoir ]
epistemique :: 2 :: [ pouvoir, devoir ]
marqueurs_niveau3 :: 2 :: [ pouvoir, devoir ]
marqueurs_niveau2 :: 2 :: [ choix, respect ]
alethique_niveau3 :: 2 :: [ pouvoir, devoir ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, devoir ]
appreciatif :: 1 :: [ respect ]
axiologique :: 1 :: [ respect ]
boulique :: 1 :: [ choix ]
appreciatif_niveau2 :: 1 :: [ respect ]
axiologique_niveau2 :: 1 :: [ respect ]
boulique_niveau2 :: 1 :: [ choix ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les innovations peuvent certes contribuer à modifier les choix , mais elles doivent s ' inscrire dans le respect de cette diversité et de la démocratie .
No features found.

