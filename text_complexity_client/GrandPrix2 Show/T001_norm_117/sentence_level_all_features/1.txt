
********** Entity Names Features **********
*******************************************

1. Sentence: Dans un regret , mais aussi dans la découverte — au fil des chantiers de fouilles scrutés par les «modernes» — que l ' Antiquité , elle aussi , avait affronté cette même difficulté : détruisant ses premiers brouillons (Sélinonte , Priène ou Milet) , les régularisant , sans hésiter à les recommencer .
Entity Name (Type) :: Milet (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Dans un regret , mais aussi dans la découverte — au fil des chantiers de fouilles scrutés par les «modernes» — que l ' Antiquité , elle aussi , avait affronté cette même difficulté : détruisant ses premiers brouillons (Sélinonte , Priène ou Milet) , les régularisant , sans hésiter à les recommencer .
Tokens having emotion: [ regret ]
Lemmas having emotion: [ regret ]
Categories of the emotion lemmas: [ culpabilite ]


********** Modality Features **********
***************************************

1. Sentence: Dans un regret , mais aussi dans la découverte — au fil des chantiers de fouilles scrutés par les «modernes» — que l ' Antiquité , elle aussi , avait affronté cette même difficulté : détruisant ses premiers brouillons (Sélinonte , Priène ou Milet) , les régularisant , sans hésiter à les recommencer .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ regret, moderne, affronter, difficulté, détruire ]
appreciatif :: 2 :: [ regret, détruire ]
axiologique :: 2 :: [ affronter, détruire ]
pas_d_indication :: 2 :: [ moderne, difficulté ]
marqueurs_niveau1 :: 2 :: [ affronter, détruire ]
marqueurs_niveau0 :: 2 :: [ moderne, difficulté ]
axiologique_niveau1 :: 2 :: [ affronter, détruire ]
pas_d_indication_niveau0 :: 2 :: [ moderne, difficulté ]
epistemique :: 1 :: [ regret ]
marqueurs_niveau2 :: 1 :: [ regret ]
appreciatif_niveau2 :: 1 :: [ regret ]
appreciatif_niveau1 :: 1 :: [ détruire ]
epistemique_niveau2 :: 1 :: [ regret ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans un regret , mais aussi dans la découverte — au fil des chantiers de fouilles scrutés par les «modernes» — que l ' Antiquité , elle aussi , avait affronté cette même difficulté : détruisant ses premiers brouillons (Sélinonte , Priène ou Milet) , les régularisant , sans hésiter à les recommencer .
No features found.

