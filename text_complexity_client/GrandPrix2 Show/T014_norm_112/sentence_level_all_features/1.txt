
********** Entity Names Features **********
*******************************************

1. Sentence: Pour l ' urbaniste , il est généralement gratifiant de prévoir de généreux espaces de voies publiques — et ce , au détriment d ' une fréquentation diluée et malgré un coût de réalisation et d ' entretien élevé .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Pour l ' urbaniste , il est généralement gratifiant de prévoir de généreux espaces de voies publiques — et ce , au détriment d ' une fréquentation diluée et malgré un coût de réalisation et d ' entretien élevé .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Pour l ' urbaniste , il est généralement gratifiant de prévoir de généreux espaces de voies publiques — et ce , au détriment d ' une fréquentation diluée et malgré un coût de réalisation et d ' entretien élevé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ généralement, généreux, détriment, malgré ]
appreciatif :: 2 :: [ généreux, détriment ]
marqueurs_niveau3 :: 2 :: [ généralement, généreux ]
alethique :: 1 :: [ généralement ]
axiologique :: 1 :: [ généreux ]
pas_d_indication :: 1 :: [ malgré ]
marqueurs_niveau2 :: 1 :: [ détriment ]
marqueurs_niveau0 :: 1 :: [ malgré ]
alethique_niveau3 :: 1 :: [ généralement ]
appreciatif_niveau3 :: 1 :: [ généreux ]
appreciatif_niveau2 :: 1 :: [ détriment ]
axiologique_niveau3 :: 1 :: [ généreux ]
pas_d_indication_niveau0 :: 1 :: [ malgré ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pour l ' urbaniste , il est généralement gratifiant de prévoir de généreux espaces de voies publiques — et ce , au détriment d ' une fréquentation diluée et malgré un coût de réalisation et d ' entretien élevé .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['généralement']
adverbiaux_purement_iteratifs::1::['généralement']
adverbiaux_iterateur_frequentiel::1::['généralement']

