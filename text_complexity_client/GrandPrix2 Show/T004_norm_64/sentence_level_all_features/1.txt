
********** Entity Names Features **********
*******************************************

1. Sentence: C ' était une méga-forme urbaine enveloppant littéralement une rue gigantesque qui , me semble-t-il , pouvait contenir une forme égale à celle de l ' Arc de Triomphe : « Le monumental devait requalifier l ' Urbain » disait Ciriani .
Entity Name (Type) :: Ciriani (PER)


********** Emotions Features **********
***************************************

1. Sentence: C ' était une méga-forme urbaine enveloppant littéralement une rue gigantesque qui , me semble-t-il , pouvait contenir une forme égale à celle de l ' Arc de Triomphe : « Le monumental devait requalifier l ' Urbain » disait Ciriani .
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' était une méga-forme urbaine enveloppant littéralement une rue gigantesque qui , me semble-t-il , pouvait contenir une forme égale à celle de l ' Arc de Triomphe : « Le monumental devait requalifier l ' Urbain » disait Ciriani .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ gigantesque, sembler, pouvoir, devoir, dire ]
epistemique :: 4 :: [ gigantesque, sembler, pouvoir, devoir ]
marqueurs_niveau3 :: 4 :: [ sembler, pouvoir, devoir, dire ]
epistemique_niveau3 :: 3 :: [ sembler, pouvoir, devoir ]
alethique :: 2 :: [ pouvoir, devoir ]
deontique :: 2 :: [ pouvoir, devoir ]
alethique_niveau3 :: 2 :: [ pouvoir, devoir ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
boulique :: 1 :: [ dire ]
marqueurs_niveau2 :: 1 :: [ gigantesque ]
boulique_niveau3 :: 1 :: [ dire ]
epistemique_niveau2 :: 1 :: [ gigantesque ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' était une méga-forme urbaine enveloppant littéralement une rue gigantesque qui , me semble-t-il , pouvait contenir une forme égale à celle de l ' Arc de Triomphe : « Le monumental devait requalifier l ' Urbain » disait Ciriani .
No features found.

