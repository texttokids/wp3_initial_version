
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai développé cette vision dans un papier auquel je tiens  : « La sociologie est un sport de plein air » , paru dans la revue Esprit en janvier 2015 .
Entity Name (Type) :: Esprit (MISC)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai développé cette vision dans un papier auquel je tiens  : « La sociologie est un sport de plein air » , paru dans la revue Esprit en janvier 2015 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: J ' ai développé cette vision dans un papier auquel je tiens  : « La sociologie est un sport de plein air » , paru dans la revue Esprit en janvier 2015 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ paraître ]
epistemique :: 1 :: [ paraître ]
marqueurs_niveau3 :: 1 :: [ paraître ]
epistemique_niveau3 :: 1 :: [ paraître ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai développé cette vision dans un papier auquel je tiens  : « La sociologie est un sport de plein air » , paru dans la revue Esprit en janvier 2015 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en janvier 2015']
adverbiaux_localisation_temporelle::1::['en janvier 2015']
adverbiaux_pointage_absolu::1::['en janvier 2015']
adverbiaux_pointage_non_absolu::1::['en janvier 2015']
adverbiaux_loc_temp_focalisation_id::1::['en janvier 2015']
adverbiaux_loc_temp_regionalisation_id::1::['en janvier 2015']
adverbiaux_loc_temp_pointage_non_absolu::1::['en janvier 2015']
adverbiaux_dur_iter_absolu::1::['en janvier 2015']

