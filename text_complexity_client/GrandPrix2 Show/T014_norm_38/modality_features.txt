====== Sentence Level Features ======

1. Sentence: Quand se distinguent , comme des évidences , des constantes suffisamment fortes , des orientations pour un projet , partagées avec les responsables et les acteurs concernés , s ' ouvrent des possibilités de négociation et d ' adaptation que réclame le passage à l ' acte .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ suffisamment, fort, responsable, ouvrir, possibilité, réclamer ]
boulique :: 2 :: [ ouvrir, réclamer ]
pas_d_indication :: 2 :: [ suffisamment, fort ]
marqueurs_niveau3 :: 2 :: [ ouvrir, possibilité ]
marqueurs_niveau0 :: 2 :: [ suffisamment, fort ]
pas_d_indication_niveau0 :: 2 :: [ suffisamment, fort ]
alethique :: 1 :: [ possibilité ]
deontique :: 1 :: [ responsable ]
marqueurs_niveau2 :: 1 :: [ responsable ]
marqueurs_niveau1 :: 1 :: [ réclamer ]
alethique_niveau3 :: 1 :: [ possibilité ]
boulique_niveau3 :: 1 :: [ ouvrir ]
boulique_niveau1 :: 1 :: [ réclamer ]
deontique_niveau2 :: 1 :: [ responsable ]
Total feature counts: 24


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ suffisamment, fort, responsable, ouvrir, possibilité, réclamer ]
boulique :: 2 :: [ ouvrir, réclamer ]
pas_d_indication :: 2 :: [ suffisamment, fort ]
marqueurs_niveau3 :: 2 :: [ ouvrir, possibilité ]
marqueurs_niveau0 :: 2 :: [ suffisamment, fort ]
pas_d_indication_niveau0 :: 2 :: [ suffisamment, fort ]
alethique :: 1 :: [ possibilité ]
deontique :: 1 :: [ responsable ]
marqueurs_niveau2 :: 1 :: [ responsable ]
marqueurs_niveau1 :: 1 :: [ réclamer ]
alethique_niveau3 :: 1 :: [ possibilité ]
boulique_niveau3 :: 1 :: [ ouvrir ]
boulique_niveau1 :: 1 :: [ réclamer ]
deontique_niveau2 :: 1 :: [ responsable ]
Total feature counts: 24

