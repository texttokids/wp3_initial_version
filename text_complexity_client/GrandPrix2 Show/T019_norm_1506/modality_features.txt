====== Sentence Level Features ======

1. Sentence: Le formidable intérêt de cette mutation profonde que constituent l ' émer‑ gence de l ' État européen , la crise des États‑nations et la montée des régions est précisément de nous obliger à ré‑interroger des notions trop confortablement (et , de fait , illusoirement) incrustées dans leur coquille nationale : citoyenneté , souveraineté , démocratie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ formidable, intérêt, crise, obliger ]
marqueurs_niveau2 :: 3 :: [ formidable, intérêt, crise ]
appreciatif :: 2 :: [ formidable, crise ]
appreciatif_niveau2 :: 2 :: [ formidable, crise ]
boulique :: 1 :: [ intérêt ]
deontique :: 1 :: [ obliger ]
epistemique :: 1 :: [ crise ]
marqueurs_niveau3 :: 1 :: [ obliger ]
boulique_niveau2 :: 1 :: [ intérêt ]
deontique_niveau3 :: 1 :: [ obliger ]
epistemique_niveau2 :: 1 :: [ crise ]
Total feature counts: 18


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ formidable, intérêt, crise, obliger ]
marqueurs_niveau2 :: 3 :: [ formidable, intérêt, crise ]
appreciatif :: 2 :: [ formidable, crise ]
appreciatif_niveau2 :: 2 :: [ formidable, crise ]
boulique :: 1 :: [ intérêt ]
deontique :: 1 :: [ obliger ]
epistemique :: 1 :: [ crise ]
marqueurs_niveau3 :: 1 :: [ obliger ]
boulique_niveau2 :: 1 :: [ intérêt ]
deontique_niveau3 :: 1 :: [ obliger ]
epistemique_niveau2 :: 1 :: [ crise ]
Total feature counts: 18

