
********** Entity Names Features **********
*******************************************

1. Sentence: Cartographier la justice environnementale et spatiale (les risques environnementaux et la distribution du bien-être social , depuis l ' école jusqu ' à la répartition territoriale des soins de santé et de nourriture saine , puis à l ' accessibilité et aux connexions avec le transport public) fournit des représentations très utiles à intégrer à l ' évaluation de l ' énergie grise et des cycles de vie .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Cartographier la justice environnementale et spatiale (les risques environnementaux et la distribution du bien-être social , depuis l ' école jusqu ' à la répartition territoriale des soins de santé et de nourriture saine , puis à l ' accessibilité et aux connexions avec le transport public) fournit des représentations très utiles à intégrer à l ' évaluation de l ' énergie grise et des cycles de vie .
Tokens having emotion: [ bien-être ]
Lemmas having emotion: [ bien-être ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Cartographier la justice environnementale et spatiale (les risques environnementaux et la distribution du bien-être social , depuis l ' école jusqu ' à la répartition territoriale des soins de santé et de nourriture saine , puis à l ' accessibilité et aux connexions avec le transport public) fournit des représentations très utiles à intégrer à l ' évaluation de l ' énergie grise et des cycles de vie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ justice, bien-être, école, santé, nourriture, utile ]
appreciatif :: 3 :: [ bien-être, nourriture, utile ]
alethique :: 2 :: [ école, nourriture ]
marqueurs_niveau2 :: 2 :: [ justice, bien-être ]
marqueurs_niveau1 :: 2 :: [ école, nourriture ]
alethique_niveau1 :: 2 :: [ école, nourriture ]
axiologique :: 1 :: [ justice ]
pas_d_indication :: 1 :: [ santé ]
marqueurs_niveau3 :: 1 :: [ utile ]
marqueurs_niveau0 :: 1 :: [ santé ]
appreciatif_niveau3 :: 1 :: [ utile ]
appreciatif_niveau2 :: 1 :: [ bien-être ]
appreciatif_niveau1 :: 1 :: [ nourriture ]
axiologique_niveau2 :: 1 :: [ justice ]
pas_d_indication_niveau0 :: 1 :: [ santé ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cartographier la justice environnementale et spatiale (les risques environnementaux et la distribution du bien-être social , depuis l ' école jusqu ' à la répartition territoriale des soins de santé et de nourriture saine , puis à l ' accessibilité et aux connexions avec le transport public) fournit des représentations très utiles à intégrer à l ' évaluation de l ' énergie grise et des cycles de vie .
No features found.

