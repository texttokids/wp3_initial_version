
********** Entity Names Features **********
*******************************************

1. Sentence: Un grand écart permanent , sans rapport avec les « villes nouvelles » où l ' État avait vraiment le pouvoir .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Un grand écart permanent , sans rapport avec les « villes nouvelles » où l ' État avait vraiment le pouvoir .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Un grand écart permanent , sans rapport avec les « villes nouvelles » où l ' État avait vraiment le pouvoir .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ grand, vraiment, pouvoir ]
alethique :: 2 :: [ vraiment, pouvoir ]
epistemique :: 2 :: [ grand, pouvoir ]
marqueurs_niveau3 :: 2 :: [ grand, pouvoir ]
epistemique_niveau3 :: 2 :: [ grand, pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau1 :: 1 :: [ vraiment ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau1 :: 1 :: [ vraiment ]
deontique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un grand écart permanent , sans rapport avec les « villes nouvelles » où l ' État avait vraiment le pouvoir .
No features found.

