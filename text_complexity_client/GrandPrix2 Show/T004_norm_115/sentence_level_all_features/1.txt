
********** Entity Names Features **********
*******************************************

1. Sentence: Pour regarder comment il se resserre progressivement vers l ' aval; comment , juste avant son entrée en ville , il détermine , en sous-œuvre , les ouvrages de la digue et du parc de la Tête d ' Or; comment , enfin , il se concentre entre les perrés et les murs de quais , surlignés par le double mail de platanes qui traverse et caractérise la ville de Lyon .
Entity Name (Type) :: Lyon (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Pour regarder comment il se resserre progressivement vers l ' aval; comment , juste avant son entrée en ville , il détermine , en sous-œuvre , les ouvrages de la digue et du parc de la Tête d ' Or; comment , enfin , il se concentre entre les perrés et les murs de quais , surlignés par le double mail de platanes qui traverse et caractérise la ville de Lyon .
Tokens having emotion: [ détermine ]
Lemmas having emotion: [ déterminer ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Pour regarder comment il se resserre progressivement vers l ' aval; comment , juste avant son entrée en ville , il détermine , en sous-œuvre , les ouvrages de la digue et du parc de la Tête d ' Or; comment , enfin , il se concentre entre les perrés et les murs de quais , surlignés par le double mail de platanes qui traverse et caractérise la ville de Lyon .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ juste ]
axiologique :: 1 :: [ juste ]
marqueurs_niveau3 :: 1 :: [ juste ]
axiologique_niveau3 :: 1 :: [ juste ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pour regarder comment il se resserre progressivement vers l ' aval; comment , juste avant son entrée en ville , il détermine , en sous-œuvre , les ouvrages de la digue et du parc de la Tête d ' Or; comment , enfin , il se concentre entre les perrés et les murs de quais , surlignés par le double mail de platanes qui traverse et caractérise la ville de Lyon .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['juste avant']
adverbiaux_localisation_temporelle::1::['juste avant']
adverbiaux_pointage_non_absolu::1::['juste avant']
adverbiaux_loc_temp_regionalisation_id::1::['juste avant']
adverbiaux_loc_temp_pointage_non_absolu::1::['juste avant']

