
********** Entity Names Features **********
*******************************************

1. Sentence: De l ' intimité à l ' ouverture , avec la mise en œuvre d ' une gestion alternative des eaux pluviales , la présence d ' un mobilier ludique spécialement créé pour ce projet , et des jeux d ' eaux… ce sont de multiples attentions aux usages et à la vie du quartier , au climat lyonnais avec ses journées de chaleur étouffante , à la sécurité des espaces et aux facilités de gestion pour les services de la Ville .
Entity Name (Type) :: Ville (LOC)


********** Emotions Features **********
***************************************

1. Sentence: De l ' intimité à l ' ouverture , avec la mise en œuvre d ' une gestion alternative des eaux pluviales , la présence d ' un mobilier ludique spécialement créé pour ce projet , et des jeux d ' eaux… ce sont de multiples attentions aux usages et à la vie du quartier , au climat lyonnais avec ses journées de chaleur étouffante , à la sécurité des espaces et aux facilités de gestion pour les services de la Ville .
No features found.


********** Modality Features **********
***************************************

1. Sentence: De l ' intimité à l ' ouverture , avec la mise en œuvre d ' une gestion alternative des eaux pluviales , la présence d ' un mobilier ludique spécialement créé pour ce projet , et des jeux d ' eaux… ce sont de multiples attentions aux usages et à la vie du quartier , au climat lyonnais avec ses journées de chaleur étouffante , à la sécurité des espaces et aux facilités de gestion pour les services de la Ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ sécurité, facilité ]
appreciatif :: 1 :: [ facilité ]
epistemique :: 1 :: [ facilité ]
pas_d_indication :: 1 :: [ sécurité ]
marqueurs_niveau2 :: 1 :: [ facilité ]
marqueurs_niveau0 :: 1 :: [ sécurité ]
appreciatif_niveau2 :: 1 :: [ facilité ]
epistemique_niveau2 :: 1 :: [ facilité ]
pas_d_indication_niveau0 :: 1 :: [ sécurité ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: De l ' intimité à l ' ouverture , avec la mise en œuvre d ' une gestion alternative des eaux pluviales , la présence d ' un mobilier ludique spécialement créé pour ce projet , et des jeux d ' eaux… ce sont de multiples attentions aux usages et à la vie du quartier , au climat lyonnais avec ses journées de chaleur étouffante , à la sécurité des espaces et aux facilités de gestion pour les services de la Ville .
No features found.

