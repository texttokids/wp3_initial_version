
********** Entity Names Features **********
*******************************************

1. Sentence: Il apparaît que , dans un contexte de changement climatique avéré , une analyse partagée consiste à considérer qu ' un des fléaux les plus graves , mis en évidence par le Grenelle de l ' environnement , concerne l ' étalement non maîtrisé de l ' urbanisation , source d ' alourdissement de la consommation d ' énergie , de déséquilibres environnementaux et d ' inégalités sociales .
Entity Name (Type) :: Grenelle de l ' environnement (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Il apparaît que , dans un contexte de changement climatique avéré , une analyse partagée consiste à considérer qu ' un des fléaux les plus graves , mis en évidence par le Grenelle de l ' environnement , concerne l ' étalement non maîtrisé de l ' urbanisation , source d ' alourdissement de la consommation d ' énergie , de déséquilibres environnementaux et d ' inégalités sociales .
Tokens having emotion: [ considérer ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Il apparaît que , dans un contexte de changement climatique avéré , une analyse partagée consiste à considérer qu ' un des fléaux les plus graves , mis en évidence par le Grenelle de l ' environnement , concerne l ' étalement non maîtrisé de l ' urbanisation , source d ' alourdissement de la consommation d ' énergie , de déséquilibres environnementaux et d ' inégalités sociales .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ considérer, grave, inégalité ]
appreciatif :: 1 :: [ grave ]
axiologique :: 1 :: [ grave ]
epistemique :: 1 :: [ considérer ]
pas_d_indication :: 1 :: [ inégalité ]
marqueurs_niveau3 :: 1 :: [ considérer ]
marqueurs_niveau2 :: 1 :: [ grave ]
marqueurs_niveau0 :: 1 :: [ inégalité ]
appreciatif_niveau2 :: 1 :: [ grave ]
axiologique_niveau2 :: 1 :: [ grave ]
epistemique_niveau3 :: 1 :: [ considérer ]
pas_d_indication_niveau0 :: 1 :: [ inégalité ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il apparaît que , dans un contexte de changement climatique avéré , une analyse partagée consiste à considérer qu ' un des fléaux les plus graves , mis en évidence par le Grenelle de l ' environnement , concerne l ' étalement non maîtrisé de l ' urbanisation , source d ' alourdissement de la consommation d ' énergie , de déséquilibres environnementaux et d ' inégalités sociales .
No features found.

