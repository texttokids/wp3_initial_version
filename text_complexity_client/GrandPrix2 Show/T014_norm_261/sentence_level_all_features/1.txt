
********** Entity Names Features **********
*******************************************

1. Sentence: La règle au service du projet Bassin de la Villette et alentour , Paris 17e , 1970-1987 Au moment du «scandale de la Villette*» (1970) , François Grether élabore à l ' Atelier parisien d ' urbanisme (Apur) un projet de transformation des abattoirs encore actifs .
Entity Name (Type) :: Apur (LOC)


********** Emotions Features **********
***************************************

1. Sentence: La règle au service du projet Bassin de la Villette et alentour , Paris 17e , 1970-1987 Au moment du «scandale de la Villette*» (1970) , François Grether élabore à l ' Atelier parisien d ' urbanisme (Apur) un projet de transformation des abattoirs encore actifs .
Tokens having emotion: [ scandale ]
Lemmas having emotion: [ scandale ]
Categories of the emotion lemmas: [ colere ]


********** Modality Features **********
***************************************

1. Sentence: La règle au service du projet Bassin de la Villette et alentour , Paris 17e , 1970-1987 Au moment du «scandale de la Villette*» (1970) , François Grether élabore à l ' Atelier parisien d ' urbanisme (Apur) un projet de transformation des abattoirs encore actifs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ scandale, abattoir ]
pas_d_indication :: 2 :: [ scandale, abattoir ]
marqueurs_niveau0 :: 2 :: [ scandale, abattoir ]
pas_d_indication_niveau0 :: 2 :: [ scandale, abattoir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La règle au service du projet Bassin de la Villette et alentour , Paris 17e , 1970-1987 Au moment du «scandale de la Villette*» (1970) , François Grether élabore à l ' Atelier parisien d ' urbanisme (Apur) un projet de transformation des abattoirs encore actifs .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['1970_1987', 'Au moment', 'encore']
adverbiaux_localisation_temporelle::2::['1970_1987', 'Au moment']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_absolu::1::['1970_1987']
adverbiaux_pointage_non_absolu::3::['Au moment', 'Au moment', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['1970_1987']
adverbiaux_loc_temp_regionalisation_id::1::['1970_1987']
adverbiaux_loc_temp_pointage_absolu::1::['1970_1987']
adverbiaux_loc_temp_pointage_non_absolu::1::['Au moment']
adverbiaux_dur_iter_relatif::2::['Au moment', 'encore']

