
********** Entity Names Features **********
*******************************************

1. Sentence: En 2002 ,  l ' AUC est lauréate des Nouveaux Albums de la Jeune Architecture et de trois études de définition sur le renouvellement des grands ensembles  : les Provinces à Troyes , Mistral à Grenoble et les Courtillières à Pantin , projet encore à l ' œuvre aujourd ' hui .
Entity Name (Type) :: Pantin (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En 2002 ,  l ' AUC est lauréate des Nouveaux Albums de la Jeune Architecture et de trois études de définition sur le renouvellement des grands ensembles  : les Provinces à Troyes , Mistral à Grenoble et les Courtillières à Pantin , projet encore à l ' œuvre aujourd ' hui .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 2002 ,  l ' AUC est lauréate des Nouveaux Albums de la Jeune Architecture et de trois études de définition sur le renouvellement des grands ensembles  : les Provinces à Troyes , Mistral à Grenoble et les Courtillières à Pantin , projet encore à l ' œuvre aujourd ' hui .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ jeune, grand ]
epistemique :: 1 :: [ grand ]
pas_d_indication :: 1 :: [ jeune ]
marqueurs_niveau3 :: 1 :: [ grand ]
marqueurs_niveau0 :: 1 :: [ jeune ]
epistemique_niveau3 :: 1 :: [ grand ]
pas_d_indication_niveau0 :: 1 :: [ jeune ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 2002 ,  l ' AUC est lauréate des Nouveaux Albums de la Jeune Architecture et de trois études de définition sur le renouvellement des grands ensembles  : les Provinces à Troyes , Mistral à Grenoble et les Courtillières à Pantin , projet encore à l ' œuvre aujourd ' hui .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['En 2002', 'encore']
adverbiaux_localisation_temporelle::1::['En 2002']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_absolu::1::['En 2002']
adverbiaux_pointage_non_absolu::2::['En 2002', 'encore']
adverbiaux_loc_temp_focalisation_id::1::['En 2002']
adverbiaux_loc_temp_regionalisation_id::1::['En 2002']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 2002']
adverbiaux_dur_iter_absolu::1::['En 2002']
adverbiaux_dur_iter_relatif::1::['encore']

