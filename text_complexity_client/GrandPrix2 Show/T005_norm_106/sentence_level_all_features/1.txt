
********** Entity Names Features **********
*******************************************

1. Sentence: Quand il repérait un projet qui lui semblait intéressant — par exemple les bassins Vauban au Havre — , je le relayais , commençant à faire réseau : « Vous pourriez faire une société de préﬁguration et contacter Jean-Paul Baïetto , directeur d ' Euralille… » Le Grand Prix de l ' urbanisme Quand Jean Frébault a eu la charge du Grand Prix de l ' urbanisme en 1992 , il m ' a montré la liste des choix proposés par la DAU et j ' ai suggéré Antoine Grumbach qui a eu le prix d ' emblée , alors que je ne m ' occupais pas du prix et que je trouvais la méthode étrange ,  l ' État décidant de la liste des nominés sans critères précis .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Quand il repérait un projet qui lui semblait intéressant — par exemple les bassins Vauban au Havre — , je le relayais , commençant à faire réseau : « Vous pourriez faire une société de préﬁguration et contacter Jean-Paul Baïetto , directeur d ' Euralille… » Le Grand Prix de l ' urbanisme Quand Jean Frébault a eu la charge du Grand Prix de l ' urbanisme en 1992 , il m ' a montré la liste des choix proposés par la DAU et j ' ai suggéré Antoine Grumbach qui a eu le prix d ' emblée , alors que je ne m ' occupais pas du prix et que je trouvais la méthode étrange ,  l ' État décidant de la liste des nominés sans critères précis .
Tokens having emotion: [ intéressant, État ]
Lemmas having emotion: [ intéressant, état ]
Categories of the emotion lemmas: [ desir, non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Quand il repérait un projet qui lui semblait intéressant — par exemple les bassins Vauban au Havre — , je le relayais , commençant à faire réseau : « Vous pourriez faire une société de préﬁguration et contacter Jean-Paul Baïetto , directeur d ' Euralille… » Le Grand Prix de l ' urbanisme Quand Jean Frébault a eu la charge du Grand Prix de l ' urbanisme en 1992 , il m ' a montré la liste des choix proposés par la DAU et j ' ai suggéré Antoine Grumbach qui a eu le prix d ' emblée , alors que je ne m ' occupais pas du prix et que je trouvais la méthode étrange ,  l ' État décidant de la liste des nominés sans critères précis .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 12 :: [ lui, sembler, pouvoir, grand, prix, grand, prix, choix, prix, prix, étrange, décider ]
epistemique :: 7 :: [ lui, sembler, pouvoir, grand, grand, étrange, décider ]
marqueurs_niveau3 :: 6 :: [ lui, sembler, pouvoir, grand, grand, décider ]
epistemique_niveau3 :: 6 :: [ lui, sembler, pouvoir, grand, grand, décider ]
appreciatif :: 5 :: [ prix, prix, prix, prix, étrange ]
marqueurs_niveau2 :: 5 :: [ prix, prix, choix, prix, prix ]
axiologique :: 4 :: [ prix, prix, prix, prix ]
appreciatif_niveau2 :: 4 :: [ prix, prix, prix, prix ]
axiologique_niveau2 :: 4 :: [ prix, prix, prix, prix ]
alethique :: 2 :: [ lui, pouvoir ]
boulique :: 2 :: [ choix, décider ]
alethique_niveau3 :: 2 :: [ lui, pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau1 :: 1 :: [ étrange ]
appreciatif_niveau1 :: 1 :: [ étrange ]
boulique_niveau3 :: 1 :: [ décider ]
boulique_niveau2 :: 1 :: [ choix ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau1 :: 1 :: [ étrange ]
Total feature counts: 66


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Quand il repérait un projet qui lui semblait intéressant — par exemple les bassins Vauban au Havre — , je le relayais , commençant à faire réseau : « Vous pourriez faire une société de préﬁguration et contacter Jean-Paul Baïetto , directeur d ' Euralille… » Le Grand Prix de l ' urbanisme Quand Jean Frébault a eu la charge du Grand Prix de l ' urbanisme en 1992 , il m ' a montré la liste des choix proposés par la DAU et j ' ai suggéré Antoine Grumbach qui a eu le prix d ' emblée , alors que je ne m ' occupais pas du prix et que je trouvais la méthode étrange ,  l ' État décidant de la liste des nominés sans critères précis .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en 1992']
adverbiaux_localisation_temporelle::1::['en 1992']
adverbiaux_pointage_absolu::1::['en 1992']
adverbiaux_pointage_non_absolu::1::['en 1992']
adverbiaux_loc_temp_focalisation_id::1::['en 1992']
adverbiaux_loc_temp_regionalisation_id::1::['en 1992']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1992']
adverbiaux_dur_iter_absolu::1::['en 1992']

