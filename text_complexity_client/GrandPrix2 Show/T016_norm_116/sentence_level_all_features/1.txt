
********** Entity Names Features **********
*******************************************

1. Sentence: Cette seconde épistémologie semble opposée à la première , elle s ' ouvre au temps long , un temps que le projet de villes et de territoires a souvent craint et rarement évoqué .
Entity Name (Type) :: Cette (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cette seconde épistémologie semble opposée à la première , elle s ' ouvre au temps long , un temps que le projet de villes et de territoires a souvent craint et rarement évoqué .
Tokens having emotion: [ craint ]
Lemmas having emotion: [ craindre ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Cette seconde épistémologie semble opposée à la première , elle s ' ouvre au temps long , un temps que le projet de villes et de territoires a souvent craint et rarement évoqué .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ sembler, ouvrir, long, souvent, craindre, rarement ]
marqueurs_niveau3 :: 5 :: [ sembler, ouvrir, souvent, craindre, rarement ]
epistemique :: 3 :: [ sembler, long, craindre ]
alethique :: 2 :: [ souvent, rarement ]
alethique_niveau3 :: 2 :: [ souvent, rarement ]
epistemique_niveau3 :: 2 :: [ sembler, craindre ]
appreciatif :: 1 :: [ craindre ]
boulique :: 1 :: [ ouvrir ]
marqueurs_niveau2 :: 1 :: [ long ]
appreciatif_niveau3 :: 1 :: [ craindre ]
boulique_niveau3 :: 1 :: [ ouvrir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 26


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cette seconde épistémologie semble opposée à la première , elle s ' ouvre au temps long , un temps que le projet de villes et de territoires a souvent craint et rarement évoqué .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::4::['Cette seconde', 'au temps', 'souvent', 'rarement']
adverbiaux_localisation_temporelle::2::['Cette seconde', 'au temps']
adverbiaux_purement_iteratifs::2::['souvent', 'rarement']
adverbiaux_pointage_non_absolu::3::['Cette seconde', 'au temps', 'au temps']
adverbiaux_loc_temp_focalisation_id::1::['Cette seconde']
adverbiaux_loc_temp_regionalisation_id::1::['Cette seconde']
adverbiaux_loc_temp_pointage_non_absolu::2::['Cette seconde', 'au temps']
adverbiaux_iterateur_frequentiel::2::['souvent', 'rarement']
adverbiaux_dur_iter_relatif::1::['au temps']

