
********** Entity Names Features **********
*******************************************

1. Sentence: À La Mecque , j ' invoque le confort urbain , je se confronter à la ville , qui est si compliquée , et à toutes les villes qu ' il faut connaître pour réussir à s ' intéresser à la sienne .
Entity Name (Type) :: La Mecque (LOC)


********** Emotions Features **********
***************************************

1. Sentence: À La Mecque , j ' invoque le confort urbain , je se confronter à la ville , qui est si compliquée , et à toutes les villes qu ' il faut connaître pour réussir à s ' intéresser à la sienne .
Tokens having emotion: [ intéresser ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: À La Mecque , j ' invoque le confort urbain , je se confronter à la ville , qui est si compliquée , et à toutes les villes qu ' il faut connaître pour réussir à s ' intéresser à la sienne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ falloir, connaître, réussir, intéresser ]
marqueurs_niveau3 :: 2 :: [ falloir, réussir ]
appreciatif :: 1 :: [ réussir ]
deontique :: 1 :: [ falloir ]
epistemique :: 1 :: [ connaître ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau2 :: 1 :: [ connaître ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
appreciatif_niveau3 :: 1 :: [ réussir ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau2 :: 1 :: [ connaître ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À La Mecque , j ' invoque le confort urbain , je se confronter à la ville , qui est si compliquée , et à toutes les villes qu ' il faut connaître pour réussir à s ' intéresser à la sienne .
No features found.

