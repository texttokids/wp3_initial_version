
********** Entity Names Features **********
*******************************************

1. Sentence: En collaboration avec l ' historien François Loyer ,  l ' étude de l ' urbanisation du 19e arrondissement au XIXe siècle , pour le ministère de la Culture , souligne le rôle intéressant des grandes infrastructures , des voies , des canaux , du chemin de fer , des abattoirs et du parc des Buttes-Chaumont dans la formation de cette partie de la ville .
Entity Name (Type) :: parc des Buttes-Chaumont (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En collaboration avec l ' historien François Loyer ,  l ' étude de l ' urbanisation du 19e arrondissement au XIXe siècle , pour le ministère de la Culture , souligne le rôle intéressant des grandes infrastructures , des voies , des canaux , du chemin de fer , des abattoirs et du parc des Buttes-Chaumont dans la formation de cette partie de la ville .
Tokens having emotion: [ intéressant ]
Lemmas having emotion: [ intéressant ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: En collaboration avec l ' historien François Loyer ,  l ' étude de l ' urbanisation du 19e arrondissement au XIXe siècle , pour le ministère de la Culture , souligne le rôle intéressant des grandes infrastructures , des voies , des canaux , du chemin de fer , des abattoirs et du parc des Buttes-Chaumont dans la formation de cette partie de la ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ collaboration, grand, abattoir ]
marqueurs_niveau3 :: 2 :: [ collaboration, grand ]
axiologique :: 1 :: [ collaboration ]
epistemique :: 1 :: [ grand ]
pas_d_indication :: 1 :: [ abattoir ]
marqueurs_niveau0 :: 1 :: [ abattoir ]
axiologique_niveau3 :: 1 :: [ collaboration ]
epistemique_niveau3 :: 1 :: [ grand ]
pas_d_indication_niveau0 :: 1 :: [ abattoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En collaboration avec l ' historien François Loyer ,  l ' étude de l ' urbanisation du 19e arrondissement au XIXe siècle , pour le ministère de la Culture , souligne le rôle intéressant des grandes infrastructures , des voies , des canaux , du chemin de fer , des abattoirs et du parc des Buttes-Chaumont dans la formation de cette partie de la ville .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['au XIXe siècle']
adverbiaux_localisation_temporelle::1::['au XIXe siècle']
adverbiaux_pointage_absolu::1::['au XIXe siècle']
adverbiaux_loc_temp_focalisation_id::1::['au XIXe siècle']
adverbiaux_loc_temp_regionalisation_id::1::['au XIXe siècle']
adverbiaux_loc_temp_pointage_absolu::1::['au XIXe siècle']

