
********** Entity Names Features **********
*******************************************

1. Sentence: En 1971 , je propose à la ville nouvelle le projet du Château d ' eau .
Entity Name (Type) :: Château d ' eau (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En 1971 , je propose à la ville nouvelle le projet du Château d ' eau .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 1971 , je propose à la ville nouvelle le projet du Château d ' eau .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ château ]
appreciatif :: 1 :: [ château ]
epistemique :: 1 :: [ château ]
marqueurs_niveau3 :: 1 :: [ château ]
appreciatif_niveau3 :: 1 :: [ château ]
epistemique_niveau3 :: 1 :: [ château ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 1971 , je propose à la ville nouvelle le projet du Château d ' eau .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['En 1971']
adverbiaux_localisation_temporelle::1::['En 1971']
adverbiaux_pointage_absolu::1::['En 1971']
adverbiaux_pointage_non_absolu::1::['En 1971']
adverbiaux_loc_temp_focalisation_id::1::['En 1971']
adverbiaux_loc_temp_regionalisation_id::1::['En 1971']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 1971']
adverbiaux_dur_iter_absolu::1::['En 1971']

