
********** Entity Names Features **********
*******************************************

1. Sentence: C ' était d ' autant plus pertinent que les villes qui se développaient s ' installaient en général sur des territoires relativement pauvres , souvent marqués par des sortes de marais , des paysages quelconques , avec peu de sollicitations , où la nature était peu présente , sinon sous forme de contraintes .
Entity Name (Type) :: C (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' était d ' autant plus pertinent que les villes qui se développaient s ' installaient en général sur des territoires relativement pauvres , souvent marqués par des sortes de marais , des paysages quelconques , avec peu de sollicitations , où la nature était peu présente , sinon sous forme de contraintes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' était d ' autant plus pertinent que les villes qui se développaient s ' installaient en général sur des territoires relativement pauvres , souvent marqués par des sortes de marais , des paysages quelconques , avec peu de sollicitations , où la nature était peu présente , sinon sous forme de contraintes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ pauvre, souvent ]
alethique :: 1 :: [ souvent ]
pas_d_indication :: 1 :: [ pauvre ]
marqueurs_niveau3 :: 1 :: [ souvent ]
marqueurs_niveau0 :: 1 :: [ pauvre ]
alethique_niveau3 :: 1 :: [ souvent ]
pas_d_indication_niveau0 :: 1 :: [ pauvre ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' était d ' autant plus pertinent que les villes qui se développaient s ' installaient en général sur des territoires relativement pauvres , souvent marqués par des sortes de marais , des paysages quelconques , avec peu de sollicitations , où la nature était peu présente , sinon sous forme de contraintes .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en général', 'souvent']
adverbiaux_purement_iteratifs::2::['en général', 'souvent']
adverbiaux_iterateur_frequentiel::2::['en général', 'souvent']

