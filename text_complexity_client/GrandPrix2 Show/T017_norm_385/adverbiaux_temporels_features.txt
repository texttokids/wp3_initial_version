====== Sentence Level Features ======

1. Sentence: À la fin des années quatrevingt , alors directeur de l ' atelier public d ' archi tecture et d ' urbanisme de Blois 1 , je teste pour la première fois la « permanence architecturale » en déménageant des bureaux municipaux dans l ' ancienne loge et le local vélo d ' une tour désaffectée , y invitant maires , préfets et ministres jusqu ' en 1994 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en 1994', 'pour la première fois']
adverbiaux_localisation_temporelle::1::['en 1994']
adverbiaux_purement_iteratifs::1::['pour la première fois']
adverbiaux_pointage_absolu::1::['en 1994']
adverbiaux_pointage_non_absolu::1::['en 1994']
adverbiaux_loc_temp_focalisation_id::1::['en 1994']
adverbiaux_loc_temp_regionalisation_id::1::['en 1994']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1994']
adverbiaux_iterateur_quantificationnel::1::['pour la première fois']
adverbiaux_dur_iter_absolu::1::['en 1994']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en 1994', 'pour la première fois']
adverbiaux_localisation_temporelle::1::['en 1994']
adverbiaux_purement_iteratifs::1::['pour la première fois']
adverbiaux_pointage_absolu::1::['en 1994']
adverbiaux_pointage_non_absolu::1::['en 1994']
adverbiaux_loc_temp_focalisation_id::1::['en 1994']
adverbiaux_loc_temp_regionalisation_id::1::['en 1994']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1994']
adverbiaux_iterateur_quantificationnel::1::['pour la première fois']
adverbiaux_dur_iter_absolu::1::['en 1994']
Total feature counts: 11

