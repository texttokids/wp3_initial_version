
********** Entity Names Features **********
*******************************************

1. Sentence: Un court passage dans une agence d ’ ailleurs tout à fait étonnante et chargée de la grande «rénovation» du 13e arrondissement aurait dû m ’ alerter : ce n ’ était pas , mais pas du tout , du côté des recherches que les choses se passaient .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Un court passage dans une agence d ’ ailleurs tout à fait étonnante et chargée de la grande «rénovation» du 13e arrondissement aurait dû m ’ alerter : ce n ’ était pas , mais pas du tout , du côté des recherches que les choses se passaient .
Tokens having emotion: [ étonnante, alerter ]
Lemmas having emotion: [ étonnant, alerter ]
Categories of the emotion lemmas: [ surprise, peur ]


********** Modality Features **********
***************************************

1. Sentence: Un court passage dans une agence d ’ ailleurs tout à fait étonnante et chargée de la grande «rénovation» du 13e arrondissement aurait dû m ’ alerter : ce n ’ était pas , mais pas du tout , du côté des recherches que les choses se passaient .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ étonnant, grand, devoir, alerter ]
epistemique :: 3 :: [ étonnant, grand, devoir ]
marqueurs_niveau3 :: 2 :: [ grand, devoir ]
epistemique_niveau3 :: 2 :: [ grand, devoir ]
alethique :: 1 :: [ devoir ]
deontique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ alerter ]
marqueurs_niveau1 :: 1 :: [ étonnant ]
marqueurs_niveau0 :: 1 :: [ alerter ]
alethique_niveau3 :: 1 :: [ devoir ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau1 :: 1 :: [ étonnant ]
pas_d_indication_niveau0 :: 1 :: [ alerter ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Un court passage dans une agence d ’ ailleurs tout à fait étonnante et chargée de la grande «rénovation» du 13e arrondissement aurait dû m ’ alerter : ce n ’ était pas , mais pas du tout , du côté des recherches que les choses se passaient .
No features found.

