
********** Entity Names Features **********
*******************************************

1. Sentence: Je ressors souvent de ma bibliothèque la magnifique analyse sur l ' armature urbaine que Félix Damette avait réalisée pour la Datar à partir les flux téléphoniques (La France en villes , La Documentation française , 1994) .
Entity Name (Type) :: La Documentation française (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Je ressors souvent de ma bibliothèque la magnifique analyse sur l ' armature urbaine que Félix Damette avait réalisée pour la Datar à partir les flux téléphoniques (La France en villes , La Documentation française , 1994) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Je ressors souvent de ma bibliothèque la magnifique analyse sur l ' armature urbaine que Félix Damette avait réalisée pour la Datar à partir les flux téléphoniques (La France en villes , La Documentation française , 1994) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ souvent, magnifique, réaliser, partir ]
alethique :: 3 :: [ souvent, réaliser, partir ]
marqueurs_niveau3 :: 2 :: [ souvent, magnifique ]
marqueurs_niveau1 :: 2 :: [ réaliser, partir ]
alethique_niveau1 :: 2 :: [ réaliser, partir ]
appreciatif :: 1 :: [ magnifique ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau3 :: 1 :: [ magnifique ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je ressors souvent de ma bibliothèque la magnifique analyse sur l ' armature urbaine que Félix Damette avait réalisée pour la Datar à partir les flux téléphoniques (La France en villes , La Documentation française , 1994) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

