
********** Entity Names Features **********
*******************************************

1. Sentence: Dès l ' origine du projet , nous avions la certitude que tous les éléments existants sur le site – la digue plusieurs fois modifiée , les quais hauts et bas , les anciennes constructions du Palais de la Foire datant de 1926 , celle du palais des Congrès de 1960 , les deux lignes de platanes , le parc de la Tête d ' Or lui-même – étaient issus d ' une même dynamique : celle que le fleuve suscite et impose .
Entity Name (Type) :: parc de la Tête d ' Or (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Dès l ' origine du projet , nous avions la certitude que tous les éléments existants sur le site – la digue plusieurs fois modifiée , les quais hauts et bas , les anciennes constructions du Palais de la Foire datant de 1926 , celle du palais des Congrès de 1960 , les deux lignes de platanes , le parc de la Tête d ' Or lui-même – étaient issus d ' une même dynamique : celle que le fleuve suscite et impose .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Dès l ' origine du projet , nous avions la certitude que tous les éléments existants sur le site – la digue plusieurs fois modifiée , les quais hauts et bas , les anciennes constructions du Palais de la Foire datant de 1926 , celle du palais des Congrès de 1960 , les deux lignes de platanes , le parc de la Tête d ' Or lui-même – étaient issus d ' une même dynamique : celle que le fleuve suscite et impose .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ certitude, haut, imposer ]
deontique :: 1 :: [ imposer ]
epistemique :: 1 :: [ certitude ]
pas_d_indication :: 1 :: [ haut ]
marqueurs_niveau3 :: 1 :: [ certitude ]
marqueurs_niveau2 :: 1 :: [ imposer ]
marqueurs_niveau0 :: 1 :: [ haut ]
deontique_niveau2 :: 1 :: [ imposer ]
epistemique_niveau3 :: 1 :: [ certitude ]
pas_d_indication_niveau0 :: 1 :: [ haut ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dès l ' origine du projet , nous avions la certitude que tous les éléments existants sur le site – la digue plusieurs fois modifiée , les quais hauts et bas , les anciennes constructions du Palais de la Foire datant de 1926 , celle du palais des Congrès de 1960 , les deux lignes de platanes , le parc de la Tête d ' Or lui-même – étaient issus d ' une même dynamique : celle que le fleuve suscite et impose .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::3::['de 1926', 'de 1960', 'plusieurs fois']
adverbiaux_localisation_temporelle::2::['de 1926', 'de 1960']
adverbiaux_purement_iteratifs::1::['plusieurs fois']
adverbiaux_pointage_absolu::2::['de 1926', 'de 1960']
adverbiaux_loc_temp_focalisation_id::2::['de 1926', 'de 1960']
adverbiaux_loc_temp_regionalisation_id::2::['de 1926', 'de 1960']
adverbiaux_loc_temp_pointage_absolu::2::['de 1926', 'de 1960']
adverbiaux_iterateur_quantificationnel::1::['plusieurs fois']

