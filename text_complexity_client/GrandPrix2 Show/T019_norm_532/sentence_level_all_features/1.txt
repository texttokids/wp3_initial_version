
********** Entity Names Features **********
*******************************************

1. Sentence: Première idée  : Paris , un siècle et demi après l ' extension de 1860 , change de taille et fusionne avec la petite couronne , la conséquence naturelle étant que les intercommunalités situées à l ' extérieur se renforcent pour faire face au « monstre » .
Entity Name (Type) :: Paris (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Première idée  : Paris , un siècle et demi après l ' extension de 1860 , change de taille et fusionne avec la petite couronne , la conséquence naturelle étant que les intercommunalités situées à l ' extérieur se renforcent pour faire face au « monstre » .
Tokens having emotion: [ monstre ]
Lemmas having emotion: [ monstre ]
Categories of the emotion lemmas: [ peur ]


********** Modality Features **********
***************************************

1. Sentence: Première idée  : Paris , un siècle et demi après l ' extension de 1860 , change de taille et fusionne avec la petite couronne , la conséquence naturelle étant que les intercommunalités situées à l ' extérieur se renforcent pour faire face au « monstre » .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ petit, monstre ]
appreciatif :: 1 :: [ monstre ]
epistemique :: 1 :: [ petit ]
marqueurs_niveau3 :: 1 :: [ petit ]
marqueurs_niveau2 :: 1 :: [ monstre ]
appreciatif_niveau2 :: 1 :: [ monstre ]
epistemique_niveau3 :: 1 :: [ petit ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Première idée  : Paris , un siècle et demi après l ' extension de 1860 , change de taille et fusionne avec la petite couronne , la conséquence naturelle étant que les intercommunalités situées à l ' extérieur se renforcent pour faire face au « monstre » .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 1860']
adverbiaux_localisation_temporelle::1::['de 1860']
adverbiaux_pointage_absolu::1::['de 1860']
adverbiaux_loc_temp_focalisation_id::1::['de 1860']
adverbiaux_loc_temp_regionalisation_id::1::['de 1860']
adverbiaux_loc_temp_pointage_absolu::1::['de 1860']

