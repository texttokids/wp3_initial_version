
********** Entity Names Features **********
*******************************************

2. Sentence: Le travail avec Gilles Clément , paysagiste , et Vincent Renard , économiste , sur La Forêt des délaissés a constitué une étape fondamentale dans la constitution de mon corpus théorique .
Entity Name (Type) :: La Forêt des délaissés (MISC)


********** Emotions Features **********
***************************************

2. Sentence: Le travail avec Gilles Clément , paysagiste , et Vincent Renard , économiste , sur La Forêt des délaissés a constitué une étape fondamentale dans la constitution de mon corpus théorique .
Tokens having emotion: [ délaissés ]
Lemmas having emotion: [ délaisser ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

2. Sentence: Le travail avec Gilles Clément , paysagiste , et Vincent Renard , économiste , sur La Forêt des délaissés a constitué une étape fondamentale dans la constitution de mon corpus théorique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ travail ]
appreciatif :: 1 :: [ travail ]
marqueurs_niveau3 :: 1 :: [ travail ]
appreciatif_niveau3 :: 1 :: [ travail ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

2. Sentence: Le travail avec Gilles Clément , paysagiste , et Vincent Renard , économiste , sur La Forêt des délaissés a constitué une étape fondamentale dans la constitution de mon corpus théorique .
No features found.

