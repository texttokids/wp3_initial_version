
********** Entity Names Features **********
*******************************************

1. Sentence: ils ont récemment créé un zonage de masse où l ' accessibilité se fait en voiture , faute d ' investissements suffisants sur les transports en commun , et la classe moyenne , qui compte pour l ' instant quelques centaines de millions d ' habitants , souhaite de plus en plus accéder à ce mode de vie .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: ils ont récemment créé un zonage de masse où l ' accessibilité se fait en voiture , faute d ' investissements suffisants sur les transports en commun , et la classe moyenne , qui compte pour l ' instant quelques centaines de millions d ' habitants , souhaite de plus en plus accéder à ce mode de vie .
Tokens having emotion: [ faute, suffisants, souhaite ]
Lemmas having emotion: [ faute, suffisant, souhaiter ]
Categories of the emotion lemmas: [ culpabilite, orgueil, desir ]


********** Modality Features **********
***************************************

1. Sentence: ils ont récemment créé un zonage de masse où l ' accessibilité se fait en voiture , faute d ' investissements suffisants sur les transports en commun , et la classe moyenne , qui compte pour l ' instant quelques centaines de millions d ' habitants , souhaite de plus en plus accéder à ce mode de vie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ faute, suffisant ]
appreciatif :: 1 :: [ faute ]
axiologique :: 1 :: [ faute ]
pas_d_indication :: 1 :: [ suffisant ]
marqueurs_niveau2 :: 1 :: [ faute ]
marqueurs_niveau0 :: 1 :: [ suffisant ]
appreciatif_niveau2 :: 1 :: [ faute ]
axiologique_niveau2 :: 1 :: [ faute ]
pas_d_indication_niveau0 :: 1 :: [ suffisant ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: ils ont récemment créé un zonage de masse où l ' accessibilité se fait en voiture , faute d ' investissements suffisants sur les transports en commun , et la classe moyenne , qui compte pour l ' instant quelques centaines de millions d ' habitants , souhaite de plus en plus accéder à ce mode de vie .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['récemment']
adverbiaux_duratifs_iteratifs::1::['récemment']
adverbiaux_loc_temp_regionalisation_id::1::['récemment']

