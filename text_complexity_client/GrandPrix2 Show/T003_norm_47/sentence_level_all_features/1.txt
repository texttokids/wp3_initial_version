
********** Entity Names Features **********
*******************************************

1. Sentence: Nous avons été formés par des gens qui s ' étaient eux-mêmes formés en accompagnant des chantiers urbains des années soixante-dix , quand on ne parlait pas encore de « projets urbains » mais que l ' on produisait une nécessaire et intense réflexion sur la ville contemporaine en essayant de sortir des oppositions tranchées entre différentes théories de la forme urbaine , entre les partisans d ' une radicalité revisitée des Modernes et ceux d ' un retour vers les formes héritées de l ' histoire .
Entity Name (Type) :: Modernes (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Nous avons été formés par des gens qui s ' étaient eux-mêmes formés en accompagnant des chantiers urbains des années soixante-dix , quand on ne parlait pas encore de « projets urbains » mais que l ' on produisait une nécessaire et intense réflexion sur la ville contemporaine en essayant de sortir des oppositions tranchées entre différentes théories de la forme urbaine , entre les partisans d ' une radicalité revisitée des Modernes et ceux d ' un retour vers les formes héritées de l ' histoire .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Nous avons été formés par des gens qui s ' étaient eux-mêmes formés en accompagnant des chantiers urbains des années soixante-dix , quand on ne parlait pas encore de « projets urbains » mais que l ' on produisait une nécessaire et intense réflexion sur la ville contemporaine en essayant de sortir des oppositions tranchées entre différentes théories de la forme urbaine , entre les partisans d ' une radicalité revisitée des Modernes et ceux d ' un retour vers les formes héritées de l ' histoire .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ nécessaire, essayer ]
marqueurs_niveau3 :: 2 :: [ nécessaire, essayer ]
alethique :: 1 :: [ nécessaire ]
boulique :: 1 :: [ essayer ]
epistemique :: 1 :: [ essayer ]
alethique_niveau3 :: 1 :: [ nécessaire ]
boulique_niveau3 :: 1 :: [ essayer ]
epistemique_niveau3 :: 1 :: [ essayer ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Nous avons été formés par des gens qui s ' étaient eux-mêmes formés en accompagnant des chantiers urbains des années soixante-dix , quand on ne parlait pas encore de « projets urbains » mais que l ' on produisait une nécessaire et intense réflexion sur la ville contemporaine en essayant de sortir des oppositions tranchées entre différentes théories de la forme urbaine , entre les partisans d ' une radicalité revisitée des Modernes et ceux d ' un retour vers les formes héritées de l ' histoire .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['des années soixante-dix', 'pas encore']
adverbiaux_localisation_temporelle::1::['des années soixante-dix']
adverbiaux_duratifs_iteratifs::1::['pas encore']
adverbiaux_pointage_absolu::1::['des années soixante-dix']
adverbiaux_pointage_non_absolu::1::['pas encore']
adverbiaux_loc_temp_focalisation_id::1::['des années soixante-dix']
adverbiaux_loc_temp_regionalisation_id::1::['des années soixante-dix']
adverbiaux_loc_temp_pointage_absolu::1::['des années soixante-dix']
adverbiaux_dur_iter_relatif::1::['pas encore']

