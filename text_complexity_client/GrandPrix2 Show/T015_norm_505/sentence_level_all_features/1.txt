
********** Entity Names Features **********
*******************************************

1. Sentence: Il faudrait imaginer une «loi Lisière» qui s ' appuierait sur la loi de 2005 visant à la protection des espaces agricoles et naturels , et qui prenne modèle sur la loi Littoral .
Entity Name (Type) :: loi Littoral (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Il faudrait imaginer une «loi Lisière» qui s ' appuierait sur la loi de 2005 visant à la protection des espaces agricoles et naturels , et qui prenne modèle sur la loi Littoral .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il faudrait imaginer une «loi Lisière» qui s ' appuierait sur la loi de 2005 visant à la protection des espaces agricoles et naturels , et qui prenne modèle sur la loi Littoral .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ falloir, imaginer, loi, loi, loi ]
deontique :: 4 :: [ falloir, loi, loi, loi ]
marqueurs_niveau2 :: 3 :: [ loi, loi, loi ]
deontique_niveau2 :: 3 :: [ loi, loi, loi ]
marqueurs_niveau3 :: 2 :: [ falloir, imaginer ]
epistemique :: 1 :: [ imaginer ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau3 :: 1 :: [ imaginer ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il faudrait imaginer une «loi Lisière» qui s ' appuierait sur la loi de 2005 visant à la protection des espaces agricoles et naturels , et qui prenne modèle sur la loi Littoral .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 2005']
adverbiaux_localisation_temporelle::1::['de 2005']
adverbiaux_pointage_absolu::1::['de 2005']
adverbiaux_loc_temp_focalisation_id::1::['de 2005']
adverbiaux_loc_temp_regionalisation_id::1::['de 2005']
adverbiaux_loc_temp_pointage_absolu::1::['de 2005']

