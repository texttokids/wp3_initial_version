====== Sentence Level Features ======

1. Sentence: À travers la notion de possibilité , les deux épistémologies , ancrées dans le présent et dans le futur , dans le temps court et dans le temps long , peuvent trouver des points de contact qui mettent en tension les attentes sociales et individuelles , les nouvelles conditions urbaines et territoriales , les contradictions et les paradoxes qui les alimentent .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possibilité, long, pouvoir ]
alethique :: 2 :: [ possibilité, pouvoir ]
epistemique :: 2 :: [ long, pouvoir ]
marqueurs_niveau3 :: 2 :: [ possibilité, pouvoir ]
alethique_niveau3 :: 2 :: [ possibilité, pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ long ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 16


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possibilité, long, pouvoir ]
alethique :: 2 :: [ possibilité, pouvoir ]
epistemique :: 2 :: [ long, pouvoir ]
marqueurs_niveau3 :: 2 :: [ possibilité, pouvoir ]
alethique_niveau3 :: 2 :: [ possibilité, pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ long ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 16

