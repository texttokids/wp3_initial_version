
********** Entity Names Features **********
*******************************************

1. Sentence: Dans le présent , émergent à la fois les continuités avec le passé récent ou lointain et des points de rupture annoncés et parfois à peine survenus .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Dans le présent , émergent à la fois les continuités avec le passé récent ou lointain et des points de rupture annoncés et parfois à peine survenus .
Tokens having emotion: [ peine ]
Lemmas having emotion: [ peine ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Dans le présent , émergent à la fois les continuités avec le passé récent ou lointain et des points de rupture annoncés et parfois à peine survenus .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ rupture, parfois, peine ]
alethique :: 1 :: [ parfois ]
appreciatif :: 1 :: [ peine ]
pas_d_indication :: 1 :: [ rupture ]
marqueurs_niveau3 :: 1 :: [ parfois ]
marqueurs_niveau2 :: 1 :: [ peine ]
marqueurs_niveau0 :: 1 :: [ rupture ]
alethique_niveau3 :: 1 :: [ parfois ]
appreciatif_niveau2 :: 1 :: [ peine ]
pas_d_indication_niveau0 :: 1 :: [ rupture ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans le présent , émergent à la fois les continuités avec le passé récent ou lointain et des points de rupture annoncés et parfois à peine survenus .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['parfois']
adverbiaux_purement_iteratifs::1::['parfois']
adverbiaux_iterateur_frequentiel::1::['parfois']

