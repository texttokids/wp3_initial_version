
********** Entity Names Features **********
*******************************************

1. Sentence: » DU RÔLE DE L ' ARCHITECTE Avec Jean-Pierre Matton nous avons alors beaucoup travaillé pour rechercher (avec les Villes nouvelles) les meilleurs sites pour implanter les Universités de Marne-la-Vallée , Cergy-Pontoise , Versailles-SaintQuentin-en-Yvelines , Évry… avec le résultat presque toujours affligeant des opérations d ' ensemble conduites dans notre pays par les promoteurs-développeurs privés .
Entity Name (Type) :: Évry (LOC)


********** Emotions Features **********
***************************************

1. Sentence: » DU RÔLE DE L ' ARCHITECTE Avec Jean-Pierre Matton nous avons alors beaucoup travaillé pour rechercher (avec les Villes nouvelles) les meilleurs sites pour implanter les Universités de Marne-la-Vallée , Cergy-Pontoise , Versailles-SaintQuentin-en-Yvelines , Évry… avec le résultat presque toujours affligeant des opérations d ' ensemble conduites dans notre pays par les promoteurs-développeurs privés .
Tokens having emotion: [ affligeant ]
Lemmas having emotion: [ affliger ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: » DU RÔLE DE L ' ARCHITECTE Avec Jean-Pierre Matton nous avons alors beaucoup travaillé pour rechercher (avec les Villes nouvelles) les meilleurs sites pour implanter les Universités de Marne-la-Vallée , Cergy-Pontoise , Versailles-SaintQuentin-en-Yvelines , Évry… avec le résultat presque toujours affligeant des opérations d ' ensemble conduites dans notre pays par les promoteurs-développeurs privés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ beaucoup, travailler, meilleur, toujours ]
appreciatif :: 2 :: [ travailler, meilleur ]
marqueurs_niveau2 :: 2 :: [ travailler, meilleur ]
appreciatif_niveau2 :: 2 :: [ travailler, meilleur ]
alethique :: 1 :: [ toujours ]
axiologique :: 1 :: [ meilleur ]
epistemique :: 1 :: [ beaucoup ]
marqueurs_niveau3 :: 1 :: [ toujours ]
marqueurs_niveau1 :: 1 :: [ beaucoup ]
alethique_niveau3 :: 1 :: [ toujours ]
axiologique_niveau2 :: 1 :: [ meilleur ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: » DU RÔLE DE L ' ARCHITECTE Avec Jean-Pierre Matton nous avons alors beaucoup travaillé pour rechercher (avec les Villes nouvelles) les meilleurs sites pour implanter les Universités de Marne-la-Vallée , Cergy-Pontoise , Versailles-SaintQuentin-en-Yvelines , Évry… avec le résultat presque toujours affligeant des opérations d ' ensemble conduites dans notre pays par les promoteurs-développeurs privés .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

