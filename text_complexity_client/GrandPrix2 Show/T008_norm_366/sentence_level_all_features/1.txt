
********** Entity Names Features **********
*******************************************

1. Sentence: Autrement dit , si l ' on a souvent dénoncé la rigidité des zonings de la planification urbaine européenne , on voit aujourd ' hui , sous une inspiration libérale et sans plan d ' urbanisme , se fabriquer des grands ensembles encore bien plus rigides .
Entity Name (Type) :: Autrement (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Autrement dit , si l ' on a souvent dénoncé la rigidité des zonings de la planification urbaine européenne , on voit aujourd ' hui , sous une inspiration libérale et sans plan d ' urbanisme , se fabriquer des grands ensembles encore bien plus rigides .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Autrement dit , si l ' on a souvent dénoncé la rigidité des zonings de la planification urbaine européenne , on voit aujourd ' hui , sous une inspiration libérale et sans plan d ' urbanisme , se fabriquer des grands ensembles encore bien plus rigides .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ dire, souvent, dénoncer, grand, bien ]
marqueurs_niveau3 :: 4 :: [ dire, souvent, grand, bien ]
axiologique :: 2 :: [ dénoncer, bien ]
alethique :: 1 :: [ souvent ]
appreciatif :: 1 :: [ bien ]
boulique :: 1 :: [ dire ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau1 :: 1 :: [ dénoncer ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
axiologique_niveau1 :: 1 :: [ dénoncer ]
boulique_niveau3 :: 1 :: [ dire ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Autrement dit , si l ' on a souvent dénoncé la rigidité des zonings de la planification urbaine européenne , on voit aujourd ' hui , sous une inspiration libérale et sans plan d ' urbanisme , se fabriquer des grands ensembles encore bien plus rigides .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['souvent', 'encore']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_iterateur_frequentiel::1::['souvent']
adverbiaux_dur_iter_relatif::1::['encore']

