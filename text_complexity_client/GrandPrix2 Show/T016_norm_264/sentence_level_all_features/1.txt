
********** Entity Names Features **********
*******************************************

1. Sentence: Imaginer un autre avenir Intégrer la longue durée dans la réflexion du projet réintroduit la possibilité d ' une pensée qui non seulement «pense l ' impensable 3» , mais qui soumet à la critique tout l ' édifice culturel , économique et social dans lequel l ' exercice est réalisé .
Entity Name (Type) :: Intégrer (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Imaginer un autre avenir Intégrer la longue durée dans la réflexion du projet réintroduit la possibilité d ' une pensée qui non seulement «pense l ' impensable 3» , mais qui soumet à la critique tout l ' édifice culturel , économique et social dans lequel l ' exercice est réalisé .
Tokens having emotion: [ critique ]
Lemmas having emotion: [ critique ]
Categories of the emotion lemmas: [ mepris ]


********** Modality Features **********
***************************************

1. Sentence: Imaginer un autre avenir Intégrer la longue durée dans la réflexion du projet réintroduit la possibilité d ' une pensée qui non seulement «pense l ' impensable 3» , mais qui soumet à la critique tout l ' édifice culturel , économique et social dans lequel l ' exercice est réalisé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ imaginer, long, possibilité, penser, réaliser ]
epistemique :: 3 :: [ imaginer, long, penser ]
marqueurs_niveau3 :: 3 :: [ imaginer, possibilité, penser ]
alethique :: 2 :: [ possibilité, réaliser ]
epistemique_niveau3 :: 2 :: [ imaginer, penser ]
marqueurs_niveau2 :: 1 :: [ long ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
alethique_niveau3 :: 1 :: [ possibilité ]
alethique_niveau1 :: 1 :: [ réaliser ]
epistemique_niveau2 :: 1 :: [ long ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Imaginer un autre avenir Intégrer la longue durée dans la réflexion du projet réintroduit la possibilité d ' une pensée qui non seulement «pense l ' impensable 3» , mais qui soumet à la critique tout l ' édifice culturel , économique et social dans lequel l ' exercice est réalisé .
No features found.

