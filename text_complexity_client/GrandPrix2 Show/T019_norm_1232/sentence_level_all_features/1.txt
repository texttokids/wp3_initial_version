
********** Entity Names Features **********
*******************************************

1. Sentence: L ' essentiel est la capacité à se projeter dans l ' avenir , à avoir confiance .
Entity Name (Type) :: L (MISC)


********** Emotions Features **********
***************************************

1. Sentence: L ' essentiel est la capacité à se projeter dans l ' avenir , à avoir confiance .
Tokens having emotion: [ confiance ]
Lemmas having emotion: [ confiance ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: L ' essentiel est la capacité à se projeter dans l ' avenir , à avoir confiance .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ confiance ]
epistemique :: 1 :: [ confiance ]
marqueurs_niveau2 :: 1 :: [ confiance ]
epistemique_niveau2 :: 1 :: [ confiance ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' essentiel est la capacité à se projeter dans l ' avenir , à avoir confiance .
No features found.

