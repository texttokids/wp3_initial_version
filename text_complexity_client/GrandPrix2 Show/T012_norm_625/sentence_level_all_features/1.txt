
********** Entity Names Features **********
*******************************************

1. Sentence: Comment nos opérations d ' intérêt national pourront-elles contribuer réellement à la stratégie de l ' Île-de-France ?
Entity Name (Type) :: Île-de-France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Comment nos opérations d ' intérêt national pourront-elles contribuer réellement à la stratégie de l ' Île-de-France ?
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Comment nos opérations d ' intérêt national pourront-elles contribuer réellement à la stratégie de l ' Île-de-France ?
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ intérêt, pouvoir ]
alethique :: 1 :: [ pouvoir ]
boulique :: 1 :: [ intérêt ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ intérêt ]
alethique_niveau3 :: 1 :: [ pouvoir ]
boulique_niveau2 :: 1 :: [ intérêt ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Comment nos opérations d ' intérêt national pourront-elles contribuer réellement à la stratégie de l ' Île-de-France ?
No features found.

