====== Sentence Level Features ======

1. Sentence: Voudrait-on retrouver la ville , discipliner ses vides , voudrait-on — et il le faut bien — suivre Philippe Panerai quand il prétend qu ' on peut civiliser les grands ensembles , adoucir ce qu ' ils ont de raide et les apprivoiser , il n ' empêche que le rêve qu ' ont caressé les architectes (de Wright à Archigram , chez Fuller et Le Corbusier) n ' a pas été celui d ' une consolidation , d ' une installation de la ville à l ' échelle des conurbations que la vitesse autorisait , mais plutôt un projet d ' abandon , un désir forcené d ' occuper la nature et de s ' en rapprocher .
Tokens having emotion: [ désir ]
Lemmas having emotion: [ désir ]
Categories of the emotion lemmas: [ desir ]


====== Text Level Features ======

Tokens having emotion: [ désir ]
Lemmas having emotion: [ désir ]
Category of the emotion lemmas: [ desir ]
Total emotion lemma counts: 1

