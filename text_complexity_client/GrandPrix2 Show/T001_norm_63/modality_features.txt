====== Sentence Level Features ======

1. Sentence: Voudrait-on retrouver la ville , discipliner ses vides , voudrait-on — et il le faut bien — suivre Philippe Panerai quand il prétend qu ' on peut civiliser les grands ensembles , adoucir ce qu ' ils ont de raide et les apprivoiser , il n ' empêche que le rêve qu ' ont caressé les architectes (de Wright à Archigram , chez Fuller et Le Corbusier) n ' a pas été celui d ' une consolidation , d ' une installation de la ville à l ' échelle des conurbations que la vitesse autorisait , mais plutôt un projet d ' abandon , un désir forcené d ' occuper la nature et de s ' en rapprocher .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ vouloir, vouloir, falloir, bien, pouvoir, grand, empêcher, rêve, désir ]
marqueurs_niveau3 :: 7 :: [ vouloir, vouloir, falloir, bien, pouvoir, grand, désir ]
boulique :: 3 :: [ vouloir, vouloir, désir ]
boulique_niveau3 :: 3 :: [ vouloir, vouloir, désir ]
alethique :: 2 :: [ pouvoir, rêve ]
appreciatif :: 2 :: [ bien, rêve ]
deontique :: 2 :: [ falloir, pouvoir ]
epistemique :: 2 :: [ pouvoir, grand ]
deontique_niveau3 :: 2 :: [ falloir, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, grand ]
axiologique :: 1 :: [ bien ]
pas_d_indication :: 1 :: [ empêcher ]
marqueurs_niveau1 :: 1 :: [ rêve ]
marqueurs_niveau0 :: 1 :: [ empêcher ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau1 :: 1 :: [ rêve ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau1 :: 1 :: [ rêve ]
axiologique_niveau3 :: 1 :: [ bien ]
pas_d_indication_niveau0 :: 1 :: [ empêcher ]
Total feature counts: 44


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ vouloir, vouloir, falloir, bien, pouvoir, grand, empêcher, rêve, désir ]
marqueurs_niveau3 :: 7 :: [ vouloir, vouloir, falloir, bien, pouvoir, grand, désir ]
boulique :: 3 :: [ vouloir, vouloir, désir ]
boulique_niveau3 :: 3 :: [ vouloir, vouloir, désir ]
alethique :: 2 :: [ pouvoir, rêve ]
appreciatif :: 2 :: [ bien, rêve ]
deontique :: 2 :: [ falloir, pouvoir ]
epistemique :: 2 :: [ pouvoir, grand ]
deontique_niveau3 :: 2 :: [ falloir, pouvoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, grand ]
axiologique :: 1 :: [ bien ]
pas_d_indication :: 1 :: [ empêcher ]
marqueurs_niveau1 :: 1 :: [ rêve ]
marqueurs_niveau0 :: 1 :: [ empêcher ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau1 :: 1 :: [ rêve ]
appreciatif_niveau3 :: 1 :: [ bien ]
appreciatif_niveau1 :: 1 :: [ rêve ]
axiologique_niveau3 :: 1 :: [ bien ]
pas_d_indication_niveau0 :: 1 :: [ empêcher ]
Total feature counts: 44

