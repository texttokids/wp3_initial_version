
********** Entity Names Features **********
*******************************************

1. Sentence: Ces sites permettent de répondre , avec un coût d ' investissement raisonnable , à la très forte demande des Français pour l ' habitat individuel et d ' assurer un rapport plus facile à la nature , à une ﬁlière agroalimentaire de proximité et à des activités de loisirs .
Entity Name (Type) :: Français (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Ces sites permettent de répondre , avec un coût d ' investissement raisonnable , à la très forte demande des Français pour l ' habitat individuel et d ' assurer un rapport plus facile à la nature , à une ﬁlière agroalimentaire de proximité et à des activités de loisirs .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ces sites permettent de répondre , avec un coût d ' investissement raisonnable , à la très forte demande des Français pour l ' habitat individuel et d ' assurer un rapport plus facile à la nature , à une ﬁlière agroalimentaire de proximité et à des activités de loisirs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ permettre, fort, facile, loisir ]
appreciatif :: 2 :: [ facile, loisir ]
marqueurs_niveau2 :: 2 :: [ facile, loisir ]
appreciatif_niveau2 :: 2 :: [ facile, loisir ]
deontique :: 1 :: [ permettre ]
epistemique :: 1 :: [ facile ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau3 :: 1 :: [ permettre ]
marqueurs_niveau0 :: 1 :: [ fort ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau2 :: 1 :: [ facile ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ces sites permettent de répondre , avec un coût d ' investissement raisonnable , à la très forte demande des Français pour l ' habitat individuel et d ' assurer un rapport plus facile à la nature , à une ﬁlière agroalimentaire de proximité et à des activités de loisirs .
No features found.

