
********** Entity Names Features **********
*******************************************

1. Sentence: Dans mes lectures , j ' aime bien découvrir un domaine .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Dans mes lectures , j ' aime bien découvrir un domaine .
Tokens having emotion: [ aime ]
Lemmas having emotion: [ aimer ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: Dans mes lectures , j ' aime bien découvrir un domaine .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ aimer, bien, découvrir ]
appreciatif :: 2 :: [ aimer, bien ]
marqueurs_niveau3 :: 2 :: [ aimer, bien ]
appreciatif_niveau3 :: 2 :: [ aimer, bien ]
axiologique :: 1 :: [ bien ]
pas_d_indication :: 1 :: [ découvrir ]
marqueurs_niveau0 :: 1 :: [ découvrir ]
axiologique_niveau3 :: 1 :: [ bien ]
pas_d_indication_niveau0 :: 1 :: [ découvrir ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Dans mes lectures , j ' aime bien découvrir un domaine .
No features found.

