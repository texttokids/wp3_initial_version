
********** Entity Names Features **********
*******************************************

1. Sentence: Convictions , passage à l ' acte , confrontation au réel et prise de risque S ' engager dans des projets , changer de thématique , voguer dans des milieux professionnels et scientifiques divers expose nécessairement à l ' erreur et à la critique .
Entity Name (Type) :: S (PER)


********** Emotions Features **********
***************************************

1. Sentence: Convictions , passage à l ' acte , confrontation au réel et prise de risque S ' engager dans des projets , changer de thématique , voguer dans des milieux professionnels et scientifiques divers expose nécessairement à l ' erreur et à la critique .
Tokens having emotion: [ critique ]
Lemmas having emotion: [ critique ]
Categories of the emotion lemmas: [ mepris ]


********** Modality Features **********
***************************************

1. Sentence: Convictions , passage à l ' acte , confrontation au réel et prise de risque S ' engager dans des projets , changer de thématique , voguer dans des milieux professionnels et scientifiques divers expose nécessairement à l ' erreur et à la critique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ nécessairement ]
alethique :: 1 :: [ nécessairement ]
marqueurs_niveau3 :: 1 :: [ nécessairement ]
alethique_niveau3 :: 1 :: [ nécessairement ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Convictions , passage à l ' acte , confrontation au réel et prise de risque S ' engager dans des projets , changer de thématique , voguer dans des milieux professionnels et scientifiques divers expose nécessairement à l ' erreur et à la critique .
No features found.

