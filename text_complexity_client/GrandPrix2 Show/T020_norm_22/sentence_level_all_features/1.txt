
********** Entity Names Features **********
*******************************************

1. Sentence: Ce qui nous intéressait à Hanoï , c ' était de décrypter comment , sous l ' effet des nouvelles politiques , la ville que nous voyions se développer était produite par trois forces dont il nous semblait qu ' elles étaient d ' égale importance .
Entity Name (Type) :: Hanoï (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ce qui nous intéressait à Hanoï , c ' était de décrypter comment , sous l ' effet des nouvelles politiques , la ville que nous voyions se développer était produite par trois forces dont il nous semblait qu ' elles étaient d ' égale importance .
Tokens having emotion: [ intéressait ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Ce qui nous intéressait à Hanoï , c ' était de décrypter comment , sous l ' effet des nouvelles politiques , la ville que nous voyions se développer était produite par trois forces dont il nous semblait qu ' elles étaient d ' égale importance .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ intéresser, force, sembler ]
pas_d_indication :: 2 :: [ intéresser, force ]
marqueurs_niveau0 :: 2 :: [ intéresser, force ]
pas_d_indication_niveau0 :: 2 :: [ intéresser, force ]
epistemique :: 1 :: [ sembler ]
marqueurs_niveau3 :: 1 :: [ sembler ]
epistemique_niveau3 :: 1 :: [ sembler ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ce qui nous intéressait à Hanoï , c ' était de décrypter comment , sous l ' effet des nouvelles politiques , la ville que nous voyions se développer était produite par trois forces dont il nous semblait qu ' elles étaient d ' égale importance .
No features found.

