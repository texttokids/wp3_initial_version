====== Sentence Level Features ======

1. Sentence: Saint-Marc est le quartier balnéaire de Saint-Nazaire où l ' estuaire s ' élargit vers le Grand large , une plage plein sud — un lieu délicat , nourri de tendresse et de nostalgie , car c ' est la plage des « Vacances de Monsieur Hulot » .
Tokens having emotion: [ tendresse, nostalgie ]
Lemmas having emotion: [ tendresse, nostalgie ]
Categories of the emotion lemmas: [ amour, tristesse ]


====== Text Level Features ======

Tokens having emotion: [ tendresse, nostalgie ]
Lemmas having emotion: [ tendresse, nostalgie ]
Category of the emotion lemmas: [ amour, tristesse ]
Total emotion lemma counts: 2

