
********** Entity Names Features **********
*******************************************

1. Sentence: La doctorante a joui d ' un terrain d ' étude concret qui lui a permis d ' influer directement sur la nature de l ' équipement ici créé .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La doctorante a joui d ' un terrain d ' étude concret qui lui a permis d ' influer directement sur la nature de l ' équipement ici créé .
Tokens having emotion: [ joui ]
Lemmas having emotion: [ jouir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: La doctorante a joui d ' un terrain d ' étude concret qui lui a permis d ' influer directement sur la nature de l ' équipement ici créé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ lui, permettre ]
marqueurs_niveau3 :: 2 :: [ lui, permettre ]
alethique :: 1 :: [ lui ]
deontique :: 1 :: [ permettre ]
epistemique :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La doctorante a joui d ' un terrain d ' étude concret qui lui a permis d ' influer directement sur la nature de l ' équipement ici créé .
No features found.

