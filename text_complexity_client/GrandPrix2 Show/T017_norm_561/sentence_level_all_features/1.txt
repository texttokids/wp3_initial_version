
********** Entity Names Features **********
*******************************************

1. Sentence: Protéger le patrimoine c ' est l ' habiter  : un acte plus efficace qu ' une protection juridique comme l ' a défendu Camillo Boito 5 au xixe siècle , révélé en France par Françoise Choay 6 .
Entity Name (Type) :: Françoise Choay (PER)


********** Emotions Features **********
***************************************

1. Sentence: Protéger le patrimoine c ' est l ' habiter  : un acte plus efficace qu ' une protection juridique comme l ' a défendu Camillo Boito 5 au xixe siècle , révélé en France par Françoise Choay 6 .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Protéger le patrimoine c ' est l ' habiter  : un acte plus efficace qu ' une protection juridique comme l ' a défendu Camillo Boito 5 au xixe siècle , révélé en France par Françoise Choay 6 .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ protéger, défendre ]
axiologique :: 1 :: [ défendre ]
pas_d_indication :: 1 :: [ protéger ]
marqueurs_niveau1 :: 1 :: [ défendre ]
marqueurs_niveau0 :: 1 :: [ protéger ]
axiologique_niveau1 :: 1 :: [ défendre ]
pas_d_indication_niveau0 :: 1 :: [ protéger ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Protéger le patrimoine c ' est l ' habiter  : un acte plus efficace qu ' une protection juridique comme l ' a défendu Camillo Boito 5 au xixe siècle , révélé en France par Françoise Choay 6 .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['au xixe siècle']
adverbiaux_localisation_temporelle::1::['au xixe siècle']
adverbiaux_pointage_absolu::1::['au xixe siècle']
adverbiaux_loc_temp_focalisation_id::1::['au xixe siècle']
adverbiaux_loc_temp_regionalisation_id::1::['au xixe siècle']
adverbiaux_loc_temp_pointage_absolu::1::['au xixe siècle']

