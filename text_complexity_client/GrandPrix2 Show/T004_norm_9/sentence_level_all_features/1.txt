
********** Entity Names Features **********
*******************************************

1. Sentence: À ce moment , j ' ai compris que notre métier relevait davantage de la transformation que de l ' invention , et que la conception à partir du contexte était décisive : « l ' architecte n ' invente pas , il transforme » dit Alvaro Siza .
Entity Name (Type) :: Alvaro Siza (PER)


********** Emotions Features **********
***************************************

1. Sentence: À ce moment , j ' ai compris que notre métier relevait davantage de la transformation que de l ' invention , et que la conception à partir du contexte était décisive : « l ' architecte n ' invente pas , il transforme » dit Alvaro Siza .
No features found.


********** Modality Features **********
***************************************

1. Sentence: À ce moment , j ' ai compris que notre métier relevait davantage de la transformation que de l ' invention , et que la conception à partir du contexte était décisive : « l ' architecte n ' invente pas , il transforme » dit Alvaro Siza .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ partir, inventer, dire ]
alethique :: 1 :: [ partir ]
boulique :: 1 :: [ dire ]
epistemique :: 1 :: [ inventer ]
marqueurs_niveau3 :: 1 :: [ dire ]
marqueurs_niveau2 :: 1 :: [ inventer ]
marqueurs_niveau1 :: 1 :: [ partir ]
alethique_niveau1 :: 1 :: [ partir ]
boulique_niveau3 :: 1 :: [ dire ]
epistemique_niveau2 :: 1 :: [ inventer ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À ce moment , j ' ai compris que notre métier relevait davantage de la transformation que de l ' invention , et que la conception à partir du contexte était décisive : « l ' architecte n ' invente pas , il transforme » dit Alvaro Siza .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['À ce moment']
adverbiaux_localisation_temporelle::1::['À ce moment']
adverbiaux_pointage_non_absolu::2::['À ce moment', 'À ce moment']
adverbiaux_loc_temp_focalisation_id::1::['À ce moment']
adverbiaux_loc_temp_regionalisation_id::1::['À ce moment']
adverbiaux_loc_temp_pointage_non_absolu::1::['À ce moment']
adverbiaux_dur_iter_anaphorique::1::['À ce moment']

