====== Sentence Level Features ======

1. Sentence: Si le sol de la ville suscite les sensations du passant , sa curiosité , son désir de déambulation ou de halte , le ciel , lui , permet l ' envol du regard ,  l ' échappée lointaine et la possibilité de se projeter , se repérer et se reconnaître dans la ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ désir, lui, permettre, possibilité ]
marqueurs_niveau3 :: 4 :: [ désir, lui, permettre, possibilité ]
alethique :: 2 :: [ lui, possibilité ]
alethique_niveau3 :: 2 :: [ lui, possibilité ]
boulique :: 1 :: [ désir ]
deontique :: 1 :: [ permettre ]
epistemique :: 1 :: [ lui ]
boulique_niveau3 :: 1 :: [ désir ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 18


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ désir, lui, permettre, possibilité ]
marqueurs_niveau3 :: 4 :: [ désir, lui, permettre, possibilité ]
alethique :: 2 :: [ lui, possibilité ]
alethique_niveau3 :: 2 :: [ lui, possibilité ]
boulique :: 1 :: [ désir ]
deontique :: 1 :: [ permettre ]
epistemique :: 1 :: [ lui ]
boulique_niveau3 :: 1 :: [ désir ]
deontique_niveau3 :: 1 :: [ permettre ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 18

