====== Sentence Level Features ======

1. Sentence: Si le sol de la ville suscite les sensations du passant , sa curiosité , son désir de déambulation ou de halte , le ciel , lui , permet l ' envol du regard ,  l ' échappée lointaine et la possibilité de se projeter , se repérer et se reconnaître dans la ville .
Tokens having emotion: [ curiosité, désir ]
Lemmas having emotion: [ curiosité, désir ]
Categories of the emotion lemmas: [ desir, desir ]


====== Text Level Features ======

Tokens having emotion: [ curiosité, désir ]
Lemmas having emotion: [ curiosité, désir ]
Category of the emotion lemmas: [ desir, desir ]
Total emotion lemma counts: 2

