
********** Entity Names Features **********
*******************************************

1. Sentence: Pour moi , il s ' agissait de profiter de ce vide , non pas pour le remplir de constructions et fusionner tout le territoire urbain – ce qui était l ' idée retenue – mais pour offrir là une perception de cette ville duelle , du territoire urbain à grande échelle , tout en faisant une entrée de ville .
Entity Name (Type) :: Pour (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Pour moi , il s ' agissait de profiter de ce vide , non pas pour le remplir de constructions et fusionner tout le territoire urbain – ce qui était l ' idée retenue – mais pour offrir là une perception de cette ville duelle , du territoire urbain à grande échelle , tout en faisant une entrée de ville .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Pour moi , il s ' agissait de profiter de ce vide , non pas pour le remplir de constructions et fusionner tout le territoire urbain – ce qui était l ' idée retenue – mais pour offrir là une perception de cette ville duelle , du territoire urbain à grande échelle , tout en faisant une entrée de ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ lui, offrir, grand ]
epistemique :: 2 :: [ lui, grand ]
marqueurs_niveau3 :: 2 :: [ lui, grand ]
epistemique_niveau3 :: 2 :: [ lui, grand ]
alethique :: 1 :: [ lui ]
appreciatif :: 1 :: [ offrir ]
marqueurs_niveau2 :: 1 :: [ offrir ]
alethique_niveau3 :: 1 :: [ lui ]
appreciatif_niveau2 :: 1 :: [ offrir ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pour moi , il s ' agissait de profiter de ce vide , non pas pour le remplir de constructions et fusionner tout le territoire urbain – ce qui était l ' idée retenue – mais pour offrir là une perception de cette ville duelle , du territoire urbain à grande échelle , tout en faisant une entrée de ville .
No features found.

