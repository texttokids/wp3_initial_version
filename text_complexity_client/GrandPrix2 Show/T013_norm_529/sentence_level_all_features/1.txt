
********** Entity Names Features **********
*******************************************

1. Sentence: Quant à Laurent Davezies 4 , il a produit en amont de la conférence de 2005 une étude riche d ' enseignements : il a démontré que les deux rives de l ' estuaire se développent , aussi bien en termes d ' emploi que de croissance démographique , contrairement à l ' idée répandue selon laquelle la rive nord se serait développée plus rapidement que le pays de Retz .
Entity Name (Type) :: pays de Retz (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Quant à Laurent Davezies 4 , il a produit en amont de la conférence de 2005 une étude riche d ' enseignements : il a démontré que les deux rives de l ' estuaire se développent , aussi bien en termes d ' emploi que de croissance démographique , contrairement à l ' idée répandue selon laquelle la rive nord se serait développée plus rapidement que le pays de Retz .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Quant à Laurent Davezies 4 , il a produit en amont de la conférence de 2005 une étude riche d ' enseignements : il a démontré que les deux rives de l ' estuaire se développent , aussi bien en termes d ' emploi que de croissance démographique , contrairement à l ' idée répandue selon laquelle la rive nord se serait développée plus rapidement que le pays de Retz .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ riche, bien, emploi, selon, rapidement ]
appreciatif :: 3 :: [ riche, bien, emploi ]
marqueurs_niveau3 :: 3 :: [ bien, selon, rapidement ]
epistemique :: 2 :: [ selon, rapidement ]
marqueurs_niveau2 :: 2 :: [ riche, emploi ]
appreciatif_niveau2 :: 2 :: [ riche, emploi ]
epistemique_niveau3 :: 2 :: [ selon, rapidement ]
axiologique :: 1 :: [ bien ]
appreciatif_niveau3 :: 1 :: [ bien ]
axiologique_niveau3 :: 1 :: [ bien ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Quant à Laurent Davezies 4 , il a produit en amont de la conférence de 2005 une étude riche d ' enseignements : il a démontré que les deux rives de l ' estuaire se développent , aussi bien en termes d ' emploi que de croissance démographique , contrairement à l ' idée répandue selon laquelle la rive nord se serait développée plus rapidement que le pays de Retz .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 2005']
adverbiaux_localisation_temporelle::1::['de 2005']
adverbiaux_pointage_absolu::1::['de 2005']
adverbiaux_loc_temp_focalisation_id::1::['de 2005']
adverbiaux_loc_temp_regionalisation_id::1::['de 2005']
adverbiaux_loc_temp_pointage_absolu::1::['de 2005']

