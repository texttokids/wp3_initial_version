====== Sentence Level Features ======

1. Sentence: Si j ' aime l ' architecture de Rem Koolhaas , tout mon passé professionnel me portait à être en désaccord avec les thèses urbaines qu ' il développe : je ne crois pas que notre métier soit d ' exacerber les conflits , de créer le stress urbain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ aimer, désaccord, croire, conflit, stress ]
appreciatif :: 2 :: [ aimer, désaccord ]
axiologique :: 2 :: [ désaccord, conflit ]
marqueurs_niveau3 :: 2 :: [ aimer, croire ]
alethique :: 1 :: [ désaccord ]
epistemique :: 1 :: [ croire ]
pas_d_indication :: 1 :: [ stress ]
marqueurs_niveau2 :: 1 :: [ conflit ]
marqueurs_niveau1 :: 1 :: [ désaccord ]
marqueurs_niveau0 :: 1 :: [ stress ]
alethique_niveau1 :: 1 :: [ désaccord ]
appreciatif_niveau3 :: 1 :: [ aimer ]
appreciatif_niveau1 :: 1 :: [ désaccord ]
axiologique_niveau2 :: 1 :: [ conflit ]
axiologique_niveau1 :: 1 :: [ désaccord ]
epistemique_niveau3 :: 1 :: [ croire ]
pas_d_indication_niveau0 :: 1 :: [ stress ]
Total feature counts: 24


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ aimer, désaccord, croire, conflit, stress ]
appreciatif :: 2 :: [ aimer, désaccord ]
axiologique :: 2 :: [ désaccord, conflit ]
marqueurs_niveau3 :: 2 :: [ aimer, croire ]
alethique :: 1 :: [ désaccord ]
epistemique :: 1 :: [ croire ]
pas_d_indication :: 1 :: [ stress ]
marqueurs_niveau2 :: 1 :: [ conflit ]
marqueurs_niveau1 :: 1 :: [ désaccord ]
marqueurs_niveau0 :: 1 :: [ stress ]
alethique_niveau1 :: 1 :: [ désaccord ]
appreciatif_niveau3 :: 1 :: [ aimer ]
appreciatif_niveau1 :: 1 :: [ désaccord ]
axiologique_niveau2 :: 1 :: [ conflit ]
axiologique_niveau1 :: 1 :: [ désaccord ]
epistemique_niveau3 :: 1 :: [ croire ]
pas_d_indication_niveau0 :: 1 :: [ stress ]
Total feature counts: 24

