
********** Entity Names Features **********
*******************************************

1. Sentence: Or , il n ' est pas possible de « faire la ville » sans prendre en considération l ' évolution des modes de vie qui peuvent et doivent être intégrés dans tout projet volontariste d ' aménagement urbain .
Entity Name (Type) :: Or (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Or , il n ' est pas possible de « faire la ville » sans prendre en considération l ' évolution des modes de vie qui peuvent et doivent être intégrés dans tout projet volontariste d ' aménagement urbain .
Tokens having emotion: [ considération ]
Lemmas having emotion: [ considération ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Or , il n ' est pas possible de « faire la ville » sans prendre en considération l ' évolution des modes de vie qui peuvent et doivent être intégrés dans tout projet volontariste d ' aménagement urbain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ possible, pouvoir, devoir ]
alethique :: 3 :: [ possible, pouvoir, devoir ]
marqueurs_niveau3 :: 3 :: [ possible, pouvoir, devoir ]
alethique_niveau3 :: 3 :: [ possible, pouvoir, devoir ]
deontique :: 2 :: [ pouvoir, devoir ]
epistemique :: 2 :: [ pouvoir, devoir ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, devoir ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Or , il n ' est pas possible de « faire la ville » sans prendre en considération l ' évolution des modes de vie qui peuvent et doivent être intégrés dans tout projet volontariste d ' aménagement urbain .
No features found.

