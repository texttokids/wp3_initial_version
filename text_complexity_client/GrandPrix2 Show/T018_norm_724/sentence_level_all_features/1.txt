
********** Entity Names Features **********
*******************************************

1. Sentence: En 2000 , le Grand Projet de Ville a été lancé par la volonté du maire confronté à la désindustrialisation et au départ des habitants en périphérie .
Entity Name (Type) :: Grand Projet de Ville (MISC)


********** Emotions Features **********
***************************************

1. Sentence: En 2000 , le Grand Projet de Ville a été lancé par la volonté du maire confronté à la désindustrialisation et au départ des habitants en périphérie .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 2000 , le Grand Projet de Ville a été lancé par la volonté du maire confronté à la désindustrialisation et au départ des habitants en périphérie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ grand, volonté ]
marqueurs_niveau3 :: 2 :: [ grand, volonté ]
boulique :: 1 :: [ volonté ]
epistemique :: 1 :: [ grand ]
boulique_niveau3 :: 1 :: [ volonté ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 2000 , le Grand Projet de Ville a été lancé par la volonté du maire confronté à la désindustrialisation et au départ des habitants en périphérie .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['En 2000']
adverbiaux_localisation_temporelle::1::['En 2000']
adverbiaux_pointage_absolu::1::['En 2000']
adverbiaux_pointage_non_absolu::1::['En 2000']
adverbiaux_loc_temp_focalisation_id::1::['En 2000']
adverbiaux_loc_temp_regionalisation_id::1::['En 2000']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 2000']
adverbiaux_dur_iter_absolu::1::['En 2000']

