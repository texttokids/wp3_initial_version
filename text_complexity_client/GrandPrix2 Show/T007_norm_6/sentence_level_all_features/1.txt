
********** Entity Names Features **********
*******************************************

1. Sentence: Je redécouvris plus tard en tant qu ' architecte-conseil de l ' État l ' œuvre d ' Eugène Claudius-Petit , maire de Firminy et ministre de la Reconstruction .
Entity Name (Type) :: ministre de la Reconstruction (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Je redécouvris plus tard en tant qu ' architecte-conseil de l ' État l ' œuvre d ' Eugène Claudius-Petit , maire de Firminy et ministre de la Reconstruction .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Je redécouvris plus tard en tant qu ' architecte-conseil de l ' État l ' œuvre d ' Eugène Claudius-Petit , maire de Firminy et ministre de la Reconstruction .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je redécouvris plus tard en tant qu ' architecte-conseil de l ' État l ' œuvre d ' Eugène Claudius-Petit , maire de Firminy et ministre de la Reconstruction .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['plus tard']
adverbiaux_localisation_temporelle::1::['plus tard']
adverbiaux_pointage_non_absolu::1::['plus tard']
adverbiaux_loc_temp_regionalisation_id::1::['plus tard']
adverbiaux_loc_temp_pointage_non_absolu::1::['plus tard']

