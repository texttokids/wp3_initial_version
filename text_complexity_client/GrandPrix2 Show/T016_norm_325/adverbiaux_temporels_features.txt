====== Sentence Level Features ======

1. Sentence: L ' article de Bernardo Secchi de 1986 , «Progetto di suolo 1» , a eu des répercussions considérables en Italie et au-delà , en proposant une révision critique de la manière de concevoir la ville et en indiquant le vide comme son matériau principal : seul le «projet de sol» peut impliquer l ' espace urbain dans son intégralité .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 1986']
adverbiaux_localisation_temporelle::1::['de 1986']
adverbiaux_pointage_absolu::1::['de 1986']
adverbiaux_loc_temp_focalisation_id::1::['de 1986']
adverbiaux_loc_temp_regionalisation_id::1::['de 1986']
adverbiaux_loc_temp_pointage_absolu::1::['de 1986']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 1986']
adverbiaux_localisation_temporelle::1::['de 1986']
adverbiaux_pointage_absolu::1::['de 1986']
adverbiaux_loc_temp_focalisation_id::1::['de 1986']
adverbiaux_loc_temp_regionalisation_id::1::['de 1986']
adverbiaux_loc_temp_pointage_absolu::1::['de 1986']
Total feature counts: 6

