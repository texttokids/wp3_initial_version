
********** Entity Names Features **********
*******************************************

1. Sentence: J ' aime aussi profondément les forêts , autre univers de complexité prodigieuse , et tout spécialement celles des Vosges du Nord , mon biotope de base , dans un pays méconnu qui est aussi , je crois , celui d ' Alfred Peter .
Entity Name (Type) :: Alfred Peter (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' aime aussi profondément les forêts , autre univers de complexité prodigieuse , et tout spécialement celles des Vosges du Nord , mon biotope de base , dans un pays méconnu qui est aussi , je crois , celui d ' Alfred Peter .
Tokens having emotion: [ aime ]
Lemmas having emotion: [ aimer ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: J ' aime aussi profondément les forêts , autre univers de complexité prodigieuse , et tout spécialement celles des Vosges du Nord , mon biotope de base , dans un pays méconnu qui est aussi , je crois , celui d ' Alfred Peter .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ aimer, croire ]
marqueurs_niveau3 :: 2 :: [ aimer, croire ]
appreciatif :: 1 :: [ aimer ]
epistemique :: 1 :: [ croire ]
appreciatif_niveau3 :: 1 :: [ aimer ]
epistemique_niveau3 :: 1 :: [ croire ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' aime aussi profondément les forêts , autre univers de complexité prodigieuse , et tout spécialement celles des Vosges du Nord , mon biotope de base , dans un pays méconnu qui est aussi , je crois , celui d ' Alfred Peter .
No features found.

