
********** Entity Names Features **********
*******************************************

1. Sentence: Elle ne se plie pas à la mise en ordre rationnelle et cartésienne à laquelle aspirent les urbanistes qui ont longtemps fait ﬁ de toute négociation .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Elle ne se plie pas à la mise en ordre rationnelle et cartésienne à laquelle aspirent les urbanistes qui ont longtemps fait ﬁ de toute négociation .
Tokens having emotion: [ aspirent ]
Lemmas having emotion: [ aspirer ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Elle ne se plie pas à la mise en ordre rationnelle et cartésienne à laquelle aspirent les urbanistes qui ont longtemps fait ﬁ de toute négociation .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ ordre, longtemps ]
deontique :: 1 :: [ ordre ]
epistemique :: 1 :: [ longtemps ]
marqueurs_niveau2 :: 1 :: [ ordre ]
marqueurs_niveau1 :: 1 :: [ longtemps ]
deontique_niveau2 :: 1 :: [ ordre ]
epistemique_niveau1 :: 1 :: [ longtemps ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Elle ne se plie pas à la mise en ordre rationnelle et cartésienne à laquelle aspirent les urbanistes qui ont longtemps fait ﬁ de toute négociation .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['longtemps']
adverbiaux_duratifs_iteratifs::1::['longtemps']

