
********** Entity Names Features **********
*******************************************

1. Sentence: La participation (ou la conversation) ne donne pas un programme , mais l ' une ou l ' autre de ces modalités d ' échange m ' intéresse car elles m ' informent sur les sensibilités , les peurs , les réticences .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La participation (ou la conversation) ne donne pas un programme , mais l ' une ou l ' autre de ces modalités d ' échange m ' intéresse car elles m ' informent sur les sensibilités , les peurs , les réticences .
Tokens having emotion: [ intéresse, sensibilités, peurs ]
Lemmas having emotion: [ intéresser, sensibilité, peur ]
Categories of the emotion lemmas: [ desir, non_specifiee, peur ]


********** Modality Features **********
***************************************

1. Sentence: La participation (ou la conversation) ne donne pas un programme , mais l ' une ou l ' autre de ces modalités d ' échange m ' intéresse car elles m ' informent sur les sensibilités , les peurs , les réticences .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ intéresser, peur ]
appreciatif :: 1 :: [ peur ]
epistemique :: 1 :: [ peur ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau2 :: 1 :: [ peur ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
appreciatif_niveau2 :: 1 :: [ peur ]
epistemique_niveau2 :: 1 :: [ peur ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La participation (ou la conversation) ne donne pas un programme , mais l ' une ou l ' autre de ces modalités d ' échange m ' intéresse car elles m ' informent sur les sensibilités , les peurs , les réticences .
No features found.

