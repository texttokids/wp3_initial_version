
********** Entity Names Features **********
*******************************************

1. Sentence: En octobre 1999 , dans un contexte difficile et à l ' issue d ' un appel d ' offres disputé , G3A a été désigné comme assistant du Syndicat Mixte du Val de Seine pour l ' aménagement des terrains Renault (trapèze Billancourt , île Seguin , bas Meudon) .
Entity Name (Type) :: bas Meudon (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En octobre 1999 , dans un contexte difficile et à l ' issue d ' un appel d ' offres disputé , G3A a été désigné comme assistant du Syndicat Mixte du Val de Seine pour l ' aménagement des terrains Renault (trapèze Billancourt , île Seguin , bas Meudon) .
Tokens having emotion: [ disputé ]
Lemmas having emotion: [ disputer ]
Categories of the emotion lemmas: [ colere ]


********** Modality Features **********
***************************************

1. Sentence: En octobre 1999 , dans un contexte difficile et à l ' issue d ' un appel d ' offres disputé , G3A a été désigné comme assistant du Syndicat Mixte du Val de Seine pour l ' aménagement des terrains Renault (trapèze Billancourt , île Seguin , bas Meudon) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ difficile, disputer ]
appreciatif :: 2 :: [ difficile, disputer ]
marqueurs_niveau2 :: 2 :: [ difficile, disputer ]
appreciatif_niveau2 :: 2 :: [ difficile, disputer ]
axiologique :: 1 :: [ disputer ]
epistemique :: 1 :: [ difficile ]
axiologique_niveau2 :: 1 :: [ disputer ]
epistemique_niveau2 :: 1 :: [ difficile ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En octobre 1999 , dans un contexte difficile et à l ' issue d ' un appel d ' offres disputé , G3A a été désigné comme assistant du Syndicat Mixte du Val de Seine pour l ' aménagement des terrains Renault (trapèze Billancourt , île Seguin , bas Meudon) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['En octobre 1999']
adverbiaux_localisation_temporelle::1::['En octobre 1999']
adverbiaux_pointage_absolu::1::['En octobre 1999']
adverbiaux_pointage_non_absolu::1::['En octobre 1999']
adverbiaux_loc_temp_focalisation_id::1::['En octobre 1999']
adverbiaux_loc_temp_regionalisation_id::1::['En octobre 1999']
adverbiaux_loc_temp_pointage_non_absolu::1::['En octobre 1999']
adverbiaux_dur_iter_absolu::1::['En octobre 1999']

