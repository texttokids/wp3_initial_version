
********** Entity Names Features **********
*******************************************

1. Sentence: Il s ' agit d ' un morceau de ville anciennement occupé jusqu ' en 1990 par la Manufacture de la Seita dont la fermeture a laissé une friche urbaine de 12 ha .
Entity Name (Type) :: Manufacture de la Seita (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Il s ' agit d ' un morceau de ville anciennement occupé jusqu ' en 1990 par la Manufacture de la Seita dont la fermeture a laissé une friche urbaine de 12 ha .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Il s ' agit d ' un morceau de ville anciennement occupé jusqu ' en 1990 par la Manufacture de la Seita dont la fermeture a laissé une friche urbaine de 12 ha .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ laisser ]
deontique :: 1 :: [ laisser ]
marqueurs_niveau2 :: 1 :: [ laisser ]
deontique_niveau2 :: 1 :: [ laisser ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il s ' agit d ' un morceau de ville anciennement occupé jusqu ' en 1990 par la Manufacture de la Seita dont la fermeture a laissé une friche urbaine de 12 ha .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['anciennement', 'en 1990']
adverbiaux_localisation_temporelle::1::['en 1990']
adverbiaux_pointage_absolu::1::['en 1990']
adverbiaux_pointage_non_absolu::1::['en 1990']
adverbiaux_loc_temp_focalisation_id::1::['en 1990']
adverbiaux_loc_temp_regionalisation_id::1::['en 1990']
adverbiaux_loc_temp_regionalisation_non_id::1::['anciennement']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1990']
adverbiaux_dur_iter_absolu::1::['en 1990']

