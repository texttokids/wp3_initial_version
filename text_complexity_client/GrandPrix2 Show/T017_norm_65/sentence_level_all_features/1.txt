
********** Entity Names Features **********
*******************************************

1. Sentence: La réappropriation des délaissés ouvre en effet un champ pour montrer qu ' il est possible de sortir de l ' impasse et de « faire de la ville » autrement .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: La réappropriation des délaissés ouvre en effet un champ pour montrer qu ' il est possible de sortir de l ' impasse et de « faire de la ville » autrement .
Tokens having emotion: [ délaissés ]
Lemmas having emotion: [ délaisser ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: La réappropriation des délaissés ouvre en effet un champ pour montrer qu ' il est possible de sortir de l ' impasse et de « faire de la ville » autrement .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ ouvrir, possible ]
marqueurs_niveau3 :: 2 :: [ ouvrir, possible ]
alethique :: 1 :: [ possible ]
boulique :: 1 :: [ ouvrir ]
alethique_niveau3 :: 1 :: [ possible ]
boulique_niveau3 :: 1 :: [ ouvrir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La réappropriation des délaissés ouvre en effet un champ pour montrer qu ' il est possible de sortir de l ' impasse et de « faire de la ville » autrement .
No features found.

