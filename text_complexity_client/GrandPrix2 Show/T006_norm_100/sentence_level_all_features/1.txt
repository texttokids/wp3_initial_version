
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est une démarche qui doit être considérée dès l ' origine d ' un projet et que j ' ai mieux comprise , en Allemagne , dans le concours pour le centre de Gelsenkirchen , organisé autour de la problématique de la « ville des petits investissements » – un thème peu exploité dans nos procédures d ' aménagement conçues le plus souvent autour du bilan d ' équilibre des ZAC .
Entity Name (Type) :: Gelsenkirchen (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est une démarche qui doit être considérée dès l ' origine d ' un projet et que j ' ai mieux comprise , en Allemagne , dans le concours pour le centre de Gelsenkirchen , organisé autour de la problématique de la « ville des petits investissements » – un thème peu exploité dans nos procédures d ' aménagement conçues le plus souvent autour du bilan d ' équilibre des ZAC .
Tokens having emotion: [ considérée ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: C ' est une démarche qui doit être considérée dès l ' origine d ' un projet et que j ' ai mieux comprise , en Allemagne , dans le concours pour le centre de Gelsenkirchen , organisé autour de la problématique de la « ville des petits investissements » – un thème peu exploité dans nos procédures d ' aménagement conçues le plus souvent autour du bilan d ' équilibre des ZAC .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ devoir, considérer, mieux, petit, exploiter, souvent ]
marqueurs_niveau3 :: 4 :: [ devoir, considérer, petit, souvent ]
epistemique :: 3 :: [ devoir, considérer, petit ]
epistemique_niveau3 :: 3 :: [ devoir, considérer, petit ]
alethique :: 2 :: [ devoir, souvent ]
appreciatif :: 2 :: [ mieux, exploiter ]
axiologique :: 2 :: [ mieux, exploiter ]
alethique_niveau3 :: 2 :: [ devoir, souvent ]
deontique :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ mieux ]
marqueurs_niveau1 :: 1 :: [ exploiter ]
appreciatif_niveau2 :: 1 :: [ mieux ]
appreciatif_niveau1 :: 1 :: [ exploiter ]
axiologique_niveau2 :: 1 :: [ mieux ]
axiologique_niveau1 :: 1 :: [ exploiter ]
deontique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 32


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est une démarche qui doit être considérée dès l ' origine d ' un projet et que j ' ai mieux comprise , en Allemagne , dans le concours pour le centre de Gelsenkirchen , organisé autour de la problématique de la « ville des petits investissements » – un thème peu exploité dans nos procédures d ' aménagement conçues le plus souvent autour du bilan d ' équilibre des ZAC .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['plus souvent']
adverbiaux_purement_iteratifs::1::['plus souvent']
adverbiaux_iterateur_frequentiel::1::['plus souvent']

