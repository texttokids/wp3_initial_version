
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est toujours aux lisières que les potentialités sont les plus riches , à cheval sur deux mondes .
Entity Name (Type) :: C (MISC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est toujours aux lisières que les potentialités sont les plus riches , à cheval sur deux mondes .
No features found.


********** Modality Features **********
***************************************

1. Sentence: C ' est toujours aux lisières que les potentialités sont les plus riches , à cheval sur deux mondes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ toujours, riche ]
alethique :: 1 :: [ toujours ]
appreciatif :: 1 :: [ riche ]
marqueurs_niveau3 :: 1 :: [ toujours ]
marqueurs_niveau2 :: 1 :: [ riche ]
alethique_niveau3 :: 1 :: [ toujours ]
appreciatif_niveau2 :: 1 :: [ riche ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est toujours aux lisières que les potentialités sont les plus riches , à cheval sur deux mondes .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

