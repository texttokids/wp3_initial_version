
********** Entity Names Features **********
*******************************************

1. Sentence: Les inégalités entre territoires sont réelles , mais incomparablement plus faibles qu ' aux États‑Unis ou en Grande‑Bretagne , en raison de la densité des maillages de l ' État social .
Entity Name (Type) :: État social (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Les inégalités entre territoires sont réelles , mais incomparablement plus faibles qu ' aux États‑Unis ou en Grande‑Bretagne , en raison de la densité des maillages de l ' État social .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Les inégalités entre territoires sont réelles , mais incomparablement plus faibles qu ' aux États‑Unis ou en Grande‑Bretagne , en raison de la densité des maillages de l ' État social .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ inégalité ]
pas_d_indication :: 1 :: [ inégalité ]
marqueurs_niveau0 :: 1 :: [ inégalité ]
pas_d_indication_niveau0 :: 1 :: [ inégalité ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les inégalités entre territoires sont réelles , mais incomparablement plus faibles qu ' aux États‑Unis ou en Grande‑Bretagne , en raison de la densité des maillages de l ' État social .
No features found.

