
********** Entity Names Features **********
*******************************************

1. Sentence: Le « courage de l ' hypothèse » Ma recherche doctorale était intimement liée à ce que j ' expérimentais tous les jours : la nécessité de décrire et de dessiner la ville contemporaine avec son espace fragmenté et peu lisible .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Le « courage de l ' hypothèse » Ma recherche doctorale était intimement liée à ce que j ' expérimentais tous les jours : la nécessité de décrire et de dessiner la ville contemporaine avec son espace fragmenté et peu lisible .
Tokens having emotion: [ courage ]
Lemmas having emotion: [ courage ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Le « courage de l ' hypothèse » Ma recherche doctorale était intimement liée à ce que j ' expérimentais tous les jours : la nécessité de décrire et de dessiner la ville contemporaine avec son espace fragmenté et peu lisible .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ courage, nécessité ]
marqueurs_niveau3 :: 2 :: [ courage, nécessité ]
alethique :: 1 :: [ nécessité ]
appreciatif :: 1 :: [ courage ]
axiologique :: 1 :: [ courage ]
alethique_niveau3 :: 1 :: [ nécessité ]
appreciatif_niveau3 :: 1 :: [ courage ]
axiologique_niveau3 :: 1 :: [ courage ]
Total feature counts: 10


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le « courage de l ' hypothèse » Ma recherche doctorale était intimement liée à ce que j ' expérimentais tous les jours : la nécessité de décrire et de dessiner la ville contemporaine avec son espace fragmenté et peu lisible .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['tous les jours']
adverbiaux_purement_iteratifs::1::['tous les jours']
adverbiaux_loc_temp_focalisation_id::1::['tous les jours']
adverbiaux_loc_temp_regionalisation_id::1::['tous les jours']
adverbiaux_iterateur_calendaire::1::['tous les jours']

