
********** Entity Names Features **********
*******************************************

1. Sentence: Si l ' esthétique est loin d ' être une notion neutre ,  l ' architecture et l ' urbanisme ont toujours produit des significations dont les auteurs étaient les traducteurs plus ou moins conscients .
Entity Name (Type) :: Si (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Si l ' esthétique est loin d ' être une notion neutre ,  l ' architecture et l ' urbanisme ont toujours produit des significations dont les auteurs étaient les traducteurs plus ou moins conscients .
Tokens having emotion: [ neutre ]
Lemmas having emotion: [ neutre ]
Categories of the emotion lemmas: [ impassibilite ]


********** Modality Features **********
***************************************

1. Sentence: Si l ' esthétique est loin d ' être une notion neutre ,  l ' architecture et l ' urbanisme ont toujours produit des significations dont les auteurs étaient les traducteurs plus ou moins conscients .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ toujours ]
alethique :: 1 :: [ toujours ]
marqueurs_niveau3 :: 1 :: [ toujours ]
alethique_niveau3 :: 1 :: [ toujours ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Si l ' esthétique est loin d ' être une notion neutre ,  l ' architecture et l ' urbanisme ont toujours produit des significations dont les auteurs étaient les traducteurs plus ou moins conscients .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

