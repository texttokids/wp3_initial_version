
********** Entity Names Features **********
*******************************************

1. Sentence: L ' apparente simplicité du projet tient à quelques décisions majeures sur le nivellement : la plateforme centrale permet de se dégager des contre-voies et de laisser passer le regard depuis la place de la Comédie jusqu ' au ﬂeuve , au-dessus des quais récemment aménagés .
Entity Name (Type) :: place de la Comédie (LOC)


********** Emotions Features **********
***************************************

1. Sentence: L ' apparente simplicité du projet tient à quelques décisions majeures sur le nivellement : la plateforme centrale permet de se dégager des contre-voies et de laisser passer le regard depuis la place de la Comédie jusqu ' au ﬂeuve , au-dessus des quais récemment aménagés .
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' apparente simplicité du projet tient à quelques décisions majeures sur le nivellement : la plateforme centrale permet de se dégager des contre-voies et de laisser passer le regard depuis la place de la Comédie jusqu ' au ﬂeuve , au-dessus des quais récemment aménagés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ décision, permettre, laisser ]
deontique :: 2 :: [ permettre, laisser ]
marqueurs_niveau2 :: 2 :: [ décision, laisser ]
boulique :: 1 :: [ décision ]
epistemique :: 1 :: [ décision ]
marqueurs_niveau3 :: 1 :: [ permettre ]
boulique_niveau2 :: 1 :: [ décision ]
deontique_niveau3 :: 1 :: [ permettre ]
deontique_niveau2 :: 1 :: [ laisser ]
epistemique_niveau2 :: 1 :: [ décision ]
Total feature counts: 14


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' apparente simplicité du projet tient à quelques décisions majeures sur le nivellement : la plateforme centrale permet de se dégager des contre-voies et de laisser passer le regard depuis la place de la Comédie jusqu ' au ﬂeuve , au-dessus des quais récemment aménagés .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['récemment']
adverbiaux_duratifs_iteratifs::1::['récemment']
adverbiaux_loc_temp_regionalisation_id::1::['récemment']

