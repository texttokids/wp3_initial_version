
********** Entity Names Features **********
*******************************************

1. Sentence: Les leçons de l ' expérience sont loin d ' être « Ce qui ne marche pas , c ' est cette nouvelle ville qui n ' a jamais intégré la culture des gens qui l ' habitent . […] On ne remettra pas d ' aplomb ces banlieues , telles qu ' elles ont été construites , par un acte technique de renouvellement urbain .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les leçons de l ' expérience sont loin d ' être « Ce qui ne marche pas , c ' est cette nouvelle ville qui n ' a jamais intégré la culture des gens qui l ' habitent . […] On ne remettra pas d ' aplomb ces banlieues , telles qu ' elles ont été construites , par un acte technique de renouvellement urbain .
Tokens having emotion: [ aplomb ]
Lemmas having emotion: [ aplomb ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Les leçons de l ' expérience sont loin d ' être « Ce qui ne marche pas , c ' est cette nouvelle ville qui n ' a jamais intégré la culture des gens qui l ' habitent . […] On ne remettra pas d ' aplomb ces banlieues , telles qu ' elles ont été construites , par un acte technique de renouvellement urbain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ marcher, jamais ]
marqueurs_niveau3 :: 2 :: [ marcher, jamais ]
alethique :: 1 :: [ jamais ]
boulique :: 1 :: [ marcher ]
alethique_niveau3 :: 1 :: [ jamais ]
boulique_niveau3 :: 1 :: [ marcher ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les leçons de l ' expérience sont loin d ' être « Ce qui ne marche pas , c ' est cette nouvelle ville qui n ' a jamais intégré la culture des gens qui l ' habitent . […] On ne remettra pas d ' aplomb ces banlieues , telles qu ' elles ont été construites , par un acte technique de renouvellement urbain .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['jamais']
adverbiaux_purement_iteratifs::1::['jamais']
adverbiaux_iterateur_frequentiel::1::['jamais']

