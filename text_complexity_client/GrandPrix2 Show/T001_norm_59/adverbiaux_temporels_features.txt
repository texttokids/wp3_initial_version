====== Sentence Level Features ======

1. Sentence: On la suppose tranquille , avide de plans bien droits (comme si Turin avait dévoré Barcelone , l ' empêchant de sourire) , mais son énigme , la plupart du temps , est justement de s ' être passée de projet , ou plus exactement d ' avoir donné à son vocabulaire (ses maisons , ses usines , la céramique de son métro et le bossage de ses palais) une priorité sur ce qui , bien plus tard , a servi à les encadrer .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['plus tard', 'la plupart du temps']
adverbiaux_localisation_temporelle::1::['plus tard']
adverbiaux_purement_iteratifs::1::['la plupart du temps']
adverbiaux_pointage_non_absolu::1::['plus tard']
adverbiaux_loc_temp_regionalisation_id::1::['plus tard']
adverbiaux_loc_temp_pointage_non_absolu::1::['plus tard']
adverbiaux_iterateur_frequentiel::1::['la plupart du temps']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['plus tard', 'la plupart du temps']
adverbiaux_localisation_temporelle::1::['plus tard']
adverbiaux_purement_iteratifs::1::['la plupart du temps']
adverbiaux_pointage_non_absolu::1::['plus tard']
adverbiaux_loc_temp_regionalisation_id::1::['plus tard']
adverbiaux_loc_temp_pointage_non_absolu::1::['plus tard']
adverbiaux_iterateur_frequentiel::1::['la plupart du temps']
Total feature counts: 8

