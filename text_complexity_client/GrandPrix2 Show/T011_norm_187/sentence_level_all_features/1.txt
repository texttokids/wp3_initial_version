
********** Entity Names Features **********
*******************************************

1. Sentence: Les grands projets du Président et les espaces publics majeurs de la capitale (le fleuve , le canal SaintMartin et le Bassin de la Villette , le grand axe Est-Ouest du Grand Louvre à l ' Arche) auraient constitué désormais le site éclaté d ' une Exposition Universelle d ' un nouveau genre dont toute la ville aurait été le théâtre .
Entity Name (Type) :: Exposition Universelle (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Les grands projets du Président et les espaces publics majeurs de la capitale (le fleuve , le canal SaintMartin et le Bassin de la Villette , le grand axe Est-Ouest du Grand Louvre à l ' Arche) auraient constitué désormais le site éclaté d ' une Exposition Universelle d ' un nouveau genre dont toute la ville aurait été le théâtre .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Les grands projets du Président et les espaces publics majeurs de la capitale (le fleuve , le canal SaintMartin et le Bassin de la Villette , le grand axe Est-Ouest du Grand Louvre à l ' Arche) auraient constitué désormais le site éclaté d ' une Exposition Universelle d ' un nouveau genre dont toute la ville aurait été le théâtre .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ grand, grand, grand ]
epistemique :: 3 :: [ grand, grand, grand ]
marqueurs_niveau3 :: 3 :: [ grand, grand, grand ]
epistemique_niveau3 :: 3 :: [ grand, grand, grand ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les grands projets du Président et les espaces publics majeurs de la capitale (le fleuve , le canal SaintMartin et le Bassin de la Villette , le grand axe Est-Ouest du Grand Louvre à l ' Arche) auraient constitué désormais le site éclaté d ' une Exposition Universelle d ' un nouveau genre dont toute la ville aurait été le théâtre .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['désormais']
adverbiaux_duratifs_iteratifs::1::['désormais']

