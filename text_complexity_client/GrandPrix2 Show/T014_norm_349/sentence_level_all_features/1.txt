
********** Entity Names Features **********
*******************************************

1. Sentence: Le monument de Renzo Piano , haut de 160 m , le parc Martin-Luther-King et la convergence des lignes du métro , du RER et du tramway doivent conjuguer attractivité et rayonnement pour former un nouveau centre d ' intérêt à l ' échelle du nord-ouest parisien .
Entity Name (Type) :: RER (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le monument de Renzo Piano , haut de 160 m , le parc Martin-Luther-King et la convergence des lignes du métro , du RER et du tramway doivent conjuguer attractivité et rayonnement pour former un nouveau centre d ' intérêt à l ' échelle du nord-ouest parisien .
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Le monument de Renzo Piano , haut de 160 m , le parc Martin-Luther-King et la convergence des lignes du métro , du RER et du tramway doivent conjuguer attractivité et rayonnement pour former un nouveau centre d ' intérêt à l ' échelle du nord-ouest parisien .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ haut, devoir, intérêt ]
alethique :: 1 :: [ devoir ]
boulique :: 1 :: [ intérêt ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
pas_d_indication :: 1 :: [ haut ]
marqueurs_niveau3 :: 1 :: [ devoir ]
marqueurs_niveau2 :: 1 :: [ intérêt ]
marqueurs_niveau0 :: 1 :: [ haut ]
alethique_niveau3 :: 1 :: [ devoir ]
boulique_niveau2 :: 1 :: [ intérêt ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
pas_d_indication_niveau0 :: 1 :: [ haut ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le monument de Renzo Piano , haut de 160 m , le parc Martin-Luther-King et la convergence des lignes du métro , du RER et du tramway doivent conjuguer attractivité et rayonnement pour former un nouveau centre d ' intérêt à l ' échelle du nord-ouest parisien .
No features found.

