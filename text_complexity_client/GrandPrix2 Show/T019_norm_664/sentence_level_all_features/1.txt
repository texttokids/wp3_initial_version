
********** Entity Names Features **********
*******************************************

1. Sentence: C ' est pourquoi je suis toujours sidéré lorsqu ' on me parle de Saclay comme d ' un projet ex nihilo .
Entity Name (Type) :: Saclay (LOC)


********** Emotions Features **********
***************************************

1. Sentence: C ' est pourquoi je suis toujours sidéré lorsqu ' on me parle de Saclay comme d ' un projet ex nihilo .
Tokens having emotion: [ sidéré ]
Lemmas having emotion: [ sidérer ]
Categories of the emotion lemmas: [ surprise ]


********** Modality Features **********
***************************************

1. Sentence: C ' est pourquoi je suis toujours sidéré lorsqu ' on me parle de Saclay comme d ' un projet ex nihilo .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ toujours ]
alethique :: 1 :: [ toujours ]
marqueurs_niveau3 :: 1 :: [ toujours ]
alethique_niveau3 :: 1 :: [ toujours ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: C ' est pourquoi je suis toujours sidéré lorsqu ' on me parle de Saclay comme d ' un projet ex nihilo .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

