
********** Entity Names Features **********
*******************************************

1. Sentence: Trouver à chaque projet un financement adéquat (quitte à l ' « inventer » au sein des budgets déjà mobilisés) suppose de considérer que tout projet de paysage , d ' urbanisme ou d ' archi tecture est un prototype , donc un moment de recherche .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Trouver à chaque projet un financement adéquat (quitte à l ' « inventer » au sein des budgets déjà mobilisés) suppose de considérer que tout projet de paysage , d ' urbanisme ou d ' archi tecture est un prototype , donc un moment de recherche .
Tokens having emotion: [ considérer ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Trouver à chaque projet un financement adéquat (quitte à l ' « inventer » au sein des budgets déjà mobilisés) suppose de considérer que tout projet de paysage , d ' urbanisme ou d ' archi tecture est un prototype , donc un moment de recherche .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ inventer, considérer ]
epistemique :: 2 :: [ inventer, considérer ]
marqueurs_niveau3 :: 1 :: [ considérer ]
marqueurs_niveau2 :: 1 :: [ inventer ]
epistemique_niveau3 :: 1 :: [ considérer ]
epistemique_niveau2 :: 1 :: [ inventer ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Trouver à chaque projet un financement adéquat (quitte à l ' « inventer » au sein des budgets déjà mobilisés) suppose de considérer que tout projet de paysage , d ' urbanisme ou d ' archi tecture est un prototype , donc un moment de recherche .
No features found.

