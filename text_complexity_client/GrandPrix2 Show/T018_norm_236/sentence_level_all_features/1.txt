
********** Entity Names Features **********
*******************************************

1. Sentence: À l ' Atelier Ruelle , nous aimons ce travail à poursuivre sur des années .
Entity Name (Type) :: Atelier Ruelle (LOC)


********** Emotions Features **********
***************************************

1. Sentence: À l ' Atelier Ruelle , nous aimons ce travail à poursuivre sur des années .
Tokens having emotion: [ aimons ]
Lemmas having emotion: [ aimer ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: À l ' Atelier Ruelle , nous aimons ce travail à poursuivre sur des années .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ aimer, travail ]
appreciatif :: 2 :: [ aimer, travail ]
marqueurs_niveau3 :: 2 :: [ aimer, travail ]
appreciatif_niveau3 :: 2 :: [ aimer, travail ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À l ' Atelier Ruelle , nous aimons ce travail à poursuivre sur des années .
No features found.

