
********** Entity Names Features **********
*******************************************

1. Sentence: Cet accueil calculé de l ' inconnu répond à une question moderne , celle de l ' entropie , de l ' éclatement architectural , de la disparité sans fin des egos des architectes , des propositions individuelles et des « styles » , toujours perçue comme la fin de l ' unité et de la cohérence de la Ville et , partant de la communauté .
Entity Name (Type) :: Ville (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Cet accueil calculé de l ' inconnu répond à une question moderne , celle de l ' entropie , de l ' éclatement architectural , de la disparité sans fin des egos des architectes , des propositions individuelles et des « styles » , toujours perçue comme la fin de l ' unité et de la cohérence de la Ville et , partant de la communauté .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Cet accueil calculé de l ' inconnu répond à une question moderne , celle de l ' entropie , de l ' éclatement architectural , de la disparité sans fin des egos des architectes , des propositions individuelles et des « styles » , toujours perçue comme la fin de l ' unité et de la cohérence de la Ville et , partant de la communauté .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ moderne, toujours, partir ]
alethique :: 2 :: [ toujours, partir ]
pas_d_indication :: 1 :: [ moderne ]
marqueurs_niveau3 :: 1 :: [ toujours ]
marqueurs_niveau1 :: 1 :: [ partir ]
marqueurs_niveau0 :: 1 :: [ moderne ]
alethique_niveau3 :: 1 :: [ toujours ]
alethique_niveau1 :: 1 :: [ partir ]
pas_d_indication_niveau0 :: 1 :: [ moderne ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Cet accueil calculé de l ' inconnu répond à une question moderne , celle de l ' entropie , de l ' éclatement architectural , de la disparité sans fin des egos des architectes , des propositions individuelles et des « styles » , toujours perçue comme la fin de l ' unité et de la cohérence de la Ville et , partant de la communauté .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

