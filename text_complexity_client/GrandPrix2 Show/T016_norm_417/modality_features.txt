====== Sentence Level Features ======

1. Sentence: En approfondissant sa connaissance , des ambiguïtés et des paradoxes émergent , renforçant alors la nature hypothétique et projectuelle de l ' image du « réservoir » , en relation aux pressions économiques , aux conséquences du changement climatique et à la forte modification de la population (le territoire de la vallée de la Durance étant parmi les plus attractifs à l ' échelle nationale pour la qualité de vie et les paysages qu ' il offre) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pression, fort, offrir ]
pas_d_indication :: 2 :: [ pression, fort ]
marqueurs_niveau0 :: 2 :: [ pression, fort ]
pas_d_indication_niveau0 :: 2 :: [ pression, fort ]
appreciatif :: 1 :: [ offrir ]
marqueurs_niveau2 :: 1 :: [ offrir ]
appreciatif_niveau2 :: 1 :: [ offrir ]
Total feature counts: 12


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pression, fort, offrir ]
pas_d_indication :: 2 :: [ pression, fort ]
marqueurs_niveau0 :: 2 :: [ pression, fort ]
pas_d_indication_niveau0 :: 2 :: [ pression, fort ]
appreciatif :: 1 :: [ offrir ]
marqueurs_niveau2 :: 1 :: [ offrir ]
appreciatif_niveau2 :: 1 :: [ offrir ]
Total feature counts: 12

