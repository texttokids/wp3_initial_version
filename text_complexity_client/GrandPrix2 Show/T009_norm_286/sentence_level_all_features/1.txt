
********** Entity Names Features **********
*******************************************

1. Sentence: Nous pensions que la position de Francilien qui émane des cerveaux de certains techniciens est un peu désuète lorsque l ' on se trouve à Bombay ou à Clermont-Ferrand et qu ' il est important d ' assumer , à partir de toutes les villes et quartiers qui la composent ,  l ' identité de la totalité de la métropole .
Entity Name (Type) :: Clermont-Ferrand (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Nous pensions que la position de Francilien qui émane des cerveaux de certains techniciens est un peu désuète lorsque l ' on se trouve à Bombay ou à Clermont-Ferrand et qu ' il est important d ' assumer , à partir de toutes les villes et quartiers qui la composent ,  l ' identité de la totalité de la métropole .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Nous pensions que la position de Francilien qui émane des cerveaux de certains techniciens est un peu désuète lorsque l ' on se trouve à Bombay ou à Clermont-Ferrand et qu ' il est important d ' assumer , à partir de toutes les villes et quartiers qui la composent ,  l ' identité de la totalité de la métropole .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ penser, certain, important, partir ]
epistemique :: 3 :: [ penser, certain, important ]
marqueurs_niveau3 :: 2 :: [ penser, certain ]
marqueurs_niveau1 :: 2 :: [ important, partir ]
epistemique_niveau3 :: 2 :: [ penser, certain ]
alethique :: 1 :: [ partir ]
appreciatif :: 1 :: [ important ]
axiologique :: 1 :: [ important ]
alethique_niveau1 :: 1 :: [ partir ]
appreciatif_niveau1 :: 1 :: [ important ]
axiologique_niveau1 :: 1 :: [ important ]
epistemique_niveau1 :: 1 :: [ important ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Nous pensions que la position de Francilien qui émane des cerveaux de certains techniciens est un peu désuète lorsque l ' on se trouve à Bombay ou à Clermont-Ferrand et qu ' il est important d ' assumer , à partir de toutes les villes et quartiers qui la composent ,  l ' identité de la totalité de la métropole .
No features found.

