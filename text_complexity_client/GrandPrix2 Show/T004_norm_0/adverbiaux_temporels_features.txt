====== Sentence Level Features ======

1. Sentence: Une part importante de mon travail et de mes réflexions se situe désormais aux limites des territoires de la ville et de la campagne ou , plus largement , du paysage : à la mitoyenneté , aujourd ' hui conflictuelle , entre ces deux mondes qui s ' ignorent et se repoussent , alors que se formuleront précisément là , demain , les projets de réconciliation que je souhaite .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['demain', 'désormais']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_duratifs_iteratifs::1::['désormais']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['demain', 'désormais']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_duratifs_iteratifs::1::['désormais']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']
Total feature counts: 9

