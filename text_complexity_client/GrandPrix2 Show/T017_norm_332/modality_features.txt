====== Sentence Level Features ======

1. Sentence: Un lotissement « noyé » dans la forêt serait donc interdit du simple point de vue du règlement incendie ,  l ' arbre étant une fois reconnu comme dangereux et une autre comme capteur de CO2 indispensable pour la biodiversité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ interdire, incendie, dangereux, indispensable ]
appreciatif :: 2 :: [ dangereux, indispensable ]
marqueurs_niveau3 :: 2 :: [ interdire, indispensable ]
deontique :: 1 :: [ interdire ]
pas_d_indication :: 1 :: [ incendie ]
marqueurs_niveau2 :: 1 :: [ dangereux ]
marqueurs_niveau0 :: 1 :: [ incendie ]
appreciatif_niveau3 :: 1 :: [ indispensable ]
appreciatif_niveau2 :: 1 :: [ dangereux ]
deontique_niveau3 :: 1 :: [ interdire ]
pas_d_indication_niveau0 :: 1 :: [ incendie ]
Total feature counts: 16


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ interdire, incendie, dangereux, indispensable ]
appreciatif :: 2 :: [ dangereux, indispensable ]
marqueurs_niveau3 :: 2 :: [ interdire, indispensable ]
deontique :: 1 :: [ interdire ]
pas_d_indication :: 1 :: [ incendie ]
marqueurs_niveau2 :: 1 :: [ dangereux ]
marqueurs_niveau0 :: 1 :: [ incendie ]
appreciatif_niveau3 :: 1 :: [ indispensable ]
appreciatif_niveau2 :: 1 :: [ dangereux ]
deontique_niveau3 :: 1 :: [ interdire ]
pas_d_indication_niveau0 :: 1 :: [ incendie ]
Total feature counts: 16

