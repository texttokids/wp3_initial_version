
********** Entity Names Features **********
*******************************************

1. Sentence: Grands projets  : la fausse promesse de l ' enrichissement des territoires Face aux grands projets comme Europacity3 , chercher un responsable particulier à l ' inaction de l ' État est inutile .
Entity Name (Type) :: État (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Grands projets  : la fausse promesse de l ' enrichissement des territoires Face aux grands projets comme Europacity3 , chercher un responsable particulier à l ' inaction de l ' État est inutile .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Grands projets  : la fausse promesse de l ' enrichissement des territoires Face aux grands projets comme Europacity3 , chercher un responsable particulier à l ' inaction de l ' État est inutile .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ faux, promesse, grand, chercher, responsable, inaction, inutile ]
pas_d_indication :: 2 :: [ promesse, inaction ]
marqueurs_niveau3 :: 2 :: [ grand, chercher ]
marqueurs_niveau2 :: 2 :: [ responsable, inutile ]
marqueurs_niveau0 :: 2 :: [ promesse, inaction ]
pas_d_indication_niveau0 :: 2 :: [ promesse, inaction ]
alethique :: 1 :: [ faux ]
appreciatif :: 1 :: [ inutile ]
boulique :: 1 :: [ chercher ]
deontique :: 1 :: [ responsable ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau1 :: 1 :: [ faux ]
alethique_niveau1 :: 1 :: [ faux ]
appreciatif_niveau2 :: 1 :: [ inutile ]
boulique_niveau3 :: 1 :: [ chercher ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Grands projets  : la fausse promesse de l ' enrichissement des territoires Face aux grands projets comme Europacity3 , chercher un responsable particulier à l ' inaction de l ' État est inutile .
No features found.

