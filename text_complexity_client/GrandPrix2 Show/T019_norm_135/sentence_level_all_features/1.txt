
********** Entity Names Features **********
*******************************************

1. Sentence: Heureusement , nous avons souvent pu contrecarrer la tentation des établissements universitaires à choisir des projets fortement « identitaires » , parfois synonymes de chichiteux ou prétentieux .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Heureusement , nous avons souvent pu contrecarrer la tentation des établissements universitaires à choisir des projets fortement « identitaires » , parfois synonymes de chichiteux ou prétentieux .
Tokens having emotion: [ Heureusement, tentation ]
Lemmas having emotion: [ heureusement, tentation ]
Categories of the emotion lemmas: [ joie, desir ]


********** Modality Features **********
***************************************

1. Sentence: Heureusement , nous avons souvent pu contrecarrer la tentation des établissements universitaires à choisir des projets fortement « identitaires » , parfois synonymes de chichiteux ou prétentieux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ heureusement, souvent, pouvoir, choisir, parfois ]
marqueurs_niveau3 :: 4 :: [ heureusement, souvent, pouvoir, parfois ]
alethique :: 3 :: [ souvent, pouvoir, parfois ]
alethique_niveau3 :: 3 :: [ souvent, pouvoir, parfois ]
appreciatif :: 1 :: [ heureusement ]
boulique :: 1 :: [ choisir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ choisir ]
appreciatif_niveau3 :: 1 :: [ heureusement ]
boulique_niveau2 :: 1 :: [ choisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 24


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Heureusement , nous avons souvent pu contrecarrer la tentation des établissements universitaires à choisir des projets fortement « identitaires » , parfois synonymes de chichiteux ou prétentieux .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['souvent', 'parfois']
adverbiaux_purement_iteratifs::2::['souvent', 'parfois']
adverbiaux_iterateur_frequentiel::2::['souvent', 'parfois']

