
********** Entity Names Features **********
*******************************************

1. Sentence: Le choc , qui reste encore le choc , c ' est Bruno Taut et les milliers de logements qu ' il a construits à Berlin et qui révèlent un rapport intime à l ' espace public , un rapport généreux à la nature .
Entity Name (Type) :: Berlin (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le choc , qui reste encore le choc , c ' est Bruno Taut et les milliers de logements qu ' il a construits à Berlin et qui révèlent un rapport intime à l ' espace public , un rapport généreux à la nature .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Le choc , qui reste encore le choc , c ' est Bruno Taut et les milliers de logements qu ' il a construits à Berlin et qui révèlent un rapport intime à l ' espace public , un rapport généreux à la nature .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ généreux ]
appreciatif :: 1 :: [ généreux ]
axiologique :: 1 :: [ généreux ]
marqueurs_niveau3 :: 1 :: [ généreux ]
appreciatif_niveau3 :: 1 :: [ généreux ]
axiologique_niveau3 :: 1 :: [ généreux ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le choc , qui reste encore le choc , c ' est Bruno Taut et les milliers de logements qu ' il a construits à Berlin et qui révèlent un rapport intime à l ' espace public , un rapport généreux à la nature .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

