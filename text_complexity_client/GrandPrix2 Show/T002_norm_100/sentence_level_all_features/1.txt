
********** Entity Names Features **********
*******************************************

1. Sentence: Le citoyen change , la vie s ' allonge : la présence de cycles , au cours de l ' existence , où les attentes et les exigences du cadre de vie évoluent , fait que le traditionnel tropisme entre l ' habitant et sa ville tend à disparaître .
Entity Name (Type) :: Le (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Le citoyen change , la vie s ' allonge : la présence de cycles , au cours de l ' existence , où les attentes et les exigences du cadre de vie évoluent , fait que le traditionnel tropisme entre l ' habitant et sa ville tend à disparaître .
Tokens having emotion: [ tend ]
Lemmas having emotion: [ tendre ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: Le citoyen change , la vie s ' allonge : la présence de cycles , au cours de l ' existence , où les attentes et les exigences du cadre de vie évoluent , fait que le traditionnel tropisme entre l ' habitant et sa ville tend à disparaître .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ tendre ]
pas_d_indication :: 1 :: [ tendre ]
marqueurs_niveau0 :: 1 :: [ tendre ]
pas_d_indication_niveau0 :: 1 :: [ tendre ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le citoyen change , la vie s ' allonge : la présence de cycles , au cours de l ' existence , où les attentes et les exigences du cadre de vie évoluent , fait que le traditionnel tropisme entre l ' habitant et sa ville tend à disparaître .
No features found.

