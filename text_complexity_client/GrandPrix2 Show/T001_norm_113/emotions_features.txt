====== Sentence Level Features ======

1. Sentence: Celui que l ' on peut observer , que l ' on ressent partout (discontinuité des tissus , omniprésence du vide , importance des trajets) , sans bien en apprécier les sources ou même en mesurer la radicalité .
Tokens having emotion: [ ressent ]
Lemmas having emotion: [ ressentir ]
Categories of the emotion lemmas: [ non_specifiee ]


====== Text Level Features ======

Tokens having emotion: [ ressent ]
Lemmas having emotion: [ ressentir ]
Category of the emotion lemmas: [ non_specifiee ]
Total emotion lemma counts: 1

