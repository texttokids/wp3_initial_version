
********** Entity Names Features **********
*******************************************

1. Sentence: Qu ' une société foncière , les EMGP , ait développé avec continuité cette stratégie , qu ' elle ait pu , grâce à cette action , changer l ' image du secteur , revaloriser les loyers , accueillir les entreprises de la communication , du net , de la mode et , demain , un grand centre commercial et de loisirs sur le bassin , est , pour nous , le signe de la pertinence économique de la démarche urbaine préconisée .
Entity Name (Type) :: EMGP (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Qu ' une société foncière , les EMGP , ait développé avec continuité cette stratégie , qu ' elle ait pu , grâce à cette action , changer l ' image du secteur , revaloriser les loyers , accueillir les entreprises de la communication , du net , de la mode et , demain , un grand centre commercial et de loisirs sur le bassin , est , pour nous , le signe de la pertinence économique de la démarche urbaine préconisée .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Qu ' une société foncière , les EMGP , ait développé avec continuité cette stratégie , qu ' elle ait pu , grâce à cette action , changer l ' image du secteur , revaloriser les loyers , accueillir les entreprises de la communication , du net , de la mode et , demain , un grand centre commercial et de loisirs sur le bassin , est , pour nous , le signe de la pertinence économique de la démarche urbaine préconisée .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pouvoir, grand, loisir ]
epistemique :: 2 :: [ pouvoir, grand ]
marqueurs_niveau3 :: 2 :: [ pouvoir, grand ]
epistemique_niveau3 :: 2 :: [ pouvoir, grand ]
alethique :: 1 :: [ pouvoir ]
appreciatif :: 1 :: [ loisir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ loisir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau2 :: 1 :: [ loisir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Qu ' une société foncière , les EMGP , ait développé avec continuité cette stratégie , qu ' elle ait pu , grâce à cette action , changer l ' image du secteur , revaloriser les loyers , accueillir les entreprises de la communication , du net , de la mode et , demain , un grand centre commercial et de loisirs sur le bassin , est , pour nous , le signe de la pertinence économique de la démarche urbaine préconisée .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['demain']
adverbiaux_localisation_temporelle::1::['demain']
adverbiaux_pointage_non_absolu::2::['demain', 'demain']
adverbiaux_loc_temp_regionalisation_id::1::['demain']
adverbiaux_loc_temp_pointage_non_absolu::1::['demain']
adverbiaux_dur_iter_deictique::1::['demain']

