
********** Entity Names Features **********
*******************************************

1. Sentence: l ' architecte , lui , est tendu vers la production d ' une œuvre finie , achevée dans le temps .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: l ' architecte , lui , est tendu vers la production d ' une œuvre finie , achevée dans le temps .
Tokens having emotion: [ tendu ]
Lemmas having emotion: [ tendu ]
Categories of the emotion lemmas: [ deplaisir ]


********** Modality Features **********
***************************************

1. Sentence: l ' architecte , lui , est tendu vers la production d ' une œuvre finie , achevée dans le temps .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ lui ]
alethique :: 1 :: [ lui ]
epistemique :: 1 :: [ lui ]
marqueurs_niveau3 :: 1 :: [ lui ]
alethique_niveau3 :: 1 :: [ lui ]
epistemique_niveau3 :: 1 :: [ lui ]
Total feature counts: 6


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: l ' architecte , lui , est tendu vers la production d ' une œuvre finie , achevée dans le temps .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['dans le temps']
adverbiaux_localisation_temporelle::1::['dans le temps']
adverbiaux_pointage_non_absolu::1::['dans le temps']
adverbiaux_loc_temp_focalisation_id::1::['dans le temps']
adverbiaux_loc_temp_regionalisation_id::1::['dans le temps']
adverbiaux_loc_temp_pointage_non_absolu::1::['dans le temps']

