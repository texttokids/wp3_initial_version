
********** Entity Names Features **********
*******************************************

1. Sentence: En 2009 commencent les études à Lyon Part-Dieu et à Chapelle International , qui se concrétisent aujourd ' hui et s ' affirment grâce à la réalisation des espaces publics avec le paysagiste Bas Smets , et s ' ouvre un temps de réflexion sur les quartiers de gare  : à Bruxelles Midi , la Gare Habitante , et sur le secteur Pleyel , le Hub métropolitain .
Entity Name (Type) :: Hub métropolitain (LOC)


********** Emotions Features **********
***************************************

1. Sentence: En 2009 commencent les études à Lyon Part-Dieu et à Chapelle International , qui se concrétisent aujourd ' hui et s ' affirment grâce à la réalisation des espaces publics avec le paysagiste Bas Smets , et s ' ouvre un temps de réflexion sur les quartiers de gare  : à Bruxelles Midi , la Gare Habitante , et sur le secteur Pleyel , le Hub métropolitain .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 2009 commencent les études à Lyon Part-Dieu et à Chapelle International , qui se concrétisent aujourd ' hui et s ' affirment grâce à la réalisation des espaces publics avec le paysagiste Bas Smets , et s ' ouvre un temps de réflexion sur les quartiers de gare  : à Bruxelles Midi , la Gare Habitante , et sur le secteur Pleyel , le Hub métropolitain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ affirmer, ouvrir ]
boulique :: 1 :: [ ouvrir ]
pas_d_indication :: 1 :: [ affirmer ]
marqueurs_niveau3 :: 1 :: [ ouvrir ]
marqueurs_niveau0 :: 1 :: [ affirmer ]
boulique_niveau3 :: 1 :: [ ouvrir ]
pas_d_indication_niveau0 :: 1 :: [ affirmer ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 2009 commencent les études à Lyon Part-Dieu et à Chapelle International , qui se concrétisent aujourd ' hui et s ' affirment grâce à la réalisation des espaces publics avec le paysagiste Bas Smets , et s ' ouvre un temps de réflexion sur les quartiers de gare  : à Bruxelles Midi , la Gare Habitante , et sur le secteur Pleyel , le Hub métropolitain .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['En 2009', 'Midi']
adverbiaux_localisation_temporelle::2::['En 2009', 'Midi']
adverbiaux_pointage_absolu::1::['En 2009']
adverbiaux_pointage_non_absolu::2::['En 2009', 'Midi']
adverbiaux_loc_temp_focalisation_id::2::['En 2009', 'Midi']
adverbiaux_loc_temp_regionalisation_id::2::['En 2009', 'Midi']
adverbiaux_loc_temp_pointage_non_absolu::2::['En 2009', 'Midi']
adverbiaux_dur_iter_absolu::1::['En 2009']

