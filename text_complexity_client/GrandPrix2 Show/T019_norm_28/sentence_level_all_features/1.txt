
********** Entity Names Features **********
*******************************************

1. Sentence: Ensuite , la recherche qui m ' intéresse est celle qui débouche sur l ' action et se nourrit d ' elle  : peut-on aujourd ' hui comprendre ce qui se passe dans la complexité des organisations , des villes , des entreprises , des univers professionnels sans y participer activement , d ' une manière ou d ' une autre ?
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Ensuite , la recherche qui m ' intéresse est celle qui débouche sur l ' action et se nourrit d ' elle  : peut-on aujourd ' hui comprendre ce qui se passe dans la complexité des organisations , des villes , des entreprises , des univers professionnels sans y participer activement , d ' une manière ou d ' une autre ?
Tokens having emotion: [ intéresse ]
Lemmas having emotion: [ intéresser ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Ensuite , la recherche qui m ' intéresse est celle qui débouche sur l ' action et se nourrit d ' elle  : peut-on aujourd ' hui comprendre ce qui se passe dans la complexité des organisations , des villes , des entreprises , des univers professionnels sans y participer activement , d ' une manière ou d ' une autre ?
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ intéresser, pouvoir ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ intéresser ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau0 :: 1 :: [ intéresser ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ intéresser ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ensuite , la recherche qui m ' intéresse est celle qui débouche sur l ' action et se nourrit d ' elle  : peut-on aujourd ' hui comprendre ce qui se passe dans la complexité des organisations , des villes , des entreprises , des univers professionnels sans y participer activement , d ' une manière ou d ' une autre ?
No features found.

