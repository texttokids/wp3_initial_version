
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai relu récemment le livre passionnant , dans lequel Bernard Hirsch raconte la naissance de Cergy-Pontoise (cela s ' appelle Oublier Cergy , L ' invention d ' une ville nouvelle , aux Presses de l ' ENPC , 1970) .
Entity Name (Type) :: Presses de l ' ENPC (MISC)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai relu récemment le livre passionnant , dans lequel Bernard Hirsch raconte la naissance de Cergy-Pontoise (cela s ' appelle Oublier Cergy , L ' invention d ' une ville nouvelle , aux Presses de l ' ENPC , 1970) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: J ' ai relu récemment le livre passionnant , dans lequel Bernard Hirsch raconte la naissance de Cergy-Pontoise (cela s ' appelle Oublier Cergy , L ' invention d ' une ville nouvelle , aux Presses de l ' ENPC , 1970) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ oublier ]
pas_d_indication :: 1 :: [ oublier ]
marqueurs_niveau0 :: 1 :: [ oublier ]
pas_d_indication_niveau0 :: 1 :: [ oublier ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai relu récemment le livre passionnant , dans lequel Bernard Hirsch raconte la naissance de Cergy-Pontoise (cela s ' appelle Oublier Cergy , L ' invention d ' une ville nouvelle , aux Presses de l ' ENPC , 1970) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['récemment']
adverbiaux_duratifs_iteratifs::1::['récemment']
adverbiaux_loc_temp_regionalisation_id::1::['récemment']

