
********** Entity Names Features **********
*******************************************

1. Sentence: Et qu ' un projet culturel peut influer sur un quartier délaissé , sans pour autant être une pièce rapportée pour le réhabiliter , mais en symbiose avec la vie qui s ' y développe .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Et qu ' un projet culturel peut influer sur un quartier délaissé , sans pour autant être une pièce rapportée pour le réhabiliter , mais en symbiose avec la vie qui s ' y développe .
Tokens having emotion: [ délaissé ]
Lemmas having emotion: [ délaisser ]
Categories of the emotion lemmas: [ tristesse ]


********** Modality Features **********
***************************************

1. Sentence: Et qu ' un projet culturel peut influer sur un quartier délaissé , sans pour autant être une pièce rapportée pour le réhabiliter , mais en symbiose avec la vie qui s ' y développe .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ pouvoir ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Et qu ' un projet culturel peut influer sur un quartier délaissé , sans pour autant être une pièce rapportée pour le réhabiliter , mais en symbiose avec la vie qui s ' y développe .
No features found.

