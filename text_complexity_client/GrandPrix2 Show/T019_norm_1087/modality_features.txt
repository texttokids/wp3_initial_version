====== Sentence Level Features ======

1. Sentence: Idem pour le temps libéré par la productivité croissante  : soit la grande masse des gens en profite pour de nouvelles activités créatives , marchandes ou pas , soit on crée des « hommes inutiles » , comme dit Pierre-Noël Giraud 12 , bloqués dans des trappes de chômage , pendant que d ' autres s ' épuisent dans des horaires insensés .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ croissant, grand, inutile, dire, chômage ]
appreciatif :: 3 :: [ croissant, inutile, chômage ]
marqueurs_niveau2 :: 3 :: [ croissant, inutile, chômage ]
appreciatif_niveau2 :: 3 :: [ croissant, inutile, chômage ]
marqueurs_niveau3 :: 2 :: [ grand, dire ]
boulique :: 1 :: [ dire ]
epistemique :: 1 :: [ grand ]
boulique_niveau3 :: 1 :: [ dire ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 20


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ croissant, grand, inutile, dire, chômage ]
appreciatif :: 3 :: [ croissant, inutile, chômage ]
marqueurs_niveau2 :: 3 :: [ croissant, inutile, chômage ]
appreciatif_niveau2 :: 3 :: [ croissant, inutile, chômage ]
marqueurs_niveau3 :: 2 :: [ grand, dire ]
boulique :: 1 :: [ dire ]
epistemique :: 1 :: [ grand ]
boulique_niveau3 :: 1 :: [ dire ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 20

