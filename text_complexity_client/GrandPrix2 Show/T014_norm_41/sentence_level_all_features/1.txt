
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai ainsi souvent trouvé un grand plaisir à entendre les élus et les différents intervenants exposer à leur manière les démarches élaborées .
Entity Name (Type) :: J (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai ainsi souvent trouvé un grand plaisir à entendre les élus et les différents intervenants exposer à leur manière les démarches élaborées .
Tokens having emotion: [ plaisir ]
Lemmas having emotion: [ plaisir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai ainsi souvent trouvé un grand plaisir à entendre les élus et les différents intervenants exposer à leur manière les démarches élaborées .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ souvent, grand, plaisir, élu ]
marqueurs_niveau3 :: 3 :: [ souvent, grand, plaisir ]
alethique :: 1 :: [ souvent ]
appreciatif :: 1 :: [ plaisir ]
epistemique :: 1 :: [ grand ]
pas_d_indication :: 1 :: [ élu ]
marqueurs_niveau0 :: 1 :: [ élu ]
alethique_niveau3 :: 1 :: [ souvent ]
appreciatif_niveau3 :: 1 :: [ plaisir ]
epistemique_niveau3 :: 1 :: [ grand ]
pas_d_indication_niveau0 :: 1 :: [ élu ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai ainsi souvent trouvé un grand plaisir à entendre les élus et les différents intervenants exposer à leur manière les démarches élaborées .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

