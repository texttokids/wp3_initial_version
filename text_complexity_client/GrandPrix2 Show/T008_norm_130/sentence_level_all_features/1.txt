
********** Entity Names Features **********
*******************************************

1. Sentence: Je mène donc d ' abord un travail sur le rapport ville-campagne , la perception à grande échelle , en constituant des « îles » bâties , posées « à bords francs » sur les grands espaces de campagne et de marais écologiquement intéressants , et en préservant les villages existants .
Entity Name (Type) :: Je (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Je mène donc d ' abord un travail sur le rapport ville-campagne , la perception à grande échelle , en constituant des « îles » bâties , posées « à bords francs » sur les grands espaces de campagne et de marais écologiquement intéressants , et en préservant les villages existants .
Tokens having emotion: [ intéressants ]
Lemmas having emotion: [ intéressant ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: Je mène donc d ' abord un travail sur le rapport ville-campagne , la perception à grande échelle , en constituant des « îles » bâties , posées « à bords francs » sur les grands espaces de campagne et de marais écologiquement intéressants , et en préservant les villages existants .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ travail, grand, grand ]
marqueurs_niveau3 :: 3 :: [ travail, grand, grand ]
epistemique :: 2 :: [ grand, grand ]
epistemique_niveau3 :: 2 :: [ grand, grand ]
appreciatif :: 1 :: [ travail ]
appreciatif_niveau3 :: 1 :: [ travail ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je mène donc d ' abord un travail sur le rapport ville-campagne , la perception à grande échelle , en constituant des « îles » bâties , posées « à bords francs » sur les grands espaces de campagne et de marais écologiquement intéressants , et en préservant les villages existants .
No features found.

