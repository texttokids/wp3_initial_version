
********** Entity Names Features **********
*******************************************

1. Sentence: Celle-ci , très belle en soi , me paraît ne pas être en harmonie avec l ' Arche , car trop haute à proportion de cette dernière , et surtout trop proche d ' elle .
Entity Name (Type) :: Arche (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Celle-ci , très belle en soi , me paraît ne pas être en harmonie avec l ' Arche , car trop haute à proportion de cette dernière , et surtout trop proche d ' elle .
Tokens having emotion: [ harmonie ]
Lemmas having emotion: [ harmonie ]
Categories of the emotion lemmas: [ apaisement ]


********** Modality Features **********
***************************************

1. Sentence: Celle-ci , très belle en soi , me paraît ne pas être en harmonie avec l ' Arche , car trop haute à proportion de cette dernière , et surtout trop proche d ' elle .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ beau, paraître, haut ]
marqueurs_niveau3 :: 2 :: [ beau, paraître ]
appreciatif :: 1 :: [ beau ]
epistemique :: 1 :: [ paraître ]
pas_d_indication :: 1 :: [ haut ]
marqueurs_niveau0 :: 1 :: [ haut ]
appreciatif_niveau3 :: 1 :: [ beau ]
epistemique_niveau3 :: 1 :: [ paraître ]
pas_d_indication_niveau0 :: 1 :: [ haut ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Celle-ci , très belle en soi , me paraît ne pas être en harmonie avec l ' Arche , car trop haute à proportion de cette dernière , et surtout trop proche d ' elle .
No features found.

