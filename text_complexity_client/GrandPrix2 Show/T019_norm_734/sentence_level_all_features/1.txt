
********** Entity Names Features **********
*******************************************

1. Sentence: La bataille du foncier Pour préserver les terres agricoles , et pour créer des quartiers plus compacts , nous avons très vite décidé de développer les nouveaux projets en densifiant les vastes campus existants établis sur du foncier d ' État (École polytechnique , université de Paris-Sud) .
Entity Name (Type) :: université de Paris-Sud (ORG)


********** Emotions Features **********
***************************************

1. Sentence: La bataille du foncier Pour préserver les terres agricoles , et pour créer des quartiers plus compacts , nous avons très vite décidé de développer les nouveaux projets en densifiant les vastes campus existants établis sur du foncier d ' État (École polytechnique , université de Paris-Sud) .
Tokens having emotion: [ État ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: La bataille du foncier Pour préserver les terres agricoles , et pour créer des quartiers plus compacts , nous avons très vite décidé de développer les nouveaux projets en densifiant les vastes campus existants établis sur du foncier d ' État (École polytechnique , université de Paris-Sud) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ bataille, vite, décider, école ]
epistemique :: 2 :: [ vite, décider ]
marqueurs_niveau3 :: 2 :: [ vite, décider ]
epistemique_niveau3 :: 2 :: [ vite, décider ]
alethique :: 1 :: [ école ]
axiologique :: 1 :: [ bataille ]
boulique :: 1 :: [ décider ]
marqueurs_niveau2 :: 1 :: [ bataille ]
marqueurs_niveau1 :: 1 :: [ école ]
alethique_niveau1 :: 1 :: [ école ]
axiologique_niveau2 :: 1 :: [ bataille ]
boulique_niveau3 :: 1 :: [ décider ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La bataille du foncier Pour préserver les terres agricoles , et pour créer des quartiers plus compacts , nous avons très vite décidé de développer les nouveaux projets en densifiant les vastes campus existants établis sur du foncier d ' État (École polytechnique , université de Paris-Sud) .
No features found.

