
********** Entity Names Features **********
*******************************************

1. Sentence: De la terre à la ville C ' est ainsi qu ' il me semble avoir toujours procédé .
Entity Name (Type) :: C (LOC)


********** Emotions Features **********
***************************************

1. Sentence: De la terre à la ville C ' est ainsi qu ' il me semble avoir toujours procédé .
No features found.


********** Modality Features **********
***************************************

1. Sentence: De la terre à la ville C ' est ainsi qu ' il me semble avoir toujours procédé .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ sembler, toujours ]
marqueurs_niveau3 :: 2 :: [ sembler, toujours ]
alethique :: 1 :: [ toujours ]
epistemique :: 1 :: [ sembler ]
alethique_niveau3 :: 1 :: [ toujours ]
epistemique_niveau3 :: 1 :: [ sembler ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: De la terre à la ville C ' est ainsi qu ' il me semble avoir toujours procédé .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['toujours']
adverbiaux_duratifs_iteratifs::1::['toujours']

