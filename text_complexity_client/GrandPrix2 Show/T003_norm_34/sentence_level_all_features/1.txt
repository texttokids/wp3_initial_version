
********** Entity Names Features **********
*******************************************

1. Sentence: Une marche « en dansant » , je peux presque le dire , tant les cours de danse ont rythmé alors mon quotidien , jusqu ' au point où j ' ai plus tard hésité pour mes études entre l ' archi tecture et la chorégraphie .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Une marche « en dansant » , je peux presque le dire , tant les cours de danse ont rythmé alors mon quotidien , jusqu ' au point où j ' ai plus tard hésité pour mes études entre l ' archi tecture et la chorégraphie .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Une marche « en dansant » , je peux presque le dire , tant les cours de danse ont rythmé alors mon quotidien , jusqu ' au point où j ' ai plus tard hésité pour mes études entre l ' archi tecture et la chorégraphie .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ danser, pouvoir, dire ]
marqueurs_niveau3 :: 2 :: [ pouvoir, dire ]
alethique :: 1 :: [ pouvoir ]
boulique :: 1 :: [ dire ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ danser ]
marqueurs_niveau0 :: 1 :: [ danser ]
alethique_niveau3 :: 1 :: [ pouvoir ]
boulique_niveau3 :: 1 :: [ dire ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ danser ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Une marche « en dansant » , je peux presque le dire , tant les cours de danse ont rythmé alors mon quotidien , jusqu ' au point où j ' ai plus tard hésité pour mes études entre l ' archi tecture et la chorégraphie .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['plus tard']
adverbiaux_localisation_temporelle::1::['plus tard']
adverbiaux_pointage_non_absolu::1::['plus tard']
adverbiaux_loc_temp_regionalisation_id::1::['plus tard']
adverbiaux_loc_temp_pointage_non_absolu::1::['plus tard']

