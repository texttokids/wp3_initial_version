
********** Entity Names Features **********
*******************************************

1. Sentence: Mais l ' idée de nature qui avait guidé sa conception au milieu du xixe siècle n ' avait plus cours depuis longtemps .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Mais l ' idée de nature qui avait guidé sa conception au milieu du xixe siècle n ' avait plus cours depuis longtemps .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Mais l ' idée de nature qui avait guidé sa conception au milieu du xixe siècle n ' avait plus cours depuis longtemps .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ longtemps ]
epistemique :: 1 :: [ longtemps ]
marqueurs_niveau1 :: 1 :: [ longtemps ]
epistemique_niveau1 :: 1 :: [ longtemps ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais l ' idée de nature qui avait guidé sa conception au milieu du xixe siècle n ' avait plus cours depuis longtemps .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['au milieu du xixe siècle', 'depuis longtemps']
adverbiaux_localisation_temporelle::1::['au milieu du xixe siècle']
adverbiaux_duratifs_iteratifs::1::['depuis longtemps']
adverbiaux_pointage_absolu::1::['au milieu du xixe siècle']
adverbiaux_pointage_non_absolu::1::['depuis longtemps']
adverbiaux_loc_temp_focalisation_id::1::['depuis longtemps']
adverbiaux_loc_temp_focalisation_non_id::1::['au milieu du xixe siècle']
adverbiaux_loc_temp_regionalisation_id::1::['au milieu du xixe siècle']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis longtemps']
adverbiaux_loc_temp_pointage_absolu::1::['au milieu du xixe siècle']
adverbiaux_dur_iter_anaphorique::1::['depuis longtemps']

