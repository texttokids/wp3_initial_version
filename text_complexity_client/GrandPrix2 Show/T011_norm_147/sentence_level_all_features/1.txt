
********** Entity Names Features **********
*******************************************

1. Sentence: Plus tard , avec l ' équipe de la Scet , nous devions prendre le même plaisir à travailler , en complicité avec Patrick Braouezec , à faire émerger la solution du grand stade à Saint-Denis .
Entity Name (Type) :: Saint-Denis (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Plus tard , avec l ' équipe de la Scet , nous devions prendre le même plaisir à travailler , en complicité avec Patrick Braouezec , à faire émerger la solution du grand stade à Saint-Denis .
Tokens having emotion: [ plaisir ]
Lemmas having emotion: [ plaisir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Plus tard , avec l ' équipe de la Scet , nous devions prendre le même plaisir à travailler , en complicité avec Patrick Braouezec , à faire émerger la solution du grand stade à Saint-Denis .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ plaisir, travailler, grand ]
appreciatif :: 2 :: [ plaisir, travailler ]
marqueurs_niveau3 :: 2 :: [ plaisir, grand ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ travailler ]
appreciatif_niveau3 :: 1 :: [ plaisir ]
appreciatif_niveau2 :: 1 :: [ travailler ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Plus tard , avec l ' équipe de la Scet , nous devions prendre le même plaisir à travailler , en complicité avec Patrick Braouezec , à faire émerger la solution du grand stade à Saint-Denis .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['Plus tard']
adverbiaux_localisation_temporelle::1::['Plus tard']
adverbiaux_pointage_non_absolu::1::['Plus tard']
adverbiaux_loc_temp_regionalisation_id::1::['Plus tard']
adverbiaux_loc_temp_pointage_non_absolu::1::['Plus tard']

