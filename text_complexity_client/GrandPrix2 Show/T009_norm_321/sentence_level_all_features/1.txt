
********** Entity Names Features **********
*******************************************

1. Sentence: Je le considère comme un homme de non-compromission , un homme du compromis intelligent par rapport au devenir de la ville .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Je le considère comme un homme de non-compromission , un homme du compromis intelligent par rapport au devenir de la ville .
Tokens having emotion: [ considère ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Je le considère comme un homme de non-compromission , un homme du compromis intelligent par rapport au devenir de la ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ considérer, intelligent ]
appreciatif :: 1 :: [ intelligent ]
epistemique :: 1 :: [ considérer ]
marqueurs_niveau3 :: 1 :: [ considérer ]
marqueurs_niveau2 :: 1 :: [ intelligent ]
appreciatif_niveau2 :: 1 :: [ intelligent ]
epistemique_niveau3 :: 1 :: [ considérer ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je le considère comme un homme de non-compromission , un homme du compromis intelligent par rapport au devenir de la ville .
No features found.

