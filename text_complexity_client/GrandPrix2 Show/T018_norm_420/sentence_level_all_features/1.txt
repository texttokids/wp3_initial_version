
********** Entity Names Features **********
*******************************************

1. Sentence: Le plaisir de travailler dans sa région y est pour une part (« J ' ai grandi dans une propriété comme celle-là ») et aussi les gros projets .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Le plaisir de travailler dans sa région y est pour une part (« J ' ai grandi dans une propriété comme celle-là ») et aussi les gros projets .
Tokens having emotion: [ plaisir ]
Lemmas having emotion: [ plaisir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Le plaisir de travailler dans sa région y est pour une part (« J ' ai grandi dans une propriété comme celle-là ») et aussi les gros projets .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ plaisir, travailler, gros ]
appreciatif :: 2 :: [ plaisir, travailler ]
marqueurs_niveau2 :: 2 :: [ travailler, gros ]
epistemique :: 1 :: [ gros ]
marqueurs_niveau3 :: 1 :: [ plaisir ]
appreciatif_niveau3 :: 1 :: [ plaisir ]
appreciatif_niveau2 :: 1 :: [ travailler ]
epistemique_niveau2 :: 1 :: [ gros ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Le plaisir de travailler dans sa région y est pour une part (« J ' ai grandi dans une propriété comme celle-là ») et aussi les gros projets .
No features found.

