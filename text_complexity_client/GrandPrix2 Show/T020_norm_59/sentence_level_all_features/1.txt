
********** Entity Names Features **********
*******************************************

1. Sentence: À l ' heure actuelle , certains programmes ont déjà trouvé leur place sur le site  : le lycée hôtelier qui s ' adresse sur le passage couvert , une halle gastronomique , des artisans , la Bourse du Travail , une piscine , un parking silo .
Entity Name (Type) :: Bourse du Travail (ORG)


********** Emotions Features **********
***************************************

1. Sentence: À l ' heure actuelle , certains programmes ont déjà trouvé leur place sur le site  : le lycée hôtelier qui s ' adresse sur le passage couvert , une halle gastronomique , des artisans , la Bourse du Travail , une piscine , un parking silo .
No features found.


********** Modality Features **********
***************************************

1. Sentence: À l ' heure actuelle , certains programmes ont déjà trouvé leur place sur le site  : le lycée hôtelier qui s ' adresse sur le passage couvert , une halle gastronomique , des artisans , la Bourse du Travail , une piscine , un parking silo .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ certain, travail ]
marqueurs_niveau3 :: 2 :: [ certain, travail ]
appreciatif :: 1 :: [ travail ]
epistemique :: 1 :: [ certain ]
appreciatif_niveau3 :: 1 :: [ travail ]
epistemique_niveau3 :: 1 :: [ certain ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: À l ' heure actuelle , certains programmes ont déjà trouvé leur place sur le site  : le lycée hôtelier qui s ' adresse sur le passage couvert , une halle gastronomique , des artisans , la Bourse du Travail , une piscine , un parking silo .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['À l ` heure actuelle']
adverbiaux_localisation_temporelle::1::['À l ` heure actuelle']
adverbiaux_pointage_non_absolu::1::['À l ` heure actuelle']
adverbiaux_loc_temp_focalisation_id::1::['À l ` heure actuelle']
adverbiaux_loc_temp_regionalisation_id::1::['À l ` heure actuelle']
adverbiaux_loc_temp_pointage_non_absolu::1::['À l ` heure actuelle']

