====== Sentence Level Features ======

1. Sentence: Des chocs , il y en eut bien d ' autres au cours de ces aventures photographiques , et je crois avoir , au fil des ans , vu , à l ' exception du Dacca de Louis Kahn ,  l ' essentiel de ce que l ' architecture du XXe siècle a produit et qui compte , les œuvres les plus sophistiquées et les plus ordinaires .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['du XXe siècle', 'au fil des ans']
adverbiaux_localisation_temporelle::1::['du XXe siècle']
adverbiaux_duratifs_iteratifs::1::['au fil des ans']
adverbiaux_pointage_absolu::1::['du XXe siècle']
adverbiaux_pointage_non_absolu::1::['au fil des ans']
adverbiaux_loc_temp_focalisation_id::1::['du XXe siècle']
adverbiaux_loc_temp_regionalisation_id::1::['du XXe siècle']
adverbiaux_loc_temp_pointage_absolu::1::['du XXe siècle']
adverbiaux_dur_iter_anaphorique::1::['au fil des ans']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['du XXe siècle', 'au fil des ans']
adverbiaux_localisation_temporelle::1::['du XXe siècle']
adverbiaux_duratifs_iteratifs::1::['au fil des ans']
adverbiaux_pointage_absolu::1::['du XXe siècle']
adverbiaux_pointage_non_absolu::1::['au fil des ans']
adverbiaux_loc_temp_focalisation_id::1::['du XXe siècle']
adverbiaux_loc_temp_regionalisation_id::1::['du XXe siècle']
adverbiaux_loc_temp_pointage_absolu::1::['du XXe siècle']
adverbiaux_dur_iter_anaphorique::1::['au fil des ans']
Total feature counts: 10

