====== Sentence Level Features ======

1. Sentence: Peut-on , en 2016 , afﬁrmer « la ﬁerté d ' être fonctionnaire » , à l ' heure où la fonction publique est estimée en partie responsable de la mauvaise situation ﬁnancière française , de lourdeurs , de freins à l ' innovation , d ' inefﬁcacité et de tant d ' autres maux ?
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, responsable, mauvais, mal ]
marqueurs_niveau3 :: 3 :: [ pouvoir, mauvais, mal ]
appreciatif :: 2 :: [ mauvais, mal ]
axiologique :: 2 :: [ mauvais, mal ]
deontique :: 2 :: [ pouvoir, responsable ]
appreciatif_niveau3 :: 2 :: [ mauvais, mal ]
axiologique_niveau3 :: 2 :: [ mauvais, mal ]
alethique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ responsable ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 24


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, responsable, mauvais, mal ]
marqueurs_niveau3 :: 3 :: [ pouvoir, mauvais, mal ]
appreciatif :: 2 :: [ mauvais, mal ]
axiologique :: 2 :: [ mauvais, mal ]
deontique :: 2 :: [ pouvoir, responsable ]
appreciatif_niveau3 :: 2 :: [ mauvais, mal ]
axiologique_niveau3 :: 2 :: [ mauvais, mal ]
alethique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ responsable ]
alethique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau3 :: 1 :: [ pouvoir ]
deontique_niveau2 :: 1 :: [ responsable ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
Total feature counts: 24

