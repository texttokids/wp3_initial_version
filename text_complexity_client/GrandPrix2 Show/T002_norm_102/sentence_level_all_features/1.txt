
********** Entity Names Features **********
*******************************************

1. Sentence: Ces exigences de services et de qualité urbaine n ' échappent pas à certaines tendances comme le besoin de réintroduire la nature en ville , dans toutes ses composantes et tous ses types , et le rêve , souvent improbable , d ' une vie de village et d ' un territoire social de proximité partagé où se situent les rencontres et la mixité urbaine .
Entity Name (Type) :: Ces (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ces exigences de services et de qualité urbaine n ' échappent pas à certaines tendances comme le besoin de réintroduire la nature en ville , dans toutes ses composantes et tous ses types , et le rêve , souvent improbable , d ' une vie de village et d ' un territoire social de proximité partagé où se situent les rencontres et la mixité urbaine .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Ces exigences de services et de qualité urbaine n ' échappent pas à certaines tendances comme le besoin de réintroduire la nature en ville , dans toutes ses composantes et tous ses types , et le rêve , souvent improbable , d ' une vie de village et d ' un territoire social de proximité partagé où se situent les rencontres et la mixité urbaine .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ échapper, certain, besoin, rêve, souvent, improbable ]
alethique :: 3 :: [ besoin, rêve, souvent ]
marqueurs_niveau3 :: 3 :: [ certain, besoin, souvent ]
epistemique :: 2 :: [ certain, improbable ]
alethique_niveau3 :: 2 :: [ besoin, souvent ]
appreciatif :: 1 :: [ rêve ]
boulique :: 1 :: [ besoin ]
pas_d_indication :: 1 :: [ échapper ]
marqueurs_niveau2 :: 1 :: [ improbable ]
marqueurs_niveau1 :: 1 :: [ rêve ]
marqueurs_niveau0 :: 1 :: [ échapper ]
alethique_niveau1 :: 1 :: [ rêve ]
appreciatif_niveau1 :: 1 :: [ rêve ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ certain ]
epistemique_niveau2 :: 1 :: [ improbable ]
pas_d_indication_niveau0 :: 1 :: [ échapper ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ces exigences de services et de qualité urbaine n ' échappent pas à certaines tendances comme le besoin de réintroduire la nature en ville , dans toutes ses composantes et tous ses types , et le rêve , souvent improbable , d ' une vie de village et d ' un territoire social de proximité partagé où se situent les rencontres et la mixité urbaine .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['souvent']
adverbiaux_purement_iteratifs::1::['souvent']
adverbiaux_iterateur_frequentiel::1::['souvent']

