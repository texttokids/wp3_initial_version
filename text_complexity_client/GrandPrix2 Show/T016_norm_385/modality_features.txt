====== Sentence Level Features ======

1. Sentence: Certaines expériences de concours pour l ' aménagement le long de l ' Escaut , par exemple , ont été l ' occasion d ' acquérir une meilleure compréhension du rôle des différents éléments spatiaux au regard des conditions de marée et de l ' environnement urbain : depuis le besoin de préserver le centre-ville en augmentant les digues actuelles espaces portuaires en friche , jusqu ' à la recherche d ' une intégration urbaine forte sur la section de berge entre le fleuve et les nouvelles zones portuaires et , enfin , jusqu ' au réaménagement de la zone d ' expan sion du fleuve , actuellement en cours , en amont de la ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ certain, long, meilleur, besoin, fort ]
epistemique :: 2 :: [ certain, long ]
marqueurs_niveau3 :: 2 :: [ certain, besoin ]
marqueurs_niveau2 :: 2 :: [ long, meilleur ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ meilleur ]
axiologique :: 1 :: [ meilleur ]
boulique :: 1 :: [ besoin ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau0 :: 1 :: [ fort ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau2 :: 1 :: [ meilleur ]
axiologique_niveau2 :: 1 :: [ meilleur ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ certain ]
epistemique_niveau2 :: 1 :: [ long ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 24


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ certain, long, meilleur, besoin, fort ]
epistemique :: 2 :: [ certain, long ]
marqueurs_niveau3 :: 2 :: [ certain, besoin ]
marqueurs_niveau2 :: 2 :: [ long, meilleur ]
alethique :: 1 :: [ besoin ]
appreciatif :: 1 :: [ meilleur ]
axiologique :: 1 :: [ meilleur ]
boulique :: 1 :: [ besoin ]
pas_d_indication :: 1 :: [ fort ]
marqueurs_niveau0 :: 1 :: [ fort ]
alethique_niveau3 :: 1 :: [ besoin ]
appreciatif_niveau2 :: 1 :: [ meilleur ]
axiologique_niveau2 :: 1 :: [ meilleur ]
boulique_niveau3 :: 1 :: [ besoin ]
epistemique_niveau3 :: 1 :: [ certain ]
epistemique_niveau2 :: 1 :: [ long ]
pas_d_indication_niveau0 :: 1 :: [ fort ]
Total feature counts: 24

