
********** Entity Names Features **********
*******************************************

1. Sentence: Je partageais sa vision sur des constats basiques , qui me paraissaient d ' évidence  : la nécessité de raisonner à l ' échelle de la grande agglomération , d ' effacer le périphérique ,  l ' urgence pour le Grand Paris de rester dans le peloton de tête des villes mondiales , non pour des raisons de prestige , mais parce qu ' il en allait de la position globale de la France .
Entity Name (Type) :: la France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Je partageais sa vision sur des constats basiques , qui me paraissaient d ' évidence  : la nécessité de raisonner à l ' échelle de la grande agglomération , d ' effacer le périphérique ,  l ' urgence pour le Grand Paris de rester dans le peloton de tête des villes mondiales , non pour des raisons de prestige , mais parce qu ' il en allait de la position globale de la France .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Je partageais sa vision sur des constats basiques , qui me paraissaient d ' évidence  : la nécessité de raisonner à l ' échelle de la grande agglomération , d ' effacer le périphérique ,  l ' urgence pour le Grand Paris de rester dans le peloton de tête des villes mondiales , non pour des raisons de prestige , mais parce qu ' il en allait de la position globale de la France .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 7 :: [ paraître, nécessité, grand, urgence, grand, prestige, aller ]
marqueurs_niveau3 :: 4 :: [ paraître, nécessité, grand, grand ]
epistemique :: 3 :: [ paraître, grand, grand ]
epistemique_niveau3 :: 3 :: [ paraître, grand, grand ]
alethique :: 2 :: [ nécessité, aller ]
appreciatif :: 1 :: [ prestige ]
pas_d_indication :: 1 :: [ urgence ]
marqueurs_niveau2 :: 1 :: [ prestige ]
marqueurs_niveau1 :: 1 :: [ aller ]
marqueurs_niveau0 :: 1 :: [ urgence ]
alethique_niveau3 :: 1 :: [ nécessité ]
alethique_niveau1 :: 1 :: [ aller ]
appreciatif_niveau2 :: 1 :: [ prestige ]
pas_d_indication_niveau0 :: 1 :: [ urgence ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je partageais sa vision sur des constats basiques , qui me paraissaient d ' évidence  : la nécessité de raisonner à l ' échelle de la grande agglomération , d ' effacer le périphérique ,  l ' urgence pour le Grand Paris de rester dans le peloton de tête des villes mondiales , non pour des raisons de prestige , mais parce qu ' il en allait de la position globale de la France .
No features found.

