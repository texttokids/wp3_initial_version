====== Sentence Level Features ======

1. Sentence: Il est évident qu ' une action d ' urbaniste ne va pas bousculer les inégalités de manière mécanique , mais il est important de se poser la question , de réfléchir aux choix , aux alliances que l ' on va passer , à ce que notre action favorise .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ aller, inégalité, important, choix, aller, favoriser ]
marqueurs_niveau1 :: 4 :: [ aller, important, aller, favoriser ]
alethique :: 2 :: [ aller, aller ]
appreciatif :: 2 :: [ important, favoriser ]
alethique_niveau1 :: 2 :: [ aller, aller ]
appreciatif_niveau1 :: 2 :: [ important, favoriser ]
axiologique :: 1 :: [ important ]
boulique :: 1 :: [ choix ]
epistemique :: 1 :: [ important ]
pas_d_indication :: 1 :: [ inégalité ]
marqueurs_niveau2 :: 1 :: [ choix ]
marqueurs_niveau0 :: 1 :: [ inégalité ]
axiologique_niveau1 :: 1 :: [ important ]
boulique_niveau2 :: 1 :: [ choix ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ inégalité ]
Total feature counts: 28


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ aller, inégalité, important, choix, aller, favoriser ]
marqueurs_niveau1 :: 4 :: [ aller, important, aller, favoriser ]
alethique :: 2 :: [ aller, aller ]
appreciatif :: 2 :: [ important, favoriser ]
alethique_niveau1 :: 2 :: [ aller, aller ]
appreciatif_niveau1 :: 2 :: [ important, favoriser ]
axiologique :: 1 :: [ important ]
boulique :: 1 :: [ choix ]
epistemique :: 1 :: [ important ]
pas_d_indication :: 1 :: [ inégalité ]
marqueurs_niveau2 :: 1 :: [ choix ]
marqueurs_niveau0 :: 1 :: [ inégalité ]
axiologique_niveau1 :: 1 :: [ important ]
boulique_niveau2 :: 1 :: [ choix ]
epistemique_niveau1 :: 1 :: [ important ]
pas_d_indication_niveau0 :: 1 :: [ inégalité ]
Total feature counts: 28

