
********** Entity Names Features **********
*******************************************

1. Sentence: Les transports en commun doivent à l ' inverse proposer un plaisir à l ' usager .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les transports en commun doivent à l ' inverse proposer un plaisir à l ' usager .
Tokens having emotion: [ plaisir ]
Lemmas having emotion: [ plaisir ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Les transports en commun doivent à l ' inverse proposer un plaisir à l ' usager .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ devoir, plaisir ]
marqueurs_niveau3 :: 2 :: [ devoir, plaisir ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ plaisir ]
deontique :: 1 :: [ devoir ]
epistemique :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau3 :: 1 :: [ plaisir ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les transports en commun doivent à l ' inverse proposer un plaisir à l ' usager .
No features found.

