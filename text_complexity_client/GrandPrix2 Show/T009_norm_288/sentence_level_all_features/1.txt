
********** Entity Names Features **********
*******************************************

1. Sentence: Je suis frappé par leur incroyable capacité à s ' approprier les sites , par l ' efficacité de la rencontre des cultures berbère et arabe et aussi un peu européenne lorsqu ' il s ' agit de fabriquer la texture de la ville .
Entity Name (Type) :: Je (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Je suis frappé par leur incroyable capacité à s ' approprier les sites , par l ' efficacité de la rencontre des cultures berbère et arabe et aussi un peu européenne lorsqu ' il s ' agit de fabriquer la texture de la ville .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Je suis frappé par leur incroyable capacité à s ' approprier les sites , par l ' efficacité de la rencontre des cultures berbère et arabe et aussi un peu européenne lorsqu ' il s ' agit de fabriquer la texture de la ville .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ frapper, incroyable ]
appreciatif :: 2 :: [ frapper, incroyable ]
axiologique :: 1 :: [ frapper ]
epistemique :: 1 :: [ incroyable ]
marqueurs_niveau2 :: 1 :: [ incroyable ]
marqueurs_niveau1 :: 1 :: [ frapper ]
appreciatif_niveau2 :: 1 :: [ incroyable ]
appreciatif_niveau1 :: 1 :: [ frapper ]
axiologique_niveau1 :: 1 :: [ frapper ]
epistemique_niveau2 :: 1 :: [ incroyable ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Je suis frappé par leur incroyable capacité à s ' approprier les sites , par l ' efficacité de la rencontre des cultures berbère et arabe et aussi un peu européenne lorsqu ' il s ' agit de fabriquer la texture de la ville .
No features found.

