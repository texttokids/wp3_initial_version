
********** Entity Names Features **********
*******************************************

1. Sentence: Partout dans le quartier , les espaces publics créés s ' orientent au soleil , s ' étirent vers la Loire , rangent les voitures à leur place , proposent des cheminements fluides aux piétons .
Entity Name (Type) :: Loire (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Partout dans le quartier , les espaces publics créés s ' orientent au soleil , s ' étirent vers la Loire , rangent les voitures à leur place , proposent des cheminements fluides aux piétons .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Partout dans le quartier , les espaces publics créés s ' orientent au soleil , s ' étirent vers la Loire , rangent les voitures à leur place , proposent des cheminements fluides aux piétons .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ partout, soleil ]
alethique :: 1 :: [ partout ]
appreciatif :: 1 :: [ soleil ]
marqueurs_niveau3 :: 1 :: [ soleil ]
marqueurs_niveau2 :: 1 :: [ partout ]
alethique_niveau2 :: 1 :: [ partout ]
appreciatif_niveau3 :: 1 :: [ soleil ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Partout dans le quartier , les espaces publics créés s ' orientent au soleil , s ' étirent vers la Loire , rangent les voitures à leur place , proposent des cheminements fluides aux piétons .
No features found.

