
********** Entity Names Features **********
*******************************************

1. Sentence: Il est faux que j ' ai refusé de participer à la consultation du Grand Paris qui m ' intéressait et je pense que , si mon état de santé me l ' avait permis et si Christian de Portzamparc me l ' avait demandé , j ' aurais tenté l ' aventure .
Entity Name (Type) :: Christian de Portzamparc (PER)


********** Emotions Features **********
***************************************

1. Sentence: Il est faux que j ' ai refusé de participer à la consultation du Grand Paris qui m ' intéressait et je pense que , si mon état de santé me l ' avait permis et si Christian de Portzamparc me l ' avait demandé , j ' aurais tenté l ' aventure .
Tokens having emotion: [ intéressait, état, tenté ]
Lemmas having emotion: [ intéresser, état, tenter ]
Categories of the emotion lemmas: [ desir, non_specifiee, desir ]


********** Modality Features **********
***************************************

1. Sentence: Il est faux que j ' ai refusé de participer à la consultation du Grand Paris qui m ' intéressait et je pense que , si mon état de santé me l ' avait permis et si Christian de Portzamparc me l ' avait demandé , j ' aurais tenté l ' aventure .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 10 :: [ faux, refuser, grand, intéresser, penser, santé, permettre, demander, tenter, aventure ]
marqueurs_niveau3 :: 4 :: [ grand, penser, permettre, demander ]
boulique :: 3 :: [ refuser, demander, tenter ]
pas_d_indication :: 3 :: [ intéresser, santé, aventure ]
marqueurs_niveau0 :: 3 :: [ intéresser, santé, aventure ]
pas_d_indication_niveau0 :: 3 :: [ intéresser, santé, aventure ]
epistemique :: 2 :: [ grand, penser ]
marqueurs_niveau2 :: 2 :: [ refuser, tenter ]
boulique_niveau2 :: 2 :: [ refuser, tenter ]
epistemique_niveau3 :: 2 :: [ grand, penser ]
alethique :: 1 :: [ faux ]
deontique :: 1 :: [ permettre ]
marqueurs_niveau1 :: 1 :: [ faux ]
alethique_niveau1 :: 1 :: [ faux ]
boulique_niveau3 :: 1 :: [ demander ]
deontique_niveau3 :: 1 :: [ permettre ]
Total feature counts: 40


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il est faux que j ' ai refusé de participer à la consultation du Grand Paris qui m ' intéressait et je pense que , si mon état de santé me l ' avait permis et si Christian de Portzamparc me l ' avait demandé , j ' aurais tenté l ' aventure .
No features found.

