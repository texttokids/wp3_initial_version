
********** Entity Names Features **********
*******************************************

1. Sentence: En 1986 , Michel Corajoud est nommé Maître de Conférence de « théorie et pratique du projet sur le paysage » et rédige à cette occasion un document sur la pédagogie de l ' enseignement qui fondera largement et la discipline elle-même et le cadre de l ' enseignement à l ' ENSP .
Entity Name (Type) :: ENSP (ORG)


********** Emotions Features **********
***************************************

1. Sentence: En 1986 , Michel Corajoud est nommé Maître de Conférence de « théorie et pratique du projet sur le paysage » et rédige à cette occasion un document sur la pédagogie de l ' enseignement qui fondera largement et la discipline elle-même et le cadre de l ' enseignement à l ' ENSP .
No features found.


********** Modality Features **********
***************************************

1. Sentence: En 1986 , Michel Corajoud est nommé Maître de Conférence de « théorie et pratique du projet sur le paysage » et rédige à cette occasion un document sur la pédagogie de l ' enseignement qui fondera largement et la discipline elle-même et le cadre de l ' enseignement à l ' ENSP .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: En 1986 , Michel Corajoud est nommé Maître de Conférence de « théorie et pratique du projet sur le paysage » et rédige à cette occasion un document sur la pédagogie de l ' enseignement qui fondera largement et la discipline elle-même et le cadre de l ' enseignement à l ' ENSP .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['En 1986']
adverbiaux_localisation_temporelle::1::['En 1986']
adverbiaux_pointage_absolu::1::['En 1986']
adverbiaux_pointage_non_absolu::1::['En 1986']
adverbiaux_loc_temp_focalisation_id::1::['En 1986']
adverbiaux_loc_temp_regionalisation_id::1::['En 1986']
adverbiaux_loc_temp_pointage_non_absolu::1::['En 1986']
adverbiaux_dur_iter_absolu::1::['En 1986']

