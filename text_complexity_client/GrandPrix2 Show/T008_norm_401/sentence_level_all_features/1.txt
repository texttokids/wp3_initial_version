
********** Entity Names Features **********
*******************************************

1. Sentence: Les immeubles , conçus et réalisés par divers architectes et maîtres d ' ouvrage , peuvent adopter des volumes et des expressions architecturales assez variés , car l ' alignement sur rue , dont la perception est renforcée par les proportions verticales , suffit à intégrer cette diversité .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Les immeubles , conçus et réalisés par divers architectes et maîtres d ' ouvrage , peuvent adopter des volumes et des expressions architecturales assez variés , car l ' alignement sur rue , dont la perception est renforcée par les proportions verticales , suffit à intégrer cette diversité .
Tokens having emotion: [ expressions ]
Lemmas having emotion: [ expression ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Les immeubles , conçus et réalisés par divers architectes et maîtres d ' ouvrage , peuvent adopter des volumes et des expressions architecturales assez variés , car l ' alignement sur rue , dont la perception est renforcée par les proportions verticales , suffit à intégrer cette diversité .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ réaliser, pouvoir, suffire ]
alethique :: 2 :: [ réaliser, pouvoir ]
deontique :: 1 :: [ pouvoir ]
epistemique :: 1 :: [ pouvoir ]
pas_d_indication :: 1 :: [ suffire ]
marqueurs_niveau3 :: 1 :: [ pouvoir ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
marqueurs_niveau0 :: 1 :: [ suffire ]
alethique_niveau3 :: 1 :: [ pouvoir ]
alethique_niveau1 :: 1 :: [ réaliser ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau3 :: 1 :: [ pouvoir ]
pas_d_indication_niveau0 :: 1 :: [ suffire ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Les immeubles , conçus et réalisés par divers architectes et maîtres d ' ouvrage , peuvent adopter des volumes et des expressions architecturales assez variés , car l ' alignement sur rue , dont la perception est renforcée par les proportions verticales , suffit à intégrer cette diversité .
No features found.

