
********** Entity Names Features **********
*******************************************

1. Sentence: Territoire riche en biodiversité , le bassin de la Durance est en France parmi les plus densément couverts d ' éléments arboricoles (haies , vergers traditionnels , forêts , arbres épars) et prairies permanentes (peu productives ou sans fertilisants chimiques enrichis en azote) .
Entity Name (Type) :: France (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Territoire riche en biodiversité , le bassin de la Durance est en France parmi les plus densément couverts d ' éléments arboricoles (haies , vergers traditionnels , forêts , arbres épars) et prairies permanentes (peu productives ou sans fertilisants chimiques enrichis en azote) .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Territoire riche en biodiversité , le bassin de la Durance est en France parmi les plus densément couverts d ' éléments arboricoles (haies , vergers traditionnels , forêts , arbres épars) et prairies permanentes (peu productives ou sans fertilisants chimiques enrichis en azote) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ riche, enrichir ]
appreciatif :: 2 :: [ riche, enrichir ]
marqueurs_niveau2 :: 2 :: [ riche, enrichir ]
appreciatif_niveau2 :: 2 :: [ riche, enrichir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Territoire riche en biodiversité , le bassin de la Durance est en France parmi les plus densément couverts d ' éléments arboricoles (haies , vergers traditionnels , forêts , arbres épars) et prairies permanentes (peu productives ou sans fertilisants chimiques enrichis en azote) .
No features found.

