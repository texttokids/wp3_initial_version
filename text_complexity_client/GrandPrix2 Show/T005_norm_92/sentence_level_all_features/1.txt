
********** Entity Names Features **********
*******************************************

1. Sentence: Lourde responsabilité qui me fait avoir une grande tendresse pour les élus qui osent agir .
Entity Name (Type) :: Lourde (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Lourde responsabilité qui me fait avoir une grande tendresse pour les élus qui osent agir .
Tokens having emotion: [ tendresse ]
Lemmas having emotion: [ tendresse ]
Categories of the emotion lemmas: [ amour ]


********** Modality Features **********
***************************************

1. Sentence: Lourde responsabilité qui me fait avoir une grande tendresse pour les élus qui osent agir .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ lourd, responsabilité, grand, élu, oser ]
epistemique :: 2 :: [ lourd, grand ]
marqueurs_niveau3 :: 2 :: [ lourd, grand ]
marqueurs_niveau2 :: 2 :: [ responsabilité, oser ]
epistemique_niveau3 :: 2 :: [ lourd, grand ]
appreciatif :: 1 :: [ oser ]
axiologique :: 1 :: [ oser ]
deontique :: 1 :: [ responsabilité ]
pas_d_indication :: 1 :: [ élu ]
marqueurs_niveau0 :: 1 :: [ élu ]
appreciatif_niveau2 :: 1 :: [ oser ]
axiologique_niveau2 :: 1 :: [ oser ]
deontique_niveau2 :: 1 :: [ responsabilité ]
pas_d_indication_niveau0 :: 1 :: [ élu ]
Total feature counts: 22


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Lourde responsabilité qui me fait avoir une grande tendresse pour les élus qui osent agir .
No features found.

