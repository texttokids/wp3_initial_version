
********** Entity Names Features **********
*******************************************

1. Sentence: Quand Pierre Mauroy , après la mort de Jean-Paul Baïetto me demande , en mars 1998 , de venir poursuivre la réalisation d ' Euralille , à la direction générale de la Sem , j ' hésite à accepter le défi .
Entity Name (Type) :: Sem (ORG)


********** Emotions Features **********
***************************************

1. Sentence: Quand Pierre Mauroy , après la mort de Jean-Paul Baïetto me demande , en mars 1998 , de venir poursuivre la réalisation d ' Euralille , à la direction générale de la Sem , j ' hésite à accepter le défi .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Quand Pierre Mauroy , après la mort de Jean-Paul Baïetto me demande , en mars 1998 , de venir poursuivre la réalisation d ' Euralille , à la direction générale de la Sem , j ' hésite à accepter le défi .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ mort, demander, venir ]
marqueurs_niveau3 :: 2 :: [ demander, venir ]
alethique :: 1 :: [ venir ]
appreciatif :: 1 :: [ mort ]
boulique :: 1 :: [ demander ]
marqueurs_niveau2 :: 1 :: [ mort ]
alethique_niveau3 :: 1 :: [ venir ]
appreciatif_niveau2 :: 1 :: [ mort ]
boulique_niveau3 :: 1 :: [ demander ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Quand Pierre Mauroy , après la mort de Jean-Paul Baïetto me demande , en mars 1998 , de venir poursuivre la réalisation d ' Euralille , à la direction générale de la Sem , j ' hésite à accepter le défi .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['en mars 1998']
adverbiaux_localisation_temporelle::1::['en mars 1998']
adverbiaux_pointage_absolu::1::['en mars 1998']
adverbiaux_pointage_non_absolu::1::['en mars 1998']
adverbiaux_loc_temp_focalisation_id::1::['en mars 1998']
adverbiaux_loc_temp_regionalisation_id::1::['en mars 1998']
adverbiaux_loc_temp_pointage_non_absolu::1::['en mars 1998']
adverbiaux_dur_iter_absolu::1::['en mars 1998']

