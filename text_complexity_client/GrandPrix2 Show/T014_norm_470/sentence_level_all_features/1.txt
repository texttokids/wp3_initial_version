
********** Entity Names Features **********
*******************************************

1. Sentence: Une première étape opérationnelle se réalise notamment rive droite , sur les quais du Châtelet , du Fort-Alleaume et du Roi , ainsi que , rive gauche , régulièrement inondée , avec le bois de l ' Île et l ' île Charlemagne .
Entity Name (Type) :: île Charlemagne (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Une première étape opérationnelle se réalise notamment rive droite , sur les quais du Châtelet , du Fort-Alleaume et du Roi , ainsi que , rive gauche , régulièrement inondée , avec le bois de l ' Île et l ' île Charlemagne .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Une première étape opérationnelle se réalise notamment rive droite , sur les quais du Châtelet , du Fort-Alleaume et du Roi , ainsi que , rive gauche , régulièrement inondée , avec le bois de l ' Île et l ' île Charlemagne .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ réaliser, droit, inonder ]
alethique :: 1 :: [ réaliser ]
deontique :: 1 :: [ droit ]
pas_d_indication :: 1 :: [ inonder ]
marqueurs_niveau3 :: 1 :: [ droit ]
marqueurs_niveau1 :: 1 :: [ réaliser ]
marqueurs_niveau0 :: 1 :: [ inonder ]
alethique_niveau1 :: 1 :: [ réaliser ]
deontique_niveau3 :: 1 :: [ droit ]
pas_d_indication_niveau0 :: 1 :: [ inonder ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Une première étape opérationnelle se réalise notamment rive droite , sur les quais du Châtelet , du Fort-Alleaume et du Roi , ainsi que , rive gauche , régulièrement inondée , avec le bois de l ' Île et l ' île Charlemagne .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['régulièrement']
adverbiaux_purement_iteratifs::1::['régulièrement']
adverbiaux_iterateur_frequentiel::1::['régulièrement']

