
********** Entity Names Features **********
*******************************************

1. Sentence: L ' urbanisme doit donc s ' inscrire dans la problématique du développement durable , qui implique de rendre aussi conciliables que possible les exigences de développement économique qui sont de l ' ordre de la «performance» , les exigences d ' équité sociale qui sont de l ' ordre de la «justice» , et les exigences environnementales qui sont de l ' ordre de l ' «éthique» .
Entity Name (Type) :: L (LOC)


********** Emotions Features **********
***************************************

1. Sentence: L ' urbanisme doit donc s ' inscrire dans la problématique du développement durable , qui implique de rendre aussi conciliables que possible les exigences de développement économique qui sont de l ' ordre de la «performance» , les exigences d ' équité sociale qui sont de l ' ordre de la «justice» , et les exigences environnementales qui sont de l ' ordre de l ' «éthique» .
No features found.


********** Modality Features **********
***************************************

1. Sentence: L ' urbanisme doit donc s ' inscrire dans la problématique du développement durable , qui implique de rendre aussi conciliables que possible les exigences de développement économique qui sont de l ' ordre de la «performance» , les exigences d ' équité sociale qui sont de l ' ordre de la «justice» , et les exigences environnementales qui sont de l ' ordre de l ' «éthique» .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ devoir, possible, ordre, ordre, justice, ordre ]
deontique :: 4 :: [ devoir, ordre, ordre, ordre ]
marqueurs_niveau2 :: 4 :: [ ordre, ordre, justice, ordre ]
deontique_niveau2 :: 3 :: [ ordre, ordre, ordre ]
alethique :: 2 :: [ devoir, possible ]
marqueurs_niveau3 :: 2 :: [ devoir, possible ]
alethique_niveau3 :: 2 :: [ devoir, possible ]
axiologique :: 1 :: [ justice ]
epistemique :: 1 :: [ devoir ]
axiologique_niveau2 :: 1 :: [ justice ]
deontique_niveau3 :: 1 :: [ devoir ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: L ' urbanisme doit donc s ' inscrire dans la problématique du développement durable , qui implique de rendre aussi conciliables que possible les exigences de développement économique qui sont de l ' ordre de la «performance» , les exigences d ' équité sociale qui sont de l ' ordre de la «justice» , et les exigences environnementales qui sont de l ' ordre de l ' «éthique» .
No features found.

