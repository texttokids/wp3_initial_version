
********** Entity Names Features **********
*******************************************

1. Sentence: Pour l ' avoir préconisé depuis 1975 , j ' ai la conviction que les consultations de concepteurs doivent démontrer que le respect de règles communes primordiales laisse place à des projets très différents et créatifs .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Pour l ' avoir préconisé depuis 1975 , j ' ai la conviction que les consultations de concepteurs doivent démontrer que le respect de règles communes primordiales laisse place à des projets très différents et créatifs .
Tokens having emotion: [ respect ]
Lemmas having emotion: [ respect ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Pour l ' avoir préconisé depuis 1975 , j ' ai la conviction que les consultations de concepteurs doivent démontrer que le respect de règles communes primordiales laisse place à des projets très différents et créatifs .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ devoir, respect, laisser ]
deontique :: 2 :: [ devoir, laisser ]
marqueurs_niveau2 :: 2 :: [ respect, laisser ]
alethique :: 1 :: [ devoir ]
appreciatif :: 1 :: [ respect ]
axiologique :: 1 :: [ respect ]
epistemique :: 1 :: [ devoir ]
marqueurs_niveau3 :: 1 :: [ devoir ]
alethique_niveau3 :: 1 :: [ devoir ]
appreciatif_niveau2 :: 1 :: [ respect ]
axiologique_niveau2 :: 1 :: [ respect ]
deontique_niveau3 :: 1 :: [ devoir ]
deontique_niveau2 :: 1 :: [ laisser ]
epistemique_niveau3 :: 1 :: [ devoir ]
Total feature counts: 18


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pour l ' avoir préconisé depuis 1975 , j ' ai la conviction que les consultations de concepteurs doivent démontrer que le respect de règles communes primordiales laisse place à des projets très différents et créatifs .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['depuis 1975']
adverbiaux_duratifs_iteratifs::1::['depuis 1975']
adverbiaux_pointage_absolu::1::['depuis 1975']
adverbiaux_loc_temp_focalisation_id::1::['depuis 1975']
adverbiaux_loc_temp_regionalisation_non_id::1::['depuis 1975']
adverbiaux_dur_iter_absolu::1::['depuis 1975']

