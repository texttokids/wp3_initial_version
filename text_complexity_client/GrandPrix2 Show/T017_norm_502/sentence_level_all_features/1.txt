
********** Entity Names Features **********
*******************************************

1. Sentence: Sans moyens , on revient à des matières premières  : paille , tourbe , terre , sciure , autant de substances organiques , fongibles , fournies par les locaux et utilisables une seule fois , un coup de balai suffisant à effacer la transformation de l ' espace .
Entity Name (Type) :: Sans (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Sans moyens , on revient à des matières premières  : paille , tourbe , terre , sciure , autant de substances organiques , fongibles , fournies par les locaux et utilisables une seule fois , un coup de balai suffisant à effacer la transformation de l ' espace .
Tokens having emotion: [ suffisant ]
Lemmas having emotion: [ suffisant ]
Categories of the emotion lemmas: [ orgueil ]


********** Modality Features **********
***************************************

1. Sentence: Sans moyens , on revient à des matières premières  : paille , tourbe , terre , sciure , autant de substances organiques , fongibles , fournies par les locaux et utilisables une seule fois , un coup de balai suffisant à effacer la transformation de l ' espace .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ suffisant ]
pas_d_indication :: 1 :: [ suffisant ]
marqueurs_niveau0 :: 1 :: [ suffisant ]
pas_d_indication_niveau0 :: 1 :: [ suffisant ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Sans moyens , on revient à des matières premières  : paille , tourbe , terre , sciure , autant de substances organiques , fongibles , fournies par les locaux et utilisables une seule fois , un coup de balai suffisant à effacer la transformation de l ' espace .
No features found.

