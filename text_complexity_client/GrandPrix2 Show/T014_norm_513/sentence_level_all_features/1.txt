
********** Entity Names Features **********
*******************************************

1. Sentence: Quand , en 1982-1983 , nous avons entrepris à l ' Apur le planprogramme de l ' est parisien — document de référence pour une vaste série d ' aménagements urbains dans sept arrondissements — , nous n ' avions ni méthode ni périmètre .
Entity Name (Type) :: Apur (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Quand , en 1982-1983 , nous avons entrepris à l ' Apur le planprogramme de l ' est parisien — document de référence pour une vaste série d ' aménagements urbains dans sept arrondissements — , nous n ' avions ni méthode ni périmètre .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Quand , en 1982-1983 , nous avons entrepris à l ' Apur le planprogramme de l ' est parisien — document de référence pour une vaste série d ' aménagements urbains dans sept arrondissements — , nous n ' avions ni méthode ni périmètre .
No features found.


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Quand , en 1982-1983 , nous avons entrepris à l ' Apur le planprogramme de l ' est parisien — document de référence pour une vaste série d ' aménagements urbains dans sept arrondissements — , nous n ' avions ni méthode ni périmètre .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['en 1982_1983', 'en 1982']
adverbiaux_localisation_temporelle::2::['en 1982_1983', 'en 1982']
adverbiaux_pointage_absolu::2::['en 1982_1983', 'en 1982']
adverbiaux_pointage_non_absolu::1::['en 1982']
adverbiaux_loc_temp_pointage_absolu::1::['en 1982_1983']
adverbiaux_loc_temp_pointage_non_absolu::1::['en 1982']
adverbiaux_dur_iter_absolu::1::['en 1982']

