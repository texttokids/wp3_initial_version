
********** Entity Names Features **********
*******************************************

1. Sentence: Il peut y avoir des désaccords , mais ceux-ci doivent être joyeux .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Il peut y avoir des désaccords , mais ceux-ci doivent être joyeux .
Tokens having emotion: [ joyeux ]
Lemmas having emotion: [ joyeux ]
Categories of the emotion lemmas: [ joie ]


********** Modality Features **********
***************************************

1. Sentence: Il peut y avoir des désaccords , mais ceux-ci doivent être joyeux .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ pouvoir, désaccord, devoir, joyeux ]
alethique :: 3 :: [ pouvoir, désaccord, devoir ]
appreciatif :: 2 :: [ désaccord, joyeux ]
deontique :: 2 :: [ pouvoir, devoir ]
epistemique :: 2 :: [ pouvoir, devoir ]
marqueurs_niveau3 :: 2 :: [ pouvoir, devoir ]
alethique_niveau3 :: 2 :: [ pouvoir, devoir ]
deontique_niveau3 :: 2 :: [ pouvoir, devoir ]
epistemique_niveau3 :: 2 :: [ pouvoir, devoir ]
axiologique :: 1 :: [ désaccord ]
marqueurs_niveau2 :: 1 :: [ joyeux ]
marqueurs_niveau1 :: 1 :: [ désaccord ]
alethique_niveau1 :: 1 :: [ désaccord ]
appreciatif_niveau2 :: 1 :: [ joyeux ]
appreciatif_niveau1 :: 1 :: [ désaccord ]
axiologique_niveau1 :: 1 :: [ désaccord ]
Total feature counts: 28


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Il peut y avoir des désaccords , mais ceux-ci doivent être joyeux .
No features found.

