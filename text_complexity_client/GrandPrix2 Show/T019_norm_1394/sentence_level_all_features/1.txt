
********** Entity Names Features **********
*******************************************

1. Sentence: Ma conviction profonde est qu ' il faudrait être beau‑ coup plus audacieux dans le rapprochement des formations d ' ingénieurs , de scientifiques , d ' architectes , de designer , de paysagistes .
Entity Name (Type) :: Ma (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Ma conviction profonde est qu ' il faudrait être beau‑ coup plus audacieux dans le rapprochement des formations d ' ingénieurs , de scientifiques , d ' architectes , de designer , de paysagistes .
Tokens having emotion: [ audacieux ]
Lemmas having emotion: [ audacieux ]
Categories of the emotion lemmas: [ audace ]


********** Modality Features **********
***************************************

1. Sentence: Ma conviction profonde est qu ' il faudrait être beau‑ coup plus audacieux dans le rapprochement des formations d ' ingénieurs , de scientifiques , d ' architectes , de designer , de paysagistes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 1 :: [ falloir ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau3 :: 1 :: [ falloir ]
deontique_niveau3 :: 1 :: [ falloir ]
Total feature counts: 4


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Ma conviction profonde est qu ' il faudrait être beau‑ coup plus audacieux dans le rapprochement des formations d ' ingénieurs , de scientifiques , d ' architectes , de designer , de paysagistes .
No features found.

