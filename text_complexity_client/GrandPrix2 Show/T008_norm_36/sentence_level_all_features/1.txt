
********** Entity Names Features **********
*******************************************

1. Sentence: J ' ai choisi le projet , abandonnant à regret ce contrat de recherche théorique .
Entity Name (Type) :: J (PER)


********** Emotions Features **********
***************************************

1. Sentence: J ' ai choisi le projet , abandonnant à regret ce contrat de recherche théorique .
Tokens having emotion: [ regret ]
Lemmas having emotion: [ regret ]
Categories of the emotion lemmas: [ culpabilite ]


********** Modality Features **********
***************************************

1. Sentence: J ' ai choisi le projet , abandonnant à regret ce contrat de recherche théorique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ choisir, abandonner, regret ]
marqueurs_niveau2 :: 3 :: [ choisir, abandonner, regret ]
boulique :: 2 :: [ choisir, abandonner ]
epistemique :: 2 :: [ abandonner, regret ]
boulique_niveau2 :: 2 :: [ choisir, abandonner ]
epistemique_niveau2 :: 2 :: [ abandonner, regret ]
appreciatif :: 1 :: [ regret ]
appreciatif_niveau2 :: 1 :: [ regret ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: J ' ai choisi le projet , abandonnant à regret ce contrat de recherche théorique .
No features found.

