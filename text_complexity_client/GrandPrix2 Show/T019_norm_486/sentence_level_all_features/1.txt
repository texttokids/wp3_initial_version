
********** Entity Names Features **********
*******************************************

1. Sentence: Pierre Veltz  : Les CDT , innovation de la loi du Grand Paris de 2010 ont été très utiles .
Entity Name (Type) :: Grand Paris de 2010 (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Pierre Veltz  : Les CDT , innovation de la loi du Grand Paris de 2010 ont été très utiles .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Pierre Veltz  : Les CDT , innovation de la loi du Grand Paris de 2010 ont été très utiles .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ loi, grand, utile ]
marqueurs_niveau3 :: 2 :: [ grand, utile ]
appreciatif :: 1 :: [ utile ]
deontique :: 1 :: [ loi ]
epistemique :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ loi ]
appreciatif_niveau3 :: 1 :: [ utile ]
deontique_niveau2 :: 1 :: [ loi ]
epistemique_niveau3 :: 1 :: [ grand ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Pierre Veltz  : Les CDT , innovation de la loi du Grand Paris de 2010 ont été très utiles .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['de 2010']
adverbiaux_localisation_temporelle::1::['de 2010']
adverbiaux_pointage_absolu::1::['de 2010']
adverbiaux_loc_temp_focalisation_id::1::['de 2010']
adverbiaux_loc_temp_regionalisation_id::1::['de 2010']
adverbiaux_loc_temp_pointage_absolu::1::['de 2010']

