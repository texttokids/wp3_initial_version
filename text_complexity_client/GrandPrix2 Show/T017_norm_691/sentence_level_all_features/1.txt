
********** Entity Names Features **********
*******************************************

1. Sentence: La délégation concerne presque uniquement des opérateurs privés « massifs » qui s ' enrichissent via des délégations de service public , partenariats public-privé ou encore Services d ' intérêt général (économiques ou non) .
Entity Name (Type) :: Services d ' intérêt général (ORG)


********** Emotions Features **********
***************************************

1. Sentence: La délégation concerne presque uniquement des opérateurs privés « massifs » qui s ' enrichissent via des délégations de service public , partenariats public-privé ou encore Services d ' intérêt général (économiques ou non) .
Tokens having emotion: [ intérêt ]
Lemmas having emotion: [ intérêt ]
Categories of the emotion lemmas: [ desir ]


********** Modality Features **********
***************************************

1. Sentence: La délégation concerne presque uniquement des opérateurs privés « massifs » qui s ' enrichissent via des délégations de service public , partenariats public-privé ou encore Services d ' intérêt général (économiques ou non) .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ enrichir, intérêt ]
marqueurs_niveau2 :: 2 :: [ enrichir, intérêt ]
appreciatif :: 1 :: [ enrichir ]
boulique :: 1 :: [ intérêt ]
appreciatif_niveau2 :: 1 :: [ enrichir ]
boulique_niveau2 :: 1 :: [ intérêt ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: La délégation concerne presque uniquement des opérateurs privés « massifs » qui s ' enrichissent via des délégations de service public , partenariats public-privé ou encore Services d ' intérêt général (économiques ou non) .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

