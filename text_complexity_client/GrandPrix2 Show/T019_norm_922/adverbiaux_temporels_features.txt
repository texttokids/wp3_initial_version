====== Sentence Level Features ======

1. Sentence: Que propose-t-on pour demain ?
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['pour demain', 'demain']
adverbiaux_localisation_temporelle::2::['pour demain', 'demain']
adverbiaux_pointage_non_absolu::3::['pour demain', 'demain', 'demain']
adverbiaux_loc_temp_focalisation_id::1::['pour demain']
adverbiaux_loc_temp_regionalisation_id::1::['pour demain']
adverbiaux_loc_temp_pointage_non_absolu::2::['pour demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::2::['pour demain', 'demain']
adverbiaux_localisation_temporelle::2::['pour demain', 'demain']
adverbiaux_pointage_non_absolu::3::['pour demain', 'demain', 'demain']
adverbiaux_loc_temp_focalisation_id::1::['pour demain']
adverbiaux_loc_temp_regionalisation_id::1::['pour demain']
adverbiaux_loc_temp_pointage_non_absolu::2::['pour demain', 'demain']
adverbiaux_dur_iter_deictique::1::['demain']
Total feature counts: 12

