
********** Entity Names Features **********
*******************************************

1. Sentence: Et , dans le système des États , chacun est concerné non seule‑ ment par la « politique étrangère » des autres , mais par une grande partie de leurs régulations internes .
Entity Name (Type) :: États (LOC)


********** Emotions Features **********
***************************************

1. Sentence: Et , dans le système des États , chacun est concerné non seule‑ ment par la « politique étrangère » des autres , mais par une grande partie de leurs régulations internes .
Tokens having emotion: [ États ]
Lemmas having emotion: [ état ]
Categories of the emotion lemmas: [ non_specifiee ]


********** Modality Features **********
***************************************

1. Sentence: Et , dans le système des États , chacun est concerné non seule‑ ment par la « politique étrangère » des autres , mais par une grande partie de leurs régulations internes .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 2 :: [ mentir, grand ]
epistemique :: 2 :: [ mentir, grand ]
marqueurs_niveau3 :: 1 :: [ grand ]
marqueurs_niveau2 :: 1 :: [ mentir ]
epistemique_niveau3 :: 1 :: [ grand ]
epistemique_niveau2 :: 1 :: [ mentir ]
Total feature counts: 8


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Et , dans le système des États , chacun est concerné non seule‑ ment par la « politique étrangère » des autres , mais par une grande partie de leurs régulations internes .
No features found.

