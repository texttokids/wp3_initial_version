
********** Entity Names Features **********
*******************************************

1. Sentence: Delors considérait aussi , à juste titre , les villes européennes comme le cœur battant du projet européen .
Entity Name (Type) :: Delors (PER)


********** Emotions Features **********
***************************************

1. Sentence: Delors considérait aussi , à juste titre , les villes européennes comme le cœur battant du projet européen .
Tokens having emotion: [ considérait ]
Lemmas having emotion: [ considérer ]
Categories of the emotion lemmas: [ admiration ]


********** Modality Features **********
***************************************

1. Sentence: Delors considérait aussi , à juste titre , les villes européennes comme le cœur battant du projet européen .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ considérer, juste, battre ]
marqueurs_niveau3 :: 2 :: [ considérer, juste ]
axiologique :: 1 :: [ juste ]
epistemique :: 1 :: [ considérer ]
pas_d_indication :: 1 :: [ battre ]
marqueurs_niveau0 :: 1 :: [ battre ]
axiologique_niveau3 :: 1 :: [ juste ]
epistemique_niveau3 :: 1 :: [ considérer ]
pas_d_indication_niveau0 :: 1 :: [ battre ]
Total feature counts: 12


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Delors considérait aussi , à juste titre , les villes européennes comme le cœur battant du projet européen .
No features found.

