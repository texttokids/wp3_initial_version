====== Sentence Level Features ======

1. Sentence: Dessiner pour les non-sachants sert ensuite à tous , élus , aménageurs , habitants… cela sans certitude que ce soit le bon outil pour gagner une consultation urbaine , car moins séducteur que de belles perspectives qui montreraient un futur pourtant bien incertain .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ élu, certitude, bon, gagner, beau, bien ]
appreciatif :: 4 :: [ bon, gagner, beau, bien ]
marqueurs_niveau3 :: 4 :: [ certitude, bon, beau, bien ]
appreciatif_niveau3 :: 3 :: [ bon, beau, bien ]
axiologique :: 2 :: [ bon, bien ]
axiologique_niveau3 :: 2 :: [ bon, bien ]
epistemique :: 1 :: [ certitude ]
pas_d_indication :: 1 :: [ élu ]
marqueurs_niveau2 :: 1 :: [ gagner ]
marqueurs_niveau0 :: 1 :: [ élu ]
appreciatif_niveau2 :: 1 :: [ gagner ]
epistemique_niveau3 :: 1 :: [ certitude ]
pas_d_indication_niveau0 :: 1 :: [ élu ]
Total feature counts: 28


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 6 :: [ élu, certitude, bon, gagner, beau, bien ]
appreciatif :: 4 :: [ bon, gagner, beau, bien ]
marqueurs_niveau3 :: 4 :: [ certitude, bon, beau, bien ]
appreciatif_niveau3 :: 3 :: [ bon, beau, bien ]
axiologique :: 2 :: [ bon, bien ]
axiologique_niveau3 :: 2 :: [ bon, bien ]
epistemique :: 1 :: [ certitude ]
pas_d_indication :: 1 :: [ élu ]
marqueurs_niveau2 :: 1 :: [ gagner ]
marqueurs_niveau0 :: 1 :: [ élu ]
appreciatif_niveau2 :: 1 :: [ gagner ]
epistemique_niveau3 :: 1 :: [ certitude ]
pas_d_indication_niveau0 :: 1 :: [ élu ]
Total feature counts: 28

