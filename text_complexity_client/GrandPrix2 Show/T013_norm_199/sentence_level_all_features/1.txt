
********** Entity Names Features **********
*******************************************

1. Sentence: Mais surtout parce qu ' il se traduisait immédiatement par une série de recettes et d ' étiquettes dont on pouvait craindre , au-delà des buts affichés , qu ' elles ne servent d ' abord une nouvelle politique de marketing territorial , surfant sur des inquiétudes sociales sans réellement se saisir de la complexité de la matière urbaine .
No features found.


********** Emotions Features **********
***************************************

1. Sentence: Mais surtout parce qu ' il se traduisait immédiatement par une série de recettes et d ' étiquettes dont on pouvait craindre , au-delà des buts affichés , qu ' elles ne servent d ' abord une nouvelle politique de marketing territorial , surfant sur des inquiétudes sociales sans réellement se saisir de la complexité de la matière urbaine .
Tokens having emotion: [ craindre, inquiétudes ]
Lemmas having emotion: [ craindre, inquiétude ]
Categories of the emotion lemmas: [ peur, peur ]


********** Modality Features **********
***************************************

1. Sentence: Mais surtout parce qu ' il se traduisait immédiatement par une série de recettes et d ' étiquettes dont on pouvait craindre , au-delà des buts affichés , qu ' elles ne servent d ' abord une nouvelle politique de marketing territorial , surfant sur des inquiétudes sociales sans réellement se saisir de la complexité de la matière urbaine .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 3 :: [ pouvoir, craindre, inquiétude ]
epistemique :: 3 :: [ pouvoir, craindre, inquiétude ]
appreciatif :: 2 :: [ craindre, inquiétude ]
marqueurs_niveau3 :: 2 :: [ pouvoir, craindre ]
epistemique_niveau3 :: 2 :: [ pouvoir, craindre ]
alethique :: 1 :: [ pouvoir ]
deontique :: 1 :: [ pouvoir ]
marqueurs_niveau2 :: 1 :: [ inquiétude ]
alethique_niveau3 :: 1 :: [ pouvoir ]
appreciatif_niveau3 :: 1 :: [ craindre ]
appreciatif_niveau2 :: 1 :: [ inquiétude ]
deontique_niveau3 :: 1 :: [ pouvoir ]
epistemique_niveau2 :: 1 :: [ inquiétude ]
Total feature counts: 20


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Mais surtout parce qu ' il se traduisait immédiatement par une série de recettes et d ' étiquettes dont on pouvait craindre , au-delà des buts affichés , qu ' elles ne servent d ' abord une nouvelle politique de marketing territorial , surfant sur des inquiétudes sociales sans réellement se saisir de la complexité de la matière urbaine .
No features found.

