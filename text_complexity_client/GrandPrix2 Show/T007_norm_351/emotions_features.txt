====== Sentence Level Features ======

1. Sentence: J ' ai aimé l ' univers de l ' atelier , son odeur , le geste juste , ressenti , de celui qui sait souder , à l ' image du geste du pizzaiolo qui balance sa pâte à pizza d ' une main à l ' autre en chaloupant les épaules .
Tokens having emotion: [ aimé, ressenti ]
Lemmas having emotion: [ aimer, ressentir ]
Categories of the emotion lemmas: [ amour, non_specifiee ]


====== Text Level Features ======

Tokens having emotion: [ aimé, ressenti ]
Lemmas having emotion: [ aimer, ressentir ]
Category of the emotion lemmas: [ amour, non_specifiee ]
Total emotion lemma counts: 2

