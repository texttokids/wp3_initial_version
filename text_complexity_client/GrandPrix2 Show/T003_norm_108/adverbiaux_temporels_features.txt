====== Sentence Level Features ======

1. Sentence: Ces connexions offraient déjà la chance d ' une singularité appuyée sur celle d ' un contexte , mais c ' est surtout l ' obligation d ' une gestion la plus écologique possible qui m ' a permis de rassembler un vocabulaire formel au service de cette gestion et de sa mise en scène à la fois  : noues , éoliennes , plantes qui nettoient , eaux purifiées , bassins biotopes… des choses connues dans le dictionnaire des techniques jardinières , mais dont la réunion produit une phrase paysagère inédite .
No features found.


====== Text Level Features ======

No features found.

