
********** Entity Names Features **********
*******************************************

1. Sentence: Nous l ' avons créé au milieu des années 1980 , avec Gabriel Dupuy et Henri Coing , autour d ' une sorte d ' utopie  : comment créer un laboratoire de sciences sociales vraiment pluridisciplinaire , qui prenne au sérieux les mondes professionnels et en particulier ceux des ingénieurs , des urbanistes , et qui ouvre , comme nous disions , les « boîtes noires » de la technique .
Entity Name (Type) :: Henri Coing (PER)


********** Emotions Features **********
***************************************

1. Sentence: Nous l ' avons créé au milieu des années 1980 , avec Gabriel Dupuy et Henri Coing , autour d ' une sorte d ' utopie  : comment créer un laboratoire de sciences sociales vraiment pluridisciplinaire , qui prenne au sérieux les mondes professionnels et en particulier ceux des ingénieurs , des urbanistes , et qui ouvre , comme nous disions , les « boîtes noires » de la technique .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Nous l ' avons créé au milieu des années 1980 , avec Gabriel Dupuy et Henri Coing , autour d ' une sorte d ' utopie  : comment créer un laboratoire de sciences sociales vraiment pluridisciplinaire , qui prenne au sérieux les mondes professionnels et en particulier ceux des ingénieurs , des urbanistes , et qui ouvre , comme nous disions , les « boîtes noires » de la technique .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 4 :: [ vraiment, sérieux, ouvrir, noir ]
pas_d_indication :: 2 :: [ sérieux, noir ]
marqueurs_niveau0 :: 2 :: [ sérieux, noir ]
pas_d_indication_niveau0 :: 2 :: [ sérieux, noir ]
alethique :: 1 :: [ vraiment ]
boulique :: 1 :: [ ouvrir ]
marqueurs_niveau3 :: 1 :: [ ouvrir ]
marqueurs_niveau1 :: 1 :: [ vraiment ]
alethique_niveau1 :: 1 :: [ vraiment ]
boulique_niveau3 :: 1 :: [ ouvrir ]
Total feature counts: 16


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Nous l ' avons créé au milieu des années 1980 , avec Gabriel Dupuy et Henri Coing , autour d ' une sorte d ' utopie  : comment créer un laboratoire de sciences sociales vraiment pluridisciplinaire , qui prenne au sérieux les mondes professionnels et en particulier ceux des ingénieurs , des urbanistes , et qui ouvre , comme nous disions , les « boîtes noires » de la technique .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['au milieu des années 1980']
adverbiaux_localisation_temporelle::1::['au milieu des années 1980']
adverbiaux_pointage_absolu::1::['au milieu des années 1980']
adverbiaux_pointage_non_absolu::1::['au milieu des années 1980']
adverbiaux_loc_temp_focalisation_non_id::1::['au milieu des années 1980']
adverbiaux_loc_temp_regionalisation_id::1::['au milieu des années 1980']
adverbiaux_loc_temp_pointage_non_absolu::1::['au milieu des années 1980']
adverbiaux_dur_iter_absolu::1::['au milieu des années 1980']

