====== Sentence Level Features ======

1. Sentence: Cette expérience a été une édition originale et personnelle du «Voyage en Italie 1» : à travers ce pays riche et surprenant des années 1990 , durant lesquelles les petites entreprises installées dans la ville diffuse avaient été capables de renverser la crise de la grande industrie des précédentes décennies en un nouvel espace social , économique et territorial ;
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ riche, surprenant, petit, crise, grand ]
epistemique :: 4 :: [ surprenant, petit, crise, grand ]
appreciatif :: 2 :: [ riche, crise ]
marqueurs_niveau3 :: 2 :: [ petit, grand ]
marqueurs_niveau2 :: 2 :: [ riche, crise ]
appreciatif_niveau2 :: 2 :: [ riche, crise ]
epistemique_niveau3 :: 2 :: [ petit, grand ]
marqueurs_niveau1 :: 1 :: [ surprenant ]
epistemique_niveau2 :: 1 :: [ crise ]
epistemique_niveau1 :: 1 :: [ surprenant ]
Total feature counts: 22


====== Text Level Features ======

Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 5 :: [ riche, surprenant, petit, crise, grand ]
epistemique :: 4 :: [ surprenant, petit, crise, grand ]
appreciatif :: 2 :: [ riche, crise ]
marqueurs_niveau3 :: 2 :: [ petit, grand ]
marqueurs_niveau2 :: 2 :: [ riche, crise ]
appreciatif_niveau2 :: 2 :: [ riche, crise ]
epistemique_niveau3 :: 2 :: [ petit, grand ]
marqueurs_niveau1 :: 1 :: [ surprenant ]
epistemique_niveau2 :: 1 :: [ crise ]
epistemique_niveau1 :: 1 :: [ surprenant ]
Total feature counts: 22

