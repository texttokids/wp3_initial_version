
********** Entity Names Features **********
*******************************************

1. Sentence: Nous avons surtout voulu éviter le modèle « zone d ' activité » ou « grand ensemble » , avec de vastes emprises et des bâtiments plantés au milieu , sur le modèle Danone , ou encore EDF , qui a été décidé au tout début du projet , un bel ensemble de bâtiments , mais pas du tout représentatif de « Il faut bien reconnaître que la réflexion globale sur le territoire français , incluant bien sûr ses adhérences européennes , a beaucoup décliné , en même temps que l ' influence de l ' ex Datar .
Entity Name (Type) :: Datar (MISC)


********** Emotions Features **********
***************************************

1. Sentence: Nous avons surtout voulu éviter le modèle « zone d ' activité » ou « grand ensemble » , avec de vastes emprises et des bâtiments plantés au milieu , sur le modèle Danone , ou encore EDF , qui a été décidé au tout début du projet , un bel ensemble de bâtiments , mais pas du tout représentatif de « Il faut bien reconnaître que la réflexion globale sur le territoire français , incluant bien sûr ses adhérences européennes , a beaucoup décliné , en même temps que l ' influence de l ' ex Datar .
No features found.


********** Modality Features **********
***************************************

1. Sentence: Nous avons surtout voulu éviter le modèle « zone d ' activité » ou « grand ensemble » , avec de vastes emprises et des bâtiments plantés au milieu , sur le modèle Danone , ou encore EDF , qui a été décidé au tout début du projet , un bel ensemble de bâtiments , mais pas du tout représentatif de « Il faut bien reconnaître que la réflexion globale sur le territoire français , incluant bien sûr ses adhérences européennes , a beaucoup décliné , en même temps que l ' influence de l ' ex Datar .
Features :: Feature-Counts :: Feature-Lemmas
--------------------------------------------
marqueurs :: 9 :: [ vouloir, grand, décider, beau, falloir, bien, bien, sûr, beaucoup ]
marqueurs_niveau3 :: 7 :: [ vouloir, grand, décider, beau, falloir, bien, bien ]
epistemique :: 4 :: [ grand, décider, sûr, beaucoup ]
appreciatif :: 3 :: [ beau, bien, bien ]
appreciatif_niveau3 :: 3 :: [ beau, bien, bien ]
axiologique :: 2 :: [ bien, bien ]
boulique :: 2 :: [ vouloir, décider ]
axiologique_niveau3 :: 2 :: [ bien, bien ]
boulique_niveau3 :: 2 :: [ vouloir, décider ]
epistemique_niveau3 :: 2 :: [ grand, décider ]
deontique :: 1 :: [ falloir ]
marqueurs_niveau2 :: 1 :: [ sûr ]
marqueurs_niveau1 :: 1 :: [ beaucoup ]
deontique_niveau3 :: 1 :: [ falloir ]
epistemique_niveau2 :: 1 :: [ sûr ]
epistemique_niveau1 :: 1 :: [ beaucoup ]
Total feature counts: 42


********** Adverbiaux Temporels Features **********
********************************************************

1. Sentence: Nous avons surtout voulu éviter le modèle « zone d ' activité » ou « grand ensemble » , avec de vastes emprises et des bâtiments plantés au milieu , sur le modèle Danone , ou encore EDF , qui a été décidé au tout début du projet , un bel ensemble de bâtiments , mais pas du tout représentatif de « Il faut bien reconnaître que la réflexion globale sur le territoire français , incluant bien sûr ses adhérences européennes , a beaucoup décliné , en même temps que l ' influence de l ' ex Datar .
Features :: Feature-Counts :: Feature-Texts
--------------------------------------------
adverbiaux_temporels::1::['encore']
adverbiaux_duratifs_iteratifs::1::['encore']
adverbiaux_pointage_non_absolu::1::['encore']
adverbiaux_dur_iter_relatif::1::['encore']

