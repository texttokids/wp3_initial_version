Cet été, Pascal part en colonie de vacances. Il a du mal à quitter ses parents mais ils lui ont promis qu’il s’amuserait.
La journée, Pascal découvre plein d’activités créatives comme fabriquer un arc ou encore créer sa propre cabane dans les bois.
Les animateurs sont tous très gentils et drôles, ils organisent de nombreux jeux en extérieur: le chat perché, la poule, le renard et la vipère ou encore cache-cache.
Le soir, Pascal dort dans un dortoir avec ses nouveaux copains. Lorsqu’ils ont du mal à se coucher, un animateur vient leur raconter une histoire.
Pascal a essayé de nombreuses nouvelles activités: pour la première fois il a fait de l’équitation. Son poney préféré s’appelle Éclair et Pascal lui donne secrètement des carottes.
Pascal a aussi fait du kayak. Accompagné d’un animateur, il a dirigé un kayak sur un magnifique lac. Il a observé des poissons de toutes les couleurs dans l’eau !
Pascal et ses copains sont montés sur un bateau pour aller visiter une île. Le capitaine du bateau a même autorisé à Pascal à prendre le gouvernail ! Il était très fier.
Pour fêter la fin de la colonie, tous les enfants ont choisi d’organiser un spectacle. Pascal a décidé de réaliser une pièce de théâtre avec ses copains et copines. À la fin de leur représentation, tous les enfants ont applaudi et les animateurs les ont félicités !
Pascal est triste de quitter la colonie et ses amis mais il est très heureux de retrouver ses parents !
