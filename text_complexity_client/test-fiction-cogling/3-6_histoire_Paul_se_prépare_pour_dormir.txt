Il est 20h30, Paul se prépare pour aller dormir. Il va dans sa chambre et enfile son pyjama.
Paul se dirige vers la salle de bain et se brosse les dents... Celles d'en haut et celles d'en bas. Il sourit devant le miroir, pour voir si elles sont bien propres.
Paul attrape un gant de toilette dans l'armoire, il met un peu de savon dessus et se frotte le visage, la nuque et derrière les oreilles.
Il fait couler un peu d'eau dans le lavabo, et rince son gant. Il refait le même chemin que la première fois, visage, nuque et oreilles.
Sa maman arrive et inspecte le travail de Paul, elle lui sourit et le félicite de s'être débrouillé seul.
Paul va ensuite choisir un livre dans sa bibliothèque, sa maman ou son papa lui racontent une histoire tous les soirs avant de dormir.
Il se glisse dans son lit et sa mère s'installe à ses côtés pour commencer la lecture.
Une fois celle-ci terminée, Paul embrasse ses parents, s'allonge confortablement dans son lit et s'endort doucement.
