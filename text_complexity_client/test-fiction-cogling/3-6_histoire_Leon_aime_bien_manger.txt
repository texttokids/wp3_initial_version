Est-ce que je peux manger les croquettes de Minou le chat ?
Oh non, Léon, ce n’est pas pour les humains.
Pourquoi ? Parce que ça donne des boutons ?
Parce que c’est de la terre ?
Parce qu’il ne voudra pas m’en donner ?
Parce que ça sent mauvais ?
Le chat n’a pas les mêmes besoins que toi. Il mange beaucoup plus de viande, et sa nourriture est très grasse.
Et si j’en mange ça va faire quoi ?
Si tu en manges, tu vas avoir mal au ventre et tu seras malade.
AAAH, comme quand je mange des choux de Bruxelles !
