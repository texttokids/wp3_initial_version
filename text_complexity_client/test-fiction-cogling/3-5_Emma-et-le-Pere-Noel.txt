Emma et le Père Noël


La grand-mère d’Emma lui montre les belles maisons le long de la rue, les jardins, les balcons, les frises.
- C’est beau, n’est-ce pas ? dit grand-mère.
- Oui, oui, dit Emma, énervée et fatiguée de lever la tête chaque fois que Grand-mère lui montre encore une porte et un portail. Elle aimerait arriver et cesser ce défilé de murs. Elle n’en peut plus.
- Et regarde cette belle maison là-haut !
- MAIS ! Ça ne m’intéresse pas les maisons !
- Qu’est-ce qui t’intéresse alors, Emma ?
- Les jouets, les cadeaux et les surprises !
- Et quoi encore ?
- Le Père Noël.

Dans sa chambre, Emma prend un des catalogues de jouets. C’est son livre préféré ! Avec un feutre, elle entoure chaque image de jeux et de jouets qu’elle veut. Après elle enverra toutes ces pages au Père Noël dans une grande enveloppe. Elle l’a déjà préparée en dessinant des cœurs et des fleurs dessus.

Elle met en premier l’image la plus importante, celle de la balançoire. C’est ce qu’Emma veut le plus au monde. Se balancer, décoller, voler, elle adore !

-Maman, tu crois que le père Noël va m’apporter la balançoire ?
- Je pense que c’est trop gros pour mettre dans sa hotte.
- Mais il l’a quand même apporté à Anna Noël dernier. Ça vient dans une boîte et puis son papa l’a monté.
- Oui mais Emma, il n’y a pas assez de place dans notre tout petit jardin.
- On la met dans le salon sinon ….
- Et où va-t-on s’asseoir ?
- Alors dans ma chambre !
- Et où vas-tu te coucher ?

Emma fait le tour de la maison.
- J’ai une idée maman : on met la balançoire dans ta chambre et toi et papa vous pouvez dormir sur le canapé lit au salon.
- Je ne pense pas que le père Noël va être d’accord.
- MAIS, il est gentil le père Noël !
- C’est qui le père Noël d’après toi ?
- C’est le gros bonhomme en rouge, toujours souriant … comme moi puisque tu m’as dit qu’il faut être gracieuse.
- Et s’il ne souriait pas, tu l’aimerais quand même ?
- Oui, il est quand même gentil.
- Et s’il était un voyou, tu l’aimerais quand même ?
- Oui, il y a des voyous gentils aussi. Tu n’as qu’à voir Gaëtan en classe !
- Et s’il était un voleur, tu l’aimerais quand même ?
- MAIS ! Ce n’est pas un voleur le père Noël, qu’est-ce que tu racontes ?
- Tu as raison Emma. Mais je pense que le père Noël est gentil aussi pour les parents et il ne voudrait pas nous mettre dehors de notre chambre.

Emma est troublée. Elle regarde le grand cerisier qui prend toute la place dans le jardin. Est-ce qu’on a vraiment besoin d’un cerisier ? se demande-t-elle. Une balançoire est beaucoup plus importante !

Emma regarde le catalogue encore et encore, mais elle n’arrive pas trouver une seule chose qu’elle voudrait autant qu’une balançoire.

La nuit au lit elle compose une lettre dans sa tête au père Noël.
« Cher père Noël,
Comme tu sais puisque tu sais tout, je suis très sage (et je fais bien mes pages dans le cahier des moyens à l’école). Je sais que le cadeau que je veux est très difficile et lourd. C’est un problème. Mon papa dit que quand on a un problème, on réfléchit. Et comme tu es très, très, très intelligent, je sais que tu vas trouver un moyen de me la donner. Merci, à bientôt, je t’attends. Et je sais aussi que je ne peux pas encore écrire et envoyer cette lettre, mais tu es tellement fort que tu lis mes pensées. »

Contente et en paix, Emma s’endort.
Elle est encore soucieuse le matin.
- Qu’est-ce qu’il y a Emma ? demande son père.
- Je m’inquiète pour le père Noël. Il a la barbe blanche, il est vieux, est-ce qu’il va pouvoir m’apporter ma balançoire ?
Son papa est tout aussi troublé.
- Peut-être pas cette année Emma, mais un jour !
- Quand alors ?
- On verra ….
Emma déteste quand ses parents disent « on verra ». Ça ne veut rien dire, ou ça veut dire « va te faire cuire un œuf ! »
Emma s’occupe à faire des cadeaux pour ses grand-mères et ses parents. Elle dessine des balançoires sur chaque feuille et les signe « Emma ».
 La veille de Noël, il n’y a rien d’autre dans sa tête qu’une balançoire qui lui donne un peu le mal de mer dans son lit. 
Le matin de Noël, il n’y a que de paquets petits sous l’arbre. Emma regard par la fenêtre. Le vieux cerisier est toujours là. Elle jette un coup d’œil dans la chambre des parents. Leur lit est toujours là.

Elle n’a pas d’appétit, même pour les gâteaux de Noël.
- Viens vite Emma. Habille-toi. On va chez grand-mère.
Emma n’a pas envie d’être sage. Elle est trop déçue. Mais elle suit ses parents.

Et là, arrivée chez grand-mère, elle la voit : la balançoire est installée dans le grand jardin de grand-mère.
- Comme il est intelligent le père Noël. Il a trouvé comment faire !

Emma pense : Ce n’est pas la peine de vouloir ce qu’on ne peut pas avoir. Mais dès fois ça marche !
