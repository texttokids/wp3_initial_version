Comme tous les matins, le roi Arthur fait le tour de la savane pour voir si les animaux ont passé une bonne nuit.
Chez les girafes, tout va bien. Mais les singes, eux, ont entendu du bruit : « Ca venait du coté de la rivière." déclare Tepee à Arthur.
Arthur se rend, à tout vitesse, à la rivière. Il est surpris de découvrir le crocodile, Tano, en train de pleurer.
"Que se passe-t-il Tano ?" demande Arthur "Je me suis blessé à la patte arrière cette nuit en revenant sur le rivage." dit Tano en pleurant à chaudes larmes.
"Laisse-moi regarder." demande Arthur. Tano sort de l'eau, pour permettre à Arthur d'accéder facilement à sa patte arrière. "Je comprends mieux pourquoi tu souffres autant. Tu as une grosse écharde plantée dans le pied, Tano. Ce n'est pas grave. Ne bouges pas, je vais aller chercher Lisa, l'amie de Tepee, elle va te soigner".
Arthur, accompagné de Lisa et de Tepee, revient bien vite auprès de Tano.
"Ne pleure plus Tano, je suis là ! " murmure gentiment Lisa. Elle se place à côté du pied du crocodile et, avec ses petits doigts agiles, retire délicatement l’écharde.
Puis elle attrape une grande feuille de bananier et enroule le pied de Tano. « Ca va te soulager!" lui dit-elle. "Merci beaucoup Lisa, je me sens beaucoup mieux !" s'exclame Tano, heureux de ne plus souffrir.
Lisa et Tepee retournent à leurs occupations. Tano va se reposer dans la rivière et le roi Arthur termine son tour du royaume, avant d'aller faire une bonne sieste bien méritée!
FIN
