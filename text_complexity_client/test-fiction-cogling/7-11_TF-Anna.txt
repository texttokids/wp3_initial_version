La famille trop de filles



1. Anna

Ils se sont rencontrés au lycée dans le cours d'histoire en seconde. Ainsi l'histoire commença, une histoire d'amour, de bébés, de travail, de vie de tous les jours et de nuits sans fin.  Quelle histoire !
Arthur se rappelle d'avoir vu Ariane avec ses longs cheveux noirs jusqu'à la taille. Ce qui a marqué Ariane c'était le sourire biscornu d'Arthur. Elle avait quinze ans et elle dit à sa mère :   "J'ai rencontré l'homme de ma vie !"
Malgré les réserves de sa mère, ils se marièrent et eurent des enfants, BEAUCOUP d'enfants !
Arthur adorait les bébés. Ariane aimait être enceinte. Elle aimait voir son ventre gonfler comme un ballon. Le premier à sortir de ce ballon était une fille, Anna. Ils étaient abonnés à la lettre A.
Pour leur deuxième fille, ils décidèrent d'avancer vers le B. Elle s'appelle Bella.
Et l'alphabet les a guidés pour leurs six filles : Anna, Bella, Cara, Dana, Elisa et Flavia. S'ils ne commencent par un A, au moins ils finissent par le A qu'ils affectionnent tant.
Ils ont décidé de fabriquer des bébés jusqu'à enfin donner naissance à un garçon, ce qui arriva avec leur septième bébé, Gabriel. Ouf !
Et puis STOP.
Car d'une façon ou d'une autre, il faut élever les enfants et gagner la vie, deux choses difficiles à concilier pour Arthur et Ariane qui ont des métiers qui les amènent loin … loin de leur famille.
 
Bien sûr, ils ne se rendaient pas compte que non seulement il faut les faire naître mais les enfants ont besoin de soins quotidiens et minute par minute. Arthur, ingénieux ingénieur, inventait des dispositifs pour tenir les biberons sans l'aide d'un parent affectueux. Il fabriquait des berceaux électriques, mais n'arrivait jamais à concocter un système pour changer les couches sans les mains humaines. Pas de problème : Anna changeait les couches de Bella qui s'occupait de Cara, et ainsi de suite. À vrai dire, c'est Anna, l'aînée qui s'occupe de tout le monde, y compris ses parents !
Pendant que son père parcourt le monde dans une direction pour l'entreprise qui a commercialisé son logiciel et ses inventions et que sa mère, grand reporter pour une chaîne de télévision, parcourt le monde dans une autre direction, Anna se sent souvent seule avec ses cinq sœurs et son frère. Malgré la présence du jeune homme irlandais, Sean, qu'ils emploient pour assurer la survie de leurs enfants, Anna prend beaucoup sur elle, sans qu'on lui demande quoi que ce soit. Elle est pleinement marquée par son rôle d'aînée.
 Si Anna a l'impression d'être Cendrillon, c'est surtout parce que cette année elle est entrée en sixième et elle aimerait avoir plus de temps pour elle-même, même si c'est seulement pour se faire belle !
 Par exemple, Anna est toujours la dernière des enfants à partir le matin alors que c'est elle qui a le plus long chemin. Sean qui n'aime le matin et a du mal à se réveiller, donne volontiers ce boulot aux grandes qui doivent habiller, nourrir toute la fratrie. Cette première année de collège l'inquiète car en plus de ses propres devoirs, elle surveille ceux des autres enfants. Sauf Gabriel qui est encore à la maternelle.   Gabriel est allergique aux acariens. Il est toujours en train de s'éternuer, renifler, avec des yeux et le nez qui coulent. Il a du mal à respirer. Et Anna est toujours inquiète.
Leur maison n'a donc pas de tapis, pas de bibliothèques, pas de tapisseries.  A la naissance de Gabriel, quand ils ont su pourquoi il toussait tout le temps, ils se sont débarrassés de tout ce qui n'est pas essentiel. Gabriel dort dans une petite chambre, presque stérile, tout seul. Les six filles dorment dans une autre chambre "dortoir" les unes sur les autres. Il n'y a pas de place pour des bureaux. Ils font leurs devoirs sur la table de la salle à manger. Les parents ont leur propre "nid d'amour", comme ils l'appellent. Et Sean a une petite chambre à lui.
Anna pense que c'est quand même plus facile maintenant qu'ils sont tous à l'école : Gabriel donc en grande section., Flavia en C.P., Elisa en C.E.1, Dana, en C.E.2, Cara en C.M.1, Bella en C.M.2 et Anna en sixième. A leur famille seule, ils recouvrent presque toute l'éducation nationale.
La vie d'Anna est donc difficile. Personne ne lui a demandé, mais Anna est préoccupée par les soucis pour sa famille : est-ce que ses parents vont bien ? Est-ce qu'ils ont eu leurs avions respectifs ? Est-ce que Gabriel ne va pas trop tousser à l'école ? Et Flavia, réussira-t-elle à maîtriser les 26 lettres pour apprendre à lire ? Et Bella, silencieuse, enfermée dans un monde à elle, elle ne parle pas beaucoup, mais elle écrit des poèmes dans le carnet qu'elle a acheté avec ses sous ? Cara est la main droite d'Anna, qui comprend sans qu'on lui demande qu'il faut mettre la table et faire les lits pour ne pas revenir à une porcherie à la fin de la journée. Il faut actionner l'aspirateur pour assassiner les acariens qui poursuivent leur frère, enlever toute trace de poussière et surtout ne pas accumuler des objets nids de poussière. Mais Cara qui se rend si utile à la maison, n'est pas bon élève. Elle s'ennuie à l'école et il faut pratiquement la fouetter pour qu'elle fasse ses devoirs. Dana est triste en ce moment et Anna ne sait pas pourquoi. Heureusement qu'Elisa est drôle et pleine de joie. Mais Anna a de quoi se soucier !
Elle est tellement anxieuse qu'elle ne se lie pas facilement avec les autres, à part Emma qui est sa copine depuis la maternelle.  De toute façon, il n'y a pas de place dans sa vie pour des amis. Elle n'a pas le temps et surtout pas l'énergie. Cela ne l'empêche pas de penser à un garçon de sa classe qui semble la suivre quand elle passe chercher son frère le soir. Il s'avère que lui aussi a un petit frère à la maternelle. Ils prennent leurs frères et se font des sourires. Anna ne sourit pas beaucoup, sauf quand Sean fait une de ses millions de fautes de français.
Quand tout le monde est bien rentré à la maison, Anna est soulagée, mais elle lance tout de suite les ordres. Les devoirs d'abord. Elle fait la maîtresse, passe de l'une à l'autre, résout les problèmes de math, fait des dictées, encourage et gronde. Elle fait couler le bain pour Gabriel, le lave, lui met son pyjama. Et puis tous en pyjama.
Pendant ce temps, Sean fait les devoirs d'anglais d'Anna et prépare le dîner.
Cara dresse la table. Bella fait chauffer le repas préparé par Sean qui fait très bien la cuisine : des lasagnes aux épinards. Dana touille la salade. Et puis, comme s'ils avaient le signal que tout est prêt, les parents téléphonent, d'abord l'un, puis l'autre. Ils se pressent de manger pour voir le reportage de leur mère à la télé.
Et c'est seulement après, quand tout le monde, dents brossées, est au lit, qu'Anna peut se donner à ses propres pensées : Martin, sa dernière conversation avec Emma sur le stage qu'elles aimeraient faire pendant les vacances de Toussaint, un stage de radio reportages. Anna aime savoir ce qui se passe dans le monde, elle est obsédée par les nouvelles et elle écoute la radio chaque fois qu'elle peut. C'est la radio qu'elle aime, beaucoup plus que la télé qu'elle regarde seulement pour voir sa mère. Mais elle est toutefois la digne fille de sa mère, passionnée par les pays, les gouvernements et la politique. 
Une fois qu'elle s’endort, c’est comme un vieil arbre mort d'un coup. Elle se réveille, surprise que la nuit est finie, consciente de tout ce qu'il faut faire avant d'aller au collège. L'école pour Anna, c'est des vacances !
Avant même de passer aux toilettes, elle fait son lit. La vielle, elle avait sélectionné les vêtements pour les plus petits, dressé la table pour le petit-déjeuner, organisé les cartables pour tout le monde, mais sûrement, elle a oublié quelque chose …
Ce n'est pas étonnant, qu'avec son air sérieux et serviable, qu'elle est élue déléguée de la classe. Malheureusement, elle déteste l'autre délégué qui était dans sa classe de CM2, Jason Lopez, un macho man qui n'aime rien de mieux que de donner les ordres en mangeant les bananes dont il a toujours un stock dans son sac.
- Tu feras la liste de toute la classe avec leurs numéros de téléphone, lui ordonne-t-il.
Anna déteste encore plus les arguments que de faire ce qu'elle n'aime pas faire. Ce travail supplémentaire va lui pousser à échanger quelques mots avec chaque membre de la classe, une autre tache dont elle a horreur. Elle n'est pas vraiment timide, juste un peu réservée.
- Tu n'as qu'à faire circuler une feuille, lui suggère Emma.
La feuille est fatalement prise par la prof d'histoire-géo (le meilleur sujet d'Anna) qui ne veut pas entendre l'explication et donne à Anna la première punition de sa vie.
En plus elle est en retard pour chercher Gabriel et elle ne peut pas arrêter les larmes qui se versent toutes seules de ses yeux et elle a honte quand elle voit Martin. Elle prend la main de Gabriel en détournant la tête, mais Martin attrape son bras pour l'arrêter.
- C'est pas grave, lui dit-il.
Anna sait qu'elle ne veut plus jamais laver son bras avec les traces de la main de Martin.
La nuit, après avoir fini sa maudite punition qu'elle a bien aimée : copier les grandes dates de la Révolution française, était encore plus douce puisque Martin l'a touchée.
Le téléphone réveille tout le monde et c'est tant mieux. Anna répond en chuchotant.
- Désolée de t'appeler si tôt, mais je voulais savoir si tu peux venir à mon "Pyjama party" samedi soir, dit Emma.
Emma, sa meilleure amie, un enfant unique qui ne saisit pas l'étendu des corvées d'Anna, ne se rend pas compte de la vie d'Anna. Emma a deux parents pour elle toute seule et aucun frère ou sœur à surveiller.
- Qui vient ?
- Je n'ai invité personne encore. Toi d'abord.
- Je te dirai demain, je ne sais pas si je verrai mes parents cette semaine et je ne sais pas si Sean sera là samedi soir.
Si ses parents reviennent ce week-end, Anna aimerait bien passer la soirée avec eux, mais en même temps elle meurt d'envie d'aller à la fête. Elle pourrait remplir la feuille si Emma invite la classe.
Mais d'abord, il faut expédier les enfants à leur école et elle aussi par la même occasion. Gabriel est grognon, il a mal au ventre, il tousse, il n'a pas faim, il ne veut pas s'habiller. 
- Tu fais les filles, je m'occupe du garçon ? demande-t-elle à Cara, sa main droite. Cara sait qu'un seul garçon est plus difficile que les six filles ! 
Cara, adjudant énergique, donne des ordres, en imitant sa sœur. Elle met les chaussettes de Flavia qui a mis la culotte toute seule. Elle verse le jus d'orange dans les verres et les céréales dans les bols.  A vrai dire, il faudrait une journée entière rien qu'à faire sortir tous les sept le matin. C'est dur pour Sean le matin, il aime faire la grasse matinée. Il est tellement gentil que l'on veut s'arranger en lui laissant ses matinées. Il se rattrape !
- Merci Cara, lui dit Anna qui finit par maîtriser Gabriel, le faire ingérer une brioche et le mettre en route. C'est Bella aujourd'hui qui le dépose à la maternelle. Ils s'en vont main dans la main, en profonde conversation. Gabriel est le seul à pouvoir faire parler Bella.
Les parents n'ont pas téléphoné et Anna ne sait pas si elle peut aller chez Emma samedi soir.
Depuis sa crise de larmes, Martin a proposé à Anna de faire le chemin ensemble. Martin est certainement le garçon le plus craquant de sa classe. Le problème c'est qu'il s'appelle Martin Martin tout comme son père s'appelle Arthur Arthur. Est-ce que l'histoire se répète ? Est-ce possible ?
Anna, qui à part Emma, n'a pas beaucoup d'amis et ne se confie pas facilement, raconte pourtant son problème du pyjama party à Martin.
- Je pense qu'il faut laisser tes parents être parents de temps en temps. Tu mérites un congé !
- Je demanderai ce soir.
Les enfants dînent avant le coup de fil des parents. Mais ils racontent chacun à son tour des problèmes qu'ils ont eu, leur mère dans un pays en guerre et leur père dans des embouteillages à l'autre côté de la France et Anna n'ose pas parler de son invitation. 
 - Alors ? lui demande Emma le lendemain.
- Je ne leur ai pas encore posé la question. 
Emma, qui a eu un téléphone portable pour son entrée en sixième, le tend à Anna en disant "Vas-y, appelle-les."
- Bonjour Papa, ça va ?
- Oui, je m'excuse pour hier soir, j'étais vraiment stressé. D'où tu téléphones ? Qu'est-ce qui arrive ? demande-t-il, un peu paniqué.
- Emma m'a prêté son portable. Papa, est-ce que je peux dormir chez elle samedi soir ?
- Aie ! Ça tombe mal cette semaine ! On sera bien là ce week-end mais samedi soir maman et moi sommes invités à un mariage d'un collègue.
Anna fait non de la tête avertissant Emma qu'elle ne pourra pas venir. Elle dit vite au revoir à son père avant de pleurer.
- Une autre semaine peut-être ? dit-elle à Emma.
- Tu dis toujours ça.
- Ecoute alors, on fera un pyjama party chez nous cette semaine, qu'est-ce que tu en dis ?
Emma adore aller chez Anna. Elle rêve de dormir dans une chambre avec toutes ces sœurs. Elle est folle du petit Gabriel. Il y a du bruit et de l'action. "C'est super. Je viendrai dormir chez vous."
Martin est scandalisé. Anna aimerait l'inviter aussi, mais elle n'ose pas.
- Ne t'en fais pas, j'ai l'habitude. Mes parents seront perdus à s'occuper de tous ces enfants sans moi. On va bien s'amuser quand même.
Sean prend ses vacances quand les parents rentrent. Avant d'aller à leur mariage, les parents préparent un festin pour les enfants avant d'aller au leur. Ils font une soupe de potiron, des cuisses de canard à l'orange avec une purée de carottes et une tarte à la citrouille.
Ils se mettent tous en pyjama pour manger, puisque c'est un pyjama party.
- Nous on mange les pâtes tous les soirs, dit Emma en regardant la soupe de potiron qu'elle ne pourrait pas avaler même si on lui payait dix mille euros ! La famille Arthur est bizarre pour elle. Ils mangent n'importe quoi ! Elle a vu aussi la tarte à la citrouille. Beurk ! Les citrouilles, c’est pour Halloween.
- On mange les pâtes seulement quand les parents n'ont rien préparé mais il y a toujours une bonne sauce.
- Les pyjama party, on doit manger des sandwiches et des chips et des cacahuètes et du pop corn.
Anna est affligée par cette nouvelle. Elle aurait aimé bien faire les choses.
- On te fera du pop-corn après le dîner. On va faire un spectacle.
Emma aime aussi les spectacles chez les Arthur. Chaque fille a un talent spécial, et Gabriel est un parfait spectateur. Elle admire aussi la façon presque militaire que les enfants débarrassent la table et nettoie la cuisine. Chez elle, on ne lui demande rien comme si elle était trop débile pour lever le petit doigt.
La première à sauter sur le pop-corn est Emma qui n'a presque rien mangé. Les autres se sont régalés de ce repas orange. Anna chante comme un ange et toute la maison est remplie de "La canne de Jeanne" Ils savent tous les paroles et se joignent à elle.
Bella lit un poème composé pour l'occasion :

« On peut danser la polka
En pyjama
On peut manger la polenta
En pyjama
On peut faire la java
En pyjama
Ou boire du cola
En pyjama
Mais on ne peut pas voyager à Odessa
En pyjama
Ce serait pas beau
Il vaut mieux en pyjama
Faire dodo. »

Cara récite une longue tirade de sa pièce. Dana fait un tour de magie. Elisa danse. Flavia ne fait rien, à part manger du pop-corn. Flavia ne veut jamais rien faire. Et Gabriel, regarde ses sœurs, béat et admiratif.
Emma aimerait rester toute sa vie avec eux, même si elle n'aime rien de ce qu'ils mangent.
Cara récite un nouveau passage d'une pièce que personne ne comprend :
« Adieu, Camille, retourne à ton couvent, et lorsqu'on te fera de ces récits hideux qui t'ont empoisonnée, réponds ce que je vais te dire : Tous les hommes sont menteurs, inconstants, faux, bavards, hypocrites, orgueilleux et lâches, méprisables et sensuels ; toutes les femmes sont perfides, artificieuses, vaniteuses, curieuses et dépravées ; le monde n'est qu'un égout sans fond où les phoques les plus informes rampent et se tordent sur des montagnes de fange ; mais il y a au monde une chose sainte et sublime, c'est l'union de deux de ces êtres si imparfaits et si affreux. On est souvent trompé en amour, souvent blessé et souvent malheureux ; mais on aime, et quand on est sur le bord de sa tombe, on se retourne pour regarder en arrière ; et on se dit : “ J'ai souffert souvent, je me suis trompé quelquefois, mais j'ai aimé. C'est moi qui ai vécu, et non pas un être factice créé par mon orgueil et mon ennui. »

Au milieu de sa performance, les parents rentrent silencieusement comme des voleurs.
Ils ne se sont pas pressés pour revenir. Mais maintenant ils sont fatigués. Ils regardent leurs enfants avec une tendresse énorme. Ils sont géniaux !
N'est-ce pas qu'ils ont raison de faire confiance à leurs enfants ? 
