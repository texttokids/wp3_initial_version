﻿Pourquoi je dois me laver les dents ?
Parce que c’est bon pour la santé le dentifrice à la fraise ?
Pour faire comme dans la publicité ?
Parce que quand ça mousse dans la bouche c’est rigolo ?
Quand tu manges du sucre par exemple, il y a des caries qui viennent s’installer dans tes dents. Elles aiment le sucre et viennent le manger.
Comme tu leur donnes du sucre tous les jours, elles restent dans tes dents et creusent des trous pour s’installer. Ces trous abiment tes dents et elles peuvent tomber !
Oh non ! Je ne veux pas qu’elles tombent !
Quand tu nettoies tous les jours tes dents, le sucre ne reste pas dessus et les caries ne s’installent pas.
Et en plus, ça sent bon une bouche toute propre !
Et Mamie, toutes ses dents sont tombées, il faut lui dire de se brosser les dents !
