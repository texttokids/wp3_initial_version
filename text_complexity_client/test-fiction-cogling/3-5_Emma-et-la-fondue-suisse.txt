Emma et la fondue suisse


Emma aime manger, mais elle aime une chose plus que toute autre. Elle aime le fromage plus que le chocolat (bien qu’elle adore le chocolat !), plus que les cornets de glace (bien qu’elle fasse la queue pour en acheter), plus que les pâtes, la purée et même les frites.
Elle s’affole aussi pour tout ce qui contient du fromage : la pizza, le gratin, le croque monsieur.
Mais le jour où elle est allée en Suisse avec mamie et elles ont mangé la fondue suisse dans un restaurant, Emma a su que la vie est vraiment belle et que la Suisse est un pays des plus sympathiques même s’il n’y a pas la mer. En Suisse il y a des montagnes très hautes, des lacs, des trains confortables, des vaches heureuses qui donnent leur lait à la fabrication du fromage le plus joyeux.
Le serveur porte un petit réchaud qu’il allume. Il pose dessus une casserole avec une espèce de soupe épaisse de fromage. Après il apporte un panier de grosses tranches de pain.
Dans le restaurant il y a d’autres enfants. Emma les voit empiler le pain sur leurs assiettes. Elle fait pareil. Elle les voit piquer le pain avec leurs longues fourchettes. Elle les imite. Elle les voit tremper les morceaux de pain dans le fromage fondu.
-Attention ! Si tu perds le pain, s’il se noie dans la fondue, tu dois payer à boire !
- Mais je n’ai pas d’argent Mamie !
- Alors, tu dois me faire un bisou.
- Ce n’est pas une punition Mamie. J’aime faire des bisous !
- Ne perds pas ton pain quand même !
Emma fait danser le pain dans le bain de fromage. Puis elle souffle et met le pain dégoulinant de fondue dans sa bouche. Quand ce pain et ce mélange de fromage chaud atterrissent sur sa langue, Emma sait que c’est un grand moment de sa vie.
Le pain ne sa fatigue pas de tournoyer dans la casserole et Emma ne se lasse pas de ce goût qui chante à travers ses dents.
Quand il n’y a plus rien, Emma demande : « On peut revenir en Suisse avec Papa, Maman, Antonin, la maîtresse et Grand-mère ? »
- On va plutôt ramener de quoi faire une fondue à la maison et on le préparera pour tout le monde.
-Et on peut revenir manger une fondue ce soir ?
-Deux fois par jour c’est trop !
Emma n’est pas d’accord. Elle aimerait en manger même au petit déjeuner.

Emma pense : Si les vaches sont heureuses, est-ce que ça veut dire que les gens sont heureux ?