Regarde dans la neige un grand renne caché au cœur de la forêt.
Regarde dans la neige deux oursons qui s'amusent au bord du lac gelé.
Regarde dans la neige trois oiseaux prêts à s'envoler.
Regarde dans la neige quatre marmottes qui sortent de leur terrier.
Regarde dans la neige cinq écureuils qui grimpent aux arbres.
Regarde dans la neige mille flocons qui tombent doucement.
