Personne	d’autre	n’a	un	éléphant.	
	
Chaque	jour,	j’emmène	mon	éléphant	se	promener.	
Pour	lui,	c’est	une	promenade	pleine	d’attention.	
	
Il	n’aime	pas	les	lignes	sur	le	trottoir.	
Je	reviens	toujours	en	arrière	pour	le	chercher.	
C’est	ce	que	font	les	amis	:	aider	à	franchir	les	lignes.	
	
Aujourd’hui	 j’emmène	 mon	 mini-éléphant	 au	 n°17.	 C’est	 la	 journée	 du	 Club	 des	
animaux,	et	tout	le	monde	y	sera.	
	
«	Allez,	viens	!	Voilà,	c’est	bien,	mon	grand	!	»	
Je	l’encourage	les	derniers	mètres	«	Tout	ira	bien.	»	
	
Quand	je	lève	la	tête,	il	y	a	un	panneau	sur	la	porte.	
	
Interdit	aux	éléphants	
	
Mon	mini-éléphant	me	ramène	jusqu’au	trottoir,	sans	se	soucier	des	lignes.		
C’est	ce	que	font	les	amis	:	braver	les	choses	effrayantes	pour	vous.	
	
«	Est-ce	 que	 toi	 aussi	 tu	 as	 essayé	 d’aller	 à	 la	 réunion	 du	 Club	 des	 animaux	?	
demande	la	fillette.	
-Oui,	mais	les	éléphants	ne	sont	pas	autorisés.	»	
	
La	pancarte	ne	disait	rien	à	propos	des	putois,	dit	la	fillette,	mais	ils	ne	veulent	
pas	non	plus	jouer	avec	nous.	
-	Ils	n’y	connaissent	rien.	»	
	
«	Il	ne	sent	pas	mauvais,	ajoute	la	fillette.		
-	Non	c’est	vrai	!	Et	si	on	fondait	notre	propre	club	?	»	
	
«	Allez,	viens	!	»	dis-je,	en	m’assurant	que	mon	mini-éléphant	me	suit	bien.	Parce	
que	c’est	ce	que	font	les	amis	:	ils	ne	laissent	jamais	personne	derrière	eux.	
	
«	On	peut	jouer	ici	»,	dit	l’un	de	nos	nouveaux	amis.	
	
«	Tous	ensemble.	»	
	
Alors	on	fabrique	notre	propre	pancarte.		

	

	
Bienvenue	à	tous	
	
Si	tu	veux,	mon	mini-éléphant	t’indiquera	le	chemin.	
Parce	que	c’est	ce	que	font	les	amis.	


