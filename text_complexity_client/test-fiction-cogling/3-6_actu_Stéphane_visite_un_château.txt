Aujourd'hui, il y a une sortie à l'école. Les enfants vont aller visiter le château de Versailles.
Stéphane est très heureux ! Il adore les histoires de rois et de reines.
Il est très fort en histoire à l'école. Il aime écouter la maîtresse leur raconter ce qu'il se passait autrefois dans les châteaux.
Toute la classe arrive au palais. Tout le monde dit "Ooooh !", car c'est très beau, très grand et ça brille de partout !
Quand Stéphane entre dans la Galerie des Glaces, il a des étoiles dans les yeux. Il aurait tant aimé vivre à cette époque. Il ferme les yeux.
Stéphane se retrouve en Roi Soleil. Il a une grosse couronne pleine de pierres précieuses, une cape rouge en fourrure et un sceptre.
Il avance dans le château, tous ses camarades de classe se sont transformés en princes, princesses et gentilshommes. Quand Stéphane passe devant eux, ils le saluent car c'est le Roi.
Stéphane est très fier d'être le Roi du château. Il sort dans le parc et contemple son royaume.
Soudain, il entend une voix lui dire : "Votre majesté accepte-t-elle de nous suivre dans le bus pour rentrer à l'école ?"
Stéphane sent soudain quelqu'un lui tirer l'oreille. Il ouvre les yeux. C'est la maîtresse !
Stéphane s'était endormi sur une chaise dans le château pendant la visite, il rêvait qu'il était un Roi et la maîtresse l’a réveillé. Rentrons à l'école raconter toutes ces aventures à Papa et Maman !
