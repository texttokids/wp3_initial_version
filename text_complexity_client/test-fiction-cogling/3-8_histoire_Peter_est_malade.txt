﻿Ce matin, Peter n'est pas très en forme, il s'est enrhumé.
Mais ce n'est pas un rhume qui va arrêter le grand pirate ! Peter aperçoit un bateau et décide de le prendre en chasse pour voler le trésor.
Le pirate essaie de rester en retrait pour ne pas se faire repérer. Quand tout à coup, il a une envie soudaine d'éternuer !
Peter essaie de se retenir en se bouchant le nez. Mais le rhume prend le dessus. Un éternuement énorme se fait entendre sur tout l'océan.
Le bruit a alerté les passagers du bateau qui commence à prendre de la vitesse.
Peter n'est pas content, il a laissé s'échapper un trésor...
Le pirate s'arrête sur une île pour déjeuner. Il sort sa canne à pêche et attend une prise.
Un banc de poissons se dirige autour de l'appât. Peter s'apprête à tirer sur le fil quand il laisse à nouveau échapper un énorme éternuement.
Les poissons s'éloignent alors tous de l'hameçon, laissant le pirate sans rien à se mettre sous la dent.
Il décide donc d'aller cueillir des fruits. "Au boins, les fruits n'auront pas peur à cause de bon rhube !" se dit-il.
Mais les éternuements se multiplient, Peter fait presque trembler les arbres.
Soudain, des tonnes de fruits tombent du ciel. Peter essaie de les éviter mais une noix de coco l’assomme, lui faisant une vilaine bosse.
Peter retourne enfin sur son bateau et va se mettre au lit. "Le rhube c'est nul, vivement debain que je puisse aller chasser à noubeau des trésors !" se dit le pirate en s'endormant.
FIN
