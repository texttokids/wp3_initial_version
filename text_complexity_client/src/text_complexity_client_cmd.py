import requests
import configparser
import ast
import os
import sys
from os import walk
from os.path import exists
import csv
import json
import time
import pandas as pd
import mdd
import re


def call_analysis_mdd_string(mdd_proc, nlp, levels: [], input_string: str, fdict, option=None):
    if option == 'TCOF':
        input_string = input_string.replace('\ufeff', '')
        input_string = input_string.replace("\\n", ". ")
        input_string = input_string.replace("..", ".")
    else:
        input_string = input_string.replace('\ufeff', '')
        input_string = input_string.replace("\\n", " ")
    doc = mdd.stanza_doc(nlp, input_string)
    retval = {}
    if 'mdd' in mdd_proc:
        # mdd.stanza_display(doc, "verbose")
        if 'text' in levels:
            tval = mdd.stanza_mdd_text(doc)
            hval = mdd.stanza_mhd_text(doc)
            retval['mdd_texte'] = [{'text_id': 0, 'mdd': tval, 'mhd': hval}]
        if 'sentence' in levels:
            sval = mdd.stanza_mdd_mhd_sentence(doc)
            if len(sval) == 0:
                return None
            retval['mdd_phrase'] = sval
    if 'freq' in mdd_proc:
        # mdd.stanza_display(doc, "verbose")
        # print(levels)
        if 'text' in levels:
            tval = mdd.stanza_freq_text(doc, fdict)
            retval['freq_texte'] = [{'text_id': 0, 'freq': tval}]
        if 'sentence' in levels:
            sval = mdd.stanza_freq_sentence(doc, fdict)
            if len(sval) == 0:
                return None
            retval['freq_phrase'] = sval
    return retval

def call_analysis_mdd(mdd_proc, nlp, levels: [], file_path: str, fdict, option=None):
    input_file = open(file_path, mode='r', encoding='utf-8')
    file_text = json.dumps(input_file.read(), ensure_ascii=False)[1:-1]
    input_file.close()
    return call_analysis_mdd_string(mdd_proc, nlp, levels, file_text, fdict, option)


def call_analysis_service(server_address: str, descriptors: [], levels: [], file_path: str):
    print('Call analysis for file ' + file_path)

    input_file = open(file_path, mode='r', encoding='utf-8')
    file_text = json.dumps(input_file.read(), ensure_ascii=False)[1:-1]
    # print(file_text)
    input_file.close()
    data = {'text': file_text, 'processors': descriptors, 'levels': levels}
    response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)

    print('Response code received for file ' + file_path + ': ' + str(response))
    # print(response.json())
    return response.json()


def call_analysis_service_string(server_address: str, descriptors: [], levels: [], input_string: str):
    # print('+:: ' + input_string)

    file_text = json.dumps(input_string, ensure_ascii=False)[1:-1]
    data = {'text': file_text, 'processors': descriptors, 'levels': levels}
    response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)

    print('Response code received for the string: ' + str(response))
    return response.json()


def write_analysis_block_result_csv(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content,
                                    type_elt, text_origin=None):
    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            if type_elt == "sentence":
                if text_origin == None:
                    titles = ['id_text', 'document', 'sent_id', 'sentence']
                else:
                    titles = ['id_text', 'text_origin', 'document', 'sent_id', 'sentence']
            else:
                if text_origin == None:
                    titles = ['id_text', 'document']
                else:
                    titles = ['id_text', 'text_origin', 'document']
            for descriptor_field in descriptor_block_content.keys():
                if not 'text_id' in descriptor_field and not 'sent_id' in descriptor_field and not 'sentence_sentence' in descriptor_field:
                    titles.append(descriptor_field)
            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        if text_origin != None:
            raw_values = [text_id, text_origin, input_file_path.replace('\\', '/')]
        else:
            raw_values = [text_id, input_file_path.replace('\\', '/')]
        if type_elt == "sentence":
            sentid = 'NA'
            sentsent = 'NA'
            for k in descriptor_block_content.keys():
                if 'sent_id' in k:
                    sentid = descriptor_block_content[k]  # 'sent_id'
                if 'sentence_sentence' in k:
                    sentsent = descriptor_block_content[k]  # 'sentence'
            raw_values.append(sentid)
            raw_values.append(sentsent)
        for analyzed_element in descriptor_block_content.keys():
            if not 'text_id' in analyzed_element and not 'sent_id' in analyzed_element and not 'sentence_sentence' in analyzed_element:
                raw_values.append(descriptor_block_content[analyzed_element])
        csv_file_writer.writerow(raw_values)


def get_analysis_block_result_type(descriptor_block_content):
    analysis_block_type = 'unknown'
    if len(descriptor_block_content) > 0:
        for descriptor_field in descriptor_block_content[0].keys():
            if descriptor_field == 'sent_id':
                analysis_block_type = 'sentence'
                break
            elif descriptor_field == 'text_id':
                analysis_block_type = 'text'
                break
    return analysis_block_type


def addexttofilename(suffix, filename):
    triplet = filename.rpartition(".")
    if (triplet[0] == ""):
        # dot not found
        return filename + suffix
    else:
        return triplet[0] + suffix + "." + triplet[2]


def write_analysis_result_global(output_file: str, text_id: int, input_file_path: str, levels, analysis,
                                 output_format: str, text_origin=None):
    if not hasattr(analysis, "keys"):
        print('no results for ', input_file_path)
        return

    # print(analysis)

    # do analysis per analysis
    global_analysis_text = {}
    global_analysis_sentence = []
    for descriptor_block_key in analysis.keys():
        # print("DESCRIPTEUR: " + str(descriptor_block_key))
        analysis_block = analysis[descriptor_block_key]
        analysis_block_type = get_analysis_block_result_type(analysis_block)

        for analysis_element in analysis_block:
            if analysis_block_type == 'text':
                # print('TEXT')
                # print(analysis_element)
                for processor_key in analysis_element.keys():
                    # print(processor_key)
                    full_key = descriptor_block_key + "_" + str(processor_key) + "_text"
                    global_analysis_text[full_key] = analysis_element[processor_key]
            elif analysis_block_type == 'sentence':
                # print('SENT')
                # print(analysis[descriptor_block_key])
                sid = analysis_element['sent_id']
                if (len(global_analysis_sentence) <= sid):
                    global_analysis_sentence.append({})
                for processor_key in analysis_element.keys():
                    full_key = descriptor_block_key + "_" + str(processor_key) + "_sentence"
                    global_analysis_sentence[sid][full_key] = analysis_element[processor_key]

    # print(global_analysis_text)
    # print(global_analysis_sentence)

    # now print results
    if output_format == "CSV":
        if 'text' in levels and 'sentence' in levels:
            # creation de deux noms de fichiers parallèles
            output_file_text = addexttofilename("_text", output_file)
            write_analysis_block_result_csv(output_file_text, text_id, input_file_path, global_analysis_text, "text",
                                            text_origin)
            output_file_sentence = addexttofilename("_sentence", output_file)
            for gas in global_analysis_sentence:
                write_analysis_block_result_csv(output_file_sentence, text_id, input_file_path, gas, "sentence",
                                                text_origin)
        else:
            if 'text' in levels:
                write_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_text, "text",
                                                text_origin)
            if 'sentence' in levels:
                for gas in global_analysis_sentence:
                    write_analysis_block_result_csv(output_file, text_id, input_file_path, gas, "sentence", text_origin)
    else:
        print("XLSX Format not yet implemented")
        sys.exit(3)

    # if output_format == "CSV":
    #     output_file_path = output_file + "_text.csv"
    #     for descriptor_block_key in global_analysis_text.keys():
    #         write_text_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_text[descriptor_block_key])
    #     output_file_path = output_file + "_sentence.csv"
    #     for descriptor_block_key in global_analysis_sentence.keys():
    #         write_sentence_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_sentence[descriptor_block_key])
    # else:
    #     print("XLSX Format not yet implemented")
    #     sys.exit(3)

def sumof(analysis1, analysis2):
    for i in analysis2:
        analysis1[i] = analysis2[i]
    return analysis1

# process(server_address, input_folder_files, descriptors, levels, output_folder_file, output_format)
def process(server_address: str, input_folder: [], descriptors: [], levels: [], output_folder_file: str,
            output_format: str, option = None, sheet_number: int = 0, column_number: int = 0, freq_file: str = ''):
    start_time = time.time()

    file_paths = []
    for iff in input_folder:
        if (os.path.isfile(iff)):
            file_paths.append(iff)
        else:
            for (dir_path, dir_names, file_names) in walk(iff):
                for file_name in file_names:
                    if (file_name.endswith('.txt')):
                        file_paths.append(dir_path + '/' + file_name)
    file_paths.sort()
    if (os.path.isdir(output_folder_file)):
        output_file = output_folder_file + '/' + 'full_result.csv'
    else:
        output_file = output_folder_file

    print("Descriptors: " + str(descriptors))
    mdd_proc = []
    fdict = {}
    if freq_file != '':
        import pandas
        df = pandas.read_csv(freq_file, header=None)
        words = df.loc[:,0]
        cnts = df.loc[:,1]
        sizefdict = sum(cnts)
        for i in range(0, len(words)):
            fdict[str(words[i])] = (cnts[i]/sizefdict) * 1000000
    if 'mdd' in descriptors or 'freq' in descriptors:
        nlp = mdd.stanza_init()
        if 'mdd' in descriptors:
            descriptors.remove('mdd')
            mdd_proc.append('mdd')
        if 'freq' in descriptors:
            if freq_file == '':
                print('The freqfile is mandatory with the freq option')
                exit(1)
            descriptors.remove('freq')
            mdd_proc.append('freq')
        print('Computing MMD or FREQ processor')
    text_id = 0
    analysis_others = None
    for file_path in file_paths:
        if file_path.endswith(".txt"):
            print('process ' + file_path)
            if len(mdd_proc) > 0 and len(descriptors) == 0:
                analysis_mdd = call_analysis_mdd(mdd_proc, nlp, levels, file_path, fdict, option)
                if analysis_mdd is not None:
                    write_analysis_result_global(output_file, text_id, file_path, levels, analysis_mdd, output_format)
            elif len(mdd_proc) == 0 and len(descriptors) > 0:
                # Call analysis service
                analysis_others = call_analysis_service(server_address, descriptors, levels, file_path)
                if analysis_others is not None:
                    write_analysis_result_global(output_file, text_id, file_path, levels, analysis_others, output_format)
            elif  len(mdd_proc) > 0 and len(descriptors) > 0:
                analysis_mdd = call_analysis_mdd(mdd_proc, nlp, levels, file_path, fdict, option)
                analysis_others = call_analysis_service(server_address, descriptors, levels, file_path)
                if analysis_others is not None:
                    analysis = sumof(analysis_mdd, analysis_others)
                    write_analysis_result_global(output_file, text_id, file_path, levels, analysis, output_format)
        elif file_path.endswith(".xlsx"):  # or file_path.endswith(".csv")
            print('process ' + file_path)
            print('Fichier de format EXCEL à convertir sur la première colonne première feuille')
            df = pd.read_excel(file_path, sheet_name=sheet_number)
            # if file_path.endswith(".csv"):
            # df = pd.read_csv(file_path)
            dftab = df.to_numpy()
            if (len(dftab[0]) < 1):
                print("Bad format for file: " + str(file_path))
            else:
                tabid = 1
                print(str(len(dftab)) + " cellules à traiter")
                for nl in range(0, len(dftab)):
                    if (isinstance(dftab[nl, column_number], str) == True):
                        scell = dftab[nl, column_number]
                        scell = scell.lstrip()
                        scell = scell.rstrip()
                        if len(scell) < 1:
                            continue
                        print(str(nl) + ':+ ' + scell)
                        #                        analysis = call_analysis_service_string(server_address, descriptors, levels, scell)
                        if len(mdd_proc) > 0:
                            analysis = call_analysis_mdd_string(mdd_proc, nlp, levels, scell, fdict, option)
                        else:
                            # Call analysis service
                            analysis = call_analysis_service_string(server_address, descriptors, levels, scell)
                        write_analysis_result_global(output_file, text_id, file_path + " " + str(tabid), levels,
                                                     analysis, output_format, scell)
                    tabid += 1
        else:
            print("Unknown format for file: " + str(file_path))
            print("Cannot process")
        text_id += 1

    print("--- %s seconds ---" % (time.time() - start_time))
    exit(0)


def help():
    print(
        'USAGE: text_complexity_client_cmd --server adr --format CSV/XLSX --output foldername --processor processor-name \
        --all --noage --allmdd --text --sentence --sheet --column --freq filename ... list of files or repertories to process ...')
    print('Levels: text ou sentence (voir options)')
    print(
        'Processeurs disponibles: graphie, phonetique, niveau_lexical, richesse_lexicale, adjectifs_ordinaux, \
        lexions_verbales, parties_du_discours, pronoms, pluriels, dependances_syntaxiques, structures_passives, \
        tructures_syntaxiques, superlatifs_inferiorite, adverbiaux_temporels, modalite, emotions, \
        connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores, mdd, freq')
    print('Processeur age disponible seulement sur certains serveurs')
    sys.exit(1)


def main(args):
    server_address = 'http://localhost:8081'
    input_folder_files = ["./input"]
    all_descriptors_no_age = ["graphie", "phonetique", "niveau_lexical", "richesse_lexicale", "adjectifs_ordinaux",
                              "flexions_verbales", "parties_du_discours", "pronoms", "pluriels",
                              "dependances_syntaxiques", "structures_passives",
                              "structures_syntaxiques", "superlatifs_inferiorite", "adverbiaux_temporels", "modalite",
                              "emotions", "emotyc", "connecteurs_organisateurs", "entites_nommees",
                              "propositions_subordonnees", "metaphores"]
    all_descriptors = all_descriptors_no_age + ["age"]
    descriptors = []
    levels = []
    output_folder_file = "./output"
    output_format = "CSV"
    config = configparser.ConfigParser()
    init_file = os.path.abspath(__file__).replace('.py', '.ini')
    sheet_number = 0
    column_number = 0
    option = None
    if (exists(init_file)):
        try:
            config.read(init_file)
            server_address = str(config['SERVER']['ADDRESS'])
            input_folder = str(config['CONFIG']['INPUT_FOLDER'])
            descriptors = ast.literal_eval(config['CONFIG']['PROCESSORS'])
            levels = ast.literal_eval(config['CONFIG']['LEVELS'])
            output_folder_file = str(config['CONFIG']['OUTPUT_FOLDER'])
        except Exception as e:
            read_config_path_exception_string = str(e)
            print("ERROR: while reading config:" + read_config_path_exception_string)

    str_descriptors = ''
    freq_file = ''
    if len(sys.argv) > 1:
        import getopt
        try:
            optlist, arguments = getopt.gnu_getopt(sys.argv, 'hf:o:p:ants',
                                               ['help', 'format=', 'server=', 'output=', 'processor=', 'all', 'allmdd', 'noage',
                                                'text', 'sentence', 'sheet=', 'column=', 'tcof', 'freq='])
        except getopt.GetoptError as param:
            print("Parameter error.")
            print(param)
            help()
            exit(1)
        input_folder_files = arguments[1:]
        for oarg in optlist:
            if (oarg[0] == '-h' or oarg[0] == '--help'):
                help()
            if (oarg[0] == '-t' or oarg[0] == '--text'):
                levels.append("text")
            if (oarg[0] == '-s' or oarg[0] == '--sentence'):
                levels.append("sentence")
            if (oarg[0] == '-o' or oarg[0] == '--output'):
                output_folder_file = oarg[1]
            if (oarg[0] == '--freq'):
                freq_file = oarg[1]
            if (oarg[0] == '-f' or oarg[0] == '--format'):
                output_format = oarg[1]
                if (output_format != "CSV" and output_format != "XLSX"):
                    print("Bad format:" + output_format)
                    sys.exit(2)
            if (oarg[0] == '--sheet'):
                if (oarg[1].isdigit() is not True):
                    print("Sheet number should be an positive integer, not:" + oarg[1])
                    sys.exit(2)
                else:
                    sheet_number = int(oarg[1])
            if (oarg[0] == '--column'):
                if (oarg[1].isdigit() is not True):
                    print("Column number should be an positive integer, not:" + oarg[1])
                    sys.exit(2)
                else:
                    column_number = int(oarg[1])
            if (oarg[0] == '--server'):
                server_address = oarg[1]
            if (oarg[0] == '--tcof'):
                option = 'TCOF'
                print("Format of input type TCOF")
            if (oarg[0] == '-p' or oarg[0] == '--processor'):
                str_descriptors = oarg[1]
            if (oarg[0] == '--all' or oarg[0] == '-a'):
                descriptors = all_descriptors
            if (oarg[0] == '--allmdd'):
                descriptors = all_descriptors
                descriptors.append('mdd')
            if (oarg[0] == '--noage'):
                descriptors = all_descriptors_no_age
        if (len(input_folder_files) < 1):
            print("No input file(s)")
            sys.exit(2)
        if (str_descriptors != ''):
            descriptors = str_descriptors.split(',')
        if freq_file != '' and 'freq' not in descriptors:
            descriptors.append('freq')

    if (len(levels) < 1): levels = ["text", "sentence"]
    if (len(descriptors) < 1):
        print('Processeurs par défaut: tous sauf age')
        descriptors = all_descriptors_no_age

    print("Server= " + server_address + "\nInputFile= " + str(input_folder_files) + "\nProcessor= " + str(
        descriptors) + "\nLevel= " + str(
        levels) + "\nOutputFile= " + output_folder_file + "\nFormat= " + output_format + "\n")
    process(server_address, input_folder_files, descriptors, levels, output_folder_file, output_format, option, sheet_number,
            column_number, freq_file)

if __name__ == "__main__":
    main(sys.argv[1:])
