import os
import sys
from os import walk
import re
import csv
import json
#import nltk
import mdd

def read_file(file_path: str):
    input_file = open(file_path, mode='r', encoding='utf-8')
    file_text = json.dumps(input_file.read(), ensure_ascii=False)[1:-1]
    input_file.close()
    return file_text

def clean_text(text):
    text = text.replace('\ufeff', '')
    text = text.replace('\\n', ' ')
    return text

# def split_into_words(text):
#     words = nltk.tokenize.word_tokenize(text, language='french', preserve_line=False)
#     return words
#
# def count_words_nltk(filewords, cnt, dict):
#     for w in filewords:
#         if "'" in w:
#             ws = w.split("'")
#             for w2 in ws:
#                 if skip(w2):
#                     continue
#                 w2 = w2.lower()
#                 if w2 in dict:
#                     dict[w2] = dict[w2] + 1
#                 else:
#                     dict[w2] = 1
#                 cnt = cnt + 1
#         else:
#             if skip(w):
#                 continue
#             w = w.lower()
#             if w in dict:
#                 dict[w] = dict[w] + 1
#             else:
#                 dict[w] = 1
#             cnt = cnt + 1
#     return (cnt, dict)

def skip(s):
    if re.search(r'[@#?+°,.;/:§!&|"_=\[\]{}()\d]', s):
        return True
    if "xx" in s:
        return True
    if "yy" in s:
        return True
    if "ww" in s:
        return True
    if s == "-":
        return True
    if s == "'":
        return True
    if s == "":
        return True
    return False

def process(input_folder, output_file):
    nlp = mdd.stanza_init()
    file_paths = []
    for iff in input_folder:
        if os.path.isfile(iff):
            file_paths.append(iff)
        else:
            for (dir_path, dir_names, file_names) in walk(iff):
                for file_name in file_names:
                    if file_name.endswith('.txt'):
                        file_paths.append(dir_path + '/' + file_name)
    dict = {}
    cnt = 0
    file_paths.sort()
    for file_path in file_paths:
        if file_path.endswith(".txt"):
            print('get ' + file_path)
            filectn = read_file(file_path)
            filectn = clean_text(filectn)
            # filedoc = split_into_words(filectn)
            # (cnt, dict) = count_words_nltk(filedoc, cnt, dict)
            filedoc = nlp(filectn)
            (cnt, dict) = mdd.stanza_count_words_text(filedoc, cnt, dict)
    print('Total words is ' + str(cnt))
    with open(output_file, 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in dict.items():
            writer.writerow([key, value])
    csv_file.close()

def help():
    print(
        'USAGE: build_dict list of files or repertories to process ...')
    sys.exit(1)

def main(args):
    if len(sys.argv) <= 1:
        help()

    import getopt
    try:
        optlist, arguments = getopt.gnu_getopt(sys.argv, 'ho:',
                                           ['help', 'output='])
    except getopt.GetoptError as param:
        print("Parameter error.")
        print(param)
        help()
        exit(1)
    input_folder_files = arguments[1:]
    output_file = ""
    for oarg in optlist:
        if oarg[0] == '-h' or oarg[0] == '--help':
            help()
        if oarg[0] == '-o' or oarg[0] == '--output':
            output_file = oarg[1]
    if len(input_folder_files) < 1:
        print("No input file(s)")
        sys.exit(2)
    if output_file == "":
        print("No output file")
        sys.exit(2)
    process(input_folder_files, output_file)

if __name__ == "__main__":
    main(sys.argv[1:])
