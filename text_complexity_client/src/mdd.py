import stanza
import build_dict as bd

def dd(head, id):
	if head == 0: return 0
	return abs(head - id)
def hd(head, id, sentence):
	if head == 0: return 0
	nblnk = 0
	#print(sentence.words[id-1].text + ' ' + str(head) + ' ' + str(id))
	while head != 0:
		nblnk = nblnk + 1
		id = head
		head = sentence.words[id-1].head
		#print(sentence.words[id-1].text + ' ' + str(head) + ' ' + str(id))
	return nblnk

def stanza_display(doc, options = ''):
	if options == "verbose":
		for sentence in doc.sentences:
			for word in sentence.words:
				print(word.pretty_print())
	else:
		for sentence in doc.sentences:
			print(len(sentence.words))
			for word in sentence.words:
				print(word.id, word.text, word.lemma, word.pos, word.feats, word.deprel, word.head, dd(word.head, word.id))
	print('MDD (total): ' + str(stanza_mdd_text(doc)))
	print('MHD (total): ' + str(stanza_mhd_text(doc)))
	# for sentence in doc.sentences:
	# 	print(sentence.ents)
	# 	print(sentence.dependencies)

def stanza_count_words_text(doc, cnt, dict):
	for sentence in doc.sentences:
		ls = len(sentence.words)
		for word in sentence.words:
			if bd.skip(word.text):
				continue
			lword = word.text.lower()
			if lword in dict:
				dict[lword] = dict[lword] + 1
			else:
				dict[lword] = 1
			cnt = cnt + 1
	return (cnt, dict)

def stanza_freq_text(doc, dict):
	tot = 0
	nbw = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		for word in sentence.words:
			#print(word.text)
			if bd.skip(word.text):
				#print('Skip: ' + word.text)
				continue
			w = word.text.lower()
			if w in dict:
				tot += dict[w]
				nbw = nbw + 1
			else:
				print('not in dict: (' + str(w) + ')') # + str(word))
	if nbw != 0:
		return tot/nbw
	else:
		return 0

def stanza_mdd_text(doc):
	totmdd = 0
	totlen = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		ddsum = 0
		for word in sentence.words:
			ddsum += dd(word.head, word.id)
		if ls >= 2:
			totmdd += ddsum
			totlen += ls-1
	if totlen < 1:
		return 0
	else:
		return totmdd/totlen

def stanza_mhd_text(doc):
	totmhd = 0
	totlen = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		hdsum = 0
		for word in sentence.words:
			hdsum += hd(word.head, word.id, sentence)
		if ls >= 2:
			totmhd += hdsum
			totlen += ls-1
	if totlen < 1:
		return 0
	else:
		return totmhd/totlen

def stanza_freq_sentence(doc, dict):
	tots = []
	nsent = 0
	for sentence in doc.sentences:
		tot = 0
		nbw = 0
		s = ''
		for word in sentence.words:
			if bd.skip(word.text):
				continue
			w = word.text.lower()
			if w in dict:
				tot += dict[w]
				nbw = nbw + 1
				s = s + '|' + str(w) + '--' + str(dict[w])
			else:
				s = s + '|' + str(w) + '--not-in-dict-'
				#print('not in dict: ' + str(w))
		if nbw != 0:
			val = tot / nbw
		else:
			val = 0
		senttot = {'sent_id': nsent, 'sentence': sentence.text, 'freq': val, 'freqwords': s}
		tots.append(senttot)
		nsent = nsent + 1
	return tots

def stanza_mdd_sentence(doc):
	mdds = []
	nsent = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		ddsum = 0
		for word in sentence.words:
			ddsum += dd(word.head, word.id)
		if ls >= 2:
			sentmdd = { 'sent_id': nsent, 'sentence': sentence.text, 'mdd': ddsum/(ls-1) }
			mdds.append(sentmdd)
			nsent = nsent + 1
	return mdds

def stanza_mhd_sentence(doc):
	mhds = []
	nsent = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		hdsum = 0
		for word in sentence.words:
			hdsum += hd(word.head, word.id, sentence)
		if ls >= 2:
			sentmhd = { 'sent_id': nsent, 'sentence': sentence.text, 'mhd': hdsum/(ls-1) }
			mhds.append(sentmhd)
			nsent = nsent + 1
	return mhds

def stanza_mdd_mhd_sentence(doc):
	mds = []
	nsent = 0
	for sentence in doc.sentences:
		ls = len(sentence.words)
		ddsum = 0
		hdsum = 0
		for word in sentence.words:
			ddsum += dd(word.head, word.id)
			hdsum += hd(word.head, word.id, sentence)
		if ls >= 2:
			sentmdd = { 'sent_id': nsent, 'sentence': sentence.text, 'mdd': ddsum/(ls-1), 'mhd': hdsum/(ls-1) }
			mds.append(sentmdd)
			nsent = nsent + 1
		else:
			sentmdd = {'sent_id': nsent, 'sentence': sentence.text, 'mdd': 0, 'mhd': 0}
			mds.append(sentmdd)
			nsent = nsent + 1
	return mds

def stanza_init():
	return stanza.Pipeline('fr', download_method=None)

def stanza_doc(nlp, text):
	return nlp(text)

def help():
    print('USAGE: teststanza.py --help --verbose --files "list of sentences or files to process ..."')
    sys.exit(1)

if __name__ == "__main__":
	import sys
	import getopt
	display = ""
	optlist, arguments = getopt.gnu_getopt(sys.argv, 'hvf', ['help', 'verbose', 'files'])
	inputs = arguments[1:]
	for oarg in optlist:
		if (oarg[0] == '-h' or oarg[0] == '--help'):
			help()
		if (oarg[0] == '-v' or oarg[0] == '--verbose'):
			display = "verbose"

	nlp = stanza_init()
	if len(inputs) < 1:
		doc = nlp('il a fait du bleu !')
		stanza_display(doc, display)
	else:
		for data in inputs:
			doc = nlp(data)
			stanza_display(doc, display)
