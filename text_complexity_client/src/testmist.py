import configparser
import sys
import os
from os import walk
from os.path import exists

def main(args):
    server_address = 'http://localhost:8081'
    input_folder_files = ["./input"]
    descriptors = ["graphie", "phonetique", "niveau_lexical", "richesse_lexicale", "adjectifs_ordinaux",
        "flexions_verbales", "parties_du_discours", "pronoms", "pluriels","dependances_syntaxiques", "structures_passives", 
        "structures_syntaxiques", "superlatifs_inferiorite", "adverbiaux_temporels", "modalite", "emotions", 
        "connecteurs_organisateurs", "entites_nommees", "propositions_subordonnees", "metaphores", "age"]
    levels = ["text","sentence"]
    output_folder_file = "./output"
    output_format = "CSV"
    config = configparser.ConfigParser()
    init_file = os.path.abspath(__file__).replace('.py', '.ini')
    if (exists(init_file)):
        try:
            config.read(init_file)
            server_address = str(config['SERVER']['ADDRESS'])
            input_folder = str(config['CONFIG']['INPUT_FOLDER'])
            descriptors = ast.literal_eval(config['CONFIG']['PROCESSORS'])
            levels = ast.literal_eval(config['CONFIG']['LEVELS'])
            output_folder_file = str(config['CONFIG']['OUTPUT_FOLDER'])
        except Exception as e:
            read_config_path_exception_string = str(e)
            print("ERROR: while reading config:" + read_config_path_exception_string)

    str_descriptors = ''
    if len(sys.argv) > 1:
        from pymist import parse
        #print(sys.argv)
        mist = parse(args)
        #print(mist)
        if ('h' in mist or 'help' in mist):
            print('USAGE: text_complex_client --server adr --format CSV/XLSX --output foldername --text --sentence ... list of files or repertories to process ...')
            sys.exit(1)
        input_folder_files = mist['_']
        if ('t' in mist or 'text' in mist) and ('s' in mist or 'sentence' in mist):
            levels = ["text", "sentence"]
        else:
            if ('t' in mist or 'text' in mist):
                levels = ["text"]
            if ('s' in mist or 'sentence' in mist):
                levels = ["sentence"]
        if ('o' in mist):
            output_folder_file = mist['o']
        if ('output' in mist):
            output_folder_file = mist['output']
        if ('format' in mist):
            output_format = mist['format']
            if (output_format != "CSV" and output_format != "XLSX"):
                print("Bad format:" + output_format)
                sys.exit(2)
        if ('server' in mist):
            server_address = mist['server']
        if ('p' in mist):
            str_descriptors = mist['p']
        if ('processor' in mist):
            str_descriptors = mist['processor']
        if (str_descriptors != ''):
            descriptors = str_descriptors.split(',')

    print("SV= " + server_address + " IF= " + str(input_folder_files) + " P= " + str(descriptors) + " L= " + str(levels) + " OF= " + output_folder_file + " F= " + output_format)

if __name__ == "__main__":
    main(sys.argv[1:])
