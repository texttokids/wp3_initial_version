import requests
import configparser
import ast
import os
from os import walk
from os.path import exists
import csv
import json
import time
import sys

def call_analysis_service(server_address: str, descriptors: [], levels: [], file_path: str):
    print('Call analysis for file ' + file_path)

    input_file = open(file_path, mode='r', encoding='utf-8')
    file_text = json.dumps(input_file.read(), ensure_ascii=False)[1:-1]
    input_file.close()
    data = {'text': file_text, 'processors': descriptors, 'levels':levels}
    response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)

    print('Response code received for file ' + file_path + ': ' + str(response))
    return response.json()


def write_text_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                if descriptor_field != 'text_id':
                    titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        raw_values = [text_id, input_file_path.replace('\\', '/')]
        for analyzed_element in descriptor_block_content:
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
        csv_file_writer.writerow(raw_values)


def write_sentence_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
       
        for analyzed_element in descriptor_block_content:
            raw_values = [text_id, input_file_path.replace('\\', '/')]
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
            csv_file_writer.writerow(raw_values)


def get_analysis_block_result_type(descriptor_block_content):

    analysis_block_type = 'unknown'
    if len(descriptor_block_content) > 0:
        for descriptor_field in descriptor_block_content[0].keys():
            if descriptor_field == 'sent_id':
                analysis_block_type = 'sentence'
                break
            elif descriptor_field == 'text_id':
                analysis_block_type = 'text'
                break
    return analysis_block_type


def write_analysis_result(output_folder: str, text_id: int, input_file_path: str, analysis):
    if not hasattr(analysis, "keys"):
        print('no results for ', input_file_path)
        return

    for descriptor_block_key in analysis.keys():
        output_file_path = output_folder + '/' + descriptor_block_key + '.csv'

        analysis_block_type = get_analysis_block_result_type(analysis[descriptor_block_key])

        if analysis_block_type == 'text':
            write_text_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])
        elif analysis_block_type == 'sentence':
            write_sentence_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])


def process(server_address: str, input_folder: str, descriptors: [], levels: [], output_folder: str):
    
    start_time = time.time()

    file_paths = []
    for (dir_path, dir_names, file_names) in walk(input_folder):
        for file_name in file_names:
            if (file_name.endswith('.txt')):
                file_paths.append(dir_path + '/' + file_name)

    text_id = 0
    for file_path in file_paths:
        # Call analysis service
        analysis = call_analysis_service(server_address, descriptors, levels, file_path)

        write_analysis_result(output_folder, text_id, file_path, analysis)
        text_id += 1

    print("--- %s seconds ---" % (time.time() - start_time))
    exit(0)

def help():
    print('USAGE: text_complex_client --server adr --format CSV/XLSX --output foldername --processor processor-name --all --noage --text --sentence input_folder')
    print('Levels: text ou sentence (voir options)')
    print('Processeurs disponibles: graphie, phonetique, niveau_lexical, richesse_lexicale, adjectifs_ordinaux, flexions_verbales, parties_du_discours, pronoms, pluriels, dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite, adverbiaux_temporels, modalite, emotions, connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores')
    print('Processeur age disponible seulement sur certains serveurs')
    sys.exit(1)

def main(args):
    server_address = 'http://localhost:8081'
    input_folder = "./input"
    all_descriptors_no_age = ["graphie", "phonetique", "niveau_lexical", "richesse_lexicale", "adjectifs_ordinaux",
        "flexions_verbales", "parties_du_discours", "pronoms", "pluriels","dependances_syntaxiques", "structures_passives", 
        "structures_syntaxiques", "superlatifs_inferiorite", "adverbiaux_temporels", "modalite", "emotions", "emotyc",
        "connecteurs_organisateurs", "entites_nommees", "propositions_subordonnees", "metaphores"]
    all_descriptors = all_descriptors_no_age + ["age"]
    descriptors = []
    levels = []
    output_folder_file = "./output"
    config = configparser.ConfigParser()
    init_file = os.path.abspath(__file__).replace('.py', '.ini')
    if (exists(init_file)):
        try:
            config.read(init_file)
            server_address = str(config['SERVER']['ADDRESS'])
            input_folder = str(config['CONFIG']['INPUT_FOLDER'])
            descriptors = ast.literal_eval(config['CONFIG']['PROCESSORS'])
            levels = ast.literal_eval(config['CONFIG']['LEVELS'])
            output_folder_file = str(config['CONFIG']['OUTPUT_FOLDER'])
        except Exception as e:
            read_config_path_exception_string = str(e)
            print("ERROR: while reading config:" + read_config_path_exception_string)

    str_descriptors = ''
    if len(sys.argv) > 1:
        import getopt
        optlist, arguments = getopt.gnu_getopt(sys.argv, 'hf:o:p:ants', ['help', 'format=', 'server=', 'output=', 'processor=', 'all', 'noage', 'text', 'sentence'])
        input_folder = arguments[1]
        for oarg in optlist:
            if (oarg[0] == '-h' or oarg[0] == '--help'):
                help()
            if (oarg[0] == '-t' or oarg[0] == '--text'):
                levels.append("text")
            if (oarg[0] == '-s' or oarg[0] == '--sentence'):
                levels.append("sentence")
            if (oarg[0] == '-o' or oarg[0] == '--output'):
                output_folder_file = oarg[1]
            if (oarg[0] == '--server'):
                server_address = oarg[1]
            if (oarg[0] == '-p' or oarg[0] == '--processor'):
                str_descriptors = oarg[1]
            if (oarg[0] == '--all' or oarg[0] == '-a'):
                descriptors = all_descriptors
            if (oarg[0] == '--noage'):
                descriptors = all_descriptors_no_age
        if (len(arguments) < 2):
            print("No input file(s)")
            sys.exit(2)
        if (len(arguments) > 2):
            print("More than one input file(s)")
            sys.exit(2)
        if (str_descriptors != ''):
            descriptors = str_descriptors.split(',')

    if (len(levels) < 1): levels = ["text", "sentence"]
    if (len(descriptors) < 1):
        print('Processeurs par défaut: tous sauf age')
        descriptors = all_descriptors_no_age

    print("Server= " + server_address + "\nInputFile= " + str(input_folder) + "\nProcessor= " + str(descriptors) + "\nLevel= " + str(levels) + "\nOutputFile= " + output_folder_file + "\n")
    process(server_address, input_folder, descriptors, levels, output_folder_file)

if __name__ == "__main__":
    main(sys.argv[1:])
