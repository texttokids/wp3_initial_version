import sys
rashedur_location = "../../text_complexity_server/alternative/rashedur/"
sys.path.insert(1, rashedur_location)
from text_processor import alternative

def test_alternative(rep_input, rep_output, text_input, zipname, choice_levels, choice_processors):
    return alternative(rashedur_location,
        rep_input,
        rep_output,
        text_input,
        zipname,
        choice_levels,
        choice_processors,
        True)

if __name__ == "__main__":
    print("Chaine alternative")
    print("Levels: text,sentence")
    print("Processeurs: graphie, phonetique, niveau_lexical, richesse_lexicale, adjectifs_ordinaux, flexions_verbales,")
    print("parties_du_discours, pronoms, pluriels, dependances_syntaxiques, structures_passives, structures_syntaxiques,")
    print("superlatifs_inferiorite, adverbiaux_temporels, modalite, emotions, emotyc,")
    print("connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores")
#    print("argX: dossier ou se situe la commande")
    print("arg1: dossier input")
    print("arg2: dossier output")
    print("arg3: fichier entree")
    print("arg4: fichier zip de sortie ou nom de repertoire")
    print("arg5: levels")
    print("arg6: processeurs")
    sys.exit(test_alternative(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5].split(","), sys.argv[6].split(",")))
