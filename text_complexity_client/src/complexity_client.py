import requests
import configparser
import ast
import os
from os import walk
from os.path import exists
import csv
import json
import time
import sys

def call_analysis_service(server_address: str, descriptors: [], levels: [], file_path: str):
    print('Call analysis for file ' + file_path)

    input_file = open(file_path, mode='r', encoding='utf-8')
    file_text = json.dumps(input_file.read(), ensure_ascii=False)[1:-1]
    input_file.close()
    data = {'text': file_text, 'processors': descriptors, 'levels':levels}
    response = requests.post(server_address + '/api/text_complexity_linguistic_description/describe', json=data)
    print('Response code received for file ' + file_path + ': ' + str(response))
    if (response.status_code != 200):
        print('Non valid response: result ignored')
        return None
    else:
        return response.json()

def write_analysis_block_result_csv(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content, type_elt):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            if type_elt == "sentence":
                titles = ['id_text', 'document', 'sent_id', 'sentence']
            else:
                titles = ['id_text', 'document']
            for descriptor_field in descriptor_block_content.keys():
                if not 'text_id' in descriptor_field and not 'sent_id' in descriptor_field and not 'sentence_sentence' in descriptor_field:
                    titles.append(descriptor_field)
            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        raw_values = [text_id, input_file_path.replace('\\', '/')]
        if type_elt == "sentence":
            sentid = 'NA'
            sentsent = 'NA'
            for k in descriptor_block_content.keys():
                if 'sent_id' in k:
                    sentid = descriptor_block_content[k] # 'sent_id'
                if 'sentence_sentence' in k:
                    sentsent = descriptor_block_content[k] # 'sentence'
            raw_values.append(sentid)
            raw_values.append(sentsent)
        for analyzed_element in descriptor_block_content.keys():
            if not 'text_id' in analyzed_element and  not 'sent_id' in analyzed_element and not 'sentence_sentence' in analyzed_element:
                raw_values.append(descriptor_block_content[analyzed_element])
        csv_file_writer.writerow(raw_values)

def write_analysis_result_global(output_file: str, text_id: int, input_file_path: str, analysis, output_format: str):
    if not hasattr(analysis, "keys"):
        print('no results for ', input_file_path)
        return

    # print(analysis)

    # do analysis per analysis
    global_analysis_text = {}
    global_analysis_sentence = []
    for descriptor_block_key in analysis.keys():
        # print("DESCRIPTEUR: " + str(descriptor_block_key))
        analysis_block = analysis[descriptor_block_key]
        analysis_block_type = get_analysis_block_result_type(analysis_block)

        for analysis_element in analysis_block:
            if analysis_block_type == 'text':
                # print('TEXT')
                # print(analysis_element)
                for processor_key in analysis_element.keys():
                    # print(processor_key)
                    full_key = str(processor_key) + "_" + descriptor_block_key
                    global_analysis_text[full_key] = analysis_element[processor_key]
            elif analysis_block_type == 'sentence':
                # print('SENT')
                # print(analysis[descriptor_block_key])
                sid = analysis_element['sent_id']
                if (len(global_analysis_sentence) <= sid ):
                    global_analysis_sentence.append({})
                for processor_key in analysis_element.keys():
                    full_key = str(processor_key) + "_" + descriptor_block_key
                    global_analysis_sentence[sid][full_key] = analysis_element[processor_key]

    # print(global_analysis_text)
    # print(global_analysis_sentence)

    # now print results
    if output_format == "multiple":
        if bool(global_analysis_text):
            write_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_text, "text")
        if bool(global_analysis_sentence):
            for gas in global_analysis_sentence:
                write_analysis_block_result_csv(output_file, text_id, input_file_path, gas, "sentence")
    else:
        print("XLSX Format not yet implemented")
        sys.exit(3)

    # if output_format == "CSV":
    #     output_file_path = output_file + "_text.csv"
    #     for descriptor_block_key in global_analysis_text.keys():
    #         write_text_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_text[descriptor_block_key])
    #     output_file_path = output_file + "_sentence.csv"
    #     for descriptor_block_key in global_analysis_sentence.keys():
    #         write_sentence_analysis_block_result_csv(output_file, text_id, input_file_path, global_analysis_sentence[descriptor_block_key])
    # else:
    #     print("XLSX Format not yet implemented")
    #     sys.exit(3)

def write_text_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                if descriptor_field != 'text_id':
                    titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        raw_values = [text_id, input_file_path.replace('\\', '/')]
        for analyzed_element in descriptor_block_content:
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
        csv_file_writer.writerow(raw_values)


def write_sentence_analysis_block_result(output_file_path: str, text_id: int, input_file_path: str, descriptor_block_content):

    if not exists(output_file_path) and len(descriptor_block_content) > 0:
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_csv_file:
            csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)

            titles = ['id_texte', 'document']
            for descriptor_field in descriptor_block_content[0].keys():
                titles.append(descriptor_field)

            csv_file_writer.writerow(titles)

    with open(output_file_path, 'a', newline='', encoding='utf-8') as output_csv_file:
        csv_file_writer = csv.writer(output_csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
       
        for analyzed_element in descriptor_block_content:
            raw_values = [text_id, input_file_path.replace('\\', '/')]
            for descriptor_field_key in analyzed_element.keys():
                if descriptor_field_key != 'text_id':
                    raw_values.append(analyzed_element[descriptor_field_key])
            csv_file_writer.writerow(raw_values)


def get_analysis_block_result_type(descriptor_block_content):

    analysis_block_type = 'unknown'
    if len(descriptor_block_content) > 0:
        for descriptor_field in descriptor_block_content[0].keys():
            if descriptor_field == 'sent_id':
                analysis_block_type = 'sentence'
                break
            elif descriptor_field == 'text_id':
                analysis_block_type = 'text'
                break
    return analysis_block_type


def write_analysis_result(output_folder: str, text_id: int, input_file_path: str, analysis):
    if not hasattr(analysis, "keys"):
        print('no results for ', input_file_path)
        return

    if not exists(output_folder):
        os.mkdir(output_folder)

    for descriptor_block_key in analysis.keys():
        output_file_path = output_folder + '/' + descriptor_block_key + '.csv'

        analysis_block_type = get_analysis_block_result_type(analysis[descriptor_block_key])

        if analysis_block_type == 'text':
            write_text_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])
        elif analysis_block_type == 'sentence':
            write_sentence_analysis_block_result(output_file_path, text_id, input_file_path, analysis[descriptor_block_key])


def process(server_address: str, input_folder_file: [], descriptors: [], levels: [], output_folder_file: str, output_format: str):
    
    file_paths = []
    for iff in input_folder_file:
        if (os.path.isfile(iff)):
            file_paths.append(iff)
        else:
            for (dir_path, dir_names, file_names) in walk(iff):
                for file_name in file_names:
                    if (file_name.endswith('.txt')):
                        file_paths.append(dir_path + '/' + file_name)

    file_paths.sort()
    # print("Input files:" + str(file_paths))
    text_id = 0
    for file_path in file_paths:
        # Call analysis service
        start_time = time.time()
        analysis = call_analysis_service(server_address, descriptors, levels, file_path)
        print("--- %s seconds ---" % (time.time() - start_time))

        if (analysis != None):
            if output_format == "simple" or output_format == "multiple":
                if (os.path.isdir(output_folder_file)):
                    output_file = output_folder_file + '/' + 'full_result.csv'
                else:
                    output_file = output_folder_file
                write_analysis_result_global(output_file, text_id, file_path, analysis, output_format)
            else:
                write_analysis_result(output_folder_file, text_id, file_path, analysis)
        text_id += 1

    exit(0)

def help():
    print('USAGE: complexity_client --server adr --format single/multiple/sxlsx/mxlsx --output folderorfilename --processor processor-name --all --noage --text --sentence ... list of files or repertories to process ...')
    print('Levels: text ou sentence (voir options)')
    print('Processeurs disponibles: graphie, phonetique, niveau_lexical, richesse_lexicale, adjectifs_ordinaux, flexions_verbales, parties_du_discours, pronoms, pluriels, dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite, adverbiaux_temporels, modalite, emotions, connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores')
    print('Processeur age disponible seulement sur certains serveurs')
    sys.exit(1)

def main(args):
    server_address = 'http://localhost:8081'
    input_folder_file = ["./input"]
    all_descriptors_no_age = ["graphie", "phonetique", "niveau_lexical", "richesse_lexicale", "adjectifs_ordinaux",
        "flexions_verbales", "parties_du_discours", "pronoms", "pluriels","dependances_syntaxiques", "structures_passives", 
        "structures_syntaxiques", "superlatifs_inferiorite", "adverbiaux_temporels", "modalite", "emotions", 
        "connecteurs_organisateurs", "entites_nommees", "propositions_subordonnees", "metaphores"]
    all_descriptors = all_descriptors_no_age + ["age"]
    descriptors = []
    levels = []
    output_folder_file = "./output"
    output_format = "multiple"
    config = configparser.ConfigParser()
    init_file = os.path.abspath(__file__).replace('.py', '.ini')
    if (exists(init_file)):
        try:
            config.read(init_file)
            server_address = str(config['SERVER']['ADDRESS'])
            input_folder_file = [str(config['CONFIG']['INPUT_FOLDER'])]
            descriptors = ast.literal_eval(config['CONFIG']['PROCESSORS'])
            levels = ast.literal_eval(config['CONFIG']['LEVELS'])
            output_folder_file = str(config['CONFIG']['OUTPUT_FOLDER'])
        except Exception as e:
            read_config_path_exception_string = str(e)
            print("ERROR: while reading config:" + read_config_path_exception_string)

    str_descriptors = ''
    if len(sys.argv) > 1:
        import getopt
        try:
            optlist, arguments = getopt.gnu_getopt(sys.argv, 'hf:o:p:ants', ['help', 'format=', 'server=', 'output=', 'processor=', 'all', 'noage', 'text', 'sentence'])
        except Exception as e:
            print("Erreur argument:" + str(e))
            sys.exit(2)
        input_folder_file = arguments[1:]
        for oarg in optlist:
            if (oarg[0] == '-h' or oarg[0] == '--help'):
                help()
            if (oarg[0] == '-t' or oarg[0] == '--text'):
                levels.append("text")
            if (oarg[0] == '-s' or oarg[0] == '--sentence'):
                levels.append("sentence")
            if (oarg[0] == '-o' or oarg[0] == '--output'):
                output_folder_file = oarg[1]
            if (oarg[0] == '--format'):
                output_format = oarg[1]
                if (output_format not in ["simple", "multiple", "sxlsx", "mxlsx"]):
                    print("Bad format:" + output_format)
                    sys.exit(2)
            if (oarg[0] == '--server'):
                server_address = oarg[1]
            if (oarg[0] == '-p' or oarg[0] == '--processor'):
                str_descriptors = oarg[1]
            if (oarg[0] == '--all' or oarg[0] == '-a'):
                descriptors = all_descriptors
            if (oarg[0] == '--noage'):
                descriptors = all_descriptors_no_age
        if (len(input_folder_file) < 1):
            print("No input file(s)")
            sys.exit(2)
        if (str_descriptors != ''):
            descriptors = str_descriptors.split(',')

    if (len(levels) < 1):
        print("Level not choosed. Exit.")
        sys.exit(2);
        #levels = ["text", "sentence"]
    if (len(levels) > 1):
        print("Only one level at the same time. Exit.")
        sys.exit(2);
    if (len(descriptors) < 1):
        print('Processeurs par défaut: tous sauf age')
        descriptors = all_descriptors_no_age

    if (format == "multiple" and not os.path.isdir(output_folder_file)):
        print("avec l'option multiple, la destination doit être un répertoire")

    print("Server= " + server_address + "\nInputFile= " + str(input_folder_file) + "\nProcessor= " + str(descriptors) + "\nLevel= " + str(levels) + "\nOutputFile= " + output_folder_file + "\nFormat= " + output_format + "\n")
    process(server_address, input_folder_file, descriptors, levels, output_folder_file, output_format)

if __name__ == "__main__":
    main(sys.argv[1:])

# note
# on 322
# off /Users/cp/miniforge3/envs/t2k_env_M1/lib/python3.10/codecs.py
    # def decode(self, input, final=False):
    #     # decode input (taking the buffer into account)
    #     data = self.buffer + input
    #     try:
    #             (result, consumed) = self._buffer_decode(data, self.errors, final)
    #     except Exception as e:
    #             print('Decoding error avoided')
    #             self.buffer = data[1:]
    #             return '#'
    #     # keep undecoded input until the next call
    #     self.buffer = data[consumed:]
    #     return result