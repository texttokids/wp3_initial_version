BEGIN {
	level = ARGV[1]
	processor = ARGV[2]
	ARGV[1] = ""
	ARGV[2] = ""

	print "[SERVER]";
	# print "ADDRESS = http://vheborto-corliapi.inist.fr:8081";
	print "ADDRESS = http://localhost:8081";
	print "";
	print "[CONFIG]";
	print "INPUT_FOLDER = /Users/cp/brainstorm/text-to-kids/wp3/text_complexity_client/test-9textes";
	print "PROCESSORS = [\"" processor "\"]";
	print "OUTPUT_FOLDER = /Users/cp/brainstorm/text-to-kids/wp3/text_complexity_client/result-9textes-new";
	print "LEVELS = [\"" level "\"]"; 

	exit;
}
