# How to extract descriptors from a corpus

### Installlation
getopt ?

### Configuration

Edit `src/text_complexity_client.ini`:

- `ADDRESS`: the address of the extraction service (http://localhost:8081 if deployed locally)
- `INPUT_FOLDER`: the path to the input folder containing input texts
- `OUTPUT_FOLDER`: the path to the output folder where generated files will be stored
- `PROCESSORS`: the list of processors that perform complexity descriptors extraction
- `LEVELS`: set to either `text`, `sentence` or both

**WARNINGS**

The following processors are very slow and can sometimes cause the extraction process to crash because of temporary files incorrectly erased: 

- `phonetique`
- `adverbiaux_temporels` 

In that case, erase generated temporary files manually and relaunch extraction process.

### Run extraction process

```
cd src

python text_complexity_client.py
```

More information on diskstation:

`\\diskstation\Partage\RechercheDeveloppement\ProjetsCollaboratifs\TextToKids\avancement\4. livrables\WP3\TextToKids_Livrable_3_1.docx`