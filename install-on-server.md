# modifications à controler pour une installation sur le serveur d'un code tournant en local

## partie text_complexity_server

OK ?? ### dans wp3/text_complexity_server/src/processor/semantique/adverbiaux_temporels.config (attention deux fichiers .config)
  - unitex_linux_directory = /home/cp/Unitex-GramLab-3.3/App
  - #unitex_linux_directory = /home/parisse/Unitex-GramLab-3.3/App

### /brainstorm/text-to-kids/wp3/text_complexity_server/src/api/settings.py
FLASK_SERVER_NAME = 'localhost:8081'

## partie text_complexity_client

OK ?? ### ALLOWED HOST dans /brainstorm/text-to-kids/wp3/text_complexity_client/django/config/settings.py
  - ALLOWED_HOSTS = ['127.0.0.1']
  - #ALLOWED_HOSTS = ['vheborto-corliapi.inist.fr']

### dans /brainstorm/text-to-kids/wp3/text_complexity_client/django/texttokids/views.py
  - server_address = 'http://vheborto-corliapi.inist.fr:8081' # Warning - put vheborto for testing outside vheborto
  - #server_address = 'http://localhost:8081' # Warning - put localhost for distribution

### pour les taches automatique sur le client - fichier /brainstorm/text-to-kids/wp3/text_complexity_client/src/writeini.awk
  - print "ADDRESS = http://vheborto-corliapi.inist.fr:8081";
	- print "ADDRESS = http://localhost:8081";


## démarrage

?? # pour le mac penser à envelever age des processeurs
dans extraction_ws.py
#from processor.complexite import age

### en local
python run_prod.py
python manage.py runserver

### sur le serveur
python run_prod.py
python manage.py runserver 0.0.0.0:8984
