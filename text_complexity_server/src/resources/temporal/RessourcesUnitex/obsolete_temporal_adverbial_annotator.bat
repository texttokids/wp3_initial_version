mkdir "%4%1_snt" 

"%2\UnitexToolLogger.exe" Normalize "%3%1.txt" "-r%2\Norm.txt" "--output_offsets=%4%1_snt\normalize.out.offsets" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Grf2Fst2 "%2\Graphs\Preprocessing\Sentence\Sentence.grf" -y "--alphabet=%2\Alphabet.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Flatten "%2\Graphs\Preprocessing\Sentence\Sentence.fst2" --rtn -d5 -qutf8-no-bom

"%2\UnitexToolLogger.exe" Fst2Txt "-t%4%1.snt" "%2\Graphs\Preprocessing\Sentence\Sentence.fst2" "-a%2\Alphabet.txt" -M "--input_offsets=%4%1_snt\normalize.out.offsets" "--output_offsets=%4%1_snt\normalize.out.offsets" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Grf2Fst2 "%2\Graphs\Preprocessing\Replace\Replace.grf" -y "--alphabet=%2\Alphabet.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Fst2Txt "-t%4%1.snt" "%2\Graphs\Preprocessing\Replace\Replace.fst2" "-a%2\Alphabet.txt" -R "--input_offsets=%4%1_snt\normalize.out.offsets" "--output_offsets=%4%1_snt\normalize.out.offsets" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Tokenize "%4%1.snt" "-a%2\Alphabet.txt" "--input_offsets=%4%1_snt\normalize.out.offsets" "--output_offsets=%4%1_snt\tokenize.out.offsets" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Dico "-t%4%1.snt" "-a%2\Alphabet.txt" "%2\Dela\motsGramf-.bin" "%2\Dela\Dela_fr.bin" -qutf8-no-bom

"%2\UnitexToolLogger.exe" SortTxt "%4%1_snt\dlf" "-l%4%1_snt\dlf.n" "-o%2\Alphabet_sort.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" SortTxt "%4%1_snt\dlc" "-l%4%1_snt\dlc.n" "-o%2\Alphabet_sort.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" SortTxt "%4%1_snt\err" "-l%4%1_snt\err.n" "-o%2\Alphabet_sort.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" SortTxt "%4%1_snt\tags_err" "-l%4%1_snt\tags_err.n" "-o%2\Alphabet_sort.txt" -qutf8-no-bom



"%2\UnitexToolLogger.exe" Grf2Fst2 "%2\Graphs\Graphes_Charles_Menel\_Main_.grf" -y "--alphabet=%2\Alphabet.txt" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Locate "-t%4%1.snt" "%2\Graphs\Graphes_Charles_Menel\_Main_.fst2" "-a%2\Alphabet.txt" -L -R --all -b -Y --stack_max=1000 --max_matches_per_subgraph=200 --max_matches_at_token_pos=400 --max_errors=50 -qutf8-no-bom

"%2\UnitexToolLogger.exe" Concord "%4%1_snt\concord.ind" "-m%4%1.xml" -qutf8-no-bom

"%2\UnitexToolLogger.exe" Normalize "%4%1.xml" "-r%2\Norm.txt" -qutf8-no-bom

mkdir "%4%1_snt" 

"%2\UnitexToolLogger.exe" Tokenize "%4%1.xml" "-a%2\Alphabet.txt" -qutf8-no-bom