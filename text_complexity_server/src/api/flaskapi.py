import logging
import api.settings as settings

from flask_restx import Api

log = logging.getLogger(__name__)

api = Api(version='1.0',
          title='Text Analysis API',
          description='API for extracting features from text documents.')


@api.errorhandler
def default_error_handler():
    """
    This is a default errors handler, it simply displays a message when something went wrong.
    :return: str
    """
    message = 'An unhandled exception occurred. Contact an administrator.'
    log.exception(message)

    if not settings.FLASK_DEBUG:
        return {'message': message}, 500
