import stanza
import json
import os
import pandas as pd
import numpy as np
import re

from copy import deepcopy
from flask_restx import reqparse
from flask import request
from flask_restx import Resource
from api.flaskapi import api
from api.descriptors_extractor.dto.description_request import description_request

from processor.util import words_util
# from processor.plongements import camembert
from processor.graphie_typographie import graphie
from processor.lexique import a_tester_diversite_lexicale, niveau_lexical, richesse_lexicale, adjectifs_ordinaux
from processor.morphosyntaxe import flexions_verbales, parties_du_discours, pronoms, pluriels
from processor.phonetique_phonologie import phonetique
from processor.lisibilite import formules_lisibilite_standard
from processor.semantique import adverbiaux_temporels, emotions, connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores, modalite, emotyc
from processor.syntaxe import dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite
# import platform
# if platform.system() != 'Darwin':
from processor.complexite import age

import sys

sys.path.insert(1, "../../")

from tools.processor_export import processor_export_format


text_complexity_linguistic_description_namespace = api.namespace('text_complexity_linguistic_description', description="Extraction des descripteurs linguistiques "
                                                                           "susceptibles d'expliquer la complexité d'appréhension des textes par un jeune public")
text_complexity_linguistic_description_namespace.add_model('description_request', description_request)

basic_processors = 'tokenize,mwt,pos,lemma,depparse,words_util,'


@text_complexity_linguistic_description_namespace.route('/describe')
class TextComplexityLinguisticDescriptionWS(Resource):

    @api.expect(description_request)
    @api.doc(description="""Processeurs disponibles :                     
                                                - graphie/typographie :   graphie
                                                - phonétique/phonologie : phonetique
                                                - lexique :               niveau_lexical, richesse_lexicale, adjectifs_ordinaux 
                                                - morphosyntaxe :         flexions_verbales, parties_du_discours, pronoms, pluriels
                                                - syntaxe :               dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite
                                                - sémantique :            adverbiaux_temporels, emotions, connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores, modalite, emotyc
                                                - complexité :            age
                            Chaque processeur correspond à un ensemble de descripteurs.""")
    # available but not set in description : 
    #   - lexique: a_tester_diversite_lexicale
    #   - lisibilité: formules_lisibilite_standard     
    #   - plongements : camembert                    
    def post(self):
        body = request.json
        text = body['text']

        processors = ','.join(body['processors'])
        if 'entites_nommees' in body['processors']:
            processors = "ner," +processors    
        print(f"Extracting : {processors}")

        levels = ','.join(body['levels'])
        if(levels == None or levels == ""):
            levels = "text,sentence"
        print(f"Analysis levels : {levels}")
        
        
        from stanza.pipeline.core import DownloadMethod
        print(basic_processors + processors)
        analysis_pipeline = stanza.Pipeline('fr',
                                         processors=basic_processors + processors,
                                         use_gpu=False,
                                         pos_batch_size=3000, 
                                         analysis_level=levels,
                                         download_method=DownloadMethod.REUSE_RESOURCES)

        # read a text uploaded via swagger
        if len(text) > 0:
            text = text.replace("\\n","\n")
            
            doc = analysis_pipeline(text)
            
            dfs = processor_export_format.export(doc)

            json_res = {}
            for res in dfs:
                json_res[res] = json.loads(dfs[res].to_json(orient="records", force_ascii=False))
            return json_res
