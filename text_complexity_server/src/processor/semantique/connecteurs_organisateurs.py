from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from stanza.models.common.doc import Document
import os
import re
from pathlib import Path
import stanza

processor_name = 'connecteurs_organisateurs'

directory_organizers = "processor/semantique/organisateurs_lemmatises"
directory_connectors = "processor/semantique/connecteurs_lemmatises"
directories = [directory_organizers, directory_connectors]

def get_lemma(input):
    lemmatized_text = ""

    if isinstance(input, Document):
        for sentence in input.sentences:
            if len(sentence.words) > 1:
                for word in sentence.words:
                    lemmatized_text += word.lemma + " "
    else:
        if len(input.words) > 1:
            for word in input.words:
                lemmatized_text += word.lemma + " "
    return lemmatized_text.rstrip() 

def load_features_dictionary(directories):
    features_dictionary = {}
    for directory in directories:
        for filename in os.listdir(directory):
            feature = Path(filename).stem
            f = os.path.join(directory, filename)
            if os.path.isfile(f):
                with open(f, 'r', encoding="utf-8") as file:
                    for line in file:  
                        if feature in features_dictionary:
                            features_dictionary[feature].append(line.rstrip())
                        else:
                            features_dictionary[feature] = [line.rstrip()]
               
    return features_dictionary


features_dictionary = load_features_dictionary(directories)

def set_feature_list():
    f_list = []
    features = list(features_dictionary.keys())
    for feature in features:
        f_list.append("nombre_"+feature)
        f_list.append("proportion_"+feature)
    return f_list
    
features_list = set_feature_list()


def setFeatures(input):

    text = ""
    sentence_count = 0
    
    #lemmatize input
    if isinstance(input, Document):
        for sentence in input.sentences:
            text += get_lemma(sentence)
            sentence_count += 1
    else:
        text = get_lemma(input)
        sentence_count = 1
    
    extracted_features = []

    for feature in list(features_dictionary.keys()):
        extracted_feature = extract_feature(feature, text)
        extracted_features.append(extracted_feature)
        extracted_features.append((extracted_feature/sentence_count) if sentence_count > 0 else 0)

    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


def extract_feature(feature, text):
    feature_count = 0
    feature_values = features_dictionary[feature]

    for feature_value in feature_values: 
        feature_count += len(re.findall(" "+feature_value+" ", " "+text+" "))

    return feature_count


@register_processor(processor_name)
class LogicalConnectorOrganizersProcessor(Processor):
    """ Processor that computes logical connectors text organizers proportion at sentence and text level """
    _requires = {"tokenize,lemma"}                              
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)    

        return doc

