import sys

from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

from subprocess import *
import os
import shutil
import configparser
import re
import time

from collections import defaultdict

processor_name = 'adverbiaux_temporels'

# read config file containing annotator path
config = configparser.ConfigParser()
config.read('processor/semantique/adverbiaux_temporels.config') ##nom du fichier changé

# annotator path
unitex_directory = str(config['UNITEX']['unitex_directory'])
import pathlib
unitex_linux_directory = str(pathlib.Path.home()) + str(config['UNITEX']['unitex_tool_directory'])

raw_features_list = [
    "adverbiaux_temporels",
    "adverbiaux_localisation_temporelle",
    "adverbiaux_purement_duratifs",
    "adverbiaux_purement_iteratifs",
    "adverbiaux_duratifs_iteratifs",
    "adverbiaux_date_duree",
    "adverbiaux_pointage_absolu",
    "adverbiaux_pointage_non_absolu",
    "adverbiaux_loc_temp_focalisation_id",
    "adverbiaux_loc_temp_focalisation_non_id",
    "adverbiaux_loc_temp_deplacement_id",
    "adverbiaux_loc_temp_deplacement_non_id",
    "adverbiaux_loc_temp_regionalisation_id",
    "adverbiaux_loc_temp_regionalisation_non_id",
    "adverbiaux_loc_temp_pointage_absolu",
    "adverbiaux_loc_temp_pointage_non_absolu",
    "adverbiaux_iterateur_calendaire",
    "adverbiaux_iterateur_frequentiel",
    "adverbiaux_iterateur_quantificationnel",
    "adverbiaux_dur_iter_absolu",
    "adverbiaux_dur_iter_anaphorique",
    "adverbiaux_dur_iter_deictique",
    "adverbiaux_dur_iter_relatif"
] 
descripteurs_nombre = ["nombre_" + feature for feature in raw_features_list] # pour les descripteurs de type "nombre"
descripteurs_proportion = ["proportion_" + feature for feature in raw_features_list] # pour les descripteurs de type "proportion"
features_list = [feature for feature in descripteurs_nombre+descripteurs_proportion]

##########-----FONCTIONS DE GESTION DES ANNOTATIONS UNITEX-----##########
def find_annotations(annotated_text):
    """
    Retrouve les unités temporelles (UT) annotées par les graphes Unitex.
    Puis stocke les infos d'annotation dans un dico.
    Renvoie une liste avec tous les dico d'infos d'annotation. Dans cette liste, un adverbial = un item, par ordre d'apparition dans le texte.
    """
    # RegEx pour retrouver les annotations
    pattern_ut = '<UT.+?</UT>'
    pattern_annotation = '<Annotation Nom=\"([^\"]+)\">([^<]+)</Annotation>'
    pattern_temporal_location_adverbial = '<Chaine>([^<]+)</Chaine>'
    
    # Toutes les UT annotées
    temporal_location_adverbials = [x.group() for x in re.finditer(pattern_ut, annotated_text)]
    
    # Pour chaque UT, on récupère les infos d'annotation
    list_annotatedUT = list()
    for temporal_location_adverbial in temporal_location_adverbials:
        annotations = {x.group(1):x.group(2) for x in re.finditer(pattern_annotation, temporal_location_adverbial)}
        temporal_location_adverbials = [x.group(1) for x in re.finditer(pattern_temporal_location_adverbial, temporal_location_adverbial)]
        annotations["Texte"]=temporal_location_adverbials[0]
        list_annotatedUT.append(annotations)
    return list_annotatedUT

def annotate_text(doc, levels, unitex_directory, annotation_batch_script,
                        input_corpus_directory, output_corpus_directory,
                        unitex_linux_directory):
    """
    lance les graphes sur le texte, en créant des fichiers intermédiaires pour conserver le découpage en phrases de Stanza
    Renvoie : 
    - deux dico du type DICT{STR("text" ou un identifiant de phrase):LIST[les adverbiaux repérés, sous forme de dicos, du type
        {STR(nom d'annotation):STR(valeur)}]}
    """
    # Step 1 : create a temporary file with Stanza's sentence splitting of the text
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    filename = "sentence"+ timestamp
    input_filename = input_corpus_directory + filename
    output_filename = output_corpus_directory + filename + ".xml"
    #print("AAAAA: " + os.getcwd() + " ! " + input_filename + " ! " + output_filename + " ! " + annotation_batch_script + " ! " + filename + " ! " + unitex_directory + " ! " + input_corpus_directory + " ! " + output_corpus_directory + " ! " + unitex_linux_directory)

    sentence_splitted_text = ""
    for i in range(len(doc.sentences)):
        if i==0:
            sentence_splitted_text += doc.sentences[i].text
        else:
            sentence_splitted_text += "<SEP/>"+doc.sentences[i].text
        
    # write a temporary file containing the sentence to be annotated
    f = open(input_filename + ".txt", "a", encoding="utf-8")
    f.write(sentence_splitted_text)
    f.close()

    #print("BBBBB")
    #print(" ".join([annotation_batch_script, filename, unitex_directory, input_corpus_directory, output_corpus_directory, unitex_linux_directory]))
    #print("CCCCC")

    # Step 2 : execute the annotation script
    if os.name == 'nt':
        p = Popen(
            [annotation_batch_script, filename, unitex_directory, input_corpus_directory, output_corpus_directory],
            stdout=PIPE, stderr=PIPE, stdin=PIPE)
    else:
        p = Popen(
            [annotation_batch_script, filename, unitex_directory, input_corpus_directory, output_corpus_directory, unitex_linux_directory],
            stdout=PIPE, stderr=PIPE, stdin=PIPE)

    output, errors = p.communicate()
    p.wait()  # wait for process to terminate
    
    # Step 3 : get the annotations
    #read generated annotation output file
    dic_annotation_text = dict()

    dic_annotation_sentence = dict()
    if os.path.exists(output_filename):
        output_annotated_text = open(output_filename, 'r', encoding="utf-8").read()

        # remove temporary files
        os.remove(output_corpus_directory + filename + ".txt")
        os.remove(output_corpus_directory + filename + ".snt")
        shutil.rmtree(output_corpus_directory + filename + "_snt")
        os.remove(output_filename)
        
        # on veut les annotations à l'échelle du texte entier
        if "text" in levels:
            annotations = find_annotations(output_annotated_text)
            dic_annotation_text['text']=list(annotations)
        
        # on veut les annotations à l'échelle des phrases
        if "sentence" in levels: 
            #print("\n----------------------SENTENCE LEVEL----------------------")
            annotated_stanza_sentences = output_annotated_text.split("<SEP/>")
            #sentence level analysis
            if len(doc.sentences)>0 and len(doc.sentences) == len(annotated_stanza_sentences):
                for i in range(len(doc.sentences)):# in annotated_stanza_sentences:
                    annotations = find_annotations(annotated_stanza_sentences[i])
                    dic_annotation_sentence[str(i+1)+". Sentence: "+doc.sentences[i].text]=list(annotations)

    return dic_annotation_text, dic_annotation_sentence

def change_dic(dic_annotation):
    """
    change le dico pour faciliter la fusion des annotations
    Entrée :
    - DICT{STR("text" ou id de phrase):LIST[adverbiaux temporels du type DICT{STR(nom annotation):STR(valeur)}]}
    Sortie :
    - DICT{STR("text" ou id de phrase):DICT{STR(chaîne de l'adverbial):LIST[toutes les occurrences de l'adverbial, sous la forme
        de dico du type DICT{STR(nom annotation):STR(valeur)}]}}
    """
    dic_UT = defaultdict(lambda :defaultdict(lambda :list()))
    for key in dic_annotation:
        if dic_annotation[key]:
            for item in dic_annotation[key]:
                dic_UT[key][item["Texte"]].append(item)   
        else:
            dic_UT[key]=dict()
    return dic_UT

def merge(dic_UT_Charles,dic_UT_Menel):
    """
    Fusionne les infos d'annotations de deux dicos
    Entrée : 
    - 2 dicos de la forme DICT{STR("text" ou id de phrase):DICT{STR(chaîne de l'adverbial):LIST[toutes les occurrences de l'adverbial, sous la forme de dico du type DICT{STR(nom annotation):STR(valeur)}]}}
    Sortie :
    - DICT{STR("text" ou id de phrase):LIST[adverbiaux sous la forme de dico DICT{STR(nom d'annotation):STR(valeur)}]}
        
    """
    dic_final = defaultdict(lambda : list())
    for key in dic_UT_Charles: # pour chaque niveau
        for adverbial in dic_UT_Charles[key]: # pour chaque adverbial trouvé par les graphes de Charles
            if adverbial in dic_UT_Menel[key]: # si l'adverbial est aussi trouvé par les graphes de Menel
                # la liste des occurrences de l'adverbial
                # chaque occ = 1 dico d'annotations
                list_annotations_Charles = [dic_annotations for dic_annotations in dic_UT_Charles[key][adverbial]]
                list_annotations_Menel = [dic_annotations for dic_annotations in dic_UT_Menel[key][adverbial]]
                for couple in zip(list_annotations_Charles,list_annotations_Menel): # pour chaque occurrence
                    dic_annot = dict() # pour stocker toutes les annotations relatives à l'adverbial
                    for k in couple[0]: # pour toutes les annotations produites par les graphes de Charles
                        dic_annot[k]=couple[0][k]
                    for k in couple[1]: # pour toutes les annotations produites par les graphes de Menel
                        # N.B.: conflit pour l'annotation "Pointage" : en ajoutant en dernier les annotations de Menel
                        #on donne la priorité à la valeur de "Pointage" donnée par Menel
                        dic_annot[k]=couple[1][k]
                    dic_final[key].append(dic_annot)
            else:
                dic_final[key].extend(dic_UT_Charles[key][adverbial])   
    for key in dic_UT_Menel:
        for adverbial in dic_UT_Menel[key]:
            if adverbial in dic_UT_Charles[key]: # déjà traité (normalement)
                pass
            else:
                dic_final[key].extend(dic_UT_Menel[key][adverbial])
                
    for key in dic_UT_Charles:
        if key not in dic_final:
            dic_final[key].append(list())
    return dic_final
    
def merge_annotations(dic_annotation_text_Charles, dic_annotation_sentence_Charles,
                         dic_annotation_text_Menel, dic_annotation_sentence_Menel):
    """
    Fusionne les dicos avec les annotations produites par les graphes de Charles et ceux de Menel.
    Renvoie un dico à l'échelle du texte et un à l'échelle de la phrase.
    """
    
    # obtention des dicos de type {"niveau":{"adverbial":[{annotations}]}}
    dic_UT_text_Charles = change_dic(dic_annotation_text_Charles)
    dic_UT_text_Menel = change_dic(dic_annotation_text_Menel)
    dic_UT_sent_Charles = change_dic(dic_annotation_sentence_Charles)
    dic_UT_sent_Menel = change_dic(dic_annotation_sentence_Menel)
    
    dic_annotation_text = merge(dic_UT_text_Charles,dic_UT_text_Menel)
    dic_annotation_sentence = merge(dic_UT_sent_Charles,dic_UT_sent_Menel)

    return dic_annotation_text, dic_annotation_sentence
 
def get_annotations(doc, levels,
                    unitex_directory, annotation_batch_script_Charles, annotation_batch_script_Menel,
                    input_corpus_directory, output_corpus_directory,
                    unitex_linux_directory):
    """
    Cette fonction récupère les annotations produites par les graphes Unitex.
    Entrée : le texte traité par Stanza, les niveaux d'analyse, les variables pour Unitex
    Sortie : 
    - deux dico de type DICT{STR("text" ou un id de phrase):LIST[les adverbiaux repérés dans le texte entier ou dans la phrase,
        selon l'échelle ; chaque adverbial est représenté par un dico de type {STR(nom d'annotation, ex. Pointage):STR(valeur)}]}
    Cette fonction mobilise les fonctions suivantes :
        - annotate_text() pour lancer les graphes Unitex et récupérer les annotations, qui utilise elle-même :
            - find_annotations() pour trouver les annotations à l'aide de RegEx
        - merge_annotations() pour fusionner les annotations issues des graphes de Charles et de ceux de Menel, qui utilise elle-même :
            - change_dic() pour modifier les dicos contenant les infos d'annotation issus de annotate_text()
            - merge() pour fusionner les annotations
    """
    
    # on annote les textes avec les graphes et on récupère les infos
    dic_annotation_text_Charles, dic_annotation_sentence_Charles = annotate_text(doc, levels,
        unitex_directory, annotation_batch_script_Charles,
        input_corpus_directory, output_corpus_directory,
        unitex_linux_directory)
        
    dic_annotation_text_Menel, dic_annotation_sentence_Menel = annotate_text(doc, levels,
        unitex_directory, annotation_batch_script_Menel,
        input_corpus_directory, output_corpus_directory,
        unitex_linux_directory)
        
    dic_annotation_text, dic_annotation_sentence = merge_annotations(
                         dic_annotation_text_Charles, dic_annotation_sentence_Charles,
                         dic_annotation_text_Menel, dic_annotation_sentence_Menel)
                         
    return dic_annotation_text, dic_annotation_sentence
##########-----FIN des FONCTIONS DE GESTION DES ANNOTATIONS UNITEX-----##########

##########-----FONCTIONS POUR LE CALCUL DES DESCRIPTEURS (FEATURES)-----##########
def extract_raw_features(raw_features_dict, annotations):
    """
    /!\ Fonction modifiée pour pouvoir calculer nos nouveaux descripteurs /!\
    Cette fonction permet de fouiller les annotations.
    Elle transforme les annotations des graphes en infos utilisables pour obtenir les descripteurs :
        elle compte le nb de fois qu'une certaine valeur d'annotation est présente.
        
    N.B.: Pour le repérage des adverbiaux temporels, on doit distinguer 6 ensembles :
        - A : adverbiaux de localisation temporelle (graphes de Charles)
        - B : adverbiaux purement duratifs (graphes de Menel)
        - C : adverbiaux purement itératifs (graphes de Menel)
        - D : adverbiaux duratifs ou itératifs selon le contexte (graphes de Menel)
        - E : adverbiaux date ou durée selon le contexte (graphes de Menel)
        - F : adverbiaux temporels, A+B+C+D+E
    """
    
    for annotation_name in annotations:
        annotation_value = str(annotations[annotation_name])
        ##Nb d'adverbiaux temporels repérés = ensemble F
        if annotation_name == "Texte": ##tous les adverbiaux ont un texte, peut importe les graphes qui les ont reconnus
            raw_features_dict["adverbiaux_temporels"]+=1
            
        ##Distinction entre les ensembles A à E cités ci-dessus
        elif annotation_name == "Pointage":
            ##Adverbiaux de Menel, B à E
            if annotation_value == "PurementDuratif":
                raw_features_dict["adverbiaux_purement_duratifs"]+=1 ##B
            elif annotation_value == "PurementIteratif":
                raw_features_dict["adverbiaux_purement_iteratifs"]+=1 ##C
            elif annotation_value == "DuratifIteratif":
                raw_features_dict["adverbiaux_duratifs_iteratifs"]+=1 ##D
            elif annotation_value == "DateDuree":
                raw_features_dict["adverbiaux_date_duree"]+=1  ##E
            ##Adverbiaux de Charles, A
            else: ##ici, on est obligé de traiter du même coup la question du pointage absolu/non_absolu
                raw_features_dict["adverbiaux_localisation_temporelle"]+=1 ##A
                if annotation_value == "Absolu":
                    raw_features_dict["adverbiaux_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                    raw_features_dict["adverbiaux_loc_temp_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour A
                else:
                    raw_features_dict["adverbiaux_pointage_non_absolu"]+=1
                    raw_features_dict["adverbiaux_loc_temp_pointage_non_absolu"]+=1

        ##Type de pointage pour les adverbiaux de B à F
        elif annotation_name == "Pointage2":
            if annotation_value == "Absolu":
                raw_features_dict["adverbiaux_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                raw_features_dict["adverbiaux_dur_iter_absolu"]+=1 ##comptage des adverbiaux absolus pour B+C+D+E
            else:
                raw_features_dict["adverbiaux_pointage_non_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                if annotation_value == "Anaphorique":
                    raw_features_dict["adverbiaux_dur_iter_anaphorique"]+=1
                elif annotation_value == "Relatif":
                    raw_features_dict["adverbiaux_dur_iter_relatif"]+=1   
                elif annotation_value == "Deictique":
                    raw_features_dict["adverbiaux_dur_iter_deictique"]+=1 

        ##Infos pour les adverbiaux de A
        elif annotation_name == "Regionalisation":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_regionalisation_id"]+=1
            else:
                raw_features_dict["adverbiaux_loc_temp_regionalisation_non_id"]+=1
        elif annotation_name == "Focalisation":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_focalisation_id"]+=1
            else:                   
                raw_features_dict["adverbiaux_loc_temp_focalisation_non_id"]+=1                         
        elif annotation_name == "Déplacement":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_deplacement_id"]+=1
            else:
                raw_features_dict["adverbiaux_loc_temp_deplacement_non_id"]+=1

        ##Infos pour les adverbiaux C et D
        elif annotation_name == "Iterateur":
            if annotation_value == "Frequentiel":
                raw_features_dict["adverbiaux_iterateur_frequentiel"]+=1
            elif annotation_value == "Calendaire":
                raw_features_dict["adverbiaux_iterateur_calendaire"]+=1
            elif annotation_value == "Quantificationnel":
                raw_features_dict["adverbiaux_iterateur_quantificationnel"]+=1  

    return raw_features_dict     

def get_features_from_annotation_list(raw_features_dict,annotation_list):
    """
    """
    for adverbial_annotations in annotation_list:
        raw_features_dict = extract_raw_features(raw_features_dict, adverbial_annotations)
    return raw_features_dict

def compute_features(raw_features, num_words):
    """
    /!\ Fonction modifiée pour pouvoir calculer nos nouveaux descripteurs /!\
    à partir de nombres d'occurrences pour les raw_features, cette fonction permet de calculer les descripteurs
    """
    computed_features_dict = dict.fromkeys(features_list , 0)

    ##Pour les features de type nombre, il suffit de mettre les nombres d'occurrences
    for raw in raw_features: ##pour chaque feature du dico raw_features
        computed_features_dict["nombre_"+raw]=raw_features[raw] ##on récupère le nb d'occurrences
        
    ##Calcul des proportions
    A = raw_features["adverbiaux_localisation_temporelle"] ##adverbiaux de localisation temporelle (graphes de Charles)
    B = raw_features["adverbiaux_purement_duratifs"] ##adverbiaux purement duratifs (graphes de Menel)
    C = raw_features["adverbiaux_purement_iteratifs"] ##adverbiaux purement itératifs (graphes de Menel)
    D = raw_features["adverbiaux_duratifs_iteratifs"] ##adverbiaux duratifs ou itératifs selon le contexte (graphes de Menel)
    E = raw_features["adverbiaux_date_duree"] ##adverbiaux date ou durée selon le contexte (graphes de Menel)
    F = raw_features["adverbiaux_temporels"] ##adverbiaux temporels, A+B+C+D+E

    #### IL FAUT POUVOIR COMPLÉTER ICI AVEC LE NB DE PHRASES####
    computed_features_dict["proportion_adverbiaux_temporels"] = F ###/nb de phrases###

    computed_features_dict["proportion_adverbiaux_localisation_temporelle"] = A/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_purement_duratifs"] = B/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_purement_iteratifs"] = C/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_duratifs_iteratifs"] = D/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_date_duree"] = E/F if F != 0 else 0

    computed_features_dict["proportion_adverbiaux_pointage_absolu"] = raw_features["adverbiaux_pointage_absolu"]/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_pointage_non_absolu"] = raw_features["adverbiaux_pointage_non_absolu"]/F if F != 0 else 0

    computed_features_dict["proportion_adverbiaux_loc_temp_focalisation_id"] = raw_features["adverbiaux_loc_temp_focalisation_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_focalisation_non_id"] = raw_features["adverbiaux_loc_temp_focalisation_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_deplacement_id"] = raw_features["adverbiaux_loc_temp_deplacement_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_deplacement_non_id"] = raw_features["adverbiaux_loc_temp_deplacement_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_regionalisation_id"] = raw_features["adverbiaux_loc_temp_regionalisation_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_regionalisation_non_id"] = raw_features["adverbiaux_loc_temp_regionalisation_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_pointage_absolu"] = raw_features["adverbiaux_loc_temp_pointage_absolu"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_pointage_non_absolu"] = raw_features["adverbiaux_loc_temp_pointage_non_absolu"]/A if A != 0 else 0

    ##théoriquement, seuls les itératifs ont un itérateur renseigné
    ##on part donc du principe que si une annotation de type iterateur est indiquée
    ##l'adverbial est un adverbial iteratif
    computed_features_dict["proportion_adverbiaux_iterateur_calendaire"] = raw_features["adverbiaux_iterateur_calendaire"]/(C+D) if (C+D) != 0 else 0
    computed_features_dict["proportion_adverbiaux_iterateur_frequentiel"] = raw_features["adverbiaux_iterateur_frequentiel"]/(C+D) if (C+D) != 0 else 0
    computed_features_dict["proportion_adverbiaux_iterateur_quantificationnel"] = raw_features["adverbiaux_iterateur_quantificationnel"]/(C+D) if (C+D) != 0 else 0

    computed_features_dict["proportion_adverbiaux_dur_iter_absolu"] = raw_features["adverbiaux_dur_iter_absolu"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_anaphorique"] = raw_features["adverbiaux_dur_iter_anaphorique"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_deictique"] = raw_features["adverbiaux_dur_iter_deictique"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_relatif"] = raw_features["adverbiaux_dur_iter_relatif"]/(B+C+D+E) if (B+C+D+E) != 0 else 0

    return computed_features_dict

def generates_features(doc,levels,dic_annotation_text, dic_annotation_sentence):
    """
    """
    dico_text = dict()
    dico_sentence = dict()
    
    if "text" in levels:
        raw_features_dict = dict.fromkeys(raw_features_list , 0)
        raw_features_dict = get_features_from_annotation_list(raw_features_dict, dic_annotation_text['text'])
        #dico_text['text'] = dict(raw_features_dict)
        dico_text['text'] = compute_features(raw_features_dict,doc.num_words)
        
    if "sentence" in levels:
        for i in range(len(doc.sentences)):
            key = str(i+1)+". Sentence: "+doc.sentences[i].text
            raw_features_dict = dict.fromkeys(raw_features_list , 0)
            raw_features_dict = get_features_from_annotation_list(raw_features_dict, dic_annotation_sentence[key])
            #dico_sentence[key] = dict(raw_features_dict)
            dico_sentence[key] = compute_features(raw_features_dict, len(doc.sentences[i].words))
            
    return dico_text,dico_sentence
##########-----FIN des FONCTIONS POUR LE CALCUL DES DESCRIPTEURS (FEATURES)-----##########

def set_adverbial_features(doc,levels):
    """
    """
    # Step 1 : variables for Unitex
    if os.name == 'nt':
        #unitex_directory = ".\\RessourcesUnitex\\"
        annotation_batch_script_Charles = unitex_directory + "\\temporal_adverbial_annotator_Charles.bat"
        annotation_batch_script_Menel = unitex_directory + "\\temporal_adverbial_annotator_Menel.bat"
        input_corpus_directory = unitex_directory + "\\Corpus\\"
        output_corpus_directory = unitex_directory + "\\Corpus\\"
        #unitex_linux_directory= ""
    else:
        #unitex_directory = "./RessourcesUnitex/"
        annotation_batch_script_Charles = "resources/temporal/RessourcesUnitex/temporal_adverbial_annotator_Charles.sh"
        annotation_batch_script_Menel = "resources/temporal/RessourcesUnitex/temporal_adverbial_annotator_Menel.sh"
        input_corpus_directory = "resources/temporal/RessourcesUnitex/Corpus/"
        output_corpus_directory = "resources/temporal/RessourcesUnitex/Corpus/"
        #unitex_linux_directory = "/home/dp/Unitex-GramLab-3.3/App"
        
    # Step 2 : annotate texts
    dic_annotation_text, dic_annotation_sentence = get_annotations(doc, levels,
                    unitex_directory, annotation_batch_script_Charles, annotation_batch_script_Menel,
                    input_corpus_directory, output_corpus_directory,
                    unitex_linux_directory)
                    
    # Step 3 : get features
    dico_text, dico_sentence = generates_features(doc,levels,dic_annotation_text, dic_annotation_sentence)
    
    return dico_text, dico_sentence

@register_processor(processor_name)
class TemporalAdverbialProcessor(Processor):
    """ Processor that computes temporal location adverbial proportion at sentence and text level """
    _requires = {'tokenize'}
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        dico_text, dico_sentence=set_adverbial_features(doc,self.levels)
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            for f, s in dico_text['text'].items():
                setattr(doc, f, s)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for i in range(len(doc.sentences)):
                key = str(i+1)+". Sentence: "+doc.sentences[i].text
                for f, s in dico_sentence[key].items():
                    setattr(doc.sentences[i], f, s)

        return doc