import sys

from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

from subprocess import *
import os
import shutil
import configparser
import re
import time

processor_name = 'adverbiaux_temporels'

# read config file containing annotator path
config = configparser.ConfigParser()
config.read('processor/semantique/adverbiaux_temporels.config') ##nom du fichier changé

# annotator path
unitex_directory = str(config['UNITEX']['unitex_directory'])
import pathlib
unitex_linux_directory = str(pathlib.Path.home()) + str(config['UNITEX']['unitex_tool_directory'])

raw_features_list = [
    "adverbiaux_temporels",
    "adverbiaux_localisation_temporelle",
    "adverbiaux_purement_duratifs",
    "adverbiaux_purement_iteratifs",
    "adverbiaux_duratifs_iteratifs",
    "adverbiaux_date_duree",
    "adverbiaux_pointage_absolu",
    "adverbiaux_pointage_non_absolu",
    "adverbiaux_loc_temp_focalisation_id",
    "adverbiaux_loc_temp_focalisation_non_id",
    "adverbiaux_loc_temp_deplacement_id",
    "adverbiaux_loc_temp_deplacement_non_id",
    "adverbiaux_loc_temp_regionalisation_id",
    "adverbiaux_loc_temp_regionalisation_non_id",
    "adverbiaux_loc_temp_pointage_absolu",
    "adverbiaux_loc_temp_pointage_non_absolu",
    "adverbiaux_iterateur_calendaire",
    "adverbiaux_iterateur_frequentiel",
    "adverbiaux_iterateur_quantificationnel",
    "adverbiaux_dur_iter_absolu",
    "adverbiaux_dur_iter_anaphorique",
    "adverbiaux_dur_iter_deictique",
    "adverbiaux_dur_iter_relatif"
] ##changé d'après la liste des nouveaux des descripteurs qu'on veut obtenir est ici : https://docs.google.com/spreadsheets/d/1JXXbxBVnMFQF_Pw1splNDJq8WG1W5sNa/edit#gid=165568490

descripteurs_nombre = ["nombre_" + feature for feature in raw_features_list] ##nouveau : pour les descripteurs de type "nombre"
descripteurs_proportion = ["proportion_" + feature for feature in raw_features_list] ##nouveau : pour les descripteurs de type "proportion"
features_list = [feature for feature in descripteurs_nombre+descripteurs_proportion] ##changé pour tenir compte des deux types de descripteurs (nombre et proportion)


def set_temporal_location_adverbial_features(doc, levels):

    raw_features_dict = dict.fromkeys(raw_features_list , 0)
    features = dict.fromkeys(features_list , 0)
    
    # set annotation script variables
    if os.name == 'nt':
        annotation_batch_script = unitex_directory + "\\temporal_adverbial_annotator.bat" ##fichier et nom du fichier changés
        input_corpus_directory = unitex_directory + "\\Corpus\\"
        output_corpus_directory = unitex_directory + "\\Corpus\\"
    else:
        annotation_batch_script = "resources/temporal/RessourcesUnitex/temporal_adverbial_annotator.sh" ##fichier et nom du fichier changés
        input_corpus_directory = "resources/temporal/RessourcesUnitex/Corpus/"
        output_corpus_directory = "resources/temporal/RessourcesUnitex/Corpus/"

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    filename = "sentence"+ timestamp
    input_filename = input_corpus_directory + filename
    output_filename = output_corpus_directory + filename + ".xml"

    sentence_splitted_text = ""
    for i in range(len(doc.sentences)):
        if i==0:
            sentence_splitted_text += doc.sentences[i].text
        else:
            sentence_splitted_text += "<SEP/>"+doc.sentences[i].text
        

    # write a temporary file containing the sentence to be annotated
    f = open(input_filename + ".txt", "a", encoding="utf-8")
    f.write(sentence_splitted_text)
    f.close()

    # print(filename, unitex_directory, input_corpus_directory, output_corpus_directory)

    # execute the annotation script
    if os.name == 'nt':
        p = Popen(
            [annotation_batch_script, filename, unitex_directory, input_corpus_directory, output_corpus_directory],
            stdout=PIPE, stderr=PIPE, stdin=PIPE)
    else:
        p = Popen(
            [annotation_batch_script, filename, unitex_directory, input_corpus_directory, output_corpus_directory, unitex_linux_directory],
            stdout=PIPE, stderr=PIPE, stdin=PIPE)

    output, errors = p.communicate()
    p.wait()  # wait for process to terminate

    # read generated annotation output file
    if os.path.exists(output_filename):
        output_annotated_text = open(output_filename, 'r', encoding="utf-8").read()

        raw_features_dict = get_features_from_annotation(raw_features_dict, output_annotated_text)


        # remove temporary files
        os.remove(output_corpus_directory + filename + ".txt")
        os.remove(output_corpus_directory + filename + ".snt")
        shutil.rmtree(output_corpus_directory + filename + "_snt")
        os.remove(output_filename)
        
        features = compute_features(raw_features_dict,doc.num_words)
        
        if "text" in levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            for f, s in features.items():
                setattr(doc, f, s)  

        if "sentence" in levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)        
            annotated_stanza_sentences = output_annotated_text.split("<SEP/>")

            #sentence level analysis
            if len(doc.sentences)>0 and len(doc.sentences) == len(annotated_stanza_sentences):
                for i in range(len(doc.sentences)):# in annotated_stanza_sentences:
                    
                    #reset features dict
                    raw_features_dict = dict.fromkeys(raw_features_list , 0)
                    raw_features_dict = get_features_from_annotation(raw_features_dict, annotated_stanza_sentences[i])
                    
                    features = compute_features(raw_features_dict, len(doc.sentences[i].words))

                    for f, s in features.items():
                        setattr(doc.sentences[i], f, s) 



def get_features_from_annotation(raw_features_dict, annotated_text):
    # find all calendar expressions
    pattern_ut = '<UT.+?</UT>'
    pattern_annotation = '<Annotation Nom=\"([^\"]+)\">([^<]+)</Annotation>'
    pattern_temporal_location_adverbial = '<Chaine>([^<]+)</Chaine>'

    temporal_location_adverbials = [x.group() for x in re.finditer(pattern_ut, annotated_text)]

    for temporal_location_adverbial in temporal_location_adverbials:
        annotations = [[x.group(1), x.group(2)] for x in re.finditer(pattern_annotation, temporal_location_adverbial)]
        temporal_location_adverbials = [["Texte", x.group(1)] for x in re.finditer(pattern_temporal_location_adverbial, temporal_location_adverbial)]
        annotations.append(temporal_location_adverbials[0])

        raw_features_dict = extract_raw_features(raw_features_dict, annotations)

    return raw_features_dict


def extract_raw_features(raw_features_dict, annotations):
    """
    /!\ Fonction modifiée pour pouvoir calculer nos nouveaux descripteurs /!\
    Cette fonction permet de fouiller les annotations.
    Elle transforme les annotations des graphes en infos utilisables pour obtenir les descripteurs :
        elle compte le nb de fois qu'une certaine valeur d'annotation est présente.
        
    N.B.: Pour le repérage des adverbiaux temporels, on doit distinguer 6 ensembles :
        - A : adverbiaux de localisation temporelle (graphes de Charles)
        - B : adverbiaux purement duratifs (graphes de Menel)
        - C : adverbiaux purement itératifs (graphes de Menel)
        - D : adverbiaux duratifs ou itératifs selon le contexte (graphes de Menel)
        - E : adverbiaux date ou durée selon le contexte (graphes de Menel)
        - F : adverbiaux temporels, A+B+C+D+E
    """
    
    for annotation_name, annotation_value in annotations:
        ##Nb d'adverbiaux temporels repérés = ensemble F
        if annotation_name == "Texte": ##tous les adverbiaux ont un texte, peut importe les graphes qui les ont reconnus
            raw_features_dict["adverbiaux_temporels"]+=1
            
        ##Distinction entre les ensembles A à E cités ci-dessus
        elif annotation_name == "Pointage":
            ##Adverbiaux de Menel, B à E
            if annotation_value == "PurementDuratif":
                raw_features_dict["adverbiaux_purement_duratifs"]+=1 ##B
            elif annotation_value == "PurementIteratif":
                raw_features_dict["adverbiaux_purement_iteratifs"]+=1 ##C
            elif annotation_value == "DuratifIteratif":
                raw_features_dict["adverbiaux_duratifs_iteratifs"]+=1 ##D
            elif annotation_value == "DateDuree":
                raw_features_dict["adverbiaux_date_duree"]+=1  ##E
            ##Adverbiaux de Charles, A
            else: ##ici, on est obligé de traiter du même coup la question du pointage absolu/non_absolu
                raw_features_dict["adverbiaux_localisation_temporelle"]+=1 ##A
                if annotation_value == "Absolu":
                    raw_features_dict["adverbiaux_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                    raw_features_dict["adverbiaux_loc_temp_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour A
                else:
                    raw_features_dict["adverbiaux_pointage_non_absolu"]+=1
                    raw_features_dict["adverbiaux_loc_temp_pointage_non_absolu"]+=1

        ##Type de pointage pour les adverbiaux de B à F
        elif annotation_name == "Pointage2":
            if annotation_value == "Absolu":
                raw_features_dict["adverbiaux_pointage_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                raw_features_dict["adverbiaux_dur_iter_absolu"]+=1 ##comptage des adverbiaux absolus pour B+C+D+E
            else:
                raw_features_dict["adverbiaux_pointage_non_absolu"]+=1 ##comptage des adverbiaux absolus pour F
                if annotation_value == "Anaphorique":
                    raw_features_dict["adverbiaux_dur_iter_anaphorique"]+=1
                elif annotation_value == "Relatif":
                    raw_features_dict["adverbiaux_dur_iter_relatif"]+=1   
                elif annotation_value == "Deictique":
                    raw_features_dict["adverbiaux_dur_iter_deictique"]+=1 

        ##Infos pour les adverbiaux de A
        elif annotation_name == "Regionalisation":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_regionalisation_id"]+=1
            else:
                raw_features_dict["adverbiaux_loc_temp_regionalisation_non_id"]+=1
        elif annotation_name == "Focalisation":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_focalisation_id"]+=1
            else:                   
                raw_features_dict["adverbiaux_loc_temp_focalisation_non_id"]+=1                         
        elif annotation_name == "Déplacement":
            if annotation_value == "Id":
                raw_features_dict["adverbiaux_loc_temp_deplacement_id"]+=1
            else:
                raw_features_dict["adverbiaux_loc_temp_deplacement_non_id"]+=1

        ##Infos pour les adverbiaux C et D
        elif annotation_name == "Iterateur":
            if annotation_value == "Frequentiel":
                raw_features_dict["adverbiaux_iterateur_frequentiel"]+=1
            elif annotation_value == "Calendaire":
                raw_features_dict["adverbiaux_iterateur_calendaire"]+=1
            elif annotation_value == "Quantificationnel":
                raw_features_dict["adverbiaux_iterateur_quantificationnel"]+=1  

    return raw_features_dict     

def compute_features(raw_features, num_words):
    """
    /!\ Fonction modifiée pour pouvoir calculer nos nouveaux descripteurs /!\
    à partir de nombres d'occurrences pour les raw_features, cette fonction permet de calculer les descripteurs
    """
    computed_features_dict = dict.fromkeys(features_list , 0)

    ##Pour les features de type nombre, il suffit de mettre les nombres d'occurrences
    for raw in raw_features: ##pour chaque feature du dico raw_features
        computed_features_dict["nombre_"+raw]=raw_features[raw] ##on récupère le nb d'occurrences
        
    ##Calcul des proportions
    A = raw_features["adverbiaux_localisation_temporelle"] ##adverbiaux de localisation temporelle (graphes de Charles)
    B = raw_features["adverbiaux_purement_duratifs"] ##adverbiaux purement duratifs (graphes de Menel)
    C = raw_features["adverbiaux_purement_iteratifs"] ##adverbiaux purement itératifs (graphes de Menel)
    D = raw_features["adverbiaux_duratifs_iteratifs"] ##adverbiaux duratifs ou itératifs selon le contexte (graphes de Menel)
    E = raw_features["adverbiaux_date_duree"] ##adverbiaux date ou durée selon le contexte (graphes de Menel)
    F = raw_features["adverbiaux_temporels"] ##adverbiaux temporels, A+B+C+D+E

    #### IL FAUT POUVOIR COMPLÉTER ICI AVEC LE NB DE PHRASES####
    computed_features_dict["proportion_adverbiaux_temporels"] = F ###/nb de phrases###

    computed_features_dict["proportion_adverbiaux_localisation_temporelle"] = A/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_purement_duratifs"] = B/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_purement_iteratifs"] = C/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_duratifs_iteratifs"] = D/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_date_duree"] = E/F if F != 0 else 0

    computed_features_dict["proportion_adverbiaux_pointage_absolu"] = raw_features["adverbiaux_pointage_absolu"]/F if F != 0 else 0
    computed_features_dict["proportion_adverbiaux_pointage_non_absolu"] = raw_features["adverbiaux_pointage_non_absolu"]/F if F != 0 else 0

    computed_features_dict["proportion_adverbiaux_loc_temp_focalisation_id"] = raw_features["adverbiaux_loc_temp_focalisation_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_focalisation_non_id"] = raw_features["adverbiaux_loc_temp_focalisation_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_deplacement_id"] = raw_features["adverbiaux_loc_temp_deplacement_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_deplacement_non_id"] = raw_features["adverbiaux_loc_temp_deplacement_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_regionalisation_id"] = raw_features["adverbiaux_loc_temp_regionalisation_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_regionalisation_non_id"] = raw_features["adverbiaux_loc_temp_regionalisation_non_id"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_pointage_absolu"] = raw_features["adverbiaux_loc_temp_pointage_absolu"]/A if A != 0 else 0
    computed_features_dict["proportion_adverbiaux_loc_temp_pointage_non_absolu"] = raw_features["adverbiaux_loc_temp_pointage_non_absolu"]/A if A != 0 else 0

    ##théoriquement, seuls les itératifs ont un itérateur renseigné
    ##on part donc du principe que si une annotation de type iterateur est indiquée
    ##l'adverbial est un adverbial iteratif
    computed_features_dict["proportion_adverbiaux_iterateur_calendaire"] = raw_features["adverbiaux_iterateur_calendaire"]/(C+D) if (C+D) != 0 else 0
    computed_features_dict["proportion_adverbiaux_iterateur_frequentiel"] = raw_features["adverbiaux_iterateur_frequentiel"]/(C+D) if (C+D) != 0 else 0
    computed_features_dict["proportion_adverbiaux_iterateur_quantificationnel"] = raw_features["adverbiaux_iterateur_quantificationnel"]/(C+D) if (C+D) != 0 else 0

    computed_features_dict["proportion_adverbiaux_dur_iter_absolu"] = raw_features["adverbiaux_dur_iter_absolu"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_anaphorique"] = raw_features["adverbiaux_dur_iter_anaphorique"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_deictique"] = raw_features["adverbiaux_dur_iter_deictique"]/(B+C+D+E) if (B+C+D+E) != 0 else 0
    computed_features_dict["proportion_adverbiaux_dur_iter_relatif"] = raw_features["adverbiaux_dur_iter_relatif"]/(B+C+D+E) if (B+C+D+E) != 0 else 0

    return computed_features_dict


@register_processor(processor_name)
class TemporalAdverbialProcessor(Processor):
    """ Processor that computes temporal location adverbial proportion at sentence and text level """
    _requires = {'tokenize'}
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        set_temporal_location_adverbial_features(doc, self.levels)

        return doc
