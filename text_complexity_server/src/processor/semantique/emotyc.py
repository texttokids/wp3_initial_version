import os
import stanza
from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer
import torch
from collections import defaultdict

processor_name = 'emotyc'

#####----- VARIABLES pour le modèle -----#####
# Paramètres pour le modèle
nom_tokeniser = "camembert-base"
tokenizer = AutoTokenizer.from_pretrained(nom_tokeniser,do_lower_case=False, download_method = None)

labels = ['Emo', # binaire
    'Comportementale', 'Designee','Montree','Suggeree', # modes
    'Base', 'Complexe', # types
    'Admiration', 'Autre', 'Colere','Culpabilite', # categories
    'Degout', 'Embarras','Fierte', 'Jalousie',
    'Joie', 'Peur','Surprise', 'Tristesse']
nb_labels = len(labels)
rep_modele_local = "resources/modele_emotyc/maisonBinaire_multi_tout_entier_isole_pondere50_9_epochs_3_checkpoint-14676/"

# Chargement du modèle local
modele_local = AutoModelForSequenceClassification.from_pretrained(rep_modele_local,
                                                                  num_labels=nb_labels,
                                                                  problem_type = "multi_label_classification")
# Initialisation du Trainer
trainer_local = Trainer(
    model=modele_local
)

#####----- FONCTIONS pour la prédiction -----#####
def recupDocStanza(chemin_fichier):
    """
    """
    with open(chemin_fichier,'r', encoding="utf-8") as my_file:
        text = my_file.read()
        if len(text) > 0:
            text = text.replace("\\n","\n")
            doc = analysis_pipeline(text)
    return doc

def recupPhrasesStanza(doc_Stanza):
    """
    """
    liste_phrases = [doc_Stanza.sentences[i].text for i in range(len(doc_Stanza.sentences))]
    return liste_phrases

def transformeResultats(resultats_predictions,seuil=0.5):
    """
    """
    sigmoid = torch.nn.Sigmoid()
    probs = sigmoid(torch.Tensor(resultats_predictions[0]))
    predictions = [[1 if p>seuil else 0 for p in item] for item in probs]
    return predictions

#####----- VARIABLES pour les features -----#####
## /!\ L'ordre des descripteurs bruts doit être EXACTEMENT le même que celui des labels !!! /!\ cf. ligne 16
liste_descripteurs_bruts = [
    "avec_emo",
    "avec_emo_comportementale",
    "avec_emo_designee",
    "avec_emo_montree",
    "avec_emo_suggeree",
    "avec_emo_base",
    "avec_emo_complexe",
    "avec_cat_autre",
    "avec_cat_admiration",
    "avec_cat_colere",
    "avec_cat_culpabilite",
    "avec_cat_degout",
    "avec_cat_embarras",
    "avec_cat_fierte",
    "avec_cat_jalousie",
    "avec_cat_joie",
    "avec_cat_peur",
    "avec_cat_surprise",
    "avec_cat_tristesse"
]

descripteurs_nombre = ["nombre_" + desc for desc in liste_descripteurs_bruts] # pour les descripteurs de type "nombre"
descripteurs_proportion = ["proportion_" + desc for desc in liste_descripteurs_bruts] # pour les descripteurs de type "proportion"
features_list = [desc for desc in descripteurs_nombre+descripteurs_proportion]

#####----- FONCTIONS pour les features -----#####
def extraitDescBrutsPhrase(dico_descripteurs_bruts,predictions_phrase):
    """
    """
    for i,classe in enumerate(labels): # pour chaque classe à prédire
        if predictions_phrase[i]==1: # cette classe a été prédite comme présente pour la phrase
            # on augement de 1 le compte pour la feature correspondant à la classe prédite
            dico_descripteurs_bruts[liste_descripteurs_bruts[i]]+=1
    return dico_descripteurs_bruts

def extraitDescNumPhrase(dico_descripteurs_numeriques,dico_descripteurs_bruts):
    """
    """
    for desc_brut in dico_descripteurs_bruts:
        dico_descripteurs_numeriques['nombre_'+desc_brut]=dico_descripteurs_bruts[desc_brut]
        dico_descripteurs_numeriques['proportion_'+desc_brut]=dico_descripteurs_bruts[desc_brut]/1 # à l'échelle de la phrase, prop = nb
    return dico_descripteurs_numeriques
    
def extraitDescNumTexte(dico_descripteurs_bruts_phrase,dico_descripteurs_numeriques_texte,nb_phrases):
    """
    """
    for desc_brut in dico_descripteurs_bruts_phrase:
        dico_descripteurs_numeriques_texte['nombre_'+desc_brut]+=dico_descripteurs_bruts_phrase[desc_brut]
        dico_descripteurs_numeriques_texte['proportion_'+desc_brut]=dico_descripteurs_numeriques_texte['nombre_'+desc_brut]/nb_phrases
    return dico_descripteurs_numeriques_texte
    
def genereDescripteursNum(doc_Stanza,levels,chemin_sortie):
    """
    """
    # Étape 1 : récupération des phrases découpées par Stanza
    liste_phrases = recupPhrasesStanza(doc_Stanza)
    
    # Étape 2 : tokenisation avec camembert
    # tokenisation des phrases
    liste_phrases_tokenisees = [
        tokenizer(phrase,
                  padding="max_length", truncation=True, add_special_tokens=False)
        for phrase in liste_phrases
    ]
    
    # Étape 3 : prédictions des classes
    # prédiction
    resultats = trainer_local.predict(test_dataset=liste_phrases_tokenisees)
    # transformation des prédictions en label 0 ou 1
    predictions = transformeResultats(resultats)
    
    # Étape 4 : transformation des prédictions en features + écriture de la sortie
    with open(chemin_sortie,'w', encoding="utf8") as sortant:
        
        # Étape 4.1 : analyse au niveau de la phrase
        if "sentence" in levels:
            sortant.write("====== Sentence Level Features ======\n\n")
            for i,predictions_phrase in enumerate(predictions):
                cle_phrase = str(i+1) + ". Sentence: " + liste_phrases[i] + "\n"
                sortant.write(cle_phrase)
                
                dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
                dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,
                                                                        predictions_phrase)
                dico_descripteurs_numeriques_phrase = dict.fromkeys(features_list,0)
                dico_descripteurs_numeriques_phrase = extraitDescNumPhrase(dico_descripteurs_numeriques_phrase,dico_descripteurs_bruts_phrase)
                               
                #if 1 in dico_descripteurs_bruts_phrase.values(): # au moins une feature a été trouvée
                #    out.write("Features :: Feature-Values\n")
                #    out.write("--------------------------------------------\n")
                #    for desc in dico_descripteurs_numeriques_phrase: # pour chaque feature
                #        sortant.write(desc + "::" + str(dico_descripteurs_numeriques_phrase[desc]) + "\n")
                #else: # aucune feature dans la phrase
                #    chaines.append("No features found.\n")
                #    sortant.write("No features found.\n")
                sortant.write("Features :: Feature-Values\n")
                sortant.write("--------------------------------------------\n")
                for desc in dico_descripteurs_numeriques_phrase: # pour chaque feature
                    sortant.write(desc + "::" + str(dico_descripteurs_numeriques_phrase[desc]) + "\n")

                sortant.write("\n")                
        
        # Étape 4.2. : analyse au niveau du texte
        if "text" in levels:
            nb_phrases = len(liste_phrases)
            dico_descripteurs_numeriques_texte = dict.fromkeys(features_list,0)
            
            # les descripteur à l'échelle de la phrase
            for i,predictions_phrase in enumerate(predictions):
                dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
                dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,
                                                                        predictions_phrase)
                # on ajoute les descripteurs de la phrase courante à ceux du texte
                dico_descripteurs_numeriques_texte = extraitDescNumTexte(dico_descripteurs_bruts_phrase,
                                                                         dico_descripteurs_numeriques_texte,
                                                                         nb_phrases)
            
            sortant.write("\n====== Text Level Features ======\n\n")
            
            #if not 1 in dico_descripteurs_numeriques_texte.values(): # aucune feature
            #    sortant.write("No features found.\n")
            #else:
            #    sortant.write("Features :: Feature-Values\n")            
            #    sortant.write("--------------------------------------------\n")
            #    for desc in dico_descripteurs_numeriques_texte:
            #        sortant.write(desc+"::"+str(dico_descripteurs_numeriques_texte[desc])+"\n")
            sortant.write("Features :: Feature-Values\n")            
            sortant.write("--------------------------------------------\n")
            for desc in dico_descripteurs_numeriques_texte:
                sortant.write(desc+"::"+str(dico_descripteurs_numeriques_texte[desc])+"\n")
            sortant.write("\n")
        return liste_phrases
        
        
def genereDescripteursNumWP3(doc_Stanza,levels):
    """
    """
    dico_texte = dict()
    dico_phrase = []
    
    # Étape 1 : récupération des phrases découpées par Stanza
    liste_phrases = recupPhrasesStanza(doc_Stanza)
    
    # Étape 2 : tokenisation avec camembert
    # tokenisation des phrases
    liste_phrases_tokenisees = [
        tokenizer(phrase,
                  padding="max_length", truncation=True, add_special_tokens=False)
        for phrase in liste_phrases
    ]
    
    # Étape 3 : prédictions des classes
    # prédiction
    resultats = trainer_local.predict(test_dataset=liste_phrases_tokenisees)
    # transformation des prédictions en label 0 ou 1
    predictions = transformeResultats(resultats)
    
    # Étape 4 : transformation des prédictions en features

    # Étape 4.1 : analyse au niveau de la phrase
    if "sentence" in levels:
        for i,predictions_phrase in enumerate(predictions):
            # extraction des descripteurs bruts
            dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
            dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,predictions_phrase)
            # transformation en descripteurs numériques
            dico_descripteurs_numeriques_phrase = dict.fromkeys(features_list,0)
            dico_descripteurs_numeriques_phrase = extraitDescNumPhrase(dico_descripteurs_numeriques_phrase,dico_descripteurs_bruts_phrase)
            # stockage
            dico_phrase.append(dict(dico_descripteurs_numeriques_phrase))
        
    # Étape 4.2. : analyse au niveau du texte
    if "text" in levels:
        nb_phrases = len(liste_phrases)
        dico_descripteurs_numeriques_texte = dict.fromkeys(features_list,0)
        
        # les descripteur à l'échelle de la phrase
        for i,predictions_phrase in enumerate(predictions):
            dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
            dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,
                                                                    predictions_phrase)
            # on ajoute les descripteurs de la phrase courante à ceux du texte
            dico_descripteurs_numeriques_texte = extraitDescNumTexte(dico_descripteurs_bruts_phrase,
                                                                     dico_descripteurs_numeriques_texte,
                                                                     nb_phrases)
        dico_texte["texte"]=dict(dico_descripteurs_numeriques_texte)
    
    return dico_texte, dico_phrase
        
@register_processor(processor_name)
class Emotyc(Processor):
    """ Processor that computes temporal location adverbial proportion at sentence and text level """
    _requires = {'tokenize'}
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        dico_texte, dico_phrase = genereDescripteursNumWP3(doc,self.levels)
        if "text" in self.levels:
            # print("dico_texte")
            # print(dico_texte)
            # print(features_list)
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            for f, s in dico_texte['texte'].items():
                # print(str(f) + " " + str(s))
                setattr(doc, f, s)

        if "sentence" in self.levels:
            # print("dico_phrase")
            # print(dico_phrase)
            # print(features_list)
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for k in range(0, len(doc.sentences)):
                for f, s in dico_phrase[k].items():
                    setattr(doc.sentences[k], f, s)
            # for sentence in doc.sentences:
            #     print('AAA')
            #     print(sentence)

            #     setFeatures(sentence)    
            # for sentence, features in dico_phrase.items():
            #     print('AAA')
            #     print(sentence)
            #     print(features)
            #     # setattr(doc, "test", sentence)
            #     # for f in features:
            #     #     print('BBB')
            #     #     print(str(f) + " " + str(features[f]))
            #     #     setattr(doc, f, features[f])
            #     for f, s in features.items():
            #         print(str(f) + " " + str(s))
            #         setattr(doc, f, s)
                
            # for i in range(len(doc.sentences)):
            #     print("RANGE: " + str(i))
            #     print(doc.sentences[i])
            #     print("NEXT")
            #     for f, s in dico_phrase[doc.sentences[i]].items():
            #         setattr(doc.sentences[i], f, s)

        return doc