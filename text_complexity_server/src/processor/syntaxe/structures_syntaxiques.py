from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'structures_syntaxiques'

features_list = [
    'nombre_subordonnees_qui',
    'proportion_subordonnees_qui',
    'nombre_subordonnees_que',
    'proportion_subordonnees_que',
    'nombre_subordonnees_prep_lequel',
    'proportion_subordonnees_prep_lequel',
    'nombre_subordonnees_dont',
    'proportion_subordonnees_dont',
    'nombre_subordonnees_completives',
    'proportion_subordonnees_completives',
    'nombre_subordonnees_hypothetiques',
    'proportion_subordonnees_hypothetiques',
    'nombre_subordonnees_gerondif',
    'proportion_subordonnees_gerondif',
    'nombre_incises',
    'proportion_incises',
    'nombre_propositions_coordonnees',
    'proportion_propositions_coordonnees'     
    ]

def load_coordination_conjonctions():
    coordination_conjonction_list = []
    
    with open("processor/syntaxe/conjonctions_coordination.txt", 'r', encoding="utf-8") as in_stream:
        in_stream.readline()  
        for line in in_stream:  
            coordination_conjonction_list += line.strip()
               
    return coordination_conjonction_list

def setFeatures(input):
    words = input.words_list
    sentence_count = 0
    
    if isinstance(input, Document):
        sentence_count = len(input.sentences)
    else:
        sentence_count = 1

    who_clauses_count = 0
    what_clauses_count = 0
    which_to_which_count = 0
    of_which_count = 0
    complementary_clause_count = 0
    if_clause_count = 0
    present_participle_clause_count = 0
    parenthetical_clause_count = 0
    coordinated_clause_count = 0

    which_to_which_trigger = False
    rel_clause_trigger = False
    coordination_clause_trigger = False

    for word in words :
        # who, what, of which (relative) clauses
        # not at the beginning of a sentence/text
        if word.id != 1 and word.upos == "PRON" :
            if word.lemma == "qui" and word.deprel == "nsubj" and word.feats is not None and "PronType=Rel" in word.feats:
                who_clauses_count += 1
                rel_clause_trigger = True
            elif word.lemma == "que" and word.deprel == "obj" and word.feats is not None and "PronType=Rel" in word.feats:
                what_clauses_count += 1
                rel_clause_trigger = True
            elif word.lemma == "dont" and word.deprel == "iobj" and word.feats is not None and "PronType=Rel" in word.feats:
                of_which_count += 1
                rel_clause_trigger = True
        # to which clauses
        if word.upos == "ADP":
            which_to_which_trigger = True
        elif which_to_which_trigger: 
            if word.lemma == "lequel":
                which_to_which_count += 1
            else:
                which_to_which_trigger = False
        # complementary clauses
        if word.upos == "VERB" and (word.deprel == "ccomp" or word.deprel == "xcomp"):
            complementary_clause_count += 1
        # if clauses
        if word.lemma == "si" and word.upos == "SCONJ":
            if_clause_count += 1

        # present participle clauses (gérondif)
        if word.upos == "VERB" and word.feats is not None and word.feats == "Tense=Pres|VerbForm=Part" and word.deprel == "advcl":
            present_participle_clause_count += 1

        # cordination clauses
        if word.upos == "CCONJ":
            coordination_clause_trigger = True

            
        # parenthetical clauses
        if (word.upos == "VERB" or word.upos == "NOUN") and (word.deprel == "conj" or word.deprel == "parataxis") and not coordination_clause_trigger:
            parenthetical_clause_count += 1
        elif word.upos == "VERB" and word.deprel == "acl:relcl":
            if not rel_clause_trigger:
                parenthetical_clause_count += 1
            else:
                rel_clause_trigger = False

        if coordination_clause_trigger and word.upos == "VERB" :
            if word.deprel == "conj":
                coordinated_clause_count += 1
            coordination_clause_trigger = False        

    if (sentence_count == 0): sentence_count = 1
    extracted_features = [
        who_clauses_count, 
        (who_clauses_count/sentence_count),
        what_clauses_count, 
        (what_clauses_count/sentence_count),
        which_to_which_count, 
        (which_to_which_count/sentence_count),
        of_which_count, 
        (of_which_count/sentence_count),
        complementary_clause_count, 
        (complementary_clause_count/sentence_count),
        if_clause_count, 
        (if_clause_count/sentence_count),
        present_participle_clause_count, 
        (present_participle_clause_count/sentence_count),
        parenthetical_clause_count, 
        (parenthetical_clause_count/sentence_count),
        coordinated_clause_count, 
        (coordinated_clause_count/sentence_count)]    


        
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class ClausesProcessor(Processor):
    """ Processor that computes specific clauses features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                            

        return doc
