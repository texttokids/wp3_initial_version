from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'structures_passives'

features_list = [
    'nombre_structures_passives',
    'proportion_structures_passives_sur_verbes',]

def setFeatures(input):
    words = input.words_list

    passive_voice_count = 0
    verb_count = 0

    for word in words :
        if 'nsubj:pass' in word.deprel:
            passive_voice_count += 1
        if word.upos == 'VERB':
            verb_count += 1

    if verb_count > 0:
        extracted_features = [
            passive_voice_count, 
            (passive_voice_count/verb_count)]    
    else:
        extracted_features = [0,0]                  
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class PassiveVoiceProcessor(Processor):
    """ Processor that computes passive voice features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                            

        return doc
