import os
import csv
from subprocess import *
from datetime import datetime


def FreqPhoneme(Phoneme):
    switcher = {
        'a': 8.1, 'r': 6.9, 'l': 6.8, 'e': 6.5, 's': 5.8, 'i': 5.6, 'ə': 4.9, 't': 4.5, 'k': 4.5, 'p': 4.49, 'd': 3.5,
        'm': 3.4, 'ɑ̃': 2.62,
        'n': 2.8, 'u': 2.7, 'v': 2.4, 'o': 2.2, 'y': 2.7, 'ɔ̃': 2.0, 'ø': 1.7, 'ɔ': 1.55, 'ɛ̃': 1.4, 'f': 1.3, 'j': 1.0,
        'w': 0.9, 'z': 0.6,
        'ʃ': 0.5, 'œ̃': 0.5, 'œ': 0.3, 'g': 0.3, 'aː': 0.2, 'ɲ': 0.1
    }
    # y est confondu avec ɥ (source : Bernadette GROMER et Marlise WEISS : Lire, tome 1 :
    # apprendre à lire - Armand Colin - 1990)
    return switcher.get(Phoneme, 0)  # Pour les phonèmes non précisés on admet que la valeur est de 0


def CasMotUneLettre(symblist):
    aAccentGraveList = ['a', 'a', 'k', 's', 'ɑ̃', 'ɡ', 'ʁ', 'a', 'v']
    for i in range(len(symblist) - len(aAccentGraveList) + 1):
        if symblist[i:len(aAccentGraveList)] == aAccentGraveList:
            symblist = symblist[:i] + ['a'] + symblist[len(aAccentGraveList):]
    YList = ['i', 'ɡ', 'ʁ', 'ɛ', 'k']
    for i in range(len(symblist) - len(YList) + 1):
        if symblist[i:len(YList)] == YList:
            symblist = symblist[:i] + ['i'] + symblist[len(aAccentGraveList):]
    return symblist


def Phonetique(Ecrit, isMotUneLettre):
    
    #Ecrit = '"' + Ecrit + '"'
    #cmd = f"../../inria_processors/extractors/eSpeak_runner.ps1 {Ecrit}"  # "./eSpeak_runner.ps1 " + Ecrit
    #cmd = "../../inria_processors/extractors/eSpeak_runner.ps1 " + Ecrit
    #proc = Popen(["powershell.exe", cmd], stdout=PIPE, shell=True)

    now = datetime.now()  # current date and time
    date_time = now.strftime("%m-%d-%Y-%H-%M-%S")

    if os.name == 'nt':
        batch_script = "inria_processors\\extractors\\eSpeak_runner.bat"
        output_filename = "inria_processors\\extractors\\output" + date_time + ".txt"
    else:
        batch_script = "inria_processors/extractors/eSpeak_runner.sh"
        output_filename = "inria_processors/extractors/output" + date_time + ".txt"
    

    proc = Popen([batch_script, Ecrit, output_filename],
              stdout=PIPE, stderr=PIPE, stdin=PIPE)
    output, errors = proc.communicate()
    proc.wait()
    
    prononciation_Ecrit = ""
    
    try:
        if os.path.exists(output_filename):
            prononciation_Ecrit = open(output_filename, 'r', encoding="utf-8").read()
            os.remove(output_filename)
    except Exception as e:
        print("ERROR in removing file: " + output_filename)

    symblist = []
    for i in range(len(prononciation_Ecrit)):
        currentSymb = prononciation_Ecrit[i]
        if currentSymb not in {"ˈ", ",", 'ˌ', "-", '_', ' ', '\n'}:
            if currentSymb == '̃' or currentSymb == 'ː':
                symblist = symblist[:-1] + [''.join([symblist[-1], currentSymb])]
            else:
                symblist += currentSymb

    if isMotUneLettre:
        symblist = CasMotUneLettre(symblist)
        # os.chdir('../../DATA/CSVfiles')
    return symblist


def PhonetiqueFeatures(Ecrit):
    ListOfPhonemes = Phonetique(Ecrit, len(Ecrit) == 1)
    phonetic_diversity = []
    phonetic_frequency = 0
    for i in range(len(ListOfPhonemes)):
        currentPhoneme = ListOfPhonemes[i]
        if currentPhoneme not in phonetic_diversity:
            phonetic_diversity += [currentPhoneme]
        phonetic_frequency += FreqPhoneme(currentPhoneme)
    if ListOfPhonemes == []:
        phonetic_diversity = 0
        phonetic_frequency = 0
    else:
        phonetic_diversity = len(phonetic_diversity) / len(ListOfPhonemes)
        phonetic_frequency = phonetic_frequency / len(ListOfPhonemes)
    return len(ListOfPhonemes), phonetic_diversity, phonetic_frequency


def GlobalisationPhonetique(phrase):
    nb_mots = 0
    Freq_phrase = []
    GlobFreq = []
    phrase_diversity = []
    GlobDiv = []
    Taille_phrase = []
    GlobTaille = []
    PhraseEcrit = ""
    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            nb_mots += 1
            mot = phrase[i].get('lemma', phrase[i].get('text'))  # form
            PhonetisationMot = PhonetiqueFeatures(mot)
            Taille_phrase += [PhonetisationMot[0]]
            phrase_diversity += [PhonetisationMot[1]]
            Freq_phrase += [PhonetisationMot[2]]
            PhraseEcrit = PhraseEcrit + " %s " % (mot)
        # break
    PhonetisationPhrase = PhonetiqueFeatures(PhraseEcrit)
    if not Freq_phrase:  # Globalisation des variables locales de fréquence, par le calcul de la moyenne et la variance
        GlobFreq = [0, 0, 0]
    else:
        n = nb_mots
        MoyFreq = sum(Freq_phrase) / n
        VarFreq = sum([(f ** 2 - MoyFreq ** 2) / n for f in Freq_phrase])
        FreqPhrase = PhonetisationPhrase[2]
        GlobFreq = [
            FreqPhrase,
            MoyFreq,
            VarFreq]

    if not phrase_diversity:
        GlobDiv = [
            #0, #DivPhrase
            0, #MoyDiv
            0 #VarDiv  
            ]
    else:
        n = nb_mots
        MoyDiv = sum(phrase_diversity) / n
        VarDiv = sum([(f ** 2 - MoyDiv ** 2) / n for f in phrase_diversity])
        DivPhrase = PhonetisationPhrase[1]
        GlobDiv = [
            #DivPhrase, 
            MoyDiv, 
            VarDiv]

    if not Taille_phrase:
        GlobTaille = [
            # 0, #TaillePhrase 
            0, #MoyTaille
            0 #VarTaille
            ]
    else:
        n = nb_mots
        MoyTaille = sum(Taille_phrase) / n
        VarTaille = sum([(t ** 2 - MoyTaille ** 2) / n for t in Taille_phrase])
        TaillePhrase = PhonetisationPhrase[0]
        GlobTaille = [
            # TaillePhrase,
            MoyTaille,
            VarTaille]

    return GlobFreq + GlobDiv + GlobTaille


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ["FréquencePhonèmesPhrase", 'FrequencePhonemesMoyenne', 'FrequencePhonemesVariance',
                           'DiversitéPhonemesPhrase', 'DiversitéPhonemesMoyenne', 'DiversitéPhonemesVariance',
                           'nbPhonemePhrase', 'nbPhonemeMoyen', 'nbPhonemeVariance']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [GlobalisationPhonetique(phrase)]
    if 'PhonetiqueFeatures.csv' not in os.listdir(os.getcwd()):
        with open('PhonetiqueFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('PhonetiqueFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return 'PhonetiqueFeatures.csv'


