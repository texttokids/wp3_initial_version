import os
import csv
import pandas


def FreqOccurenceMot(Mot):
    Lexique = pandas.read_csv(os.path.abspath('inria_processors/tools/Lexique')+'/logp-mots-fr.tsv', sep='\t',
                              header=None).values  # Source : Etienne Brunet ; eduscol
    Mots = list(Lexique[:, 0])
    # Freq = list(Lexique[:,2])
    Freq = list(Lexique[:, 1])
    if Mot in Mots:
        return Freq[Mots.index(Mot)]
    else:
        return -6


def GlobalisationFrequenceOccurence(phrase):
    nb_mots = 0
    Freq_phrase = []
    GlobFreq = []
    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            nb_mots += 1
            mot = phrase[i].get('lemma')#phrase[i].get('form')  # lemma
            FrequenceMot = FreqOccurenceMot(mot)
            Freq_phrase += [FrequenceMot]
    if Freq_phrase == []:  # Globalisation des variables locales de fréquence, par le calcul de la moyenne et la variance
        GlobFreq = [0, 0]
    else:
        n = nb_mots
        MoyFreq = sum(Freq_phrase) / n
        VarFreq = sum([(f ** 2 - MoyFreq ** 2) / n for f in Freq_phrase])
        GlobFreq = [MoyFreq, VarFreq]

    return GlobFreq


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ["MoyFreqOccurence", "VarFreqOccurence"]
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [GlobalisationFrequenceOccurence(phrase)]
    if 'FrequenceOccurenceFeatures.csv' not in os.listdir(os.getcwd()):
        with open('FrequenceOccurenceFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('FrequenceOccurenceFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return 'FrequenceOccurenceFeatures.csv'

