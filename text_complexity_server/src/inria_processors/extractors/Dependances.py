
import os
import csv



def DistDependance(mot):
    if mot.get("upos") != 'PUNCT' and 'head' in mot:  # upos
        mot_head = mot.get("head")
        mot_indice = mot.get("id")
        Distdep = abs(mot_head - mot_indice)
    else:
        Distdep = -1
    return Distdep


def dependanceDistMax(phrase):
    if (len(phrase) < 1): return 0
    # ListOfMot = []
    ListOfDep = []
    for i in range(len(phrase)):
        mot = phrase[i]
        ListOfDep += [DistDependance(mot)]
    return max(ListOfDep)


def dependanceDistMoy(phrase):
    if (len(phrase) < 1): return 0
    # ListOfMot = []
    ListOfDep = []
    for i in range(len(phrase)):
        mot = phrase[i]
        ListOfDep += [DistDependance(mot)]
    return sum(ListOfDep) / len(ListOfDep)


def Dependants(mot, phrase):
    Indice = mot.get('id')
    Deps = []
    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            MotCourant = phrase[i]
            MotCourantHead = MotCourant.get('head')
            if MotCourantHead == Indice:
                Deps += [MotCourant]
    return Deps


def DistDesPointeurs(mot, phrase):
    """
    retourne, pour un mot donné, la distance à laquelle il est des mots qui pointent vers lui en moyenne
    """
    DistancesDespointeurs = 0
    pointeurs = Dependants(mot, phrase)
    indice = mot.get('id')
    for i in range(len(pointeurs)):
        pointeur = pointeurs[i]
        pointeur_id = pointeur.get('id')
        dist = abs(indice - pointeur_id)
        DistancesDespointeurs += dist
    if pointeurs == []:
        return 0
    else:
        return DistancesDespointeurs / len(pointeurs)


def NombreDependantsPhrase(phrase):
    """
    Retourne pour chaque mot, le nombre de mot qui y font référence si ils en ont
    """
    Dependances = []
    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            mot = phrase[i]
            Dependances += [len(Dependants(mot, phrase))]
    return Dependances


def ProfondeurArbreDep(phrase):
    # print("phrase: ", phrase)
    Profondeur = 1
    ProfondeurCourante = 0
    for i in range(len(phrase)):
        mot = phrase[i]
        new_mot = None
        if 'head' in mot:
            head = mot.get('head')
            ProfondeurCourante = 0
            while head != 0:
                aux_mot = new_mot
                new_mot = phrase[head - 1]
                # print(f"mot= {mot}, new_mot= {new_mot}")

                if type(mot.get('id')) == tuple or type(new_mot.get('id')) == tuple:
                    head = 0
                    ProfondeurCourante = 1
                    continue
                if mot.get('id') == new_mot.get('head') and new_mot.get('id') == (mot.get('head')-1):
                    head = 0
                    ProfondeurCourante = 1
                    continue
                if mot.get('id') == (new_mot.get('id')-1) and mot.get('head') == (new_mot.get('head')+1):
                    head = 0
                    ProfondeurCourante = 1
                    continue
                if (mot == new_mot) or (aux_mot == mot) or ('head' not in mot):
                    ProfondeurCourante = 1
                    head = 0
                else:
                    mot = new_mot
                    head = mot.get('head')
                    if head is None:
                        head = 0
                    ProfondeurCourante += 1
        Profondeur = max(Profondeur, ProfondeurCourante)
    return Profondeur


def DependanceGlobalisation(phrase):
    Distances = []
    GlobDist = []

    NbPointeurs = []
    GlobNbPointeurs = []

    for i in range(len(phrase)):
        n = len(phrase)

        Distances += [DistDesPointeurs(phrase[i], phrase)]
        MoyDist = sum(Distances) / n
        VarDist = sum([(d ** 2 - MoyDist ** 2) / n for d in Distances])
        GlobDist = [MoyDist, VarDist]

        NbPointeurs += [len(Dependants(phrase[i], phrase))]
        MoyNbP = sum(NbPointeurs) / n
        VarNbP = sum([(d ** 2 - MoyNbP ** 2) / n for d in NbPointeurs])
        GlobNbPointeurs = [MoyNbP, VarNbP]

    return GlobDist + GlobNbPointeurs


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ['ProfArbDep', 'depDistMax', 'depDistMoy', 'PointDistMoy', 'PointDistVar',
                           'NombreMoyenDedependances', 'VarianceNombreDependances']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [[ProfondeurArbreDep(phrase), dependanceDistMax(phrase),
                            dependanceDistMoy(phrase)] + DependanceGlobalisation(phrase)]
    if 'DependancesFeatures.csv' not in os.listdir(os.getcwd()):
        with open('DependancesFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('DependancesFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return 'DependancesFeatures.csv'


def CsvMakerMots(ListOfPhrases):
    ListfeaturesMot = ['DistanceMotPointé', 'DistanceMoyMotsPointeurs', 'NombreMotsPointeurs']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        for j in range(len(phrase)):
            if phrase[j].get('upos') != 'PUNCT':
                mot = phrase[j]
                ListfeaturesEX += [[DistDependance(mot), DistDesPointeurs(mot, phrase), len(Dependants(mot, phrase))]]
    if 'DependancesFeaturesLocals.csv' not in os.listdir(os.getcwd()):
        with open('DependancesFeaturesLocals.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesMot)
    with open('DependancesFeaturesLocals.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return "DependancesFeaturesLocals.csv"

