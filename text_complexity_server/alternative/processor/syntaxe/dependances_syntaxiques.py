from inria_processors.extractors.Dependances import *
from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from stanza.models.common.doc import Document

processor_name = 'dependances_syntaxiques'

features_list = [
    'profondeur_arbre_syntaxique', 
    'distance_max_dependances', 
    'distance_moyenne_dependances', 
    'point_distance_moyenne', 
    'point_distance_variance',
    'nombre_moyen_dependances', 
    'variance_nombre_dependances']

def setFeatures(input):

    #conll format conversion
    if isinstance(input, Document):
        conll_text = input.to_dict()
    else:
        conll_text = [input.to_dict()]

    
    #flatten list of sentences if input is doc
    if isinstance(conll_text, list):
        conll_text = [item for sublist in conll_text for item in sublist]

    extracted_features = [ProfondeurArbreDep(conll_text), dependanceDistMax(conll_text),
                               dependanceDistMoy(conll_text)] + DependanceGlobalisation(conll_text)
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class SyntacticDependenciesProcessor(Processor):
    """ Processor that computes syntactic dependencies metric at sentence and text levels """
    
    _provides = {processor_name}
    
    levels = []

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                           

        return doc
