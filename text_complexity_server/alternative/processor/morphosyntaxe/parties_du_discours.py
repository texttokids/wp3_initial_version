from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'parties_du_discours'

upos_list = [
    'ADJ',
    'ADP',
    'ADV',
    'AUX',
    'CCONJ',
    'DET',
    'INTJ',
    'NOUN',
    'NUM',
    'PART',
    'PRON',
    'PROPN',
    'PUNCT',
    'SCONJ',
    'SYM',
    'VERB',
    'X'    
]

features_list = ["proportion_" + feature for feature in upos_list]



def setFeatures(input):
    
    #conll format conversion
    if isinstance(input, Document):
        conll_text = input.to_dict()
    else:
        conll_text = [input.to_dict()]

    #flatten list of sentences if input is doc
    if isinstance(conll_text, list):
        conll_text = [item for sublist in conll_text for item in sublist]
    
    features_upos = extract_upos(input)
            
    features = zip(features_list, 
                    features_upos)
    
    for f, s in features:
        setattr(input, f, s)



def extract_upos(input):
    """
    Computes the number of different POS tags in the input text sequence.
    :param input text sequence
    :return: dict
    """
    words_pos = []
    upos_count_dict = dict.fromkeys(upos_list , 0)
    upos_features_dict = dict.fromkeys(upos_list , 0)
    
    if isinstance(input, Document):
        for sentence in input.sentences:
            words_pos += [word.upos for word in sentence.words]
    else:
        words_pos = [word.upos for word in input.words]
    
    for word_pos in words_pos:
        if word_pos in upos_list:
            upos_count_dict[word_pos] += 1

    for upos in upos_list:
        if input.words_count>0:
            upos_features_dict[upos] = upos_count_dict[upos] / input.words_count 
    return list(upos_features_dict.values())



@register_processor(processor_name)
class POSProportionProcessor(Processor):
    """ Processor that computes part of speech proportion at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)    

        return doc
