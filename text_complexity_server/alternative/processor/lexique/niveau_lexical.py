from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'niveau_lexical'

def get_features_list():
    features = ["proportion_niveau_" + str(x+1) for x in range(11)]
    features.append("niveau_scolaire_moyen")
    return features

features_list = get_features_list()

def get_scolar_level_from_dubois_level(level):
    if level <= 7: # scolar level 1 => from 1 to 7 on Dubois Buyse scale, ie approx. CP, age 6 to 7
       return 1
    elif level <= 11: # scolar level 2 => from 8 to 11 on Dubois Buyse scale, ie approx. CE1, age 7 to 8
        return 2
    elif level <= 15: # scolar level 3 => from 12 to 15 on Dubois Buyse scale, ie approx. CE2, age 8 to 9
        return 3
    elif level <= 19: # etc.
        return 4
    elif level <= 23:
        return 5
    elif level <= 27:
        return 6
    elif level <= 31:
        return 7
    elif level <= 35:
        return 8
    elif level <= 39:
        return 9
    elif level <= 42: # scolar level 10 => from 40 to 42 on Dubois Buyse scale, ie approx. 2nde, age 15 to 16
        return 10
    return 11

def load_dubois_lexicon():
    dubois_dict = dict()

    with open("processor/lexique/duboisbuyse.txt", 'r', encoding="utf-8") as in_stream:
        in_stream.readline()  
        for line in in_stream:  
            elements = line.strip().split("|")
            if elements[0] != '' and elements[1] != '': 
                lemma = elements[0]
                level = int(elements[1])  
                dubois_dict[lemma] = get_scolar_level_from_dubois_level(level)
               
    return dubois_dict



def setFeatures(input):

    dubois_dict = load_dubois_lexicon()
    
    
    features_dict = dict.fromkeys(features_list , 0)
    occurences_at_level_dict = dict.fromkeys(["nombre_niveau_" + str(x+1) for x in range(11)], 0)
    

    #count lemma at each level
    for word in input.words_list :
        if word.lemma in dubois_dict:
            level = dubois_dict[word.lemma]
            occurences_at_level_dict["nombre_niveau_"+str(level)] += 1
            
        else:
            occurences_at_level_dict["nombre_niveau_11"] += 1

    average_level = 0
    #count proportion at each level
    for x in range(11):
        occurences_at_level = occurences_at_level_dict["nombre_niveau_"+str(x+1)]
        proportion = 0
        if input.words_count>0:
            proportion = occurences_at_level/input.words_count
        features_dict["proportion_niveau_"+str(x+1)] = proportion
        average_level += (x+1)*proportion
    
    features_dict["niveau_scolaire_moyen"] = average_level
    extracted_features =list(features_dict.values())


    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class LexicalLevelProcessor(Processor):
    """ Processor that lexical level features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        
        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                           

        return doc
