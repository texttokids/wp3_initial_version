from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from inria_processors.extractors import Phonetique
from tools.processor_export import processor_export_format

processor_name = 'phonetique'

features_list = [       'frequence_phonemes_phrase',
                       'frequence_phonemes_moyenne',
                       'frequence_phonemes_variance',
                       # 'DiversitéPhonemesPhrase',
                       'diversite_phonemes_moyenne',
                       'diversite_phonemes_variance',
                       # 'nbPhonemePhrase',
                       'nombre_phonemes_moyen',
                       'nombre_phonemes_variance']


def setFeatures(input):

    #conll format conversion
    if isinstance(input, Document):
        conll_text = input.to_dict()
    else:
        conll_text = [input.to_dict()]

    #flatten list of sentences if input is doc
    if isinstance(conll_text, list):
        conll_text = [item for sublist in conll_text for item in sublist]

    features = zip(features_list, Phonetique.GlobalisationPhonetique(conll_text))
    for f, s in features:
        setattr(input, f, s)


@register_processor("phonetique")
class PhoneticProcessor(Processor):
    """ Processor that computes phonetic features at sentence and text levels """
    _provides = {'phonetique'}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)  

        return doc
