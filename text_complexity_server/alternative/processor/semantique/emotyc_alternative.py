import os
import stanza
from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from transformers import AutoTokenizer, AutoModelForSequenceClassification, Trainer
import torch
from collections import defaultdict

processor_name = "emotyc"

#####----- VARIABLES pour le modèle -----#####
# Paramètres pour le modèle
nom_tokeniser = "camembert-base"
tokenizer = AutoTokenizer.from_pretrained(nom_tokeniser,do_lower_case=False)

labels = ['Emo', # binaire
          'Comportementale', 'Designee','Montree','Suggeree', # modes
          'Base', 'Complexe', # types
          'Admiration', 'Autre', 'Colere','Culpabilite', # categories
             'Degout', 'Embarras','Fierte', 'Jalousie',
             'Joie', 'Peur','Surprise', 'Tristesse']
nb_labels = len(labels)
#rep_modele_local = "./Modeles/maisonBinaire_multi_tout_entier_isole_pondere50_9_epochs_3_checkpoint-14676/"
rep_modele_local = "../../src/resources/modele_emotyc/maisonBinaire_multi_tout_entier_isole_pondere50_9_epochs_3_checkpoint-14676/"

# Chargement du modèle local
modele_local = AutoModelForSequenceClassification.from_pretrained(rep_modele_local,
                                                                  num_labels=nb_labels,
                                                                  problem_type = "multi_label_classification")
# Initialisation du Trainer
trainer_local = Trainer(
    model=modele_local
)

#####----- FONCTIONS pour la prédiction -----#####
def recupDocStanza(chemin_fichier):
    """
    """
    with open(chemin_fichier,'r', encoding="utf-8") as my_file:
        text = my_file.read()
        if len(text) > 0:
            text = text.replace("\\n","\n")
            doc = analysis_pipeline(text)
    return doc

def recupPhrasesStanza(doc_Stanza):
    """
    """
    liste_phrases = [doc_Stanza.sentences[i].text for i in range(len(doc_Stanza.sentences))]
    return liste_phrases

def transformeResultats(resultats_predictions,seuil=0.5):
    """
    """
    sigmoid = torch.nn.Sigmoid()
    probs = sigmoid(torch.Tensor(resultats_predictions[0]))
    predictions = [[1 if p>seuil else 0 for p in item] for item in probs]
    return predictions

#####----- VARIABLES pour les features -----#####
## /!\ L'ordre des descripteurs bruts doit être EXACTEMENT le même que celui des labels !!! /!\ cf. ligne 16
liste_descripteurs_bruts = [
    "avec_emo",
    "avec_emo_comportementale",
    "avec_emo_designee",
    "avec_emo_montree",
    "avec_emo_suggeree",
    "avec_emo_base",
    "avec_emo_complexe",
    "avec_cat_autre",
    "avec_cat_admiration",
    "avec_cat_colere",
    "avec_cat_culpabilite",
    "avec_cat_degout",
    "avec_cat_embarras",
    "avec_cat_fierte",
    "avec_cat_jalousie",
    "avec_cat_joie",
    "avec_cat_peur",
    "avec_cat_surprise",
    "avec_cat_tristesse"
]

descripteurs_nombre = ["nombre_" + desc for desc in liste_descripteurs_bruts] # pour les descripteurs de type "nombre"
descripteurs_proportion = ["proportion_" + desc for desc in liste_descripteurs_bruts] # pour les descripteurs de type "proportion"
features_list = [desc for desc in descripteurs_nombre+descripteurs_proportion]

#####----- FONCTIONS pour les features -----#####
def extraitDescBrutsPhrase(dico_descripteurs_bruts,predictions_phrase):
    """
    """
    for i,classe in enumerate(labels): # pour chaque classe à prédire
        if predictions_phrase[i]==1: # cette classe a été prédite comme présente pour la phrase
            # on augement de 1 le compte pour la feature correspondant à la classe prédite
            dico_descripteurs_bruts[liste_descripteurs_bruts[i]]+=1
    return dico_descripteurs_bruts

def genereDescripteurs(doc_Stanza,levels,chemin_sortie):
    """
    """
    dico_phrase_chaines = {}
    liste_cles_phrase = []
    # Étape 1 : récupération des phrases découpées par Stanza
    liste_phrases = recupPhrasesStanza(doc_Stanza)
    
    # Étape 2 : tokenisation avec camembert
    # tokenisation des phrases
    liste_phrases_tokenisees = [
        tokenizer(phrase,
                  padding="max_length", truncation=True, add_special_tokens=False)
        for phrase in liste_phrases
    ]
    
    # Étape 3 : prédictions des classes
    # prédiction
    resultats = trainer_local.predict(test_dataset=liste_phrases_tokenisees)
    # transformation des prédictions en label 0 ou 1
    predictions = transformeResultats(resultats)
    
    # Étape 4 : transformation des prédictions en features + écriture de la sortie
    with open(chemin_sortie,'w', encoding="utf8") as sortant:
        
        # Étape 4.1 : analyse au niveau de la phrase
        if "sentence" in levels:
            sortant.write("====== Sentence Level Features ======\n\n")
            for i,predictions_phrase in enumerate(predictions):
                cle_phrase = str(i+1) + ". Sentence: " + liste_phrases[i] + "\n"
                liste_cles_phrase.append(cle_phrase)
                sortant.write(cle_phrase)
                
                dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
                dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,
                                                                        predictions_phrase)
                               
                chaines = list()
                if 1 in dico_descripteurs_bruts_phrase.values(): # au moins une feature a été trouvée
                    chaines.append("Features :: Feature-Counts :: Feature-Texts\n") 
                    chaines.append("--------------------------------------------\n")
                    sortant.write("Features :: Feature-Counts :: Feature-Texts\n") 
                    sortant.write("--------------------------------------------\n")
                    for desc in dico_descripteurs_bruts_phrase: # pour chaque feature
                        if dico_descripteurs_bruts_phrase[desc]!=0: # si elle est dans la phrase
                            chaines.append(desc + "::" + "1" + "::" + "[ " + "oui" + " ]\n")
                            sortant.write(desc + "::" + "1" + "::" + "[ " + "oui" + " ]\n")
                else: # aucune feature dans la phrase
                    chaines.append("No features found.\n")
                    sortant.write("No features found.\n")
                    
                chaines.append("\n")
                sortant.write("\n")                
                dico_phrase_chaines[cle_phrase] = chaines
        
        # Étape 4.2. : analyse au niveau du texte
        if "text" in levels:
            nb_phrases = len(liste_phrases)
            descripteurs_extraits_texte = list()
            descripteurs_extraits_compteur_texte = defaultdict(lambda :0)
            descripteurs_extraits_phrases_concernees_texte = defaultdict(lambda :list())
            compteur_descripteurs_extraits_texte = 0
            
            # les descripteur à l'échelle de la phrase
            for i,predictions_phrase in enumerate(predictions):
                cle_phrase = str(i+1) + ". Sentence: " + liste_phrases[i].strip()
                dico_descripteurs_bruts_phrase = dict.fromkeys(liste_descripteurs_bruts,0)
                dico_descripteurs_bruts_phrase = extraitDescBrutsPhrase(dico_descripteurs_bruts_phrase,
                                                                        predictions_phrase)
                
                for desc in dico_descripteurs_bruts_phrase: # pour chaque feature
                    if dico_descripteurs_bruts_phrase[desc]!=0: # si la feature est dans la phrase
                        if desc not in descripteurs_extraits_texte:
                            descripteurs_extraits_texte.append(desc)
                        descripteurs_extraits_compteur_texte[desc]+=1
                        descripteurs_extraits_phrases_concernees_texte[desc].append(cle_phrase)
                        
            # nb total de features trouvées
            for desc in descripteurs_extraits_compteur_texte:
                compteur_descripteurs_extraits_texte += descripteurs_extraits_compteur_texte[desc]
            
            sortant.write("\n====== Text Level Features ======\n\n")
            
            if descripteurs_extraits_compteur_texte == 0: # aucune feature
                sortant.write("No features found.\n")
            else:
                sortant.write("Features :: Feature-Counts :: Feature-Texts\n")            
                sortant.write("--------------------------------------------\n")
                for desc in descripteurs_extraits_compteur_texte:
                    sortant.write(desc + "::" +
                                  str(descripteurs_extraits_compteur_texte[desc]) + "::" +
                                  "[ " + ", ".join(set(descripteurs_extraits_phrases_concernees_texte[desc])) + " ]\n")
                sortant.write("Total feature counts: " + str(compteur_descripteurs_extraits_texte) + "\n")
            sortant.write("\n")
        return liste_cles_phrase, dico_phrase_chaines
