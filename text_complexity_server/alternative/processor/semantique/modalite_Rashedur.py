import os

from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

from collections import defaultdict
import regex
import operator
from collections import OrderedDict

processor_name = 'modalite'

nom_descripteurs = [
    "marqueurs",
    "alethique",
    "appreciatif",
    "axiologique",
    "boulique",
    "deontique",
    "epistemique",
    "pas_d_indication",
    "marqueurs_niveau0",
    "marqueurs_niveau1",
    "marqueurs_niveau2",
    "marqueurs_niveau3",
    "alethique_niveau0",
    "alethique_niveau1",
    "alethique_niveau2",
    "alethique_niveau3",
    "appreciatif_niveau0",
    "appreciatif_niveau1",
    "appreciatif_niveau2",
    "appreciatif_niveau3",
    "axiologique_niveau0",
    "axiologique_niveau1",
    "axiologique_niveau2",
    "axiologique_niveau3",
    "boulique_niveau0",
    "boulique_niveau1",
    "boulique_niveau2",
    "boulique_niveau3",
    "deontique_niveau0",
    "deontique_niveau1",
    "deontique_niveau2",
    "deontique_niveau3",
    "epistemique_niveau0",
    "epistemique_niveau1",
    "epistemique_niveau2",
    "epistemique_niveau3",
    "pas_d_indication_niveau0",
    "pas_d_indication_niveau1",
    "pas_d_indication_niveau2",
    "pas_d_indication_niveau3"
]
descripteurs_nombre = ["nombre_"+n for n in nom_descripteurs] # liste des descripteurs de type nombre
descripteurs_proportions = ["proportion_"+n for n in nom_descripteurs] # liste des descripteurs de type proportion
liste_descripteurs = [f for f in descripteurs_nombre+descripteurs_proportions] # tous les descripteurs

def recupLexique(chemin):
    """
    Récupère le lexique et le stocke dans un dictionnaire.
    Entrée :
    - STR(chemin du lexique)
    Sortie :
    - DICT{
        STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Enrichissement):LIST[STR(niveau d'enrichissement, 0, 1, 2 ou 3)]
                                                }
    }
    """
    dico_marqueur_infos = defaultdict(lambda:dict())
    with open(chemin,'r',encoding="utf-8") as entrant:
        entrant.readline() # on élimine la ligne avec les en-têtes
        for ligne in entrant: # pour chaque marqueur de modalité
            elements = ligne.strip().split("\t")
            tr = str.maketrans("é '", "e__")
            elements1 = elements[1].translate(tr)  # elements[1].replace("é", "e").replace("'", "_").replace(" ", "_")
            elements6 =  elements[6].translate(tr)  # elements[6].replace("é", "e").replace("'", "_").replace(" ", "_")
            dico_marqueur_infos[elements[0]]["Cat_modale"]=[e for e in elements1.split(";")] # peut y avoir plusieurs cat modales
            dico_marqueur_infos[elements[0]]["Enrichissement"]=[e for e in elements6.split(";")] # normalement, un seul niveau d'enrichissement
    return dico_marqueur_infos
   
def compteMarqueurs(liste_lemmes,dico_marqueur_infos,dico_descripteur_occurrences):
    """
    Repère et classe les marqueurs de modalité présents dans une phrase
    Entrée :
    - LIST[STR(lemme1),STR(lemme2)]
    - DICT{
        STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Enrichissement):LIST[STR(niveau d'enrichissement, 0, 1, 2 ou 3)]
                                                }
    }
    - dico_descripteur_occurrences de la forme
    DICT{
        STR(nom du descripteur) : INT(nb d'occurrence de la catégorie nécessaire pour calculer le descripteur)
    }
    Sortie :
    - dico_descripteur_occurrences complété
    """
    dict_descripteur_lemma = {}
    for key in dico_descripteur_occurrences.keys():
        dict_descripteur_lemma[key] = []

    for lemme in liste_lemmes: # pour chaque lemme
        if lemme in dico_marqueur_infos: # si le lemme est dans notre lexique
            dico_descripteur_occurrences["marqueurs"]+=1 # +1 marqueur de modalité reconnu
            dict_descripteur_lemma["marqueurs"] += [lemme]
            
            # Catégories modales - reconnaissance générale
            for c in dico_marqueur_infos[lemme]["Cat_modale"]: # pour chaque catégorie modale associée au marqueur
                dico_descripteur_occurrences[c]+=1 # +1 marqueur de cette cat modale reconnu
                dict_descripteur_lemma[c] += [lemme]

                # catégorie modale - par niveau d'enrichissement
                for n in dico_marqueur_infos[lemme]["Enrichissement"]:
                    dico_descripteur_occurrences[c+"_niveau"+n]+=1
                    dict_descripteur_lemma[c+"_niveau"+n] += [lemme]

            # Niveau de confiance - reconnaissance générale
            for n in dico_marqueur_infos[lemme]["Enrichissement"]: # pour chaque niveau d'enrichissement - normalement un seul
                dico_descripteur_occurrences["marqueurs_niveau"+n]+=1 # +1 marqueur de ce niveau d'enrichissement reconnu
                dict_descripteur_lemma["marqueurs_niveau"+n] += [lemme]

    
    # for key in dico_descripteur_occurrences.keys():
    #     if dico_descripteur_occurrences[key] > 0:
    #         print(key + "::" + str(dico_descripteur_occurrences[key]) + " :::", dict_descripteur_lemma[key][1:] )
    
    return dico_descripteur_occurrences, dict_descripteur_lemma

    
def calculeDescripteurs(liste_lemmes,dico_descripteur_occurrences,dico_descripteurs):
    """
    Calcule les descripteurs qu'on veut que la chaîne retourne. Donc des nombres d'occurrence et des proportions.
    Entrée :
    - la liste de lemmes dans lesquels on a fait le repérage de marqueurs de modalité
    - dico_descripteur_occurrences
    - dico_descripteurs
    Sortie :
    - dico_descripteurs complété
    """
    for nom_descripteur in dico_descripteur_occurrences:
        # Descripteurs de type nombre : la valeur = le nb d'occurrences
        dico_descripteurs["nombre_"+nom_descripteur] = dico_descripteur_occurrences[nom_descripteur]
        # Descripteurs de type proportion : il faut calculer les proportions
        if (nom_descripteur=="marqueurs" or
           nom_descripteur=="alethique" or
           nom_descripteur=="appreciatif" or
           nom_descripteur=="axiologique" or
           nom_descripteur=="boulique" or
           nom_descripteur=="deontique" or
           nom_descripteur=="epistemique" or
           nom_descripteur=="pas_d_indication"): # pour ces catégories générales, on calcule par rapport au nb de mots total
            dico_descripteurs["proportion_"+nom_descripteur]=(dico_descripteur_occurrences[nom_descripteur]/len(liste_lemmes)) if len(liste_lemmes) > 0 else 0
        else: # pour les autres, on calcule par rapport au nb de marqueurs repérés dans la catégorie
            m=regex.match("(.+)_(niveau\d)",nom_descripteur)
            cat=m.group(1)
            niveau=m.group(2)
            try:
                dico_descripteurs["proportion_"+nom_descripteur]=dico_descripteur_occurrences[nom_descripteur]/dico_descripteur_occurrences[cat]
            except ZeroDivisionError:
                dico_descripteurs["proportion_"+nom_descripteur]=float(0)
    return dico_descripteurs

def creeListeLemmes(phraseStanza):
    """
    /!\ ne fonctionne qu'avec un objet de type Sentence issu de `doc.sentences` où doc est un texte parsé par Stanza
    Pour une phrase Stanza donnée,
    renvoie une liste de liste de lemme.
    """
    liste_lemmes = list()
    phrase = phraseStanza.to_dict() # on convertit l'objet Sentence pour pouvoir itérer dessus
    for i in range(len(phrase)): # pour chaque mot de la phrase
        l = phrase[i].get('lemma', phrase[i].get('text')) # on récupère le lemme
        liste_lemmes.append(l)
    return liste_lemmes


def getSentenceStr(phraseStanza):
    liste_toks = list()
    phrase = phraseStanza.to_dict() # on convertit l'objet Sentence pour pouvoir itérer dessus
    for i in range(len(phrase)): # pour chaque mot de la phrase
        l = phrase[i].get('text', phrase[i].get('text')) # on récupère le lemme
        liste_toks.append(l)
    return " ".join(liste_toks)


    
def analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs):
    """
    Pour une liste de lemmes donnée, analyse la modalité.
    Entrée :
    - LIST[STR(lemme1),STR(lemme2)]
    - lexique de marqueurs de modalité
        DICT{
            STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Enrichissement):LIST[STR(niveau d'enrichissement, 0, 1, 2 ou 3)]
                                                }
        }
    - LIST[STR(nom brut du descripteur 1),STR(nom brut du descripteur 2)]
    - LIST[STR(nombre/proportion+nom brut du descripteur 1)),etc.]
    Sortie :
    - DICT{STR(nom du descripteur):FLOAT(la valeur du descripteur)}
    """   
    # Étape 1 : création des dictionnaires pour accueillir les descripteurs
    dico_descripteur_occurrences = dict.fromkeys(nom_descripteurs , 0) # nb d'occurrences
    dico_descripteurs = dict.fromkeys(liste_descripteurs , 0) # descripteurs en tant que tel
    
    # Étape 2 : repérage des marqueurs de modalité --> on compte les occurrences de chaque catégorie
    dico_descripteur_occurrences, dict_descripteur_lemma = compteMarqueurs(liste_lemmes,dico_lexique,dico_descripteur_occurrences)
    
    # Étape 3 : calculer les descripteurs en tant que tels
    dico_descripteurs = calculeDescripteurs(liste_lemmes,dico_descripteur_occurrences,dico_descripteurs)
    
    return dico_descripteur_occurrences, dico_descripteurs, dict_descripteur_lemma
    

def get_modality_features(docStanza,levels, outfile):
    chemin_lexique = "../processor/semantique/tous_marqueurs_harmonise_sans_doublons_formes_isolees_A_JOUR.tsv"
    dico_lexique = recupLexique(chemin_lexique)

    dict_sent_return_strings = {}
    sentence_list = []
    
    # with open("/home/rahman/Downloads/modality.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:
            # print("====== Sentence Level Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")
            for idx, phrase in enumerate(docStanza.sentences):
                sent = getSentenceStr(phrase)
                liste_lemmes = creeListeLemmes(phrase)
                dico_descripteur_occurrences, dico_descripteurs, dict_descripteur_lemma=analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs)
                dico_descripteur_occurrences = dict(sorted(dico_descripteur_occurrences.items(), key=operator.itemgetter(1),reverse=True))
                
                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + phrase.text + "\n"
                sentence_list.append(sent_key)
                
                outfile.write(str(idx+1) + ". Sentence: " + phrase.text + "\n")
                has_modalite = False
                for key in dico_descripteur_occurrences.keys():
                    if dico_descripteur_occurrences[key] > 0:
                        has_modalite = True
                        break

                if has_modalite: 
                    return_strings.append("Features :: Feature-Counts :: Feature-Lemmas\n")
                    return_strings.append("--------------------------------------------\n")
                    outfile.write("Features :: Feature-Counts :: Feature-Lemmas\n")
                    outfile.write("--------------------------------------------\n")
                    count = 0
                    for key in dico_descripteur_occurrences.keys():
                        if dico_descripteur_occurrences[key] > 0:
                            count += dico_descripteur_occurrences[key]
                            return_strings.append(key + " :: " + str(dico_descripteur_occurrences[key]) + " :: [ " + ", ".join(list(dict_descripteur_lemma[key])) + " ]\n")
                            outfile.write(key + " :: " + str(dico_descripteur_occurrences[key]) + " :: [ " + ", ".join(list(dict_descripteur_lemma[key])) + " ]\n")
                
                    return_strings.append("Total feature counts: " + str(count) + "\n")
                    outfile.write("Total feature counts: " + str(count) + "\n")
                else:
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")
                
                return_strings.append("\n")
                outfile.write("\n")
                dict_sent_return_strings[sent_key] = return_strings
                

        if "text" in levels:
            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")
            liste_lemmes = [l for phrase in docStanza.sentences for l in creeListeLemmes(phrase)]

            dico_descripteur_occurrences, dico_descripteurs, dict_descripteur_lemma=analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs)
            dico_descripteur_occurrences = dict(sorted(dico_descripteur_occurrences.items(), key=operator.itemgetter(1),reverse=True))
            
            has_modalite = False
            for key in dico_descripteur_occurrences.keys():
                if dico_descripteur_occurrences[key] > 0:
                    has_modalite = True
                    break

            if has_modalite: 
                # print("Features :: Feature-Counts :: Feature-Lemmas\n")
                # print("--------------------------------------------\n")
                outfile.write("Features :: Feature-Counts :: Feature-Lemmas\n")
                outfile.write("--------------------------------------------\n")
                total_occurences_doc_level = 0
                for key in dico_descripteur_occurrences.keys():
                    if dico_descripteur_occurrences[key] > 0:
                        total_occurences_doc_level += dico_descripteur_occurrences[key]
                        # print(key + " :: " + str(dico_descripteur_occurrences[key]) + " :: [ " + ", ".join(list(dict_descripteur_lemma[key])) + " ]\n" )
                        outfile.write(key + " :: " + str(dico_descripteur_occurrences[key]) + " :: [ " + ", ".join(list(dict_descripteur_lemma[key])) + " ]\n" )
                
                # print("Total feature counts: " + str(total_occurences_doc_level) + "\n")
                outfile.write("Total feature counts: " + str(total_occurences_doc_level) + "\n")
            else:
                # print("No features found.\n")
                outfile.write("No features found.\n")

            # print("\n")
            outfile.write("\n")

    return sentence_list, dict_sent_return_strings

