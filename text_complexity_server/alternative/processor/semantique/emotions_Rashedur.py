
from stanza.models.common.doc import Document
from inria_processors.extractors import Sentiment_Rashedur

lexicon = Sentiment_Rashedur.getLexicon("../inria_processors/tools/Lexique/lexique_emotionnel.tsv")[1]

def get_emotion_features(doc, levels, outfile):
    dict_sent_return_strings = {}
    sentence_list = []

    # with open("/home/rahman/Downloads/emotions.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:
            # print("====== Sentence Levsel Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")
            sents_emotion_lemmas_toks = []
            for idx, sentence in enumerate(doc.sentences):
                if isinstance(sentence, Document):
                    conll_text = sentence.to_dict()
                else:
                    conll_text = [sentence.to_dict()]
                sent, lemmasPorteursEmotion, toksPorteursEmotion = Sentiment_Rashedur.EmotionVectorPhrase(conll_text[0], lexicon)

                sents_emotion_lemmas_toks.append((sent, lemmasPorteursEmotion, toksPorteursEmotion))

                categories = [lexicon[lemma] for lemma in lemmasPorteursEmotion]

                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
                sentence_list.append(sent_key)
                outfile.write(str(idx+1) + ". Sentence: " + sentence.text + "\n")

                if lemmasPorteursEmotion: 
                    return_strings.append("Tokens having emotion: " + "[ " + ", ".join(toksPorteursEmotion) + " ]\n")
                    return_strings.append("Lemmas having emotion: " + "[ " + ", ".join(lemmasPorteursEmotion) + " ]\n")
                    return_strings.append("Categories of the emotion lemmas: " + "[ " + ", ".join(categories) + " ]\n")

                    outfile.write("Tokens having emotion: " + "[ " + ", ".join(toksPorteursEmotion) + " ]\n")
                    outfile.write("Lemmas having emotion: " + "[ " + ", ".join(lemmasPorteursEmotion) + " ]\n")
                    outfile.write("Categories of the emotion lemmas: " + "[ " + ", ".join(categories) + " ]\n")
                else:
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")

                return_strings.append("\n")
                outfile.write("\n")
                dict_sent_return_strings[sent_key] = return_strings


        if "text" in levels: 
            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")

            sents_emotion_lemmas_toks = []
            for idx, sentence in enumerate(doc.sentences):
                if isinstance(sentence, Document):
                    conll_text = sentence.to_dict()
                else:
                    conll_text = [sentence.to_dict()]
                sent, lemmasPorteursEmotion, toksPorteursEmotion = Sentiment_Rashedur.EmotionVectorPhrase(conll_text[0], lexicon)

                sents_emotion_lemmas_toks.append((sent, lemmasPorteursEmotion, toksPorteursEmotion))


            sents_emotion_lemmas_toks = list(zip(*sents_emotion_lemmas_toks))
            sents, emotion_lemmas, emotion_toks = sents_emotion_lemmas_toks[0], sents_emotion_lemmas_toks[1], sents_emotion_lemmas_toks[2]

            emotion_lemmas_doc = [ lemma for el in emotion_lemmas for lemma in el]
            emotion_toks_doc = [ tok for et in emotion_toks for tok in et]

            categories = [lexicon[lemma] for lemma in emotion_lemmas_doc]


            if emotion_lemmas_doc: 
                # print("Tokens having emotion: " + "[ " + ", ".join(emotion_toks_doc) + " ]\n")
                # print("Lemmas having emotion: " + "[ " + ", ".join(emotion_lemmas_doc) + " ]\n")
                # print("Category of the emotion lemmas: " + "[ " + ", ".join(categories) + " ]\n")
                # print("Total emotion lemma counts: " + str(len(emotion_lemmas_doc)) + "\n")

                outfile.write("Tokens having emotion: " + "[ " + ", ".join(emotion_toks_doc) + " ]\n")
                outfile.write("Lemmas having emotion: " + "[ " + ", ".join(emotion_lemmas_doc) + " ]\n")
                outfile.write("Category of the emotion lemmas: " + "[ " + ", ".join(categories) + " ]\n")
                outfile.write("Total emotion lemma counts: " + str(len(emotion_lemmas_doc)) + "\n")
            else:
                # print("No features found.\n")
                outfile.write("No features found.\n")
            # print("\n")
            outfile.write("\n")

    return sentence_list, dict_sent_return_strings