from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from stanza.models.common.doc import Document
import os
import re
from pathlib import Path

processor_name = 'connecteurs_organisateurs'

directory_organizers = "../processor/semantique/organisateurs"
directory_connectors = "../processor/semantique/connecteurs"
directories = [directory_organizers, directory_connectors]

def load_features_dictionary(directories):
    features_dictionary = {}
    for directory in directories:
        for filename in os.listdir(directory):
            feature = Path(filename).stem
            f = os.path.join(directory, filename)
            if os.path.isfile(f):
                with open(f, 'r', encoding="utf-8") as file:
                    for line in file:  
                        if feature in features_dictionary:
                            features_dictionary[feature].append(line.rstrip())
                        else:
                            features_dictionary[feature] = [line.rstrip()]
               
    return features_dictionary


features_dictionary = load_features_dictionary(directories)

def set_feature_list():
    f_list = []
    features = list(features_dictionary.keys())
    for feature in features:
        f_list.append("nombre_"+feature)
        f_list.append("proportion_"+feature)
    return f_list
    
features_list = set_feature_list()


def setFeatures(text):
    extracted_features = []
    extracted_features_and_counts = {}
    extracted_features_and_feature_texts = {}

    for feature in list(features_dictionary.keys()):
        extracted_feature_count, feature_texts = extract_feature(feature, text)

        if feature_texts: 
            extracted_features.append(feature)
            extracted_features_and_counts[feature] = extracted_feature_count
            extracted_features_and_feature_texts[feature] = feature_texts
        else:
            extracted_features_and_counts[feature] = 0
            extracted_features_and_feature_texts[feature] = []

    
    return extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts


def extract_feature(feature, text):
    feature_count = 0
    feature_values = features_dictionary[feature]
    feature_texts = []

    for feature_value in feature_values: 
        # f_texts = re.findall(feature_value, text) 
        #f_texts = re.findall(" " + feature_value + " ", " " + text + " ") #ne retrouve pas "or" dans je sors or, il pleut"
        f_texts = re.findall("\W" + feature_value + "\W", " " + text + " ") #normalement, règle le pb
        feature_count += len(f_texts)
        if len(f_texts) > 0:
            feature_texts.extend([s.strip() for s in f_texts])

    return feature_count, feature_texts


def get_connector_organizers_features(doc,levels, outfile):
    dict_sent_return_strings = {}
    sentence_list = []

    # with open("/home/rahman/Downloads/connector_organizers.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:       
            # print("====== Sentence Level Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")
            for idx, sentence in enumerate(doc.sentences):
                extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts = setFeatures(sentence.text) 

                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
                sentence_list.append(sent_key)

                outfile.write(str(idx+1) + ". Sentence: " + sentence.text + "\n")

                if extracted_features:
                    return_strings.append("Features :: Feature-Counts :: Feature-Texts\n")                
                    return_strings.append("--------------------------------------------\n")

                    outfile.write("Features :: Feature-Counts :: Feature-Texts\n")                
                    outfile.write("--------------------------------------------\n")
                    for f in extracted_features:
                        return_strings.append(f + " :: " + str(extracted_features_and_counts[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts[f])) + " ]\n")
                        outfile.write(f + " :: " + str(extracted_features_and_counts[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts[f])) + " ]\n")
                else: 
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")
                
                return_strings.append("\n")
                outfile.write("\n")                
                dict_sent_return_strings[sent_key] = return_strings


        if "text" in levels:
            sent_count = len(doc.sentences)
            extracted_features_doc_level = []
            extracted_features_and_counts_doc_level = {}
            extracted_features_and_feature_texts_doc_level = {}

            for sentence in doc.sentences:
                extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts = setFeatures(sentence.text)
                extracted_features_doc_level.extend(extracted_features)

                for f in extracted_features:
                    if f not in extracted_features_and_counts_doc_level:
                        extracted_features_and_counts_doc_level[f] = extracted_features_and_counts[f]
                        extracted_features_and_feature_texts_doc_level[f] = extracted_features_and_feature_texts[f]
                    else:
                        extracted_features_and_counts_doc_level[f] += extracted_features_and_counts[f]
                        extracted_features_and_feature_texts_doc_level[f] += extracted_features_and_feature_texts[f]

            # extracted_features_doc_level = set(extracted_features_doc_level)
            extracted_feature_counts_doc_level = 0
            for f in extracted_features_and_counts_doc_level:
                extracted_feature_counts_doc_level += extracted_features_and_counts_doc_level[f]

            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")

            if extracted_feature_counts_doc_level == 0:
                # print("No features found.\n")
                outfile.write("No features found.\n")
            else:
                # print("Features :: Feature-Counts :: Feature-Texts\n")            
                # print("--------------------------------------------\n")

                outfile.write("Features :: Feature-Counts :: Feature-Texts\n")            
                outfile.write("--------------------------------------------\n")
                for f in extracted_features_and_counts_doc_level:
                    # print(f + " :: " + str(extracted_features_and_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts_doc_level[f])) + " ]\n")
                    outfile.write(f + " :: " + str(extracted_features_and_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts_doc_level[f])) + " ]\n")

                # print("Total feature counts: " + str(extracted_feature_counts_doc_level) + "\n")
                outfile.write("Total feature counts: " + str(extracted_feature_counts_doc_level) + "\n")

            # print("\n")
            outfile.write("\n")

    return sentence_list, dict_sent_return_strings
        


@register_processor(processor_name)
class LogicalConnectorOrganizersProcessor(Processor):
    """ Processor that computes logical connectors text organizers proportion at sentence and text level """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)    

        return doc

