from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format
from stanza.models.common.doc import Document
import os
import re
from pathlib import Path
import stanza


processor_name = 'metaphores'

directory = "../processor/semantique/metaphores/"
metaphores_filename = "metaphores_min_2_mots.txt"
metaphores_lemmatized_filename = "metaphores_min_2_mots_lemmatises.txt"


def get_lemma(input):
    lemmatized_text = ""

    if isinstance(input, Document):
        for sentence in input.sentences:
            if len(sentence.words) > 1:
                for word in sentence.words:
                    lemmatized_text += word.lemma + " "
    else:
        if len(input.words) > 1:
            for word in input.words:
                lemmatized_text += word.lemma + " "
    return lemmatized_text.rstrip() 


def get_lemmatized_sent(sentence):
    lemmatized_sent = ""

    if len(sentence.words) > 1:
        for word in sentence.words:
            lemmatized_sent += word.lemma + " "

    return lemmatized_sent.rstrip()


def load_features_dictionary(directory):
    features_dictionary = {}

    feature = Path(metaphores_filename).stem

    f = os.path.join(directory, metaphores_filename)

    f_path_lemmatized = os.path.join(directory, metaphores_lemmatized_filename)
    
    if os.path.isfile(f_path_lemmatized):
        print("metaphores lemmatized file loading")
        with open(f_path_lemmatized, 'r', encoding="utf-8") as file:
            for line in file:  
                entry = line.rstrip()
                if entry != "":
                    if feature in features_dictionary:
                        features_dictionary[feature].append(entry)
                    else:
                        features_dictionary[feature] = [entry]            
    
    # if metaphore lemmatized file does not exist, cretae it
    elif os.path.isfile(f):
        f_lemmatized = open(f_path_lemmatized, 'w', encoding="utf-8")
        
        from stanza.pipeline.core import DownloadMethod
        nlp_processor = stanza.Pipeline('fr',
                        processors="tokenize,lemma",
                        use_gpu=False,
                        pos_batch_size=3000,
                        download_method=DownloadMethod.REUSE_RESOURCES)

        with open(f, 'r', encoding="utf-8") as file:
            for line in file:  

                entry = get_lemma(nlp_processor(line.rstrip()))
                if entry != "":
                    f_lemmatized.write(entry)
                    f_lemmatized.write('\n')

                    if feature in features_dictionary:
                        features_dictionary[feature].append(entry)
                    else:
                        features_dictionary[feature] = [entry]
        f_lemmatized.close() 
   
    return features_dictionary

features_dictionary = load_features_dictionary(directory)
print(features_dictionary.keys())


def set_feature_list():
    f_list = []
    features = list(features_dictionary.keys())
    for feature in features:
        f_list.append("nombre_"+feature)
        f_list.append("proportion_"+feature)
    return f_list
    
features_list = set_feature_list()


def setFeatures(lemmatized_text):
    extracted_features = []
    extracted_features_and_counts = {}
    extracted_features_and_feature_texts = {}

    for feature in list(features_dictionary.keys()):
        extracted_feature_count, feature_texts = extract_feature(feature, lemmatized_text)

        if feature_texts: 
            extracted_features.append(feature)
            extracted_features_and_counts[feature] = extracted_feature_count
            extracted_features_and_feature_texts[feature] = feature_texts
        else:
            extracted_features_and_counts[feature] = 0
            extracted_features_and_feature_texts[feature] = []

    return extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts    


def extract_feature(feature, text):
    feature_count = 0
    feature_values = features_dictionary[feature]
    feature_texts = []

    for feature_value in feature_values: 
        # f_texts = re.findall(feature_value, text)
        f_texts = re.findall(" " + feature_value + " ", " " + text + " ")
        feature_count += len(f_texts)
        if len(f_texts) > 0:
            feature_texts.extend([s.strip() for s in f_texts])

    return feature_count, feature_texts


def get_metaphore_features(doc,levels, outfile):
    dict_sent_return_strings = {}
    sentence_list = []

    # with open("/home/rahman/Downloads/metaphores.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:
            # print("====== Sentence Level Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")
            for idx, sentence in enumerate(doc.sentences):
                lemmatized_sent = get_lemmatized_sent(sentence)  
                extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts = setFeatures(lemmatized_sent) 

                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
                sentence_list.append(sent_key)
                
                outfile.write(str(idx+1) + ". Sentence: " + sentence.text + "\n")

                if extracted_features:
                    return_strings.append("Features :: Feature-Counts :: Feature-Texts\n")
                    return_strings.append("--------------------------------------------\n")
                    outfile.write("Features :: Feature-Counts :: Feature-Texts\n")
                    outfile.write("--------------------------------------------\n")
                    for f in extracted_features:
                        return_strings.append(f + " :: " + str(extracted_features_and_counts[f]) + " :: " + "[ " + ", ".join(list(extracted_features_and_feature_texts[f])) + " ]\n")
                        outfile.write(f + " :: " + str(extracted_features_and_counts[f]) + " :: " + "[ " + ", ".join(list(extracted_features_and_feature_texts[f])) + " ]\n")
                else: 
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")

                return_strings.append("\n")
                outfile.write("\n")
                dict_sent_return_strings[sent_key] = return_strings



        if "text" in levels:
            sent_count = len(doc.sentences)
            extracted_features_doc_level = []
            extracted_features_and_counts_doc_level = {}        
            extracted_features_and_feature_texts_doc_level = {}

            for sentence in doc.sentences:
                lemmatized_sent = get_lemmatized_sent(sentence)
                extracted_features, extracted_features_and_counts, extracted_features_and_feature_texts = setFeatures(lemmatized_sent)
                extracted_features_doc_level.extend(extracted_features)

                for f in extracted_features:
                    if f not in extracted_features_and_counts_doc_level:
                        extracted_features_and_counts_doc_level[f] = extracted_features_and_counts[f]
                        extracted_features_and_feature_texts_doc_level[f] = extracted_features_and_feature_texts[f]
                    else:
                        extracted_features_and_counts_doc_level[f] += extracted_features_and_counts[f]
                        extracted_features_and_feature_texts_doc_level[f] += extracted_features_and_feature_texts[f]



            # extracted_features_doc_level = set(extracted_features_doc_level)
            extracted_feature_counts_doc_level = 0
            for f in extracted_features_and_counts_doc_level:
                extracted_feature_counts_doc_level += extracted_features_and_counts_doc_level[f]

            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")
            if extracted_feature_counts_doc_level == 0:
                # print("No features found.\n")
                outfile.write("No features found.\n")
            else:
                # print("Features :: Feature-Counts :: Feature-Texts\n")
                # print("--------------------------------------------\n")
                outfile.write("Features :: Feature-Counts :: Feature-Texts\n")
                outfile.write("--------------------------------------------\n")
                for f in extracted_features_and_counts_doc_level:
                    # print(f + " :: "+ str(extracted_features_and_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts_doc_level[f])) + " ]\n")
                    outfile.write(f + " :: "+ str(extracted_features_and_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(extracted_features_and_feature_texts_doc_level[f])) + " ]\n")

                # print("Total feature counts: " + str(extracted_feature_counts_doc_level) + "\n")
                outfile.write("Total feature counts: " + str(extracted_feature_counts_doc_level) + "\n")

            # print("\n")
            outfile.write("\n")

    return sentence_list, dict_sent_return_strings



@register_processor(processor_name)
class LogicalConnectorOrganizersProcessor(Processor):
    """ Processor that computes metaphores proportion at sentence and text level """
    _requires = {"tokenize,lemma"}
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)   

        return doc

