from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'propositions_subordonnees'

# features_list = [
#     'nombre_subordonnees_type_savoir_que',
#     'proportion_subordonnees_type_savoir_que',
#     'nombre_subordonnees_type_savoir_que_negation',
#     'proportion_subordonnees_type_savoir_que_negation',
#     'nombre_subordonnees_type_croire_que_present',
#     'proportion_subordonnees_type_croire_que_present',
#     'nombre_subordonnees_type_savoir_si',
#     'proportion_subordonnees_type_savoir_si',
#     'nombre_subordonnees_avant_apres',
#     'proportion_subordonnees_avant_apres',
#     'nombre_subordonnees_gerondif',
#     'proportion_subordonnees_gerondif'    
#     ]


features_list = [
    'subordonnees_type_savoir_que',
    'subordonnees_type_savoir_que_negation',
    'subordonnees_type_croire_que_present',
    'subordonnees_type_savoir_si',
    'subordonnees_avant_apres',
    'subordonnees_gerondif'
    ]

def setFeatures(sentence):
    words = sentence.words_list
    sentence_count = 1

    # know if clause identification
    know_if_clauses_count = 0    
    know_if_clause_trigger = False
    know_if_clauses_lemmas = [] 

    words_in_between = 0
    words_in_not_clause_between = 0

    trigger_lemma = ""
    for word in words :
        if word.lemma == "savoir":
            know_if_clause_trigger = True
            trigger_lemma = word.lemma
        elif know_if_clause_trigger:
            if word.upos != 'PUNCT' and words_in_between <= 3:
                if word.lemma == "si":
                    know_if_clauses_count += 1
                    know_if_clauses_lemmas.append(trigger_lemma + ":" + word.lemma)
                    know_if_clause_trigger = False
                    trigger_lemma = ""
                    words_in_between = 0
                else:
                    words_in_between += 1
            else:
                know_if_clause_trigger = False
                trigger_lemma = ""
                words_in_between = 0


    # temporal adverbial clause
    temporal_adverbial_clause_count = 0
    temporal_adverbial_clause_trigger = False
    temporal_adverbial_clause_lemmas = []

    trigger_lemma = ""
    for word in words :            
        if word.upos == "ADP" and word.text == "avant" or word.text == "après":
            temporal_adverbial_clause_trigger = True
            trigger_lemma = word.lemma
        if temporal_adverbial_clause_trigger: 
            if word.upos == "NOUN" or word.upos == "VERB":
                if word.deprel == "advcl" or word.deprel == "obl":
                    temporal_adverbial_clause_count += 1
                    temporal_adverbial_clause_lemmas.append(trigger_lemma + ":" + word.text)
                else:
                    temporal_adverbial_clause_trigger = False
                    trigger_lemma = ""

    #know that clauses identification
    know_that_clauses_count = 0
    know_that_clauses_lemmas = []
    know_that_clause_trigger = False 
    know_that_not_clauses_count = 0
    know_that_not_clauses_lemmas = []
    know_that_not_clause_trigger = False 
    know_that_present_clause_count = 0
    know_that_present_clause_lemmas = []
    know_that_present_clause_trigger = False
    words_in_between = 0
    words_in_not_clause_between = 0

    know_that_clauses_trigger_lemma = ""   
    know_that_not_clauses_trigger_lemma = "" 
    know_that_present_clause_trigger_lemma = ""
    for word in words :
        if word.lemma == "savoir" or word.lemma == "croire":
            know_that_clause_trigger = True
            know_that_clauses_trigger_lemma = word.lemma     
            if word.feats is not None and "Tense=Pres" in word.feats and "Mood=Ind" in word.feats:
                know_that_present_clause_trigger = True
                know_that_present_clause_trigger_lemma = word.lemma
        elif know_that_clause_trigger:
            if word.upos != 'PUNCT' and words_in_between <= 3:
                if word.lemma == "que":
                    know_that_clauses_count += 1
                    know_that_clauses_lemmas.append(know_that_clauses_trigger_lemma + ":" + word.lemma)
                    know_that_clause_trigger = False
                    know_that_clauses_trigger_lemma = ""   
                    words_in_between = 0
                    know_that_not_clause_trigger = True 
                    know_that_not_clause_trigger_lemma = word.lemma
                    
                    if know_that_present_clause_trigger:
                        know_that_present_clause_trigger = False
                        know_that_present_clause_count += 1
                        know_that_present_clause_lemmas.append(know_that_present_clause_trigger_lemma + ":" + word.lemma)
                        know_that_present_clause_trigger_lemma = ""

                else:
                    words_in_between += 1
            else:
                know_that_clause_trigger = False
                know_that_not_clause_trigger = False  
                know_that_present_clause_trigger = False
                know_that_clauses_trigger_lemma = ""   
                know_that_not_clauses_trigger_lemma = "" 
                know_that_present_clause_trigger_lemma = ""
                words_in_between = 0                
        elif know_that_not_clause_trigger:
            if word.upos != 'PUNCT' and words_in_not_clause_between <= 5:
                if word.lemma == "ne":
                    know_that_not_clauses_count += 1
                    know_that_not_clauses_lemmas.append(know_that_not_clause_trigger_lemma + ":" + word.lemma)
                    know_that_not_clause_trigger = False
                    know_that_not_clauses_trigger_lemma = ""   
                    words_in_not_clause_between = 0
                else:
                    words_in_not_clause_between += 1
            else:
                know_that_not_clause_trigger = False 
                know_that_not_clauses_trigger_lemma = "" 
                words_in_not_clause_between = 0

    #gerund clause count
    gerund_clause_count = 0
    gerund_clause_lemmas = []

    for word in words :
        if word.upos == "VERB" and word.feats != None and "VerbForm=Part" in word.feats and word.deprel == "advcl":
            gerund_clause_count += 1
            gerund_clause_lemmas.append(word.text)

    # extracted_features = [
    #     know_that_clauses_count, 
    #     (know_that_clauses_count/sentence_count),
    #     know_that_not_clauses_count, 
    #     (know_that_not_clauses_count/sentence_count),
    #     know_that_present_clause_count,
    #     (know_that_present_clause_count/sentence_count),
    #     know_if_clauses_count, 
    #     (know_if_clauses_count/sentence_count),
    #     temporal_adverbial_clause_count, 
    #     (temporal_adverbial_clause_count/sentence_count),
    #     gerund_clause_count, 
    #     (gerund_clause_count/sentence_count)] 

    
    features_and_clause_counts = {}
    features_and_lemmas = {}

    features_and_clause_counts['subordonnees_type_savoir_que'] = know_that_clauses_count
    features_and_clause_counts['subordonnees_type_savoir_que_negation'] = know_that_not_clauses_count
    features_and_clause_counts['subordonnees_type_croire_que_present'] = know_that_present_clause_count
    features_and_clause_counts['subordonnees_type_savoir_si'] = know_if_clauses_count
    features_and_clause_counts['subordonnees_avant_apres'] = temporal_adverbial_clause_count
    features_and_clause_counts['subordonnees_gerondif'] = gerund_clause_count

    features_and_lemmas['subordonnees_type_savoir_que'] = know_that_clauses_lemmas
    features_and_lemmas['subordonnees_type_savoir_que_negation'] = know_that_not_clauses_lemmas
    features_and_lemmas['subordonnees_type_croire_que_present'] = know_that_present_clause_lemmas
    features_and_lemmas['subordonnees_type_savoir_si'] = know_if_clauses_lemmas
    features_and_lemmas['subordonnees_avant_apres'] = temporal_adverbial_clause_lemmas
    features_and_lemmas['subordonnees_gerondif'] = gerund_clause_lemmas

    # for key in features_and_lemmas:
    #     print(key, ":", features_and_clause_counts[key], ":", len(features_and_lemmas[key]), ":", features_and_lemmas[key])

    return features_list, features_and_clause_counts, features_and_lemmas





def get_propositions_subordonnees_features(doc, levels, outfile):
    dict_sent_return_strings = {}
    sentence_list = []

    # with open("/home/rahman/Downloads/propositions_subordonnees.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:       
            # print("====== Sentence Level Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")
            for idx, sentence in enumerate(doc.sentences):            
                features_list, features_and_clause_counts, features_and_lemmas = setFeatures(sentence)

                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
                sentence_list.append(sent_key)

                outfile.write(str(idx+1) + ". Sentence: " + sentence.text + "\n")

                extracted_features = [f for f in features_list if features_and_clause_counts[f] > 0]
                # print(extracted_features)
                if extracted_features:
                    return_strings.append("Features :: Feature-Counts :: Feature-Texts\n")
                    return_strings.append("--------------------------------------------\n")

                    outfile.write("Features :: Feature-Counts :: Feature-Texts\n")
                    outfile.write("--------------------------------------------\n")
                    for f in extracted_features:
                        return_strings.append(f + " :: " + str(features_and_clause_counts[f]) + " :: " + "[ " + ", ".join(set(features_and_lemmas[f])) + " ]\n")
                        outfile.write(f + " :: " + str(features_and_clause_counts[f]) + " :: " + "[ " + ", ".join(set(features_and_lemmas[f])) + " ]\n")

                else: 
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")
                
                return_strings.append("\n")
                outfile.write("\n")
                dict_sent_return_strings[sent_key] = return_strings



        if "text" in levels:
            sent_count = len(doc.sentences)
            features_list = []
            features_and_clause_counts_doc_level = {}
            features_and_lemmas_doc_level = {}

            for sentence in doc.sentences:
                features_list, features_and_clause_counts, features_and_lemmas = setFeatures(sentence)

                for f in features_list:
                    if f not in features_and_clause_counts_doc_level:
                        features_and_clause_counts_doc_level[f] = features_and_clause_counts[f]
                        features_and_lemmas_doc_level[f] = features_and_lemmas[f]
                    else:
                        features_and_clause_counts_doc_level[f] += features_and_clause_counts[f]
                        features_and_lemmas_doc_level[f] += features_and_lemmas[f]

            extracted_clause_counts_doc_level = 0
            for f in features_and_clause_counts_doc_level:
                extracted_clause_counts_doc_level += features_and_clause_counts_doc_level[f]

            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")

            if extracted_clause_counts_doc_level == 0:
                # print("No features found.\n")
                outfile.write("No features found.\n")
            else:
                # print("Features :: Feature-Counts :: Feature-Lemmas\n")
                # print("--------------------------------------------\n")

                outfile.write("Features :: Feature-Counts :: Feature-Lemmas\n")
                outfile.write("--------------------------------------------\n")
                for f in features_and_clause_counts_doc_level:
                    # print(f + " :: " + str(features_and_clause_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(features_and_lemmas_doc_level[f])) + " ]\n")
                    outfile.write(f + " :: " + str(features_and_clause_counts_doc_level[f]) + " :: " + "[ " + ", ".join(set(features_and_lemmas_doc_level[f])) + " ]\n")

                # print("Total feature counts: " + str(extracted_clause_counts_doc_level) + "\n")
                outfile.write("Total feature counts: " + str(extracted_clause_counts_doc_level) + "\n")

            # print("\n")
            outfile.write("\n")

    return sentence_list, dict_sent_return_strings

        

@register_processor(processor_name)
class SubordinateClausesProcessor(Processor):
    """ Processor that computes subordinate clauses features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                             

        return doc
