from stanza.pipeline.processor import Processor, register_processor
from stanza.models.common.doc import Document
from tools.processor_export import processor_export_format

processor_name = 'propositions_subordonnees'

features_list = [
    'nombre_subordonnees_type_savoir_que',
    'proportion_subordonnees_type_savoir_que',
    'nombre_subordonnees_type_savoir_que_negation',
    'proportion_subordonnees_type_savoir_que_negation',
    'nombre_subordonnees_type_croire_que_present',
    'proportion_subordonnees_type_croire_que_present',
    'nombre_subordonnees_type_savoir_si',
    'proportion_subordonnees_type_savoir_si',
    'nombre_subordonnees_avant_apres',
    'proportion_subordonnees_avant_apres',
    'nombre_subordonnees_gerondif',
    'proportion_subordonnees_gerondif'    
    ]

def setFeatures(input):
    words = input.words_list
    sentence_count = 0
    
    if isinstance(input, Document):
        sentence_count = len(input.sentences)
    else:
        sentence_count = 1

    # know if clause identification
    know_if_clauses_count = 0    
    know_if_clause_trigger = False 

    words_in_between = 0
    words_in_not_clause_between = 0

    for word in words :
        if word.lemma == "savoir":
            know_if_clause_trigger = True
        elif know_if_clause_trigger:
            if word.upos != 'PUNCT' and words_in_between <= 3:
                if word.lemma == "si":
                    know_if_clauses_count += 1
                    know_if_clause_trigger = False
                    words_in_between = 0
                else:
                    words_in_between += 1
            else:
                know_if_clause_trigger = False
                words_in_between = 0


    # temporal adverbial clause
    temporal_adverbial_clause_count = 0
    temporal_adverbial_clause_trigger = False

    for word in words :            
        if word.upos == "ADP" and word.text == "avant" or word.text == "après":
            temporal_adverbial_clause_trigger = True
        if temporal_adverbial_clause_trigger: 
            if word.upos == "NOUN" or word.upos == "VERB":
                if word.deprel == "advcl" or word.deprel == "obl":
                    temporal_adverbial_clause_count += 1
                else:
                    temporal_adverbial_clause_trigger = False

    #know that clauses identification
    know_that_clauses_count = 0
    know_that_clause_trigger = False 
    know_that_not_clauses_count = 0
    know_that_not_clause_trigger = False 
    know_that_present_clause_count = 0
    know_that_present_clause_trigger = False
    words_in_between = 0
    words_in_not_clause_between = 0
    
    for word in words :
        if word.lemma == "savoir" or word.lemma == "croire":
            know_that_clause_trigger = True     
            if word.feats is not None and "Tense=Pres" in word.feats and "Mood=Ind" in word.feats:
                know_that_present_clause_trigger = True
        elif know_that_clause_trigger:
            if word.upos != 'PUNCT' and words_in_between <= 3:
                if word.lemma == "que":
                    know_that_clauses_count += 1
                    know_that_clause_trigger = False
                    words_in_between = 0
                    know_that_not_clause_trigger = True 
                    
                    if know_that_present_clause_trigger:
                        know_that_present_clause_trigger = False
                        know_that_present_clause_count += 1

                else:
                    words_in_between += 1
            else:
                know_that_clause_trigger = False
                know_that_not_clause_trigger = False  
                know_that_present_clause_trigger = False
                words_in_between = 0                
        elif know_that_not_clause_trigger:
            if word.upos != 'PUNCT' and words_in_not_clause_between <= 5:
                if word.lemma == "ne":
                    know_that_not_clauses_count += 1
                    know_that_clause_trigger = False
                    words_in_not_clause_between = 0
                else:
                    words_in_not_clause_between += 1
            else:
                know_that_not_clause_trigger = False 
                words_in_not_clause_between = 0

    #gerund clause count
    gerund_clause_count = 0

    for word in words :
        if word.upos == "VERB" and word.feats != None and "VerbForm=Part" in word.feats and word.deprel == "advcl":
            gerund_clause_count += 1

    if (sentence_count == 0): sentence_count = 1
    extracted_features = [
        know_that_clauses_count, 
        (know_that_clauses_count/sentence_count),
        know_that_not_clauses_count, 
        (know_that_not_clauses_count/sentence_count),
        know_that_present_clause_count,
        (know_that_present_clause_count/sentence_count),
        know_if_clauses_count, 
        (know_if_clauses_count/sentence_count),
        temporal_adverbial_clause_count, 
        (temporal_adverbial_clause_count/sentence_count),
        gerund_clause_count, 
        (gerund_clause_count/sentence_count)]    
        
    features = zip(features_list, extracted_features)
    for f, s in features:
        setattr(input, f, s)    


@register_processor(processor_name)
class SubordinateClausesProcessor(Processor):
    """ Processor that computes subordinate clauses features at sentence and text levels """
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):

        if "text" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_texte", "text", "csv", features_list)
            setFeatures(doc)

        if "sentence" in self.levels:
            processor_export_format.add_config(doc, processor_name+"_phrase", "sentence", "csv", features_list)
            for sentence in doc.sentences:
                setFeatures(sentence)                                             

        return doc
