import os

from stanza.pipeline.processor import Processor, register_processor
from tools.processor_export import processor_export_format

from collections import defaultdict
import regex

processor_name = 'modalite'

nom_descripteurs = [
    "marqueurs",
    "alethique",
    "appreciatif",
    "axiologique",
    "boulique",
    "deontique",
    "epistemique",
    "pas_d_indication",
    "marqueurs_niveau3",
    "marqueurs_niveau2",
    "marqueurs_niveau1",
    "marqueurs_niveau0",
    "alethique_niveau3",
    "alethique_niveau2",
    "alethique_niveau1",
    "alethique_niveau0",
    "appreciatif_niveau3",
    "appreciatif_niveau2",
    "appreciatif_niveau1",
    "appreciatif_niveau0",
    "axiologique_niveau3",
    "axiologique_niveau2",
    "axiologique_niveau1",
    "axiologique_niveau0",
    "boulique_niveau3",
    "boulique_niveau2",
    "boulique_niveau1",
    "boulique_niveau0",
    "deontique_niveau3",
    "deontique_niveau2",
    "deontique_niveau1",
    "deontique_niveau0",
    "epistemique_niveau3",
    "epistemique_niveau2",
    "epistemique_niveau1",
    "epistemique_niveau0",
    "pas_d_indication_niveau3",
    "pas_d_indication_niveau2",
    "pas_d_indication_niveau1",
    "pas_d_indication_niveau0"
]
descripteurs_nombre = ["nombre_"+n for n in nom_descripteurs] # liste des descripteurs de type nombre
descripteurs_proportions = ["proportion_"+n for n in nom_descripteurs] # liste des descripteurs de type proportion
liste_descripteurs = [f for f in descripteurs_nombre+descripteurs_proportions] # tous les descripteurs

def recupLexique(chemin):
    """
    Récupère le lexique et le stocke dans un dictionnaire.
    Entrée :
    - STR(chemin du lexique)
    Sortie :
    - DICT{
        STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Confiance):LIST[STR(niveau de confiance, 3, 2, 1 ou 0)]
                                                }
    }
    """
    dico_marqueur_infos = defaultdict(lambda:dict())
    with open(chemin,'r',encoding="utf-8") as entrant:
        entrant.readline() # on élimine la ligne avec les en-têtes
        for ligne in entrant: # pour chaque marqueur de modalité
            elements = ligne.strip().split("\t")
            dico_marqueur_infos[elements[0]]["Cat_modale"]=[e for e in elements[1].split(";")] # peut y avoir plusieurs cat modales
            dico_marqueur_infos[elements[0]]["Confiance"]=[e for e in elements[6].split(";")] # normalement, un seul niveau de confiance
    return dico_marqueur_infos
   
def compteMarqueurs(liste_lemmes,dico_marqueur_infos,dico_descripteur_occurrences):
    """
    Repère et classe les marqueurs de modalité présents dans une phrase
    Entrée :
    - LIST[STR(lemme1),STR(lemme2)]
    - DICT{
        STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Confiance):LIST[STR(niveau de confiance, 3, 2, 1 ou 0)]
                                                }
    }
    - dico_descripteur_occurrences de la forme
    DICT{
        STR(nom du descripteur) : INT(nb d'occurrence de la catégorie nécessaire pour calculer le descripteur)
    }
    Sortie :
    - dico_descripteur_occurrences complété
    """
    for lemme in liste_lemmes: # pour chaque lemme
        if lemme in dico_marqueur_infos: # si le lemme est dans notre lexique
            dico_descripteur_occurrences["marqueurs"]+=1 # +1 marqueur de modalité reconnu
            
            # Catégories modales - reconnaissance générale
            for c in dico_marqueur_infos[lemme]["Cat_modale"]: # pour chaque catégorie modale associée au marqueur
                dico_descripteur_occurrences[c]+=1 # +1 marqueur de cette cat modale reconnu
                # catégorie modale - par niveau de confiance
                for n in dico_marqueur_infos[lemme]["Confiance"]:
                    dico_descripteur_occurrences[c+"_niveau"+n]+=1
            
            # Niveau de confiance - reconnaissance générale
            for n in dico_marqueur_infos[lemme]["Confiance"]: # pour chaque niveau de confiance - normalement un seul
                dico_descripteur_occurrences["marqueurs_niveau"+n]+=1 # +1 marqueur de ce niveau de confiance reconnu
        
    return dico_descripteur_occurrences
    
def calculeDescripteurs(liste_lemmes,dico_descripteur_occurrences,dico_descripteurs):
    """
    Calcule les descripteurs qu'on veut que la chaîne retourne. Donc des nombres d'occurrence et des proportions.
    Entrée :
    - la liste de lemmes dans lesquels on a fait le repérage de marqueurs de modalité
    - dico_descripteur_occurrences
    - dico_descripteurs
    Sortie :
    - dico_descripteurs complété
    """
    for nom_descripteur in dico_descripteur_occurrences:
        # Descripteurs de type nombre : la valeur = le nb d'occurrences
        dico_descripteurs["nombre_"+nom_descripteur] = dico_descripteur_occurrences[nom_descripteur]
        # Descripteurs de type proportion : il faut calculer les proportions
        if (nom_descripteur=="marqueurs" or
           nom_descripteur=="alethique" or
           nom_descripteur=="appreciatif" or
           nom_descripteur=="axiologique" or
           nom_descripteur=="boulique" or
           nom_descripteur=="deontique" or
           nom_descripteur=="epistemique" or
           nom_descripteur=="pas_d_indication"): # pour ces catégories générales, on calcule par rapport au nb de mots total
            dico_descripteurs["proportion_"+nom_descripteur]=(dico_descripteur_occurrences[nom_descripteur]/len(liste_lemmes)) if len(liste_lemmes) > 0 else 0
        else: # pour les autres, on calcule par rapport au nb de marqueurs repérés dans la catégorie
            m=regex.match("(.+)_(niveau\d)",nom_descripteur)
            cat=m.group(1)
            niveau=m.group(2)
            try:
                dico_descripteurs["proportion_"+nom_descripteur]=dico_descripteur_occurrences[nom_descripteur]/dico_descripteur_occurrences[cat]
            except ZeroDivisionError:
                dico_descripteurs["proportion_"+nom_descripteur]=float(0)
    return dico_descripteurs

def creeListeLemmes(phraseStanza):
    """
    /!\ ne fonctionne qu'avec un objet de type Sentence issu de `doc.sentences` où doc est un texte parsé par Stanza
    Pour une phrase Stanza donnée,
    renvoie une liste de liste de lemme.
    """
    liste_lemmes = list()
    phrase = phraseStanza.to_dict() # on convertit l'objet Sentence pour pouvoir itérer dessus
    for i in range(len(phrase)): # pour chaque mot de la phrase
        l = phrase[i].get('lemma', phrase[i].get('text')) # on récupère le lemme
        liste_lemmes.append(l)
    return liste_lemmes
    
def analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs):
    """
    Pour une liste de lemmes donnée, analyse la modalité.
    Entrée :
    - LIST[STR(lemme1),STR(lemme2)]
    - lexique de marqueurs de modalité
        DICT{
            STR(lemme du marqueur de modalité) : DICT{
                                               STR(Cat_modale):LIST[STR(categorie1),STR(categorie2)],
                                               STR(Confiance):LIST[STR(niveau de confiance, 3, 2, 1 ou 0)]
                                                }
        }
    - LIST[STR(nom brut du descripteur 1),STR(nom brut du descripteur 2)]
    - LIST[STR(nombre/proportion+nom brut du descripteur 1)),etc.]
    Sortie :
    - DICT{STR(nom du descripteur):FLOAT(la valeur du descripteur)}
    """   
    # Étape 1 : création des dictionnaires pour accueillir les descripteurs
    dico_descripteur_occurrences = dict.fromkeys(nom_descripteurs , 0) # nb d'occurrences
    dico_descripteurs = dict.fromkeys(liste_descripteurs , 0) # descripteurs en tant que tel
    
    # Étape 2 : repérage des marqueurs de modalité --> on compte les occurrences de chaque catégorie
    dico_descripteur_occurrences = compteMarqueurs(liste_lemmes,dico_lexique,dico_descripteur_occurrences)
    
    # Étape 3 : calculer les descripteurs en tant que tels
    dico_descripteurs = calculeDescripteurs(liste_lemmes,dico_descripteur_occurrences,dico_descripteurs)
    
    return dico_descripteurs
    
def set_modality_features(docStanza,levels):
    """                      
    Entrée :                 
    - doc Stanza, au minimum tokenisé et lemmatisé
    - levels : text and/or sentence
    Sortie :
    - rien
    """
    # Étape 1 : récupération du lexique de marqueurs de modalité
    chemin_lexique = "processor/semantique/lexique_marqueurs_modalite_harmonise_sans_doublons.tsv"
    dico_lexique = recupLexique(chemin_lexique)
    
    # Étape 2 : on calcule les descripteurs à partir du document tokenisé et lemmatisé
    # Échelle d'extraction = texte
    if "text" in levels:
        processor_export_format.add_config(docStanza, processor_name+"_texte", "text", "csv", liste_descripteurs)
        # on met tous les lemmes du texte dans une seule liste
        liste_lemmes = [l for phrase in docStanza.sentences for l in creeListeLemmes(phrase)]
        # on calcule les descripteurs de modalité
        dico_descripteurs=analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs)
        ### /!\ c'est là qu'on écrit les descripteurs ??? peut-être à modifier...
        for descripteur,valeur in dico_descripteurs.items():
            setattr(docStanza,descripteur,valeur)
    
    # Échelle d'extraction = phrase
    if "sentence" in levels:
        processor_export_format.add_config(docStanza, processor_name+"_phrase", "sentence", "csv", liste_descripteurs)
        for phrase in docStanza.sentences:
            # on traite les phrases les unes après les autres
            liste_lemmes = creeListeLemmes(phrase)
            # on calcule les descripteurs de modalité
            dico_descripteurs=analyseModalite(liste_lemmes,dico_lexique,nom_descripteurs,liste_descripteurs)
            ### /!\ c'est là qu'on écrit les descripteurs ??? peut-être à modifier...
            for descripteur,valeur in dico_descripteurs.items():
                setattr(docStanza,descripteur,valeur)


@register_processor(processor_name)
class ModaliteProcessor(Processor):
    """ Processor that computes modality features at sentence and text levels """
    #_requires = {'tokenize'} ###/!\ est-ce qu'il faut ajouter ça ?? aussi qqc pour indique qu'il faut que le texte soit lemmatisé ??
    _provides = {processor_name}

    def __init__(self, device, config, pipeline):
        if "analysis_level" in pipeline.kwargs:
            self.levels = pipeline.kwargs["analysis_level"].split(",")
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, docStanza):

        set_modality_features(docStanza,self.levels)
        
        return docStanza

