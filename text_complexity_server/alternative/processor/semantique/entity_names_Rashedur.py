
from stanza.models.common.doc import Document


def get_entity_names_features(doc, levels, outfile):
    dict_sent_return_strings = {}
    sentence_list = []

    # with open("/home/rahman/Downloads/ent_names.txt", "a") as outfile:
    with open(outfile, "a") as outfile:
        if "sentence" in levels:
            # print("====== Sentence Level Features ======\n\n")
            outfile.write("====== Sentence Level Features ======\n\n")

            for idx, sentence in enumerate(doc.sentences):                
                return_strings = []
                sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
                sentence_list.append(sent_key)

                outfile.write(str(idx+1) + ". Sentence: " + sentence.text + "\n")

                sent_ents = []
                for ent in sentence.ents:  
                    ent_txt = ent.text
                    ent_typ = ent.type

                    if ent_txt == "Les" and ent_typ == "LOC": continue

                    sent_ents.append(ent)

                if sent_ents: 
                    return_strings.append("Entity Name (Type) :: " + ent.text + " (" + ent.type + ")" + "\n")
                    outfile.write("Entity Name (Type) :: " + ent.text + " (" + ent.type + ")" + "\n")
                else:
                    return_strings.append("No features found.\n")
                    outfile.write("No features found.\n")

                return_strings.append("\n")
                outfile.write("\n")
                dict_sent_return_strings[sent_key] = return_strings


        if "text" in levels: 
            # print("\n====== Text Level Features ======\n\n")
            outfile.write("\n====== Text Level Features ======\n\n")

            doc_ents = []
            for ent in doc.ents:
                ent_txt = ent.text
                ent_typ = ent.type

                if ent_txt == "Les" and ent_typ == "LOC": continue

                doc_ents.append(ent)

            if doc_ents: 
                # print("Entity Name (Entity Type)\n")
                # print("-------------------------\n")
                outfile.write("Entity Name (Entity Type)\n")
                outfile.write("-------------------------\n")
                for ent in doc_ents: 
                    # print(ent.text + " (" + ent.type + ")\n")
                    outfile.write(ent.text + " (" + ent.type + ")\n")

                # print("Total entitiy counts: " + str(len(doc_ents)) + "\n")
                outfile.write("Total entitiy counts: " + str(len(doc_ents)) + "\n")
            else:
                # print("No features found.\n")
                outfile.write("No features found.\n")
            
            # print("\n")
            outfile.write("\n")
    
    return sentence_list, dict_sent_return_strings