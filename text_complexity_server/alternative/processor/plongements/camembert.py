import torch
from stanza.pipeline.processor import Processor, register_processor
from transformers import CamembertModel, CamembertTokenizer
from tools.processor_export import processor_export_format


def get_embedding(sentence):
    """
    Compute a distributed representation of a text in the sentence.
    :param sentence: str
    :return: list
    """
    tokenizer = CamembertTokenizer.from_pretrained("camembert-base")
    camembert = CamembertModel.from_pretrained("camembert-base")
    camembert.eval()  # disable dropout (or leave in train mode to finetune)

    # 1-hot encode and add special starting and end tokens
    encoded_sentence = tokenizer.encode(sentence)

    # Feed tokens to Camembert as a torch tensor (batch dim 1)
    encoded_sentence = torch.tensor(encoded_sentence).unsqueeze(0)
    embeddings, _ = camembert(encoded_sentence)

    # mean pooling:
    vec = embeddings.mean(1)
    return vec.tolist()[0]


@register_processor("camembert")
class EmbeddingsProcessor(Processor):
    """ Processor that compute plongements features for a sequence """
    _provides = {'camembert'}
    OVERRIDE = True

    def __init__(self, device, config, pipeline):
        pass

    def _set_up_model(self, *args):
        pass

    def process(self, doc):
        processor_export_format.add_config(doc, "camembert_phrase", "sentence", "csv", ["embeddings"])

        for sentence in doc.sentences:
            setattr(sentence, "embeddings", get_embedding(sentence.text))

        return doc
