import stanza
import os
import sys
import shutil

from pathlib import Path

def start_feature_extraction(input_dir, output_dir, levels, choice_processors, analysis_pipeline,
        #entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Rashedur, propositions_subordonnees_Rashedur):
        entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Aline, propositions_subordonnees_Rashedur, adverbiaux_temporels_Aline, emotyc_alternative):
    input_files = os.listdir(input_dir)
    for f in input_files:
        primary_out_dir = output_dir + f.replace(".txt", "") + "/"
        file_feature_extraction(input_dir, f, primary_out_dir, levels, choice_processors, analysis_pipeline,
            #entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Rashedur, propositions_subordonnees_Rashedur)
            entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Aline, propositions_subordonnees_Rashedur, adverbiaux_temporels_Aline, emotyc_alternative)

def file_feature_extraction(input_dir, input_file, primary_out_dir, levels, choice_processors, analysis_pipeline,
        #entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Rashedur, propositions_subordonnees_Rashedur):
        entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Aline, propositions_subordonnees_Rashedur, adverbiaux_temporels_Aline, emotyc_alternative):
    text = ""
    print("file_feature_extraction: " + os.getcwd())
    with open(input_dir + input_file, "r") as myfile:
        text = myfile.read()

    if len(text) == 0: return False

    print("********** Stanza **********")
    text = text.replace("\\n","\n")
    doc = analysis_pipeline(text)

    sent_output_dir = primary_out_dir + "sentence_level_all_features/"

    if os.path.exists(primary_out_dir):
        # print(primary_out_dir + " TREE NOT YET REMOVED")
        shutil.rmtree(primary_out_dir)
    os.makedirs(primary_out_dir)
    os.makedirs(sent_output_dir)

    #print("output dir: " + sent_output_dir)
    #print("input file: " + input_file)

    if ("entity_names" in choice_processors):
        ##### Entity Names #####
        print("********** Entity Names **********")
        sentence_list_EN, dict_sent_return_strings_EN = entity_names_Rashedur.get_entity_names_features(doc, levels, primary_out_dir + "entity_names_features.txt")

    if ("emotions" in choice_processors):
        ##### Emotions #####
        print("********** Emotions **********")
        sentence_list_EM, dict_sent_return_strings_EM = emotions_Rashedur.get_emotion_features(doc, levels, primary_out_dir + "emotions_features.txt")

    if ("modality" in choice_processors):
        ##### Modalite #####
        print("********** Modality **********")
        sentence_list_MD, dict_sent_return_strings_MD = modalite_Rashedur.get_modality_features(doc, levels, primary_out_dir + "modality_features.txt")

    if ("metaphores" in choice_processors):
        ##### Metaphores #####  bug: re.findall;  needs an appropriate text to check 
        print("********** Metaphores **********")
        sentence_list_MP, dict_sent_return_strings_MP = metaphores_Rashedur.get_metaphore_features(doc, levels, primary_out_dir + "metaphores_features.txt")

    if ("connecteurs_organisateurs" in choice_processors):
        ##### Connecteurs Organisateurs ##### bug: re.findall; 
        print("********** Connecteurs Organisateurs **********")
        #sentence_list_CO, dict_sent_return_strings_CO = connecteurs_organisateurs_Rashedur.get_connector_organizers_features(doc, levels, primary_out_dir + "connecteurs_organisateurs_features.txt")
        sentence_list_CO, dict_sent_return_strings_CO = connecteurs_organisateurs_Aline.get_connector_organizers_features(doc, levels, primary_out_dir + "connecteurs_organisateurs_features.txt")

    if ("propositions_subordonnees" in choice_processors):
        ##### Propositions Subordonnees ##### needs an appropriate text to check 
        print("********** Propositions Subordonnees **********")
        sentence_list_PS, dict_sent_return_strings_PS = propositions_subordonnees_Rashedur.get_propositions_subordonnees_features(doc, levels, primary_out_dir + "propositions_subordonnees_features.txt")
        
    if ("adverbiaux_temporels" in choice_processors):
        ##### Adverbiaux temporels ##### 
        print("********** Adverbiaux temporels **********")
        sentence_list_AT, dict_sent_return_strings_AT = adverbiaux_temporels_Aline.get_adverbial_features(doc, levels, primary_out_dir + "adverbiaux_temporels_features.txt")
        
    if ("emotyc" in choice_processors):
        ##### EMoTyC ##### 
        print("********** EMoTyC **********")
        sentence_list_EM, dict_sent_return_strings_EM = emotyc_alternative.genereDescripteurs(doc, levels, primary_out_dir + "emotyc.txt")

    if "sentence" in levels:
        sentence_list = []

        for idx, sentence in enumerate(doc.sentences):
            return_strings = []
            sent_key = str(idx+1) + ". Sentence: " + sentence.text + "\n"
            sentence_list.append(sent_key)

        for idx, sent in enumerate(sentence_list):
            outfile = sent_output_dir + str(idx+1) + ".txt"
            with open(outfile, "a") as outfile:

                if ("entity_names" in choice_processors):
                    outfile.write("\n********** Entity Names Features **********\n")
                    outfile.write("*******************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_EN[sent]
                    outfile.writelines(output_strings)

                if ("emotions" in choice_processors):
                    outfile.write("\n********** Emotions Features **********\n")
                    outfile.write("***************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_EM[sent]
                    outfile.writelines(output_strings)

                if ("modality" in choice_processors):
                    outfile.write("\n********** Modality Features **********\n")
                    outfile.write("***************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_MD[sent]
                    outfile.writelines(output_strings)

                if ("metaphores" in choice_processors):
                    outfile.write("\n********** Metaphores Features **********\n")
                    outfile.write("*****************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_MP[sent]
                    outfile.writelines(output_strings)

                if ("connecteurs_organisateurs" in choice_processors):
                    outfile.write("\n********** Connecteurs Organisateurs Features **********\n")
                    outfile.write("********************************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_CO[sent]
                    outfile.writelines(output_strings)

                if ("propositions_subordonnees" in choice_processors):
                    outfile.write("\n********** Propositions Subordonnees Features **********\n")
                    outfile.write("********************************************************\n\n")
                    outfile.write(sent)
                    output_strings = dict_sent_return_strings_PS[sent]
                    outfile.writelines(output_strings)
                
                if ("adverbiaux_temporels" in choice_processors):
                    outfile.write("\n********** Adverbiaux Temporels Features **********\n")
                    outfile.write("********************************************************\n\n")
                    outfile.write(sent)
                    # print(str(dict_sent_return_strings_AT))
                    output_strings = dict_sent_return_strings_AT[sent]
                    outfile.writelines(output_strings)
                    
                if ("emotyc" in choice_processors):
                    outfile.write("\n********** EmoTyC Features **********\n")
                    outfile.write("********************************************************\n\n")
                    outfile.write(sent)
                    #print(str(dict_sent_return_strings_AT))
                    output_strings = dict_sent_return_strings_EM[sent]
                    outfile.writelines(output_strings)
                    
    return True

def alternative(arg1, arg2, arg3, arg4, arg5, choice_levels, choice_processors, nozip=False):
    # print("ALTERNATIVE: " + arg1 + " " + arg2 + " " + arg3 + " " + arg4 + " " + arg5 + " " + ','.join(choice_levels) + " " + ','.join(choice_processors))
    prev_dir = os.getcwd()
    # print("Calling dir is: " + prev_dir)

    if arg1 != "":
        os.chdir(arg1)
        print("Working dir is now: " + os.getcwd())

    # sys.path.insert(1, "../../")
    sys.path.insert(1, "../")

    from processor.util import words_util

    '''
    from processor.graphie_typographie import graphie
    from processor.lexique import a_tester_diversite_lexicale, niveau_lexical, richesse_lexicale, adjectifs_ordinaux
    from processor.morphosyntaxe import flexions_verbales, parties_du_discours, pronoms, pluriels
    from processor.phonetique_phonologie import phonetique
    from processor.lisibilite import formules_lisibilite_standard
    from processor.semantique import adverbiaux_temporels, emotions, connecteurs_organisateurs, entites_nommees, propositions_subordonnees, metaphores, modalite
    from processor.syntaxe import dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite 
    '''

    from stanza.pipeline.processor import Processor, register_processor
    from stanza.models.common.doc import Document

    from processor.semantique import entity_names_Rashedur
    from processor.semantique import emotions_Rashedur
    from processor.semantique import modalite_Rashedur
    from processor.semantique import metaphores_Rashedur
    #from processor.semantique import connecteurs_organisateurs_Rashedur
    from processor.semantique import connecteurs_organisateurs_Aline
    from processor.semantique import propositions_subordonnees_Rashedur
    from processor.semantique import adverbiaux_temporels_Aline
    from processor.semantique import emotyc_alternative

    processors = 'tokenize,mwt,pos,lemma,depparse,words_util'

    ner_processors = True # True / False

    if ner_processors:
        processors += ",ner"   
    print(f"Extracting : {processors}")

    levels = ','.join(choice_levels)
    if (levels == None or levels == ""):
        levels = "text,sentence"
    print(f"Analysis levels : {levels}")


    # analysis_pipeline = stanza.Pipeline('fr',
    #                                  processors=processors,
    #                                  use_gpu=False,
    #                                  pos_batch_size=3000, 
    #                                  analysis_level=levels)
    from stanza.pipeline.core import DownloadMethod
    analysis_pipeline = stanza.Pipeline('fr', processors=processors, analysis_level=levels, download_method = None, use_gpu=False)

    if arg2 == "":
        input_dir = "data/input/"
        output_dir = "data/output/"
        start_feature_extraction(input_dir, output_dir, levels, choice_processors, analysis_pipeline,
            #entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Rashedur, propositions_subordonnees_Rashedur)
            entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Aline, propositions_subordonnees_Rashedur, adverbiaux_temporels_Aline, emotyc_alternative)
        return 0
    else:
        input_dir = arg2
        output_dir = arg3
        retval = file_feature_extraction(input_dir, arg4, output_dir + "temporary_output/", levels, choice_processors, analysis_pipeline,
            #entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Rashedur, propositions_subordonnees_Rashedur)
            entity_names_Rashedur, emotions_Rashedur, modalite_Rashedur, metaphores_Rashedur, connecteurs_organisateurs_Aline, propositions_subordonnees_Rashedur, adverbiaux_temporels_Aline, emotyc_alternative)
        if retval == False:
            os.chdir(prev_dir)
            return 1
        
        import pathlib
        if nozip == True:
            directory_tmp = pathlib.Path(output_dir + "temporary_output")
            directory_target = pathlib.Path(output_dir + arg5)
            print(str(directory_tmp) + " ... " + str(directory_target))
            shutil.move(directory_tmp, directory_target)
        else:
            import zipfile
            directory = pathlib.Path(output_dir + "temporary_output")
            with zipfile.ZipFile(output_dir + arg5, mode="w") as archive:
                for file_path in directory.rglob("*"):
                    archive.write(
                        file_path,
                        arcname=file_path.relative_to(directory)
                    )
                archive.close()
        os.chdir(prev_dir)
        return 0

if __name__ == "__main__":
    print("Called from main command")
    print("arg1: dossier ou se situe la commande")
    print("arg2: dossier input")
    print("arg3: dossier output")
    print("arg4: fichier entree")
    print("arg5: fichier sortie zip")
    print("arg6: levels")
    print("arg7: processeurs")
    if len(sys.argv) < 2:
        sys.exit(alternative("", "", "", "", [], []))
    elif len(sys.argv) < 3:
        sys.exit(alternative(sys.argv[1], "", "", "", [], []))
    elif len(sys.argv) < 7:
        sys.exit(alternative(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], [], []))
    else:
        sys.exit(alternative(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6].split(","), sys.argv[7].split(",")))
