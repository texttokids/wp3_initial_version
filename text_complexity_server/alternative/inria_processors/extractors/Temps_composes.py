import os
import csv

# Les temps simples
Indicatif_Present = {"nom": "ind-pst", "isComposed": False, "mode": "indicatif", "syst_temp": "present"}
Indicatif_Passe = {"nom": "ind-ps", "isComposed": False, "mode": "indicatif", "syst_temp": "passe"}
Indicatif_Futur = {"nom": "ind-fut", "isComposed": False, "mode": "indicatif", "syst_temp": "futur"}
Indicatif_Imparfait = {"nom": "ind-impft", "isComposed": False, "mode": "indicatif", "syst_temp": "passe"}
Subjonctif_Present = {"nom": "subj-pst", "isComposed": False, "mode": "subjonctif", "syst_temp": "present"}
Conditionnel_present = {"nom": "ind-cond", "isComposed": False, "mode": "indicatif", "syst_temp": "futur"}
Infinitif = {"nom": "inf-", "isComposed": False, "mode": "infinitif", "syst_temp": "present"}
# Les temps composés
Indicatif_Passe_compose = {"nom": 'ind-passe_comp', "isComposed": True, "mode": "indicatif", "syst_temp": "passe"}
Indicatif_Passe_anterieur = {"nom": 'ind-passe_ant', "isComposed": True, "mode": "indicatif", "syst_temp": "passe"}
Indicatif_Futur_anterieur = {"nom": 'ind-fut_ant', "isComposed": True, "mode": "indicatif", "syst_temp": "futur"}
Indicatif_Plus_Que_Parfait = {"nom": "ind-plus_que_parf", "isComposed": True, "mode": "indicatif", "syst_temp": "passe"}
Subjonctif_Passe = {"nom": "subj-passe", "isComposed": True, "mode": "subjonctif", "syst_temp": "passe"}
Conditionnel_Passe = {"nom": "cond-passe", "isComposed": True, "mode": "Conditionnel", "syst_temp": "passe"}
Infinitif_Passe = {"nom": "inf_passe", "isComposed": True, "mode": "infinitif", "syst_temp": "passe"}

Liste_des_Temps = [Indicatif_Present, Indicatif_Passe, Indicatif_Futur, Indicatif_Imparfait, Subjonctif_Present,
                   Conditionnel_present, Infinitif,
                   Indicatif_Passe_compose, Indicatif_Passe_anterieur, Indicatif_Futur_anterieur,
                   Indicatif_Plus_Que_Parfait, Subjonctif_Passe, Conditionnel_Passe, Infinitif_Passe
                   ]

def TranscriptionTemps(mode_temps):
    switcher = {
        'Ind-Pres': 'ind-pst',
        'Ind-Past': 'ind-ps',
        'Ind-Fut': 'ind-fut',
        'Ind-Imp': 'ind-impft',
        'Subj-Pres': 'subj-pst',
        'Cnd-Pres': 'ind-cond',
        'Inf': 'inf-'
    }
    return switcher.get(mode_temps)

def TempsAux2TempsComp(tempsAUX):  # A chaque temps simple correspond un temps composé
    switcher = {
        'Ind-Pres': 'ind-passe_comp',
        'Ind-Past': 'ind-passe_ant',
        'Ind-Fut': 'ind-fut_ant',
        'Ind-Imp': 'ind-plus_que_parf',
        'Subj-Imp': 'subj-passe',
        'Cnd-Pres': 'cond-passe',
        'Inf': 'inf_passe'
    }
    return switcher.get(tempsAUX)


def Reconnaissance_temps(phrase):
    Liste_verbes = []
    Liste_temps = []
    Auxiliaires = []
    

    for i in range(len(phrase)):  # Listons les verbes
        mot = phrase[i]
        if (mot.get("upos") == "VERB" or mot.get("upos") == "AUX") and mot.get('feats') is not None : # & mot.get("deprel") == "cop"
            Liste_verbes += [mot]


    for j in range(len(Liste_verbes)):
        verbe = Liste_verbes[j]
        d_feats = dict([(e.split('=')[0], e.split('=')[1]) for e in verbe.get('feats').split('|')])

        if verbe.get("deprel") == "aux:tense":#'aux' in verbe.get("deprel"):  # == 'aux_tps':
            Auxiliaires += [verbe]
        elif verbe.get('feats'):
            if 'VerbForm' in d_feats and d_feats.get('VerbForm')!= 'Part': # On ne compte pas les participe des temps composes #mode_temps != 'part-past':
                mode = ""
                temps = ""
                
                if d_feats.get('VerbForm') == 'Inf':  # Ajout des temps simples  VINF
                    mode_temps = "Inf"
                else:
                    if 'Mood' in d_feats:
                        mode = d_feats.get('Mood')  
                    if 'Tense' in d_feats: 
                        temps = d_feats.get('Tense') 
                
                    mode_temps = mode + "-" + temps

                Liste_temps += [TranscriptionTemps(mode_temps)]
    for k in range(len(Auxiliaires)):
        auxiliaire = Auxiliaires[k]
        d_feats = dict([(e.split('=')[0], e.split('=')[1]) for e in auxiliaire.get('feats').split('|')])

        if 'VerbForm' in d_feats:
            if d_feats.get('VerbForm')!= 'Inf':  # Ajout des temps composés  VINF #verbe.get('xpos') != 'INF'
                mode = ""
                temps = ""

                if 'Mood' in d_feats:
                    mode = d_feats.get('Mood')     
                if 'Tense' in d_feats:  
                    temps = d_feats.get('Tense')  

                mode_temps = mode + "-" + temps

                Liste_temps += [TempsAux2TempsComp(mode_temps)]  # traduction du temps de l'auxiliaire en un temps composé
            else:
                Liste_temps += ["inf_passe"] 

    return Liste_verbes, Liste_temps, Auxiliaires


def DiversiteDesTemps(phrase):
    listeTemps = Reconnaissance_temps(phrase)[1]
    listeTempsDiff = []
    for i in range(len(listeTemps)):
        if listeTemps[i] not in listeTempsDiff:
            listeTempsDiff += [listeTemps[i]]
    return listeTempsDiff


def ProportionTemps(phrase):
    listeTemps = Reconnaissance_temps(phrase)[1]
    listeTempsDiff = DiversiteDesTemps(phrase)
    listeTempsProp = []
    # print(len(listeTempsDiff))
    for i in range(len(listeTempsDiff)):
        CurrentTemp = listeTempsDiff[i]
        CurrentTempProp = 0
        for j in range(len(listeTemps)):
            if listeTemps[j] == CurrentTemp:
                CurrentTempProp += 1
                CurrentTempPercent = CurrentTempProp / len(listeTemps) * 100
        listeTempsProp += [[CurrentTemp, CurrentTempPercent]]
    return listeTempsProp


def DiversiteDesSysTemp(phrase):
    
    listeTemps = Reconnaissance_temps(phrase)[1]
    listeSysTempsDiff = []
    for i in range(len(listeTemps)):
        for j in range(len(Liste_des_Temps)):
            if listeTemps[i] == Liste_des_Temps[j].get('nom'):
                if Liste_des_Temps[j].get('syst_temp') not in listeSysTempsDiff:
                    listeSysTempsDiff += [Liste_des_Temps[j].get('syst_temp')]
    return listeSysTempsDiff


def ProportionSysTemp(phrase):
    listeTemps = Reconnaissance_temps(phrase)[1]
    SysTempDiff = DiversiteDesSysTemp(phrase)
    listeSysTempProp = []
    # print(len(listeTempsDiff))
    for i in range(len(SysTempDiff)):
        CurrentSysTemp = SysTempDiff[i]
        CurrentSysTempProp = 0
        for k in range(len(Liste_des_Temps)):
            if CurrentSysTemp == Liste_des_Temps[k].get('syst_temp'):
                for j in range(len(listeTemps)):
                    if listeTemps[j] == Liste_des_Temps[k].get('nom'):
                        CurrentSysTempProp += 1
                        CurrentSysTempPercent = (CurrentSysTempProp / len(listeTemps)) * 100
        listeSysTempProp += [[CurrentSysTemp, CurrentSysTempPercent]]
    return listeSysTempProp


def ProportionTempsSimplesOuComposes(phrase):
    NbSimpl = 0
    NbCompo = 0
    listeTemps = Reconnaissance_temps(phrase)[1]

    for i in range(len(listeTemps)):
        CurrentTemps = listeTemps[i]
        for j in range(len(Liste_des_Temps)):
            if CurrentTemps == (Liste_des_Temps[j]).get('nom'):
                if Liste_des_Temps[j].get('isComposed'):
                    NbCompo += 1
                else:
                    NbSimpl += 1
    if len(listeTemps) != 0:
        PropoSimpl = (NbSimpl / len(listeTemps)) * 100
        PropoCompo = (NbCompo / len(listeTemps)) * 100
    else:
        PropoSimpl = 0
        PropoCompo = 0
    return [PropoSimpl, PropoCompo]


def ProportionModes(phrase):
    NbInd = 0
    NbSubj = 0
    NbInf = 0
    listeTemps = Reconnaissance_temps(phrase)[1]
    for i in range(len(listeTemps)):
        CurrentTemps = listeTemps[i]
        for j in range(len(Liste_des_Temps)):
            if CurrentTemps == (Liste_des_Temps[j]).get('nom'):
                if Liste_des_Temps[j].get('mode') == 'indicatif':
                    NbInd += 1
                else:
                    if Liste_des_Temps[j].get('mode') == 'subjonctif':
                        NbSubj += 1
                    else:
                        if Liste_des_Temps[j].get('mode') == 'infinitif':
                            NbInf += 1
    if len(listeTemps) != 0:
        PropoInd = (NbInd / len(listeTemps)) * 100
        PropoSubj = (NbSubj / len(listeTemps)) * 100
        PropoInf = (NbInf / len(listeTemps)) * 100

    else:
        PropoInd = 0
        PropoSubj = 0
        PropoInf = 0
    return [PropoInd, PropoSubj, PropoInf]


def matchingPropsTemps(listTemps):
    listTempsNames = ['ind-pst', 'ind-ps', 'ind-fut', 'ind-impft', 'subj-pst', 'ind-cond', 'inf-',
                      'ind-passe_comp', 'ind-passe_ant', 'ind-fut_ant', 'ind-plus_que_parf', 'subj-passe', 'cond-passe',
                      'inf_passe']
    MatchedListPhrase = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(listTemps)):
        CurrentPhraseTemps = listTemps[i]
        for j in range(len(listTempsNames)):
            if CurrentPhraseTemps[0] == listTempsNames[j]:
                MatchedListPhrase[j] = CurrentPhraseTemps[1]
    return MatchedListPhrase


def matchingPropsSystTemps(listTemps):
    listTempsNames = ['passe', 'present', 'futur']
    MatchedListPhrase = [0, 0, 0]
    for i in range(len(listTemps)):
        CurrentPhraseTemps = listTemps[i]
        for j in range(len(listTempsNames)):
            if CurrentPhraseTemps[0] == listTempsNames[j]:
                MatchedListPhrase[j] = CurrentPhraseTemps[1]
    return MatchedListPhrase


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ['DiversiteDesTempsVerbaux'] + ['Proportion ind-pst', 'Proportion ind-ps',
                                                          'Proportion ind-fut', 'Proportion ind-impft',
                                                          'Proportion subj-pst', 'Proportion ind-cond',
                                                          'Proportion inf-',
                                                          'Proportion ind-passe_comp', 'Proportion ind-passe_ant',
                                                          'Proportion ind-fut_ant', 'Proportion ind-plus_que_parf',
                                                          'Proportion subj-passe', 'Proportion cond-passe',
                                                          'Proportion inf_passe'] + ['DiversiteDesSystTemps'] + [
                              'passe', 'present', 'futur'] + ['Proportion_temps_simple', 'Proportion_temps_compose'] + [
                              'Proportion_indicatif', 'Proportion_subjonctif', 'Proportion_infinitif']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [[len(DiversiteDesTemps(phrase))] + matchingPropsTemps(ProportionTemps(phrase)) + [
            len(DiversiteDesSysTemp(phrase))] + matchingPropsSystTemps(
            ProportionSysTemp(phrase)) + ProportionTempsSimplesOuComposes(phrase) + ProportionModes(phrase)]
    if 'Temps_verbaux.csv' not in os.listdir(os.getcwd()):
        with open('Temps_verbaux.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('Temps_verbaux.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for j in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[j])
    return 'Temps_verbaux.csv'

