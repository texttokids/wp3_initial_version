import os
import csv



def ListageDesConjugaisons(phrase):
    Personnes = []
    Nombres = []
    for j in range(len(phrase)):
        mot = (phrase[j])
        if (mot.get("upos") == "VERB" or mot.get("upos") == "AUX") and "feats" in mot:  # mot.get("feats"):  # upostag, VERB

            d_feats = dict([(e.split('=')[0], e.split('=')[1]) for e in mot.get("feats").split('|')])

            nombre = d_feats.get("Number")  # n
            personne = d_feats.get("Person")  # p
            if personne != None:
                Personnes.append(personne)
            if nombre != None:
                Nombres.append(nombre)

    return [Personnes, Nombres]


def ProportionPersonnes(phrase):
    listePersonnes = ListageDesConjugaisons(phrase)[0]
    listePersonnesDiff = ['1', '2', '3']
    listePersonnesProp = []
    for i in range(len(listePersonnesDiff)):
        CurrentPersonnes = listePersonnesDiff[i]
        CurrentPersonnesTotal = 0
        for j in range(len(listePersonnes)):
            if listePersonnes[j] == CurrentPersonnes:
                CurrentPersonnesTotal += 1
        if len(listePersonnes) != 0:
            CurrentPersonnesPercent = CurrentPersonnesTotal / len(listePersonnes) * 100
        else:
            CurrentPersonnesPercent = 0
        listePersonnesProp += [CurrentPersonnesPercent]
    return listePersonnesProp


def ProportionNombre(phrase):
    listeNombres = ListageDesConjugaisons(phrase)[1]
    listeNombresDiff = ["Sing", "Plur"]  # ['s', 'p']
    listeNombresProp = []
    for i in range(len(listeNombresDiff)):
        CurrentNombres = listeNombresDiff[i]
        CurrentNombresProp = 0
        for j in range(len(listeNombres)):
            if listeNombres[j] == CurrentNombres:
                CurrentNombresProp += 1
        if len(listeNombres) != 0:
            CurrentNombresPercent = CurrentNombresProp / len(listeNombres) * 100
        else:
            CurrentNombresPercent = 0
        listeNombresProp += [CurrentNombresPercent]
    return listeNombresProp


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ['Proportion1erePersonne', 'Proportion2emePersonne', 'Proportion3emePersonne',
                           'ProportionSingulier', 'ProportionPluriel']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [ProportionPersonnes(phrase) + ProportionNombre(phrase)]
    if 'ConjugaisonFeatures.csv' not in os.listdir(os.getcwd()):
        with open('ConjugaisonFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('ConjugaisonFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return 'ConjugaisonFeatures.csv'

