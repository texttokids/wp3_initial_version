import os
import csv

Matrice_confusion = [
    [0.54, 0.01, 0.01, 0.06, 0.05, 0.00, 0.03, 0.01, 0.00, 0.00, 0.00, 0.00, 0.02, 0.05, 0.14, 0.01, 0.03, 0.00, 0.00,
     0.01, 0.03, 0.00, 0.00, 0.00, 0.01, 0.00],
    [0.03, 0.53, 0.01, 0.02, 0.01, 0.00, 0.06, 0.21, 0.01, 0.00, 0.07, 0.00, 0.00, 0.02, 0.01, 0.00, 0.01, 0.00, 0.00,
     0.00, 0.02, 0.01, 0.00, 0.01, 0.00, 0.00],
    [0.01, 0.03, 0.23, 0.02, 0.02, 0.03, 0.02, 0.01, 0.09, 0.02, 0.09, 0.00, 0.01, 0.03, 0.01, 0.00, 0.00, 0.18, 0.04,
     0.02, 0.02, 0.02, 0.01, 0.10, 0.01, 0.01],
    [0.01, 0.03, 0.01, 0.80, 0.01, 0.00, 0.01, 0.00, 0.01, 0.06, 0.00, 0.01, 0.01, 0.01, 0.01, 0.01, 0.00, 0.00, 0.00,
     0.00, 0.01, 0.01, 0.00, 0.01, 0.00, 0.01],
    [0.18, 0.03, 0.00, 0.05, 0.21, 0.01, 0.05, 0.02, 0.01, 0.02, 0.00, 0.00, 0.01, 0.06, 0.27, 0.02, 0.03, 0.00, 0.01,
     0.00, 0.01, 0.00, 0.02, 0.00, 0.00, 0.00],
    [0.00, 0.00, 0.01, 0.03, 0.00, 0.27, 0.01, 0.01, 0.06, 0.15, 0.01, 0.26, 0.00, 0.00, 0.00, 0.01, 0.01, 0.11, 0.01,
     0.02, 0.02, 0.00, 0.00, 0.01, 0.01, 0.00],
    [0.03, 0.00, 0.00, 0.01, 0.02, 0.00, 0.75, 0.01, 0.00, 0.01, 0.00, 0.00, 0.00, 0.03, 0.02, 0.01, 0.07, 0.00, 0.01,
     0.00, 0.02, 0.00, 0.01, 0.00, 0.00, 0.01],
    [0.01, 0.05, 0.00, 0.01, 0.00, 0.00, 0.02, 0.69, 0.01, 0.00, 0.09, 0.01, 0.01, 0.05, 0.01, 0.01, 0.00, 0.00, 0.00,
     0.02, 0.01, 0.00, 0.02, 0.00, 0.00, 0.00],
    [0.01, 0.01, 0.03, 0.02, 0.00, 0.03, 0.00, 0.01, 0.34, 0.09, 0.01, 0.18, 0.01, 0.00, 0.01, 0.01, 0.01, 0.01, 0.00,
     0.07, 0.01, 0.00, 0.00, 0.03, 0.01, 0.00],
    [0.00, 0.00, 0.00, 0.01, 0.00, 0.03, 0.01, 0.00, 0.03, 0.80, 0.00, 0.10, 0.00, 0.00, 0.00, 0.00, 0.01, 0.01, 0.00,
     0.01, 0.01, 0.00, 0.00, 0.00, 0.01, 0.00],
    [0.01, 0.01, 0.00, 0.00, 0.01, 0.02, 0.01, 0.07, 0.01, 0.00, 0.71, 0.03, 0.00, 0.02, 0.01, 0.00, 0.00, 0.01, 0.01,
     0.02, 0.01, 0.00, 0.01, 0.03, 0.01, 0.01],
    [0.00, 0.00, 0.00, 0.01, 0.00, 0.07, 0.00, 0.00, 0.12, 0.09, 0.02, 0.50, 0.01, 0.00, 0.00, 0.01, 0.01, 0.06, 0.01,
     0.09, 0.00, 0.01, 0.00, 0.01, 0.01, 0.00],
    [0.03, 0.00, 0.01, 0.00, 0.00, 0.00, 0.01, 0.01, 0.01, 0.02, 0.01, 0.01, 0.73, 0.06, 0.00, 0.01, 0.01, 0.02, 0.01,
     0.00, 0.01, 0.01, 0.02, 0.02, 0.01, 0.00],
    [0.05, 0.01, 0.01, 0.02, 0.01, 0.01, 0.02, 0.03, 0.02, 0.01, 0.00, 0.01, 0.02, 0.64, 0.03, 0.01, 0.02, 0.03, 0.02,
     0.00, 0.01, 0.01, 0.01, 0.01, 0.00, 0.00],
    [0.14, 0.02, 0.03, 0.01, 0.10, 0.01, 0.02, 0.01, 0.01, 0.00, 0.00, 0.00, 0.01, 0.07, 0.42, 0.03, 0.02, 0.02, 0.02,
     0.01, 0.05, 0.01, 0.01, 0.01, 0.01, 0.00],
    [0.03, 0.03, 0.01, 0.02, 0.03, 0.00, 0.03, 0.02, 0.00, 0.00, 0.02, 0.00, 0.02, 0.14, 0.06, 0.50, 0.01, 0.01, 0.01,
     0.01, 0.02, 0.00, 0.02, 0.01, 0.01, 0.00],
    [0.15, 0.00, 0.00, 0.02, 0.01, 0.02, 0.10, 0.01, 0.01, 0.01, 0.01, 0.01, 0.03, 0.04, 0.02, 0.05, 0.44, 0.01, 0.01,
     0.01, 0.02, 0.00, 0.01, 0.00, 0.00, 0.00],
    [0.02, 0.01, 0.02, 0.01, 0.01, 0.03, 0.02, 0.01, 0.07, 0.02, 0.03, 0.01, 0.00, 0.02, 0.01, 0.02, 0.00, 0.37, 0.04,
     0.05, 0.01, 0.03, 0.03, 0.09, 0.04, 0.03],
    [0.06, 0.03, 0.02, 0.00, 0.07, 0.00, 0.02, 0.04, 0.02, 0.01, 0.06, 0.01, 0.01, 0.13, 0.07, 0.01, 0.02, 0.05, 0.14,
     0.01, 0.01, 0.06, 0.05, 0.07, 0.03, 0.02],
    [0.00, 0.00, 0.03, 0.02, 0.00, 0.04, 0.01, 0.01, 0.14, 0.02, 0.09, 0.15, 0.00, 0.01, 0.00, 0.00, 0.01, 0.10, 0.03,
     0.24, 0.02, 0.02, 0.00, 0.02, 0.03, 0.02],
    [0.08, 0.04, 0.01, 0.03, 0.02, 0.00, 0.02, 0.01, 0.01, 0.01, 0.02, 0.01, 0.02, 0.04, 0.05, 0.00, 0.00, 0.01, 0.01,
     0.00, 0.50, 0.07, 0.03, 0.01, 0.01, 0.01],
    [0.00, 0.03, 0.00, 0.00, 0.01, 0.00, 0.00, 0.01, 0.02, 0.00, 0.03, 0.01, 0.00, 0.03, 0.03, 0.00, 0.00, 0.03, 0.02,
     0.01, 0.04, 0.51, 0.15, 0.02, 0.05, 0.01],
    [0.01, 0.00, 0.01, 0.01, 0.01, 0.01, 0.01, 0.00, 0.00, 0.00, 0.01, 0.00, 0.03, 0.03, 0.00, 0.00, 0.00, 0.01, 0.00,
     0.00, 0.01, 0.06, 0.73, 0.02, 0.03, 0.01],
    [0.01, 0.01, 0.02, 0.01, 0.01, 0.00, 0.01, 0.01, 0.04, 0.01, 0.08, 0.01, 0.01, 0.04, 0.01, 0.01, 0.00, 0.05, 0.03,
     0.01, 0.01, 0.04, 0.02, 0.53, 0.02, 0.03],
    [0.01, 0.00, 0.00, 0.00, 0.00, 0.02, 0.01, 0.00, 0.01, 0.00, 0.02, 0.00, 0.00, 0.01, 0.01, 0.01, 0.00, 0.01, 0.00,
     0.01, 0.02, 0.13, 0.02, 0.02, 0.67, 0.02],
    [0.01, 0.00, 0.04, 0.02, 0.01, 0.02, 0.03, 0.00, 0.03, 0.02, 0.03, 0.00, 0.01, 0.01, 0.02, 0.01, 0.00, 0.10, 0.05,
     0.01, 0.03, 0.07, 0.04, 0.16, 0.10, 0.19]]


def FreqLettres(lettre):
    switcher = {
        'e': 12.10, 'a': 7.11, 'i': 6.59, 's': 6.51, 'n': 6.39, 'r': 6.07, 't': 5.92, 'o': 5.02, 'l': 4.96, 'u': 4.49,
        'd': 3.67, 'c': 3.18, 'm': 2.62, 'p': 2.49,
        'é': 1.94, 'g': 1.23, 'b': 1.14, 'v': 1.11, 'h': 1.11, 'f': 1.11, 'q': 0.65, 'y': 0.46, 'x': 0.38, 'j': 0.34,
        'è': 0.31, 'à': 0.31, 'k': 0.29, 'w': 0.17,
        'z': 0.15, 'ê': 0.08, 'ç': 0.06, 'ô': 0.04, 'â': 0.03, 'î': 0.03, 'û': 0.02, 'ù': 0.02, 'ï': 0.01, 'ü': 0.01,
        'ë': 0.01, 'ö': 0.01, 'í': 0.01,
    }
    return switcher.get(lettre, 0)  # On suppose que les autres caractères sont trop rares, y compris les majuscules


def confusion(lettre):
    if lettre in ['é', 'è', 'ê', 'ë']:
        lettre = 'e'
    if lettre in ['î', 'ï']:
        lettre = 'i'
    if lettre in ['ù', 'û', 'í']:
        lettre = 'u'
    if lettre in ['ö', 'ô']:
        lettre = 'o'
    if lettre in ['à', 'â']:
        lettre = 'a'
    if lettre == 'ç':
        lettre = 'c'
    if 'a' <= lettre <= 'z':
        rank = (ord(lettre) - ord('a'))
    else:
        rank = None
    return rank


def confusion_dechiffrage(mot):
    confusion_mot = 0
    mot = mot.lower()
    for i in range(len(mot) - 1):
        lettre1 = mot[i]
        lettre2 = mot[i + 1]
        ligne = confusion(lettre1)
        colonne = confusion(lettre2)
        if lettre1 != lettre2 and ((ligne is not None) and (colonne is not None)):
            confusion_mot += Matrice_confusion[ligne][colonne]
    return confusion_mot


def frequence_mot(mot):
    freq_mot = []
    for i in range(len(mot)):
        freq_mot += [FreqLettres(mot[i])]
    else:
        freq_mot += [0]
    return sum(freq_mot) / len(freq_mot)


def GlobalisationGraphie(phrase):
    Freq_phrase = []
    GlobFreq = []

    Conf_phrases = []
    GlobConf = []

    Taille_phrase = []
    GlobTaille = []

    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            mot = phrase[i].get('lemma', phrase[i].get('text'))  # form
            Freq_phrase += [frequence_mot(mot)]
            Conf_phrases += [confusion_dechiffrage(mot)]
            Taille_phrase += [len(mot)]

    if not Freq_phrase:  # Globalisation des variables locales de fréquence,par le calcul de la moyenne et la variance
        GlobFreq = [0, 0]
    else:
        n = len(phrase)
        MoyFreq = sum(Freq_phrase) / n
        VarFreq = sum([(f ** 2 - MoyFreq ** 2) / n for f in Freq_phrase])
        GlobFreq = [MoyFreq, VarFreq]

    if not Conf_phrases:
        GlobConf = [0, 0]
    else:
        n = len(Conf_phrases)
        MoyConf = sum(Conf_phrases) / n
        VarConf = sum([(f ** 2 - MoyConf ** 2) / n for f in Conf_phrases])
        GlobConf = [MoyConf, VarConf]

    if not Taille_phrase:
        GlobTaille = [0, 0]
    else:
        n = len(Taille_phrase)
        MoyTaille = sum(Taille_phrase) / n
        VarTaille = sum([(t ** 2 - MoyTaille ** 2) / n for t in Taille_phrase])
        GlobTaille = [MoyTaille, VarTaille]

    #return GlobFreq + GlobConf + GlobTaille
    return GlobFreq + GlobTaille


def longueur_phrase(phrase):
    Nombre_mots = 0
    for i in range(len(phrase)):
        if phrase[i].get('upos') != 'PUNCT':
            Nombre_mots += 1
    return Nombre_mots


def CsvMakerPhrases(ListOfPhrases):
    ListfeaturesPhrases = ['longueur_phrase', 'FrequenceMoyenne', 'VarianceFrequence', 'ConfusionMoyenne',
                           'VarianceConfusion', 'LongueurMotsMoyenne', 'VarianceLongueurMots']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        ListfeaturesEX += [[longueur_phrase(phrase)] + GlobalisationGraphie(phrase)]
    if 'GraphieFeatures.csv' not in os.listdir(os.getcwd()):
        with open('GraphieFeatures.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesPhrases)
    with open('GraphieFeatures.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return 'GraphieFeatures.csv'


def CsvMakerMots(ListOfPhrases):
    ListfeaturesMot = ['longeur_mot', 'confusion_dechiffrage', 'frequence_mot']
    ListfeaturesEX = []
    for i in range(len(ListOfPhrases)):
        phrase = ListOfPhrases[i]
        for j in range(len(phrase)):
            if phrase[j].get('upos') != 'PUNCT':
                mot = phrase[j].get('lemma')  # form
                ListfeaturesEX += [[len(mot), confusion_dechiffrage(mot), frequence_mot(mot)]]
    if 'GraphieFeaturesLocals.csv' not in os.listdir(os.getcwd()):
        with open('GraphieFeaturesLocals.csv', 'w', newline='', encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter="\t")
            spamwriter.writerow(ListfeaturesMot)
    with open('GraphieFeaturesLocals.csv', 'a', newline='', encoding='utf-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter="\t")
        for i in range(len(ListfeaturesEX)):
            spamwriter.writerow(ListfeaturesEX[i])
    return "GraphieFeaturesLocals.csv"
