# Text Complexity

This project is part of the project TextToKids in collaboration with the [IRISA](https://www.irisa.fr/), [MODYCO](https://modyco.fr/), [QWANT](https://www.qwant.com/), and the [ALBERT](https://www.journal-albert.fr/) newspaper.

The aim of this project is to bring together processors to measure the characterization of the complexity of a text:

- either at the sentence level

- or at text level

This contains lists of available processors:

- plongements :           camembert
- graphie/typographie :   graphie
- phonétique/phonologie : phonetique
- lexique :               niveau_lexical, richesse_lexicale, adjectifs_ordinaux
- morphosyntaxe :         flexions_verbales, parties_du_discours, pronoms, pluriels
- syntaxe :               dependances_syntaxiques, structures_passives, structures_syntaxiques, superlatifs_inferiorite
- sémantique :            adverbiaux_temporels, emotions, connecteurs_organisateurs,  entites_nommees, propositions_subordonnees, metaphores, modalite, emotyc
- complexité :            age

## Client

To generate the different csv files containing a list of processed features, you can use the client "text_complexity_client"

# Server Installation

In order to use the complete processing chain, it is necessary to install the necessary elements for the environment.

The processing chain is tested and validated under Windows 10 and Linux Ubuntu.

Installation of the necessary libraries for the environment:
The installation of the chain is done by creating a new blank anaconda environment. 
You will first have to install the following analysis utilities, following the instructions dedicated to each tool: 

### Prerequisites
Windows :

Install  Windows eSpeak (http://espeak.sourceforge.net/download.html)


Linux :

Install eSpeak 

Unitex installation is required only for Linux deployment.

Linux:

Install UNITEX (https://unitexgramlab.org/fr)
    Télécharger Unitex-GramLab-3.3-linux-i686.run

Then
```
chmod a+x Unitex-GramLab-3.3-linux-i686.run
./Unitex-GramLab-3.3-linux-x86_64.run
```
A UNITEX folder will be created on your machine, you will then have to modify the configuration file \src\processor\semantique\adverbiaux_temporels.config
Complete the path "unitex_linux_directory" to point to your UNITEX App folder.

### Creation env

i. Install anaconda: conda >= 4.10.1 et Pip

ii. Create the virtual environment :
```
conda create -n t2k_env python=3.7.4
conda activate t2k_env
pip install -r requirements.txt
```

Pour Macos Apple M1
```
install Miniforge https://github.com/conda-forge/miniforge
conda create -n t2k_310_env python=3.10
conda activate t2k_310_env

conda install -c conda-forge scikit-learn
conda install numpy scipy matplotlib
conda install h5py spacy
conda install --file reqconda.txt

pip install Werkzeug
pip install flask-restx
pip install flask

pip install -r req_specific.txt

# si besoin lorsqu'une demande de bibliothèque complémentaire lors de l'execution est demandée
pip3 install --upgrade requests

# variante pour utiliser les embedings
conda install -c apple tensorflow-deps
python -m pip install tensorflow-macos
python -m pip install tensorflow-metal
pip3 install torch torchvision torchaudio
pip install ktrain
# ne fonctionne pas avec la version actuelle du code "age"

```

Finally, download the corresponding NLP packages for stanza, spacy and NLTK:

```
#le package TAL français pour stanza: 
python -c "import stanza; stanza.download('fr')"
#Télécharger les ressources linguistiques pour NLTK:
python -c "import nltk; nltk.download('punkt')"
```

## Parameters settings

To specify the age prediction models path, edit src/api/settings.py and set the path in the variable SENTENCE_LEVEL_AGE_MODEL_PATH

```
# Sentence age prediction model path
SENTENCE_LEVEL_AGE_MODEL_PATH = '../age_model/models_for_fiction_data_TALN22_v2'
```

## Run the server

To run locally:

```
cd src
python run_dev.py
```

For production environment : 

Set the flask server name in `text_complexity_server/src/api/settings.py`:

```
FLASK_SERVER_NAME = '[SERVER_ADDRESS]'
```

Launch the service:

```
cd src
python run_prod.py
```


### Contact

lucile.callebert@synapse-fr.com, guilhem.valentin@synapse-fr.com 
