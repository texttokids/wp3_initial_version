from setuptools import setup, find_packages

setup(
    name='text-complexity',
    version='1.0.0',
    description='Extract complexity and description elements from a text files corpus',
    url='https://synapse-gitlab.westeurope.cloudapp.azure.com/thibault.beaufort/text-complexity',
    author='Thiziri Belkacem, Thibault Beaufort, Clément Tourne, Charles Teissèdre',

    packages=find_packages(),

    install_requires=['flask-restplus==0.9.2', 'Flask-SQLAlchemy==2.1'],
)
