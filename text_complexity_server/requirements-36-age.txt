Keras==2.6.0
Werkzeug==1.0.1
conllu==2.3
flask-restx
flask
h5py==3.1.0
json5==0.9.5
jsonable==0.3.1
jsonpickle==2.0.0
lexicalrichness==0.1.3
markupsafe==1.1.1
nltk==3.5
pandas==1.1.5
protobuf==3.13.0
scikit-learn==0.23.2
# scikit-multilearn
scipy==1.5.2
sklearn
# spacy-readability==1.4.1
spacy==2.3.2
stanza==1.4.0
textblob-fr==0.2.0
textblob==0.15.3
textstat==0.6.2
torch==1.10.1
tqdm==4.62.3
transformers==4.10
ktrain
# waitress==1.4.4
# wget==3.2
