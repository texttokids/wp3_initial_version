Keras==2.4.3
PyHyphen==3.0.1
Werkzeug==0.16.1
conllu==4.2.2
flask-restplus==0.13.0
flask=1.1.2
## h5py==2.10.0
json5==0.9.5
jsonable==0.3.1
jsonpickle==2.0.0
#ktrain==0.20.2
lexicalrichness==0.1.3
markupsafe==2.0.1
nltk==3.5
pandas==1.3.4
protobuf==3.20
# scikit-learn==0.23.2
scikit-multilearn==0.2.0
# scipy==1.4.1
sklearn==0.0
spacy-readability==1.4.1
## spacy==2.3.4
stanza==1.1.1
# tensorflow==2.1.0
textblob-fr==0.2.0
textblob==0.15.3
textstat==0.6.2
# torch==1.9.1
tqdm==4.41.1
transformers==3.0.2
waitress==1.4.4
wget==3.2

scikit-learn==1.2.0
tensorflow==2.13.0rc0
transformers==4.17.0
ktrain==0.35.1

